<PARAGRAPH> Two people are discussing whether or not the civil rights activists are for gay marriage.
S1 states that the comparison of a race of people's fight for equal rights and a class of people determined by their sexual orientation should not be made, as it is an insult to all African Americans.
He also wonders how Rev. MLK would feel about the gay rights issue.
S2 retorts by saying that Coretta Scott King and Julian Bond are both for gay marriage.
