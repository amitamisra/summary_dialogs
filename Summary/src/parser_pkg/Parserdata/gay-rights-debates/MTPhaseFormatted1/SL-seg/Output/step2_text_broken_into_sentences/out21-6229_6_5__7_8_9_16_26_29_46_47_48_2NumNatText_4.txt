<PARAGRAPH> S1 says that someone, "her", is trying to put their religious beliefs into law that goes against the religious beliefs of S1.
The law would bar the family of S1 from legal protections but would not affect her.
S1 believes that she is not protecting marriage but preserving her heterosexual privilege.
Her religious belief is that marriage should be between a man and a woman but S1 believes that marriage should be between two people in a loving relationship regardless of sex.
If she put her religious belief into law it would hurt S1 but if the religious views of S1 are put into law she would still be able to choose who she wanted and S1 would be able to choose whomever.
She is trying to keep gay people out of marriage.
S2 understands the perspective of S1 but does not think it is a valid argument.
S2 holds that either if either religious belief were put into law that it would infringe on the other's beliefs.
S2 claims that S1 has not shown how the religious beliefs would be taken away from such a law.
It can be argued in other ways but the idea of not giving gays marriage benefits is imposing on religious freedom is an empty argument.
S2 counters S1 by saying that it is impossible to know whether or not she is trying to turn religious belief into law.
It is impossible to know or to police someone's real motives.
