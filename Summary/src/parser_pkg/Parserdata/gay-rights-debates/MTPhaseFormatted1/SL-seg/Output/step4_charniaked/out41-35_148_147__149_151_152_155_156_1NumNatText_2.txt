(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ claims) (SBAR (IN that) (S (NP (JJS most) (NNS Christians)) (VP (AUX are) (RB not) (ADVP (RB actively)) (VP (VBG attempting) (S (VP (TO to) (VP (VB ban) (NP (NN homosexuality)))))))))) (CC and) (VP (VBZ supports) (NP (DT this) (NN claim)) (PP (IN by) (S (VP (VBG citing) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (NP (NNP Evangelical) (NNP Christendom)) (PP (IN as) (NP (DT a) (NN whole)))) (VP (AUX is) (ADVP (RB rarely)) (VP (ADVP (RB ever)) (VBN united) (PP (IN on) (NP (NN anything))) (, ,) (SBAR (IN so) (S (NP (PRP they)) (VP (MD could) (RB not) (ADVP (RB all)) (VP (AUX be) (ADJP (VBN opposed) (PP (TO to) (NP (NN homosexuality)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ proposes) (SBAR (IN that) (S (NP (JJ gay) (NNS lobbyists)) (VP (AUX have) (VP (VBN crossed) (NP (NP (DT the) (NN line)) (PP (PP (IN from) (NP (NN counter-attacking))) (PP (TO to) (S (VP (ADVP (RB simply)) (VBG attacking) (NP (NNS Christians)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJS most) (NNS Christians)) (VP (AUX do) (VP (VB oppose) (NP (NP (JJ same-sex) (NN marriage)) (, ,) (NP (JJ civil) (NNS unions)) (, ,) (NP (JJ gay) (NN adoption)) (, ,) (CC and) (NP (JJ anti-discrimination) (NNS laws)))))))) (. .)))

(S1 (S (S (VP (TO To) (VP (VB support) (NP (DT this) (NN point))))) (, ,) (NP (PRP he)) (VP (VBZ cites) (NP (NP (DT the) (JJ various) (NN state) (JJ constitutional) (NNS amendments)) (SBAR (WHNP (WDT that)) (S (VP (VBP ban) (NP (JJ same-sex) (ADJP (NN marriage) (CC and) (JJ civil)) (NNS unions))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NP (DT this) (NN opinion)) (PP (IN on) (NP (NP (DT the) (NN topic)) (PP (IN of) (NP (NN homosexuality)))))) (VP (AUX is) (ADJP (JJ unique) (PP (TO to) (NP (NNP America)))) (, ,) (S (VP (VBG claiming) (SBAR (IN that) (S (NP (NP (JJ Evangelical) (NNS Christians)) (PP (IN in) (NP (NNP Spain)))) (VP (AUX are) (RB not) (ADJP (RB actively) (VBN opposed) (PP (TO to) (NP (JJ civil) (NNS unions)))) (PP (IN despite) (S (VP (VBG sharing) (NP (DT the) (JJ same) (JJ religious) (NN belief) (NNS systems)))))))))))))) (. .)))

