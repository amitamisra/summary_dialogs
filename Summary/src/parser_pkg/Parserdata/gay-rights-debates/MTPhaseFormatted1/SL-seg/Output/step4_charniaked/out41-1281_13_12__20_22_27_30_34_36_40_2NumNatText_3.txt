(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS subjects)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ gay) (NN marriage) (NNS rights)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (NP (JJ anti-marriage) (NN redefinition)) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (NP (JJ pro-gay) (NN marriage)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (NN marriage)) (VP (VP (AUX is) (VP (VBN classified) (PP (IN by) (NP (DT a) (JJ heterosexual) (NN contract))))) (CC and) (VP (AUX is) (RB not) (ADJP (JJ legal) (CC or) (JJ natural)) (SBAR (IN if) (S (NP (PRP it)) (VP (VBZ involves) (NP (NP (NNS couples)) (PP (IN of) (NP (DT the) (JJ same) (NN sex)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ challenges) (NP (NP (DT the) (JJ religious) (NNS aspects)) (PP (IN of) (NP (NP (NNP S1) (POS 's)) (NN belief)))) (SBAR (IN because) (S (NP (NNP God)) (VP (AUX is) (RB not) (VP (VBN involved) (PP (PP (IN in) (NP (NP (DT the) (NNS ceremonies)) (VP (VBN performed) (PP (IN by) (NP (DT a) (NN judge)))))) (CC and) (RB not) (PP (IN in) (NP (DT a) (NN church))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ offers) (NP (NN information)) (PP (IN in) (NP (NP (NN regard)) (PP (TO to) (NP (NP (DT a) (NN tribe)) (PP (IN in) (NP (NP (DT the) (NNP Congo)) (SBAR (WHNP (WDT which)) (S (VP (VBD required) (S (NP (PRP$ its) (NNS warriors)) (VP (TO to) (VP (VB marry) (NP (JJ other) (NNS warriors)) (SBAR (IN as) (S (NP (PRP they)) (VP (VBD believed) (SBAR (S (NP (NP (NNS relations)) (PP (IN with) (NP (NNS women)))) (VP (MD would) (VP (VB soften) (NP (PRP them)))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ advises) (SBAR (S (NP (NNS values)) (VP (AUX are) (ADVP (RB always)) (VP (VBN believed) (S (VP (TO to) (VP (AUX be) (ADJP (JJ correct)) (PP (IN by) (NP (NP (DT the) (NN person) (CC or) (NN group)) (SBAR (WHNP (WP who)) (S (VP (NNS practices) (NP (PRP them))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB think) (SBAR (S (NP (NNS morals)) (VP (MD should) (VP (AUX be) (VP (VBN forced)))))))) (. .)))

