<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ marriage/NN to/TO be/AUX both/PDT a/DT legal/JJ contract/NN as/RB well/RB as/IN a/DT religious/JJ ceremony/NN and/CC thinks/VBZ the/DT government/NN and/CC society/NN is/AUX stuck/VBN in/IN this/DT duality/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ the/DT courts/NNS must/MD deal/VB with/IN this/DT </C>
<C>and/CC different/JJ religions/NNS may/MD do/AUX as/IN they/PRP wish/VBP ./. </C>
</S>
<S>
<C>Winning/VBG argument/NN from/IN states/NNS that/WDT declared/VBD banning/VBG gay/JJ marriage/NN unconstitutional/JJ ,/, he/PRP says/VBZ ,/, are/AUX based/VBN on/IN state/NN constitutional/JJ provisions/NNS that/WDT closely/RB follow/VBP the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ gay/JJ rights/NNS are/AUX a/DT real/JJ test/NN of/IN the/DT constitutional/JJ system/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ that/IN violating/VBG a/DT state/NN constitution/NN has/AUX nothing/NN to/TO do/AUX with/IN the/DT federal/JJ one/NN </C>
<C>and/CC that/IN many/JJ state/NN constitutions/NNS give/VBP rights/NNS ,/, such/JJ as/IN prohibiting/VBG discrimination/NN against/IN gays/NNS ,/, </C>
<C>that/WDT the/DT federal/JJ one/NN does/AUX not/RB grant/VB ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ it/PRP should/MD always/RB be/AUX clear/JJ which/WDT constitution/NN is/AUX being/AUXG discussed/VBN ./. </C>
</S>
</P>
</T>
