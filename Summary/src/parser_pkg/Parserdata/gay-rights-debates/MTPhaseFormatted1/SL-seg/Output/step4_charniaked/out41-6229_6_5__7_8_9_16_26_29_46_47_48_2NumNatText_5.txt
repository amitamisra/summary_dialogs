(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (VP (VBG talking) (PP (IN about) (NP (NP (DT a) (NN female)) (SBAR (WHNP (WP who)) (S (VP (AUX is) (VP (VP (VBG ignoring) (NP (NP (NNP S1) (POS 's)) (JJ religious) (NN freedom))) (CC and) (VP (VBG barring) (NP (PRP$ his) (NN family)) (PP (IN from) (NP (JJ legal) (NNS protections)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ questions) (PP (TO to) (SBAR (WHADVP (WRB how)) (S (NP (PRP she)) (VP (AUX is) (VP (VBG impeding) (NP (NNP S1)) (PP (IN from) (NP (PRP$ his) (JJ religious) (NN freedom))))))))) (CC and) (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP she)) (VP (AUX is) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB take) (PRT (RP away)) (NP (NP (PRP$ his) (NN right)) (PP (IN from) (NP (DT a) (JJ religious) (NN ceremony))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (S (NP (NP (DT the) (NN woman) (POS 's)) (JJ religious) (NN belief)) (VP (AUX is) (SBAR (IN that) (S (NP (NN marriage)) (VP (MD should) (VP (AUX be) (PP (IN between) (NP (NP (DT a) (NN man)) (CC and) (NP (DT a) (NN woman)))))))))) (, ,) (CC and) (S (NP (NP (NNP S1) (POS 's)) (JJ religious) (NN belief)) (VP (AUX is) (SBAR (IN that) (S (NP (NN marriage)) (VP (MD can) (VP (AUX be) (PP (IN between) (NP (NP (DT any) (CD two) (NNS people)) (SBAR (WHNP (WDT that)) (S (VP (VBP love) (NP (DT each) (JJ other)) (ADVP (RB regardless) (PP (IN of) (NP (PRP$ their) (NN gender)))))))))))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (NP (DT the) (NN woman) (POS 's)) (JJ religious) (NN belief)) (VP (VBZ gets) (VP (VBN incorporated) (PP (IN in) (PP (TO to) (NP (DT the) (NN law)))))))) (, ,) (S (NP (NNP S1)) (VP (MD will) (RB not) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB marry) (NP (NP (DT the) (NN person)) (PP (IN of) (NP (PRP$ his) (NN choice))))))))))) (CC but) (S (NP (PRP she)) (VP (MD will) (ADVP (RB still)) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB marry) (ADVP (RB regardless) (PP (IN of) (NP (DT the) (NN law))))))))))) (. .)))

(S1 (S (NP (DT The) (NN woman)) (VP (AUX is) (ADVP (RB just)) (VP (VBG trying) (S (VP (TO to) (VP (VB keep) (NP (JJ gay) (NNS people)) (PP (IN out) (PP (IN of) (NP (NN marriage) (S (VP (TO to) (VP (VB preserve) (NP (PRP$ her) (JJ heterosexual) (NN privilege))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT either) (CD one)) (PP (IN of) (NP (PRP them)))) (VP (MD can) (VP (AUX be) (VP (VBG impeding) (PP (IN on) (NP (DT the) (NNS others))) (NP (JJ religious) (NN freedom)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX does) (RB not) (VP (VB give) (PRT (RP in)))) (CC and) (VP (VBZ keeps) (S (VP (VBG arguing) (SBAR (IN that) (S (NP (PRP she)) (VP (AUX is) (VP (VBG trying) (S (VP (TO to) (VP (VP (VB turn) (NP (PRP$ her) (JJ religious) (NN belief)) (PP (IN into) (NP (NN law)))) (CC and) (VP (VB bar) (NP (PRP him)) (PP (IN from) (S (VP (VBG getting) (VP (VBN married))))))))))))))))) (. .)))

