<PARAGRAPH> S1 asks S2 to clarify his statement about Jeffersonian liberalism.
He believes that S2 is hypocritical about supporting Jeffersonian values on "making sure there is fairness" and "honest advocacy" for the people while opposing gay marriage.
S2 believes that the term "liberalism" has a different meaning different compared to the time of Thomas Jefferson.
He said that Jefferson is for decentralization of government which is the opposite stance taken by the Democratic Party.
He believes that liberalism has been hijacked by extremist special interest groups such as the gay lobby.
The Democratic Party today is favoring special interest groups instead of engaging in honest advocacy for the people.
S2 supports equal opportunity for gay people, but not the redefinition of marriage in the US.
