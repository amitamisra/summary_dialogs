(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (MD should) (VP (VB stay) (ADVP (RB out) (PP (IN of) (NP (NN marriage)))))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (VP (AUX be) (VP (VBN left) (PP (TO to) (NP (JJ religious) (NNS institutions)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (SBAR (S (NP (EX there)) (VP (VBZ needs) (S (VP (TO to) (VP (AUX be) (NP (DT a) (JJR better) (NN system)))))))) (CC and) (SBAR (IN that) (S (NP (JJ single) (NNS people)) (VP (AUX are) (NP (NP (DT the) (NNS ones)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (VP (VBN harmed) (NP (DT the) (JJS most)) (PP (IN by) (NP (NN marriage) (NNS laws))) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ unable) (S (VP (TO to) (VP (VB get) (NP (NP (DT any)) (PP (IN of) (NP (NP (DT the) (NNS benefits)) (SBAR (IN that) (S (S (NP (JJ married) (NNS people)) (VP (AUX do) (SBAR (RB even) (IN if) (S (NP (PRP they)) (VP (VBP want) (NP (PRP them))))))) (, ,) (CC or) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ important) (PP (TO to) (NP (PRP$ their) (NN situation)))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (S (NP (JJ religious) (NNS ceremonies)) (VP (AUX are) (RB not) (SBAR (WHNP (WP what)) (S (NP (JJ gay) (NNS people)) (VP (VBP want) (SBAR (IN because) (S (NP (PRP they)) (ADVP (RB already)) (VP (MD can) (VP (AUX have) (NP (PRP them)) (PP (IN via) (NP (NNS churches)))))))))))))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (VBP want) (NP (DT the) (NNS rights)))) (CC and) (S (S (VP (TO to) (VP (VB keep) (NP (DT the) (NN government)) (PRT (RP out))))) (VP (MD would) (VP (AUX be) (S (VP (TO to) (VP (VB give) (PRT (RP up)) (NP (DT those) (NNS rights)))))))) (. .)))

(S1 (S (S (SBAR (IN If) (S (NP (JJ single) (NNS people)) (VP (VBP want) (NP (DT those) (NNS rights))))) (NP (PRP they)) (VP (MD should) (VP (VB get) (VP (VBN married))))) (, ,) (CC but) (S (NP (PRP he)) (VP (VBZ thinks) (SBAR (S (NP (PRP you)) (VP (MD should) (VP (AUX be) (ADJP (JJ free) (S (VP (TO to) (VP (VB marry) (SBAR (WHNP (WP who)) (S (NP (PRP you)) (VP (VBP wish)))))))))))))) (. .)))

