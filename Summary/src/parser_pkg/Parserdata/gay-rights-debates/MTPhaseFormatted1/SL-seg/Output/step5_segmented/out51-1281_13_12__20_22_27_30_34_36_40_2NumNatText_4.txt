<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ marriage/NN is/AUX recognizing/VBG a/DT family/NN unit/NN based/VBN on/IN natural/JJ heterosexual/JJ contact/NN ./. </C>
</S>
<S>
<C>When/WRB questioned/VBN about/IN artificial/JJ insemination/NN by/IN S2/NNP </C>
<C>he/PRP replies/VBZ that/IN it/PRP is/AUX not/RB the/DT way/NN that/IN the/DT children/NNS are/AUX created/VBN that/IN is/AUX important/JJ ./. </C>
</S>
<S>
<C>It/PRP is/AUX the/DT nature/NN of/IN the/DT relationship/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ that/IN values/NNS are/AUX what/WP guide/NN people/NNS and/CC everyone/NN thinks/VBZ theirs/PRP are/AUX correct/JJ ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ allowing/VBG gay/JJ marriage/NN forces/NNS values/NNS on/IN society/NN </C>
<C>by/IN forcing/VBG them/PRP to/TO accept/VB it/PRP ./. </C>
</S>
<S>
<C>S1/NNP questions/VBZ what/WP God/NNP enforces/VBZ marriage/NN rules/NNS </C>
<C>if/IN it/PRP is/AUX performed/VBN by/IN a/DT judge/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN there/EX is/AUX a/DT Congo/NNP tribe/NN in/IN which/WDT warriors/NNS marry/VB other/JJ warriors/NNS </C>
<C>because/IN being/AUXG with/IN a/DT woman/NN may/MD make/VB them/PRP less/RBR feared/VBN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ that/IN not/RB allowing/VBG gay/JJ marriage/NN is/AUX discrimination/NN and/CC forcing/VBG morals/NNS on/IN other/JJ people/NNS ./. </C>
</S>
</P>
</T>
