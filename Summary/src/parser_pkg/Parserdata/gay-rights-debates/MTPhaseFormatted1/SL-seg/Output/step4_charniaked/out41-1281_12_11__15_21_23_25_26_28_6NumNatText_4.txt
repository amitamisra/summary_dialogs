(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT all) (NN sex)) (VP (AUX is) (NP (NN behavior)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NP (RB just) (IN about) (JJ heterosexual) (NN contact)) (PP (IN in) (NP (JJ many) (NNS countries)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (EX there)) (VP (AUX are) (NP (NP (JJ many) (JJ married) (JJ heterosexual) (NNS couples)) (SBAR (WHNP (WP who)) (S (ADVP (RB either)) (VP (VBP choose) (RB not) (S (VP (TO to) (VP (VP (AUX have) (NP (NNS children))) (CC or) (VP (MD can) (RB not))))) (, ,) (SBAR (IN so) (S (NP (NN marriage)) (VP (AUX is) (RB not) (ADVP (RB simply)) (PP (IN about) (S (VP (AUXG having) (NP (DT a) (NN family)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (DT the) (S (NP (PRP US)) (VP (AUX is) (ADJP (RBR more) (JJ similar) (PP (TO to) (NP (NNP Anglo) (NNS nations))))))) (CC and) (SBAR (IN in) (S (NP (NP (JJ many)) (PP (IN of) (NP (DT those) (JJ gay) (NN marriage)))) (VP (AUX is) (ADJP (JJ legal))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (NN something)) (VP (AUX is) (RB not) (VP (VBN judged) (PP (IN as) (ADJP (JJ good))) (SBAR (ADVP (RB just)) (IN because) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ natural)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NN marriage)) (VP (AUX is) (VP (VBG recognizing) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (JJ natural) (JJ heterosexual) (NN contact)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (ADJP (NP (JJ many) (NNS laws)) (JJ different) (PP (IN than) (NP (NP (DT the) (NNP US)) (PP (IN in) (NP (JJ other) (NNS nations)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (NN family)) (VP (AUX is) (NP (NP (DT a) (JJ basic) (NN unit)) (VP (ADVP (RB traditionally)) (VBG consisting) (PP (IN of) (S (NP (CD two) (NNS parents)) (VP (VBG raising) (NP (NNS children))))))))))) (. .)))

(S1 (S (NP (NNS Homosexuals)) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ believes))) (, ,)) (VP (AUX are) (ADJP (RBR more) (JJ political))) (. .)))

