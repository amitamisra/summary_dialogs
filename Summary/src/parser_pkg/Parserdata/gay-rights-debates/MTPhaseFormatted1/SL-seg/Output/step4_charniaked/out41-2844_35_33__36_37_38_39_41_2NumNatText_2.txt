(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asks) (NP (NNP S2)) (S (VP (TO to) (VP (VB clarify) (NP (NP (PRP$ his) (NN statement)) (PP (IN about) (NP (NNP Jeffersonian) (NN liberalism)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX is) (ADJP (JJ hypocritical) (PP (IN about) (S (VP (VBG supporting) (NP (NP (NNP Jeffersonian) (NNS values)) (PP (IN on) (`` ``) (S (VP (VBG making) (ADJP (JJ sure) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (NN fairness)) ('' '') (CC and) (`` ``) (NP (JJ honest) (NN advocacy)) ('' '') (PP (IN for) (NP (DT the) (NNS people)))))))) (SBAR (IN while) (S (VP (VBG opposing) (NP (JJ gay) (NN marriage))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NN term) (`` ``) (NN liberalism) ('' '')) (VP (AUX has) (NP (NP (DT a) (JJ different) (NN meaning)) (ADJP (JJ different) (PP (VBN compared) (PP (TO to) (NP (NP (DT the) (NN time)) (PP (IN of) (NP (NNP Thomas) (NNP Jefferson)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (NP (NNP Jefferson)) (VP (AUX is) (PP (IN for) (NP (NP (NN decentralization)) (PP (IN of) (NP (NN government))) (SBAR (WHNP (WDT which)) (S (VP (AUX is) (NP (NP (DT the) (JJ opposite) (NN stance)) (VP (VBN taken) (PP (IN by) (NP (DT the) (NNP Democratic) (NNP Party)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN liberalism)) (VP (AUX has) (VP (AUX been) (VP (VBN hijacked) (PP (IN by) (NP (NP (NN extremist) (JJ special) (NN interest) (NNS groups)) (PP (JJ such) (IN as) (NP (DT the) (JJ gay) (NN lobby))))))))))) (. .)))

(S1 (S (NP (DT The) (NNP Democratic) (NNP Party)) (NP (NN today)) (VP (AUX is) (VP (VBG favoring) (NP (JJ special) (NN interest) (NNS groups)) (PP (RB instead) (IN of) (S (VP (VBG engaging) (PP (IN in) (NP (NP (JJ honest) (NN advocacy)) (PP (IN for) (NP (DT the) (NNS people)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ supports) (NP (NP (NP (JJ equal) (NN opportunity)) (PP (IN for) (NP (JJ gay) (NNS people)))) (, ,) (CONJP (CC but) (RB not)) (NP (NP (DT the) (NN redefinition)) (PP (IN of) (NP (NN marriage))) (PP (IN in) (NP (DT the) (NNP US)))))) (. .)))

