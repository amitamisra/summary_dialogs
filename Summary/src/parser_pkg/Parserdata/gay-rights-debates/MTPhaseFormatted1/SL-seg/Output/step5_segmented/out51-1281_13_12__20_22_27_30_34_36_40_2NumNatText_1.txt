<T>
<P>
</P>
<P>
<S>
<C>S1/NNP makes/VBZ the/DT claim/NN that/IN marriage/NN is/AUX defined/VBN as/IN a/DT family/NN unit/NN based/VBN on/IN natural/JJ heterosexual/JJ contact/NN ,/, </C>
<C>and/CC suggests/VBZ that/IN it/PRP is/AUX the/DT heterosexual/JJ nature/NN of/IN the/DT relationship/NN that/WDT is/AUX important/JJ ,/, </C>
<C>not/RB how/WRB the/DT children/NNS are/AUX conceived/VBN within/IN the/DT relationship/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN homosexuals/NNS are/AUX attempting/VBG to/TO force/VB their/PRP$ morals/NNS on/IN others/NNS </C>
<C>by/IN legalizing/VBG same-sex/JJ marriage/NN ,/, </C>
<C>claiming/VBG that/IN legalizing/VBG a/DT behavior/NN is/AUX a/DT tacit/JJ form/NN of/IN acceptance/NN that/WDT will/MD affect/VB society/NN ./. </C>
</S>
<S>
<C>S2/NNP questions/VBZ the/DT religious/JJ basis/NN of/IN a/DT civil/JJ ceremony/NN like/IN marriage/NN ,/, </C>
<C>suggesting/VBG that/IN the/DT stipulations/NNS based/VBN on/IN religious/JJ reasons/NNS would/MD vary/VB depending/VBG on/IN the/DT chosen/VBN religion/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB compares/VBZ homosexuality/NN to/TO polygamy/NN ,/, </C>
<C>claiming/VBG that/IN polygamy/NN has/AUX been/AUX unofficially/RB accepted/VBN for/IN years/NNS ,/, </C>
<C>and/CC argues/VBZ that/IN maintaining/VBG the/DT current/JJ same-sex/JJ marriage/NN bans/NNS should/MD be/AUX considered/VBN discriminatory/JJ ./. </C>
</S>
</P>
</T>
