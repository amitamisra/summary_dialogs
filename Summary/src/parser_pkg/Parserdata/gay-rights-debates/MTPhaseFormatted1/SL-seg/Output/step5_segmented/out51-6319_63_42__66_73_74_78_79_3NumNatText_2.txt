<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN religious/JJ freedom/NN allows/VBZ for/IN people/NNS to/TO not/RB do/AUX something/NN ,/, such/JJ as/IN serve/VBP a/DT gay/JJ customer/NN ,/, </C>
<C>that/IN they/PRP find/VBP objective/JJ to/TO their/PRP$ religion/NN ./. </C>
</S>
<S>
<C>He/PRP gives/VBZ an/DT example/NN that/IN Jewish/JJ people/NNS should/MD not/RB be/AUX forced/VBN to/TO work/VB on/IN the/DT Sabbath/NNP or/CC eat/VB pork/NN ./. </C>
</S>
<S>
<C>Likewise/RB ,/, he/PRP thinks/VBZ Christians/NNS should/MD not/RB be/AUX required/VBN to/TO serve/VB gay/JJ people/NNS in/IN a/DT way/NN that/WDT violates/VBZ their/PRP$ religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ it/PRP should/MD be/AUX a/DT requirement/NN that/IN you/PRP serve/VBP all/DT customers/NNS and/CC gives/VBZ the/DT example/NN that/IN he/PRP could/MD not/RB refuse/VB to/TO serve/VB a/DT boy/NN scout/NN or/CC Christian/NNP that/IN he/PRP does/AUX not/RB agree/VB with/IN ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/RB to/TO say/VB that/IN taxpayers/NNS and/CC employers/NNS are/AUX the/DT ones/NNS providing/VBG the/DT paycheck/NN ,/, not/RB God/NNP ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ service/NN is/AUX a/DT requirement/NN to/TO express/VB the/DT freedom/NN people/NNS have/AUX ./. </C>
</S>
</P>
</T>
