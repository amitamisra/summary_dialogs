<T>
<P>
</P>
<P>
<S>
<C>S1/NNP says/VBZ we/PRP benefit/VBP from/IN families/NNS with/IN adopted/VBN children/NNS ,/, step/NN children/NNS ,/, or/CC families/NNS with/IN no/DT children/NNS at/IN all/DT ./. </C>
</S>
<S>
<C>Marriage/NN has/AUX nothing/NN to/TO do/AUX with/IN the/DT bible/NN and/CC everything/NN to/TO do/AUX with/IN the/DT slippery/JJ slope/NN logical/JJ fallacy/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ a/DT married/JJ couple/NN as/IN the/DT head/NN of/IN a/DT home/NN is/AUX considered/VBN a/DT nuclear/JJ family/NN </C>
<C>because/IN is/AUX already/RB that/IN the/DT question/NN of/IN legality/NN ./. </C>
</S>
<S>
<C>He/PRP questions/VBZ S1/NNP </C>
<C>if/IN he/PRP is/AUX trying/VBG to/TO say/VB </C>
<C>because/IN gay/JJ marriage/NN is/AUX less/RBR acceptable/JJ than/IN incestuous/JJ relations/NNS that/IN it/PRP is/AUX okay/JJ to/TO grant/VB gay/JJ marriage/NN but/CC not/RB sibling/JJ marriage/NN ./. </C>
</S>
<S>
<C>According/VBG to/TO S1/NNP ,/, incest/NN and/CC polygamy/NN does/AUX not/RB relate/VB specifically/RB to/TO gay/JJ legal/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/JJ questions/NNS to/TO what/WP exactly/RB does/AUX the/DT social/JJ acceptance/NN of/IN any/DT type/NN of/IN ``/`` taboo/JJ ''/'' relationship/NN have/AUX to/TO do/AUX with/IN their/PRP$ right/JJ to/TO be/AUX legally/RB married/JJ ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN gay/JJ marriage/NN will/MD change/VB the/DT definition/NN of/IN marriage/NN from/IN ``/`` a/DT legal/JJ union/NN between/IN man/NN and/CC woman/NN ''/'' to/TO ``/`` a/DT legal/JJ union/NN between/IN any/DT parties/NNS that/WDT love/VBP each/DT other/JJ ''/'' ./. </C>
</S>
<S>
<C>He/PRP poses/VBZ the/DT question/NN if/IN it/PRP is/AUX okay/JJ for/IN two/CD people/NNS to/TO tie/VB the/DT knot/NN then/RB how/WRB could/MD incest/NN be/AUX taboo/JJ ./. </C>
</S>
</P>
</T>
