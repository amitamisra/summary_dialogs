<s> <PARAGRAPH> </s>
<s>  Two subjects are discussing the issue of gay marriage rights. </s>
<s> S1 is anti-marriage redefinition while S2 is pro-gay marriage. </s>
<s> S1 feels marriage is classified by a heterosexual contract and is not legal or natural if it involves couples of the same sex. </s>
<s> S2 challenges the religious aspects of S1's belief because God is not involved in the ceremonies performed by a judge and not in a church. </s>
<s> He also offers information in regard to a tribe in the Congo which required its warriors to marry other warriors as they believed relations with women would soften them. </s>
<s> S1 advises values are always believed to be correct by the person or group who practices them. </s>
<s> S2 does not think morals should be forced. </s>
<s>  </s>
