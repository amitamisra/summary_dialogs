<T>
<P>
</P>
<P>
<S>
<C>S1/NNP presents/VBZ the/DT idea/NN that/IN gay/JJ marriage/NN has/AUX been/AUX pervasive/JJ in/IN many/JJ cultures/NNS ,/, including/VBG the/DT Romans/NNPS ,/, Greeks/NNPS ,/, Celts/NNPS ,/, and/CC American/NNP Indians/NNPS ./. </C>
</S>
<S>
<C>He/PRP also/RB mentions/VBZ the/DT existence/NN of/IN other/JJ alternative/JJ forms/NNS of/IN marriage/NN such/JJ as/IN polygamy/NN and/CC polyandry/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN gay/JJ marriage/NN is/AUX not/RB a/DT new/JJ idea/NN </C>
<C>and/CC that/IN the/DT idea/NN of/IN marriage/NN being/AUXG between/IN a/DT man/NN </C>
<C>and/CC a/DT woman/NN is/AUX a/DT recent/JJ development/NN ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN although/IN homosexual/JJ acts/NNS did/AUX occur/VB in/IN many/JJ past/JJ cultures/NNS ,/, </C>
<C>these/DT relationships/NNS were/AUX not/RB marriages/NNS ./. </C>
</S>
<S>
<C>He/PRP insists/VBZ that/IN marriages/NNS were/AUX almost/RB exclusively/RB between/IN people/NNS of/IN opposite/JJ genders/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN same-sex/JJ marriage/NN has/AUX never/RB been/AUX commonplace/JJ or/CC accepted/VBN ,/, despite/IN homosexual/JJ acts/NNS occurring/VBG at/IN times/NNS ,/, </C>
<C>and/CC that/IN the/DT original/JJ idea/NN of/IN marriage/NN is/AUX a/DT union/NN between/IN people/NNS of/IN opposite/JJ genders/NNS ./. </C>
</S>
</P>
</T>
