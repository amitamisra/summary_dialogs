(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ cites) (NP (DT a) (JJ key) (NN statement)) (PP (IN in) (NP (NP (DT the) (JJ same-sex) (NN marriage) (NN legislation)) (SBAR (WHNP (WDT that)) (S (VP (VBZ allows) (S (NP (NN marriage) (NNS officers)) (NP (DT the) (NN right) (S (RB not) (VP (TO to) (VP (VB perform) (NP (DT a) (JJ same-sex) (NN marriage)) (SBAR (IN if) (S (NP (PRP it)) (VP (NNS conflicts) (PP (IN with) (NP (PRP$ their) (NN conscience) (, ,) (NN religion) (, ,) (CC or) (NNS beliefs))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (DT a) (JJ good) (NN caveat)) (, ,) (S (VP (VBG claiming) (SBAR (IN that) (S (S (VP (VBG forcing) (S (NP (NNS officials)) (VP (TO to) (VP (VB perform) (NP (JJ same-sex) (NNS marriages))))))) (VP (MD would) (VP (AUX be) (S (VP (VBG infringing) (PP (IN upon) (NP (PRP$ their) (JJ religious) (NN freedom))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (JJ public) (NN official)) (VP (AUX had) (NP (NP (DT a) (JJ religious) (NN objection)) (PP (TO to) (NP (JJ heterosexual) (NNS marriages))))))) (, ,) (ADVP (RB then)) (NP (PRP he)) (VP (VP (MD would) (RB not) (VP (VB expect) (S (NP (DT that) (NN person)) (VP (TO to) (VP (VB perform) (NP (PRP$ his) (NN marriage))))))) (CC and) (VP (MD would) (VP (ADVP (RB instead)) (VB seek) (PRT (RP out)) (NP (NP (NN someone)) (SBAR (WHNP (WP who)) (S (VP (MD would))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ inappropriate) (SBAR (IN for) (S (NP (JJ public) (NNS officials)) (VP (TO to) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB deny) (NP (NP (NNS services)) (VP (VBN based) (PP (IN on) (NP (PRP$ their) (JJ personal) (JJ religious) (NNS beliefs)))))))))))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ unable) (S (VP (TO to) (VP (AUX do) (NP (PRP$ their) (NN job))))))))) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ suggests))) (, ,)) (NP (PRP they)) (VP (MD should) (RB not) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB hold) (NP (DT that) (NN position)))))))) (. .)))

