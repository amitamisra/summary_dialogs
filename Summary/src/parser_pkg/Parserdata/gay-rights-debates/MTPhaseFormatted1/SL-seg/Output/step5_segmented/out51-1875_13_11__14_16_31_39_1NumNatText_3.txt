<T>
<P>
</P>
<P>
<S>
<C>Two/CD subjects/NNS are/AUX discussing/VBG Virginia/NNP 's/POS unwillingness/NN to/TO allow/VB gay/JJ couples/NNS to/TO marry/VB ./. </C>
</S>
<S>
<C>S1/NNP is/AUX using/VBG the/DT basis/NN that/IN the/DT request/NN for/IN legalizing/VBG gay/JJ marriage/NN is/AUX mostly/RB based/VBN on/IN the/DT want/VBP and/CC need/NN for/IN healthcare/NN benefits/NNS one/CD 's/POS spouse/NN in/IN entitled/VBN to/TO using/VBG the/DT example/NN that/IN if/IN a/DT gentleman/NN and/CC his/PRP$ girlfriend/NN are/AUX not/RB married/JJ ,/, </C>
<C>the/DT girlfriend/NN would/MD not/RB be/AUX eligible/JJ for/IN healthcare/NN under/IN the/DT boyfriend/NN either/RB ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ this/DT is/AUX not/RB a/DT singular/JJ subject/NN and/CC is/AUX more/RBR based/VBN on/IN rights/NNS as/IN the/DT heterosexual/JJ couple/NN S1/NNP uses/VBZ </C>
<C>as/IN an/DT example/NN has/AUX the/DT right/NN to/TO get/VB married/VBN </C>
<C>if/IN they/PRP choose/VBP ,/, </C>
<C>while/IN a/DT gay/JJ couple/NN does/AUX not/RB have/AUX the/DT option/NN to/TO make/VB that/DT same/JJ choice/NN to/TO ./. </C>
</S>
<S>
<C>S1/NNP holds/VBZ hope/NN that/IN gay/JJ marriage/NN will/MD be/AUX banned/VBN everywhere/RB ./. </C>
</S>
</P>
</T>
