<s> <PARAGRAPH> </s>
<s>  This discussion is about the government roll in marriage, specifically pertaining to homosexual marriage. </s>
<s> S1 believes that the government should have no say in marriage. </s>
<s> S2 believes that government should have a say in marriage because it is more practical in today's world. </s>
<s> S1 believes that individual contracts by lawyers rather than the government being involved would be the fairest compromise. </s>
<s> S2 encountered problems with this view in their personal life when their partner died, and they were unable to sort out all of the legal issues involved in death, like inheritance, or social security rights. </s>
<s> S2 believes there will be privileges that come only in government recognized unions. </s>
<s> S1 comments best way to go about solving these problems is to take them one issue at a time rather then facing the problem as a whole. </s>
<s> S2 thinks that you need to look at situations in from different perspectives like trees in the woods. </s>
<s> Both S1 and S2 have previous experience with married life and single life. </s>
<s>  </s>
