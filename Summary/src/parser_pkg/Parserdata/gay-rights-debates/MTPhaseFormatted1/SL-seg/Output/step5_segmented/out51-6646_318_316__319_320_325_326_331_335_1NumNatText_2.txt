<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ </C>
<C>since/IN gay/JJ couples/NNS qualify/VBP for/IN the/DT same/JJ benefits/NNS as/IN married/JJ heterosexual/JJ couples/NNS there/EX is/AUX no/DT reason/NN to/TO allow/VB for/IN gay/JJ marriage/NN advising/VBG his/her/NN agreeing/VBG with/IN these/DT points/NNS does/AUX not/RB make/VB him/her/VB a/DT bigot/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ supporting/VBG arguments/NNS with/IN logic/NN eliminates/VBZ the/DT ability/NN to/TO label/VB one/NN as/IN a/DT bigot/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ not/RB redefining/VBG marriage/NN </C>
<C>to/TO allow/VB for/IN same/JJ sex/NN couples/NNS to/TO unite/VB leaves/NNS the/DT door/NN open/JJ for/IN gay/JJ couples/NNS to/TO not/RB be/AUX given/VBN the/DT same/JJ rights/NNS and/CC benefits/NNS as/IN hetero/NN couples/NNS ./. </C>
</S>
<S>
<C>He/she/NNP believes/VBZ </C>
<C>as/RB long/RB as/IN there/EX is/AUX a/DT separate/JJ set/NN of/IN standards/NNS there/EX can/MD be/AUX rule/NN changes/NNS to/TO prevent/VB their/PRP$ benefits/NNS ./. </C>
</S>
<S>
<C>S2/NNP concedes/VBZ that/IN S1/NNP is/AUX not/RB labeled/VBN as/IN a/DT bigot/NN </C>
<C>but/CC supports/VBZ those/DT who/WP are/AUX in/IN a/DT sense/NN bigots/NNS or/CC supporters/NNS ./. </C>
</S>
</P>
</T>
