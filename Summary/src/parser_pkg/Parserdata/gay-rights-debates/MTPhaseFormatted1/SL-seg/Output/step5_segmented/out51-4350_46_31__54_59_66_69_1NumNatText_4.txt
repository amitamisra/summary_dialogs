<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ that/IN we/PRP do/AUX not/RB just/RB benefit/VB from/IN what/WP is/AUX often/RB considered/VBN the/DT common/JJ ,/, nuclear/JJ family/NN ./. </C>
</S>
<S>
<C>We/PRP benefit/VBP from/IN families/NNS with/IN adopted/VBN children/NNS ,/, step/NN children/NNS ,/, families/NNS with/IN no/DT children/NNS at/IN all/DT ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/DT marriage/NN has/AUX nothing/NN to/TO do/AUX with/IN the/DT bible/JJ ./. </C>
</S>
<S>
<C>Then/RB what/WP may/MD lead/VB to/TO incest/NN and/CC polygamy/NN does/AUX not/RB relate/VB specifically/RB to/TO gay/JJ legal/JJ marriage/NN at/IN all/DT ./. </C>
</S>
<S>
<C>S1/NNP is/AUX trying/VBG to/TO show/VB that/IN people/NNS who/WP are/AUX against/IN gay/JJ marriage/NN try/VBP to/TO dissuade/VB others/NNS </C>
<C>by/IN attempting/VBG to/TO incite/VB fear/NN </C>
<C>by/IN spouting/VBG off/RP a/DT parade/NN of/IN horribles/NNS that/WDT have/AUX nothing/NN to/TO do/AUX with/IN the/DT issue/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP holds/VBZ that/IN love/NN applies/VBZ to/TO straight/JJ marriages/NNS as/RB well/RB as/IN gay/JJ marriages/NNS </C>
<C>and/CC that/IN love/NN is/AUX not/RB exclusive/JJ to/TO gay/JJ marriages/NNS ./. </C>
</S>
<S>
<C>It/PRP is/AUX fallacious/JJ to/TO believe/VB that/IN love/NN will/MD lead/VB to/TO a/DT slippery/JJ slope/NN ./. </C>
</S>
<S>
<C>Those/DT against/IN gay/JJ marriage/NN use/VBP logical/JJ fallacies/NNS ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB see/VB the/DT logic/NN in/IN bringing/VBG up/RP sibling/JJ marriages/NNS and/CC is/AUX curious/JJ to/TO know/VB how/WRB S1/NNP would/MD respond/VB to/TO such/JJ movements/NNS ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB that/IN what/WP people/NNS do/AUX in/IN their/PRP$ private/JJ lives/NNS is/AUX anyone/NN else/RB 's/POS business/NN ./. </C>
</S>
<S>
<C>What/WP may/MD lead/VB to/TO incestuous/JJ marriage/NN is/AUX if/IN siblings/NNS or/CC related/VBN people/NNS who/WP love/VBP each/DT other/JJ and/CC want/VBP to/TO live/VB together/RB under/IN the/DT legal/JJ union/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>What/WP may/MD lead/VB to/TO polygamous/JJ marriage/NN is/AUX 3/CD or/CC more/JJR people/NNS who/WP love/VBP each/DT other/JJ and/CC want/VBP to/TO live/VB together/RB under/IN the/DT legal/JJ union/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>Gay/JJ marriage/NN was/AUX considered/VBN just/RB as/RB much/JJ a/DT taboo/NN as/IN incest/NN or/CC polygamy/NN just/RB a/DT few/JJ years/NNS ago/RB ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB understand/VB why/WRB it/PRP is/AUX ok/JJ to/TO deny/VB incestuous/JJ relations/NNS the/DT right/NN to/TO marry/VB </C>
<C>on/IN the/DT grounds/NNS that/IN it/PRP is/AUX considered/VBN taboo/NN ,/, but/CC ok/JJ to/TO approve/VB gay/JJ marriage/NN on/IN the/DT same/JJ grounds/NNS ./. </C>
</S>
</P>
</T>
