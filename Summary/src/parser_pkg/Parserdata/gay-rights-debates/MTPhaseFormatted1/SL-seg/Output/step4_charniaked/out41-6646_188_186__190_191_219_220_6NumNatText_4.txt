(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNS S1)) (VP (VBP bring) (NP (NP (JJ sup) (JJ historical) (NN evidence)) (PP (IN in) (NP (NN support))) (SBAR (IN that) (S (NP (JJ homosexual) (NN marriage)) (VP (AUX was) (VP (VBN conducted) (PP (IN in) (NP (DT the) (NN past))) (, ,) (S (VP (VBG naming) (NP (DT the) (NNPS Romans) (, ,) (NNPS Celts) (, ,) (NNPS Greeks) (CC and) (JJ Native) (NNP American) (NNS tribes)))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (NP (JJ such) (NNS unions)) (VP (AUX were) (ADJP (JJ important) (PP (IN in) (NP (NP (DT the) (JJ Native) (NNP American) (NN tribe) (POS 's)) (NN culture)))) (SBAR (IN as) (S (NP (DT these) (NNS individuals)) (VP (AUX were) (VP (VBN seen) (PP (IN as) (NP (NNS healers) (CC and) (NNS prophets))))))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB also)) (VP (VBP point) (PRT (RP out)) (SBAR (IN that) (S (NP (NN polygamy)) (VP (VP (AUX has) (VP (AUX been) (ADVP (RB around)) (ADVP (RB historically) (RB as) (RB well)))) (, ,) (CC and) (VP (VBZ argues) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX has) (ADVP (RB never)) (VP (AUX been) (NP (NP (DT a) (JJ binary) (NN union)) (PP (IN between) (NP (CD one) (NN man) (CC and) (CD one) (NN woman))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ responds) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (DT the) (NN evidence)) (VP (VP (VBD provided) (SBAR (S (VP (AUX does) (RB not) (VP (VB classify) (NP (DT these) (NNS unions)) (PP (IN as) (NP (NN marriage)))))))) (, ,) (CC but) (VP (NP (NP (JJ special) (NN case) (NNS scenarios)) (SBAR (WHNP (WDT that)) (S (VP (AUX do) (RB not) (VP (VBP overlap) (PP (IN with) (NP (VBG existing) (NN marriage) (NNS laws)))))))) (PP (IN in) (NP (NN existence))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX has) (ADVP (RB always)) (VP (AUX been) (VP (VBN considered) (S (VP (TO to) (VP (AUX be) (PP (IN between) (NP (NP (CD one) (NN man)) (CC and) (NP (CD one) (NN woman)))))))))))) (CC and) (SBAR (IN that) (S (NP (JJ homosexual) (NNS liaisons)) (VP (AUX were) (NP (JJ special) (NNS cases))))))) (. .)))

