(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN group)) (VP (VBN called) (S (NP (NP (NNS Domionists)) (SBAR (WHNP (WDT that)) (S (VP (VBP care) (ADVP (RB only)) (PP (PP (IN about) (NP (NN power))) (CC and) (PP (RB not) (IN about) (NP (NNS people))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (PRP one)) (VP (MD can) (VP (VB learn) (PP (IN about) (NP (DT this) (NN group))) (PP (IN by) (S (VP (VBG looking) (PP (IN at) (NP (NP (NNP Jerry) (NNP Falwell)) (CC or) (NP (NNP Pat) (NNP Robertson)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX was) (ADJP (JJ unable) (S (VP (TO to) (VP (VB locate) (NP (NP (DT any) (NN information)) (PP (IN on) (NP (NP (DT the) (NN group)) (ADJP (JJ other) (PP (IN than) (NP (NP (NNS articles)) (PP (IN by) (NP (NNS liberals)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (JJ radical) (NNS leftists)) (VP (VBD created) (NP (DT the) (NN group)) (PP (IN as) (NP (NP (DT a) (NN way)) (PP (IN of) (S (VP (VBG promoting) (NP (NP (NN fear)) (PP (IN of) (NP (NNS Christians))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ asks) (PP (IN for) (NP (JJ objective) (NN evidence)))) (CC and) (VP (VBZ thinks) (SBAR (S (NP (NP (NP (NNP S1) (POS 's)) (NN lack)) (PP (IN of) (NP (NN evidence)))) (VP (VBZ proves) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NN none)))))))))) (. .)))

(S1 (S (ADVP (RB Also)) (, ,) (NP (PRP he)) (VP (VBZ thinks) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ corrupt) (S (VP (TO to) (VP (VP (VB post) (NP (DT an) (NN article))) (CC but) (VP (VB withhold) (NP (DT the) (NN source) (NN link))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (MD could) (VP (VB post) (NP (NP (DT an) (NN article)) (VP (VBG stating) (SBAR (IN that) (S (S (NP (NNPS Democrats)) (VP (AUX are) (ADVP (RB really)) (NP (NNP Satan) (NNPS Worshipers)))) (CC but) (S (NP (DT that)) (VP (MD would) (RB not) (VP (VB make) (S (NP (PRP it)) (ADJP (JJ true)))))) (, ,) (CC and) (S (NP (NP (DT any) (NNS assertions)) (VP (VBN made))) (VP (MD should) (VP (AUX be) (VP (VBN backed) (PRT (RP up)) (PP (IN with) (NP (NN evidence)))))))))))))))) (. .)))

