<T>
<P>
</P>
<P>
<S>
<C>S1/NNS claim/VBP that/IN he/PRP has/AUX been/AUX unjustly/RB called/VBN a/DT bigot/NN for/IN stating/VBG as/IN a/DT fact/NN that/IN AIDs/NNP is/AUX initially/RB spread/VBN in/IN the/DT US/NNP primarily/RB by/IN gays/NNS ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN research/NN to/TO combat/NN AIDs/NNS have/AUX consumed/VBN an/DT inordinate/JJ amount/NN of/IN resources/NNS that/WDT could/MD have/AUX been/AUX used/VBN to/TO curing/NN cancer/NN and/CC other/JJ diseases/NNS ./. </C>
</S>
<S>
<C>He/PRP asserts/VBZ that/IN the/DT majority/NN of/IN US/PRP population/NN is/AUX paying/VBG the/DT price/NN for/IN the/DT promiscuity/NN of/IN the/DT gay/JJ minority/NN ./. </C>
</S>
<S>
<C>S2/NNP asserts/VBZ that/IN gays/NNS are/AUX the/DT first/JJ victims/NNS of/IN the/DT AIDS/NNP epidemic/NN and/CC acknowledges/VBZ that/IN gays/NNS are/AUX the/DT first/JJ people/NNS to/TO spread/VB it/PRP ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN casting/VBG gays/NNS as/IN spreaders/NNS of/IN AIDs/NNP alone/RB drives/NNS hate-speech/JJ ./. </C>
</S>
<S>
<C>An/DT example/NN is/AUX Paul/NNP Cameron/NNP 's/POS statements/NNS that/WDT call/VBP for/IN the/DT extermination/NN of/IN gays/NNS ./. </C>
</S>
</P>
</T>
