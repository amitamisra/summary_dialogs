<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG about/IN redefining/VBG the/DT meaning/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ that/IN it/PRP is/AUX unnecessary/JJ to/TO redefine/VB marriage/NN </C>
<C>to/TO include/VB gays/NNS ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN all/DT of/IN the/DT rights/NNS and/CC benefits/NNS that/IN married/JJ couples/NNS have/AUX are/AUX already/RB granted/VBN to/TO gays/NNS without/IN the/DT need/NN for/IN redefining/VBG ./. </C>
</S>
<S>
<C>Redefining/VBG only/RB serves/VBZ to/TO hurt/VB the/DT many/JJ people/NNS who/WP see/VBP marriage/NN as/IN a/DT sacred/JJ bond/NN between/IN man/NN and/CC woman/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ that/IN it/PRP is/AUX necessary/JJ to/TO redefine/VB marriage/NN ./. </C>
</S>
<S>
<C>If/IN not/RB ,/, the/DT rights/NNS and/CC benefits/NNS granted/VBN to/TO gays/NNS will/MD not/RB be/AUX guaranteed/VBN ./. </C>
</S>
<S>
<C>Also/RB ,/, he/PRP believes/VBZ that/IN offending/VBG someone/NN 's/POS sensibility/NN is/AUX not/RB real/JJ ,/, measurable/JJ damage/NN ,/, </C>
<C>so/RB there/RB is/AUX not/RB reason/NN not/RB to/TO redefine/VB marriage/NN ,/, </C>
<C>not/RB doing/VBG </C>
<C>so/IN only/JJ appeases/NNS faith/NN justified/VBN hate/VBP ./. </C>
</S>
</P>
</T>
