<PARAGRAPH> Two subjects are discussing liberalism and its similarities or lack thereof to the Jeffersonian belief system.
S2 believes they are nothing alike while S1 believes they have many similarities.
S2 advises Jefferson believed liberty would be threatened by a centralized government.
He also advises liberalism is not based on the same theory.
S1 believes the opposite stating the opinion that the group believes in fairness for all, except the gay community.
S2 advises he supports equal opportunities for all people despite their sexual preferences; however, he does not support the redefinition of marriage to allow for gay unions.
S1 feels this is a double standard because it is basically saying equal rights for all but gays still cannot marry.
