<T>
<P>
</P>
<P>
<S>
<C>S1/NNP :/: Washington/NNP DC/NNP '/POS s/NNP city/NN council/NN recently/RB legalized/VBD gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>People/NNS 's/POS support/NN of/IN gay/JJ marriage/NN has/AUX been/AUX growing/VBG steadily/RB in/IN the/DT past/JJ ten/CD years/NNS ,/, </C>
<C>and/CC will/MD soon/RB be/AUX the/DT majority/NN opinion/NN ./. </C>
</S>
<S>
<C>Five/CD states/NNS have/AUX already/RB legalized/VBN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>Those/DT opposed/VBN to/TO gay/JJ marriage/NN may/MD view/VB gays/NNS as/IN promiscuous/JJ and/CC sexual/JJ deviants/NNS ,/, </C>
<C>but/CC most/JJS gay/JJ men/NNS are/AUX assuredly/RB quite/RB normal/JJ ./. </C>
</S>
<S>
<C>Civil/JJ unions/NNS will/MD not/RB be/AUX accepted/VBN in/IN place/NN of/IN marriage/NN ,/, </C>
<C>as/IN they/PRP do/AUX not/RB allow/VB the/DT same/JJ legal/JJ rights/NNS as/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/NN :/: Councils/NNS and/CC legislatures/NNS may/MD vote/VB in/IN favor/NN of/IN gay/JJ marriage/NN ,/, </C>
<C>but/CC it/PRP has/AUX yet/RB to/TO be/AUX the/DT majority/NN vote/NN of/IN the/DT people/NNS ./. </C>
</S>
<S>
<C>Gay/JJ marriage/NN should/MD be/AUX played/VBN in/IN a/DT positive/JJ light/NN by/IN its/PRP$ supporters/NNS ,/, </C>
<C>because/IN the/DT current/JJ public/JJ perception/NN of/IN gay/JJ men/NNS is/AUX that/IN they/PRP are/AUX promiscuous/JJ and/CC lead/VBP a/DT ``/`` gay/JJ lifestyle/NN ''/'' that/WDT is/AUX offensive/JJ ./. </C>
</S>
<S>
<C>Anti-gay/JJ marriage/NN supporters/NNS will/MD support/VB civil/JJ unions/NNS in/IN lieu/NN of/IN gay/JJ marriage/NN ,/, </C>
<C>and/CC homosexuals/NNS should/MD be/AUX satisfied/VBN with/IN that/DT ./. </C>
</S>
</P>
</T>
