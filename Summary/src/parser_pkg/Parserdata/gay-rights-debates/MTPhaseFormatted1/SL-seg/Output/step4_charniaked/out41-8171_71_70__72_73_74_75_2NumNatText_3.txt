(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (CD one)) (SBAR (WHNP (WP who)) (S (VP (VBZ supports) (NP (NP (JJ gay) (NN right)) (CC but) (NP (RB not) (JJ gay) (NN marriage))))))) (VP (VBZ brings) (NP (DT an) (JJ inconsistent) (NN problem)))))) (. .)))

(S1 (S (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (VP (VBZ says) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VBZ sees) (NP (DT any) (NN inconsistency))) (, ,) (CC and) (VP (VBZ compares) (NP (PRP it)) (PP (TO to) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (NN abortion))))))))) (CC and) (SBAR (WHADVP (WRB how)) (S (NP (PRP one)) (VP (MD can) (VP (AUX be) (NP (NP (QP (IN against) (PDT such)) (NN issue)) (PP (IN for) (NP (NN society))))))))))) (CC but) (S (PP (IN for) (NP (NNS ones) (NN position))) (NP (PRP it)) (VP (MD may) (VP (AUX be) (ADJP (JJ possible))))) (. .)))

(S1 (S (-LRB- -LRB-) (NP (NNP S1)) (-RRB- -RRB-) (VP (VBZ tells) (PRN (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-)) (SBAR (IN that) (S (NP (PRP it)) (VP (VBZ seems) (SBAR (IN like) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB understand) (NP (NP (DT the) (NN concept)) (PP (IN of) (NP (NN inconsistency)))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (ADVP (RB also)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ hopes) (SBAR (IN that) (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (VP (AUX did) (RB not) (VP (VB think) (PP (IN of) (NP (NP (JJ gay) (NN marriage)) (SBAR (S (VP (TO to) (VP (AUX be) (NP (DT a) (NN right)))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP keep) (S (VP (VBG arguing) (PRT (RP about)) (NP (NNS positions)) (UCP (PP (IN in) (NP (DT the) (NN matter))) (CC and) (SBAR (WHNP (WP who)) (S (VP (AUX is) (ADJP (JJ inconsistent))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (S (NP (JJ many) (NNS people)) (ADVP (RB often)) (NP (NNS times)) (VP (VB say) (NP (NP (NNS things)) (PP (JJ such) (IN as) (`` ``) (S (NP (PRP I)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (DT a) (NN right)))))))) ('' ''))))) (CC but) (S (NP (PRP they)) (ADVP (RB also)) (VP (VBP believe) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD should) (VP (AUX have) (NP (JJ equal) (NNS rights))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (NP (NNP Biden) (POS 's)) (NN opinion)) (VP (AUX is) (ADJP (JJ consistent)))) (CC and) (S (NP (NNP Biden)) (VP (AUX is) (RB not) (VP (VBG lying) (PP (IN about) (SBAR (WHNP (WP what)) (S (NP (PRP he)) (VP (VBZ believes))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP he)) (VP (VBP know) (SBAR (IN that) (S (S (NP (EX there)) (VP (AUX is) (ADJP (JJ inconsistency)))) (CC but) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB care)))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS people)) (VP (VP (VB weight) (NP (NP (DT the) (ADJP (CD ay) (NN marriage)) (NN opinion) (NN issue)) (PP (IN against) (NP (NP (NN politician) (POS 's)) (NNS weeks)))) (PP (IN before) (NP (NNS elections)))) (CC and) (VP (VB balance) (NP (DT the) (VBG responding) (NN approach))))))) (. .)))

