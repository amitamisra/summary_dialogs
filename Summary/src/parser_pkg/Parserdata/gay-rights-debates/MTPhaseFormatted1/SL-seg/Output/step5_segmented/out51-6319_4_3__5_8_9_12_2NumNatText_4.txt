<T>
<P>
</P>
<P>
<S>
<C>S1/NNP said/VBD that/IN in/IN South/NNP Africa/NNP it/PRP seems/VBZ as/IN if/IN marriage/NN freedom/NN does/AUX not/RB trump/NNP religious/JJ freedom/NN as/IN public/JJ officials/NNS do/AUX not/RB have/AUX to/TO perform/VB a/DT gay/JJ marriage/NN ceremony/NN </C>
<C>if/IN they/PRP believe/VBP that/IN doing/VBG so/RB would/MD be/AUX against/IN their/PRP$ values/NNS ./. </C>
</S>
<S>
<C>He/PRP said/VBD that/IN if/IN a/DT public/JJ official/NN did/AUX not/RB want/VB to/TO marry/VB him/PRP in/IN a/DT heterosexual/JJ marriage/NN </C>
<C>he/PRP would/MD want/VB to/TO go/VB elsewhere/RB rather/RB than/IN force/VB him/PRP to/TO do/AUX it/PRP ./. </C>
</S>
<S>
<C>S2/NNP said/VBD that/IN public/JJ officials/NNS should/MD serve/VB the/DT public/NN equally/RB under/IN the/DT law/NN </C>
<C>as/IN that/DT is/AUX what/WP they/PRP are/AUX supposed/VBN to/TO do/AUX ./. </C>
</S>
<S>
<C>He/PRP said/VBD that/IN they/PRP should/MD have/AUX to/TO look/VB for/IN another/DT job/NN </C>
<C>if/IN they/PRP do/AUX not/RB want/VB to/TO do/AUX their/PRP$ jobs/NNS ./. </C>
</S>
<S>
<C>He/PRP expressed/VBD his/PRP$ gladness/NN at/IN being/AUXG in/IN Canada/NNP where/WRB there/EX are/AUX more/JJR rights/NNS ./. </C>
</S>
</P>
</T>
