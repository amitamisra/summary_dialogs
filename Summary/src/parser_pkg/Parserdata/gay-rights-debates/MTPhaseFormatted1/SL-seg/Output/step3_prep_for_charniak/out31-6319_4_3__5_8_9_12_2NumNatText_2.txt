<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing the situation in which a marriage officiant may be requested to perform a gay marriage but have personal religious beliefs against homosexuality or gay marriage. </s>
<s> S1 defends and respects the personal religious beliefs and notes that the provision they are discussing ( which is either a law or a proposed law) provides the marriage official with the ability to opt out of performing the ceremony if it violates his or her religious beliefs. </s>
<s> S2 finds that offensive, reasoning that if the person is licensed by the state to perform marriage ceremonies that person should do so for all marriages, not just heterosexual marriages. </s>
<s> S1 asserts that South Africa takes his view, while S2 claims Canada sides with him. </s>
<s>  </s>
