<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG the/DT situation/NN in/IN which/WDT a/DT marriage/NN officiant/NN may/MD be/AUX requested/VBN to/TO perform/VB a/DT gay/JJ marriage/NN </C>
<C>but/CC have/AUX personal/JJ religious/JJ beliefs/NNS against/IN homosexuality/NN or/CC gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP defends/VBZ and/CC respects/VBZ the/DT personal/JJ religious/JJ beliefs/NNS and/CC notes/NNS that/IN the/DT provision/NN they/PRP are/AUX discussing/VBG 
<M>-LRB-/-LRB- which/WDT is/AUX either/RB a/DT law/NN or/CC a/DT proposed/VBN law/NN -RRB-/-RRB- </M>
provides/VBZ the/DT marriage/NN official/NN with/IN the/DT ability/NN to/TO opt/VB out/RP of/IN performing/VBG the/DT ceremony/NN </C>
<C>if/IN it/PRP violates/VBZ his/PRP$ or/CC her/PRP$ religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>S2/NNP finds/VBZ that/DT offensive/JJ ,/, reasoning/NN that/IN if/IN the/DT person/NN is/AUX licensed/VBN by/IN the/DT state/NN to/TO perform/VB marriage/NN ceremonies/NNS that/IN person/NN should/MD do/AUX so/RB for/IN all/DT marriages/NNS ,/, not/RB just/RB heterosexual/JJ marriages/NNS ./. </C>
</S>
<S>
<C>S1/NNP asserts/VBZ that/IN South/NNP Africa/NNP takes/VBZ his/PRP$ view/NN ,/, </C>
<C>while/IN S2/NNP claims/VBZ Canada/NNP sides/NNS with/IN him/PRP ./. </C>
</S>
</P>
</T>
