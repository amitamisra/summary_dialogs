<PARAGRAPH> S1: Washington DC' s city council recently legalized gay marriage.
People's support of gay marriage has been growing steadily in the past ten years, and will soon be the majority opinion.
Five states have already legalized gay marriage.
Those opposed to gay marriage may view gays as promiscuous and sexual deviants, but most gay men are assuredly quite normal.
Civil unions will not be accepted in place of marriage, as they do not allow the same legal rights as marriage.
S2: Councils and legislatures may vote in favor of gay marriage, but it has yet to be the majority vote of the people.
Gay marriage should be played in a positive light by its supporters, because the current public perception of gay men is that they are promiscuous and lead a "gay lifestyle" that is offensive.
Anti-gay marriage supporters will support civil unions in lieu of gay marriage, and homosexuals should be satisfied with that.
