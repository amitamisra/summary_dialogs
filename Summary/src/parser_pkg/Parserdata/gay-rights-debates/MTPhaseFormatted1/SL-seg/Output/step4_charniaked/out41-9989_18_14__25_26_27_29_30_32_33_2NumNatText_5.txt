(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (VP (VBD served) (NP (NNP Uncle) (NNP Sam))) (CC and) (VP (VBD fought) (PP (IN for) (NP (NP (PRP$ his) (NN freedom)) (PP (IN of) (NP (NN speech))))) (PP (ADVP (RB right)) (IN unlike) (NP (JJ other) (NNS people)))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (AUX is) (VP (VBN amused) (PP (IN at) (NP (NP (NNP S1) (POS 's)) (NN statement)))))) (CC yet) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (S (NP (NNS gays)) (VP (MD should) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB serve) (PP (IN in) (NP (NP (DT the) (NN military)) (PP (IN of) (NP (NP (JJ have) (NN freedom)) (PP (IN of) (NP (NN speech)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ says) (SBAR (S (NP (NNP S1)) (VP (VP (AUX did) (RB not) (ADVP (RB actually)) (VP (VB fight) (PP (IN for) (NP (NP (PRP$ his) (NN freedom)) (PP (IN of) (NP (NN speech))))))) (CC but) (VP (AUX was) (VP (VBN guaranteed) (NP (PRP it)) (PP (IN by) (NP (NP (DT the) (NN constitution)) (SBAR (WHNP (WDT which)) (S (VP (VBZ applies) (PP (TO to) (NP (NP (DT all) (NNPS Americans)) (PP (VBG including) (NP (NNS gays)))))))))))))))) (. .)))

(S1 (S (NP (JJ S2) (NNS questions)) (VP (VBP S1) (PP (TO to) (SBAR (WHADVP (WRB why)) (S (NP (EX there)) (VP (MD should) (VP (AUX be) (NP (NP (NNS consequences)) (PP (IN on) (NP (NN earth)))) (SBAR (IN because) (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ wrong)) (PP (VBG according) (PP (TO to) (NP (PRP$ his) (JJ religious) (NN book)))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ tells) (NP (NNP S1)) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (NP (NP (DT no) (NN evidence)) (PP (IN of) (SBAR (WHNP (WP what)) (S (NP (PRP he)) (VP (VBZ claims) (CC but) (VBZ asks) (NP (NNP S2)) (SBAR (WHADVP (WRB why)) (S (NP (DT all) (NNPS Americans)) (VP (MD should) (VP (VB believe) (NP (NP (NNP S1) (POS 's)) (NN book))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (S (S (NP (NP (DT the) (NN power)) (PP (IN of) (NP (DT the) (NN government)))) (VP (VBZ comes) (PP (IN from) (NP (DT the) (NNS people))))) (CC and) (S (NP (PRP$ his) (NN fight)) (VP (AUX is) (ADVP (RB again)) (NP (NP (NNS bigots)) (SBAR (WHNP (WP who)) (S (VP (VBZ feels) (SBAR (IN that) (S (NP (PRP they)) (VP (MD can) (VP (VB use) (NP (PRP$ their) (JJ personal) (NNS beliefs)) (S (VP (TO to) (VP (VB dictate) (SBAR (WHNP (WP what) (JJ other)) (S (VP (VP (MD may)) (CC or) (VP (MD may) (RB not) (VP (AUX do)))))))))))))))))))))) (. .)))

(S1 (S (NP (JJ S1) (NNS questions)) (VP (VBP S2) (PP (IN as) (PP (TO to) (SBAR (WHNP (WP who)) (S (VP (AUX is) (S (NP (PRP he)) (VP (TO to) (VP (VB decide) (SBAR (WHNP (WP what)) (S (NP (JJ religious-minded) (NNS people)) (VP (MD can) (VP (CC and) (VP (VB cannot) (VP (AUX do))) (CC and) (VP (VBZ says) (SBAR (S (NP (NP (NNP S2) (POS 's)) (JJ personal) (NN bigotry)) (VP (AUX is) (VP (VBG getting) (PP (TO to) (NP (PRP him))))))))))))))))))))) (. .)))

