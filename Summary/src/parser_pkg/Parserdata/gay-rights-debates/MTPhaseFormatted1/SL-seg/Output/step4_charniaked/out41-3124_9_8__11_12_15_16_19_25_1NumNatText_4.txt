(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB want) (S (NP (NNS people)) (VP (TO to) (VP (VB compare) (NP (NP (DT a) (NN race)) (PP (IN of) (NP (NP (NP (NNS people) (POS 's)) (NN fight)) (PP (IN for) (NP (NP (JJ equal) (NNS rights)) (PP (TO to) (NP (NP (DT a) (NN class)) (PP (IN of) (NP (NNS people))) (VP (VBN determined) (PP (IN by) (NP (PRP$ their) (JJ sexual) (NN orientation)))))))))))))))) (. .)))

(S1 (S (NP (PRP She)) (ADVP (RB then)) (VP (VBZ asks) (PP (IN for) (NP (NP (DT a) (ADJP (VBP cite)) (NN link)) (SBAR (WHNP (WDT that)) (S (VP (VBZ says) (SBAR (S (NP (NNP African) (NNPS Americans)) (VP (AUX are) (PP (IN for) (NP (JJ gay) (NN marriage)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNP S2)) (VP (MD can) (RB not) (VP (VB come) (PRT (RP up)) (PP (IN with) (NP (DT a) (NN reference) (NN link)))))))) (. .)))

(S1 (S (ADVP (RB Then)) (, ,) (NP (PRP she)) (VP (VP (VBZ turns) (NP (PRP$ her) (NN question)) (PP (TO to) (NP (NNP African) (NNPS Americans)))) (CC and) (VP (VBZ asks) (SBAR (WHADVP (WRB how)) (S (NP (NNP Martin) (NNP Luther) (NNP King) (NNP Junior)) (VP (MD would) (VP (AUX have) (VP (VBD felt) (PP (IN about) (NP (NP (DT this) (NN issue-)) (SBAR (S (NP (PRP he)) (VP (AUX has) (ADJP (ADJP (JJR more) (NN credibility)) (PP (IN than) (NP (NP (DT all)) (PP (IN of) (NP (PRP$ her) (NNS examples)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NNP Coretta) (NNP Scott) (NNP King)) (CC and) (NP (NNP Jillian) (NNP Bond))) (VP (AUX are) (PP (IN against) (NP (JJ gay) (NNS rights))))))) (. .)))

(S1 (S (NP (PRP She)) (VP (VBZ lets) (S (NP (NNP S1)) (VP (VB know) (SBAR (IN that) (S (NP (VB cite) (NNS links)) (VP (AUX have) (VP (AUX been) (VP (VBN shared) (NP (JJ many) (NNS times)))))))))) (. .)))

(S1 (S (NP (PRP She)) (ADVP (RB then)) (VP (VP (VBZ shares) (NP (NP (DT a) (NN couple)) (PP (IN of) (NP (NP (NNS links)) (PP (IN for) (NP (NNP S1)))))) (S (VP (TO to) (VP (VB see))))) (CC and) (RB then) (VP (VBZ replies) (SBAR (IN that) (S (NP (DT no) (NN one)) (VP (MD will) (ADVP (RB ever)) (VP (VB know) (PP (IN for) (ADJP (JJ sure))))))))) (. .)))

