<s> <PARAGRAPH> </s>
<s>  S1 believes that a big 'bunch' of people cherish the notion that marriage is a sacred bond of union between a man and a woman. </s>
<s> Redefining marriage to include same-sex couples upsets the sensitivities of these people. </s>
<s> This is unnecessary because it is possible for gay couples to obtain rights similar to married couples without having to redefine marriage. </s>
<s> S2 asserts that redefining marriage to include same-sex couples does not harm anyone. </s>
<s> He said that offending the sensibilities of people does not constitute harm. </s>
<s> He claims that evidence show that the majority of people do not want to grant same-sex couples rights similar to marriage under a separate label. </s>
<s> Hence gays have to resort to redefining marriage to secure legal rights under the marriage arrangement. </s>
<s>  </s>
