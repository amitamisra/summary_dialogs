<T>
<P>
</P>
<P>
<S>
<C>S1/NNP states/VBZ that/IN the/DT majority/NN of/IN the/DT supreme/JJ court/NN cases/NNS which/WDT have/AUX addressed/VBN a/DT ban/NN on/IN same-sex/JJ marriage/NN have/AUX deemed/VBN the/DT laws/NNS unconstitutional/JJ ./. </C>
</S>
<S>
<C>He/PRP recognized/VBD the/DT difficulty/NN imposed/VBN by/IN the/DT fact/NN that/IN marriage/NN is/AUX both/PDT a/DT legal/JJ contract/NN and/CC a/DT religious/JJ ceremony/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN although/IN the/DT cited/VBN cases/NNS involved/VBD state/NN constitutions/NNS ,/, </C>
<C>the/DT ruling/NN were/AUX based/VBN on/IN provisions/NNS very/RB similar/JJ to/TO the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN supreme/JJ court/NN decisions/NNS regarding/VBG violations/NNS of/IN state/NN constitutions/NNS are/AUX irrelevant/JJ to/TO the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN the/DT Massachusetts/NNP ruling/NN was/AUX based/VBN on/IN a/DT provision/NN prohibiting/VBG discrimination/NN based/VBN on/IN sexual/JJ preference/NN which/WDT is/AUX not/RB found/VBN in/IN the/DT federal/JJ constitution/NN ,/, </C>
<C>but/CC later/RB admits/VBZ that/IN he/PRP was/AUX in/IN error/NN </C>
<C>and/CC that/IN it/PRP was/AUX based/VBN on/IN prohibiting/VBG discrimination/NN based/VBN on/IN sex/NN ./. </C>
</S>
</P>
</T>
