<T>
<P>
</P>
<P>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- states/VBZ that/IN he/PRP has/AUX served/VBN Uncle/NNP Sam/NNP and/CC fought/VBN for/IN his/PRP$ freedom/NN rights/NNS ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- responds/VBZ that/IN -LRB-/-LRB- S1/NNP -RRB-/-RRB- does/AUX not/RB thinks/VBZ that/IN gay/JJ people/NNS should/MD have/AUX the/DT opportunity/NN to/TO serve/VB in/IN the/DT military/NN or/CC have/AUX freedom/NN of/IN speech/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- also/RB stated/VBD that/IN -LRB-/-LRB- S1/NNP -RRB-/-RRB- did/AUX not/RB fight/VB for/IN his/PRP$ freedom/NN of/IN speech/NN </C>
<C>because/IN that/DT right/NN is/AUX guaranteed/VBN in/IN the/DT constitution/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- accuses/VBZ -LRB-/-LRB- S2/NNP -RRB-/-RRB- about/IN lying/VBG about/IN what/WP other/JJ have/AUX said/VBN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN gays/NNS can/MD serve/VB </C>
<C>but/CC they/PRP need/AUX to/TO stay/VB in/IN the/DT closet/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- challenges/VBZ -LRB-/-LRB- S1/NNP -RRB-/-RRB- </C>
<C>to/TO show/VB him/PRP where/WRB he/PRP lied/VBD and/CC states/VBZ that/IN gays/NNS can/MD not/RB say/VB whatever/WDT they/PRP want/VBP to/TO </C>
<C>because/IN they/PRP 'll/MD get/VB kicked/VBN out/RP and/CC says/VBZ that/DT is/AUX not/RB freedom/NN of/IN speech/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- charges/VBZ -LRB-/-LRB- S2/NNP -RRB-/-RRB- of/IN being/AUXG in/IN denial/NN </C>
<C>but/CC -LRB-/-LRB- S2/NNP -RRB-/-RRB- defends/VBZ himself/PRP </C>
<C>by/IN saying/VBG that/IN he/PRP never/RB said/VBD he/PRP was/AUX in/IN denial/NN </C>
<C>and/CC that/IN the/DT religious/JJ book/NN is/AUX only/RB a/DT philosophical/JJ fiction/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN his/PRP$ fight/NN is/AUX not/RB directly/RB or/CC only/RB with/IN -LRB-/-LRB- S1/NNP -RRB-/-RRB- but/CC with/IN others/NNS who/WP think/VBP that/IN they/PRP can/MD tell/VB people/NNS how/WRB they/PRP need/AUX to/TO live/VB their/PRP$ lives/NNS based/VBN on/IN religious/JJ beliefs/NNS ./. </C>
</S>
</P>
</T>
