<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG the/DT topic/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ the/DT term/NN husband/NN is/AUX not/RB recognizable/JJ in/IN a/DT same/JJ sex/NN relationship/NN nor/CC is/AUX the/DT term/NN wife/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ the/DT only/JJ reason/NN these/DT terms/NNS are/AUX not/RB recognized/VBN is/AUX because/IN the/DT couple/NN is/AUX gay/JJ </C>
<C>and/CC gay/JJ marriage/NN is/AUX not/RB yet/RB recognized/VBN legally/RB ./. </C>
</S>
<S>
<C>S1/NNP holds/VBZ that/IN the/DT marriage/NN is/AUX viewed/VBN legally/RB as/IN one/CD man/NN and/CC one/CD woman/NN ,/, not/RB same/JJ sex/NN ;/: </C>
<C>comparing/VBG the/DT theory/NN that/IN a/DT cat/NN is/AUX not/RB a/DT dog/NN </C>
<C>simply/RB because/IN one/PRP chooses/VBZ to/TO call/VB it/PRP so/RB ./. </C>
</S>
<S>
<C>S1/NNP also/RB cites/VBZ that/IN according/VBG to/TO the/DT CBO/NNP ,/, 600,000/CD gay/JJ couples/NNS would/MD want/VB to/TO wed/VBN </C>
<C>if/IN legislation/NN were/AUX passed/VBN to/TO allow/VB it/PRP ./. </C>
</S>
<S>
<C>S2/NNP wants/VBZ logical/JJ reasons/NNS for/IN now/RB allowing/VBG gays/NNS to/TO marry/VB ./. </C>
</S>
</P>
</T>
