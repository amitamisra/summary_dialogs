<T>
<P>
</P>
<P>
<S>
<C>Two/CD subjects/NNS are/AUX discussing/VBG a/DT law/NN banning/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ the/DT law/NN is/AUX unjust/JJ </C>
<C>as/IN it/PRP segregates/VBZ the/DT gay/JJ community/NN from/IN that/DT of/IN the/DT heterosexual/JJ community/NN ./. </C>
</S>
<S>
<C>S1/NNP supports/VBZ the/DT nation/NN 's/POS ability/NN to/TO vote/VB on/IN such/JJ issues/NNS and/CC supports/VBZ the/DT ``/`` majority/NN rules/NNS ''/'' philosophy/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ the/DT courts/NNS should/MD take/VB individuals/NNS '/POS into/IN consideration/NN and/CC rule/VB on/IN each/DT instead/RB of/IN as/IN a/DT whole/NN ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ this/DT idea/NN to/TO be/AUX incorrect/JJ citing/VBG the/DT fact/NN that/IN those/DT who/WP believe/VBP abortion/NN should/MD be/AUX illegal/JJ are/AUX technically/RB the/DT minority/NN </C>
<C>and/CC that/IN a/DT court/NN would/MD not/RB rule/VB on/IN that/DT issue/NN individually/RB ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ S2/NNP and/CC others/NNS like/IN him/her/NN support/VBP society/NN 's/POS oppression/NN of/IN certain/JJ groups/NNS ./. </C>
</S>
<S>
<C>S1/NNP disagrees/VBZ advising/VBG he/she/NN wants/VBZ only/RB a/DT society/NN free/JJ to/TO vote/VB on/IN laws/NNS ./. </C>
</S>
</P>
</T>
