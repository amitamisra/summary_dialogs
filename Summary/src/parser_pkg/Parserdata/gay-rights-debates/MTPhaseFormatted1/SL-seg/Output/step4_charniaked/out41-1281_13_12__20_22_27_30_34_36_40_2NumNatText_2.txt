(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ views) (NP (DT the) (NN marriage) (NN arrangement)) (PP (IN as) (S (NP (NN society)) (VP (VBG recognizing) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (DT a) (JJ natural) (JJ heterosexual) (NN contract))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ explains) (SBAR (IN that) (S (NP (NP (JJ natural) (NNS means)) (PP (IN between) (NP (NP (DT a) (NN man)) (CC and) (NP (DT a) (NN woman)))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ accepts) (SBAR (IN that) (S (NP (NN everyone)) (VP (AUX has) (NP (PRP$ their) (JJ own) (NNS values))))))) (CC but) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ unreasonable) (S (VP (TO to) (VP (VB expect) (SBAR (IN that) (S (NP (DT all) (NNS values)) (VP (AUX are) (ADJP (RB socially) (JJ acceptable))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (S (VP (VBG legalizing) (NP (JJ gay) (NN marriage)))) (VP (AUX is) (NP (NP (DT a) (NN form)) (PP (IN of) (S (VP (VBG forcing) (S (NP (NN society)) (VP (TO to) (VP (VB accept) (NP (JJ gay) (NN behavior))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ questions) (NP (NP (DT the) (NN validity)) (PP (IN of) (S (VP (VBG using) (NP (DT the) (NN term) (NN natural)) (S (VP (TO to) (VP (VB define) (NP (NN marriage)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ uses) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NP (JJ same-sex) (NN marriage)) (PP (IN in) (NP (DT a) (NNP Congo) (NN tribe)))))) (S (VP (TO to) (VP (VB show) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX is) (VP (VBN defined) (PP (IN by) (NP (NP (DT the) (NNS needs)) (PP (IN of) (NP (DT the) (JJ social) (NN group))))))))))))) (. .)))

(S1 (S (S (VP (VBG Banning) (NP (JJ gay) (NN marriage)))) (VP (AUX is) (VP (VBG forcing) (NP (NP (CD one) (NN set)) (PP (IN of) (NP (VBZ believes)))) (PP (IN onto) (NP (NNS others))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (VP (VBG legalizing) (NP (JJ gay) (NN marriage)))) (VP (AUX does) (RB not) (VP (VB force) (NP (JJ gay) (NNS values)) (PP (IN onto) (NP (NN anyone)))))))) (. .)))

