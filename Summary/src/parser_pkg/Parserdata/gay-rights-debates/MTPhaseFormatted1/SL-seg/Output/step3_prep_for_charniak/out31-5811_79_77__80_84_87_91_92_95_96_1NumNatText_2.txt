<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage. </s>
<s> S1 believes that the anti-gay agenda is based on prejudice, and that simply calling something moral or religious does not mean it can not be prejudice at the same time. </s>
<s> He contends that people who justify the anti-gay agenda claim a moral or religious stance without having one. </s>
<s> He also states that he has never seen someone justify an anti-gay agenda based solely on morals or religion. </s>
<s> S2 retorts by stating that when people claim religion in doing prejudice they are actually abandoning their morals, and prejudice is not what's behind people's opposition to gays being in romantic relationships. </s>
<s> He states that the simple reason is because it's Biblical; the Bible says it should not be done so it should not be done. </s>
<s> S2 then points out that if that is someone's belief then that's fine, but banning someone else from doing it due to that belief is imposing their views on others, which is wrong. </s>
<s>  </s>
