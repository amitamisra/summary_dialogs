<s> <PARAGRAPH> </s>
<s>  S1 Questions the role of government in marriage, maintaining that it should have a very limited one. </s>
<s> S1 says that governments want to be involved in our lives as much as they can be, and that getting the government out of marriage getting the government out of marriage would be a good idea even if people are supporting this change for the wrong reasons. </s>
<s> It is not necessary to have government in marriage and citizens should be vigilant in making sure that the government does not intrude where it is not needed. </s>
<s> S1 believes that single people have the biggest stake in this issue. </s>
<s> S2 criticizes the new push for government getting out of marriage. </s>
<s> S2 talks about marriage being an ancient tradition, based on the pair bonding that is natural for humans, and that is one of the foundations for social stability. </s>
<s> It is thought that it would be natural for the government to want to support it and that it should because the government is the source of law. </s>
<s> Replacing marriage would mean a plethora of minor and specific legal contracts to cover all the hundreds of rights and mutual obligations currently conferred by marriage. </s>
<s> Legal issues such as inheritance, pension and social security rights, adoption, custody, and home ownership that would need separate contracts without the umbrella of marriage. </s>
<s> Marriage is more convenient. </s>
<s>  </s>
