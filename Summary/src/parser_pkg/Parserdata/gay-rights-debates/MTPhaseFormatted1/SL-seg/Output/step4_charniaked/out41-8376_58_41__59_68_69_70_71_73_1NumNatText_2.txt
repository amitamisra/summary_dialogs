(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT This) (NN discussion)) (VP (AUX is) (PP (IN about) (NP (DT the) (NN government) (NN roll))) (PP (IN in) (NP (NN marriage))) (, ,) (PP (ADVP (RB specifically)) (VBG pertaining) (PP (TO to) (NP (JJ homosexual) (NN marriage))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (MD should) (VP (AUX have) (NP (DT no) (NN say)) (PP (IN in) (NP (NN marriage)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN government)) (VP (MD should) (VP (AUX have) (NP (NP (DT a) (NN say)) (PP (IN in) (NP (NN marriage)))) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (ADJP (RBR more) (JJ practical) (PP (IN in) (NP (NP (NN today) (POS 's)) (NN world)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (JJ individual) (NNS contracts)) (PP (IN by) (NP (NP (NNS lawyers)) (CONJP (RB rather) (IN than)) (NP (NP (DT the) (NN government)) (VP (AUXG being) (VP (VBN involved))))))) (VP (MD would) (VP (AUX be) (NP (DT the) (JJS fairest) (NN compromise))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBD encountered) (NP (NP (NNS problems)) (PP (IN with) (NP (NP (DT this) (NN view)) (PP (IN in) (NP (PRP$ their) (JJ personal) (NN life)))))) (SBAR (WHADVP (WRB when)) (S (NP (PRP$ their) (NN partner)) (VP (VBD died)))))) (, ,) (CC and) (S (NP (PRP they)) (VP (AUX were) (ADJP (JJ unable) (S (VP (TO to) (VP (VB sort) (PRT (RP out)) (NP (NP (DT all)) (PP (IN of) (NP (NP (DT the) (JJ legal) (NNS issues)) (VP (VBN involved) (PP (IN in) (NP (NN death)))) (, ,) (PP (IN like) (NP (NP (NN inheritance)) (, ,) (CC or) (NP (JJ social) (NN security) (NNS rights))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (EX there)) (VP (MD will) (VP (AUX be) (NP (NP (NNS privileges)) (SBAR (WHNP (WDT that)) (S (VP (VBP come) (PP (ADVP (RB only)) (IN in) (NP (NN government) (VBN recognized) (NNS unions)))))))))))) (. .)))

(S1 (S (NP (NP (JJ S1) (NNS comments)) (JJS best) (NN way) (S (VP (TO to) (VP (VB go) (SBAR (IN about) (S (VP (VBG solving) (NP (DT these) (NNS problems))))))))) (VP (AUX is) (S (VP (TO to) (VP (VB take) (NP (PRP them)) (NP (CD one) (NN issue)) (PP (IN at) (NP (NP (DT a) (NN time)) (VP (ADVP (RB rather) (RB then)) (VBG facing) (NP (NP (DT the) (NN problem)) (PP (IN as) (NP (DT a) (NN whole))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (PRP you)) (VP (AUX need) (S (VP (TO to) (VP (VB look) (PP (IN at) (NP (NP (NNS situations)) (PP (IN in) (PP (IN from) (NP (NP (JJ different) (NNS perspectives)) (PP (IN like) (NP (NP (NNS trees)) (PP (IN in) (NP (DT the) (NNS woods))))))))))))))))) (. .)))

(S1 (S (NP (DT Both) (NNP S1) (CC and) (NNP S2)) (VP (AUX have) (NP (NP (JJ previous) (NN experience)) (PP (IN with) (NP (NP (JJ married) (NN life)) (CC and) (NP (JJ single) (NN life)))))) (. .)))

