<s> <PARAGRAPH> </s>
<s>  S1 one believes that it is inconsistent to support gay rights but to be against gay marriage. </s>
<s> S1 does not have a problem with such an inconsistency but knowing that S2 supports gay marriage it would make sense that S2 would be bothered by it. </s>
<s> S1 does not believe that S2 understands the concept of inconsistency. </s>
<s> S1 thinks that S2 would be troubled by Biden's position on gay marriage. </s>
<s> it would be like someone saying they are for reproductive / abortion rights, but yet being against it being legal for women to get an abortion at an abortion clinic. </s>
<s> S2 does not see an inconsistency in not supporting gay marriage but being for gay rights. </s>
<s> S2 explains that there is not an inconsistency in being against gay marriage, but for gay rights in every other way. </s>
<s> Showing an inconsistency would be like Frank saying he is against abortion rights but has no problem with his wife getting one. </s>
<s> Just saying that Frank is inconsistent on abortion issues is not enough to show what is inconsistent. </s>
<s> S2 is arguing against the position regardless of who is holding it. </s>
<s>  </s>
