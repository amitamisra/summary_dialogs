(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ implies) (SBAR (IN that) (S (NP (NP (DT the) (NNP Dominionist)) (PP (IN as) (NP (DT a) (NN group)))) (VP (AUX is) (RB not) (ADJP (JJ interested) (PP (IN in) (NP (NP (NNS people) (POS 's)) (NN well-being)))))))) (CC but) (VP (VBP want) (S (VP (TO to) (VP (VB control) (NP (NP (NNS people) (POS 's)) (NNS lives))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NNPS Dominionists)) (VP (VBZ exists))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (NNP Jerry) (NNP Falwell)) (CC and) (NP (NNP Pat) (NNP Robertson))) (PP (IN as) (NP (NP (NNS sources)) (SBAR (WHNP (WDT that)) (S (VP (VBP refer) (PP (TO to) (NP (DT the) (NNPS Dominionists))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ tells) (S (NP (NNP S2)) (VP (TO to) (VP (AUX do) (NP (PRP$ his) (NN homework)) (PP (IN in) (S (VP (VBG verifying) (NP (NP (DT the) (NN existence)) (PP (IN of) (NP (DT the) (NNPS Dominionists))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (DT the) (NNPS Dominionists)) (VP (AUX is) (NP (NP (DT a) (JJ fictional) (NN group)) (VP (VBN created) (PP (IN by) (NP (JJ radical) (JJ left-leaning) (NNS groups))) (S (VP (TO to) (VP (VB demonize) (NP (NNP Christianity))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD said) (SBAR (IN that) (S (NP (PRP he)) (VP (VP (VBD looked) (PRT (RP up)) (NP (NP (NNP Dominionist) (NNS websites)) (PP (IN on) (NP (NNP Google))))) (CC but) (VP (MD can) (ADVP (RB only)) (VP (VB find) (NP (NP (NNS articles)) (PP (IN about) (NP (PRP them)))) (PP (IN on) (NP (NP (NNS websites)) (SBAR (WHNP (WDT that)) (S (VP (VBP promote) (NP (JJ liberal) (NNS views))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ challenges) (S (NP (NNP S1)) (VP (TO to) (VP (VP (ADVP (RB either)) (VB produce) (NP (NP (DT the) (NN link)) (PP (TO to) (NP (NP (DT the) (NN source)) (PP (IN of) (NP (DT a) (NNP Dominionist) (NN article))) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBD posted)))))))) (CC or) (VP (VB delete) (NP (NP (DT the) (NN post)) (PP (IN as) (NP (DT a) (NN lie))))))))) (. .)))

