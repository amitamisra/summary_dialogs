<PARAGRAPH> S1 is of the opinion that a certain level of dishonesty is understandable and expected in politics.
In regard to gay rights, the religious right and conservative republicans have made it a poisoned topic such that politicians will not say the right thing because it will cost them votes.
Democrats have had to be moderate on the issue in order to still be in a position to do something.
It's too naive to expect to a politician to give their view on matters, and so it is necessary to figure out how often they lie, what they lie about, and how big their lies are.
In this particular case, if Biden says he does not support gay marriage S1 is just as likely to think it's because he knows that open support could cost him the White House as any other reason.
S2 believes that as political consumers we should expect honesty of the politicians.
It is not too much to demand honesty.
Without that expectation it is pointless in asking a politician a direct question.
S2 understands that politicians lie but holds that if words of politicians mean nothing then there is little point in having political discourse.
Political cynicism is no excuse to not put expectations on our candidates to be honest with the American people.
