(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (NP (NN marriage)) (VP (AUX is) (VP (VBG recognizing) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (JJ natural) (JJ heterosexual) (NN contact)))))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (VP (VBN questioned) (PP (IN about) (NP (JJ artificial) (NN insemination))) (PP (IN by) (NP (NNP S2)))))) (NP (PRP he)) (VP (VBZ replies) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NP (DT the) (NN way)) (SBAR (WHNP (IN that)) (S (NP (DT the) (NNS children)) (VP (AUX are) (VP (VBN created) (SBAR (IN that) (S (VP (AUX is) (ADJP (JJ important)))))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (NP (NP (DT the) (NN nature)) (PP (IN of) (NP (DT the) (NN relationship))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNS values)) (VP (AUX are) (SBAR (WHNP (WP what) (NN guide) (NNS people) (CC and) (NN everyone)) (S (VP (VBZ thinks) (SBAR (S (NP (PRP theirs)) (VP (AUX are) (ADJP (JJ correct)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (S (VP (VBG allowing) (NP (JJ gay) (NN marriage) (NNS forces)) (NP (NP (NNS values)) (PP (IN on) (NP (NN society)))) (PP (IN by) (S (VP (VBG forcing) (S (NP (PRP them)) (VP (TO to) (VP (VB accept) (NP (PRP it))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ questions) (SBAR (WHNP (WP what)) (S (NP (NNP God)) (VP (VBZ enforces) (NP (NN marriage) (NNS rules)) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (VP (VBN performed) (PP (IN by) (NP (DT a) (NN judge))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NNP Congo) (NN tribe)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (NNS warriors)) (VP (VB marry) (NP (JJ other) (NNS warriors)) (SBAR (IN because) (S (S (VP (AUXG being) (PP (IN with) (NP (DT a) (NN woman))))) (VP (MD may) (VP (VB make) (S (NP (PRP them)) (ADJP (RBR less) (VBN feared))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (VP (VP (RB not) (VBG allowing) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (NN discrimination)))))) (CC and) (VP (VBG forcing) (NP (NNS morals)) (PP (IN on) (NP (JJ other) (NNS people)))))))) (. .)))

