<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX in/IN unfriendly/JJ argument/NN about/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>The/DT argument/NN originally/RB starts/VBZ in/IN a/DT tangent/NN of/IN Abortion/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ that/IN S1/NNP is/AUX trolling/VBG the/DT thread/NN and/CC briefly/NN comment/NN on/IN abortion/NN playing/VBG the/DT devil/JJ advocate/NN to/TO shows/NNS the/DT misuse/NN of/IN logic/NN is/AUX S1/NNP 's/POS thinking/NN for/IN issues/NNS in/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP continues/VBZ to/TO antagonize/VB S2/NNP ./. </C>
</S>
<S>
<C>S2/NNP continues/VBZ to/TO discuss/VB the/DT necessity/NN of/IN drawing/NN boundaries/NNS in/IN reason/NN and/CC the/DT importance/NN staying/VBG on/IN track/NN ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ they/PRP should/MD use/VB familiar/JJ comparison/NN of/IN attacks/NNS on/IN inter-racial/JJ marriage/NN where/WRB if/IN you/PRP are/AUX against/IN interracial/JJ marriage/NN ,/, </C>
<C>you/PRP are/AUX for/IN slavery/NN ,/, </C>
<C>to/TO show/VB his/PRP$ support/NN of/IN gay/JJ marriage/NN ,/, </C>
<C>just/RB to/TO show/VB S1that/NNP type/NN of/IN discourse/NN is/AUX not/RB logically/RB accurate/JJ ./. </C>
</S>
<S>
<C>S1/NNP brings/VBZ the/DT ``/`` 9th/NN ''/'' from/IN Maine/NNP and/CC ``/`` Prop/VB 8/CD ''/'' from/IN California/NNP as/IN example/NN of/IN states/NNS that/WDT have/AUX showed/VBN that/IN their/PRP$ majority/NN was/AUX against/IN gay/JJ marriage/NN ,/, </C>
<C>but/CC yet/RB people/NNS in/IN those/DT states/NNS are/AUX still/RB trying/VBG to/TO get/VB them/PRP overturned/VBN ./. </C>
</S>
<S>
<C>S2/JJ compliments/NNS S1/VBP for/IN clever/JJ banter/NN ./. </C>
</S>
<S>
<C>S2/NNP ends/VBZ the/DT discussion/NN </C>
<C>by/IN accusing/VBG S1/NNP of/IN repeating/VBG his/PRP$ points/NNS ,/, </C>
<C>which/WDT pertain/VBP to/TO two/CD separate/JJ issues/NNS ./. </C>
</S>
</P>
</T>
