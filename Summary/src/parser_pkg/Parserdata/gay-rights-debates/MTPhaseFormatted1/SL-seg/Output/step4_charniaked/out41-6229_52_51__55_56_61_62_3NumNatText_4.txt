(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asks) (NP (NNP S2)) (SBAR (WHADVP (WRB how)) (S (NP (JJ equal) (NN marriage)) (VP (MD would) (VP (VB hurt) (NP (NP (NP (NN someone)) (PRN (, ,) (S (`` ``) (NP (PRP her)) ('' '')) (, ,))) (CC or) (NP (NN anyone) (RB else)))))))) (. .)))

(S1 (S (NP (NP (NP (NNP S1) (POS 's)) (NN way)) (PP (IN of) (NP (JJ equal) (NN marriage)))) (VP (MD would) (RB not) (ADJP (JJ S1) (CC or) (PRP$ her))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX is) (ADVP (RB only)) (VP (VBN concerned) (PP (IN with) (NP (JJ gay) (NNS rights))))) (CC and) (VP (MD will) (VP (VB talk) (NP (NNS people)) (PP (IN out) (PP (IN of) (S (VP (VBG attending) (NP (JJ bible) (NNS churches))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (MD will) (VP (VB try) (S (VP (TO to) (VP (VP (ADVP (RB legally)) (VB disrupt) (NP (NP (DT any)) (PP (IN of) (NP (DT the) (NN fundraising)))) (NP (NNS activities) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (VP (VBG going) (PRT (RP on)))))))) (, ,) (VP (VB protest) (NP (NP (PRP$ their) (NNS weddings)) (CC and) (NP (PRP$ their) (NNS services))))))))) (. .)))

(S1 (S (SBAR (IN Once) (S (NP (PRP they)) (VP (VP (VBD crossed) (NP (DT the) (NN line))) (CC and) (VP (VBN enacted) (NP (NP (NNS laws)) (SBAR (WHNP (WDT that)) (S (VP (VBP hurt) (NP (NP (NNP S1) (CC and) (NNP S1) (POS 's)) (NN family)))))))))) (, ,) (RB then) (NP (PRP they)) (VP (AUX have) (NP (NP (DT no) (NN place)) (PP (IN in) (NP (NN society))))) (. .)))

(S1 (S (NP (DT The) (NN majority)) (VP (MD should) (RB not) (VP (AUX have) (NP (DT the) (NN right) (S (VP (TO to) (VP (VP (VB enslave) (NP (PRP$ their) (NN fellow) (NNS citizens))) (CC and) (VP (VB send) (NP (PRP them)) (PP (TO to) (NP (NN concentration) (NNS camps)))))))))) (. .)))

(S1 (S (NP (PRP She)) (VP (MD should) (VP (VB admit) (PP (TO to) (NP (NP (PRP$ her) (NN bigotry)) (, ,) (NP (NN discrimination)) (CC and) (NP (JJ anti-gay) (NNS views)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (AUX is) (ADJP (JJ glad) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBD abandoned) (NP (DT a) (JJ silly) (NN argument)) (PP (IN about) (NP (NP (DT the) (NN violation)) (PP (IN of) (NP (DT the) (JJ first) (NN amendment)))) (ADVP (RB right)))))))) (, ,) (CC but) (ADVP (RB still)) (VP (AUX does) (RB not) (VP (VB see) (SBAR (WHADVP (WRB why)) (S (NP (NP (JJ only) (NNS laws)) (SBAR (WHNP (WDT that)) (S (VP (AUX do) (RB not) (VP (VB cause) (NP (NN harm))))))) (VP (MD should) (VP (AUX be) (VP (VBN enacted))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX is) (VP (VBN surprised) (PP (IN at) (NP (NP (NP (NNP S1) (POS 's)) (NN affinity)) (PP (IN for) (NP (DT the) (NN harm) (NN principle))))) (PP (VBN given) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX has) (VP (VBN expressed) (S (VP (AUXG being) (PP (IN at) (NP (NP (NN war)) (PP (IN with) (NP (JJ evangelical) (JJ anti-gay) (NNP christianity)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ holds) (SBAR (IN that) (S (NP (NNS people)) (VP (MD should) (VP (AUX be) (VP (IN about) (S (VP (VP (TO to) (VP (VB legislate) (NP (PRP$ their) (NNS views)))) (, ,) (CC and) (VP (VBZ wants) (S (NP (NNP S1)) (VP (TO to) (VP (VB explain) (SBAR (WHADVP (WRB why)) (S (NP (DT that)) (VP (MD should) (RB not) (VP (AUX be) (ADVP (RB so)) (PP (IN with) (NP (JJ anti-gay) (NNS people))))))))))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (DT a) (NN democracy)) (PP (IN like) (NP (JJ ours))))) (NP (NNS people)) (VP (VBP use) (NP (DT the) (JJ democratic) (NN process)) (S (VP (TO to) (VP (AUX have) (NP (NP (NNS views)) (VP (VBN enacted) (PP (IN into) (NP (NN law))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP she)) (VP (AUX is) (ADJP (JJ free) (S (VP (TO to) (VP (AUX do) (NP (DT this)) (, ,) (ADVP (RB then)) (ADVP (RB so))))))))) (VP (AUX is) (ADJP (JJ S1))) (. .)))

