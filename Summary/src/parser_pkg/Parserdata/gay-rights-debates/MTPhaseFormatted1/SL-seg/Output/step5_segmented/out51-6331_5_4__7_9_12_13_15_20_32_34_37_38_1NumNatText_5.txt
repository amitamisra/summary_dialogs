<T>
<P>
</P>
<P>
<S>
<C>S1/NNP argues/VBZ that/IN group/NN marriages/NNS would/MD not/RB make/VB sense/NN under/IN current/JJ marriage/NN laws/NNS ,/, </C>
<C>as/IN they/PRP only/RB account/VBP for/IN two/CD individuals/NNS in/IN a/DT legal/JJ marriage/NN ./. </C>
</S>
<S>
<C>They/PRP state/VBP that/IN opponents/NNS of/IN homosexual/JJ marriage/NN tend/VBP to/TO argue/VB that/IN a/DT change/NN to/TO marriage/NN law/NN would/MD make/VB it/PRP too/RB open/JJ ended/VBD ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB that/IN to/TO be/AUX true/JJ ,/, and/CC argues/VBZ that/IN homosexuals/NNS are/AUX not/RB trying/VBG to/TO broaden/VB the/DT scope/NN of/IN marriage/NN ,/, </C>
<C>but/CC only/RB change/VB the/DT law/NN to/TO be/AUX inclusive/JJ of/IN themselves/PRP ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN bringing/VBG up/RP hypothetical/JJ arguments/NNS to/TO the/DT contrary/NN is/AUX a/DT distraction/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN an/DT effect/NN of/IN homosexuals/NNS being/AUXG allowed/VBN to/TO legally/RB marry/VB would/MD lead/VB to/TO even/RB more/JJR changes/NNS to/TO marriage/NN laws/NNS that/WDT would/MD include/VB more/JJR than/IN simply/RB homosexual/JJ relationships/NNS ./. </C>
</S>
<S>
<C>This/DT person/NN believes/VBZ that/IN allowing/VBG homosexual/JJ marriage/NN is/AUX a/DT case/NN that/WDT broadens/VBZ marriage/NN </C>
<C>and/CC if/IN allowed/VBN ,/, </C>
<C>can/MD open/VB basis/NN for/IN future/JJ arguments/NNS </C>
<C>to/TO broaden/VB the/DT law/NN further/RB ./. </C>
</S>
<S>
<C>They/PRP firmly/RB believe/VBP that/IN marriage/NN as/IN an/DT institution/NN is/AUX between/IN a/DT heterosexual/NN male/JJ and/CC female/JJ </C>
<C>and/CC that/IN changing/VBG that/DT in/IN any/DT way/NN gives/VBZ legal/JJ grounds/NNS to/TO continue/VB changing/VBG it/PRP ./. </C>
</S>
</P>
</T>
