<s> <PARAGRAPH> </s>
<s>  S1 claims that the problem with an unmentioned study is that the majority of people identify as Christian, but he also recognizes S2's argument that more details of the study would be helpful to determine whether this is actually a problem. </s>
<s> S1 also claims that the forum tends to make Christianity look bad, and points out that the divorce rate of Christians is irrelevant to the gay marriage amendment. </s>
<s> S2 defends the unmentioned study by suggesting that S1 is unaware of the particular details of the study which could invalidate his argument. </s>
<s> He also suggests that S1 dismissed the results of the study without trying to understand them, and that the study was in fact well done and any objections should be based on evidence. </s>
<s>  </s>
