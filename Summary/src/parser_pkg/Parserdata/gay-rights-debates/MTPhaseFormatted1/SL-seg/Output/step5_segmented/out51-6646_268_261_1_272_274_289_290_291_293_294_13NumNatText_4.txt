<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB that/IN disagreement/NN is/AUX the/DT same/JJ as/IN being/AUXG bigoted/VBN ,/, </C>
<C>and/CC that/IN if/IN someone/NN resorts/VBZ to/TO name/VB calling/VBG in/RP an/DT argument/NN </C>
<C>it/PRP means/VBZ they/PRP no/RB longer/RB have/AUX any/DT sound/JJ arguments/NNS ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN any/DT argument/NN goes/VBZ back/RB to/TO one/CD 's/POS motivations/NNS </C>
<C>and/CC that/IN people/NNS do/AUX not/RB often/RB oppose/VB homosexual/JJ marriage/NN on/IN the/DT basis/NN of/IN not/RB liking/VBG homosexual/JJ individuals/NNS ,/, but/CC because/IN of/IN their/PRP$ views/NNS on/IN marriage/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN people/NNS who/WP base/VBP their/PRP$ arguments/NNS on/IN how/WRB they/PRP view/VBP marriage/NN would/MD still/RB think/VB that/DT way/NN towards/IN any/DT group/NN that/WDT desired/VBD </C>
<C>to/TO change/VB their/PRP$ definition/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP disagrees/VBZ and/CC believes/VBZ that/IN arguments/NNS made/VBN against/IN homosexual/JJ marriage/NN are/AUX based/VBN upon/IN a/DT bias/NN against/IN homosexuality/NN as/IN a/DT whole/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN these/DT peoples/NNS '/POS opinions/NNS are/AUX definitively/RB against/IN homosexuals/NNS in/IN general/JJ ./. </C>
</S>
</P>
</T>
