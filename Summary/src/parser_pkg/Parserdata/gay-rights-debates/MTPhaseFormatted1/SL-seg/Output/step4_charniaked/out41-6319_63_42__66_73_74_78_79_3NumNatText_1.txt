(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT a) (ADJP (RB seemingly) (JJ pending)) (NN law)) (SBAR (WHNP (WDT that)) (S (VP (MD would) (VP (VB permit) (S (NP (NN marriage) (NNS officials)) (VP (TO to) (VP (VB decline) (S (VP (TO to) (VP (VB conduct) (NP (JJ gay) (NNS marriages)) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX are) (ADJP (VBN opposed) (PP (TO to) (NP (PRP them)))) (PP (IN on) (NP (JJ religious) (NNS principals))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ finds) (S (NP (PRP it)) (ADJP (JJ acceptable))) (, ,) (SBAR (IN while) (S (NP (NNP S2)) (VP (VBZ complains) (PP (IN about) (NP (PRP it))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ proposes) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ easy) (SBAR (IN for) (S (NP (NNP S2)) (VP (TO to) (VP (AUX be) (ADJP (JJ unsympathetic)) (PP (IN because) (NP (NNP S2) (NN dislikes) (NN religion)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ acknowledges) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ considers) (S (NP (JJ such) (JJ religious) (NNS objections)) (VP (TO to) (VP (AUX be) (ADJP (JJ evil))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (SBAR (IN because) (S (NP (DT the) (NN individual)) (VP (VBD licensed) (S (VP (TO to) (VP (VB perform) (SBAR (S (NP (DT a) (NN marriage)) (VP (AUX is) (VP (VP (VBN paid)) (CC or) (VP (VBN licensed) (PP (IN by) (NP (DT the) (NN State)))))))))))))) (, ,) (NP (DT the) (NN individual)) (VP (MD should) (VP (VP (VB reflect) (NP (NP (DT the) (NN will) (CC and) (NNS choices)) (PP (IN of) (NP (DT the) (NNP State))))) (CC and) (VP (ADVP (RB therefore)) (VB perform) (NP (JJ gay) (NNS marriages)) (PP (IN despite) (NP (NP (PRP$ his) (CC or) (PRP$ her) (JJ personal) (JJ religious) (NN objection)) (PP (TO to) (NP (PRP them))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ suspects) (SBAR (IN that) (S (NP (DT the) (NN law)) (VP (MD may) (VP (VB result) (PP (IN in) (NP (NP (DT an) (JJ effective) (NN denial)) (PP (IN of) (NP (JJ gay) (NN marriage)))))))))) (. .)))

