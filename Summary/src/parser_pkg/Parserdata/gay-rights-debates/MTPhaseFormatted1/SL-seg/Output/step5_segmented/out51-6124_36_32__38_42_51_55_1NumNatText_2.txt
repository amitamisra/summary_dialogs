<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ that/IN the/DT government/NN should/MD stay/VB out/RB of/IN marriage/NN </C>
<C>and/CC that/IN it/PRP should/MD be/AUX left/VBN to/TO religious/JJ institutions/NNS ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ there/EX needs/VBZ to/TO be/AUX a/DT better/JJR system/NN </C>
<C>and/CC that/IN single/JJ people/NNS are/AUX the/DT ones/NNS that/WDT are/AUX harmed/VBN the/DT most/JJS by/IN marriage/NN laws/NNS </C>
<C>because/IN they/PRP are/AUX unable/JJ to/TO get/VB any/DT of/IN the/DT benefits/NNS that/IN married/JJ people/NNS do/AUX </C>
<C>even/RB if/IN they/PRP want/VBP them/PRP ,/, </C>
<C>or/CC it/PRP is/AUX important/JJ to/TO their/PRP$ situation/NN ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ religious/JJ ceremonies/NNS are/AUX not/RB what/WP gay/JJ people/NNS want/VBP </C>
<C>because/IN they/PRP already/RB can/MD have/AUX them/PRP via/IN churches/NNS ./. </C>
</S>
<S>
<C>They/PRP want/VBP the/DT rights/NNS </C>
<C>and/CC to/TO keep/VB the/DT government/NN out/RP would/MD be/AUX to/TO give/VB up/RP those/DT rights/NNS ./. </C>
</S>
<S>
<C>If/IN single/JJ people/NNS want/VBP those/DT rights/NNS </C>
<C>they/PRP should/MD get/VB married/VBN ,/, </C>
<C>but/CC he/PRP thinks/VBZ you/PRP should/MD be/AUX free/JJ to/TO marry/VB who/WP you/PRP wish/VBP ./. </C>
</S>
</P>
</T>
