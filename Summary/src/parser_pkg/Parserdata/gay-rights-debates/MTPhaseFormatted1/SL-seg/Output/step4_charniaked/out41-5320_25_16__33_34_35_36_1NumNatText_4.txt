(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (JJ gay) (NN marriage)) (CC and) (NP (DT the) (NNP Ccnstitution))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NP (CD eight)) (PP (IN out) (PP (IN of) (NP (NP (CD nine) (NN state) (JJ supreme) (NNS courts)) (SBAR (WHNP (WDT that)) (S (VP (AUX have) (VP (VBN taken) (PRT (RP up)) (NP (DT the) (NN issue)))))))))) (VP (AUX has) (VP (VBN ruled) (PP (VBG banning) (NP (NP (JJ same-sex) (NN marriage)) (ADJP (JJ unconstitutional)) (, ,) (VP (VBN based) (PP (IN on) (NP (PRP$ their) (NN state) (NNS constitutions)))))) (SBAR (IN that) (S (VP (ADVP (RB closely)) (VB parallel) (NP (DT the) (JJ federal) (CD one)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NP (DT the) (NN nature)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (ADJP (DT both) (JJ legal) (CC and) (JJ religious))))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT the) (NNS courts)) (ADVP (RB only)) (VP (VB deal) (PP (IN with) (NP (DT the) (JJ legal) (NN aspect)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (NNP Massachusetts) (NN ruling)) (VP (AUX was) (VP (VBN based) (PP (IN on) (NP (NP (DT a) (JJ specific) (NN provision)) (PP (IN in) (NP (PRP$ their) (NN constitution))) (SBAR (WHNP (WDT that)) (S (VP (VBZ prohibits) (NP (NP (NN discrimination)) (PP (VBN based) (PP (IN on) (NP (NP (JJ sexual) (NN discrimination)) (, ,) (SBAR (WHNP (WDT which)) (S (VP (AUX is) (RB not) (VP (VBN founded) (PP (IN in) (NP (NP (DT the) (JJ federal) (NN constitution)) (, ,) (SBAR (WHPP (TO to) (WHNP (WDT which))) (S (NP (NNP S1)) (VP (VBZ states) (SBAR (S (NP (DT the) (NN ruling)) (VP (AUX was) (PP (IN on) (NP (DT the) (JJ equal) (NN protection) (NNS grounds))))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ many) (NN state) (NNS constitutions)) (VP (VBP guarantee) (NP (NP (NNS rights)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (RB not) (PP (IN in) (NP (DT the) (JJ federal) (NN constitution))))))))))) (. .)))

