<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG marriage/NN benefits/NNS ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN healthcare/NNP usually/RB applies/VBZ to/TO spouses/NNS ,/, </C>
<C>so/RB in/IN order/NN to/TO get/VB the/DT benefits/NNS of/IN any/DT health/NN care/NN provided/VBN by/IN a/DT spouses/NNS job/NN the/DT requirement/NN is/AUX marriage/NN ./. </C>
</S>
<S>
<C>Since/IN gays/NNS can/MD not/RB get/VB married/VBN ,/, </C>
<C>they/PRP cannot/VBP share/NN in/IN each/DT other/JJ 's/POS health/NN care/NN ,/, </C>
<C>and/CC he/PRP hopes/VBZ that/IN gay/JJ marriages/NNS will/MD be/AUX sorted/VBN out/RP </C>
<C>so/IN he/PRP does/AUX not/RB have/AUX to/TO hear/VB them/PRP complain/VB about/IN health/NN care/NN ./. </C>
</S>
<S>
<C>He/PRP then/RB uses/VBZ an/DT example/NN of/IN a/DT straight/JJ guy/NN who/WP has/AUX a/DT girlfriend/NN who/WP can/MD not/RB get/VB any/DT health/NN care/NN </C>
<C>because/IN they/PRP 're/AUX not/RB married/JJ ,/, </C>
<C>and/CC he/PRP asks/VBZ S2/NNP if/IN she/PRP hould/RB get/VBP benefits/NNS ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN legally/RB if/IN they/PRP cohabitate/VBP for/IN a/DT prescribed/JJ period/NN of/IN time/NN or/CC have/AUX children/NNS ,/, </C>
<C>by/IN common/JJ law/NN they/PRP can/MD ./. </C>
</S>
</P>
</T>
