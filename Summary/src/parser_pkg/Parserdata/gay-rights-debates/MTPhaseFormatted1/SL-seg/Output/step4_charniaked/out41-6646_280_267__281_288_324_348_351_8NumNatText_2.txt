(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (NP (NP (PRP he)) (CC and) (NP (PRP$ his) (NN family))) (VP (VBZ represents) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (RB normal))))))))) (. .)))

(S1 (S (NP (NP (NN Homosexuality)) (PP (IN in) (NP (PRP$ his) (NN opinion)))) (VP (VP (AUX is) (NP (NP (DT a) (NN deviation)) (PP (IN from) (NP (DT the) (NN norm))))) (CC and) (VP (AUX is) (ADJP (JJ perverse)))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ rejects) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (PRP he)) (VP (VP (VBZ hates) (NP (JJ gay) (NNS people))) (CC and) (VP (VBZ says) (SBAR (S (NP (PRP he)) (ADVP (RB simply)) (VP (AUX does) (RB not) (VP (VB think) (SBAR (S (NP (PRP we)) (VP (MD should) (VP (VB redefine) (NP (NN marriage)) (PP (IN for) (NP (PRP them))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (S (NP (NNP S2)) (VP (MD may) (VP (VB believe) (SBAR (IN as) (S (VP (VBZ wishes) (SBAR (RB even) (IN if) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ wrong))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ questions) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (JJ perverse)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ equates) (NP (NP (DT the) (NN word)) (ADJP (JJ perverse) (PP (IN with) (NP (NN diversity)))))) (. .)))

(S1 (S (NP (NP (DT A) (NN deviation)) (PP (IN for) (NP (NP (DT the) (NN norm)) (PP (IN in) (NP (PRP$ his) (NN opinion)))))) (VP (AUX is) (NP (NN diversity))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ doubts) (SBAR (S (NP (NNP S1)) (VP (AUX is) (PP (IN against) (NP (NN diversity))) (SBAR (IN so) (S (NP (PRP he)) (VP (VBZ thinks) (SBAR (S (NP (PRP he)) (ADVP (RB simply)) (VP (VBZ hates) (NP (NP (NP (DT a) (NN form)) (PP (IN of) (NP (PRP it)))) (, ,) (NP (JJ gay) (NNS people))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ views) (S (VP (VBG calling) (NP (NN something)) (NP (NP (JJ perverse) (NN ad) (NN evidence)) (PP (IN of) (NP (DT this) (NN hate))))))) (CC and) (VP (AUX does) (RB not) (VP (VB believe) (NP (NP (NNP S1)) (SBAR (WHADVP (WRB when)) (S (NP (PRP he)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB hate) (SBAR (RB gays) (S (NP (PRP he)) (VP (AUX is) (ADVP (RB simply)) (ADJP (VBN worried) (PP (IN about) (NP (NN marriage)))))))))))))))))) (. .)))

