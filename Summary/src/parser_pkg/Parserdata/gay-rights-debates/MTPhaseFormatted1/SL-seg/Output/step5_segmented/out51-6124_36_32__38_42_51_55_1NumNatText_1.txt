<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP discuss/VB the/DT civil/JJ benefits/NNS of/IN marriage/NN and/CC how/WRB they/PRP could/MD or/CC could/MD not/RB be/AUX extended/VBN to/TO single/JJ people/NNS and/CC gay/JJ people/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX initially/RB against/IN government/NN being/AUXG involved/VBN in/IN the/DT equation/NN ,/, </C>
<C>but/CC after/IN S2/NNP describes/VBZ many/JJ of/IN the/DT legal/JJ rights/NNS and/CC benefits/NNS associated/VBN with/IN marriage/NN ,/, such/JJ as/IN automatic/JJ inheritance/NN and/CC hospital/NN visitation/NN rights/NNS ,/, </C>
<C>S1/NNP is/AUX then/RB able/JJ to/TO relate/VB to/TO the/DT issues/NNS </C>
<C>by/IN considering/VBG the/DT case/NN of/IN his/PRP$ life-long/JJ bachelor/NN uncle/NN ,/, suggesting/VBG that/IN it/PRP is/AUX single/JJ people/NNS who/WP are/AUX discriminated/VBN against/IN by/IN being/AUXG excluded/VBN from/IN the/DT marriage/NN benefits/NNS ./. </C>
</S>
<S>
<C>S2/NNP suggests/VBZ that/IN if/IN a/DT single/JJ person/NN desires/VBZ the/DT benefits/NNS of/IN marriage/NN ,/, </C>
<C>they/PRP should/MD marry/VB ,/, </C>
<C>but/CC that/IN they/PRP should/MD be/AUX able/JJ to/TO marry/VB a/DT person/NN of/IN either/DT gender/NN as/IN they/PRP see/VBP fit/NN ./. </C>
</S>
</P>
</T>
