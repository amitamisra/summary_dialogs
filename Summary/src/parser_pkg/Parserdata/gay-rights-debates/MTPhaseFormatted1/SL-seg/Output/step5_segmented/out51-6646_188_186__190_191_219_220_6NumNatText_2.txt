<T>
<P>
</P>
<P>
<S>
<C>S1/NNP provides/VBZ two/CD websites/NNS which/WDT outline/VB the/DT history/NN of/IN gay/JJ marriage/NN in/IN a/DT few/JJ different/JJ cultures/NNS advising/VBG homosexuals/NNS were/AUX held/VBN in/IN high/JJ regard/NN in/IN some/DT cultures/NNS and/CC were/AUX thought/VBN of/IN as/IN healers/NNS and/CC prophets/NNS ./. </C>
</S>
<S>
<C>S1/NNP cites/VBZ several/JJ cultures/NNS who/WP practices/NNS gay/JJ unions/NNS such/JJ as/IN Romans/NNPS and/CC Celts/NNPS ./. </C>
</S>
<S>
<C>S1/NNP advises/VBZ some/DT cultures/NNS still/RB practice/VB polygamy/NN and/CC one/CD in/IN Nigeria/NNP even/RB practices/NNS polyandry/JJ ./. </C>
</S>
<S>
<C>S1/NNP advises/VBZ </C>
<C>while/IN homosexual/JJ unions/NNS were/AUX present/JJ in/IN history/NN they/PRP were/AUX not/RB actually/RB thought/VBN of/IN as/IN marriages/NNS but/CC more/JJR of/IN unions/NNS for/IN a/DT specific/JJ purpose/NN ./. </C>
</S>
<S>
<C>He/she/NNP contends/VBZ that/IN matrimonial/JJ unions/NNS were/AUX between/IN opposite/JJ genders/NNS ,/, even/RB throughout/IN history/NN ./. </C>
</S>
<S>
<C>S1/NNP concedes/VBZ that/IN homosexual/JJ liaisons/NNS were/AUX common/JJ in/IN other/JJ times/NNS and/CC cultures/NNS but/CC never/RB recognized/VBN as/IN actual/JJ marriages/NNS and/CC were/AUX frowned/VBN upon/IN ./. </C>
</S>
</P>
</T>
