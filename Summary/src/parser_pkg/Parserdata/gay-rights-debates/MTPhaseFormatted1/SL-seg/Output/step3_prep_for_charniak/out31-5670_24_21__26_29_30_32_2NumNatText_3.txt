<s> <PARAGRAPH> </s>
<s>  S1 is uncomfortable with the study because there is not information on how they got the results. </s>
<s> S1 goes on to say the study vilifies a group of people, namely Christians, and that most people in the country may identify as Christian. </s>
<s> S1 also thinks the divorce rate of Christians has no bearing on the issue of gay marriage. </s>
<s> S2 thinks S1 should ask questions about the study rather than just assume it is incorrect and that it is not being used to vilify anyone. </s>
<s> S1 thinks the results are professional and S1 is just uncomfortable with the results. </s>
<s> S2 thinks the divorce rate of Christians is very important to the issue because Christians say they are protecting the "sanctity" of marriage. </s>
<s>  </s>
