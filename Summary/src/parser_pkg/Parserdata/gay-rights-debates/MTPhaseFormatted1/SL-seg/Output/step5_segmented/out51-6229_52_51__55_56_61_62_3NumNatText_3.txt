<T>
<P>
</P>
<P>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- states/NNS how/WRB can/MD gay/JJ marriage/NN hurt/VB politicians/NNS or/CC anyone/NN else/RB ?/. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN there/EX are/AUX laws/NNS that/WDT hurt/VBP all/DT kinds/NNS of/IN peoples/NNS and/CC argues/VBZ about/IN violation/NN of/IN the/DT First/NNP Amendment/NNP </C>
<C>by/IN imposing/VBG someone/NN 's/POS belief/NN over/IN another/DT ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- argues/VBZ that/IN he/PRP is/AUX only/JJ concern/NN with/IN gay/JJ peoples/NNS rights/NNS and/CC says/VBZ that/IN the/DT politician/NN wants/VBZ gays/NNS to/TO remain/VB as/IN second-class/JJ citizens/NNS ./. </C>
</S>
<S>
<C>Argues/VBZ that/IN politician/she/NN is/AUX discriminating/VBG and/CC protecting/VBG her/PRP$ heterosexual/JJ privilege/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ that/IN not/RB accepting/VBG gay/JJ marriage/NN is/AUX punishment/NN to/TO him/PRP ,/, his/PRP$ family/NN and/CC others/NNS ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- He/PRP argues/VBZ about/IN the/DT kind/NN of/IN democracy/NN in/IN where/WRB majority/NN of/IN the/DT people/NNS decide/VBP what/WP best/JJS for/IN society/NN </C>
<C>and/CC how/WRB being/AUXG anti-gay/JJ is/AUX a/DT right/NN of/IN the/DT First/NNP Amendment/NNP ./. </C>
</S>
<S>
<C>Is/AUX a/DT gay/JJ person/NN is/AUX allowed/VBN to/TO have/AUX the/DT freedom/NN ,/, </C>
<C>so/RB does/AUX he/PRP who/WP and/CC others/NNS who/WP may/MD not/RB agree/VB ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- then/RB gives/VBZ the/DT example/NN that/IN not/RB just/RB because/IN majority/NN rules/NNS it/PRP means/VBZ that/IN it/PRP 's/AUX a/DT good/JJ thing/NN ./. </C>
</S>
<S>
<C>For/IN example/NN ,/, enslaving/JJ people/NNS was/AUX accepted/VBN in/IN previous/JJ societies/NNS ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- argues/VBZ that/IN although/IN previous/JJ societies/NNS had/AUX legal/JJ rights/NNS to/TO enslave/JJ people/NNS </C>
<C>but/CC that/IN does/AUX not/RB mean/VB that/IN it/PRP makes/VBZ those/DT decisions/NNS all/PDT the/DT time/NN </C>
<C>and/CC that/IN is/AUX like/IN gay/JJ marriage/NN ./. </C>
</S>
</P>
</T>
