(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (SBAR (IN whether) (CC or) (RB not) (S (NP (NNS Christians)) (VP (VBP want) (S (VP (TO to) (VP (VB ban) (NP (JJ gay) (NNS people)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (JJS most) (NNS Christians)) (VP (AUX are) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB ban) (NP (NP (DT the) (NN decriminalization)) (PP (IN of) (NP (NN same-sex)))) (, ,) (SBAR (S (NP (PRP it)) (VP (AUX was) (ADVP (RB just)) (NP (NP (DT an) (NN accusation)) (VP (VBN made)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NNS evangelicals)) (VP (AUX are) (ADVP (RB rarely)) (VP (VBN united) (PP (IN for) (NP (NP (NN anything)) (, ,) (VP (ADVP (VB let) (RB alone)) (VBG banning) (NP (JJ gay) (NNS people)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ gay) (NNS lobbyists)) (VP (AUX have) (ADVP (RB long)) (VP (VBN stepped) (PP (IN over) (NP (NP (DT the) (NN line)) (PP (IN of) (S (ADVP (RB simply)) (VP (VBG counter-attacking) (PP (IN with) (NP (PRP$ their) (NN defense))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (NP (DT a) (NN majority)) (PP (IN of) (NP (NP (JJ self-identified) (NNS Christians)) (PP (IN in) (NP (DT the) (NNP U.S.)))))) (VP (VBP oppose) (NP (NP (JJ civil) (NN marriage)) (PP (IN for) (NP (JJ gay) (NNS people)))) (, ,) (PP (IN among) (NP (NP (JJ other) (NNS things)) (PP (JJ such) (IN as) (NP (NP (JJ civil) (NNS unions)) (, ,) (NP (JJ gay) (NNS adoptions)) (CC and) (NP (JJ anti-discrimination) (NNS laws))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (PP (IN in) (NP (NNP Spain))) (, ,) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (AUX do) (NP (DT that))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ points) (PRT (RP out)) (NP (NP (DT a) (NN state) (JJ constitutional) (NN amendment)) (VP (VBG trying) (S (VP (TO to) (VP (VB prevent) (NP (JJ same-sex) (NN marriage)))))))))))) (. .)))

