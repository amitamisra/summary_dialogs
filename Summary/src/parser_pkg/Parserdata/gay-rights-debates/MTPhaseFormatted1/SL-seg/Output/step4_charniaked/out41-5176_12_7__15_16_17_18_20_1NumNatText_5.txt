(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS subjects)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT a) (NN law)) (VP (VBG banning) (NP (JJ gay) (NN marriage)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN law)) (VP (AUX is) (ADJP (JJ unjust) (SBAR (IN as) (S (NP (PRP it)) (VP (VBZ segregates) (NP (DT the) (JJ gay) (NN community)) (PP (IN from) (NP (NP (DT that)) (PP (IN of) (NP (DT the) (JJ heterosexual) (NN community))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ supports) (NP (NP (DT the) (NN nation) (POS 's)) (NN ability) (S (VP (TO to) (VP (VB vote) (PP (IN on) (NP (JJ such) (NNS issues)))))))) (CC and) (VP (VBZ supports) (NP (DT the) (`` ``) (NN majority) (NNS rules) ('' '')) (NP (NN philosophy)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NNS courts)) (VP (MD should) (VP (VP (VB take) (NP (NNS individuals) (POS ')) (PP (IN into) (NP (NN consideration)))) (CC and) (VP (VB rule) (PP (IN on) (NP (NP (DT each)) (PP (RB instead) (IN of) (PP (IN as) (NP (DT a) (NN whole)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (NP (DT this) (NN idea) (S (VP (TO to) (VP (AUX be) (ADJP (JJ incorrect)) (S (VP (VBG citing) (NP (DT the) (NN fact) (SBAR (SBAR (IN that) (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP believe) (SBAR (S (NP (NN abortion)) (VP (MD should) (VP (AUX be) (ADJP (JJ illegal)))))))))) (VP (AUX are) (ADVP (RB technically)) (NP (DT the) (NN minority))))) (CC and) (SBAR (IN that) (S (NP (DT a) (NN court)) (VP (MD would) (RB not) (VP (VB rule) (PP (IN on) (NP (DT that) (NN issue))) (ADVP (RB individually))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (NP (NNP S2)) (CC and) (NP (NP (NNS others)) (PP (IN like) (NP (NN him/her))))) (VP (VBP support) (NP (NP (NP (NN society) (POS 's)) (NN oppression)) (PP (IN of) (NP (JJ certain) (NNS groups)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ disagrees) (SBAR (S (NP (VBG advising) (NN he/she)) (VP (VBZ wants) (S (NP (RB only) (DT a) (NN society)) (ADJP (JJ free) (S (VP (TO to) (VP (VB vote) (PP (IN on) (NP (NNS laws)))))))))))) (. .)))

