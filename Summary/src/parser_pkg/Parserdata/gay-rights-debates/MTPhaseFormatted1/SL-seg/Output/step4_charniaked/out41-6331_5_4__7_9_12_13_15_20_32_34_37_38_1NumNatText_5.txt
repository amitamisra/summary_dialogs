(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NN group) (NNS marriages)) (VP (MD would) (RB not) (VP (VB make) (NP (NN sense)) (PP (IN under) (NP (JJ current) (NN marriage) (NNS laws))) (, ,) (SBAR (IN as) (S (NP (PRP they)) (ADVP (RB only)) (VP (VBP account) (PP (IN for) (NP (NP (CD two) (NNS individuals)) (PP (IN in) (NP (DT a) (JJ legal) (NN marriage))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP state) (SBAR (IN that) (S (NP (NP (NNS opponents)) (PP (IN of) (NP (JJ homosexual) (NN marriage)))) (VP (VBP tend) (S (VP (TO to) (VP (VB argue) (SBAR (IN that) (S (NP (NP (DT a) (NN change)) (PP (TO to) (NP (NN marriage) (NN law)))) (VP (MD would) (VP (VB make) (S (NP (PRP it)) (ADJP (RB too) (JJ open) (SBAR (S (VP (VBD ended))))))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (VP (TO to) (VP (VP (AUX be) (ADJP (JJ true))) (, ,) (CC and) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB broaden) (NP (NP (DT the) (NN scope)) (PP (IN of) (NP (NN marriage)))))))))))) (, ,) (CC but) (ADVP (RB only)) (VP (VB change) (NP (NP (DT the) (NN law)) (SBAR (S (VP (TO to) (VP (AUX be) (ADJP (JJ inclusive) (PP (IN of) (NP (PRP themselves)))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (S (VP (VBG bringing) (PRT (RP up)) (NP (JJ hypothetical) (NNS arguments)) (PP (TO to) (NP (DT the) (NN contrary))))) (VP (AUX is) (NP (DT a) (NN distraction)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT an) (NN effect)) (PP (IN of) (NP (NP (NNS homosexuals)) (VP (AUXG being) (VP (VBN allowed) (S (VP (TO to) (ADVP (RB legally)) (VP (VB marry))))))))) (VP (MD would) (VP (VB lead) (PP (TO to) (NP (NP (ADJP (RB even) (JJR more)) (NNS changes)) (PP (TO to) (NP (NN marriage) (NNS laws))) (SBAR (WHNP (WDT that)) (S (VP (MD would) (VP (VB include) (NP (QP (JJR more) (IN than) (RB simply)) (JJ homosexual) (NNS relationships))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (S (VP (VBG allowing) (NP (JJ homosexual) (NN marriage)))) (VP (AUX is) (NP (NP (DT a) (NN case)) (SBAR (WHNP (WDT that)) (S (VP (VBZ broadens) (NP (NP (NN marriage)) (SBAR (S (CC and) (SBAR (IN if) (S (VP (VBN allowed)))) (, ,) (VP (MD can) (VP (VB open) (NP (NP (NN basis)) (PP (IN for) (NP (JJ future) (NNS arguments)))) (S (VP (TO to) (VP (VB broaden) (NP (DT the) (NN law)) (ADVP (RB further)))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB firmly)) (VP (VBP believe) (SBAR (SBAR (IN that) (S (NP (NP (NN marriage)) (PP (IN as) (NP (DT an) (NN institution)))) (VP (AUX is) (PP (IN between) (NP (NP (DT a) (NN heterosexual)) (ADJP (JJ male) (CC and) (JJ female))))))) (CC and) (SBAR (IN that) (S (S (VP (VBG changing) (NP (NP (DT that)) (PP (IN in) (NP (DT any) (NN way)))))) (VP (VBZ gives) (NP (NP (JJ legal) (NNS grounds)) (SBAR (S (VP (TO to) (VP (VB continue) (S (VP (VBG changing) (NP (PRP it)))))))))))))) (. .)))

