(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asks) (NP (NNP S2)) (SBAR (IN if) (S (NP (PRP he)) (VP (VBZ knows) (SBAR (IN that) (S (NP (DT the) (JJ gay) (NN marriage) (NN debate)) (VP (AUX is) (RB not) (ADJP (JJ synonymous) (PP (IN with) (NP (NP (NN someone)) (VP (VBG condoning) (NP (NP (NN violence)) (PP (IN against) (NP (NNS gays))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ agrees) (SBAR (IN that) (S (NP (NNS gays)) (VP (MD should) (RB not) (VP (VB experience) (NP (NP (NN violence)) (CC and) (NP (NN name-calling)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (IN that) (S (PP (IN on) (NP (NP (NNP Archie) (POS 's)) (NN post))) (NP (DT that) (NNP Archie)) (VP (ADVP (RB regularly)) (VBZ denigrates) (NP (NP (NNS gays)) (PP (IN with) (NP (NP (DT a) (NN variety)) (PP (IN of) (NP (NNS slurs)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ finds) (S (NP (PRP it)) (ADJP (JJ funny)) (SBAR (IN that) (S (NP (DT the) (JJ whole) (NN violence) (NN thing)) (VP (VBZ gets) (VP (VBN used) (PP (IN as) (NP (NP (DT a) (JJ means-to-an-end) (NN tool)) (PP (IN by) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP argue) (PP (IN against) (NP (NP (JJ religious) (NNS people)) (PP (IN on) (NP (DT this)))))))))))))))))) (. .)))

(S1 (S (NP (NNS People)) (VP (MD should) (RB not) (VP (AUX have) (S (VP (TO to) (VP (VB stop) (S (VP (VBG standing) (PRT (RP up)) (PP (IN for) (NP (PRP$ their) (NNS principles))))) (PP (ADVP (RB just)) (IN because) (IN of) (NP (JJ other) (NNS people)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (S (VP (VBG telling) (NP (NNS people)) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (MD will) (VP (VBZ makes) (SBAR (S (NP (PRP$ their) (NNS kids)) (VP (AUX have) (S (VP (TO to) (VP (AUX be) (VP (VBN taught) (SBAR (IN about) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (NP (DT a) (NN threat)) (, ,) (CONJP (RB as) (RB well) (IN as)) (S (VP (VBG telling) (NP (NNS people)) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (MD will) (VP (VB require) (NP (NNS churches)) (S (VP (TO to) (VP (AUX have) (S (VP (TO to) (VP (VB perform) (NP (JJ gay) (NNS marriages))))))))))))))))))))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NNP S1)) (VP (AUX is) (VP (VBG ignoring) (NP (PRP him)))))))) (CC but) (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX is) (RB not) (SBAR (RB rather) (S (NP (NP (NNP S2) (POS 's)) (NN tone)) (VP (AUX has) (VP (VBN changed)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (NN name) (NN calling) (CC and) (NN violence)) (VP (AUX were) (NP (NP (DT the) (NNS things)) (SBAR (S (NP (NNP Jason)) (VP (VBD conflated) (PP (IN with) (S (VP (VBG opposing) (NP (JJ gay) (NN marriage))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ asks) (NP (NNP S1)) (S (VP (TO to) (VP (VB quote) (SBAR (WHNP (RB exactly) (WP what)) (S (NP (PRP he)) (VP (VBD said) (SBAR (IN because) (S (NP (PRP he)) (VP (VBZ thinks) (SBAR (S (NP (NNP Jason)) (VP (AUX did) (NP (DT no) (JJ such) (NN thing))))))))))))))) (. .)))

