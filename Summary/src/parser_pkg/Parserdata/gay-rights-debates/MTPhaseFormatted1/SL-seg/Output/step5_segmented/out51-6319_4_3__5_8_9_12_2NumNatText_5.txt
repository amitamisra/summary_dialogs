<T>
<P>
</P>
<P>
<S>
<C>S1/NNP notes/VBZ a/DT provision/NN in/IN law/NN that/WDT makes/VBZ performing/VBG marriage/NN ceremonies/NNS by/IN sanctioned/VBN marriage/NN officers/NNS non-mandatory/JJ </C>
<C>when/WRB it/PRP comes/VBZ to/TO same/JJ sex/NN couples/NNS and/CC marriage/NN </C>
<C>as/RB long/JJ as/IN performing/VBG said/VBD ceremony/NN would/MD conflict/NN with/IN that/DT person/NN 's/POS conscience/NN ,/, religion/NN and/CC belief/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN forcing/VBG them/PRP to/TO do/AUX so/RB against/IN their/PRP$ beliefs/NNS would/MD infringe/VB on/IN their/PRP$ religious/JJ freedom/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ that/IN it/PRP is/AUX unfortunate/JJ if/IN that/DT applies/VBZ to/TO public/JJ officials/NNS ,/, </C>
<C>as/IN they/PRP believe/VBP that/IN public/JJ officials/NNS must/MD serve/VB the/DT public/NN in/IN its/PRP$ entirety/NN ,/, equally/RB ,/, under/IN the/DT law/NN ./. </C>
</S>
<S>
<C>He/PRP or/CC she/PRP feels/VBZ that/IN if/IN one/CD cannot/NN perform/VB the/DT job/NN ,/, </C>
<C>then/RB that/DT person/NN should/MD look/VB for/IN another/DT job/NN ./. </C>
</S>
<S>
<C>This/DT person/NN believes/VBZ that/IN it/PRP is/AUX an/DT impediment/NN to/TO the/DT public/JJ servant/NN performing/VBG his/PRP$ or/CC her/PRP$ job/NN </C>
<C>if/IN it/PRP 's/AUX true/JJ ./. </C>
</S>
</P>
</T>
