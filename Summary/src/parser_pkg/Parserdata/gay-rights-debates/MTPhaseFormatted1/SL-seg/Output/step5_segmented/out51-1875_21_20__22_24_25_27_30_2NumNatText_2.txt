<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ the/DT two/CD gay/JJ people/NNS living/VBG together/RB should/MD not/RB expect/VB healthcare/NN </C>
<C>since/IN straight/JJ people/NNS that/WDT are/AUX not/RB married/JJ can/MD not/RB get/VB it/PRP ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ gays/NNS can/MD marry/VB members/NNS of/IN the/DT opposite/JJ sex/NN </C>
<C>just/RB as/IN heterosexuals/NNS do/AUX </C>
<C>and/CC heterosexuals/NNS can/MD not/RB marry/VB members/NNS of/IN the/DT same/JJ sex/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ gay/JJ people/NNS have/AUX the/DT same/JJ rights/NNS as/IN heterosexuals/NNS and/CC does/AUX not/RB view/VB marriage/NN as/IN a/DT right/NN ./. </C>
</S>
<S>
<C>Therefore/RB ,/, slavery/NN and/CC women/NNS 's/POS rights/NNS are/AUX bad/JJ comparisons/NNS ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ S1/NNP 's/POS argument/NN is/AUX ridiculous/JJ </C>
<C>and/CC gays/NNS lack/VBP rights/NNS ./. </C>
</S>
<S>
<C>He/PRP says/VBZ gay/JJ people/NNS can/MD not/RB marry/VB who/WP they/PRP love/VBP and/CC compares/VBZ it/PRP to/TO people/NNS protesting/VBG interracial/JJ marriage/NN ,/, slavery/NN ,/, and/CC women/NNS 's/POS rights/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ gay/JJ people/NNS are/AUX in/IN the/DT process/NN to/TO overcome/VB </C>
<C>and/CC they/PRP will/MD win/VB the/DT right/NN to/TO marry/VB and/CC to/TO have/AUX healthcare/NN as/RB well/RB ./. </C>
</S>
</P>
</T>
