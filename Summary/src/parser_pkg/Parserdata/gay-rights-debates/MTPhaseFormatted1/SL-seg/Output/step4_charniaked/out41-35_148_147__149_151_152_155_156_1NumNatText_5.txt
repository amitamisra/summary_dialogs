(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NP (JJS most) (NNS Christians)) (PP (IN in) (NP (DT this) (NN country)))) (VP (VBP oppose) (NP (NP (JJ civil) (NNS marriages)) (PP (IN for) (NP (JJ gay) (NNS rights)))))))) (. .)))

(S1 (S (NP (JJ Evangelical) (NNS Christians)) (VP (VP (AUX have) (VP (AUX been) (ADJP (VBN opposed)))) (CC and) (VP (AUX do) (VP (VB speak) (PP (IN out) (PP (IN against) (NP (JJ gay) (NN marriage))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP go) (PRT (RP on)) (S (VP (TO to) (VP (VB announce) (SBAR (IN that) (S (NP (DT a) (NN state) (JJ constitutional) (NN amendment)) (VP (AUX is) (VP (AUXG being) (VP (VBN passed) (S (VP (TO to) (VP (VB preclude) (NP (NP (JJ same) (NN sex) (NN marriage)) (CC and) (NP (JJ civil) (NNS unions))))))))))))))) (. .)))

(S1 (S (NP (DT That)) (VP (VBZ means) (SBAR (IN that) (S (NP (JJ same) (NN sex) (NNS couples)) (VP (MD will) (VP (AUX be) (VP (VBN seen) (PP (IN in) (NP (DT the) (JJ same) (NN light))) (PP (IN as) (NP (JJ straight) (NNS couples))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ defends) (NP (DT the) (NNS Christians)) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB try) (S (VP (TO to) (VP (JJ discriminate) (PP (IN on) (NP (JJ gay) (NNS marriages)))))))))))))) (, ,) (CC and) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB ban) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (NP (DT the) (NN GUEST)) (VP (VBZ thinks) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ laughable) (S (VP (TO to) (VP (VB think) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (DT that) (VP (VP (VBN united) (PP (IN in) (NP (CD one) (JJ particular) (NN event)))) (CC or) (VP (VB fight) (ADVP (RB together)) (PP (IN for) (NP (NN good)))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP think) (SBAR (IN that) (S (NP (DT the) (NN GUEST)) (VP (AUX has) (VP (VBN blown) (NP (DT the) (JJ whole) (NN conversation)) (PP (IN out) (PP (IN of) (NP (NN proportion))))))))) (. .)))

