(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ concedes) (SBAR (IN that) (S (NP (EX there)) (VP (MD would) (VP (AUX be) (NP (NP (JJ little) (JJ financial) (NN impact)) (PP (IN on) (NP (NP (NN support)) (PP (IN for) (NP (JJ homosexual) (NN marriage)))))))))))) (, ,) (CC and) (S (VP (MD would) (PP (IN in) (NP (NN fact))) (VP (VB supply) (NP (DT a) (NN surplus))))) (. .)))

(S1 (S (ADVP (RB However)) (, ,) (NP (DT this) (NN person)) (ADVP (RB still)) (VP (VBZ desires) (NP (NP (DT a) (NN debate)) (PP (IN on) (NP (JJ moral) (CC and) (NN health) (NNS concerns))) (PP (IN of) (NP (DT the) (NN issue))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB view) (NP (PRP themselves)) (PP (IN as) (NP (NP (NN someone)) (SBAR (WHNP (WP who)) (S (VP (VP (VBZ imposes) (NP (PRP$ their) (JJ moral) (NNS beliefs)) (PP (IN on) (NP (NNS others)))) (, ,) (CC and) (VP (AUX is) (ADJP (JJ willing) (S (VP (TO to) (VP (VB let) (S (NP (NN society)) (VP (VB determine) (NP (PRP$ its) (JJ own) (NNS limits)))))))))))))))) (. .)))

(S1 (S (SBAR (IN While) (S (NP (PRP they)) (ADVP (RB vocally)) (VP (VBP defend) (NP (NP (DT the) (NNP United) (NNP States) (NNP Constitution)) (ADJP (RB as) (VBN written)))))) (, ,) (NP (PRP they)) (ADVP (RB also)) (VP (VBP desire) (S (VP (TO to) (VP (AUX be) (ADJP (ADJP (RBR more) (JJ flexible)) (PP (IN with) (NP (NP (JJ public) (VBN determined) (NN acceptance)) (PP (IN of) (NP (NN behavior)))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (MD would) (VP (VB like) (S (VP (TO to) (VP (VB see) (NP (DT this) (NN concession)) (PP (IN as) (NP (NP (DT a) (NN means)) (SBAR (S (VP (TO to) (VP (VB lead) (PP (TO to) (NP (NP (DT a) (NN discussion)) (PP (IN on) (NP (NP (DT the) (JJ societal) (NNS benefits)) (PP (IN of) (NP (JJ same-sex) (NN marriage))))))))))))) (, ,) (ADVP (RB however)))))))) (, ,) (NP (PRP they)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG attempting) (S (VP (TO to) (VP (VB hide) (PP (IN behind) (NP (PRP$ their) (JJ moral) (NNS grounds))) (SBAR (IN because) (S (NP (NNP S1)) (VP (VBZ sees) (NP (PRP it)) (PP (IN as) (ADJP (JJ relative) (CC and) (JJ subjective)))))))))))))) (. .)))

