<s> <PARAGRAPH> </s>
<s>  S1 believes that gay marriage will hurt more people than it helps, and that the benefits deserved by homosexuals can be had without redefining marriage. </s>
<s> They believe that it hurts people who view marriage as a sacred bond between a man and a woman. </s>
<s> They believe that homosexuals are entitled to their rights, but do not need to redefine marriage in order to get them. </s>
<s> S2 does not believe that offending one group of individuals counts as damage, but they do believe that appeasing faith justified hate does. </s>
<s> They do not believe people are interested in giving homosexuals marriage rights under a different name, and also do not believe people wish to grant them any rights at all. </s>
<s> They believe separate rules will be too easy to manipulate. </s>
<s>  </s>
