<s> <PARAGRAPH> </s>
<s>  Two people are discussing the spread of AIDS in the US as part of a discussion on gay marriage in the USA. </s>
<s> S1 states that location is relevant, as the way AIDS spread in Africa is different from how it spreads in the US. </s>
<s> He continues to state that AIDS spread in the US because of the gay community, and the gay community continually tries to deflect the argument and include Africa in the discussion instead of providing real facts. </s>
<s> S2 argues that bringing up the topic of AIDS is solely as a weapon against gays, and that the location of the AIDS epidemic is irrelevant to the discussion. </s>
<s> He then states that Africa is included in order to refute the opposing logic. </s>
<s>  </s>
