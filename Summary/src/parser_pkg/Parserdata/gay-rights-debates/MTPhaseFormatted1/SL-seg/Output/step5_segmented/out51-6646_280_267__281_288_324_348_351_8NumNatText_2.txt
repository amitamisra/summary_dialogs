<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ he/PRP and/CC his/PRP$ family/NN represents/VBZ what/WP is/AUX normal/RB ./. </C>
</S>
<S>
<C>Homosexuality/NN in/IN his/PRP$ opinion/NN is/AUX a/DT deviation/NN from/IN the/DT norm/NN and/CC is/AUX perverse/JJ ./. </C>
</S>
<S>
<C>He/PRP rejects/VBZ the/DT idea/NN that/IN he/PRP hates/VBZ gay/JJ people/NNS and/CC says/VBZ he/PRP simply/RB does/AUX not/RB think/VB we/PRP should/MD redefine/VB marriage/NN for/IN them/PRP ./. </C>
</S>
<S>
<C>He/PRP states/VBZ S2/NNP may/MD believe/VB </C>
<C>as/IN wishes/VBZ </C>
<C>even/RB if/IN it/PRP is/AUX wrong/JJ ./. </C>
</S>
<S>
<C>S2/NNP questions/VBZ what/WP is/AUX perverse/JJ ./. </C>
</S>
<S>
<C>He/PRP equates/VBZ the/DT word/NN perverse/JJ with/IN diversity/NN ./. </C>
</S>
<S>
<C>A/DT deviation/NN for/IN the/DT norm/NN in/IN his/PRP$ opinion/NN is/AUX diversity/NN ./. </C>
</S>
<S>
<C>He/PRP doubts/VBZ S1/NNP is/AUX against/IN diversity/NN </C>
<C>so/IN he/PRP thinks/VBZ he/PRP simply/RB hates/VBZ a/DT form/NN of/IN it/PRP ,/, gay/JJ people/NNS ./. </C>
</S>
<S>
<C>He/PRP views/VBZ calling/VBG something/NN perverse/JJ ad/NN evidence/NN of/IN this/DT hate/NN and/CC does/AUX not/RB believe/VB S1/NNP when/WRB he/PRP says/VBZ that/IN he/PRP does/AUX not/RB hate/VB gays/RB he/PRP is/AUX simply/RB worried/VBN about/IN marriage/NN ./. </C>
</S>
</P>
</T>
