<s> <PARAGRAPH> </s>
<s>  S1 is not bothered by the inconsistency of one supporting gay rights but not gay marriage, but if one supports gay marriage they would be bothered by it. </s>
<s> S2 does not see the inconsistency and does not understand why S1 keeps calling it that. </s>
<s> He says that S1 has not shown an inconsistency yet and gives an example of how it is like saying "Frank is inconsistent on the abortion issue", but would be showing inconsistency if is Frank said "I am against all abortion". </s>
<s> S1 does not know what else to say and that it would be different if S2 did not think gay marriage would be a right. </s>
<s> S2 argues that S1s' position is inconsistent and that S2 can see a person for gay rights and against gay marriage. </s>
<s> He thinks Biden is wrong but his view is consistent. </s>
<s> S1 says he does not care about the inconsistency of one supporting gay rights and not gay marriage. </s>
<s> S2 says to push hard on Biden about the "gay marriage opinion" a few weeks away from election day and balance the response. </s>
<s>  </s>
