(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN spread)) (PP (IN of) (NP (NNP AIDS)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT no) (NN one)) (VP (VBZ knows) (SBAR (WHADVP (WRB how)) (S (NP (NNP AIDS)) (VP (VBD got) (PP (TO to) (NP (DT the) (PRP US)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBD made) (S (NP (NNP AIDS)) (NP (DT a) (JJ major) (NN health))))))) (VP (AUX are) (NP (NP (NP (DT the) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX did) (RB not) (VP (VB clean) (NP (PRP$ their) (NNS needles)) (SBAR (IN while) (S (VP (VBG doing) (NP (NNS drugs)))))))))) (, ,) (NP (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX do) (RB not) (VP (VB practice) (NP (JJ safe) (NN sex))))))) (, ,) (CC and) (NP (NP (DT those)) (VP (VBG giving) (NP (NN blood) (NNS transfusions)) (SBAR (IN before) (S (NP (PRP it)) (VP (AUX was) (NP (NP (DT a) (JJ known) (NN form)) (PP (IN of) (NP (NN transmission))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ rebuts) (, ,) (S (VP (VBG asking) (NP (NNP S2)) (S (VP (TO to) (VP (VB provide) (NP (NP (NN evidence)) (SBAR (S (VP (TO to) (VP (VB back) (PRT (RP up)) (NP (PRP$ his) (NN claim))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ quotes) (NP (NP (DT a) (JJ recent) (NN post)) (PP (IN from) (NP (NNP S1))) (, ,) (VP (VBG stating) (SBAR (IN that) (S (NP (DT any) (NN debate)) (VP (MD will) (VP (VP (VB start) (SBAR (WHADVP (WRB when)) (S (NP (NNP S1)) (VP (VBZ owns) (PRT (RP up)) (PP (TO to) (NP (PRP$ his) (JJ own) (NNS requirements))))))) (CC and) (VP (VB defend) (NP (PRP$ his) (NNS statements)))))))) (, ,) (SBAR (WHPP (TO to) (WHNP (WDT which))) (S (NP (NNP S1)) (VP (VBZ guaranteers) (SBAR (IN that) (S (NP (PRP he)) (VP (MD can) (VP (VB prove) (NP (PRP$ his) (NNS assertions)) (, ,) (ADVP (ADVP (RB as) (RB long)) (SBAR (IN as) (S (NP (NNP S2)) (VP (VBZ accepts) (NP (NP (PRP$ his) (NN challenge)) (PP (TO to) (NP (DT a) (NN debate))))))))))))))))) (. .)))

