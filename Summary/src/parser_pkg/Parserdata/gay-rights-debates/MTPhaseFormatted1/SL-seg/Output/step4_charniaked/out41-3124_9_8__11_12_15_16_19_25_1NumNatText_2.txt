(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (SBAR (IN whether) (CC or) (RB not) (S (NP (DT the) (JJ civil) (NNS rights) (NNS activists)) (VP (AUX are) (PP (IN for) (NP (JJ gay) (NN marriage)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT the) (NN comparison)) (PP (IN of) (NP (NP (DT a) (NN race)) (PP (IN of) (NP (NP (NP (NNS people) (POS 's)) (NN fight)) (PP (IN for) (NP (NP (JJ equal) (NNS rights)) (CC and) (NP (NP (DT a) (NN class)) (PP (IN of) (NP (NP (NNS people)) (VP (VBN determined) (PP (IN by) (NP (PRP$ their) (JJ sexual) (NN orientation)))))))))))))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN made) (, ,) (SBAR (IN as) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT an) (NN insult)) (PP (TO to) (NP (DT all) (NNP African) (NNPS Americans))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ wonders) (SBAR (WHADVP (WRB how)) (S (NP (NNP Rev.) (NNP MLK)) (VP (MD would) (VP (VB feel) (PP (IN about) (NP (DT the) (JJ gay) (NNS rights) (NN issue)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG saying) (SBAR (IN that) (S (NP (NP (NNP Coretta) (NNP Scott) (NNP King)) (CC and) (NP (NNP Julian) (NNP Bond))) (VP (AUX are) (DT both) (PP (IN for) (NP (JJ gay) (NN marriage)))))))))) (. .)))

