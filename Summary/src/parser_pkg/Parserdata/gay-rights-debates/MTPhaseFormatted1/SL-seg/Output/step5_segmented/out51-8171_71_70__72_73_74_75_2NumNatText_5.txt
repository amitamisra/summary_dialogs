<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX not/RB bothered/VBN by/IN the/DT inconsistency/NN of/IN one/CD supporting/VBG gay/JJ rights/NNS but/CC not/RB gay/JJ marriage/NN ,/, </C>
<C>but/CC if/IN one/PRP supports/VBZ gay/JJ marriage/NN </C>
<C>they/PRP would/MD be/AUX bothered/VBN by/IN it/PRP ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB see/VB the/DT inconsistency/NN and/CC does/AUX not/RB understand/VB why/WRB S1/NNP keeps/VBZ calling/VBG it/PRP that/IN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN S1/NNP has/AUX not/RB shown/VBN an/DT inconsistency/NN yet/RB and/CC gives/VBZ an/DT example/NN of/IN how/WRB it/PRP is/AUX like/IN saying/VBG ``/`` Frank/NNP is/AUX inconsistent/JJ on/IN the/DT abortion/NN issue/NN ''/'' ,/, </C>
<C>but/CC would/MD be/AUX showing/VBG inconsistency/NN </C>
<C>if/IN is/AUX Frank/NNP said/VBD ``/`` I/PRP am/AUX against/IN all/DT abortion/NN ''/'' ./. </C>
</S>
<S>
<C>S1/NNP does/AUX not/RB know/VB what/WP else/RB to/TO say/VB and/CC that/IN it/PRP would/MD be/AUX different/JJ if/IN S2/NNP did/AUX not/RB think/VB gay/JJ marriage/NN would/MD be/AUX a/DT right/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN S1s/NNP '/POS position/NN is/AUX inconsistent/JJ </C>
<C>and/CC that/IN S2/NNP can/MD see/VB a/DT person/NN for/IN gay/JJ rights/NNS and/CC against/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ Biden/NNP is/AUX wrong/JJ </C>
<C>but/CC his/PRP$ view/NN is/AUX consistent/JJ ./. </C>
</S>
<S>
<C>S1/NNP says/VBZ he/PRP does/AUX not/RB care/VB about/IN the/DT inconsistency/NN of/IN one/CD supporting/VBG gay/JJ rights/NNS and/CC not/RB gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ to/TO push/VB hard/RB on/IN Biden/NNP about/IN the/DT ``/`` gay/JJ marriage/NN opinion/NN ''/'' a/DT few/JJ weeks/NNS away/RB from/IN election/NN day/NN and/CC balance/VB the/DT response/NN ./. </C>
</S>
</P>
</T>
