<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN gay/JJ marriage/NN will/MD hurt/VB more/JJR people/NNS than/IN it/PRP helps/VBZ ,/, </C>
<C>and/CC that/IN the/DT benefits/NNS deserved/VBD by/IN homosexuals/NNS can/MD be/AUX had/AUX without/IN redefining/VBG marriage/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN it/PRP hurts/VBZ people/NNS who/WP view/VBP marriage/NN as/IN a/DT sacred/JJ bond/NN between/IN a/DT man/NN and/CC a/DT woman/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN homosexuals/NNS are/AUX entitled/VBN to/TO their/PRP$ rights/NNS ,/, </C>
<C>but/CC do/AUX not/RB need/AUX to/TO redefine/VB marriage/NN </C>
<C>in/IN order/NN to/TO get/VB them/PRP ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB that/IN offending/VBG one/CD group/NN of/IN individuals/NNS counts/NNS as/IN damage/NN ,/, </C>
<C>but/CC they/PRP do/AUX believe/VB that/IN appeasing/VBG faith/NN justified/JJ hate/NN does/AUX ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB people/NNS are/AUX interested/JJ in/IN giving/VBG homosexuals/NNS marriage/NN rights/NNS under/IN a/DT different/JJ name/NN ,/, </C>
<C>and/CC also/RB do/AUX not/RB believe/VB people/NNS wish/VBP to/TO grant/VB them/PRP any/DT rights/NNS at/IN all/DT ./. </C>
</S>
<S>
<C>They/PRP believe/VBP separate/JJ rules/NNS will/MD be/AUX too/RB easy/JJ to/TO manipulate/VB ./. </C>
</S>
</P>
</T>
