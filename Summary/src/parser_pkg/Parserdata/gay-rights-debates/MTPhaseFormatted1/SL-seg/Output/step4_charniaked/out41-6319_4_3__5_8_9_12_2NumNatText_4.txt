(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBD said) (SBAR (IN that) (S (PP (IN in) (NP (NNP South) (NNP Africa))) (NP (PRP it)) (VP (VBZ seems) (SBAR (IN as) (IN if) (S (NP (NN marriage) (NN freedom)) (VP (AUX does) (RB not) (NP (ADJP (NNP trump) (JJ religious)) (NN freedom)) (SBAR (IN as) (S (NP (JJ public) (NNS officials)) (VP (AUX do) (RB not) (VP (AUX have) (S (VP (TO to) (VP (VB perform) (NP (DT a) (JJ gay) (NN marriage) (NN ceremony)) (SBAR (IN if) (S (NP (PRP they)) (VP (VBP believe) (SBAR (IN that) (S (S (VP (VBG doing) (ADVP (RB so)))) (VP (MD would) (VP (AUX be) (PP (IN against) (NP (PRP$ their) (NNS values)))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (JJ public) (NN official)) (VP (AUX did) (RB not) (VP (VB want) (S (VP (TO to) (VP (VB marry) (NP (PRP him)) (PP (IN in) (NP (DT a) (JJ heterosexual) (NN marriage)))))))))) (NP (PRP he)) (VP (MD would) (VP (VB want) (S (VP (TO to) (VP (VB go) (ADVP (RB elsewhere)) (PP (RB rather) (IN than) (VP (VB force) (S (NP (PRP him)) (VP (TO to) (VP (AUX do) (NP (PRP it))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD said) (SBAR (IN that) (S (NP (JJ public) (NNS officials)) (VP (MD should) (VP (VB serve) (NP (DT the) (NN public)) (PP (ADVP (RB equally)) (IN under) (NP (DT the) (NN law))) (SBAR (IN as) (S (NP (DT that)) (VP (AUX is) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (AUX are) (VP (VBN supposed) (S (VP (TO to) (VP (AUX do)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (NP (PRP they)) (VP (MD should) (VP (AUX have) (S (VP (TO to) (VP (VB look) (PP (IN for) (NP (DT another) (NN job))) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB want) (S (VP (TO to) (VP (AUX do) (NP (PRP$ their) (NNS jobs))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD expressed) (NP (PRP$ his) (NN gladness)) (PP (IN at) (S (VP (AUXG being) (PP (IN in) (NP (NP (NNP Canada)) (SBAR (WHADVP (WRB where)) (S (NP (EX there)) (VP (AUX are) (NP (JJR more) (NNS rights))))))))))) (. .)))

