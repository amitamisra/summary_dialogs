<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG anti-gay/JJ bias/NN and/CC the/DT nature/NN of/IN prejudice/NN versus/CC valid/JJ religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX prejudicial/JJ to/TO hold/VB anti-gay/JJ opinions/NNS ,/, whether/IN based/VBN on/IN religious/JJ values/NNS or/CC otherwise/RB ,/, </C>
<C>and/CC use/VB them/PRP </C>
<C>to/TO impose/VB ones/NNS views/NNS on/IN another/DT ./. </C>
</S>
<S>
<C>S2/NNP agrees/VBZ that/IN religious/JJ belief/NN can/MD be/AUX improperly/RB used/VBN as/IN the/DT pretext/NN for/IN acts/NNS of/IN prejudice/NN against/IN the/DT gay/JJ community/NN ,/, </C>
<C>but/CC proposes/VBZ that/IN only/JJ people/NNS who/WP are/AUX lying/VBG about/IN having/AUXG religious/JJ values/NNS against/IN homosexuality/NN should/MD be/AUX accused/VBN of/IN being/AUXG prejudiced/VBN ./. </C>
</S>
<S>
<C>S2/NNP asserts/VBZ vaguely/RB that/IN her/PRP$ belief/NN that/IN ``/`` Christ/NNP is/AUX a/DT resurrected/VBN being/NN ''/'' somehow/RB absolves/VBZ her/PRP of/IN prejudice/NN in/IN being/AUXG generally/RB opposed/VBN to/TO homosexuality/NN ./. </C>
</S>
<S>
<C>S1/NNP seems/VBZ inclined/VBN to/TO grant/VB her/PRP$ leeway/NN based/VBN on/IN such/PDT a/DT religious/JJ objection/NN ,/, but/CC requests/NNS more/JJR specifics/NNS as/IN to/TO why/WRB a/DT Christian/NNP must/MD oppose/VB homosexuality/NN and/CC admonishes/NNS S1/NNP not/RB to/TO actively/RB agitate/VB against/IN the/DT gay/JJ community/NN or/CC advocate/NN for/IN public/JJ policies/NNS which/WDT are/AUX discriminatory/JJ ./. </C>
</S>
<S>
<C>S1/NNP is/AUX opposed/VBN to/TO groups/NNS such/JJ as/IN ``/`` Focus/VB on/IN the/DT Family/NNP ''/'' 
<M>-LRB-/-LRB- may/MD be/AUX a/DT misnomer/NN -RRB-/-RRB- </M>
who/WP attempt/VBP to/TO impose/VB their/PRP$ views/NNS on/IN others/NNS ./. </C>
</S>
</P>
</T>
