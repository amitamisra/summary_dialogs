<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN gays/NNS do/AUX not/RB have/AUX the/DT right/NN to/TO shared/VBN health/NN care/NN </C>
<C>because/IN they/PRP 're/AUX not/RB married/JJ ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN gays/NNS do/AUX have/AUX the/DT right/NN to/TO marry/VB members/NNS of/IN the/DT opposite/JJ sex/NN ,/, but/CC not/RB the/DT same/JJ sex/NN ,/, </C>
<C>which/WDT is/AUX the/DT same/JJ has/AUX heterosexuals/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN there/EX will/MD never/RB be/AUX a/DT federal/JJ law/NN allowing/VBG gay/JJ marriages/NNS in/IN the/DT US/NNP ,/, </C>
<C>as/IN there/EX is/AUX allot/JJ of/IN backlash/NN going/VBG on/RP lately/RB ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN gays/NNS do/AUX not/RB have/AUX the/DT right/NN to/TO marry/VB who/WP they/PRP love/VBP ,/, </C>
<C>which/WDT is/AUX what/WP they/PRP are/AUX fighting/VBG for/IN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN they/PRP will/MD win/VB ,/, just/RB as/IN what/WP happened/VBD with/IN slavery/NN and/CC women/NNS voting/NN ,/, </C>
<C>to/TO which/WDT S1/NNP states/VBZ where/WRB entirely/RB different/JJ issues/NNS ./. </C>
</S>
</P>
</T>
