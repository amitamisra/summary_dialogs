(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-) (NN Marriage)) (VP (VP (AUX is) (ADJP (JJ meaningful))) (CC and) (VP (MD should) (RB not) (VP (AUX be) (NP (NP (DT a) (NN concern)) (PP (IN of) (NP (DT the) (NN government))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN marriage)) (VP (VP (AUX is) (NP (NP (DT an) (JJ ancient) (JJ human) (NN institution)) (, ,) (ADJP (JJ natural) (PP (IN for) (NP (NNS humans)))))) (CC and) (VP (AUX is) (NP (NP (CD one)) (PP (IN of) (NP (NP (DT the) (NNS foundations)) (PP (IN of) (NP (JJ social) (NN stability))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS Christians)) (VP (AUX are) (VP (VBN involved) (PP (IN in) (NP (DT the) (NN topic))) (SBAR (IN because) (S (NP (PRP they)) (VP (VBP want) (S (NP (DT the) (NN government)) (VP (TO to) (VP (`` ``) (VB get) (ADVP (RB out) (PP (IN of) (NP (NN marriage)))) ('' '') (SBAR (IN since) (S (NP (JJ gay) (NNS people)) (VP (AUX are) (VP (VBG becoming) (ADJP (VBN involved))))))))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS Christians)) (VP (AUX are) (NP (NNS hypocrites)) (SBAR (RB because) (IN before) (S (NP (PRP they)) (VP (AUX had) (RB not) (VP (VBN complained))))))))) (. .)))

(S1 (S (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (VP (VBZ states) (NP (NP (DT the) (NN importance)) (PP (IN for) (NP (NN government)))) (S (VP (TO to) (VP (VB get) (VP (VBN involved) (ADVP (RB however)))))))) (, ,) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ convenient)))) (CC and) (S (VP (VBZ mentions) (NP (NNP Mr.) (NNP WriteLA)) (PP (IN as) (NP (NP (NN someone)) (SBAR (WHNP (WP who)) (S (VP (AUX has) (VP (ADVP (RB eloquently)) (VBN talked) (PP (IN about) (NP (DT the) (NN topic))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-) (NN government)) (VP (VBZ desires) (S (VP (TO to) (VP (AUX be) (VP (VBN involved) (PP (IN in) (NP (NNS lives)))))))) (. .)))

(S1 (FRAG (NP (NP (NNS Talks)) (PP (IN about) (NP (DT the) (`` ``) (JJ Healthy) (NN Marriage) (NN Initiative) ('' '')))) (CC and) (SBAR (WHADVP (WRB how)) (S (NP (DT some) (JJ mean-spirited) (NNS hypocrites)) (VP (VBP want) (NP (DT the) (NN government)) (PRT (RP out)) (PP (IN for) (NP (DT the) (JJ wrong) (NNS reasons)))))) (. .)))

(S1 (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (NP (NNS Questions)) (SBARQ (WHADVP (WRB why)) (SQ (AUX is) (NP (PRP it)) (PP (IN in) (NP (NP (NNS societies) (NN interest)) (PP (IN in) (S (VP (VBG preventing) (S (NP (DT the) (NN government)) (VP (TO to) (VP (AUX be) (VP (VBN involved) (PP (IN in) (NP (NN marriage))))))))))))) (. .))))

(S1 (SINV (ADVP (RB Also)) (VP (VBZ mentions)) (NP (NP (NNP Mr.) (NNP WriteLA)) (SBAR (WHNP (WP who)) (S (VP (VBD said) (SBAR (IN that) (S (NP (NP (DT the) (NN government) (POS 's)) (NN involvement)) (VP (AUX is) (ADJP (JJ inevitable))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (MD should) (VP (VB get) (ADVP (RP out) (PP (IN of) (NP (DT the) (NN marriage) (NN business)))) (SBAR (IN because) (S (S (NP (PRP it)) (VP (AUX is) (RB not) (VP (VBN needed)))) (CC and) (S (NP (NP (DT the) (NNS benefits)) (PP (IN of) (NP (NN marriage)))) (VP (AUX are) (ADJP (RB terribly) (JJ unfair) (PP (IN for) (NP (JJ single) (NNS people))))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (S (NP (JJ single) (NNS people)) (VP (AUX do) (RB not) (VP (AUX have) (NP (NP (JJ same) (NNS issues)) (PP (IN as) (NP (NP (JJ married) (NNS people)) (PP (IN from) (NP (NN experience))))))))))) (. .)))

