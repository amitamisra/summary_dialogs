(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NP (NNS statistics)) (PP (IN on) (NP (NN divorce) (NNS rates)))) (VP (AUX are) (VP (VBN skewed) (PP (IN by) (NP (NP (DT the) (JJ high) (NN proportion)) (PP (IN of) (NP (NP (JJ young) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP end) (PRT (RP up)) (S (VP (VBG getting) (NP (NNS divorces)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (DT the) (NN divorce) (NN rate)) (VP (VBZ drops) (ADVP (RB significantly)) (SBAR (IN if) (S (NP (PRP you)) (VP (VBP focus) (ADVP (RB only)) (PP (IN on) (NP (NP (NNS couples)) (SBAR (WHNP (WP who)) (S (VP (AUX have) (VP (AUX been) (ADJP (ADJP (NN marriage) (JJR more)) (PP (IN than) (NP (CD five) (NNS years))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNS statistics)) (VP (VBP show) (SBAR (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (ADJP (RBR more) (JJ promiscuous) (PP (IN than) (NP (NNS heterosexuals))))))) (CC and) (SBAR (IN that) (S (NP (DT this)) (VP (MD would) (VP (VB lead) (PP (TO to) (NP (NP (DT a) (JJR higher) (NN divorce) (NN rate)) (PP (IN for) (NP (JJ same-sex) (NNS marriages)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (NP (DT the) (NN age)) (PP (IN of) (NP (DT the) (NNS spouses)))) (VP (AUX is) (ADJP (JJ irrelevant)))) (CC and) (S (NP (NP (DT that) (QP (JJR more) (IN than) (PDT half))) (PP (IN of) (NP (DT all) (JJ heterosexual) (NNS marriages)))) (VP (VP (VBP end) (PP (IN in) (NP (NN divorce)))) (CC and) (VP (VBZ suggests) (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (ADVP (RB likely)) (VP (AUX be) (NP (NP (DT no) (NN difference)) (PP (IN for) (NP (JJ same-sex) (NNS marriages)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ rejects) (NP (NP (DT the) (NN comparison)) (PP (IN of) (NP (NN promiscuity))) (PP (IN between) (NP (NNS heterosexuals) (CC and) (NNS homosexuals)))) (PP (IN by) (S (VP (VBG pointing) (PRT (RP out)) (SBAR (IN that) (S (NP (JJ only) (NNS heterosexuals)) (VP (AUX are) (ADVP (RB legally)) (VP (VBN allowed) (S (VP (TO to) (VP (VB marry)))))))))))) (. .)))

