(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (-LRB- -LRB-) (NP (NNP S1)) (-RRB- -RRB-) (VP (VBZ begins) (VP (VBG talking) (PP (IN about) (SBAR (SBAR (WHADVP (WRB how)) (S (NP (PRP we)) (VP (VBP benefit) (PP (IN from) (SBAR (WHNP (WP what)) (S (VP (AUX is) (VP (VBN considered) (S (ADJP (JJ common))) (, ,) (PP (JJ such) (IN as) (NP (DT a) (JJ nuclear) (NN family))))))))))) (CC and) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (AUX is) (VP (VBN composed))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ right)) (SBAR (S (S (NP (PRP it)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (DT the) (JJ bible)) (ADVP (RB however))))))))))) (: ;) (S (NP (PRP it)) (VP (AUX has) (NP (NP (NN everything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (JJ slippery) (NN slop))) (NP (JJ logical) (NN fallacy))))))))))))))) (. .)))

(S1 (SINV (ADVP (RB Then)) (, ,) (VP (VBZ gives)) (NP (NP (DT the) (NN example)) (PP (IN of) (SBAR (WHADVP (WRB how)) (S (NP (NN incest) (CC and) (NN polygamy)) (VP (AUX is) (ADJP (RBR less) (RB socially) (JJ accepted) (PP (IN in) (NP (NP (NN comparison)) (PP (TO to) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (CC And) (ADVP (RB so)) (, ,) (NP (PRP it)) (VP (VBZ rebuttals) (NP (NP (DT the) (NN argument)) (VP (VBG using) (NP (DT both) (NNS issues)))) (PP (IN with) (NP (JJ equal) (NN weight))) (SBAR (IN because) (S (NP (PRP it)) (VP (VBZ scares) (NP (NP (DT some) (NNS people)) (SBAR (WHNP (RB away) (WP who)) (S (VP (MD might) (RB not) (VP (AUX have) (NP (JJ much) (NN knowledge)) (PP (IN about) (NP (DT the) (NNS topics)))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT a) (JJ married) (NN couple)) (VP (AUX is) (VP (VBN considered) (UCP (NP (DT a) (JJ nuclear) (NN family)) (CC and) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX is) (NP (DT a) (JJ legal) (NN institution))))))))))) (. .)))

(S1 (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (VP (VBZ poses) (NP (NP (NP (DT the) (NN question)) (PP (IN of) (NP (NP (NN weather)) (SBAR (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ acceptable) (PP (IN for) (NP (JJ gay) (NN marriage)))) (S (VP (TO to) (VP (AUX be) (VP (VBN accepted))))))))))) (CONJP (CC but) (RB not)) (NP (DT the) (JJ other))) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (UCP (ADJP (RBR less) (JJ popular)) (CC and) (IN if) (CC so) (WHADVP (WRB why))))))) (. ?)))

(S1 (S (-LRB- -LRB-) (NP (NNP S1)) (-RRB- -RRB-) (VP (VBZ answers) (SBAR (IN that) (S (NP (DT those) (NNS issues)) (VP (AUX do) (RB not) (VP (VB relate) (ADVP (RB specifically)) (PP (TO to) (NP (NP (JJ gay) (JJ legal) (NN marriage)) (CC and) (NP (DT that) (JJ many) (NN attempt) (S (VP (TO to) (VP (VB use) (NP (DT those) (NNS issues)) (S (ADVP (RB similarly)) (VP (TO to) (VP (VB argue) (PP (IN against) (NP (JJ gay) (NN marriage))))))))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ brings) (PRT (RP up)) (NP (NP (NP (DT the) (NN question)) (PP (IN of) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (RB socially) (JJ accepted))))))) (CC and) (NP (NP (DT the) (NNS types)) (PP (IN of) (NP (NP (JJ taboo) (NN relationship)) (CC and) (NP (NN legality))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (VP (VBZ says) (SBAR (IN that) (S (NP (NN love)) (VP (AUX is) (RB not) (ADJP (RB exceptionally) (PP (IN for) (NP (NP (JJ gay) (NNS people)) (CONJP (CC but) (RB also)) (NP (NNS others))))))))) (. .)))

(S1 (FRAG (ADVP (RB Also)) (SBAR (IN that) (S (NP (NNS taboos)) (VP (AUX are) (VP (VBN constructed) (PP (IN on) (NP (NP (DT the) (NN basis)) (PP (IN of) (NP (NP (NNS peoples) (NN reasoning)) (, ,) (NP (NNS indications)) (CC and) (NP (NNS practices)))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ keeps) (S (VP (VBG arguing) (SBAR (WHADVP (WRB why)) (S (VP (AUX is) (NP (NP (JJ anestrous) (NN taboo)) (CC and) (RB not) (NP (JJ gay) (NN marriage))))))))) (. ?)))

