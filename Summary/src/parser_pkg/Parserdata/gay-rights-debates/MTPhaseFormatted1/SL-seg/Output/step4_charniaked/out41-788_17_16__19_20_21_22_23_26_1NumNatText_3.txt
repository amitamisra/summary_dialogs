(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (CD two) (NNS men)) (PP (IN in) (NP (DT a) (JJ monogamous) (JJ homosexual) (NN relationship)))) (VP (AUX is) (ADJP (RBR more) (JJ normative) (PP (IN than) (NP (NP (DT a) (NN man)) (PP (IN in) (NP (NP (DT a) (JJ heterosexual) (NN relationship)) (PP (IN with) (NP (JJ multiple) (NNS women))))))))))) (, ,) (S (VP (VBG citing) (NP (NNP fidelity)) (PP (IN as) (NP (NP (DT a) (JJ normative) (NN attribute)) (PP (IN of) (NP (NP (NNS relationships)) (PP (IN in) (NP (NNP America)))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (NN response)) (PP (TO to) (NP (NP (DT the) (NN poll)) (VP (VBN cited) (PP (IN by) (NP (NNP S2)))))))) (, ,) (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (NNS opinions)) (VP (VBG regarding) (NP (JJ homosexual) (NNS marriages)))) (VP (VBP extend) (PP (IN beyond) (NP (JJ political) (NN party) (NNS lines))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ cites) (NP (NP (DT a) (NNP Harris) (NNP Interactive) (NN poll)) (PP (IN from) (NP (CD 2003))) (VP (VBG showing) (SBAR (IN that) (S (NP (NP (CD 62) (NN %)) (PP (IN of) (NP (NNPS Americans)))) (VP (AUX did) (RB not) (VP (VB think) (SBAR (S (NP (JJ homosexual) (NNS marriages)) (VP (MD should) (VP (AUX be) (VP (VBN recognized) (PP (IN as) (ADJP (JJ legal))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (NNS advocates)) (PP (IN of) (NP (JJ homosexual) (NN marriage)))) (VP (AUX are) (NP (NP (NNS heterophobes)) (VP (VBG trying) (S (VP (TO to) (VP (VB force) (NP (JJ homosexual) (NN marriage)) (PP (IN onto) (NP (NNP America)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NNS people)) (VP (AUX have) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB support) (NP (JJ homosexual) (NN marriage))))))))) (, ,) (NP (RB then) (JJ other) (NNS people)) (VP (AUX have) (NP (RB just) (ADJP (RB as) (JJ much)) (NN right)) (S (VP (TO to) (VP (VB oppose) (NP (NP (JJ gay) (NN marriage)) (PP (JJ due) (TO to) (NP (NP (DT the) (NNS freedoms)) (VP (VBN described) (PP (IN in) (NP (DT the) (NNP US) (NNP Constitution)))))))))))))) (. .)))

