(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP healthcare)) (VP (VBZ applies) (PP (TO to) (NP (NNS spouses))) (, ,) (SBAR (IN so) (S (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ illegal)) (SBAR (IN for) (S (NP (JJ gay) (NNS people)) (VP (TO to) (VP (VB get) (VP (VBN married) (PP (IN in) (NP (NNP VA))))))))))) (NP (PRP they)) (VP (MD can) (RB not) (VP (VB share) (NP (NN healthcare)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ goes) (ADVP (RB on)) (S (VP (TO to) (VP (VB say) (SBAR (S (NP (PRP it)) (VP (VBZ works) (NP (DT the) (JJ same) (NN way)) (SBAR (IN if) (S (NP (DT a) (JJ straight) (NN man)) (VP (AUX has) (NP (DT a) (NN girlfriend)))))))))))) (. .)))

(S1 (S (NP (PRP She)) (VP (MD can) (RB not) (VP (VB get) (NP (NN healthcare)) (PP (IN through) (NP (PRP$ his) (NN job))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ hopes) (SBAR (SBAR (S (NP (NP (JJ gay) (NNS people)) (VP (VBN upset) (PP (IN by) (NP (NNP VA))))) (VP (VBP leave)))) (CC and) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (VBZ gets) (VP (VBN outlawed) (ADVP (RB everywhere)) (SBAR (IN so) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (AUX have) (S (VP (TO to) (VP (VB hear) (S (NP (JJ gay) (NNS people)) (VP (VBP complain) (ADVP (RB anymore))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ corrects) (NP (NP (NP (NNP S2) (POS 's)) (NN grammar)) (SBAR (WHADVP (WRB when)) (S (NP (PRP he)) (VP (NNS types) (NP (JJ common) (NN law))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (DT a) (JJ straight) (NN man)) (VP (MD can) (VP (VB marry) (NP (PRP$ his) (NN girlfriend))))) (CC and) (S (NP (PRP she)) (VP (MD can) (VP (VB get) (NP (NN healthcare))))))))) (, ,) (CC but) (S (NP (JJ gay) (NNS people)) (VP (AUX are) (VP (VBN prohibited) (PP (IN from) (S (VP (VBG getting) (VP (VBN married)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (SBAR (IN if) (S (NP (CD two) (NNS people)) (VP (VP (VBP cohabitate) (PP (IN for) (ADVP (RB awhile)))) (CC or) (VP (AUX have) (NP (NNS children)))))) (NP (PRP they)) (VP (MD should) (VP (VB get) (NP (NN healthcare)) (PP (IN because) (IN of) (NP (JJ Common) (NN law)))))))) (. .)))

