(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage) (NNS issues)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (IN in) (NP (NP (NN favor)) (PP (IN of) (NP (JJ gay) (NN marriage))))) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (ADJP (IN against)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ cites) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (ADVP (RB already)) (VP (VBN allowed) (PP (IN in) (NP (JJ many) (NNS countries))))))))) (CC and) (VP (VBZ feels) (SBAR (S (NP (PRP it)) (ADVP (RB soon)) (VP (MD will) (VP (AUX be) (PP (IN in) (NP (NP (DT the) (NN state)) (PP (IN of) (NP (NNP Massachusetts))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NNP US)) (VP (MD will) (RB not) (VP (VB pass) (NP (DT the) (NNS laws)) (ADVP (RB so) (RB easily)) (S (VP (VBG citing) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (NNP Hawaii)) (VP (AUX has) (ADVP (RB already)) (VP (VBN banned) (NP (DT the) (NN practice)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ feels) (SBAR (S (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ gay) (NNS rights)))) (VP (AUX is) (ADJP (RBR more) (JJ political)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ disputes) (SBAR (S (NP (NP (NP (NNP S1) (POS 's)) (NN comparison)) (PP (IN between) (NP (NP (JJ gay) (NN marriage) (CC and) (NN polygamy)) (, ,) (SBAR (WHNP (WDT which)) (S (NP (PRP he) (NNS fees)) (VP (MD could) (VP (AUX be) (NP (DT the) (JJ next) (NN thing))))))))) (VP (VBD allowed) (SBAR (SINV (MD should) (NP (JJ gay) (NN marriage)) (VP (VB become) (NP (NN commonplace))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ holds) (PP (TO to) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (PRP it)) (VP (MD will) (VP (AUX be) (VP (VP (VBN passed)) (CC and) (VP (VBN passed) (ADVP (RB soon))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT any) (NN headway)) (VP (MD will) (VP (VB take) (NP (JJ many) (NNS years))))))) (. .)))

