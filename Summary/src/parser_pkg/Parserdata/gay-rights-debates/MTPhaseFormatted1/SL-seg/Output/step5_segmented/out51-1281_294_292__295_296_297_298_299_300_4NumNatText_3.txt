<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG the/DT issue/NN of/IN gay/JJ marriage/NN and/CC the/DT financial/JJ effect/NN it/PRP will/MD have/AUX on/IN society/NN ./. </C>
</S>
<S>
<C>S1/NNP is/AUX against/IN gay/JJ marriage/NN due/JJ to/TO the/DT effects/NNS he/PRP believes/VBZ passing/VBG such/PDT a/DT law/NN will/MD have/AUX on/IN society/NN </C>
<C>while/IN S2/NNP is/AUX in/IN favor/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ allowing/VBG gay/JJ marriage/NN would/MD cause/VB a/DT dependent/JJ class/NN needing/VBG more/JJR benefits/NNS of/IN one/CD kind/NN or/CC another/DT ./. </C>
</S>
<S>
<C>S2/NNP questions/VBZ why/WRB gay/JJ citizens/NNS who/WP pay/VBP into/IN the/DT economic/JJ system/NN should/MD also/RB not/RB benefit/VB from/IN them/PRP ./. </C>
</S>
<S>
<C>He/PRP also/RB provides/VBZ an/DT analogy/NN based/VBN on/IN a/DT marriage/NN break/NN up/RP </C>
<C>to/TO support/VB this/DT ,/, </C>
<C>advising/VBG </C>
<C>if/IN gay/JJ marriage/NN were/AUX recognized/VBN </C>
<C>and/CC there/EX was/AUX a/DT divorce/NN ,/, </C>
<C>the/DT spouse/NN making/VBG more/JJR money/NN would/MD have/AUX to/TO pay/VB alimony/NN </C>
<C>instead/RB of/IN the/DT other/JJ going/VBG to/TO the/DT state/NN ./. </C>
</S>
</P>
</T>
