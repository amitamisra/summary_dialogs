<s> <PARAGRAPH> </s>
<s>  S1 sides with the court's argument that in 1909 there was no prejudice toward gays as they did not know what this was at that time and this makes it a moral standpoint, not a prejudice against. </s>
<s> S1 feels S2 does not have a firm grasp of historical facts due to the points made. </s>
<s> S1 rejects the point of gays having been forced to conform to society and cites that hetero-society did not believe in sodomy and due to this would not support relationships between two males. </s>
<s> S2 compares the treatment of homosexuals in that era with the oppression of the black race and the belief women were a lesser gender. </s>
<s> S2 believes the morality of the era was based on ignorance and prejudice as it is today. </s>
<s> S2 advises the moral system used in that era was prejudice based on ignorance and society is plagued by the same ignorance and non-morals. </s>
<s> S2 also believes gays were not thought of as human. </s>
<s> Free to be gay as long as they were not openly gay. </s>
<s>  </s>
