<T>
<P>
</P>
<P>
<S>
<C>S1/NNP says/VBZ that/IN healthcare/NNP applies/VBZ to/TO spouses/NNS ,/, </C>
<C>so/IN if/IN it/PRP is/AUX illegal/JJ for/IN gay/JJ people/NNS to/TO get/VB married/VBN in/IN VA/NNP </C>
<C>they/PRP can/MD not/RB share/VB healthcare/NN ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/RB to/TO say/VB it/PRP works/VBZ the/DT same/JJ way/NN </C>
<C>if/IN a/DT straight/JJ man/NN has/AUX a/DT girlfriend/NN ./. </C>
</S>
<S>
<C>She/PRP can/MD not/RB get/VB healthcare/NN through/IN his/PRP$ job/NN ./. </C>
</S>
<S>
<C>He/PRP hopes/VBZ gay/JJ people/NNS upset/VBN by/IN VA/NNP leave/VBP </C>
<C>and/CC that/IN gay/JJ marriage/NN gets/VBZ outlawed/VBN everywhere/RB </C>
<C>so/IN he/PRP does/AUX not/RB have/AUX to/TO hear/VB gay/JJ people/NNS complain/VBP anymore/RB ./. </C>
</S>
<S>
<C>S1/NNP corrects/VBZ S2/NNP 's/POS grammar/NN when/WRB he/PRP types/NNS common/JJ law/NN ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ that/IN a/DT straight/JJ man/NN can/MD marry/VB his/PRP$ girlfriend/NN </C>
<C>and/CC she/PRP can/MD get/VB healthcare/NN ,/, </C>
<C>but/CC gay/JJ people/NNS are/AUX prohibited/VBN from/IN getting/VBG married/VBN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN if/IN two/CD people/NNS cohabitate/VBP for/IN awhile/RB or/CC have/AUX children/NNS </C>
<C>they/PRP should/MD get/VB healthcare/NN because/IN of/IN Common/JJ law/NN ./. </C>
</S>
</P>
</T>
