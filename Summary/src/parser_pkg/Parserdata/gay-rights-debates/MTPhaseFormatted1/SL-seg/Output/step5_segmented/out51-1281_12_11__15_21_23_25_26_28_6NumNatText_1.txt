<T>
<P>
</P>
<P>
<S>
<C>S2/NNP claims/VBZ that/DT marriage/NN is/AUX defined/VBN as/IN a/DT family/NN unit/NN based/VBN on/IN a/DT heterosexual/JJ contract/NN ./. </C>
</S>
<S>
<C>S1/NNP refutes/VBZ this/DT assertion/NN ,/, </C>
<C>citing/VBG a/DT number/NN of/IN countries/NNS which/WDT recognize/VBP same-sex/JJ marriage/NN and/CC compares/VBZ the/DT lack/NN of/IN reproductive/JJ potential/NN for/IN homosexual/JJ couples/NNS to/TO a/DT heterosexual/JJ couple/NN that/WDT chooses/VBZ not/RB to/TO have/AUX children/NNS or/CC is/AUX unable/JJ to/TO have/AUX children/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ that/IN single/JJ parent/NN families/NNS are/AUX not/RB considered/VBN traditional/JJ and/CC references/NNS a/DT poll/NN showing/VBG support/NN for/IN gay/JJ rights/NNS among/IN the/DT 18/CD -/: 29/CD demographic/JJ ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN a/DT traditional/JJ family/NN is/AUX composed/VBN of/IN a/DT husband/NN and/CC wife/NN </C>
<C>and/CC that/IN the/DT laws/NNS of/IN other/JJ countries/NNS are/AUX irrelevant/JJ to/TO the/DT laws/NNS of/IN America/NNP ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN single/JJ parent/NN families/NNS are/AUX natural/JJ and/CC suggests/VBZ that/DT time/NN will/MD decide/VB which/WDT side/NN is/AUX right/JJ ./. </C>
</S>
</P>
</T>
