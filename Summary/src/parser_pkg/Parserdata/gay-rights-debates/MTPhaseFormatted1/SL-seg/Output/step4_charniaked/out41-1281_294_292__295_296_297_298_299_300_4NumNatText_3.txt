(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (NP (JJ gay) (NN marriage)) (CC and) (NP (NP (DT the) (JJ financial) (NN effect)) (SBAR (S (NP (PRP it)) (VP (MD will) (VP (AUX have) (PP (IN on) (NP (NN society))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (IN against) (NP (NP (JJ gay) (NN marriage)) (PP (JJ due) (TO to) (NP (NP (DT the) (NNS effects)) (SBAR (S (NP (PRP he)) (VP (VBZ believes) (SBAR (S (S (VP (VBG passing) (NP (PDT such) (DT a) (NN law)))) (VP (MD will) (VP (AUX have) (PP (IN on) (NP (NN society))) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (PP (IN in) (NP (NN favor)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (S (VP (VBG allowing) (NP (JJ gay) (NN marriage)))) (VP (MD would) (VP (VB cause) (NP (NP (DT a) (JJ dependent) (NN class)) (VP (VBG needing) (NP (NP (JJR more) (NNS benefits)) (PP (IN of) (NP (NP (CD one) (NN kind)) (CC or) (NP (DT another)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ questions) (SBAR (WHADVP (WRB why)) (S (NP (NP (JJ gay) (NNS citizens)) (SBAR (WHNP (WP who)) (S (VP (VBP pay) (PP (IN into) (NP (DT the) (JJ economic) (NN system))))))) (VP (MD should) (ADVP (RB also)) (RB not) (VP (VB benefit) (PP (IN from) (NP (PRP them)))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ provides) (NP (NP (DT an) (NN analogy)) (VP (VBN based) (PP (IN on) (NP (DT a) (NN marriage) (NN break))))) (PRT (RP up)) (S (VP (TO to) (VP (VB support) (NP (DT this))))) (, ,) (S (VP (VBG advising) (SBAR (S (SBAR (IN if) (S (S (NP (JJ gay) (NN marriage)) (VP (AUX were) (VP (VBN recognized)))) (CC and) (S (NP (EX there)) (VP (AUX was) (NP (DT a) (NN divorce)))))) (, ,) (NP (NP (DT the) (NN spouse)) (VP (VBG making) (NP (JJR more) (NN money)))) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB pay) (NP (NN alimony)) (PP (RB instead) (IN of) (S (NP (DT the) (JJ other)) (VP (VBG going) (PP (TO to) (NP (DT the) (NN state)))))))))))))))) (. .)))

