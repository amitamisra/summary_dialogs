(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (ADJP (VBN upset) (PP (IN with) (NP (NNP Matt)))) (PP (PP (IN about) (S (VP (VBG making) (NP (DT an) (NN assertion) (SBAR (IN that) (S (NP (NNPS Republicans)) (VP (MD would) (VP (VB send) (NP (NNS gays)) (PP (TO to) (NP (NN concentration) (NNS camps))))))))))) (CC and) (PP (IN about) (NP (NP (DT a) (NN statement)) (PP (IN by) (NP (NN psychologist) (NNP Paul) (NNP Cameron))))))) (. .)))

(S1 (S (ADVP (RB Instead)) (NP (PRP he)) (VP (MD should) (VP (AUX have) (VP (VBN linked) (NP (NP (NN someone)) (PP (IN in) (NP (DT the) (NNP Republican) (NN leadership))) (VP (VBG saying) (NP (DT those) (NNS things))))))) (. .)))

(S1 (S (NP (NNP Paul) (NNP Cameron)) (VP (AUX is) (NP (NP (DT a) (JJ frequent) (NN target)) (PP (IN of) (NP (JJ gay) (NN pride) (NN movement))) (PP (IN for) (NP (NN demonization))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP Paul) (NNP Cameron)) (VP (AUX is) (NP (NP (DT the) (ADJP (ADVP (RBS most) (RB frequently)) (VBN quoted)) (`` ``) (NN expert) ('' '')) (VP (VBN cited) (PP (IN by) (NP (NNP GOP))) (PP (IN in) (NP (PDT all) (PRP$ their) (NNS attempts))) (S (VP (TO to) (VP (VB limit) (NP (NP (DT the) (NNS rights)) (PP (IN of) (NP (JJ gay) (NNS people))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN point)) (VP (AUX is) (SBAR (IN that) (S (NP (NNP Matt)) (VP (VBD commented) (SBAR (SBAR (IN that) (S (NP (NP (NN someone)) (SBAR (WHNP (WP who)) (S (VP (MD may) (RB not) (ADVP (RB even)) (VP (AUX be) (NP (DT a) (JJ republican))))))) (VP (VBZ hates) (NP (NNS gays))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS republicans)) (VP (VBP hate) (NP (NNS gays))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (SBAR (S (NP (PRP it)) (VP (AUX 's) (RB not) (PP (PP (IN about) (NP (NNP Paul) (NNP Cameron))) (CC but) (PP (IN about) (NP (NP (NNP Matt) (POS 's)) (JJ ridiculous) (NNS comments))))))) (CC and) (SBAR (IN that) (S (NP (NP (DT no) (JJ republican)) (PP (IN in) (NP (NN leadership)))) (VP (MD would) (VP (VB call) (PP (IN for) (NP (NP (DT the) (NN extermination)) (PP (IN of) (NP (NNS gays))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NNP Paul) (NNP Cameron) (POS 's)) (NN research)) (VP (AUX is) (VP (VBN cited) (PP (IN in) (NP (NP (RB almost) (DT every) (JJ single) (NN attack)) (PP (IN on) (NP (NP (JJ gay) (NNS people)) (PP (IN in) (NP (NP (NN state)) (PP (IN after) (NP (NN state) (NN legislature)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (PP (TO to) (NP (NNP S2))) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (VP (VBG making) (NP (DT a) (JJ ridiculous) (NN generalization)) (SBAR (IN while) (S (NP (NNP S2)) (VP (VBZ replies) (SBAR (IN that) (S (NP (PRP he)) (VP (VBD said) (SBAR (S (NP (NNP S1)) (VP (AUX is) (ADVP (RB probably)) (ADJP (JJ right) (S (VP (TO to) (VP (VB assume) (SBAR (IN that) (S (NP (DT some) (NNS people)) (VP (AUX were) (ADJP (JJ anti-religion))))))))))))))))))))))) (. .)))

