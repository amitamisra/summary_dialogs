(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (DT the) (JJ gay) (NN marriage) (NN debate)) (VP (AUX is) (RB not) (ADJP (JJ synonymous) (PP (IN with) (NP (NP (NN someone)) (VP (VBG condoning) (NP (NP (NN violence)) (PP (IN against) (NP (NNS gays)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NP (NN anyone)) (ADVP (RB here))) (VP (MD would) (PP (IN with) (NP (NNP S2))) (SBAR (IN that) (S (NP (NNS gays)) (VP (MD should) (RB not) (VP (VB experience) (NP (NP (NN violence)) (CC and) (NP (NN name-calling))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ doubts) (SBAR (IN that) (S (NP (NN anyone)) (VP (MD would) (VP (AUX be) (ADJP (JJ satisfied)) (SBAR (IN if) (S (NP (NP (DT every) (JJ religious) (NN person)) (SBAR (WHNP (WP who)) (S (ADVP (RB genuinely)) (VP (VBD disagreed) (PP (IN with) (NP (JJ gay) (NN marriage))) (PP (IN because) (IN of) (NP (PRP$ their) (NN religion))))))) (VP (VBD added) (NP (NP (NNP a) (POS ')) (NN but-no-violence)))))))))) (. !)))

(S1 (FRAG ('' ') (NP (NN addendum)) (PP (TO to) (NP (NP (RB everytime)) (SBAR (S (NP (PRP they)) (VP (VBD weighed) (PRT (RP in)) (PP (IN on) (NP (PRP it)))))))) (. .)))

(S1 (S (NP (NNS People)) (VP (MD should) (RB not) (VP (AUX have) (S (VP (TO to) (VP (VB stop) (S (VP (VBG standing) (PRT (RP up)) (PP (IN for) (NP (PRP$ their) (NNS principles))) (PP (ADVP (RB just)) (IN because) (IN of) (NP (NP (NNS nutcases)) (PP (IN out) (NP (EX there)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX is) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB ignore) (NP (NNP S2))))))) (CC and) (VP (VBZ defends) (S (RB not) (VP (VBG attacking) (NP (NNP S2)) (PP (JJ due) (TO to) (NP (NP (DT the) (NN change)) (PP (IN of) (NP (NP (NN tone)) (PP (IN of) (NP (NNP S2))))))))))) (. .)))

(S1 (S (NP (NN Namecalling) (CC and) (NN violence)) (VP (AUX are) (ADJP (JJ wrong)) (, ,) (ADVP (RB REGARDLESS) (PP (IN of) (SBAR (WHADVP (WRB where)) (S (NP (NNS people)) (VP (VBP fall) (PP (IN on) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NN fact))) (, ,) (ADVP (RBR more) (RB importantly)) (, ,) (NP (PRP they)) (VP (AUX 're) (ADVP (RB always)) (ADJP (JJ wrong)) (ADVP (DT no) (NN matter) (SBAR (WHNP (WP what)) (FRAG (NP (DT the) (NN issue)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ points) (PP (TO to) (NP (NP (DT another) (NN author) (NNP Archie)) (SBAR (WHNP (WP who)) (S (VP (ADVP (RB regularly)) (VBZ denigrates) (NP (NP (NNS gays)) (PP (IN with) (NP (NP (DT a) (NN variety)) (PP (IN of) (NP (NP (NNS slurs)) (PP (IN as) (NP (NP (NN someone)) (SBAR (WHNP (WP who)) (S (VP (VBZ uses) (NP (JJ inflammatory) (NN language)))))))))))))))))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX are) (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP put) (PRT (RP out)) (NP (NP (NN junk)) (SBAR (WHNP (WDT that)) (S (VP (VBZ makes) (S (NP (NNS people)) (VP (VB feel) (ADJP (JJ threatened) (PP (IN by) (NP (NNS gays))))))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX 's) (VP (VBN built) (PRT (RP up)) (NP (NP (NN toxicity)) (PP (IN from) (NP (NP (PDT all) (DT the) (JJ anti-gay) (NN rhetoric)) (SBAR (WHNP (WDT that)) (S (VP (VBZ reaches) (NP (DT a) (VBG boiling) (NN point)))))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (NP (DT the) (NN rhetoric)) (VP (VBZ inflames)))) (, ,) (NP (PRP it)) (VP (AUX 's) (ADJP (JJ obvious)) (SBAR (WHADVP (WRB where)) (S (NP (PRP it)) (VP (VBZ starts) (SBAR (WHADVP (WRB when)) (S (NP (NN somebody)) (VP (VBZ gets) (VP (VBN burned))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP S1)) (VP (VP (VBZ throws) (NP (JJ arbitrary) (NNS arguments)) (ADVP (RB out) (RB there))) (, ,) (VP (VBZ belittles) (NP (NP (DT the) (NN claim)) (PP (IN of) (NP (NNP S1))))) (CC and) (VP (VBZ demonstrates) (NP (NP (DT an) (JJ ulterior) (NN motive)) (PP (IN for) (S (VP (VBG ignoring) (NP (DT the) (NN statement))))))))))) (. .)))

