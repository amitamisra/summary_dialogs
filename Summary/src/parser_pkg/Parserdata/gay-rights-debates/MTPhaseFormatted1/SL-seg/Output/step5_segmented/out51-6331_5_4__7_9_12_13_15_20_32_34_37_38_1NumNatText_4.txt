<T>
<P>
</P>
<P>
<S>
<C>S1/NNP -LRB-/-LRB- Kronos/NNP -RRB-/-RRB- and/CC S2/NNP -LRB-/-LRB- Jyoshu/NNP -RRB-/-RRB- are/AUX discussing/VBG the/DT fact/NN that/IN there/EX are/AUX other/JJ groups/NNS besides/IN the/DT gay/JJ community/NN who/WP may/MD want/VB marriage/NN equality/NN rights/NNS ,/, such/JJ as/IN polygamy/JJ advocates/NNS or/CC 17/CD year/NN olds/NNS ./. </C>
</S>
<S>
<C>Jyoshu/NNP sees/VBZ the/DT changes/NNS in/IN law/NN that/WDT would/MD be/AUX required/VBN as/IN an/DT attack/NN on/IN social/JJ and/CC legal/JJ precedent/NN that/WDT can/MD get/VB out/RP of/IN control/NN or/CC be/AUX difficult/JJ to/TO implement/VB ./. </C>
</S>
<S>
<C>Kronos/NNP complains/VBZ that/IN this/DT is/AUX a/DT frequent/JJ tactic/NN of/IN anti-gay/JJ marriage/NN advocates/NNS who/WP unfairly/RB claim/VBP that/IN gays/NNS want/VBP to/TO ``/`` make/VB marriage/NN an/DT open/JJ ended/VBD institution/NN ''/'' ./. </C>
</S>
<S>
<C>Jyoshu/NNP notes/VBZ that/IN whatever/WDT the/DT intent/NN ,/, </C>
<C>the/DT result/NN is/AUX the/DT kind/NN of/IN change/NN the/DT anti-gay/JJ advocates/NNS warn/VBP against/IN ./. </C>
</S>
<S>
<C>Kronos/NNP asserts/VBZ that/IN specific/JJ laws/NNS could/MD accommodate/VB specific/JJ groups/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ that/IN the/DT motivation/NN of/IN gays/NNS who/WP wish/VBP to/TO marry/VB is/AUX sincere/JJ </C>
<C>and/CC the/DT same/JJ as/IN the/DT motivation/NN behind/IN opposite/JJ sex/NN marriages/NNS ,/, and/CC therefore/RB the/DT broadening/VBG of/IN marriage/NN is/AUX a/DT side/NN effect/NN and/CC not/RB the/DT goal/NN ./. </C>
</S>
<S>
<C>Jyoshu/NNP says/VBZ she/PRP understands/VBZ but/CC complains/VBZ that/IN gay/JJ marriage/NN would/MD open/VB the/DT door/NN to/TO further/RB broadening/VBG of/IN marriage/NN ./. </C>
</S>
</P>
</T>
