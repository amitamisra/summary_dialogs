<s> <PARAGRAPH> </s>
<s>  S1 ( Kronos) and S2 ( Jyoshu) are discussing the fact that there are other groups besides the gay community who may want marriage equality rights, such as polygamy advocates or 17 year olds. </s>
<s> Jyoshu sees the changes in law that would be required as an attack on social and legal precedent that can get out of control or be difficult to implement. </s>
<s> Kronos complains that this is a frequent tactic of anti-gay marriage advocates who unfairly claim that gays want to "make marriage an open ended institution". </s>
<s> Jyoshu notes that whatever the intent, the result is the kind of change the anti-gay advocates warn against. </s>
<s> Kronos asserts that specific laws could accommodate specific groups. </s>
<s> He also argues that the motivation of gays who wish to marry is sincere and the same as the motivation behind opposite sex marriages, and therefore the broadening of marriage is a side effect and not the goal. </s>
<s> Jyoshu says she understands but complains that gay marriage would open the door to further broadening of marriage. </s>
<s>  </s>
