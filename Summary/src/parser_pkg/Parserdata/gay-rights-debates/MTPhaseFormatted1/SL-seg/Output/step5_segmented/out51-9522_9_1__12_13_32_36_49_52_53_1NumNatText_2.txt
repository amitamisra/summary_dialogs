<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN people/NNS in/IN support/NN of/IN marriage/NN equality/NN is/AUX growing/VBG ,/, </C>
<C>and/CC he/PRP contends/VBZ that/IN equality/NN will/MD soon/RB stand/VB on/IN the/DT side/NN of/IN the/DT majority/NN ./. </C>
</S>
<S>
<C>S2/NNP retorts/VBZ that/IN gays/NNS lose/VBP every/DT time/NN gay/JJ marriage/NN is/AUX put/VBN to/TO a/DT vote/NN in/IN a/DT ballot/NN ,/, </C>
<C>and/CC that/IN gays/NNS need/AUX a/DT better/JJR PR/NNP strategy/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN majority/NN of/IN people/NNS will/MD support/VB civil/JJ unions/NNS with/IN the/DT same/JJ benefits/NNS but/CC not/RB marriage/NN </C>
<C>and/CC there/EX will/MD be/AUX repercussions/NNS due/JJ to/TO the/DT efforts/NNS to/TO give/VB tax/NN money/NN to/TO federal/JJ employee/NN 's/POS same/JJ sex/NN partners/NNS despite/IN the/DT Defense/NNP of/IN Marriage/NN Act/NNP ./. </C>
</S>
<S>
<C>To/TO contradict/VB ,/, </C>
<C>S2/NNP states/VBZ that/IN so/RB far/RB 5/CD states/NNS have/AUX passed/VBN gay/JJ marriage/NN laws/NNS ,/, </C>
<C>and/CC that/IN gay/JJ people/NNS pay/VB their/PRP$ taxes/NNS just/RB like/IN everyone/NN else/RB ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN the/DT gay/JJ community/NN is/AUX not/RB a/DT bunch/NN of/IN promiscuous/JJ sex/NN crazed/VBN people/NNS ./. </C>
</S>
<S>
<C>S1/NNP rebutts/VBZ by/IN stating/VBG that/IN in/IN those/DT 5/CD states/NNS ,/, gay/JJ marriage/NN was/AUX not/RB put/VBN to/TO a/DT ballot/NN ,/, </C>
<C>but/CC forced/VBN through/IN the/DT court/NN or/CC legislature/NN ./. </C>
</S>
</P>
</T>
