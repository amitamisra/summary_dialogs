<PARAGRAPH> Two subjects are discussing a law banning gay marriage.
S2 believes the law is unjust as it segregates the gay community from that of the heterosexual community.
S1 supports the nation's ability to vote on such issues and supports the "majority rules" philosophy.
S2 believes the courts should take individuals' into consideration and rule on each instead of as a whole.
S1 contends this idea to be incorrect citing the fact that those who believe abortion should be illegal are technically the minority and that a court would not rule on that issue individually.
S1 feels S2 and others like him/her support society's oppression of certain groups.
S1 disagrees advising he/she wants only a society free to vote on laws.
