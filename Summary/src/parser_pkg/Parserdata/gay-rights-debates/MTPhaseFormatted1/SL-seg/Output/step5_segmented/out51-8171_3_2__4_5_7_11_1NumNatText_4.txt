<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX of/IN the/DT opinion/NN that/IN a/DT certain/JJ level/NN of/IN dishonesty/NN is/AUX understandable/JJ and/CC expected/VBN in/IN politics/NNS ./. </C>
</S>
<S>
<C>In/IN regard/NN to/TO gay/JJ rights/NNS ,/, the/DT religious/JJ right/JJ and/CC conservative/JJ republicans/NNS have/AUX made/VBN it/PRP a/DT poisoned/VBN topic/NN such/JJ that/IN politicians/NNS will/MD not/RB say/VB the/DT right/JJ thing/NN </C>
<C>because/IN it/PRP will/MD cost/VB them/PRP votes/NNS ./. </C>
</S>
<S>
<C>Democrats/NNPS have/AUX had/AUX to/TO be/AUX moderate/JJ on/IN the/DT issue/NN </C>
<C>in/IN order/NN to/TO still/RB be/AUX in/IN a/DT position/NN to/TO do/AUX something/NN ./. </C>
</S>
<S>
<C>It/PRP 's/AUX too/RB naive/JJ to/TO expect/VB to/TO a/DT politician/NN to/TO give/VB their/PRP$ view/NN on/IN matters/NNS ,/, </C>
<C>and/CC so/RB it/PRP is/AUX necessary/JJ to/TO figure/VB out/RP how/WRB often/RB they/PRP lie/VBP ,/, </C>
<C>what/WP they/PRP lie/VBP about/IN ,/, </C>
<C>and/CC how/WRB big/JJ their/PRP$ lies/NNS are/AUX ./. </C>
</S>
<S>
<C>In/IN this/DT particular/JJ case/NN ,/, if/IN Biden/NNP says/VBZ he/PRP does/AUX not/RB support/VB gay/JJ marriage/NN </C>
<C>S1/NNP is/AUX just/RB as/RB likely/JJ to/TO think/VB it/PRP 's/AUX because/IN he/PRP knows/VBZ that/IN open/JJ support/NN could/MD cost/VB him/PRP the/DT White/NNP House/NNP as/IN any/DT other/JJ reason/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN as/IN political/JJ consumers/NNS we/PRP should/MD expect/VB honesty/NN of/IN the/DT politicians/NNS ./. </C>
</S>
<S>
<C>It/PRP is/AUX not/RB too/RB much/RB to/TO demand/VB honesty/NN ./. </C>
</S>
<S>
<C>Without/IN that/DT expectation/NN it/PRP is/AUX pointless/JJ in/IN asking/VBG a/DT politician/NN a/DT direct/JJ question/NN ./. </C>
</S>
<S>
<C>S2/NNP understands/VBZ that/IN politicians/NNS lie/VBP but/CC holds/VBZ that/IN if/IN words/NNS of/IN politicians/NNS mean/VBP nothing/NN </C>
<C>then/RB there/EX is/AUX little/JJ point/NN in/IN having/AUXG political/JJ discourse/NN ./. </C>
</S>
<S>
<C>Political/JJ cynicism/NN is/AUX no/DT excuse/NN to/TO not/RB put/VB expectations/NNS on/IN our/PRP$ candidates/NNS to/TO be/AUX honest/JJ with/IN the/DT American/JJ people/NNS ./. </C>
</S>
</P>
</T>
