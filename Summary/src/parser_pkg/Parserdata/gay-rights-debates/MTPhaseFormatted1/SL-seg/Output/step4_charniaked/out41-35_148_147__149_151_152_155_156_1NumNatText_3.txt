(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (NNS Christians)) (VP (AUX are) (VP (VBG trying) (S (VP (TO to) (VP (PRN (, ,) (PP (IN in) (NP (DT a) (NN sense))) (, ,)) (VB ban) (NP (JJ gay) (NNS people)) (PP (IN by) (S (VP (VBG banning) (NP (NP (DT all) (NNS activities)) (VP (VBG pertaining) (PP (TO to) (NP (NP (NNS gays)) (PP (JJ such) (IN as) (NP (NP (NN marriage)) (, ,) (NP (JJ civil) (NNS unions)) (, ,) (CC and) (NP (JJ gay) (NN adoption))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (SBAR (S (NP (DT this)) (VP (AUX is) (RB not) (NP (DT the) (NN case))))) (CC and) (SBAR (IN that) (S (NP (JJS most) (NNS Christians)) (VP (AUX do) (RB not) (VP (VB treat) (NP (JJ gay) (NNS people)) (PP (IN in) (NP (DT this) (NN fashion))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB also)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NN issue)) (PP (IN at) (NP (NN hand))) (VP (AUX is) (RB not) (VP (AUXG being) (VP (VBN discussed) (SBAR (IN as) (S (NP (DT the) (NN forum)) (VP (VBD began) (PP (IN with) (S (VP (VBG speaking) (PP (IN about) (NP (DT the) (JJ gay) (NN pride) (NN industry))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ supports) (NP (PRP$ his) (NN theory)) (PP (IN by) (S (VP (VBG citing) (NP (NP (DT the) (JJ constitutional) (NN amendment)) (SBAR (S (VP (TO to) (VP (VB ban) (NP (DT both) (JJ gay) (ADJP (NN marriage) (CC and) (JJ same)) (NN sex) (JJ civil) (NNS unions))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (NP (DT the) (JJ Christian) (NN equivalent)) (PP (IN of) (S (VP (VBG banning) (NP (JJ gay) (NNS people)) (PP (IN by) (S (VP (VBG oppressing) (NP (PRP$ their) (NN lifestyle))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (NP (DT a) (JJ weak) (NN argument)) (SBAR (WHNP (WDT which)) (S (NP (JJ gay) (NNS lobbyists)) (VP (AUX have) (ADJP (JJ overused)))))))))) (. .)))

