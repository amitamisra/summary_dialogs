(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN divorce) (NN rate)) (PP (IN in) (NP (DT the) (NNP US)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (DT the) (JJ high) (NN divorce) (NN rate)) (VP (AUX is) (ADJP (JJ due) (PP (TO to) (NP (NP (DT the) (JJ high) (NN rate)) (PP (IN of) (NP (NP (JJ young) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP get) (VP (VBN divorced)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (NP (DT the) (NN rate)) (PP (IN of) (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP get) (VP (VBN divorced) (SBAR (WHNP (WP who)) (S (VP (AUX have) (VP (AUX been) (ADJP (VBN married) (PP (IN over) (NP (CD five) (NNS years))))))))))))))) (VP (VBZ drops) (ADVP (RB dramatially)))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNS statistics)) (VP (VB prove) (SBAR (IN that) (S (NP (NP (NNS gays)) (PP (IN in) (ADJP (JJ general)))) (VP (AUX are) (ADJP (ADVP (RB much) (RBR more)) (JJ promiscuous)) (NP (RB then) (JJ straight) (NNS people))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG stating) (SBAR (SBAR (IN that) (S (NP (NP (JJ young) (NNS people)) (CC and) (NP (JJ young) (NN marriage))) (VP (AUX are) (RB not) (NP (DT the) (JJ same))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (NNS couples)) (PP (IN in) (NP (JJ general) (WP who) (NN divorce)))) (VP (AUX do) (ADVP (RB so) (RBR more) (RB often)) (PP (IN in) (NP (DT the) (JJ first) (CD five) (NNS years))) (, ,) (ADVP (RB regardless) (PP (IN of) (NP (NN age)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (DT the) (JJ heterosexual) (NN divorce) (NN rate)) (VP (AUX is) (NP (CD 50) (NN %)))) (, ,) (CC and) (S (NP (NNS heterosexuals)) (VP (AUX are) (RB not) (ADJP (ADJP (JJR better)) (PP (IN at) (S (VP (VBG staying) (ADVP (RB together)) (ADVP (RB then)) (SBAR (S (NP (NNS people)) (VP (VBP expect) (S (NP (NNS homosexuals)) (VP (TO to))))))))))))))) (. .)))

