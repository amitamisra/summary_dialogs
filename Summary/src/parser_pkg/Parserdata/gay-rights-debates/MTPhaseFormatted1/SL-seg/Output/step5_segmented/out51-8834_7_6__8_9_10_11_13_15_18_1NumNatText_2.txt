<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN in/IN the/DT next/JJ 20/CD years/NNS the/DT wave/NN of/IN gay/JJ marriage/NN legalization/NN will/MD be/AUX over/RP ,/, </C>
<C>to/TO which/WDT S2/NNP states/VBZ that/IN if/IN Congress/NNP removes/VBZ the/DT Defense/NNP of/IN Marriage/NN Act/NNP ,/, </C>
<C>the/DT full/JJ faith/NN and/CC credit/NN clause/NN is/AUX a/DT clear/JJ constitutional/JJ mandate/NN ./. </C>
</S>
<S>
<C>S1/NNP rebuts/VBZ saying/VBG that/IN even/RB without/IN the/DT federal/JJ DOMA/NN ,/, the/DT choice/NN of/IN law/NN doctrine/NN means/VBZ that/IN states/NNS can/MD choose/VB not/RB to/TO recognize/VB gay/JJ marriage/NN ,/, </C>
<C>or/CC even/RB be/AUX forced/VBN to/TO recognize/VB another/DT state/NN 's/POS gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN the/DT full/JJ faith/NN and/CC credit/NN clause/NN has/AUX never/RB been/AUX used/VBN in/IN that/DT way/NN ,/, and/CC due/JJ to/TO the/DT lengthy/JJ legal/JJ argument/NN involved/VBN it/PRP is/AUX unlikely/JJ to/TO be/AUX used/VBN ./. </C>
</S>
<S>
<C>To/TO contradict/VB this/DT ,/, </C>
<C>S2/NNP states/VBZ that/IN each/DT Justice/NNP will/MD vote/VB to/TO side/VB with/IN gay/JJ marriage/NN ,/, </C>
<C>and/CC there/EX is/AUX a/DT current/JJ tradition/NN of/IN one/CD state/NN respecting/VBG another/DT 's/AUX marriage/NN license/NN when/WRB it/PRP comes/VBZ to/TO heteros/NNS ,/, a/DT tradition/NN that/WDT will/MD be/AUX kept/VBN </C>
<C>if/IN gay/JJ marriage/NN laws/NNS are/AUX passed/VBN ./. </C>
</S>
</P>
</T>
