<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN new/NNP England/NNP and/CC New/NNP York/NNP will/MD have/AUX gay/JJ marriage/NN within/IN the/DT next/JJ few/JJ years/NNS </C>
<C>and/CC that/IN the/DT west/JJ coast/NN might/MD come/VB soon/RB after/IN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN after/IN this/DT time/NN period/NN ,/, gay/JJ marriage/NN legislation/NN will/MD stop/VB for/IN the/DT next/JJ twenty/CD years/NNS or/CC so/RB </C>
<C>and/CC that/IN thirty/CD states/NNS already/RB define/VB marriage/NN as/IN between/IN one/CD man/NN and/CC one/CD woman/NN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB that/IN the/DT supreme/JJ court/NN would/MD overturn/VB gay/JJ marriage/NN bans/NNS as/IN unconstitutional/JJ and/CC believes/VBZ that/IN states/NNS will/MD ultimately/RB be/AUX the/DT ones/NNS to/TO recognize/VB </C>
<C>whether/IN it/PRP should/MD be/AUX legal/JJ or/CC not/RB ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN current/JJ marriage/NN laws/NNS are/AUX disputed/VBN between/IN states/NNS and/CC have/AUX been/AUX for/IN decades/NNS and/CC believes/VBZ that/IN the/DT courts/NNS will/MD be/AUX unable/JJ to/TO intervene/VB ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN a/DT federal/JJ recognition/NN of/IN gay/JJ marriage/NN will/MD be/AUX soon/RB coming/VBG ,/, and/CC argues/VBZ </C>
<C>against/IN S1/NNP 's/POS claims/NNS on/IN the/DT basis/NN of/IN Roberts/NNP and/CC Alito/NNP being/AUXG constitutional/JJ literalists/NNS ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN there/EX is/AUX enough/JJ support/NN in/IN the/DT Supreme/NNP Court/NNP to/TO follow/VB the/DT constitution/NN </C>
<C>as/IN written/VBN </C>
<C>and/CC that/IN it/PRP will/MD find/VB any/DT laws/NNS prohibiting/VBG gay/JJ marriage/NN unconstitutional/JJ as/IN a/DT result/NN ./. </C>
</S>
</P>
</T>
