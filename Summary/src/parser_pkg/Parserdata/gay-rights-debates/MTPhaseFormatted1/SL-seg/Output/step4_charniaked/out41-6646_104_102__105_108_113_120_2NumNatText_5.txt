(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (NNP AIDS)) (VP (VBD became) (NP (NP (DT an) (NN epidemic)) (PP (IN in) (NP (DT the) (NNP US)))) (PP (IN through) (NP (DT the) (JJ gay) (NN community))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (S (VP (VBG changing) (NP (NP (DT the) (NN topic)) (PP (TO to) (NP (NNP AIDs))) (PP (IN in) (NP (NNP Africa)))) (S (VP (TO to) (VP (VB deflect) (NP (NN attention)) (PP (IN from) (NP (NP (DT the) (NN topic)) (PP (IN of) (NP (NNP AIDs))) (PP (IN in) (NP (DT the) (NNP US)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ challenges) (S (NP (NNP S2)) (VP (TO to) (VP (VB refute) (NP (PRP$ his) (NN assertion)) (PP (IN with) (NP (NN proof))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBD said) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (NP (DT a) (NN bigot))))) (, ,) (CC but) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (ADVP (RB just)) (VP (VBG presenting) (NP (NP (DT a) (NN fact)) (PP (IN of) (SBAR (WHADVP (WRB how)) (S (NP (NNP AIDs)) (VP (VBD became) (NP (NP (DT an) (NN epidemic)) (PP (IN in) (NP (DT the) (NNP US)))) (PP (IN through) (NP (DT the) (JJ gay) (NN community)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX did) (RB not) (VP (VB present) (NP (PRP$ his) (NNS statistics)) (PP (IN as) (NP (NP (DT a) (NN discussion)) (PP (IN about) (NP (NP (DT the) (NN progression)) (PP (IN of) (NP (NNP AIDS))) (PP (IN in) (NP (DT the) (NNP US)))))))))))) (. .)))

(S1 (S (ADVP (RB Rather)) (, ,) (NP (NNP S1)) (VP (VBD presented) (S (NP (PRP$ his) (NNS statistics)) (VP (TO to) (VP (VB justify) (NP (NP (NN opposition)) (PP (TO to) (NP (JJ gay) (NNS rights)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD brought) (PRT (RP out)) (NP (NP (NNS AIDs)) (PP (IN in) (NP (NNP Africa)))) (SBAR (IN because) (S (NP (NN location)) (VP (AUX is) (ADJP (JJ irrelevant) (PP (IN in) (S (VP (VBG refuting) (NP (NP (NNP S1) (POS 's)) (NN logic)))))))))) (. .)))

