<PARAGRAPH> Two people are discussing gay marriage.
S1 states that marriage is recognizing a family unit based on a natural heterosexual contract.
S2 counters by asking about artificial insemination.
S1 states that it is not about the way children are created but about the nature of the relationship, which remains natural.
S2 then questions which god would enforce marriage when the ceremony is done by a judge, and he states that polygamy is a way for bisexuals to stay under the radar.
He states that the US has been dealing with polygamy for years, with very few prosecutions.
S1 contends that polygamy would not count because it is one man or women married to multiple members of the opposite sex.
