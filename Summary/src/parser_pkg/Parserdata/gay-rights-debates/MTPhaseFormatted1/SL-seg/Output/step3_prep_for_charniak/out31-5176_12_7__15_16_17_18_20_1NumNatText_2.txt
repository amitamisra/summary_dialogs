<s> <PARAGRAPH> </s>
<s>  S1 believes that the majority rules no matter what the minority thinks. </s>
<s> He thinks this is the way the law works and that it does not matter how an individual feels. </s>
<s> It matters what the majority votes on. </s>
<s> He cites slavery as an example. </s>
<s> If someone wants a law changed S1 thinks they should get the majority to side with them and change it. </s>
<s> S2 thinks that S1 does not understand because he is heterosexual and not the one being oppressed. </s>
<s> S2 believes that the courts should look at individuals lives and the effect of laws on everyone. </s>
<s> He thinks if a law is unjust then it should be overturned regardless of what the majority wants. </s>
<s> He thinks conservatives are heartless and do not care if people suffer. </s>
<s>  </s>
