<s> <PARAGRAPH> </s>
<s>  S1 asserts that gay marriage and polygamy are commonplace in ancient Greece, Rome and China. </s>
<s> He listed hyperlinks to the source of his assertion. </s>
<s> He argues that marriage as a union between one man and one woman is a recent construct as compared to gay marriage. </s>
<s> S2 asserts that same-sex marriage has never been commonplace. </s>
<s> He argues that a couple of Buddhist sects allow it. </s>
<s> It was frowned upon in Roman times. </s>
<s> The Greeks had homosexual liaisons, some from desire, some for a form of mentoring reasons. </s>
<s> But those were neither marriages nor assumed to be so by those societies. </s>
<s> Marriage is between people of opposite genders in those societies, and thus it is not a new concept. </s>
<s>  </s>
