(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB marry))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN marriage)) (VP (MD should) (VP (AUX be) (VP (VBN left) (PP (TO to) (NP (NP (DT a) (JJ religious) (NN institution)) (SBAR (WHADVP (WRB where)) (S (NP (PRP it)) (VP (MD would) (VP (VB mean) (NP (NN something)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (DT the) (NNS ceremonies) (SBAR (IN that) (S (NP (NNS gays)) (VP (AUX are) (VP (VBG looking) (SBAR (IN for) (, ,) (S (NP (PRP it)) (VP (AUX is) (NP (PDT all) (DT the) (JJ civil) (NNS benefits)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ retorts) (SBAR (IN that) (S (NP (EX there)) (VP (MD should) (VP (AUX be) (NP (NP (DT a) (JJR better) (NN system)) (, ,) (SBAR (WHNP (WDT that)) (S (VP (ADVP (RB even)) (VBZ allows) (NP (NP (JJ single) (NNS people)) (PP (IN with) (NP (JJS best) (NNS friends)))) (NP (NP (NN access)) (PP (TO to) (NP (DT the) (JJ same) (NNS benefits))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (S (VP (VBG saying) (SBAR (IN that) (S (S (NP (NNS gays)) (VP (AUX are) (RB not) (NP (JJ single) (NNS people)))) (, ,) (CC and) (S (NP (JJ single) (NNS people)) (VP (MD can) (ADVP (RB always)) (VP (VB get) (VP (VBN married)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contradicts) (PP (IN by) (S (VP (VBG saying) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD should) (ADVP (RB just)) (VP (VB marry) (NP (NP (DT the) (JJ opposite) (NN sex)) (, ,) (SBAR (WHPP (TO to) (WHNP (WDT which))) (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (AUX are) (VP (VBG seeking) (NP (DT the) (NN right) (S (VP (VP (TO to) (VP (VB marry) (NP (NP (DT a) (NN person)) (PP (IN of) (NP (PRP$ their) (NN choosing)))))) (, ,) (RB not) (VP (TO to) (ADVP (RB just)) (VP (VB marry) (NP (NP (NN anyone)) (PP (IN for) (NP (NNS benefits))))))))))))))))))))))))) (. .)))

