<s> <PARAGRAPH> </s>
<s>  S1 thinks that we do not just benefit from what is often considered the common, nuclear family. </s>
<s> We benefit from families with adopted children, step children, families with no children at all. </s>
<s> S1 believes that marriage has nothing to do with the bible. </s>
<s> Then what may lead to incest and polygamy does not relate specifically to gay legal marriage at all. </s>
<s> S1 is trying to show that people who are against gay marriage try to dissuade others by attempting to incite fear by spouting off a parade of horribles that have nothing to do with the issue of gay marriage. </s>
<s> S1 holds that love applies to straight marriages as well as gay marriages and that love is not exclusive to gay marriages. </s>
<s> It is fallacious to believe that love will lead to a slippery slope. </s>
<s> Those against gay marriage use logical fallacies. </s>
<s> S2 does not see the logic in bringing up sibling marriages and is curious to know how S1 would respond to such movements. </s>
<s> S2 does not believe that what people do in their private lives is anyone else's business. </s>
<s> What may lead to incestuous marriage is if siblings or related people who love each other and want to live together under the legal union of marriage. </s>
<s> What may lead to polygamous marriage is 3 or more people who love each other and want to live together under the legal union of marriage. </s>
<s> Gay marriage was considered just as much a taboo as incest or polygamy just a few years ago. </s>
<s> S2 does not understand why it is ok to deny incestuous relations the right to marry on the grounds that it is considered taboo, but ok to approve gay marriage on the same grounds. </s>
<s>  </s>
