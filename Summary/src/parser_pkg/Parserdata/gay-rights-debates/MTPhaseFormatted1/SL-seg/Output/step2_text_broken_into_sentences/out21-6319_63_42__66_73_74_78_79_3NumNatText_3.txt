<PARAGRAPH> The issues here appears to stem from an employee citing some religious view to his/her boss indicating why they cannot perform a job duty.
Speaker one does not think people should pick and choose what they want equal rights on, in this case between religion and other rights issues, that it should be all or nothing.
Speaker one therefore believes that speaker two is unfairly discriminating against an employee based off their religious views.
Speaker two believes that society dictates that you have to be biased at times.
Speaker two continues that if speaker one's logic holds true, then he/she should be able to turn away customers based off his/her own personal religious views.
