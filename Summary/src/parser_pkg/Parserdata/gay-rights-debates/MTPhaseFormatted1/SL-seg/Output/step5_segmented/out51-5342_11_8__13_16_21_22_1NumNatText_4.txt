<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN there/EX 's/AUX a/DT big/JJ difference/NN between/IN possibility/NN and/CC likelihood/NN ./. </C>
</S>
<S>
<C>S1/NNP finds/VBZ fault/NN with/IN the/DT stance/NN that/IN republicans/NNS would/MD ship/VB gay/JJ people/NNS to/TO concentration/NN camps/NNS ./. </C>
</S>
<S>
<C>S1/JJ objects/NNS to/TO bringing/VBG up/RP a/DT statement/NN by/IN Paul/NNP Cameron/NNP ,/, as/RB if/IN Paul/NNP Cameron/NNP has/AUX anything/NN to/TO do/AUX whatsoever/RB with/IN Republican/NNP politics/NNS or/CC leadership/NN ./. </C>
</S>
<S>
<C>Matt/NNP is/AUX pulling/VBG some/DT wild/JJ point/NN out/IN of/IN his/PRP$ hat/NN ,/, </C>
<C>saying/VBG that/IN because/IN someone/NN who/WP may/MD not/RB even/RB be/AUX a/DT republican/JJ hates/VBZ gays/NNS ,/, </C>
<C>that/WDT republicans/NNS hate/VBP gays/NNS ./. </C>
</S>
<S>
<C>I/PRP might/MD as/RB well/RB have/AUX said/VBN that/IN since/IN John/NNP Lennon/NNP attacked/VBD religion/NN ,/, </C>
<C>then/RB Democrats/NNPS hate/VBP religion/NN ./. </C>
</S>
<S>
<C>It/PRP 's/AUX about/IN Matt/NNP 's/POS ridiculous/JJ generalizations/NNS and/CC comments/NNS </C>
<C>and/CC S1/NNP holds/VBZ that/IN there/EX is/AUX NO/DT republican/JJ in/IN leadership/NN that/WDT would/MD call/VB for/IN the/DT extermination/NN of/IN gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN S2/NNP is/AUX making/VBG ridiculous/JJ generalizations/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN S1/NNP is/AUX defending/VBG Paul/NNP Cameron/NNP and/CC states/NNS that/IN he/PRP is/AUX the/DT single/JJ most/RBS frequently/RB quoted/VBN ''/'' expert/NN ''/'' cited/VBN by/IN the/DT GOP/NNP in/IN ALL/PDT their/PRP$ attempts/NNS to/TO limit/VB the/DT rights/NNS of/IN gay/JJ people/NNS ./. </C>
</S>
<S>
<C>Paul/NNP Cameron/NNP 's/POS research/NN is/AUX cited/VBN in/IN virtually/RB every/DT single/JJ attack/NN on/IN gay/JJ people/NNS in/IN state/NN after/IN state/NN after/IN state/NN legislature/NN ./. </C>
</S>
<S>
<C>It/PRP was/AUX presented/VBN in/IN the/DT ''/'' debate/NN ''/'' about/IN same-sex/JJ marriage/NN in/IN the/DT U.S./NNP friggin/NN '/POS Senate/NNP ./. </C>
</S>
<S>
<C>His/PRP$ name/NN appears/VBZ in/IN the/DT citations/NNS of/IN virtually/RB every/DT legal/JJ brief/NN filed/VBN by/IN opponents/NNS of/IN same-sex/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP is/AUX surprised/JJ that/IN reasonable/JJ people/NNS talk/VBP about/IN Paul/NNP Cameron/NNP ./. </C>
</S>
<S>
<C>S2/NNP finds/VBZ it/PRP laughable/JJ to/TO try/VB to/TO defend/VB the/DT GOP/NNP when/WRB one/CD of/IN their/PRP$ admittedly/RB most/JJS effective/JJ propaganda/NN campaigns/NNS is/AUX to/TO rally/VB the/DT ''/'' family-values/NNS ''/'' crowd/NN against/IN gay/JJ rights/NNS ./. </C>
</S>
</P>
</T>
