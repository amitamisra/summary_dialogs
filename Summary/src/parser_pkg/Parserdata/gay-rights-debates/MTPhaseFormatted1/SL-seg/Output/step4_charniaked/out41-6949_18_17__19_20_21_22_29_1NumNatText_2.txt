(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN world)) (VP (VBN changed) (PP (IN with) (NP (NP (DT the) (NN legislation)) (PP (IN in) (NP (NNP Massachusetts))) (S (VP (TO to) (VP (VB allow) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ assumes) (SBAR (IN that) (S (NP (NNP S2)) (, ,) (S (VP (AUXG having) (VP (VBN stated) (NP (JJ several) (NNS times)) (SBAR (S (NP (PRP he)) (VP (AUX is) (PP (IN against) (NP (JJ gay) (NN marriage))))))))) (, ,) (VP (MD will) (VP (VB hope) (SBAR (S (NP (DT the) (NN legislation)) (VP (VP (AUX is) (VP (VBN overturned))) (CC but) (VP (MD may) (VP (AUX be) (PP (IN out) (PP (IN of) (NP (NN luck)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (SBAR (IN if) (S (NP (NP (NNP S2) (POS 's)) (NN opinion)) (VP (AUX has) (VP (VBN changed) (SBAR (RB now) (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (PP (IN in) (NP (NN place))) (PP (IN in) (NP (NNP Massachusetts)))))))))) (CC and) (SBAR (IN if) (S (NP (PRP he)) (VP (VBZ wants) (SBAR (S (NP (PRP it)) (VP (VBD overturned))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ resents) (NP (NP (DT the) (NN assumption)) (PP (IN of) (NP (NNP S1))))) (CC and) (VP (VBZ says) (SBAR (IN that) (S (NP (NP (NN legislation)) (SBAR (S (VP (TO to) (VP (VB overturn) (NP (DT the) (NNP Massachusetts) (NN decision))))))) (VP (MD would) (VP (AUX be) (NP (NP (DT an) (JJ over) (NN intrusion)) (PP (IN by) (NP (DT the) (NN government)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (MD would) (RB not) (VP (VB want) (S (NP (DT an) (NN amendment)) (VP (TO to) (VP (VB ban) (NP (JJ gay) (NN marriage))))))))) (CC and) (SBAR (IN that) (S (VP (AUX has) (ADVP (RB always)) (VP (AUX been) (NP (PRP$ his) (NN opinion)) (SBAR (IN because) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB agree) (PP (IN with) (NP (NP (IN over) (NN intrusion)) (PP (IN by) (NP (DT the) (NN government))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ sees) (NP (DT this)) (PP (IN as) (NP (NP (DT a) (JJ true) (JJ conservative) (NN opinion)) (SBAR (S (VP (TO to) (VP (VB take)))))))) (. .)))

