(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG debating) (SBAR (IN whether) (S (S (VP (VBG making) (S (NP (JJ gay) (NN marriage)) (ADJP (JJ legal))))) (VP (MD will) (VP (VB open) (NP (NP (NNS opportunities)) (PP (IN for) (NP (NP (JJ other) (`` ``) (NNS taboos) ('' '')) (PP (IN in) (NP (PRP$ our) (NN society)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (S (PP (IN in) (S (VP (VBG opening) (NP (NP (DT this) (NN opportunity)) (PP (IN for) (NP (JJ gay) (NN marriage))))))) (, ,) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (MD will) (VP (VB change)))) (CC and) (S (NP (PRP it)) (VP (MD will) (VP (VB allow) (S (NP (NP (JJ other) (NNS issues)) (PP (IN like) (NP (NP (NN incest)) (CC and) (NP (NN polygamy))))) (VP (TO to) (VP (VB become) (ADJP (JJ legal))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (NP (NN hypocracy)))) (. .)))

(S1 (S (PP (IN In) (NP (DT a) (NN sense))) (, ,) (NP (PRP they)) (VP (VBP believe) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (MD will) (VP (AUX have) (NP (NP (DT a) (NN sort)) (PP (IN of) (NP (NN domino) (NN effect)))) (PP (IN for) (NP (JJ other) (NNS issues)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (VP (VBG arguing) (SBAR (IN that) (S (S (VP (VBG using) (S (NP (DT these) (JJ other) (NNS issues)) (VP (TO to) (VP (AUX be) (NP (NP (NN evidence)) (PP (IN for) (SBAR (WHADVP (WRB why)) (S (NP (NN gay-marriage)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN allowed))))))))))))) (VP (AUX is) (RB not) (NP (DT a) (JJ valid) (NN argument))))))) (. .)))

(S1 (S (RB Then) (NP (NNP S1)) (VP (VBZ goes) (ADVP (RB on)) (S (VP (TO to) (VP (VB say) (SBAR (IN that) (S (NP (DT this) (NN tactic)) (VP (AUX is) (VP (VBN used) (S (VP (TO to) (VP (VB instill) (NP (NN fear)) (PP (TO to) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX do) (RB not) (VP (VB support) (NP (DT those) (JJ other) (NNS issues))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ understands) (NP (DT the) (JJ other) (NNS issues))) (, ,) (CC but) (VP (AUX does) (RB not) (VP (VB see) (NP (NP (DT the) (NN relationship)) (PP (IN between) (NP (NP (DT those)) (CC and) (NP (NP (DT that)) (PP (IN of) (NP (JJ same-sex) (NN marriage)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ keeps) (S (VP (VBG arguing) (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (RB not) (VP (AUX be) (ADJP (JJ different)) (S (VP (VBG figuring) (PRT (RP out)) (SBAR (IN whether) (S (NP (PRP one)) (VP (MD can) (VP (VB approve) (NP (NP (JJ gay) (NN marriage)) (CONJP (CC but) (RB not)) (NP (NN incest))) (SBAR (IN since) (S (NP (PRP they)) (VP (VBP believe) (S (NP (DT the) (CD two)) (VP (TO to) (VP (AUX be) (ADJP (RB somewhat) (JJ related))))))))))))))))))))) (. .)))

