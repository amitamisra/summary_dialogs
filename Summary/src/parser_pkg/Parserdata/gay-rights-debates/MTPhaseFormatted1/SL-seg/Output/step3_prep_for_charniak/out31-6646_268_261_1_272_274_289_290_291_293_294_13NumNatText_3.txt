<s> <PARAGRAPH> </s>
<s>  Two people are arguing gay marriage. </s>
<s> S1 states that resorting to name calling indicates the inability to resort to sound arguments. </s>
<s> He then states that the word bigotry is an ugly word, and its ignorant to automatically write off disagreements as "bigotry". </s>
<s> S2 rebuts by saying that he's calling it bigotry because that's what it is. </s>
<s> He then states that if it were another minority being challenged then it would clearly be bigotry. </s>
<s> He states whenever gays are involved a group of people always opposes. </s>
<s> S1 refutes that by saying its all about motivation, if any other minority would try and change traditional marriage, they would be met with the same opposition. </s>
<s>  </s>
