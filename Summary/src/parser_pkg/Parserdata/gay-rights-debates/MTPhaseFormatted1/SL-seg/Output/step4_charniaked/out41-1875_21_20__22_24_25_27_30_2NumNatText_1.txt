(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (JJ homosexual) (NNS partners)) (VP (MD should) (RB not) (VP (VB expect) (NP (NP (VBN shared) (NN healthcare)) (PP (IN in) (NP (DT the) (JJ same) (NN way)))) (SBAR (IN that) (S (NP (DT an) (JJ unmarried) (NN couple)) (VP (MD should) (RB not))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (RB not) (VP (AUXG being) (VP (VBN denied) (NP (DT any) (NNS rights)) (, ,) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (NP (NP (PDT all) (DT the) (JJ same) (NNS rights)) (PP (IN as) (NP (NNS heterosexuals))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (NN nobody)) (VP (MD can) (VP (VB marry) (NP (NP (NP (NN someone)) (PP (IN of) (NP (DT the) (JJ same) (NN sex)))) (, ,) (CONJP (RB not) (RB just)) (NP (NNS homosexuals)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ rejects) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (MD will) (VP (VB succeed) (PP (IN in) (S (VP (VBG legalizing) (NP (JJ same-sex) (NN marriage))))) (, ,) (S (VP (VBG citing) (NP (NP (JJ several) (NNS cases)) (SBAR (WHADVP (WRB where)) (S (NP (JJ homosexual) (NNS marriages)) (VP (AUX were) (VP (VBN banned)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (S (VP (VBG accepting) (SBAR (IN that) (S (NP (DT some) (NNS people)) (VP (MD should) (RB not) (VP (VB receive) (NP (DT the) (JJ basic) (NN right) (S (VP (TO to) (VP (VB marry) (NP (NP (DT the) (NN person)) (SBAR (S (NP (PRP they)) (VP (VBP love))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ likens) (NP (DT this)) (PP (TO to) (NP (NP (NN slavery)) (, ,) (NP (NN prohibition)) (, ,) (CC and) (NP (NP (NNS women) (POS 's)) (NN suffrage))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (DT the) (NNS proponents)) (PP (IN of) (NP (JJ same-sex) (NN marriage)))) (VP (MD will) (ADVP (RB eventually)) (VP (AUX be) (ADJP (JJ successful)) (, ,) (SBAR (RB just) (IN as) (S (NP (JJ human) (NNS rights) (NNS advocates)) (VP (AUX were) (PP (IN with) (NP (NP (NN slavery)) (, ,) (NP (NN voting)) (, ,) (CC and) (NP (JJ interracial) (NN marriage)))))))))))) (. .)))

