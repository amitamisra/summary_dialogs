<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN laws/NNS are/AUX based/VBN on/IN morals/NNS </C>
<C>and/CC because/IN the/DT United/NNP States/NNPS is/AUX a/DT predominantly/RB Christian/JJ nation/NN ,/, </C>
<C>the/DT laws/NNS are/AUX going/VBG to/TO be/AUX biased/VBN toward/IN Christian/NNP beliefs/NNS ./. </C>
</S>
<S>
<C>He/PRP asserts/VBZ that/IN homosexuality/NN has/AUX negative/JJ consequences/NNS and/CC harms/VBZ the/DT integrity/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN people/NNS have/AUX the/DT right/NN to/TO believe/VB freely/RB ,/, </C>
<C>but/CC that/IN the/DT government/NN controls/VBZ behavior/NN ,/, </C>
<C>citing/VBG the/DT freedom/NN of/IN religion/NN </C>
<C>until/IN the/DT practices/NNS do/AUX not/RB conform/VB to/TO society/NN 's/POS rules/NNS ./. </C>
</S>
<S>
<C>He/PRP brings/VBZ up/RP the/DT issue/NN of/IN equality/NN ,/, </C>
<C>claiming/VBG that/IN different/JJ religions/NNS are/AUX not/RB actually/RB equal/JJ </C>
<C>even/RB though/IN the/DT law/NN says/VBZ they/PRP should/MD all/RB be/AUX treated/VBN equally/RB </C>
<C>because/IN although/IN each/DT of/IN the/DT religions/NNS has/AUX the/DT same/JJ rights/NNS ,/, </C>
<C>they/PRP are/AUX still/RB not/RB all/RB treated/VBN equally/RB ./. </C>
</S>
<S>
<C>S2/NNP rejects/VBZ S1/NNP 's/POS differentiation/NN between/IN the/DT right/NN to/TO believe/VB and/CC the/DT right/NN to/TO behave/VB and/CC says/VBZ it/PRP is/AUX inappropriate/JJ to/TO compare/VB homosexuality/NN to/TO non-consensual/JJ behaviors/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN behaviors/NNS like/IN drug/NN trafficking/NN and/CC gambling/NN have/AUX links/NNS to/TO crime/NN and/CC should/MD therefore/RB be/AUX regulated/VBN ,/, </C>
<C>but/CC that/IN there/EX is/AUX no/DT reason/NN to/TO believe/VB that/IN homosexuality/NN is/AUX harmful/JJ </C>
<C>so/IN it/PRP should/MD not/RB be/AUX controlled/VBN ./. </C>
</S>
</P>
</T>
