<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN US/PRP laws/NNS does/AUX not/RB say/VB man/NN and/CC woman/NN </C>
<C>because/IN it/PRP presumes/VBZ that/IN marriage/NN is/AUX between/IN a/DT man/NN and/CC a/DT women/NNS ./. </C>
</S>
<S>
<C>He/PRP makes/VBZ the/DT argument/NN that/IN gay/JJ people/NNS should/MD be/AUX allowed/VBN to/TO marry/VB not/RB because/IN of/IN an/DT automatic/JJ right/NN ,/, </C>
<C>but/CC because/IN it/PRP does/AUX no/DT harm/NN to/TO society/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN in/IN Britain/NNP ,/, gay/JJ marriage/NN has/AUX had/AUX no/DT impact/NN to/TO society/NN ,/, </C>
<C>and/CC even/RB though/IN Christians/NNS believe/VBP gay/JJ sex/NN is/AUX a/DT sin/NN ,/, </C>
<C>many/JJ other/JJ people/NNS commit/VBP sins/NNS and/CC are/AUX still/RB allowed/VBN to/TO marry/VB ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ that/IN the/DT fact/NN that/IN gay/JJ marriage/NN in/IN ancient/JJ Rome/NNP was/AUX allowed/VBN means/VBZ historically/RB it/PRP was/AUX not/RB always/RB between/IN man/NN and/CC women/NNS ,/, </C>
<C>as/IN claimed/VBD ,/, </C>
<C>and/CC gay/JJ marriage/NN does/AUX not/RB threaten/VB Christianity/NNP ./. </C>
</S>
</P>
</T>
