<T>
<P>
</P>
<P>
<S>
<C>S1/RB ,/, a/DT heterosexual/JJ ,/, and/CC S2/NNP ,/, a/DT homosexual/JJ ,/, discuss/VB the/DT justness/NN of/IN the/DT legislative/JJ process/NN ,/, majority/NN rule/NN ,/, </C>
<C>and/CC how/WRB the/DT latter/NN affects/VBZ individuals/NNS and/CC the/DT gay/JJ community/NN ./. </C>
</S>
<S>
<C>S2/NNP describes/VBZ how/WRB the/DT gay/JJ community/NN is/AUX discriminated/VBN against/IN ,/, </C>
<C>and/CC posits/VBZ that/IN because/IN laws/NNS regarding/VBG homosexuals/NNS impact/VB them/PRP more/RBR directly/RB than/IN the/DT general/JJ population/NN ,/, </C>
<C>it/PRP is/AUX unfair/JJ to/TO have/AUX a/DT majority/NN rule/NN apply/VB ./. </C>
</S>
<S>
<C>S1/NNP accuses/VBZ S2/NNP of/IN not/RB understanding/VBG the/DT legal/JJ system/NN and/CC proposes/VBZ that/IN S2/NNP find/VB majority/NN to/TO have/AUX legislation/NN passed/VBN ,/, and/CC not/RB look/VB to/TO the/DT courts/NNS to/TO redress/VB the/DT discrimination/NN ./. </C>
</S>
<S>
<C>S1/NNP analogizes/VBZ that/DT process/NN to/TO how/WRB slavery/NN was/AUX abolished/VBN ,/, and/CC also/RB analogizes/VBZ the/DT alleged/JJ oppression/NN to/TO the/DT oppression/NN that/WDT may/MD be/AUX felt/VBN by/IN anti-abortion/JJ advocates/NNS and/CC child/NN molesters/NNS as/IN a/DT result/NN of/IN majority/NN rule/NN ./. </C>
</S>
</P>
</T>
