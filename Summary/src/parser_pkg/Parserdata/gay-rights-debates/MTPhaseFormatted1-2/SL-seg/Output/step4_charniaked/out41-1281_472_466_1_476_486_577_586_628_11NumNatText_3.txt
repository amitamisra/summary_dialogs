(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN increase)) (PP (IN in) (NP (NP (NN frequency)) (PP (IN of) (NP (NNS earthquakes)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NP (DT the) (NN amount)) (PP (IN of) (NP (NNS earthquakes))) (PP (IN in) (NP (DT the) (JJ last) (NN century)))) (VP (AUX is) (VP (VBG increasing) (ADVP (RB exponentially))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ contends) (SBAR (IN that) (S (NP (EX there)) (VP (VP (AUX has) (VP (AUX been) (NP (NP (CD 20) (JJ large) (NNS earthquakes)) (PP (IN in) (NP (NP (DT the) (JJ latter) (NN half)) (PP (IN of) (NP (DT this) (NN century)))))) (, ,) (PP (VBN compared) (PP (TO to) (NP (QP (RB about) (CD 15))))) (PP (IN for) (NP (DT the) (JJ first) (NN half))))) (CC and) (VP (NP (QP (IN about) (CD 7))) (PP (IN in) (NP (DT the) (JJ entire) (JJ 19th) (NN century)))))))) (. .)))

(S1 (S (S (NP (NP (JJ S2) (NNS retorts)) (SBAR (WHNP (WDT that)) (S (NP (DT the) (NN method)) (VP (VBD used) (S (VP (TO to) (VP (VB detect) (CC and) (VB record) (NP (NNS earthquakes))))))))) (VP (AUX have) (VP (AUX been) (VP (VBG taking) (NP (JJ exponential) (NNS leaps)) (ADVP (RB forward)))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG taking) (NP (NP (NNS statistics)) (PP (IN from) (NP (CD one) (NN source)))) (PP (IN without) (NP (JJ critical) (NN thought))) (, an) (S (VP (VBG accepting) (NP (PRP it)) (PP (IN as) (NP (NN fact))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ rebuts) (S (VP (VBG stating) (SBAR (SBAR (IN that) (S (NP (DT the) (JJ new) (NNS methods)) (VP (VBP detect) (NP (ADJP (JJ minimal) (CC or) (JJ localized)) (NNS earthquakes))))) (CC and) (SBAR (IN that) (S (NP (JJ major) (NNS ones)) (VP (MD can) (VP (AUX be) (VP (VBN detected) (PP (IN without) (NP (DT a) (NN seismograph)))))))))))) (. .)))

