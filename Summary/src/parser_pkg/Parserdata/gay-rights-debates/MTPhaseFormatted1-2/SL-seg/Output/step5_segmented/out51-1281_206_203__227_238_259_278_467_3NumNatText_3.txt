<T>
<P>
</P>
<P>
<S>
<C>S1/NNP :/: All/DT laws/NNS are/AUX based/VBN on/IN morals/NNS 
<M>-/: Christian/NNP morals/NNS ,/, as/IN the/DT U.S./NNP was/AUX founded/VBN as/IN a/DT Christian/NNP country/NN </M>
./. </C>
</S>
<S>
<C>By/IN changing/VBG family/NN standards/NNS </C>
<C>by/IN allowing/VBG gay/JJ marriage/NN ,/, </C>
<C>the/DT integrity/NN of/IN heterosexual/NN marriage/NN and/CC family/NN structure/NN is/AUX no/RB longer/RB stable/JJ ./. </C>
</S>
<S>
<C>Prostitution/NN ,/, polygamy/JJ ,/, human/JJ trafficking/NN ,/, gambling/NN and/CC drug/NN distribution/NN are/AUX all/DT ``/`` consensual/JJ acts/NNS ''/'' as/IN gay/JJ relations/NNS claim/VBP to/TO be/AUX ;/: </C>
<C>should/MD these/DT be/AUX legalized/VBN too/RB ?/. </C>
</S>
<S>
<C>Homosexuality/NN is/AUX linked/VBN to/TO crime/NN ,/, </C>
<C>and/CC many/JJ consider/VBP homosexuality/NN to/TO be/AUX harmful/JJ ./. </C>
</S>
<S>
<C>Equality/NN means/VBZ treating/VBG everyone/NN the/DT same/JJ ,/, </C>
<C>but/CC not/RB treating/VBG every/DT behavior/NN the/DT same/JJ ./. </C>
</S>
<S>
<C>The/DT people/NNS will/MD determine/VB what/WP is/AUX in/IN the/DT public/NN 's/POS best/JJS interest/NN to/TO protect/VB and/CC ban/VB ./. </C>
</S>
<S>
<C>S2/NNP :/: What/WP if/IN belief/NN in/IN God/NNP was/AUX legally/RB allowed/VBN ,/, but/CC worshiping/VBG in/RP churches/NNS or/CC even/RB praying/VBG in/IN private/JJ was/AUX illegal/JJ ?/. A/DT parallel/NN to/TO gay/JJ marriage/NN 
<M>-/: gay/JJ relations/NNS between/IN two/CD consenting/VBG adults/NNS is/AUX fine/JJ ,/, 
<breakWithinParens>but/CC gay/JJ marriage/NN is/AUX not/RB </M>
./. </C>
</S>
<S>
<C>Acts/NNS like/IN prostitution/NN and/CC drug/NN trafficking/NN are/AUX not/RB equal/JJ exchanges/NNS ,/, </C>
<C>and/CC are/AUX linked/VBN to/TO crime/NN ;/: </C>
<C>homosexuality/NN is/AUX not/RB ./. </C>
</S>
<S>
<C>Equality/NN means/VBZ worshiping/JJ whomever/NN you/PRP want/VBP ,/, </C>
<C>not/RB what/WP the/DT majority/NN believes/VBZ is/AUX correct/JJ ./. </C>
</S>
</P>
</T>
