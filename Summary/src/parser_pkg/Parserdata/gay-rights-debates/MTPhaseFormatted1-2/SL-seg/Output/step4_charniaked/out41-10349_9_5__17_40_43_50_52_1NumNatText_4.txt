(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT the) (JJ cultural) (NN meaning)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (NP (NP (DT a) (NN formalization)) (PP (IN of) (NP (NP (DT a) (NN mating) (NN pair)) (, ,) (SBAR (WHNP (WP who)) (S (VP (VB create) (CC and) (VB support) (NP (DT a) (NN family) (NN unit)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (PP (IN in) (NP (NP (NN order)) (PP (IN for) (NP (NNS homosexuals))) (SBAR (S (VP (TO to) (VP (AUX be) (VP (VBN included)))))))) (, ,) (NP (NP (NP (DT the) (NN definition)) (CC and) (NP (JJ cultural) (NN meaning))) (PP (IN of) (NP (NN marriage)))) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB change) (S (VP (TO to) (VP (VB accommodate) (NP (PRP it))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (NP (NP (DT the) (ADJP (RBS most) (JJ dominant)) (JJ cultural) (NN view)) (PP (IN on) (NP (NN marriage)))) (VP (AUX is) (VP (VBN based) (PP (IN upon) (NP (JJ evolutionary) (JJ sound) (NN logic))))))) (, ,) (SBAR (IN that) (S (NP (NP (DT a) (JJ monogamous) (NN pair)) (PP (IN of) (NP (NNS individuals)))) (VP (MD will) (VP (VB reproduce)))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (DT the) (NN argument)) (VP (VBZ stands) (PP (IN on) (NP (NN equality))) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NN equality)) (VP (NNS factors) (PP (IN into) (NP (NP (DT the) (JJ evolutionary) (NN view)) (PP (IN of) (NP (NN marriage))))))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VP (VBZ argues) (PP (IN against) (NP (DT the) (NN idea))) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (VBN set) (, ,) (JJ evolutionary) (NN definition)) (PP (IN of) (NP (NN marriage)))))))) (CC and) (VP (VBZ uses) (NP (DT the) (NN example) (SBAR (IN that) (S (NP (NP (NNS people)) (PP (ADVP (RB well)) (IN over) (NP (NP (DT the) (NN age)) (PP (IN of) (NP (NN reproduction)))))) (VP (AUX are) (RB not) (ADVP (RB only)) (ADJP (RB legally) (JJ capable) (PP (IN of) (S (VP (VBG getting) (VP (VBN married))))))))))))) (, ,) (CC but) (S (ADVP (RB also)) (VP (VBP suffer) (NP (NP (DT no) (JJ special) (NN stigma)) (PP (IN for) (S (VP (VBG doing) (ADVP (RB so)))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (WHNP (WP what)) (S (VP (VBZ defines) (SBAR (S (NP (NN marriage)) (VP (AUX is) (NP (NP (NN love)) (PP (IN between) (NP (NP (CD two) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP desire) (NP (NP (DT a) (NN commitment)) (PP (IN between) (NP (PRP themselves))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (NP (JJ many)) (VP (AUX do) (RB not) (VP (VB share) (NP (NP (NP (NNP S1) (POS 's)) (NN definition)) (PP (IN of) (NP (NN marriage)))))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX 's) (NP (NP (DT an) (NN issue)) (PP (IN of) (NP (NP (JJ personal) (NN belief)) (CONJP (RB rather) (IN than)) (NP (NN culture)))))))))) (. .)))

