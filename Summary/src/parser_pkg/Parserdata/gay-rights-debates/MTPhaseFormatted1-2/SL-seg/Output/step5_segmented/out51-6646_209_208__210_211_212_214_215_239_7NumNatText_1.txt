<T>
<P>
</P>
<P>
<S>
<C>S1/JJ References/NNS S2/VBP that/IN marriage/NN is/AUX a/DT public/JJ institution/NN limited/VBN by/IN law/NN ./. </C>
</S>
<S>
<C>This/DT is/AUX subject/JJ to/TO the/DT will/NN of/IN the/DT people/NNS expressed/VBN in/IN a/DT vote/NN ./. </C>
</S>
<S>
<C>It/PRP 's/AUX ok/JJ to/TO lobby/NN on/IN either/DT side/NN ./. </C>
</S>
<S>
<C>S2/NNP Calls/VBZ for/IN empathy/JJ </C>
<C>and/CC that/IN people/NNS are/AUX denying/VBG gays/NNS what/WP everyone/NN else/RB has/AUX ./. </C>
</S>
<S>
<C>S1/NNP Claims/VBZ it/PRP is/AUX ok/JJ for/IN people/NNS to/TO believe/VB </C>
<C>as/IN they/PRP wish/VBP on/IN the/DT issue/NN and/CC can/MD lobby/VB on/IN it/PRP ,/, </C>
<C>but/CC not/RB to/TO belittle/VB those/DT who/WP oppose/VBP it/PRP due/JJ any/DT reason/NN ./. </C>
</S>
<S>
<C>S2/NNP Refers/VBZ back/RB to/TO the/DT empathy/JJ question/NN and/CC wishes/VBZ S1/NNP to/TO imagine/VB a/DT reversed/JJ world/NN where/WRB being/JJ heterosexual/NN denied/VBD you/PRP marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP Claims/VBZ it/PRP 's/AUX not/RB injustice/NN </C>
<C>because/IN it/PRP 's/AUX not/RB denying/VBG something/NN that/IN you/PRP 're/AUX allowed/VBN to/TO do/AUX and/CC refers/VBZ to/TO society/NN 's/POS right/NN to/TO determine/VB its/PRP$ own/JJ rules/NNS ./. </C>
</S>
<S>
<C>S2/NNP Refers/VBZ to/TO similarity/NN between/IN gay/JJ marriage/NN and/CC slavery/NN ,/, refers/VBZ to/TO empathy/NN and/CC claims/VBZ cruelty/NNP to/TO deny/VB gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP Denies/VBZ similarity/NN and/CC degree/NN between/IN gay/JJ marriage/NN and/CC slavery/NN ,/, Again/RB refers/VBZ to/TO society/NN determining/VBG own/JJ rules/NNS ./. </C>
</S>
<S>
<C>S2/NNP States/NNP not/RB comparing/VBG the/DT two/CD issues/NNS just/RB applying/VBG S1/NNP 's/POS beliefs/NNS and/CC standards/NNS to/TO slavery/NN ./. </C>
</S>
</P>
</T>
