<T>
<P>
</P>
<P>
<S>
<C>S1/NNP suggests/VBZ that/IN statistics/NNS on/IN divorce/NN rates/NNS are/AUX skewed/VBN by/IN the/DT high/JJ proportion/NN of/IN young/JJ people/NNS who/WP end/VBP up/RP getting/VBG divorces/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN the/DT divorce/NN rate/NN drops/VBZ significantly/RB </C>
<C>if/IN you/PRP focus/VBP only/RB on/IN couples/NNS who/WP have/AUX been/AUX marriage/NN more/JJR than/IN five/CD years/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB claims/VBZ that/IN statistics/NNS show/VBP that/IN homosexuals/NNS are/AUX more/RBR promiscuous/JJ than/IN heterosexuals/NNS </C>
<C>and/CC that/IN this/DT would/MD lead/VB to/TO a/DT higher/JJR divorce/NN rate/NN for/IN same-sex/JJ marriages/NNS ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN the/DT age/NN of/IN the/DT spouses/NNS is/AUX irrelevant/JJ </C>
<C>and/CC that/DT more/JJR than/IN half/PDT of/IN all/DT heterosexual/JJ marriages/NNS end/VBP in/IN divorce/NN and/CC suggests/VBZ that/IN it/PRP would/MD likely/RB be/AUX no/DT difference/NN for/IN same-sex/JJ marriages/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB rejects/VBZ the/DT comparison/NN of/IN promiscuity/NN between/IN heterosexuals/NNS and/CC homosexuals/NNS </C>
<C>by/IN pointing/VBG out/RP that/IN only/JJ heterosexuals/NNS are/AUX legally/RB allowed/VBN to/TO marry/VB ./. </C>
</S>
</P>
</T>
