(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NNS economics)) (PP (IN of) (NP (JJ gay) (NN marriage)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (VBZ poses) (NP (NP (DT a) (JJ tremendous) (JJ new) (NN tax)) (PP (IN on) (NP (DT the) (NN economy)))) (SBAR (IN as) (S (NP (PRP it)) (VP (MD would) (VP (VB create) (NP (NP (DT a) (JJ new) (JJ dependent) (NN class)) (SBAR (WHNP (WDT that)) (S (VP (AUX does) (RB not) (ADVP (RB presently)) (VP (VB exist)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ compares) (NP (NP (JJ same-sex) (NN marriage)) (PP (TO to) (NP (JJ single) (NN couple) (NNS roommates))))) (CC and) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (RB not) (PP (IN in) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))))))) (, ,) (NP (PRP it)) (VP (AUX is) (RB not) (NP (NN marriage))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN since) (S (NP (JJ gay) (NNS people)) (VP (VB pay) (PP (IN into) (NP (DT the) (JJ same) (NN system)))))) (, ,) (NP (PRP they)) (VP (MD should) (ADVP (RB also)) (VP (VB benefit) (PP (IN from) (NP (PRP it)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (PP (IN by) (S (RB not) (VP (VBG allowing) (NP (JJ gay) (NN marriage))))) (, ,) (SBAR (IN if) (S (NP (DT a) (NN divorce)) (VP (VBZ occurs) (NP (DT the) (NN state))))) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB care) (PP (IN for) (NP (NP (DT the) (JJ dependent) (NN partner)) (, ,) (CONJP (RB instead) (IN of)) (NP (NP (DT the) (JJR richer) (NN partner)) (PP (IN as) (PP (IN in) (NP (JJ heterosexual) (NNS marriages))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT a) (JJ federal) (NN marriage) (NN amendment)) (PP (TO to) (NP (DT the) (NN constitution)))) (VP (AUX is) (NP (DT a) (JJ long) (NN shot)))))) (. .)))

