(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT all) (NNS homosexuals)) (VP (AUX are) (ADJP (VBN doomed) (PP (TO to) (NP (NP (DT the) (JJ fiery) (NNS pits)) (PP (IN of) (NP (NN hell)))))) (, ,) (S (VP (VP (VBG grouping) (NP (NP (PRP them)) (NP (DT all))) (ADVP (RB together))) (CC and) (VP (VBG calling) (S (NP (PRP them)) (VP (VB godless) (NP (NNS heathens))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX is) (PP (IN of) (NP (DT the) (NN mind) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (PRP$ his) (JJ christian) (NN duty)) (SBAR (S (VP (TO to) (VP (VB inform) (NP (PRP them)) (PP (IN of) (NP (DT this))) (, ,) (SBAR (IN as) (S (NP (NP (NN non)) (PP (IN of) (NP (NP (NNP S1) (POS 's)) (NNS brethren)))) (VP (MD will))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (SBAR (IN until) (S (NP (DT some) (JJ scholarly) (NN evidence)) (VP (AUX is) (ADJP (VBN produced) (SBAR (IN that) (S (VP (VBZ refutes) (NP (NP (DT the) (NN resurrection)) (PP (IN of) (NP (NNP Christ))))))))))) (, ,) (NP (DT any) (NNS non-believers)) (VP (AUX are) (ADVP (RB just)) (NP (JJ noisy) (NNP Christ) (NNS deniers)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ uses) (NP (NP (JJ strong) (NNS words)) (, ,) (NP (NNS threats)) (, ,) (CC and) (NP (NNS insults)) (, ,)) (S (VP (TO to) (VP (VB validate) (NP (NP (PRP$ his) (NN argument)) (, ,) (CONJP (RB as) (RB well) (IN as)) (NP (DT a) (NN story))) (PP (IN on) (NP (NNP Fox) (NN news))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ calls) (NP (NP (NNP S1) (POS 's)) (JJ entire) (NN argument)) (PP (IN into) (NP (NN question)))) (, ,) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (PRP he)) (VP (MD will) (VP (AUX be) (VP (VBG going) (S (VP (TO to) (VP (NN Hell) (, ,) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (AUX does) (RB not) (VP (VB exist))))) (, ,) (CC and) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN proof)) (PP (TO to) (NP (DT the) (NN contrary))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NP (NN proof)) (PP (IN of) (NP (NP (DT the) (NN resurrection)) (PP (IN of) (NP (NNP Christ)))))) (VP (VBD existed)))) (NP (PRP it)) (VP (MD would) (VP (AUX have) (ADVP (RB already)) (VP (AUX been) (VP (VBN produced)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (S (VP (VP (VBG dodging) (NP (DT the) (NN argument))) (CC and) (VP (VBG attempting) (S (VP (TO to) (VP (VB reverse) (NP (NP (DT the) (NN burden)) (PP (IN of) (NP (NN proof)))) (, ,) (SBAR (S (NP (PRP he)) (VP (VP (VBZ calls) (S (NP (DT this) (NN tactic)) (ADJP (JJ pathetic)))) (CC and) (VP (VBZ uses) (NP (NNP Fox)) (PP (IN as) (NP (NP (NN proof)) (PP (IN of) (NP (DT this) (NN point))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB think) (SBAR (IN that) (S (NP (NP (NNS insults)) (CC and) (NP (NNS threats))) (VP (AUX are) (NP (NP (DT an) (ADJP (JJ intelligent) (CC or) (JJ useful)) (NN form)) (PP (IN of) (NP (NN debate)))))))))))) (. .)))

