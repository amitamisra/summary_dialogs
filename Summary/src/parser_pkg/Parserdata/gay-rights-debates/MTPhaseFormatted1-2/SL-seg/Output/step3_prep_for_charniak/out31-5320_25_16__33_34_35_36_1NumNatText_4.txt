<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage and the Ccnstitution. </s>
<s> S1 contends that eight out of nine state supreme courts that have taken up the issue has ruled banning same-sex marriage unconstitutional, based on their state constitutions that closely parallel the federal one. </s>
<s> He states that the nature of marriage is both legal and religious, and that the courts only deal with the legal aspect. </s>
<s> S2 states that the Massachusetts ruling was based on a specific provision in their constitution that prohibits discrimination based on sexual discrimination, which is not founded in the federal constitution, to which S1 states the ruling was on the equal protection grounds. </s>
<s> S2 then states that many state constitutions guarantee rights that are not in the federal constitution. </s>
<s>  </s>
