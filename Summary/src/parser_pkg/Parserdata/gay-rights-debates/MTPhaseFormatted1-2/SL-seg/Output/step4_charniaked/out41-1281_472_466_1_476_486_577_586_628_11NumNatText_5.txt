(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ notes) (SBAR (IN that) (S (NP (NP (DT the) (NN number)) (PP (IN of) (NP (NP (NNS earthquakes)) (PP (IN outside) (NP (NNP Israel)))))) (VP (AUX has) (VP (VBN increased) (S (NP (PRP I)) (ADJP (NP (DT the) (JJ last) (QP (CD one) (CD hundred)) (NNS years)) (RB exponentially)))))))) (. .)))

(S1 (S (NP (PRP He) (CC or) (PRP she)) (VP (VBZ quotes) (NP (NP (DT the) (NNS lists)) (PP (IN of) (`` ``) (NP (NP (NP (NNP World) (POS 's)) (ADJP (RBS Most) (JJ Dangerous)) (NNS Earthquakes)) (CC and) (NP (NP (JJ Selected) (NNS Earthquakes)) (PP (IN in) (NP (DT the) (NNP United) (NNPS States))))) ('' ''))) (PP (PP (IN from) (NP (NP (DT the) (NNP Citizen) (POS 's)) (NNP Guide))) (PP (TO to) (NP (JJ Geologic) (NNS Hazards))))) (. .)))

(S1 (S (NP (PRP He) (CC or) (PRP she)) (VP (VBZ notes) (NP (NP (QP (RB about) (CD twenty)) (JJ large) (NNS earthquakes)) (PP (ADVP (RB globally)) (IN in) (NP (NP (DT the) (JJ latter) (NN half)) (PP (IN of) (NP (DT the) (NN century)))))) (PP (VBN compared) (PP (TO to) (NP (NP (QP (RB about) (CD fifteen))) (PP (IN for) (NP (NP (NP (DT the) (JJ first) (NN half)) (PP (IN of) (NP (DT this) (NN century)))) (CC and) (NP (NP (QP (RB only) (CD seven)) (JJ large) (NNS earthquakes)) (VP (VBN recorded) (PP (IN in) (NP (DT the) (JJ entire) (JJ nineteenth) (NN century))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (DT the) (NNP United) (NNP States) (NN Government)) (VP (AUX is) (VP (VBN ruled) (PP (IN by) (NP (DT the) (NN majority))) (SBAR (IN until) (S (NP (PRP it)) (VP (VBZ violates) (NP (DT the) (NN constitution)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ refutes) (NP (DT this)) (PP (IN by) (S (VP (VBG stating) (SBAR (SBAR (IN that) (S (NP (NP (JJ technological) (NNS advances)) (PP (IN in) (NP (NN earthquake) (NN tremor) (NN detection)))) (VP (AUX has) (VP (VBN improved) (ADVP (RB greatly)) (PP (IN since) (NP (DT the) (JJ nineteenth) (NN century))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (NNP S1) (POS 's)) (NN argument)) (ADVP (RB purposefully)) (VP (VBZ excludes) (NP (DT this) (NN fact)))))))))) (. .)))

