<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing anti-gay bias and the nature of prejudice versus valid religious beliefs. </s>
<s> S1 believes it is prejudicial to hold anti-gay opinions, whether based on religious values or otherwise, and use them to impose ones views on another. </s>
<s> S2 agrees that religious belief can be improperly used as the pretext for acts of prejudice against the gay community, but proposes that only people who are lying about having religious values against homosexuality should be accused of being prejudiced. </s>
<s> S2 asserts vaguely that her belief that "Christ is a resurrected being" somehow absolves her of prejudice in being generally opposed to homosexuality. </s>
<s> S1 seems inclined to grant her leeway based on such a religious objection, but requests more specifics as to why a Christian must oppose homosexuality and admonishes S1 not to actively agitate against the gay community or advocate for public policies which are discriminatory. </s>
<s> S1 is opposed to groups such as "Focus on the Family" ( may be a misnomer) who attempt to impose their views on others. </s>
<s>  </s>
