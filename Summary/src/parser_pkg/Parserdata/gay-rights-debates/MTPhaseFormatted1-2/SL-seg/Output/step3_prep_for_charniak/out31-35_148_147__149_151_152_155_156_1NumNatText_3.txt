<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage. </s>
<s> S2 feels Christians are trying to, in a sense, ban gay people by banning all activities pertaining to gays such as marriage, civil unions, and gay adoption. </s>
<s> S1 feels this is not the case and that most Christians do not treat gay people in this fashion. </s>
<s> S1 also feels the issue at hand is not being discussed as the forum began with speaking about the gay pride industry. </s>
<s> S2 supports his theory by citing the constitutional amendment to ban both gay marriage and same sex civil unions. </s>
<s> He feels this is the Christian equivalent of banning gay people by oppressing their lifestyle. </s>
<s> S1 feels this is a weak argument which gay lobbyists have overused. </s>
<s>  </s>
