(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (AUX are) (NP (NP (DT the) (NNS ones)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBD made) (NP (NP (NNP AIDS)) (NP (DT a) (JJ major) (NN health) (NN concern))) (PP (IN in) (NP (DT the) (NNP US)))) (CC and) (VP (VBZ asks) (PP (IN for) (NP (NP (NN proof)) (PP (TO to) (NP (DT the) (NN contrary)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (S (NP (NNP S2)) (VP (AUX is) (ADVP (RB simply)) (VP (VBG giving) (NP (NP (NNS opinions)) (CC and) (RB not) (NP (NNS facts)))))) (CC and) (S (SBAR (WHADVP (WRB when)) (S (NP (PRP he)) (VP (VBZ quotes) (NP (PRP him))))) (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX is) (VP (VBN flattered) (SBAR (IN that) (S (NP (NP (DT a) (NN quote)) (PP (IN from) (NP (PRP him)))) (VP (AUX is) (NP (NP (DT the) (JJS best) (JJ intelligent) (NN statement)) (SBAR (S (NP (PRP he)) (VP (MD could) (VP (VB come) (PRT (RP up)) (PP (IN with))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ wishes) (S (VP (TO to) (VP (VB debate) (NP (NNP S2)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT no) (NN one)) (VP (VBZ knows) (SBAR (WHADVP (WRB how)) (S (NP (NNP AIDS)) (VP (VBD came) (PP (TO to) (NP (DT the) (NP (PRP US)) (CC and) (NP (NP (NNS states)) (SBAR (WHNP (WDT that)) (S (NP (NP (NP (NNS people)) (VP (VBG using) (NP (JJ dirty) (NNS needles)))) (, ,) (NP (NP (DT those)) (VP (VBG participating) (PP (IN in) (NP (JJ unsafe) (NN sex))))) (, ,) (CC and) (NP (DT those))) (VP (VBN involved) (PP (IN in) (NP (NN blood) (NNS transfusions))) (SBAR (IN before) (S (NP (PRP they)) (VP (VBD knew) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (AUX was) (VP (VBN spread))))) (SBAR (S (VP (AUX were) (NP (NP (DT the) (NNS ones)) (SBAR (WHNP (WDT that)) (S (VP (VBD made) (NP (PRP it)) (NP (DT a) (NN health) (NN concern))))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ quotes) (NP (NNP S1)) (PP (IN as) (NP (NP (NN evidence)) (PP (IN of) (NP (NP (PRP$ his) (NN hypocrisy)) (PP (IN in) (S (VP (VBG requesting) (NP (NN evidence)) (SBAR (WHADVP (WRB when)) (S (NP (PRP he)) (VP (VBZ offers) (NP (NN none))))))))))))) (. .)))

