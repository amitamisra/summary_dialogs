<s> <PARAGRAPH> </s>
<s>  S1 provides a link to a quiz to see where ones views line up with those of the Supreme Court and the American public. </s>
<s> He thinks the problem with the quiz is that it is opinion based. </s>
<s> S1 thinks the Supreme Court should not rule based on their opinions, but rather on their interpretation of the Constitution. </s>
<s> Also, he thinks labels such as conservative are incorrect as they express a political view and not judicial interpretation. </s>
<s> S2 dislikes the quiz. </s>
<s> He thinks people are making courts a legislative body and if rulings are opinion based Congress should do it as they are voted in. </s>
<s> He agrees that the rulings should not be considered liberal or conservative and gives an example of corporations running ad campaigns. </s>
<s>  </s>
