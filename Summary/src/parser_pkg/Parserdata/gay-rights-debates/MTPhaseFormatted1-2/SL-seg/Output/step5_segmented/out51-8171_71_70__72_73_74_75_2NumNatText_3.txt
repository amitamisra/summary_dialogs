<T>
<P>
</P>
<P>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- states/VBZ that/IN one/CD who/WP supports/VBZ gay/JJ right/NN but/CC not/RB gay/JJ marriage/NN brings/VBZ an/DT inconsistent/JJ problem/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN he/PRP does/AUX not/RB sees/VBZ any/DT inconsistency/NN ,/, </C>
<C>and/CC compares/VBZ it/PRP to/TO the/DT issue/NN of/IN abortion/NN </C>
<C>and/CC how/WRB one/PRP can/MD be/AUX against/IN such/PDT issue/NN for/IN society/NN </C>
<C>but/CC for/IN ones/NNS position/NN it/PRP may/MD be/AUX possible/JJ ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- tells/VBZ -LRB-/-LRB- S2/NNP -RRB-/-RRB- that/IN it/PRP seems/VBZ like/IN he/PRP does/AUX not/RB understand/VB the/DT concept/NN of/IN inconsistency/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- also/RB says/VBZ that/IN he/PRP hopes/VBZ that/IN -LRB-/-LRB- S2/NNP -RRB-/-RRB- did/AUX not/RB think/VB of/IN gay/JJ marriage/NN to/TO be/AUX a/DT right/NN ./. </C>
</S>
<S>
<C>They/PRP keep/VBP arguing/VBG about/RP positions/NNS in/IN the/DT matter/NN </C>
<C>and/CC who/WP is/AUX inconsistent/JJ ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN many/JJ people/NNS often/RB times/NNS say/VB things/NNS such/JJ as/IN ``/`` I/PRP do/AUX not/RB believe/VB gay/JJ marriage/NN is/AUX a/DT right/NN ''/'' </C>
<C>but/CC they/PRP also/RB believe/VBP that/IN gay/JJ people/NNS should/MD have/AUX equal/JJ rights/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN Biden/NNP 's/POS opinion/NN is/AUX consistent/JJ </C>
<C>and/CC Biden/NNP is/AUX not/RB lying/VBG about/IN what/WP he/PRP believes/VBZ ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- says/VBZ that/IN he/PRP know/VBP that/IN there/EX is/AUX inconsistency/JJ </C>
<C>but/CC he/PRP does/AUX not/RB care/VB ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN people/NNS weight/VB the/DT ay/CD marriage/NN opinion/NN issue/NN against/IN politician/NN 's/POS weeks/NNS before/IN elections/NNS and/CC balance/VB the/DT responding/VBG approach/NN ./. </C>
</S>
</P>
</T>
