(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (NP (JJ gay) (NN marriage)) (CC and) (NP (NN polygamy))) (VP (AUX are) (ADJP (JJ commonplace) (PP (IN in) (NP (JJ ancient) (NNP Greece) (, ,) (NNP Rome) (CC and) (NNP China)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD listed) (NP (NNS hyperlinks)) (PP (TO to) (NP (NP (DT the) (NN source)) (PP (IN of) (NP (PRP$ his) (NN assertion)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NN marriage)) (PP (IN as) (NP (NP (DT a) (NN union)) (PP (IN between) (NP (CD one) (NN man) (CC and) (CD one) (NN woman)))))) (VP (AUX is) (NP (NP (DT a) (JJ recent) (VB construct)) (PP (IN as) (PP (VBN compared) (PP (TO to) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (JJ same-sex) (NN marriage)) (VP (AUX has) (ADVP (RB never)) (VP (AUX been) (ADJP (JJ commonplace))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT a) (NN couple)) (PP (IN of) (NP (NNP Buddhist) (NNS sects)))) (VP (VBP allow) (NP (PRP it)))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX was) (VP (VBN frowned) (PP (IN upon)) (PP (IN in) (NP (NNP Roman) (NNS times))))) (. .)))

(S1 (S (NP (DT The) (NNPS Greeks)) (VP (AUX had) (NP (NP (JJ homosexual) (NNS liaisons)) (, ,) (NP (NP (NP (DT some)) (PP (IN from) (NP (NN desire)))) (, ,) (NP (DT some)))) (PP (IN for) (NP (NP (DT a) (NN form)) (PP (IN of) (NP (JJ mentoring) (NNS reasons)))))) (. .)))

(S1 (S (CC But) (NP (DT those)) (VP (AUX were) (UCP (CC neither) (ADJP (JJ marriages)) (CC nor) (VP (VBN assumed) (S (VP (TO to) (VP (AUX be) (ADVP (RB so))))) (PP (IN by) (NP (DT those) (NNS societies)))))) (. .)))

(S1 (S (S (NP (NN Marriage)) (VP (AUX is) (PP (IN between) (NP (NP (NNS people)) (PP (IN of) (NP (NP (JJ opposite) (NNS genders)) (PP (IN in) (NP (DT those) (NNS societies))))))))) (, ,) (CC and) (S (ADVP (RB thus)) (NP (PRP it)) (VP (AUX is) (RB not) (NP (DT a) (JJ new) (NN concept)))) (. .)))

