(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (PP (IN about) (NP (NNS economics))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (MD would) (VP (VP (VB create) (NP (DT a) (JJ new) (JJ dependent) (NN class))) (CC and) (VP (AUX be) (NP (NP (NP (DT a) (JJ large) (JJ new) (NN tax)) (PP (IN on) (NP (NNS businesses)))) (, ,) (NP (NNP Social) (NNP Security)) (, ,) (CC and) (NP (NN healthcare)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (NP (JJ same) (NN sex) (NNS partners)) (VP (VBG living) (ADVP (RB together)))) (VP (AUX are) (NP (NNS roommates)))))) (. .)))

(S1 (S (NP (NNS Roommates)) (VP (VP (AUX are) (ADJP (JJ single))) (CC and) (VP (MD should) (RB not) (VP (AUX have) (NP (NN marriage) (NNS benefits))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ states) (SBAR (S (NP (PRP he)) (VP (AUX is) (NP (DT a) (NNP Jeffersonian) (NN liberal)))))) (CC and) (VP (VBZ believes) (PP (IN in) (NP (DT the) (NNP Constitution))) (PP (IN as) (NP (NP (JJ written)) (CC and) (NP (NP (DT the) (NNS people) (POS 's)) (NN will)))))) (. .)))

(S1 (S (NP (JJS Most) (NNPS Americans)) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ thinks))) (, ,)) (VP (AUX are) (PP (IN against) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ states) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (VBP pay) (PP (IN into) (NP (DT those) (JJ same) (NNS systems))))))) (CC and) (VP (VBZ believes) (SBAR (S (NP (PRP they)) (VP (MD should) (ADVP (RB also)) (VP (VB benefit) (PP (IN from) (NP (PRP them))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT an) (JJ economic) (NN burden)) (SBAR (WHADVP (WRB when)) (S (NP (JJ same) (NN sex) (NNS couples)) (VP (VBP break) (PRT (RP up)) (PP (IN without) (NP (NP (JJ legal) (NN recognition)) (PP (IN of) (NP (DT the) (NN marriage))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (JJ same) (NN sex) (NNS couples)) (VP (AUX are) (RB not) (ADJP (JJ single)))) (CC and) (S (NP (PRP they)) (VP (AUX are) (RB not) (NP (NP (DT the) (JJ same) (NN thing)) (PP (IN as) (NP (NNS roommates))))))))) (. .)))

