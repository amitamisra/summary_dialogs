(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ states) (SBAR (IN that) (`` ``) (S (NP (PRP it)) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (JJ racial) (NN discrimination)))))) ('' ''))) (, ,) (VP (ADVP (RB in)) (VBZ regards) (PP (TO to) (NP (NP (NN discrimination)) (PP (IN against) (NP (NNS gays)))))) (, ,) (CC and) (VP (VBZ denies) (S (VP (VBG saying) (NP (PRP it)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (IN that) (S (NP (PRP$ his) (NN post)) (VP (VBD implied) (SBAR (IN that) (S (NP (NP (DT the) (NN plight)) (PP (IN of) (NP (NNS gays)))) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (NP (DT the) (NN plight)) (PP (IN of) (NP (NNS blacks)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ asks) (SBAR (WHADVP (WRB why)) (S (NP (NNP S2)) (VP (MD would) (VP (VB think) (SBAR (IN that) (S (NP (PRP he)) (VP (VP (VBD implied) (NP (DT that))) (, ,) (CC and) (VP (VBZ calls) (NP (DT the) (NN assumption) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX did) (ADVP (RB wrong))))))))))))))) (. .)))

(S1 (FRAG (NP (JJ Several) (NNS times)) (S (NP (PRP he)) (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (S (VP (RB not) (VBG knowing) (SBAR (WHNP (WP what)) (S (NP (PRP he)) (VP (AUX is) (VP (VBG talking) (PP (IN about))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX has) (ADVP (RB only)) (VP (VBN succeeded) (PP (IN in) (NP (NP (NN discrediting)) (NP (PRP himself))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ responds) (PP (TO to) (NP (NP (NNP S1)) (VP (VBG saying) (, ,) (`` ``) (S (NP (PRP it)) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (JJ racial) (NN discrimination)))))) ('' '')))) (, ,) (PP (IN with) (NP (DT the) (NN argument) (SBAR (IN that) (S (NP (DT all) (NN segregation)) (, ,) (PP (IN throughout) (NP (NN history))) (, ,) (VP (AUX has) (VP (AUX been) (NP (DT the) (JJ same))))))))) (. .)))

(S1 (S (SBAR (IN That) (S (NP (DT the) (NNS perpetrators)) (VP (AUX have) (VP (VBN used) (NP (DT the) (JJ same) (NN argument)) (S (VP (TO to) (VP (VB support) (NP (PRP it))))))))) (, ,) (NP (RB namely) (DT the)) (VP (VBP claim) (SBAR (IN that) (`` ``) (S (NP (DT this)) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (DT the) (JJ last)))))) ('' '') (CC and) (SBAR (IN that) (S (, ,) (PP (IN without) (NP (NN exception))) (, ,) (NP (PRP it)) (VP (AUX is)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ contends) (SBAR (IN that) (SBAR (IN if) (S (NP (NNP S1)) (VP (AUX had) (RB not) (VP (VBN wanted) (S (VP (TO to) (VP (VB compare) (NP (JJ racial) (NN discrimination)) (PP (TO to) (NP (NP (NN discrimination)) (PP (IN against) (NP (NP (NNS gays)) (SBAR (S (NP (PRP he)) (VP (VP (VBD miss-phrased) (NP (PRP$ his) (NN post))) (, ,) (CC and) (VP (VBZ asks) (S (NP (NNP S1)) (VP (TO to) (VP (VB explain) (NP (PRP himself)))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (S (VP (VBG throwing) (NP (DT a) (NNP temper) (NN tantrum)) (S (VP (TO to) (VP (VB validate) (NP (PRP$ his) (NN argument)) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (NP (DT the) (NN job)) (PP (IN of) (NP (NN discrediting)))) (NP (PRP him))) (VP (AUX is) (ADJP (RB painfully) (JJ easy)))))))))))) (. .)))

