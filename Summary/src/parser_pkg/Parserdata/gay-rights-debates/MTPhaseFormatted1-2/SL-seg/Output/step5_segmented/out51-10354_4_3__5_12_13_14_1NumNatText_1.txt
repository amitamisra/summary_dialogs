<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN the/DT defense/NN of/IN marriage/NN act/NN is/AUX not/RB unconstitutional/JJ ,/, </C>
<C>that/IN the/DT supreme/JJ court/NN has/AUX yet/RB to/TO rule/VB on/IN it/PRP ,/, </C>
<C>and/CC that/IN President/NNP Obama/NNP should/MD be/AUX impeached/VBN for/IN his/PRP$ behaviors/NNS in/IN office/NN ./. </C>
</S>
<S>
<C>He/PRP further/RB suggests/VBZ that/IN proponents/NNS of/IN same-sex/JJ marriage/NN should/MD be/AUX educated/VBN or/CC deported/VBD ./. </C>
</S>
<S>
<C>He/PRP then/RB presents/VBZ a/DT quote/NN from/IN the/DT Bible/JJ to/TO justify/VB why/WRB homosexuality/NN is/AUX wrong/JJ ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN there/EX is/AUX nothing/NN wrong/JJ with/IN homosexuals/NNS </C>
<C>and/CC that/IN they/PRP are/AUX actually/RB an/DT important/JJ part/NN of/IN the/DT country/NN ./. </C>
</S>
<S>
<C>He/PRP cites/VBZ his/PRP$ own/JJ ability/NN as/IN a/DT railway/NN conductor/NN and/CC suggests/VBZ that/IN it/PRP would/MD be/AUX difficult/JJ to/TO find/VB someone/NN to/TO replace/VB him/PRP </C>
<C>if/IN he/PRP were/AUX deported/VBD for/IN being/AUXG a/DT homosexual/JJ ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ that/IN the/DT laws/NNS of/IN America/NNP are/AUX not/RB based/VBN on/IN any/DT religious/JJ texts/NNS ./. </C>
</S>
</P>
</T>
