(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (NP (DT the) (NN amount)) (PP (IN of) (NP (NP (JJ homosexual) (NNS encounters)) (PP (IN at) (NP (NN rest) (NNS stops)))))) (VP (AUX is) (ADJP (RB very) (JJ high))))) (CC and) (SBAR (IN that) (S (NP (NN rest) (NNS stops)) (VP (AUX are) (VP (VBG closing) (PRT (RP down)) (PP (RB because) (PP (IN of) (NP (PRP them)))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (AUX has) (RB not) (VP (VBN found) (NP (NP (DT a) (JJ reasonable) (NN refutation)) (PP (IN of) (NP (DT this) (NN fact)))) (PP (IN in) (NP (NP (PRP$ their) (NNS eyes)) (PP (IN from) (NP (DT any) (JJ gay) (NN supporter))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (AUX does) (RB not) (VP (VB see) (NP (NP (DT any) (NN lack)) (PP (IN of) (NP (NP (JJ moral) (NN character)) (VP (VBN exhibited) (PP (IN by) (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (VP (VBG exercising) (NP (NP (PRP$ their) (JJ Constitutional) (NNS Rights)) (SBAR (S (VP (TO to) (VP (VB deny) (NP (NP (DT the) (NN sanctity)) (PP (IN of) (NP (NN marriage)))) (, ,) (PP (TO to) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP believe) (NP (ADJP (RB as) (JJ immoral)) (NNS associations)))))) (, ,) (PP (IN by) (S (RB not) (VP (VBG allowing) (NP (NP (DT the) (NN definition)) (PP (IN of) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP believe) (S (NP (NN marriage)) (VP (TO to) (VP (AUX be) (S (VP (TO to) (VP (VB change)))))))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN lack)) (PP (IN of) (NP (NP (JJ moral) (NN character)) (PP (IN in) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VB vote) (PRT (RP away)) (NP (NP (JJ other) (NNS people) (POS 's)) (NNS rights)))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP use) (NP (NNP Proposition) (CD 8)) (PP (IN as) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (DT this)))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP compare) (NP (NP (DT the) (NN example)) (PP (IN of) (NP (NP (JJ illicit) (JJ public) (JJ sexual) (NNS acts)) (PP (IN in) (NP (NP (NN rest) (VBZ stops)) (PP (IN between) (NP (NNS homosexuals)))))))) (PP (TO to) (NP (NP (JJ heterosexual) (NNS men)) (SBAR (WHNP (WP who)) (S (ADVP (RB also)) (VP (VBP make) (S (NP (PRP it)) (ADJP (JJ public)) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX do) (NP (DT the) (JJ same)) (PP (IN with) (NP (NP (JJ heterosexual) (NNS women)) (PP (IN in) (NP (JJ public) (NNS places))))))))))))))) (. .)))

(S1 (S (S (NP (DT This) (NN person)) (VP (ADVP (RB somewhat)) (VBZ agrees) (PP (IN with) (NP (NNP S1))) (SBAR (IN that) (S (S (VP (VBG exercising) (NP (JJ Constitutional) (NNPS Rights)))) (VP (AUX is) (RB not) (ADJP (RB morally) (JJ objectionable))))))) (, ,) (CC but) (S (S (VP (VBG using) (NP (DT that)) (PP (IN as) (NP (NP (DT a) (NN platform)) (SBAR (S (VP (TO to) (VP (VB deny) (NP (NP (NN marriage) (NNS rights)) (PP (TO to) (NP (NN loving)))))))))))) (, ,) (NP (JJ committed) (JJ homosexual) (NNS couples)) (VP (AUX is))) (. .)))

