(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (-LRB- -LRB-) (NP (NNP S1)) (-RRB- -RRB-) (VP (NNS states))) (SBARQ (WHADVP (WRB how)) (SQ (MD can) (NP (JJ gay) (NN marriage)) (VP (VB hurt) (NP (NNS politicians) (CC or) (NN anyone) (RB else)))) (. ?))))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VP (VBZ says) (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (NP (NP (NNS laws)) (SBAR (WHNP (WDT that)) (S (VP (VBP hurt) (NP (NP (DT all) (NNS kinds)) (PP (IN of) (NP (NNS peoples)))))))))))) (CC and) (VP (VBZ argues) (PP (IN about) (NP (NP (NN violation)) (PP (IN of) (NP (DT the) (NNP First) (NNP Amendment))))) (PP (IN by) (S (VP (VBG imposing) (NP (NP (NN someone) (POS 's)) (NN belief)) (PP (IN over) (NP (DT another)))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (VP (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (NP (NP (JJ only) (NN concern)) (PP (IN with) (NP (JJ gay) (NNS peoples) (NNS rights)))))))) (CC and) (VP (VBZ says) (SBAR (IN that) (S (NP (DT the) (NN politician)) (VP (VBZ wants) (S (NP (NNS gays)) (VP (TO to) (VP (VB remain) (PP (IN as) (NP (JJ second-class) (NNS citizens))))))))))) (. .)))

(S1 (S (VP (VBZ Argues) (SBAR (IN that) (S (NP (NN politician/she)) (VP (AUX is) (VP (VBG discriminating) (CC and) (VBG protecting) (NP (PRP$ her) (JJ heterosexual) (NN privilege))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ argues) (SBAR (IN that) (S (S (RB not) (VP (VBG accepting) (NP (JJ gay) (NN marriage)))) (VP (AUX is) (NP (NP (NN punishment)) (PP (TO to) (NP (NP (PRP him)) (, ,) (NP (PRP$ his) (NN family)) (CC and) (NP (NNS others))))))))) (. .)))

(S1 (S (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-) (NP (PRP He)) (VP (VBZ argues) (PP (IN about) (NP (NP (DT the) (NN kind)) (PP (IN of) (NP (NP (NN democracy)) (PP (IN in) (SBAR (SBAR (WHADVP (WRB where)) (S (NP (NP (NN majority)) (PP (IN of) (NP (DT the) (NNS people)))) (VP (VBP decide) (SBAR (WHNP (WP what)) (S (NP (JJS best)) (PP (IN for) (NP (NN society)))))))) (CC and) (SBAR (WHADVP (WRB how)) (S (S (VP (AUXG being) (ADJP (JJ anti-gay)))) (VP (AUX is) (NP (NP (DT a) (NN right)) (PP (IN of) (NP (DT the) (NNP First) (NNP Amendment)))))))))))))) (. .)))

(S1 (S (VP (AUX Is) (NP (DT a) (JJ gay) (NN person))) (VP (AUX is) (VP (VBN allowed) (S (VP (TO to) (VP (AUX have) (NP (DT the) (NN freedom)) (, ,) (SBAR (RB so) (S (VP (AUX does) (NP (NP (PRP he)) (WP who) (CC and) (NP (NP (NNS others)) (SBAR (WHNP (WP who)) (S (VP (MD may) (RB not) (VP (VB agree))))))))))))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S1) (-RRB- -RRB-)) (ADVP (RB then)) (VP (VBZ gives) (NP (DT the) (NN example) (SBAR (IN that) (S (PP (ADVP (RB not) (RB just)) (IN because) (NP (NN majority) (NNS rules))) (NP (PRP it)) (VP (VBZ means) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX 's) (NP (DT a) (JJ good) (NN thing)))))))))) (. .)))

(S1 (S (PP (IN For) (NP (NN example))) (, ,) (NP (JJ enslaving) (NNS people)) (VP (AUX was) (VP (VBN accepted) (PP (IN in) (NP (JJ previous) (NNS societies))))) (. .)))

(S1 (S (NP (-LRB- -LRB-) (NNP S2) (-RRB- -RRB-)) (VP (VBZ argues) (SBAR (IN that) (SBAR (IN although) (S (NP (JJ previous) (NNS societies)) (VP (AUX had) (NP (NP (JJ legal) (NNS rights)) (PP (TO to) (NP (JJ enslave) (NNS people))))))) (CC but) (SBAR (IN that) (S (VP (AUX does) (RB not) (VP (VB mean) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (VBZ makes) (NP (DT those) (NNS decisions)) (NP (PDT all) (DT the) (NN time))))) (CC and) (SBAR (IN that) (S (VP (AUX is) (PP (IN like) (NP (JJ gay) (NN marriage))))))))))))) (. .)))

