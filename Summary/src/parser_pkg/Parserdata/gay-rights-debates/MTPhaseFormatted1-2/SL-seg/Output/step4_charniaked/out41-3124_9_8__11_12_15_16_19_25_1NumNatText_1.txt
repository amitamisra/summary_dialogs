(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (ADVP (RB Prior) (PP (TO to) (NP (DT this) (NN dialog)))) (, ,) (NP (NNP S2)) (ADVP (RB apparently)) (VP (VBD likened) (NP (DT the) (JJ gay) (NN marriage) (NN movement)) (PP (TO to) (NP (NP (DT the) (JJ Civil) (NNS Rights) (NN movement)) (PP (IN of) (NP (DT the) (NNP African) (NNP American) (NN community)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (VP (VBN offended) (PP (IN by) (NP (DT the) (JJ alleged) (NN analogy))) (, ,) (S (VP (VBG finding) (NP (NP (DT a) (JJ material) (NN distinction)) (PP (IN between) (NP (NP (NN race)) (CC and) (NP (JJ sexual) (NN orientation))))))))) (. .)))

(S1 (S (NP (PRP She)) (VP (VBZ suggests) (SBAR (IN that) (S (PP (TO to) (NP (NN view))) (NP (PRP it)) (ADVP (RB otherwise)) (VP (MD would) (VP (AUX be) (NP (NP (DT an) (NN insult)) (PP (TO to) (NP (DT all) (NNP African) (NNPS Americans))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (NP (CD two) (ADJP (RB presumably) (JJ prominent)) (NNP African) (NNP American) (NNS figures)) (, ,) (NP (NP (NNP Corretta) (NNP Scott) (NNP King)) (CC and) (NP (NNP Jullian) (NNP Bond))) (, ,)) (VP (AUX are) (ADJP (JJ pro-gay) (NN marriage)))))) (. .)))

(S1 (S (S (VP (VBZ S1) (NP (NNS questions) (SBAR (IN whether) (S (NP (PRP they)) (VP (AUX do) (PP (IN in) (NP (NN fact))) (VP (VB support) (NP (JJ gay) (NN marriage))))))))) (, ,) (CC and) (S (NP (NNP S2)) (VP (VBZ provides) (NP (NP (NNS links)) (SBAR (S (VP (TO to) (VP (VB support) (NP (PRP$ his) (NN assertion))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ accepts) (NP (DT the) (NN evidence))) (CC and) (VP (VBZ concedes) (SBAR (IN that) (S (NP (DT some) (NNP African) (NNPS Americans)) (VP (MD may) (RB not) (VP (VP (AUX be) (RB as) (VP (VBN insulted) (PP (IN by) (NP (DT the) (NN analogy))))) (CC and) (VP (VBZ wonders) (SBAR (WHADVP (WRB how)) (S (NP (NNP Martin) (NNP Luther) (NNP King)) (VP (VBD felt) (PP (IN about) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ gay) (NN marriage))))))))))))))) (. .)))

