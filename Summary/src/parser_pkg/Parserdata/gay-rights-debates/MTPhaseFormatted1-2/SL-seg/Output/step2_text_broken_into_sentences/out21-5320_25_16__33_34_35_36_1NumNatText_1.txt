<PARAGRAPH> S1 and S2 are discussing the issue of marriage equality.
Both subjects seem to be on the fence with the issue.
S1 cites that eight of nine supreme courts have contended same sex marriage to be unconstitutional.
S2 questions how gay marriage would violate someone's constitutional rights.
S1 advises the bulk of the arguments were based on state constitutions which mirror the federal constitution.
S2 uses the example of Massachusetts' allowing gay marriage under its state constitutional law prohibiting discrimination based on sexual orientation.
S1 contends there is no such piece to the Massachusetts constitution.
S2 then concedes he was incorrect about the citing and the law actually banned only discrimination based on gender and not sexual orientation.
