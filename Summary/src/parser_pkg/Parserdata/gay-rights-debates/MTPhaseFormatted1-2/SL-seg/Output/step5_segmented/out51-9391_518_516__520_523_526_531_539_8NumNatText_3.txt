<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ people/NNS are/AUX using/VBG some/DT valid/JJ arguments/NNS for/IN keeping/VBG homosexual/JJ couples/NNS from/IN getting/VBG married/VBN ,/, </C>
<C>but/CC he/PRP does/AUX not/RB necessarily/RB side/VB with/IN all/DT of/IN those/DT arguments/NNS ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN couples/NNS of/IN any/DT kind/NN ,/, gay/JJ or/CC straight/JJ ,/, do/AUX not/RB have/AUX rights/NNS ./. </C>
</S>
<S>
<C>Individuals/NNS are/AUX the/DT ones/NNS who/WP have/AUX rights/NNS ,/, </C>
<C>but/CC those/DT rights/NNS are/AUX limited/VBN by/IN rules/NNS that/IN cannot/NN be/AUX changed/VBN </C>
<C>just/RB because/IN somebody/NN personally/RB wants/VBZ them/PRP changed/JJ ./. </C>
</S>
<S>
<C>For/IN example/NN ,/, a/DT person/NN does/AUX not/RB have/AUX the/DT right/NN to/TO marry/VB someone/NN </C>
<C>if/IN they/PRP are/AUX already/RB married/JJ ,/, or/CC someone/NN who/WP is/AUX a/DT minor/JJ ./. </C>
</S>
<S>
<C>Further/RB ,/, he/PRP believes/VBZ heterosexual/JJ couples/NNS can/MD be/AUX viewed/VBN as/IN more/RBR valid/JJ on/IN an/DT evolutionary/JJ basis/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB there/RB any/DT valid/JJ arguments/NNS to/TO keep/VB homosexuals/NNS from/IN marrying/VBG ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ the/DT issue/NN of/IN gay/JJ marriage/NN is/AUX no/RB different/JJ than/IN than/IN of/IN marriage/NN between/IN different/JJ races/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB disagrees/VBZ with/IN the/DT notion/NN that/IN only/JJ individuals/NNS have/AUX rights/NNS ./. </C>
</S>
<S>
<C>The/DT gay/JJ marriage/NN issue/NN is/AUX largely/RB about/IN the/DT rights/NNS of/IN couples/NNS ,/, including/VBG the/DT right/NN to/TO visit/VB one/NN another/DT in/IN a/DT hospital/NN ,/, the/DT right/NN to/TO inherit/VBP from/IN one/CD another/DT ,/, and/CC the/DT right/NN to/TO file/VB taxes/NNS jointly/RB ./. </C>
</S>
<S>
<C>He/PRP also/RB believes/VBZ the/DT true/JJ reason/NN for/IN people/NNS being/AUXG against/IN gay/JJ marriage/NN is/AUX disapproval/NN of/IN gays/RB having/AUXG sex/NN ./. </C>
</S>
</P>
</T>
