<s> <PARAGRAPH> </s>
<s>  S1 argues that marriage has a specific and deeply embedded definition within culture. </s>
<s> Marriage is a formalized mating pair. </s>
<s> Marriage's legal definition closely mirrors that definition. </s>
<s> Homosexual marriage has never been an explicit or implicit part of those definitions. </s>
<s> Including it would make the definition of marriage inherently self-contradictory. </s>
<s> S2 argues that S1 is exercising a subjective definition. </s>
<s> S2 notes that different members of society have different definitions of marriage. </s>
<s> Further, there are instances of accepted marriage which violate S1's provided definitions. </s>
<s> Instead, love, the emotional connection is what is most important. </s>
<s> S1 argues that legal and cultural definitions are not the same. </s>
<s> The cultural definition is what is important. </s>
<s> Most associate marriage with producing offspring, hence it is deemed important in the cultural definition. </s>
<s> S2 questions S1's authority to determine what is culturally important and whether his cultural definition is appropriate for a legal definition. </s>
<s> S1 says appropriateness is irrelevant. </s>
<s> Cultural definitions have formed the root of all other definitions. </s>
<s> The cultural definition of marriage has always been one male and female in monogamy. </s>
<s>  </s>
