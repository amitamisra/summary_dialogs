<s> <PARAGRAPH> </s>
<s>  S1 argues that in spite of the vitriol employed, comparing interracial and gay marriage is a useful analogy. </s>
<s> Opponents of gay marriage use similar arguments as those employed by opponents of interracial marriage. </s>
<s> S2 argues that this comparison is irrelevant, he has not mentioned religion or interracial marriage in his opposition to gay marriage. </s>
<s> In fact, none of the tropes associated with interracial marriage actually apply. </s>
<s> He has instead argued that redefining marriage would require redefining several other cultural practices as well. </s>
<s> He has also proposed that those seeking to enjoy same sex marriage are doing so out of some form of self-shame. </s>
<s> S1 argues that words and associated cultural practices are redefined all the time, providing the example of "war" to illustrate his point. </s>
<s> In comparison to these major re-definitions, redefining what constitutes a valid marriage seems minor in comparison. </s>
<s> S2 brings up a counterpoint to S1's argument about war. </s>
<s> S1 clarifies S2's argument about war to illustrate how S2's example is in accordance with S1's original statement. </s>
<s> S1 stands by his previous counter-example. </s>
<s>  </s>
