(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ heterosexual) (NN marriage)) (VP (AUX is) (NP (NP (CD one)) (PP (IN of) (NP (NP (DT the) (RBS most) (JJ universal)) (PP (IN of) (NP (DT all) (JJ human) (NNS institutions))) (, ,) (VP (VBN found) (PP (IN in) (NP (NP (RB almost) (DT all) (NNS cultures)) (SBAR (WHNP (WDT that)) (S (VP (AUX have) (ADVP (RB ever)) (VP (VBN existed))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (DT the) (JJ true) (NN question)) (VP (AUX is) (VP (VBG proposing) (NP (NP (DT a) (NN justification)) (PP (IN in) (S (VP (VBG revolutionizing) (NP (DT that) (JJ ancient) (NN institution))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (NP (NP (NNS groups)) (PP (IN of) (NP (NP (NNS homosexuals)) (SBAR (WHNP (WP who)) (S (VP (VBP want) (S (VP (VP (TO to) (VP (VB change) (NP (NN marriage)) (PP (IN for) (NP (JJ various) (NNS reasons))) (, ,) (SBAR (IN whether) (S (NP (PRP it)) (VP (AUX be) (PP (IN for) (NP (ADJP (NN romance) (, ,) (JJ economic) (CC and) (JJ legal)) (NNS reasons)))))))) (CONJP (CC but) (RB also)) (VP (TO to) (VP (VB affirm) (SBAR (IN that) (S (NP (NN homosexuality) (CC and) (NN heterosexuality)) (VP (AUX are) (ADVP (RB just)) (ADJP (ADJP (RB as) (JJ good)) (PP (IN as) (NP (DT each) (JJ other))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP warn) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN risk)) (PP (IN of) (NP (NP (NN backlash)) (PP (IN with) (NP (NP (DT this) (NN way)) (PP (IN of) (NP (NN thinking)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NN homosexuality)) (VP (VP (AUX is) (NP (DT an) (NN eccentricity))) (CC and) (VP (AUX is) (RB not) (ADJP (ADJP (JJ equal) (PP (IN in) (NP (NN value)))) (PP (TO to) (NP (NN heterosexuality))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (S (NP (NNS homosexuals)) (VP (AUX are) (VP (VBG attempting) (S (VP (TO to) (VP (VB destroy) (NP (NP (DT the) (NN idea)) (PP (IN of) (NP (NN marriage)))))))))) (CC but) (S (RB only) (VP (VBP wish) (S (VP (TO to) (VP (AUX be) (VP (VBN included) (PP (IN into) (NP (PRP it))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (MD should) (VP (VP (AUX be) (RB as) (VP (VBN accepted) (PP (RB socially) (IN as) (NP (NNS heterosexuals))))) (CC and) (VP (VB find) (S (NP (PRP it)) (ADJP (JJ difficult) (SBAR (S (VP (TO to) (VP (VB see) (S (NP (NP (DT a) (NN problem)) (PP (IN with) (NP (NNS homosexuals)))) (VP (VP (VBG believing)) (CC and) (VP (VBG acting) (SBAR (IN as) (S (VP (VBZ equals) (PP (TO to) (NP (NNS heterosexuals))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (VBP want) (S (VP (VP (TO to) (VP (AUX have) (NP (NP (DT a) (JJ positive) (NN influence)) (PP (IN on) (NP (DT the) (NN institution))) (PP (IN of) (NP (NN marriage)))))) (, ,) (RB not) (VP (VB destroy) (NP (PRP it))))))))) (. .)))

