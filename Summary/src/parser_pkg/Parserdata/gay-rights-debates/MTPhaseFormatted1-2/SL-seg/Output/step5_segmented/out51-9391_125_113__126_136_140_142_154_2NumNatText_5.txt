<T>
<P>
</P>
<P>
<S>
<C>S1/NNP states/VBZ that/IN when/WRB gays/NNS start/VBP to/TO lose/VB an/DT argument/NN </C>
<C>they/PRP imply/VBP the/DT other/JJ person/NN is/AUX a/DT closet/NN homosexual/JJ ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ that/IN most/JJS people/NNS do/AUX not/RB trust/VB homosexuals/NNS </C>
<C>and/CC that/IN they/PRP use/VBP people/NNS ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ </C>
<C>just/RB because/IN someone/NN is/AUX nice/JJ that/IN is/AUX not/RB a/DT reason/NN to/TO allow/VB them/PRP to/TO marry/VB or/CC approve/VB of/IN deviant/JJ sex/NN ,/, </C>
<C>which/WDT he/PRP considers/VBZ gay/JJ sex/NN to/TO be/AUX ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN while/IN most/JJS people/NNS do/AUX not/RB support/VB gay/JJ marriage/NN </C>
<C>they/PRP do/AUX support/VB equal/JJ rights/NNS and/CC do/AUX not/RB care/VB about/IN the/DT sex/NN lives/NNS of/IN other/JJ adults/NNS ./. </C>
</S>
<S>
<C>She/PRP gives/VBZ examples/NNS of/IN her/PRP$ son/NN 's/POS adoptive/JJ father/NN ,/, friend/NN ,/, and/CC aunt/NN who/WP are/AUX all/PDT loving/VBG parents/NNS and/CC good/JJ friends/NNS ./. </C>
</S>
<S>
<C>She/PRP rejects/VBZ the/DT idea/NN that/IN most/JJS people/NNS do/AUX not/RB trust/VB gay/JJ people/NNS </C>
<C>and/CC that/IN they/PRP are/AUX using/VBG other/JJ people/NNS ./. </C>
</S>
</P>
</T>
