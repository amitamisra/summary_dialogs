<PARAGRAPH> S1 ( Kronos) and S2 ( Jyoshu) are discussing the fact that there are other groups besides the gay community who may want marriage equality rights, such as polygamy advocates or 17 year olds.
Jyoshu sees the changes in law that would be required as an attack on social and legal precedent that can get out of control or be difficult to implement.
Kronos complains that this is a frequent tactic of anti-gay marriage advocates who unfairly claim that gays want to "make marriage an open ended institution".
Jyoshu notes that whatever the intent, the result is the kind of change the anti-gay advocates warn against.
Kronos asserts that specific laws could accommodate specific groups.
He also argues that the motivation of gays who wish to marry is sincere and the same as the motivation behind opposite sex marriages, and therefore the broadening of marriage is a side effect and not the goal.
Jyoshu says she understands but complains that gay marriage would open the door to further broadening of marriage.
