<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG the/DT financial/JJ effect/NN of/IN allowing/VBG for/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP originally/RB believed/VBD allowing/VBG gay/JJ marriage/NN would/MD have/AUX a/DT negative/JJ financial/JJ impact/NN on/IN society/NN as/IN a/DT whole/NN ./. </C>
</S>
<S>
<C>He/PRP has/AUX since/IN amended/VBN that/DT belief/NN due/JJ to/TO information/NN found/VBN in/IN the/DT CBO/NNP ./. </C>
</S>
<S>
<C>His/PRP$ reasoning/NN for/IN still/RB supporting/VBG the/DT ban/NN on/IN gay/JJ marriage/NN has/AUX changed/VBN over/RP to/TO moral/JJ and/CC health/NN related/VBN concerns/NNS ./. </C>
</S>
<S>
<C>Although/IN S2/NNP is/AUX pleased/VBN with/IN the/DT concession/NN of/IN S1/NNP 's/POS former/JJ opinion/NN ,/, </C>
<C>he/PRP believes/VBZ jumping/VBG to/TO the/DT moral/JJ high/JJ ground/NN still/RB ignores/VBZ other/JJ issues/NNS ./. </C>
</S>
<S>
<C>He/PRP would/MD like/VB to/TO discuss/VB the/DT positive/JJ impact/NN that/IN allowing/VBG gay/JJ marriage/NN would/MD have/AUX on/IN the/DT country/NN practically/RB ./. </C>
</S>
<S>
<C>S1/NNP advises/VBZ </C>
<C>while/IN he/PRP is/AUX a/DT defender/NN of/IN the/DT Constitution/NNP ,/, </C>
<C>he/PRP does/AUX feel/VB society/NN should/MD decide/VB ./. </C>
</S>
</P>
</T>
