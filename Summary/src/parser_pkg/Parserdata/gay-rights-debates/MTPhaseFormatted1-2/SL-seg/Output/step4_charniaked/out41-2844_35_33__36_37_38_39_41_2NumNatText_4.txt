(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ supports) (NP (JJ gay) (NN marriage))) (CC and) (VP (VBZ thinks) (SBAR (S (NP (NNP S2)) (VP (AUX is) (VP (AUXG being) (VP (JJ hypocritical) (PP (IN by) (S (S (VP (VBG saying) (SBAR (S (NP (PRP he)) (VP (VBZ agrees) (PP (IN with) (NP (NP (NN fairness)) (CC and) (NP (JJ honest) (NN advocacy))))))))) (CC but) (S (VP (VBG excluding) (NP (JJ gay) (NNS people)) (PP (IN from) (NP (PRP it)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ implies) (PP (IN with) (NP (NN sarcasm))) (SBAR (IN that) (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NN marriage)) (VP (AUX is) (ADVP (RB only)) (PP (IN for) (NP (NP (NNS heterosexuals)) (SBAR (WHNP (WDT that)) (S (VP (VBP wish) (S (VP (TO to) (VP (VB reproduce)))))))))))))))) (CC and) (VP (ADVP (RB only)) (VBZ uses) (NP (ADJP (RB religiously) (VBN approved)) (NNS positions)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ wishes) (SBAR (S (NP (JJ modern) (NN liberalism)) (VP (AUX were) (PP (ADVP (RBR more)) (IN like) (NP (NNP Jeffersonian) (NN liberalism))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (S (S (NP (NN decentralization)) (VP (AUX was) (NP (NP (DT a) (JJ big) (NN theme)) (PP (IN for) (NP (NNP Jefferson)))))) (CC and) (S (NP (PRP he)) (VP (MD would) (VP (AUX have) (VP (VBN opposed) (NP (NN over-regulation))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (VBZ agrees) (PP (IN with) (S (VP (VBG taking) (NP (NP (NP (NN care)) (PP (IN of) (NP (DT the) (JJ elderly)))) (, ,) (NP (NN fairness)) (, ,) (CC and) (NP (NN freedom)))))))))) (. .)))

(S1 (S (ADVP (RB However)) (, ,) (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB support) (S (VP (VP (VBG redefining) (NP (NN marriage))) (, ,) (CC and) (VP (VBZ thinks) (SBAR (S (NP (JJ modern) (NN liberalism)) (VP (AUX has) (VP (AUX been) (VP (VBN taken) (PRT (RP over)) (PP (IN by) (NP (NP (JJ special) (NN interest) (NNS groups)) (, ,) (PP (JJ such) (IN as) (NP (DT the) (JJ gay) (NN lobby))) (, ,))) (PP (IN in) (NP (NP (DT the) (JJ same) (NN way)) (SBAR (S (NP (DT the) (NNPS Republicans)) (VP (AUX were) (VP (VBN taken) (PRT (RP over)) (PP (IN by) (NP (NP (DT the) (JJ religious) (NN right)) (PP (IN in) (NP (DT the) (CD 1980) (POS 's))))))))))))))))))))) (. .)))

