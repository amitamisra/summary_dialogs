(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (NN equality)) (CC and) (NP (JJ natural) (NNS rights))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (NP (NN discrimination)) (PP (IN on) (NP (NP (NNS people)) (PP (IN for) (S (VP (VP (AUXG being) (ADJP (JJ religious))) (CC and) (VP (VBG keeping) (NP (NNS people)) (PP (IN from) (NP (NN religion)))))))))) (VP (VBZ infringes) (PP (IN on) (NP (JJ natural) (NNS rights))))) (, ,) (CC and) (S (SBAR (IN if) (S (NP (PRP you)) (VP (AUX are) (PP (IN for) (NP (NP (NN equality)) (PP (IN for) (NP (NNS gays)))))))) (ADVP (RB then)) (NP (PRP you)) (VP (AUX need) (NP (NP (NN equality)) (PP (IN for) (NP (DT all))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT this)) (VP (VBZ allows) (S (NP (NNS people)) (VP (TO to) (VP (AUX do) (SBAR (WHNP (WDT whatever)) (S (NP (PRP they)) (VP (VP (VBP want)) (CC and) (VP (VBP use) (NP (PRP$ their) (JJ religious) (NNS beliefs)) (PP (IN as) (NP (DT an) (NN excuse)))))))))))))) (. .)))

(S1 (S (S (VP (TO To) (VP (VB contradict)))) (, ,) (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (RB only) (JJ serious) (JJ religious) (NNS views)) (VP (AUX are) (VP (VBN taken) (PP (IN into) (NP (NN account)))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ proposes) (NP (NP (DT a) (NN scenario)) (SBAR (WHADVP (WRB where)) (S (NP (NP (DT the) (JJ only) (NN person)) (ADJP (JJ available) (S (VP (TO to) (VP (VB perform) (NP (DT the) (JJ gay) (NN marriage) (NN ceremony))))))) (VP (AUX is) (VP (VBN exempted) (PP (VBN based) (PP (IN on) (NP (JJ religious) (NNS beliefs)))) (, ,) (SBAR (S (NP (PRP$ their) (NNS rights)) (VP (MD will) (RB not) (VP (AUX be) (VP (VBN upholded)))))))))))) (. .)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ rebuts) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (DT the) (VBN proposed) (NN provision)) (VP (MD would) (VP (VB allow) (NP (NP (NN someone)) (ADJP (RB else)))))))))))) (, ,) (CC and) (S (NP (DT this)) (VP (AUX is) (ADJP (JJ due) (PP (TO to) (NP (JJ religious) (NN freedom)))))) (. .)))

