(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT the) (JJ bible) (VBG warning)) (PP (IN of) (NP (NP (NNS wars)) (CC and) (NP (JJ natural) (NNS disasters))))) (VP (AUX is) (RB not) (NP (NP (JJ viable) (NN proof)) (CC nor) (NP (NP (DT an) (NN argument)) (SBAR (IN since) (S (NP (NP (NNS wars)) (CC and) (NP (JJ natural) (NNS disasters))) (VP (MD will) (ADVP (RB always)) (VP (VB happen))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (PP (IN within) (NP (NP (DT the) (NN majority) (POS 's)) (JJS best) (NN interest))) (S (VP (TO to) (VP (VB look) (PRT (RP out)) (PP (IN for) (NP (DT the) (NN minority))) (, ,) (SBAR (IN as) (S (NP (DT the) (NN minority)) (ADVP (RB still)) (VP (AUX has) (NP (NP (DT a) (JJ great) (NN number)) (PP (IN of) (NP (NNS people)))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP state) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (RB not) (VP (AUX been) (VP (VBN argued) (SBAR (SBAR (IN that) (S (NP (JJ religious) (NNS beliefs)) (VP (VB condone) (NP (NN homosexuality))))) (, ,) (CC but) (SBAR (IN that) (S (NP (JJ religious) (NNS beliefs)) (VP (MD should) (RB not) (VP (AUX have) (NP (NP (NN anything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (DT another) (NN person) (POS 's)) (JJ individual) (NN life))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VB refute) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (NNS earthquakes)) (VP (AUX are) (NP (DT a) (ADJP (RB divinely) (JJ inspired)) (NN event))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB also)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNP genetics)) (VP (AUX has) (ADJP (ADJP (JJR more) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NN homosexuality) (CC and) (NN alcoholism)))))))) (PP (IN than) (NP (NN choice) (CC or) (NN upbringing)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (PP (IN since) (NP (NP (DT the) (NN number)) (PP (IN of) (NP (NNS earthquakes))))) (VP (AUX have) (VP (VBN increased) (ADVP (IN outside) (PP (IN of) (NP (NNP Israel)))) (PP (IN over) (NP (DT the) (JJ past) (QP (CD one) (CD hundred)) (NNS years))) (, ,) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT a) (NN sign)) (PP (IN of) (NP (NP (NNP God) (POS 's)) (NN love) (NN waxing) (NN cold)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NP (DT the) (NN way)) (SBAR (S (NP (DT the) (NNP United) (NNP States) (NN government)) (VP (VBZ works))))) (VP (AUX is) (PP (IN through) (NP (DT the) (NN majority))) (SBAR (IN until) (S (NP (NN something)) (VP (AUX is) (VP (VBN viewed) (PP (IN as) (ADJP (JJ unconstitutional))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (NP (NNP genetics)) (VP (AUX does) (RB not) (VP (AUX have) (NP (NP (NN anything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NN homosexuality))) (SBAR (IN since) (S (NP (PRP they)) (ADVP (RB also)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NN alcoholism)) (VP (AUX is) (ADJP (JJ genetic))))))))))))))))))) (. .)))

