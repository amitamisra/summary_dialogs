<s> <PARAGRAPH> </s>
<s>  S1 claims that homosexual partners should not expect shared healthcare in the same way that an unmarried couple should not. </s>
<s> He suggests that homosexuals are not being denied any rights, that they have all the same rights as heterosexuals. </s>
<s> He points out that nobody can marry someone of the same sex, not just homosexuals. </s>
<s> He rejects the idea that homosexuals will succeed in legalizing same-sex marriage, citing several cases where homosexual marriages were banned. </s>
<s> S2 accuses S1 of accepting that some people should not receive the basic right to marry the person they love. </s>
<s> He likens this to slavery, prohibition, and women's suffrage. </s>
<s> He claims that the proponents of same-sex marriage will eventually be successful, just as human rights advocates were with slavery, voting, and interracial marriage. </s>
<s>  </s>
