<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX debating/VBG the/DT issue/NN of/IN freedom/NN of/IN speech/NN and/CC gay/JJ rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX against/IN gay/JJ rights/NNS and/CC believes/VBZ they/PRP earned/VBD their/PRP$ freedom/NN of/IN speech/NN </C>
<C>through/IN serving/VBG in/IN the/DT military/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ everyone/NN has/AUX the/DT freedom/NN of/IN speech/NN and/CC is/AUX against/IN speaking/VBG out/RP towards/IN other/JJ kinds/NNS of/IN people/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX very/RB rude/JJ and/CC constantly/RB being/AUXG immature/VBD towards/IN S2/NNP ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ that/IN we/PRP are/AUX granted/VBN freedom/NN of/IN speech/NN by/IN the/DT constitution/NN ,/, </C>
<C>and/CC S1/NNP responds/VBZ with/IN immature/JJ remarks/NNS and/CC questions/NNS ./. </C>
</S>
<S>
<C>S2/NNP points/VBZ out/RP that/IN S1/NNP has/AUX no/DT evidence/NN of/IN any/DT sort/NN for/IN his/PRP$ arguments/NNS </C>
<C>and/CC that/IN believing/VBG in/IN the/DT bible/NN should/MD not/RB be/AUX a/DT reason/NN to/TO dictate/VB everyone/NN else/RB 's/POS rights/NNS ./. </C>
</S>
<S>
<C>Also/RB ,/, S2/NNP adds/VBZ that/IN he/PRP does/AUX not/RB need/AUX to/TO prove/VB his/PRP$ beliefs/NNS the/DT same/JJ way/NN S1/NNP should/MD not/RB dictate/VB his/PRP$ beliefs/NNS ./. </C>
</S>
<S>
<C>S1/NNP seems/VBZ to/TO be/AUX ignorant/JJ to/TO the/DT actual/JJ context/NN of/IN the/DT conversation/NN </C>
<C>while/IN S2/NNP is/AUX in/IN belief/NN that/IN S1/NNP should/MD not/RB use/VB his/PRP$ beliefs/NNS </C>
<C>to/TO dictate/VB the/DT rights/NNS of/IN other/JJ individuals/NNS ./. </C>
</S>
</P>
</T>
