<s> <PARAGRAPH> </s>
<s>  S1 argues that is important that a society lives by its laws. </s>
<s> They argue that a heterosexual marriage is a formalization of the need to reproduce. </s>
<s> They do not believe that the Constitution requires a state to recognize another state's laws because it is up to the states what is recognized and what is not. </s>
<s> They argue that heterosexuals and homosexuals are biologically different and that some people just have to deal with the ramifications of being biologically different. </s>
<s> S2 argues against the idea of marriage being solely for reproduction by bringing up the fact that heterosexuals who are well past birthing age can legally get married without any consequence. </s>
<s> They argue that it is unfair that even though they may have a legal grounds and license to prove that they are married in one state, if they go to another state that does not recognize that license then that marriage is not accepted. </s>
<s> This person believes that it should be up to the federal government and not the state government to recognize a marriage license. </s>
<s>  </s>
