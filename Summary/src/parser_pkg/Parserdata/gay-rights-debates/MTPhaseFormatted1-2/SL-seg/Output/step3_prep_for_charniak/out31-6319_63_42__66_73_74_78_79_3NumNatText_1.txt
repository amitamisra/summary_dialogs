<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing a seemingly pending law that would permit marriage officials to decline to conduct gay marriages if they are opposed to them on religious principals. </s>
<s> S1 finds it acceptable, while S2 complains about it. </s>
<s> S1 proposes that it is easy for S2 to be unsympathetic because S2 dislikes religion. </s>
<s> S1 acknowledges that he considers such religious objections to be evil. </s>
<s> He suggests that because the individual licensed to perform a marriage is paid or licensed by the State, the individual should reflect the will and choices of the State and therefore perform gay marriages despite his or her personal religious objection to them. </s>
<s> S2 suspects that the law may result in an effective denial of gay marriage. </s>
<s>  </s>
