(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (SBAR (IN that) (S (NP (NP (NNP Jim) (CC and) (NNP MrWrite) (POS 's)) (NN argument)) (VP (AUX has) (VP (VBN become) (NP (NP (DT a) (NN battle)) (PP (IN of) (NP (NNS insults)))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT this)) (VP (AUX is) (ADJP (JJ unfortunate))))) (, ,) (CC but) (SBAR (IN that) (S (NP (NP (DT the) (NN comparison)) (PP (IN between) (NP (NP (DT the) (NN argument)) (PP (IN against) (NP (NP (JJ gay) (NN marriage)) (CC and) (NP (DT that)))) (PP (IN against) (NP (JJ interracial) (NN marriage)))))) (VP (AUX is) (RB not) (ADJP (JJ baseless))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (SBAR (IN that) (S (NP (NP (NNS opponents)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (VP (VBP use) (NP (NP (JJ many)) (PP (IN of) (NP (NP (DT the) (JJ same) (NNS arguments)) (SBAR (WHNP (WDT that)) (S (VP (AUX were) (VP (VBN used) (S (VP (TO to) (VP (VB oppose) (NP (JJ interracial) (NN marriage))))) (, ,) (SBAR (IN before) (S (NP (DT that)) (VP (AUX was) (VP (VBN outlawed)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (JJS most) (NNS opponents)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (VP (AUX do) (ADVP (RB so)) (PP (IN for) (NP (ADJP (JJ religious) (CC or) (JJ homophobic)) (NNS reasons))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNS words)) (VP (VBP get) (VP (VBD redefined) (PP (IN on) (NP (DT an) (JJ ongoing) (NN basis))) (, ,) (S (VP (VBG citing) (NP (NP (DT the) (NN word) (NN war)) (VP (AUXG being) (VP (VBN modified) (S (VP (TO to) (VP (VB suit) (NP (JJ political) (NNS purposes))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (SBAR (IN that) (S (NP (NP (DT the) (NNS justifications)) (PP (IN for) (NP (NP (DT the) (JJ current) (NN war)) (PP (IN in) (NP (NNP Iraq)))))) (VP (AUX have) (NP (JJ much) (NN merit))))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT the) (JJ same) (NN argument)) (VP (MD could) (VP (AUX be) (VP (VBN made) (PP (IN for) (S (VP (VBG reentering) (NP (NP (NNP Vietnam)) (CC or) (NP (NNP Korea)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (IN that) (S (NP (NP (JJ interracial) (NN marriage)) (CC and) (NP (JJ homosexual) (NN marriage))) (VP (AUX have) (NP (NP (NN anything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (DT each) (JJ other))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (JJ separate-but-equal)) (VP (VP (MD would) (VP (VB apply) (PP (TO to) (NP (NP (DT the) (NNS records)) (PP (IN of) (NP (JJ gay) (NNS couples))))))) (CC but) (VP (MD would) (RB not) (VP (VB justify) (NP (NP (DT any) (NNS restrictions)) (PP (IN of) (NP (RB there) (NNS rights) (CC or) (NNS privileges)))) (PP (IN as) (NP (DT a) (NN couple))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (ADVP (RB preemptively)) (VBZ denies) (S (VP (AUXG being) (ADJP (JJ homophobic)))) (, ,) (S (VP (VBG citing) (NP (NP (NP (PRP$ his) (AUXG having)) (JJ gay) (NNS friends)) (SBAR (WHNP (WP who)) (S (VP (VBP know) (SBAR (WHNP (WP what)) (S (NP (NP (PRP$ his) (NNS views)) (PP (IN on) (NP (DT the) (NN word) (NN marriage)))) (VP (AUX are))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (SBAR (IN that) (S (NP (DT the) (NNP Iraq) (NNP War)) (VP (AUX is) (ADVP (RB technically)) (NP (NP (DT a) (JJ new) (NN war)) (VP (VBG requiring) (NP (NP (DT all)) (PP (IN of) (NP (DT the) (JJ congressional) (NNS steps))))))))) (, ,) (RB rather) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX was) (ADVP (RB merely)) (NP (NP (DT a) (NN continuation)) (PP (IN of) (NP (DT the) (NNP Gulf) (NNP War))))))) (, ,) (CC or) (SBAR (IN that) (S (NP (DT the) (JJ same) (NNS arguments)) (VP (MD could) (VP (AUX be) (VP (VBN applied) (PP (TO to) (NP (DT those) (JJ other) (NNS conflicts))))))))))) (. .)))

