<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN laws/NNS ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN since/IN the/DT traditional/JJ definition/NN of/IN marriage/NN is/AUX human/JJ made/VBN ,/, </C>
<C>it/PRP can/MD be/AUX changed/VBN to/TO meet/VB changing/VBG times/NNS ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN the/DT legal/JJ definition/NN is/AUX human/JJ made/VBN and/CC can/MD change/VB ,/, </C>
<C>but/CC since/IN we/PRP 're/AUX in/IN a/DT democracy/NN </C>
<C>it/PRP must/MD change/VB by/IN vote/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN so/RB far/RB votes/NNS favor/VBP a/DT ``/`` one/CD man/NN one/CD woman/NN ''/'' definition/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN the/DT courts/NNS were/AUX designed/VBN to/TO be/AUX an/DT intermediate/JJ between/IN people/NNS and/CC legislature/NN ,/, </C>
<C>and/CC if/IN same/JJ gender/NN marriage/NN can/MD be/AUX banned/VBN by/IN state/NN action/NN </C>
<C>it/PRP must/MD be/AUX proven/VBN that/IN marriage/NN is/AUX not/RB a/DT fundamental/JJ right/NN ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ that/IN by/IN allowing/VBG this/DT the/DT judiciary/NN branch/NN becomes/VBZ the/DT legislative/JJ branch/NN ,/, </C>
<C>which/WDT is/AUX not/RB so/RB ./. </C>
</S>
</P>
</T>
