<PARAGRAPH> This is about the use of arguments of violence towards gay-marriage.
S1 starts off saying there is a difference between being anti-gay and someone condoning violence against gay people.
S2 talks about how violence is used to make people feel threatened to feel a certain way about a topic or debate.
S2 used the example of kids being taught about gay marriage in school as a threat.
S2 also mentions someone named Archie who posts include a variety of slurs towards gay people.
S1 brushes on the topic, but goes off track from the topic quite a bit.
These two people start referring to each others "attacks" on each other towards the end.
S1 says they were referring to namecalling and violence from the original post that was opposing gay rights.
These two are not really in opposite positions/beliefs, but they are more trying to discuss the ways they argue for or against gay marriage and how it may be portrayed by others.
