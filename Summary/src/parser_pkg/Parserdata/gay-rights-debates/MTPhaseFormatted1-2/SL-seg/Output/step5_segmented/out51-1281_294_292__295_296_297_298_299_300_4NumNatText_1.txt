<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN the/DT issue/NN of/IN same-sex/JJ marriage/NN is/AUX an/DT economic/JJ issue/NN </C>
<C>because/IN it/PRP would/MD have/AUX negative/JJ effects/NNS on/IN businesses/NNS ,/, social/JJ security/NN ,/, and/CC the/DT healthcare/NN systems/NNS ./. </C>
</S>
<S>
<C>He/PRP compares/VBZ homosexual/JJ partners/NNS to/TO roommates/NNS and/CC and/CC demonstrates/VBZ that/IN you/PRP can/MD not/RB make/VB marriage/NN mean/VB whatever/WDT you/PRP want/VBP </C>
<C>by/IN explaining/VBG that/IN he/PRP is/AUX not/RB legally/RB allowed/VBN to/TO marry/VB his/PRP$ dog/NN ./. </C>
</S>
<S>
<C>He/PRP makes/VBZ it/PRP clear/JJ that/IN he/PRP believes/VBZ same-sex/JJ marriage/NN is/AUX wrong/JJ and/CC claims/VBZ that/IN the/DT majority/NN of/IN Americans/NNPS are/AUX also/RB against/IN same-sex/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP suggests/VBZ that/IN homosexual/JJ partners/NNS would/MD not/RB be/AUX a/DT burden/NN on/IN the/DT mentioned/VBN systems/NNS </C>
<C>because/IN they/PRP would/MD be/AUX paying/VBG into/IN the/DT systems/NNS as/RB well/RB as/IN receiving/VBG benefits/NNS from/IN them/PRP ./. </C>
</S>
<S>
<C>He/PRP cites/VBZ the/DT legalization/NN of/IN same-sex/JJ marriages/NNS in/IN various/JJ other/JJ countries/NNS in/IN Europe/NNP and/CC North/NNP America/NNP ./. </C>
</S>
</P>
</T>
