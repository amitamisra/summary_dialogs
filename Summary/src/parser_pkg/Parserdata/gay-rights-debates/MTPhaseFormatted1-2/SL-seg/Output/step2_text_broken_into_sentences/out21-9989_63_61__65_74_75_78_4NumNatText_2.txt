<PARAGRAPH> S1 attacks previous posts for their bigotry and argues that the differences between people are not a bad thing.
He suggests that S2 should make their decisions without reference to the Bible.
He claims that the Bible is not an appropriate source for morality, citing the proscription that misbehaving children should be stoned to death.
He argues that we should instead treat people with kindness and respect.
S2 claims that their views on homosexuals are defined by the Bible and that he will not listen to those who ignore God's Word.
He argues against sacrificing one's religious beliefs in favor of political correctness and suggests that homosexuals repent their sins so that they can go to Heaven.
They claim that homosexuality will lead to Hell.
