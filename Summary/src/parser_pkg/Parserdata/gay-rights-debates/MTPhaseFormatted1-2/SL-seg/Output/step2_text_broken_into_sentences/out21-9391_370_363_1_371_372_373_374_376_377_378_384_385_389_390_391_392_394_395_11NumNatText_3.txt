<PARAGRAPH> The text begins with S1 saying "It's nothing like racial segregation".
But the statement he was responding to is not mentioned in the text.
The statement he was responding to is also not from the other writer ( S2), but from someone mentioned later as Addison.
S1 believes the other writer ( S2) has misunderstood his comment to Addison.
He does not believe his words had anything to do with denying the current plight of gay people is similar to the past plight of black people.
He believes the other writer is jumping to conclusions, possibly based on information in other posts.
S1 is offended by the other writer's use of the phrase, "It's nothing like racial segregation".
He believes the words are about the possibility of segregating gay people.
He the words imply that the segregation of gay people would either be nothing like the past treatment of black people, or would be less wrong.
He believes segregation throughout history is always the same, but people always deny the similarities.
This includes segregation according to race, religion, or sexual orientation.
At one point, he admits he may have interpreted the other writer's words incorrectly.
