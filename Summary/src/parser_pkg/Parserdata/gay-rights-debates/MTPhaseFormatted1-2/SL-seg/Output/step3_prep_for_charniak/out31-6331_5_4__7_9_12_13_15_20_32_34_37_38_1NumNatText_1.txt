<s> <PARAGRAPH> </s>
<s>  S1: Opposers to gay marriage often jump to conclusions about who can get married -why not cousins, group marriages, children, dogs? </s>
<s> Polygamous marriage raises legal concerns that are not currently covered in traditional marriage laws, for example. </s>
<s> The goal of gay marriage supporters is to be able to get married - not to broaden marriage. </s>
<s> Broadened marriage is a side effect of legal gay marriage. </s>
<s> S2: Gay marriage supporters want marriage laws changed to accommodate gay marriage. </s>
<s> Broadened marriage could change the current traditional marriage laws. </s>
<s> Polygamy, for example, would require more marriage laws changed than gay marriage would, as more people are involved. </s>
<s> But if enough people wanted legal polygamous marriage, the laws would be passed. </s>
<s> Marriage would be broadened if gay marriage is made legal, whether that is the goal of gay marriage or not. </s>
<s>  </s>
