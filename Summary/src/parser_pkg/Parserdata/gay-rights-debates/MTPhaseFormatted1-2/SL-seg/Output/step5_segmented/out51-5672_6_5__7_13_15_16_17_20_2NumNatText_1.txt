<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN despite/IN arguments/NNS linking/VBG same-sex/JJ marriage/NN to/TO negative/JJ effects/NNS on/IN society/NN and/CC traditional/JJ families/NNS ,/, he/PRP is/AUX against/IN the/DT constitutional/JJ amendment/NN to/TO ban/VB same-sex/JJ marriages/NNS </C>
<C>on/IN the/DT grounds/NNS that/IN it/PRP is/AUX an/DT inappropriate/JJ topic/NN for/IN a/DT constitutional/JJ amendment/NN ./. </C>
</S>
<S>
<C>He/PRP references/NNS a/DT person/NN named/VBN Colson/NNP who/WP believes/VBZ that/IN the/DT terms/NNS ``/`` marriage/NN ''/'' and/CC ``/`` family/NN ''/'' need/NN to/TO meet/VB specific/JJ criteria/NNS ,/, </C>
<C>and/CC that/IN legalizing/VBG same-sex/JJ marriage/NN would/MD change/VB those/DT criteria/NNS ,/, </C>
<C>leading/VBG to/TO changes/NNS in/IN the/DT meaning/NN of/IN traditional/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB claims/VBZ that/IN Colson/NNP believes/VBZ that/IN traditional/JJ families/NNS have/AUX the/DT best/JJS outcomes/NNS in/IN regards/VBZ to/TO criminal/JJ behavior/NN of/IN offspring/NN ./. </C>
</S>
<S>
<C>S2/NNP rejects/VBZ the/DT claim/NN that/IN same-sex/JJ marriage/NN has/AUX a/DT negative/JJ effect/NN on/IN society/NN ,/, </C>
<C>suggesting/VBG that/IN traditional/JJ marriage/NN was/AUX on/IN the/DT decline/NN </C>
<C>long/RB before/IN same-sex/JJ marriage/NN was/AUX ever/RB legalized/VBN ./. </C>
</S>
<S>
<C>He/PRP also/RB questions/VBZ what/WP negative/JJ effect/NN same-sex/JJ marriage/NN could/MD have/AUX on/IN society/NN ,/, </C>
<C>claiming/VBG that/IN there/EX are/AUX no/DT studies/NNS suggesting/VBG that/IN two/CD heterosexual/JJ parents/NNS fare/VBP any/DT better/RBR than/IN two/CD homosexual/JJ parents/NNS </C>
<C>because/IN there/EX is/AUX no/DT difference/NN in/IN reality/NN ,/, </C>
<C>and/CC that/IN there/EX is/AUX no/DT reason/NN to/TO predict/VB that/IN same-sex/JJ marriages/NNS would/MD affect/VB crime/NN ./. </C>
</S>
</P>
</T>
