<s> <PARAGRAPH> </s>
<s>  Two people are discussing the subject of gay marriage rights. </s>
<s> S1 is against gay marriage but adamantly states his objection is not based on religion but because there are laws prohibiting the practice. </s>
<s> S2 is in support of gay marriage and argues laws could also be passed to ban things such as worshiping God and begs the question of whether or not S1 would so adamantly support the laws he cites if that were the case. </s>
<s> S1 advises he would still support the law as there are already laws banning certain religious practices such as polygamy. </s>
<s> He advises that even though such a thing would most likely not happen, he would still support it because it would be fair and equal toward all. </s>
<s> S1 contends that if this were the case, crime rate would increase due to the banning of religious freedom, in a sense. </s>
<s> S1 believes homosexuality should be considered a crime because sodomy is a crime. </s>
<s> S2 feels S1 is distorting the law to justify homosexuals being discriminated against. </s>
<s> S1 denies this accusation. </s>
<s>  </s>
