(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NNP Christianity)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN eye) (NN witness)) (SBAR (S (NP (NP (NNS accounts)) (PP (IN of) (NP (DT any) (NN resurrection)))) (ADVP (RB ever)) (VP (VBD recorded)))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (NN everything)) (PP (IN in) (NP (DT the) (NNP Gospels)))) (VP (AUX is) (ADJP (JJ hearsay))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (ADVP (RB not) (RB only)) (VP (AUX is) (ADVP (RB there)) (NP (NN evidence))))))) (, ,) (CC but) (S (NP (ADVP (RB scientifically) (CC and) (RB logically)) (DT no) (NN one)) (VP (MD can) (VP (VP (AUX be) (ADJP (JJ dead)) (PP (IN for) (NP (CD three) (NNS days)))) (CC and) (VP (VP (VB get) (PRT (RP up))) (CC and) (VP (VB walk) (ADVP (RB away))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NP (DT the) (NN burden)) (PP (IN of) (NP (NN proof)))) (ADVP (RB instead)) (VP (VBZ lies) (PP (IN with) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP believe) (PP (IN in) (NP (DT the) (NN resurrection))))))))))) (, ,) (CC and) (SBAR (IN that) (S (PP (VBN based) (PP (IN on) (NP (DT the) (NN way) (NNS Christians) (NN life)) (NP (PRP$ their) (NNS lives)))) (NP (PRP they)) (VP (MD would) (ADVP (RB ironically)) (VP (VB end) (PRT (RP up)) (PP (IN in) (NP (DT the) (NN pit))) (ADVP (RB also)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT all) (JJ unfounded) (NN conjecture)) (, ,) (ADJP (JJ full) (PP (IN of) (NP (JJ left) (NN wing)))) (, ,) (ADJP (JJS antichrist) (JJ dysentery))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ challenges) (S (NP (NNP S2)) (VP (TO to) (VP (VB find) (NP (NP (JJ credible) (NNS arguments)) (SBAR (WHNP (IN that)) (S (VP (NN bust) (NP (NP (DT the) (NN resurrection)) (PP (IN of) (NP (NNP Christ)))))))))))) (. .)))

