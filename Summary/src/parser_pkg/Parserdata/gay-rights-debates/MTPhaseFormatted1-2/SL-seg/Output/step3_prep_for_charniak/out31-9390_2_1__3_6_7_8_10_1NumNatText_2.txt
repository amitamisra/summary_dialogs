<s> <PARAGRAPH> </s>
<s>  S1 claims that it is inappropriate to compare opposing homosexuality with racial discrimination. </s>
<s> He explains the difference that racial discrimination is the fear of a particular person and that opposing homosexuality is based on disapproval of a person's behavior. </s>
<s> He proposes that if a homosexual were fired from their job, it would be due to their incompetence and not due to their homosexuality. </s>
<s> S2 objects to S1's claim that homophobia is disapproval of behavior, questioning which behaviors warrant disapproval. </s>
<s> He claims that being homosexual is in itself not a behavior. </s>
<s> He also argues that homosexuals are frequently legally fired for their sexual preference with no regard for competence. </s>
<s> Finally, he asks what a gay activist is and S1 responds by recommending S2 google the term. </s>
<s>  </s>
