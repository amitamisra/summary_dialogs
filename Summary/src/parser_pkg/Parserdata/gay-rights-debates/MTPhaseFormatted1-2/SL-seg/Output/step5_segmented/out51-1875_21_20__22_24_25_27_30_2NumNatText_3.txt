<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX debating/VBG gay/JJ marriage/NN and/CC healthcare/NN benefits/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX against/IN gay/JJ marriage/NN as/RB well/RB as/IN shared/VBN healthcare/NN between/IN gay/JJ couples/NNS ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ this/DT objection/NN points/VBZ to/TO S1/NNP 's/POS being/AUXG in/IN support/NN of/IN things/NNS such/JJ as/IN segregation/NN and/CC prohibition/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN gays/NNS do/AUX have/AUX the/DT right/NN to/TO marry/VB </C>
<C>in/IN order/NN to/TO qualify/VB for/IN healthcare/NN ,/, </C>
<C>they/PRP just/RB have/AUX to/TO marry/VB someone/NN of/IN the/DT opposite/JJ sex/NN ,/, </C>
<C>advising/VBG straight/JJ people/NNS </C>
<C>and/CC gay/JJ people/NNS have/AUX the/DT same/JJ rights/NNS due/JJ to/TO this/DT ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN due/JJ to/TO the/DT fact/NN gay/JJ people/NNS cannot/JJ choose/VBP to/TO marry/VB someone/NN they/PRP love/VBP ,/, of/IN the/DT same/JJ sex/NN ,/, </C>
<C>they/PRP do/AUX not/RB have/AUX the/DT same/JJ rights/NNS as/IN heterosexuals/NNS </C>
<C>and/CC this/DT is/AUX prejudice/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ gay/JJ marriage/NN laws/NNS will/MD be/AUX passed/VBN ./. </C>
</S>
</P>
</T>
