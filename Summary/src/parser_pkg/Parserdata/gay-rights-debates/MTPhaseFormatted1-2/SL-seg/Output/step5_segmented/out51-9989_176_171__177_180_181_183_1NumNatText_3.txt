<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB that/IN the/DT Supreme/NNP Court/NNP should/MD rule/VB based/VBN on/IN the/DT majority/NN 's/POS opinions/NNS of/IN the/DT issue/NN ,/, but/CC on/IN the/DT Court/NNP 's/POS interpretation/NN of/IN the/DT United/NNP States/NNP Constitution/NNP ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB that/IN court/NN rulings/NNS should/MD be/AUX reflective/JJ of/IN the/DT justices/NNS '/POS personal/JJ political/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>They/PRP are/AUX against/IN applying/VBG labels/NNS such/JJ as/IN ``/`` conservative/JJ ''/'' or/CC ``/`` liberal/JJ ''/'' to/TO the/DT court/NN 's/POS decisions/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN most/JJS people/NNS are/AUX using/VBG the/DT Supreme/NNP Court/NNP as/IN a/DT second/JJ legislative/JJ body/NN ,/, </C>
<C>and/CC agrees/VBZ with/IN S1/NNP that/IN a/DT ruling/NN should/MD be/AUX made/VBN in/IN an/DT unbiased/JJ manner/NN and/CC purely/RB on/IN their/PRP$ legality/NN under/IN the/DT Constitution/NNP ./. </C>
</S>
<S>
<C>They/PRP agree/VBP that/IN the/DT Supreme/NNP Court/NNP rulings/NNS should/MD not/RB lean/VB to/TO one/CD political/JJ outlook/NN or/CC the/DT other/JJ ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN those/DT passing/VBG rulings/NNS do/AUX so/RB regardless/RB if/IN their/PRP$ decision/NN offends/VBZ the/DT public/NN ./. </C>
</S>
</P>
</T>
