<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB that/IN the/DT Defense/NNP of/IN Marriage/NN Act/NNP is/AUX unconstitutional/JJ ,/, </C>
<C>and/CC that/IN the/DT Supreme/NNP Court/NNP has/AUX not/RB ruled/VBN on/IN it/PRP stating/VBG that/IN it/PRP is/AUX ./. </C>
</S>
<S>
<C>He/PRP or/CC she/PRP believes/VBZ that/IN public/JJ officials/NNS who/WP make/VBP up/RP their/PRP$ own/JJ laws/NNS that/IN ignore/VBP what/WP has/AUX been/AUX passed/VBN by/IN the/DT mass/JJ majority/NN should/MD be/AUX removed/VBN from/IN office/NN </C>
<C>and/CC that/IN homosexual/JJ individuals/NNS should/MD be/AUX educated/VBN or/CC deported/VBD ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN homosexuals/NNS are/AUX hard/RB working/JJ ,/, dedicated/VBN and/CC talented/VBN people/NNS that/WDT serve/VBP to/TO sustain/VB the/DT country/NN just/RB as/RB much/JJ as/IN anyone/NN else/RB ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB that/IN their/PRP$ rights/NNS are/AUX dictated/VBN by/IN the/DT Bible/NNP or/CC any/DT religious/JJ texts/NNS ,/, </C>
<C>and/CC that/IN those/DT who/WP wish/VBP to/TO enforce/VB their/PRP$ beliefs/NNS on/IN others/NNS invite/VB a/DT dangerous/JJ idea/NN of/IN a/DT police/NN state/NN upon/IN the/DT country/NN ./. </C>
</S>
</P>
</T>
