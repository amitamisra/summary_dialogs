(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (NN diversity)) (CC and) (NP (NNS gays))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (PRP$ his) (NN family)) (VP (VBZ represents) (NP (NP (DT the) (NN norm)) (, ,) (RB not) (NP (NP (DT a) (NN deviation)) (PP (IN from) (NP (PRP it))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ calls) (NP (RB gays) (DT a) (NN perversity))) (, ,) (RB then) (VP (VP (VBZ states) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB hate) (NP (NNS gays))))))) (CC but) (ADVP (RB rather)) (VP (VBZ believes) (SBAR (S (NP (DT that) (NN marriage)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBD redefined) (PP (IN for) (NP (PRP them))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contradicts) (, ,) (S (VP (VBG stating) (SBAR (S (NP (NNS gays)) (VP (AUX are) (UCP (NP (DT a) (NN diversity)) (, ,) (NP (RB not) (DT a) (NN perversion)) (, ,) (CC and) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBZ hates) (NP (NNS gays)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (WHADVP (WRB how)) (S (NP (NNP S1)) (VP (MD can) (VP (VB claim) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB hate) (NP (NNS gays)) (SBAR (WHADVP (WRB when)) (S (NP (PRP he)) (VP (ADVP (RB clearly)) (VBZ calls) (NP (NP (PRP him)) (CC and) (NP (PRP$ his) (NN family))) (NP (DT a) (NN perversion)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN although) (S (NP (PRP he)) (VP (AUX is) (ADJP (ADJP (JJR younger)) (PP (IN than) (NP (NNP S1))))))) (, ,) (NP (PRP he)) (VP (AUX was) (RB not) (VP (VBN born) (NP (NP (NN yesterday)) (, ,) (SBAR (WHPP (TO to) (WHNP (WDT which))) (S (NP (NNP S1)) (VP (VBZ replies) (SBAR (IN that) (S (NP (NP (PRP he)) (PRN (-LRB- -LRB-) (NP (NNP S2)) (-RRB- -RRB-))) (VP (MD can) (VP (VB believe) (SBAR (WHNP (WP what)) (S (NP (PRP he)) (VP (VBZ wants) (, ,) (ADVP (DT no) (NN matter) (SBAR (WHADJP (WRB how) (JJ wrong)) (S (NP (PRP he)) (VP (AUX is))))))))))))))))))))) (. .)))

