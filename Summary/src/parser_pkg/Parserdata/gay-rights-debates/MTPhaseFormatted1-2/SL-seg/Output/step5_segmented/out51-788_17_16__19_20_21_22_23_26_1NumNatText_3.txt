<T>
<P>
</P>
<P>
<S>
<C>S1/NNP argues/VBZ that/IN two/CD men/NNS in/IN a/DT monogamous/JJ homosexual/JJ relationship/NN is/AUX more/RBR normative/JJ than/IN a/DT man/NN in/IN a/DT heterosexual/JJ relationship/NN with/IN multiple/JJ women/NNS ,/, </C>
<C>citing/VBG fidelity/NNP as/IN a/DT normative/JJ attribute/NN of/IN relationships/NNS in/IN America/NNP ./. </C>
</S>
<S>
<C>In/IN response/NN to/TO the/DT poll/NN cited/VBN by/IN S2/NNP ,/, S1/NNP claims/VBZ that/IN opinions/NNS regarding/VBG homosexual/JJ marriages/NNS extend/VBP beyond/IN political/JJ party/NN lines/NNS ./. </C>
</S>
<S>
<C>S2/NNP cites/VBZ a/DT Harris/NNP Interactive/NNP poll/NN from/IN 2003/CD showing/VBG that/IN 62/CD %/NN of/IN Americans/NNPS did/AUX not/RB think/VB homosexual/JJ marriages/NNS should/MD be/AUX recognized/VBN as/IN legal/JJ ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN advocates/NNS of/IN homosexual/JJ marriage/NN are/AUX heterophobes/NNS trying/VBG to/TO force/VB homosexual/JJ marriage/NN onto/IN America/NNP ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN if/IN people/NNS have/AUX the/DT right/NN to/TO support/VB homosexual/JJ marriage/NN ,/, </C>
<C>then/RB other/JJ people/NNS have/AUX just/RB as/RB much/JJ right/NN to/TO oppose/VB gay/JJ marriage/NN due/JJ to/TO the/DT freedoms/NNS described/VBN in/IN the/DT US/NNP Constitution/NNP ./. </C>
</S>
</P>
</T>
