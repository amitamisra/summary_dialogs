<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage and its possible consequences. </s>
<s> S1 contends that our current marriage benefit laws are set up for two adults running a household, so recognising group marriages is a huge task. </s>
<s> He points out that even though its for one man and wife, that is not a necessity and would be a much easier task than allowing group marriages. </s>
<s> He argues that the purpose of what gays are trying to do is to get married, and a side effect is that people will see it as the scope of marriage being broadened. </s>
<s> But, broadening marriage to include polygamy and other types of marriages is a side effect that does not need to be accepted. </s>
<s> S2 contends that cultural and legal precedent is very real, and that intent is irrelevant. </s>
<s> This is because by seeking gay marriage, the boundaries of what defines marriage is widening to include more than opposite-sex couples and because that, gays are broadening marriage, which inevitably leads to broadening in the other areas. </s>
<s>  </s>
