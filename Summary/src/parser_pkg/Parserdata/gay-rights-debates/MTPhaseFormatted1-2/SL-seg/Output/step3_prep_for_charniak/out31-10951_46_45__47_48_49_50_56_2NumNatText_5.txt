<s> <PARAGRAPH> </s>
<s>  S1 compares being against gay marriage to the Taliban and Sharia law. </s>
<s> He sees no difference in those that oppose gay marriage for religious reasons and have laws against it and Muslims that pass laws based on religion. </s>
<s> He finds it hypocritical for people to be against Sharia law when they are, in his opinion, doing the same thing. </s>
<s> He believes marriage is a legal contract between two people and the state and he supports gay marriage. </s>
<s> S2 does not agree with the comparison of Sharia law and being against gay marriage. </s>
<s> He has not advocated for jail time or the death of homosexuals. </s>
<s> They are free to do as they wish. </s>
<s> He believes gay marriage pushes that relationship onto others, and he opposes it. </s>
<s>  </s>
