(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT This)) (VP (AUX is) (PP (IN about) (NP (NP (DT the) (NN use)) (PP (IN of) (NP (NP (NNS arguments)) (PP (IN of) (NP (NP (NN violence)) (PP (IN towards) (NP (NN gay-marriage)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ starts) (PRT (RP off)) (S (VP (VBG saying) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN difference)) (PP (IN between) (S (VP (AUXG being) (NP (NP (NN anti-gay) (CC and) (NN someone)) (VP (VBG condoning) (NP (NP (NN violence)) (PP (IN against) (NP (JJ gay) (NNS people)))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ talks) (PP (IN about) (SBAR (WHADVP (WRB how)) (S (NP (NN violence)) (VP (AUX is) (VP (VBN used) (S (VP (TO to) (VP (VB make) (S (NP (NNS people)) (VP (VB feel) (ADJP (JJ threatened) (S (VP (TO to) (VP (VB feel) (NP (DT a) (JJ certain) (NN way)) (PP (IN about) (NP (DT a) (NN topic) (CC or) (NN debate)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD used) (NP (NP (DT the) (NN example)) (PP (IN of) (NP (NP (NNS kids)) (VP (AUXG being) (VP (VBN taught) (PP (IN about) (NP (JJ gay) (NN marriage)))))))) (PP (IN in) (NP (NN school))) (PP (IN as) (NP (DT a) (NN threat)))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ mentions) (NP (NP (NN someone)) (VP (VBN named) (NP (NNP Archie))) (SBAR (WHNP (WP who)) (S (NP (NNS posts)) (VP (VBP include) (NP (NP (DT a) (NN variety)) (PP (IN of) (NP (NP (NNS slurs)) (PP (IN towards) (NP (JJ gay) (NNS people))))))))))) (. .)))

(S1 (S (VP (VP (VBZ S1) (NP (NNS brushes)) (PP (IN on) (NP (DT the) (NN topic)))) (, ,) (CC but) (VP (VBZ goes) (PRT (RP off)) (NP (NN track)) (PP (IN from) (NP (DT the) (NN topic))) (NP (RB quite) (DT a) (NN bit)))) (. .)))

(S1 (S (NP (DT These) (CD two) (NNS people)) (VP (VBP start) (S (VP (VBG referring) (PP (TO to) (NP (DT each) (NNS others))) (NP (`` ``) (NP (NNS attacks)) ('' '') (PP (IN on) (NP (DT each) (JJ other)))))) (PP (IN towards) (NP (DT the) (NN end)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX were) (VP (VBG referring) (PP (TO to) (NP (NP (NN namecalling)) (CC and) (NP (NP (NN violence)) (PP (IN from) (NP (NP (DT the) (JJ original) (NN post)) (SBAR (WHNP (WDT that)) (S (VP (AUX was) (VP (VBG opposing) (NP (JJ gay) (NNS rights)))))))))))))))) (. .)))

(S1 (S (S (NP (DT These) (CD two)) (VP (AUX are) (RB not) (ADVP (RB really)) (PP (IN in) (NP (JJ opposite) (NNS positions/beliefs))))) (, ,) (CC but) (S (NP (PRP they)) (VP (AUX are) (VP (ADVP (RBR more)) (VBG trying) (S (VP (TO to) (VP (VB discuss) (NP (NP (DT the) (NNS ways)) (SBAR (SBAR (S (NP (PRP they)) (VP (VBP argue) (PP (IN for) (CC or) (IN against) (NP (JJ gay) (NN marriage)))))) (CC and) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (MD may) (VP (AUX be) (VP (VBN portrayed) (PP (IN by) (NP (NNS others)))))))))))))))) (. .)))

