<T>
<P>
</P>
<P>
<S>
<C>S1/NNP Questions/VBZ the/DT role/NN of/IN government/NN in/IN marriage/NN ,/, </C>
<C>maintaining/VBG that/IN it/PRP should/MD have/AUX a/DT very/RB limited/VBN one/NN ./. </C>
</S>
<S>
<C>S1/NNP says/VBZ that/IN governments/NNS want/VBP to/TO be/AUX involved/VBN in/IN our/PRP$ lives/NNS as/RB much/RB as/IN they/PRP can/MD be/AUX ,/, </C>
<C>and/CC that/IN getting/VBG the/DT government/NN out/IN of/IN marriage/NN getting/VBG the/DT government/NN out/IN of/IN marriage/NN would/MD be/AUX a/DT good/JJ idea/NN </C>
<C>even/RB if/IN people/NNS are/AUX supporting/VBG this/DT change/NN for/IN the/DT wrong/JJ reasons/NNS ./. </C>
</S>
<S>
<C>It/PRP is/AUX not/RB necessary/JJ to/TO have/AUX government/NN in/IN marriage/NN </C>
<C>and/CC citizens/NNS should/MD be/AUX vigilant/JJ in/IN making/VBG sure/JJ that/IN the/DT government/NN does/AUX not/RB intrude/VBP where/WRB it/PRP is/AUX not/RB needed/VBN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN single/JJ people/NNS have/AUX the/DT biggest/JJS stake/NN in/IN this/DT issue/NN ./. </C>
</S>
<S>
<C>S2/NNP criticizes/VBZ the/DT new/JJ push/NN for/IN government/NN getting/VBG out/IN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/JJ talks/NNS about/IN marriage/NN being/AUXG an/DT ancient/JJ tradition/NN ,/, based/VBN on/IN the/DT pair/NN bonding/NN that/WDT is/AUX natural/JJ for/IN humans/NNS ,/, </C>
<C>and/CC that/WDT is/AUX one/CD of/IN the/DT foundations/NNS for/IN social/JJ stability/NN ./. </C>
</S>
<S>
<C>It/PRP is/AUX thought/VBN that/IN it/PRP would/MD be/AUX natural/JJ for/IN the/DT government/NN to/TO want/VB to/TO support/VB it/PRP </C>
<C>and/CC that/IN it/PRP should/MD </C>
<C>because/IN the/DT government/NN is/AUX the/DT source/NN of/IN law/NN ./. </C>
</S>
<S>
<C>Replacing/VBG marriage/NN would/MD mean/VB a/DT plethora/NN of/IN minor/JJ and/CC specific/JJ legal/JJ contracts/NNS to/TO cover/VB all/PDT the/DT hundreds/NNS of/IN rights/NNS and/CC mutual/JJ obligations/NNS currently/RB conferred/VBD by/IN marriage/NN ./. </C>
</S>
<S>
<C>Legal/JJ issues/NNS such/JJ as/IN inheritance/NN ,/, pension/NN and/CC social/JJ security/NN rights/NNS ,/, adoption/NN ,/, custody/NN ,/, and/CC home/NN ownership/NN that/WDT would/MD need/AUX separate/JJ contracts/NNS without/IN the/DT umbrella/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>Marriage/NN is/AUX more/RBR convenient/JJ ./. </C>
</S>
</P>
</T>
