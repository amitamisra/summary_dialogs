<T>
<P>
</P>
<P>
<S>
<C>S1/NNP considers/VBZ Colson/NNP 's/POS article/NN on/IN gay/JJ marriage/NN to/TO be/AUX compelling/JJ ./. </C>
</S>
<S>
<C>He/PRP reads/VBZ it/PRP as/IN a/DT comment/NN on/IN society/NN generally/RB ,/, defends/VBZ the/DT author/NN 's/POS views/NNS on/IN traditional/JJ families/NNS and/CC does/AUX not/RB see/VB it/PRP as/IN a/DT direct/JJ attack/NN on/IN same/JJ sex/NN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP refers/VBZ to/TO the/DT statistics/NNS on/IN children/NNS of/IN traditional/JJ families/NNS being/AUXG at/IN a/DT lower/JJR risk/NN of/IN ending/VBG up/RP in/IN prison/NN ./. </C>
</S>
<S>
<C>He/PRP later/RB mentions/VBZ that/IN he/PRP does/AUX not/RB necessarily/RB agree/VB with/IN this/DT conclusion/NN that/IN was/AUX drawn/VBN by/IN Colson/NNP ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ against/IN any/DT protection/NN of/IN the/DT traditional/JJ family/NN pointing/VBG out/RP that/IN it/PRP has/AUX been/AUX having/AUXG its/PRP$ own/JJ issues/NNS ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ Colson/NNP 's/POS article/NN as/IN a/DT direct/JJ attack/NN on/IN any/DT family/NN other/JJ than/IN what/WP has/AUX been/AUX labeled/VBN traditional-/JJ two/CD heterosexual/JJ parents/NNS raising/VBG children/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN the/DT article/NN is/AUX inferring/VBG that/IN homosexual/JJ parents/NNS would/MD end/VB up/RP raising/VBG children/NNS who/WP commit/VBP crimes/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ with/IN the/DT concept/NN that/IN homosexual/JJ marriages/NNS will/MD have/AUX a/DT direct/JJ negative/JJ impact/NN on/IN heterosexual/JJ marriages/NNS mentioning/VBG that/IN any/DT claims/NNS need/AUX to/TO be/AUX backed/VBN up/RP to/TO be/AUX reliable/JJ that/IN there/EX is/AUX nothing/NN to/TO back/VB up/RP any/DT claims/NNS on/IN this/DT issue/NN ./. </C>
</S>
</P>
</T>
