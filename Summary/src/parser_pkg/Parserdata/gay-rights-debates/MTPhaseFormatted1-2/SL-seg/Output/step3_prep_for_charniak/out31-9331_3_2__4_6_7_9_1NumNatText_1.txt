<s> <PARAGRAPH> </s>
<s>  S1 argues that the proponents of Prop 8 need to verify that the California Amendment banning gay marriage was enacted legally. </s>
<s> This person believes that if DOMA is not declared unconstitutional than those arguing against Prop 8 do not have a valid platform. </s>
<s> He or she argue that they would have to first strike down the federal law that gives states the right to ignore other states' laws before they could do the same to the California Amendment. </s>
<s> S2 argues that the California Amendment is being tried to see if it violates the 14th Amendment of the Constitution. </s>
<s> This person believes that DOMA violates the federal constitution. </s>
<s> This person argues that there is nothing in DOMA that prevents California's court from ruling the 14th amendment requires gay marriage. </s>
<s>  </s>
