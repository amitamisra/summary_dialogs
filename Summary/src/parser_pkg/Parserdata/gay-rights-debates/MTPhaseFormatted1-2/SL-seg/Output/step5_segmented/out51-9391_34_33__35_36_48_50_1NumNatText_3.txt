<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB that/IN any/DT evidence/NN coming/VBG from/IN Cameron/NNP should/MD not/RB be/AUX viewed/VBN without/IN suspicion/NN on/IN grounds/NNS that/IN he/PRP has/AUX been/AUX kicked/VBN out/RP of/IN the/DT legitimate/JJ scientific/JJ community/NN and/CC has/AUX gotten/VBN in/IN trouble/NN with/IN judges/NNS and/CC scientists/NNS that/WDT publicly/RB condemn/VB him/PRP for/IN misusing/VBG their/PRP$ research/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN even/RB if/IN it/PRP were/AUX true/JJ ,/, </C>
<C>it/PRP does/AUX not/RB legitimize/VB anti-gay/JJ legislation/NN or/CC discrimination/NN </C>
<C>and/CC that/IN being/AUXG homosexual/JJ does/AUX not/RB hurt/VB anyone/NN ,/, </C>
<C>nor/CC should/MD it/PRP be/AUX counted/VBN as/IN a/DT disease/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN HIV/NNP can/MD affect/VB anyone/NN ,/, not/RB just/RB the/DT homosexual/JJ community/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN giving/VBG homosexuals/NNS rights/NNS and/CC privileges/NNS affects/VBZ everyone/NN else/RB financially/RB ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN something/NN should/MD be/AUX done/AUX if/IN a/DT scientific/JJ study/NN links/VBZ a/DT dangerous/JJ disease/NN to/TO the/DT activities/NNS of/IN a/DT certain/JJ group/NN of/IN people/NNS ./. </C>
</S>
</P>
</T>
