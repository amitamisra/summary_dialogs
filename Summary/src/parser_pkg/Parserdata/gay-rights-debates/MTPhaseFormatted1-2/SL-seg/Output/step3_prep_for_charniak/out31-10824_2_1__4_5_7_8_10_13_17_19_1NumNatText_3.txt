<s> <PARAGRAPH> </s>
<s>  S1 believes that the gay-rights movement, S2 in particular, are paranoid. </s>
<s> There is not much being said to support his argument, most of the conversation is dominated by S2. </s>
<s> He argues that economic issues are more likely to hurt those in the gay rights movement than any possible laws. </s>
<s> S2 has concerns about the possibility of creating a true second class citizens of the GLBTQ community. </s>
<s> He refers to President Obama of being the only politician to put his beliefs aside and support gay marriage. </s>
<s> He references past injustices to support his claims that it is possible that gays will be lawfully discriminated against. </s>
<s> He mentions Native Americans and the genocide they experienced; Japanese Americans and the internment camps; African Americans and past slavery. </s>
<s> He worries that history will be allowed to repeat itself in the form of discrimination of homosexuals and points out recent attempts at renewing sodomy laws by some politicians. </s>
<s> At the end he seems to be in favor of an amendment to the Constitution to protect gay rights. </s>
<s>  </s>
