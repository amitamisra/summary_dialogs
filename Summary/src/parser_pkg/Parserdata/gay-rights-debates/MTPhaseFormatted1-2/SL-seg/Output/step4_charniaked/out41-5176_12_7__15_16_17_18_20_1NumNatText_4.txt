(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage) (NNS laws)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (DT the) (JJ current) (NN system)) (NP (DT the) (JJS most) (NNS votes)) (VP (VB win) (, ,) (SBAR (WHPP (TO to) (WHNP (WDT which))) (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (IN that) (S (NP (DT the) (NNS laws)) (VP (MD should) (VP (AUX be) (VP (VBN decided) (PP (VBN based) (PP (IN on) (S (VP (VBG looking) (PP (IN at) (NP (NP (DT the) (NNS individuals)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (VP (VBN affected))))))))))))))))))))))) (. .)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NN one)) (VP (VBZ wants) (S (VP (TO to) (VP (VB change) (NP (DT the) (NN law)))))))) (, ,) (NP (PRP he)) (VP (MD must) (VP (VB get) (NP (NP (DT a) (NN majority)) (PP (IN of) (NP (NP (NNS people)) (SBAR (S (VP (TO to) (VP (AUX do) (NP (PRP it))))))))))))))) (, ,) (CC and) (S (NP (DT the) (NN majority)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN ignored) (PP (IN because) (NP (NP (NN someone) (NNS objects)) (PP (TO to) (NP (PRP them))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (DT the) (NNS courts)) (VP (MD should) (VP (VP (VB look) (PP (IN at) (NP (NP (DT the) (JJ individual) (NNS lives)) (SBAR (WHNP (WDT that)) (S (VP (MD will) (VP (AUX be) (VP (VBN affected))))))))) (CC and) (VP (VB decide)))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (NNS laws)) (SBAR (WHNP (WDT that)) (S (NP (DT a) (NN majority)) (VP (VBZ favors) (SBAR (IN that) (S (VP (AUX are) (ADJP (JJ unjust) (PP (IN in) (NP (DT an) (NNS individuals) (NN life))))))))))) (VP (MD must) (VP (AUX be) (VP (VBN overruled)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ rebuts) (SBAR (IN that) (S (NP (DT that)) (VP (VBZ goes) (PP (IN against) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (NNS courts)) (VP (AUX were) (VP (VBN designed) (S (VP (TO to) (VP (VB work))))))))))))) (. .)))

