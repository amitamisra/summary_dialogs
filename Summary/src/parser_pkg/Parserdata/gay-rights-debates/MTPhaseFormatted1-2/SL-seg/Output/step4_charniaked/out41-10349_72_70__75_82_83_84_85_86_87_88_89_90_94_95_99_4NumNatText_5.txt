(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (NN marriage)) (, ,) (PP (IN in) (NP (NP (PRP it) (POS 's)) (NN conception))) (, ,) (VP (AUX was) (VP (VBN made) (PP (IN for) (NP (JJ biological) (NNS reasons))))))) (, ,) (SBAR (IN that) (S (NP (NN marriage)) (VP (VBZ exists) (S (VP (TO to) (VP (VB allow) (S (NP (NNS children)) (VP (TO to) (VP (AUX be) (VP (VBN born) (ADVP (RBR more) (RB easily)))))))))))))) (. .)))

(S1 (S (SBAR (IN Although) (S (NP (DT this)) (VP (MD should) (, ,) (PP (IN in) (NP (DT no) (NN way)))))) (VP (VBP force) (S (S (NP (NNS people)) (VP (TO to) (VP (AUX have) (NP (NNS children))))) (, ,) (CC or) (S (NP (PRP$ their)) (VP (AUXG being) (ADJP (JJ capable) (PP (IN of) (S (VP (AUXG having) (S (NP (NP (NNS children)) (PP (IN in) (NP (DT the) (JJ first) (NN place)))) (VP (VB make) (NP (DT a) (NN difference)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (SBAR (IN that) (S (VP (VBZ heterosexuals) (CC and) (VBP homosexualsare) (ADJP (RB fundamentally) (JJ different))))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT that) (NN difference)) (VP (AUX is) (SBAR (WHADVP (WRB why)) (S (NP (PRP they)) (VP (AUX are) (VP (VBN treated) (ADVP (RB differently))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ compares) (NP (NP (DT the) (NN institution)) (PP (IN of) (NP (NN marriage)))) (PP (TO to) (S (NP (DT the) (NN world)) (VP (VP (AUXG being) (VP (VBN built) (S (VP (TO to) (VP (VB accommodate) (NP (ADJP (RB right) (VBN handed)) (NNS people))))))) (, ,) (CC and) (VP (AUX is) (VP (VBN insulted) (SBAR (IN that) (S (NP (DT the) (NN world)) (VP (MD should) (VP (AUX be) (VP (VBN forced) (S (VP (TO to) (VP (AUX be) (VP (VBN changed) (PP (IN for) (NP (NNS lefties))) (, ,) (S (RB not) (VP (VBG believing) (SBAR (S (NP (PRP they)) (VP (AUX have) (NP (DT that) (NN right)))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (IN that) (, ,) (S (NP (NP (NP (DT the) (JJ federal) (NN government)) (CC and) (NP (JJ other) (NNS states))) (, ,) (VP (RB not) (VBG recognizing) (NP (PRP$ his) (NN marriage) (NN license))) (, ,)) (VP (AUX is) (ADJP (JJ unconstitutional)) (, ,) (SBAR (IN as) (S (NP (NNP Iowa)) (VP (VBZ issues) (NP (NP (DT the) (JJ same) (NN license)) (PP (IN for) (NP (DT both) (JJ hetero-) (CC and) (JJ homosexual) (NNS couples))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN difference)) (PP (IN between) (S (S (NP (DT a) (JJ gay) (NN couple)) (VP (VBG getting) (VP (VBN married)))) (, ,) (CC and) (S (NP (DT a) (ADJP (NP (CD 70) (NN year)) (JJ old)) (JJ straight) (NN couple)) (VP (VBG getting) (VP (VBN married))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ wants) (NP (JJ equal) (NN right)) (S (VP (TO to) (VP (VB get) (VP (VBN married)))))) (, ,) (CC and) (VP (AUX have) (S (NP (DT that) (NN marriage)) (VP (VBN recognized) (, ,) (PP (IN as) (NP (NP (NNS couples)) (SBAR (WHNP (WP who)) (S (VP (VP (ADVP (RB either)) (MD can) (RB not) (VP (AUX have) (NP (NNS children)))) (CC or) (VP (VB choose) (S (RB not) (VP (TO to))))))))))))) (. .)))

