(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT The) (NN issue)) (ADVP (RB here)) (VP (AUX is) (SBAR (SBAR (IN whether) (S (NP (NN government) (CC or) (NN religion)) (MD should) (VP (VBZ decides) (NP (NP (DT the) (NNS principles)) (PP (IN of) (NP (NN marriage))))))) (, ,) (CC and) (SBAR (WHNP (WP who)) (S (VP (AUX is) (VP (VBN allowed) (S (VP (TO to) (VP (VB get) (VP (VBN married))))))))))) (. .)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (VBZ believes) (SBAR (IN that) (S (S (VP (VBG leaving) (NP (PRP it)) (PP (RP up) (PP (TO to) (NP (NNS religions) (NNS groups)))))) (VP (AUX does) (RB not) (VP (VB satisfy) (SBAR (WHNP (WP what)) (S (NP (NNS gays)) (VP (AUX are) (VP (VBG looking) (PP (IN for))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX are) (VP (VBG searching) (PP (IN for) (NP (NP (DT the) (JJ civil) (NNS benefits)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBP come) (PP (IN with) (NP (DT a) (NN marriage)))) (CC and) (VP (MD would) (VP (VB like) (S (VP (TO to) (VP (AUX be) (VP (VBN treated) (ADVP (RB equally)) (PP (IN in) (NP (DT that) (NN respect)))))))))))))))) (. .)))

(S1 (S (NP (DT The) (NN speaker)) (VP (VBZ believes) (SBAR (S (NP (JJ gay)) (VP (MD should) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VP (VB marry) (NP (NP (DT a) (NN person)) (PP (IN of) (NP (PRP$ their) (NN choice))))) (CC and) (VP (VB get) (NP (JJ equal) (NNS rights)))))))))))) (. .)))

(S1 (NP (NP (NNP Speaker) (CD two) (NNS opinions)) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (MD should) (ADVP (RB indeed)) (VP (AUX be) (NP (NP (DT a) (JJR better) (NN system)) (PP (IN for) (NP (NN marriage) (NNS benefits)))))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT all) (`` ``) (JJ single) ('' '') (NNS people)) (SBAR (WHNP (WDT that)) (S (VP (VBP get) (VP (VBN screwed) (PRT (RP over)) (PP (IN by) (NP (NP (NN marriage) (POS 's)) (JJ current) (NN stature)))))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD should) (VP (VB marry) (NP (DT a) (NN woman)) (SBAR (IN if) (S (NP (PRP they)) (VP (VBP want) (NP (DT the) (JJ same) (NNS rights)))))))))) (. .)))

