<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP discuss/VB the/DT United/NNP States/NNPS Constitutional/JJ legal/JJ system/NN and/CC whether/IN and/CC how/WRB same/JJ sex/NN marriage/NN can/MD be/AUX banned/VBN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ the/DT concept/NN of/IN marriage/NN has/AUX evolved/VBN dramatically/RB over/IN time/NN and/CC can/MD now/RB be/AUX declared/VBN by/IN the/DT Supreme/NNP Court/NNP to/TO be/AUX a/DT fundamental/JJ right/NN not/RB to/TO be/AUX denied/VBN to/TO same/JJ sex/NN couples/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ it/PRP is/AUX a/DT legislative/JJ matter/NN which/WDT should/MD be/AUX left/VBN to/TO voting/NN ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ that/IN fundamental/JJ rights/NNS are/AUX protected/VBN by/IN the/DT Constitution/NNP and/CC are/AUX not/RB subject/JJ to/TO a/DT plebescite/NN ./. </C>
</S>
<S>
<C>S2/NNP accuses/VBZ S1/NNP of/IN believing/VBG in/IN an/DT activist/JJ judiciary/NN and/CC evolving/VBG Constitutional/JJ standards/NNS which/WDT usurp/VB the/DT role/NN of/IN the/DT legislature/NN ./. </C>
</S>
<S>
<C>S2/NNP also/RB criticizes/VBZ Loving/NNP v/NNP ./. </C>
</S>
<S>
<C>Virginia/NNP as/RB incorrect/JJ in/IN reasoning/NN and/CC an/DT example/NN of/IN judicial/JJ activism/NN ./. </C>
</S>
</P>
</T>
