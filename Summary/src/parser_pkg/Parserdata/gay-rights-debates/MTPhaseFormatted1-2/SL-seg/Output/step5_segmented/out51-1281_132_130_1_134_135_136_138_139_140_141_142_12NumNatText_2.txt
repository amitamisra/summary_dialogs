<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG the/DT topic/NN of/IN gay/JJ marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ he/PRP is/AUX not/RB exactly/RB opposed/VBN to/TO gay/JJ marriage/NN </C>
<C>but/CC would/MD like/VB someone/NN to/TO give/VB valid/JJ reasoning/NN </C>
<C>behind/IN restructuring/VBG the/DT basis/NN of/IN marriage/NN since/IN it/PRP 's/AUX been/AUX the/DT same/JJ since/IN the/DT beginning/NN of/IN time/NN ,/, </C>
<C>comparing/VBG it/PRP to/TO the/DT Parthenon/NNP ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ it/PRP is/AUX circular/JJ to/TO classify/VB marriage/NN as/IN simply/RB heterosexual/JJ ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ no/DT one/NN is/AUX attempting/VBG to/TO take/VB away/RP the/DT idea/NN of/IN marriage/NN ,/, </C>
<C>just/RB asking/VBG for/IN homosexual/JJ couples/NNS to/TO be/AUX included/VBN in/IN the/DT fray/NN ./. </C>
</S>
<S>
<C>S1/NNP disagrees/VBZ with/IN S2/NNP 's/POS statements/NNS that/IN at/IN the/DT root/NN ,/, most/JJS homosexuals/NNS fighting/VBG for/IN marriage/NN rights/NNS are/AUX doing/VBG so/RB on/IN a/DT conservative/JJ note/NN ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ instead/RB that/IN the/DT reasons/NNS are/AUX a/DT ``/`` we/PRP are/AUX entitled/VBN to/TO what/WP you/PRP have/AUX ''/'' mentality/NN ./. </C>
</S>
<S>
<C>S2/NNP disagrees/VBZ stating/VBG that/IN while/IN many/JJ do/AUX want/VB acceptance/NN from/IN society/NN ,/, </C>
<C>most/JJS want/VBP marriage/NN for/IN the/DT basic/JJ reasons/NNS of/IN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB feels/VBZ S1/NNP 's/POS arguments/NNS support/VBP the/DT notion/NN that/IN laws/NNS should/MD stay/VB absolute/JJ and/CC cannot/JJ be/AUX changed/VBN or/CC altered/VBN ./. </C>
</S>
<S>
<C>S1/NNP disagrees/VBZ with/IN this/DT statement/NN advising/VBG he/PRP simply/RB wants/VBZ the/DT reasons/NNS supported/VBN ./. </C>
</S>
</P>
</T>
