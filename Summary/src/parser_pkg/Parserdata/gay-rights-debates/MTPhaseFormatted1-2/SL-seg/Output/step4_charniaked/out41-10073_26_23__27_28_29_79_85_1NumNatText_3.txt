(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (NN marriage) (NN equality)) (, ,) (CC but) (NP (DT an) (NN attempt) (S (VP (TO to) (VP (VB redefine) (NP (NP (DT the) (NN meaning)) (PP (IN of) (NP (NN marriage))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ wishes) (SBAR (IN that) (S (NP (DT the) (JJ gay) (NNS posters)) (VP (MD would) (VP (VP (AUX be) (ADJP (JJ honest))) (CC and) (RB not) (VP (VB use) (NP (NP (NNS words)) (PP (JJ such) (IN as) (NP (NP (NN equality)) (CC and) (NP (NP (NN discrimination)) (SBAR (WHADVP (WRB where)) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB apply) (, ,) (SBAR (IN since) (S (NP (NN marriage)) (VP (AUX is) (NP (NP (DT an) (NN institution)) (VP (VBN created) (S (VP (TO to) (VP (AUX be) (NP (NP (DT a) (NN union)) (PP (IN between) (NP (NN man) (CC and) (NN woman))) (SBAR (WHNP (WDT that)) (S (VP (VBZ leads) (PP (TO to) (NP (NP (DT the) (NN creation)) (PP (IN of) (NP (NNS children))))))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (CD two) (NNS people)) (VP (VBG giving) (NP (DT each) (JJ other)) (NP (DT a) (NN wedding) (NN ring)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG questioning) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NN discrimination)) (SBAR (IN if) (S (S (NP (PRP he)) (VP (MD can) (RB not) (VP (VB marry) (NP (NP (DT the) (NN person)) (PP (IN of) (NP (PRP$ his) (NN choice))))))) (CC but) (S (NP (DT a) (NN heterosexual)) (VP (MD can)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (NNP Las) (NNP Vegas)) (NP (PRP itself))) (VP (VBZ refutes) (NP (NP (NNP S1) (POS 's)) (NNS arguments)))))) (. .)))

