<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN his/PRP$ opposition/NN to/TO gay/JJ marriage/NN is/AUX based/VBN on/IN the/DT belief/NN that/IN it/PRP will/MD hurt/VB more/JJR people/NNS than/IN it/PRP helps/VBZ ,/, not/RB on/IN bigotry/NN ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN it/PRP would/MD hurt/VB people/NNS with/IN a/DT traditional/JJ view/NN of/IN marriage/NN as/IN between/IN a/DT man/NN and/CC a/DT woman/NN ,/, </C>
<C>and/CC that/IN the/DT benefits/NNS of/IN marriage/NN can/MD be/AUX gained/VBN by/IN homosexuals/NNS </C>
<C>without/IN redefining/VBG marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN gay/JJ marriage/NN does/AUX not/RB hurt/VB anyone/NN ,/, straight/JJ or/CC gay/JJ ,/, </C>
<C>and/CC offending/VBG proponents/NNS of/IN traditional/JJ marriage/NN is/AUX not/RB a/DT valid/JJ argument/NN against/IN it/PRP ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN preventing/VBG gay/JJ marriage/NN is/AUX simply/RB faith/NN based/VBN hate/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN having/AUXG separate/JJ rules/NNS for/IN straight/JJ and/CC gay/JJ marriage/NN creates/VBZ the/DT possibility/NN of/IN those/DT rules/NNS being/AUXG different/JJ for/IN the/DT two/CD groups/NNS ,/, </C>
<C>allowing/VBG the/DT gay/JJ minority/NN to/TO be/AUX disenfranchised/VBN ./. </C>
</S>
</P>
</T>
