(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP we)) (VP (VBP benefit) (PP (IN from) (NP (NP (NNS families)) (PP (IN with) (NP (NP (VBN adopted) (NX (NX (NNS children)) (, ,) (NX (NN step) (NNS children)) (, ,) (CC or) (NX (NNS families)))) (PP (IN with) (NP (NP (DT no) (NNS children)) (PP (IN at) (NP (DT all))))))))))))) (. .)))

(S1 (S (NP (NN Marriage)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (DT the) (NN bible) (CC and) (NN everything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (DT the) (JJ slippery) (NN slope))) (NP (JJ logical) (NN fallacy)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (NP (DT a) (JJ married) (NN couple)) (PP (IN as) (NP (NP (DT the) (NN head)) (PP (IN of) (NP (DT a) (NN home)))))) (VP (AUX is) (VP (VBN considered) (NP (DT a) (JJ nuclear) (NN family)) (SBAR (IN because) (S (VP (AUX is) (NP (NP (RB already) (IN that) (DT the) (NN question)) (PP (IN of) (NP (NN legality)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (NP (NNP S1)) (SBAR (IN if) (S (NP (PRP he)) (VP (AUX is) (VP (VBG trying) (S (VP (TO to) (VP (VB say) (SBAR (IN because) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (ADJP (ADJP (RBR less) (JJ acceptable) (PP (IN than) (NP (JJ incestuous) (NNS relations)))) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ okay) (S (VP (TO to) (VP (VB grant) (NP (NP (JJ gay) (NN marriage)) (CONJP (CC but) (RB not)) (NP (JJ sibling) (NN marriage)))))))))))))))))))))) (. .)))

(S1 (S (PP (VBG According) (PP (TO to) (NP (NNP S1)))) (, ,) (NP (NP (NN incest)) (CC and) (NP (NN polygamy))) (VP (AUX does) (RB not) (VP (VB relate) (ADVP (RB specifically)) (PP (TO to) (NP (JJ gay) (JJ legal) (NN marriage))))) (. .)))

(S1 (S (NP (NP (JJ S2) (NNS questions)) (PP (TO to) (SBAR (WHNP (WP what) (RB exactly)) (S (VP (AUX does) (NP (NP (DT the) (JJ social) (NN acceptance)) (PP (IN of) (NP (NP (DT any) (NN type)) (PP (IN of) (NP (`` ``) (JJ taboo) ('' '') (NN relationship))))))))))) (VP (AUX have) (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (PRP$ their) (JJ right)))))) (S (VP (TO to) (VP (AUX be) (ADJP (RB legally) (JJ married)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (MD will) (VP (VB change) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (PP (PP (IN from) (`` ``) (NP (NP (DT a) (JJ legal) (NN union)) (PP (IN between) (NP (NN man) (CC and) (NN woman)))) ('' '')) (PP (TO to) (`` ``) (NP (NP (DT a) (JJ legal) (NN union)) (PP (IN between) (NP (NP (DT any) (NNS parties)) (SBAR (WHNP (WDT that)) (S (VP (VBP love) (NP (DT each) (JJ other)))))))) ('' '')))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ poses) (NP (DT the) (NN question) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (ADJP (ADJP (JJ okay)) (SBAR (IN for) (S (NP (CD two) (NNS people)) (VP (TO to) (VP (VB tie) (NP (DT the) (NN knot)) (ADVP (RB then)) (SBAR (WHADVP (WRB how)) (S (VP (MD could) (ADVP (NN incest)) (VP (AUX be) (ADJP (JJ taboo)))))))))))))))) (. .)))

