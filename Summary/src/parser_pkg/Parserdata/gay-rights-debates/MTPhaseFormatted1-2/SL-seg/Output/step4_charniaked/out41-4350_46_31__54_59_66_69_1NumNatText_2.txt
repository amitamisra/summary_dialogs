(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (VB discuss) (NP (JJ same-sex) (NN marriage))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX is) (PP (IN for) (NP (JJ gay) (NN marriage)))) (, ,) (CC and) (VP (VBZ opens) (NP (DT the) (NN discussion)) (PP (IN by) (S (VP (VBG showing) (NP (NP (JJ logical) (NNS fallacies)) (VP (VBN used) (PP (IN by) (NP (DT the) (NN opposition))) (S (VP (TO to) (VP (VB deter) (NP (NNS people)) (PP (IN from) (S (VP (VBG believing) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (NP (DT an) (NN extension)) (PP (IN of) (NP (JJ equal) (NNS rights)))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ modern) (NNS families)) (VP (VP (AUX are) (RB not) (NP (JJ traditional) (NNS families))) (CC and) (VP (AUX are) (ADVP (RB often)) (ADJP (JJR better) (RP off) (PP (IN for) (NP (PRP it))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB specifically)) (VP (VBZ points) (PRT (RP out)) (NP (NP (DT the) (NN comparison)) (PP (IN of) (NP (NP (JJ gay) (NN marriage)) (PP (TO to) (NP (NP (NN incest)) (CC or) (NP (NN polygamy)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (NP (DT a) (NN scare) (NN tactic)) (SBAR (S (VP (TO to) (VP (VB turn) (NP (NNS people)) (PRT (RP away))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (NN homosexuality)) (, ,) (NP (NN incest)) (, ,) (CC and) (NP (NN polygamy))) (VP (AUX are) (RB all) (ADJP (VBN related)) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (NP (NP (DT all) (NNS taboos)) (PP (IN of) (NP (NN society))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ wants) (S (VP (TO to) (VP (VB draw) (NP (NP (DT the) (NN line)) (SBAR (IN as) (S (VP (AUX is) (SBAR (IN so) (S (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (AUX does) (RB not) (VP (VB change))))))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP it)) (VP (AUX does) (VP (VB change))))) (ADVP (RB then)) (NP (EX there)) (VP (MD would) (VP (AUX be) (NP (NP (DT no) (NN limit)) (PP (TO to) (SBAR (WHNP (WP who)) (S (VP (MD could) (VP (VB marry))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ explains) (SBAR (IN that) (S (NP (NN love)) (VP (AUX is) (ADJP (JJ present) (PP (IN in) (NP (DT all) (NN marriage)))) (SBAR (IN so) (S (NP (NP (DT the) (NNS claims)) (VP (VBN made) (PP (IN by) (NP (NNP S2))) (PP (IN about) (NP (NP (JJ gay) (NN marriage) (VBG leading)) (PP (TO to) (NP (NN incest))))))) (, ,) (VP (MD could) (VP (AUX be) (VP (VBN used) (PP (IN for) (NP (JJ straight) (NN marriage))) (ADVP (RB as) (RB well))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ attacks) (NP (NNP S1))) (CC and) (VP (VBZ thinks) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NP (DT the) (JJ other) (NNS parties)) (VP (VBN mentioned))) (VP (VP (VBD went)) (CC and) (VP (VBD paraded) (NP (PRP$ their) (NN lifestyle)))))) (ADVP (RB then)) (NP (PRP they)) (VP (MD would) (ADVP (RB also)) (VP (AUX have) (NP (DT an) (JJ equal) (NN opportunity) (S (VP (TO to) (VP (VB make) (S (NP (PRP$ their) (NN marriage)) (NP (DT a) (JJ social) (NN issue))))))))))))) (. .)))

