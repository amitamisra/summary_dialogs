<T>
<P>
</P>
<P>
<S>
<C>S1/NNP begins/VBZ the/DT dialog/NN </C>
<C>by/IN attacking/VBG conservative/JJ republicans/NNS for/IN the/DT lack/NN of/IN gay/JJ rights/NNS in/IN America/NNP ,/, </C>
<C>and/CC claims/NNS democrats/NNS have/AUX been/AUX trying/VBG to/TO progress/VB gay/JJ rights/NNS </C>
<C>but/CC are/AUX afraid/JJ to/TO loose/VB their/PRP$ political/JJ position/NN ./. </C>
</S>
<S>
<C>S2/NNP responds/VBZ by/IN commenting/VBG that/IN Democratic/NNP Vice/NNP President/NNP Joe/NNP Biden/NNP stated/VBD he/PRP was/AUX against/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ he/PRP was/AUX lying/VBG </C>
<C>to/TO attract/VB votes/NNS ./. </C>
</S>
<S>
<C>S2/NNP expresses/VBZ desire/NN for/IN a/DT candidate/NN who/WP stands/VBZ behind/IN their/PRP$ beliefs/NNS regardless/RB of/IN popular/JJ opinion/NN ,/, </C>
<C>and/CC not/RB lie/VBP </C>
<C>to/TO get/VB votes/NNS ./. </C>
</S>
<S>
<C>S1/NNP calls/VBZ S2/NNP naive/JJ for/IN believing/VBG that/IN lying/VBG does/AUX not/RB happen/VB in/IN politics/NNS today/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX the/DT responsibility/NN of/IN the/DT public/NN to/TO decipher/VB political/JJ language/NN </C>
<C>to/TO know/VB when/WRB politicians/NNS are/AUX lying/VBG or/CC not/RB ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN the/DT public/NN should/MD demand/VB honesty/NN but/CC not/RB assume/VB it/PRP ./. </C>
</S>
<S>
<C>There/EX is/AUX no/DT point/NN is/AUX questioning/VBG candidates/NNS ,/, </C>
<C>if/IN it/PRP is/AUX known/VBN they/PRP are/AUX going/VBG to/TO lie/VB ./. </C>
</S>
<S>
<C>S2/NNP addresses/VBZ that/IN S1/NNP also/RB believes/VBZ that/IN honesty/NN should/MD be/AUX demanded/VBN </C>
<C>because/IN their/PRP$ earlier/JJR statements/NNS insinuated/VBD that/IN they/PRP thought/VBD this/DT practice/NN was/AUX wrong/JJ </C>
</S>
</P>
</T>
