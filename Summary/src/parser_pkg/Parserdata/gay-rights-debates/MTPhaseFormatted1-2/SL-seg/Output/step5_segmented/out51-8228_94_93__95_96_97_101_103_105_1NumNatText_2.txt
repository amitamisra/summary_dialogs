<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX talking/VBG about/IN the/DT views/NNS of/IN gay/JJ marriage/NN in/IN our/PRP$ culture/NN ./. </C>
</S>
<S>
<C>S1/NNP is/AUX against/IN gay/JJ marriage/NN </C>
<C>but/CC thinks/VBZ it/PRP is/AUX unfair/JJ for/IN supporters/NNS of/IN gay/JJ marriage/NN to/TO link/VB his/PRP$ view/NN with/IN violence/NN towards/IN gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ violence/NN towards/IN gays/NNS is/AUX used/VBN </C>
<C>as/IN launching/VBG point/NN for/IN gays/NNS to/TO preach/VBP gay/JJ rights/NNS ./. </C>
</S>
<S>
<C>S2/NNP makes/VBZ reference/NN other/JJ poster/NN and/CC homophobic/JJ slurs/NNS that/IN be/AUX frequently/RB used/VBN on/IN the/DT thread/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/DT violence/NN towards/IN gay/JJ stem/NN from/IN slanderous/JJ representations/NNS of/IN gays/NNS ,/, </C>
<C>and/CC produces/VBZ a/DT toxic/JJ environment/NN where/WRB people/NNS will/MD act/VB in/IN violence/NN out/IN of/IN fear/NN ./. </C>
</S>
<S>
<C>S1/NNP is/AUX </C>
<C>once/IN again/RB gets/VBZ angered/VBN by/IN the/DT generalizations/NNS used/VBN by/IN S2/NNP ,/, </C>
<C>because/IN it/PRP does/AUX not/RB represent/VB his/PRP$ point/NN ./. </C>
</S>
<S>
<C>S2/NNP is/AUX happy/JJ S1/NNP is/AUX no/RB loner/RBR attacking/VBG him/PRP ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ he/PRP was/AUX only/RB responding/VBG to/TO the/DT attacks/NNS by/IN S2/NNP ./. </C>
</S>
<S>
<C>S2/NNP becomes/VBZ angry/JJ with/IN S1/NNP for/IN avoiding/VBG the/DT main/JJ topic/NN at/IN hand/NN ./. </C>
</S>
<S>
<C>The/DT discussion/NN diverges/VBZ into/IN S2/NNP quoting/VBG previous/JJ posters/NNS and/CC showing/VBG hateful/JJ speech/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ this/DT dehumanizing/NN talk/NN in/IN responsible/JJ for/IN hate/NN against/IN gays/NNS ./. </C>
</S>
</P>
</T>
