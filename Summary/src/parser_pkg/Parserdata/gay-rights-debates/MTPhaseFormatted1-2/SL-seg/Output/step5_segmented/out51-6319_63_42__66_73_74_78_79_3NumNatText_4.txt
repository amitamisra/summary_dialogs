<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG equality/NN and/CC natural/JJ rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN discrimination/NN on/IN people/NNS for/IN being/AUXG religious/JJ and/CC keeping/VBG people/NNS from/IN religion/NN infringes/VBZ on/IN natural/JJ rights/NNS ,/, </C>
<C>and/CC if/IN you/PRP are/AUX for/IN equality/NN for/IN gays/NNS </C>
<C>then/RB you/PRP need/AUX equality/NN for/IN all/DT ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ that/IN this/DT allows/VBZ people/NNS to/TO do/AUX whatever/WDT they/PRP want/VBP and/CC use/VBP their/PRP$ religious/JJ beliefs/NNS as/IN an/DT excuse/NN ./. </C>
</S>
<S>
<C>To/TO contradict/VB ,/, </C>
<C>S1/NNP states/VBZ that/IN only/RB serious/JJ religious/JJ views/NNS are/AUX taken/VBN into/IN account/NN ./. </C>
</S>
<S>
<C>S2/NNP then/RB proposes/VBZ a/DT scenario/NN where/WRB the/DT only/JJ person/NN available/JJ to/TO perform/VB the/DT gay/JJ marriage/NN ceremony/NN is/AUX exempted/VBN based/VBN on/IN religious/JJ beliefs/NNS ,/, </C>
<C>their/PRP$ rights/NNS will/MD not/RB be/AUX upholded/VBN ./. </C>
</S>
<S>
<C>S1/NNP rebuts/VBZ by/IN stating/VBG that/IN the/DT proposed/VBN provision/NN would/MD allow/VB someone/NN else/RB ,/, </C>
<C>and/CC this/DT is/AUX due/JJ to/TO religious/JJ freedom/NN ./. </C>
</S>
</P>
</T>
