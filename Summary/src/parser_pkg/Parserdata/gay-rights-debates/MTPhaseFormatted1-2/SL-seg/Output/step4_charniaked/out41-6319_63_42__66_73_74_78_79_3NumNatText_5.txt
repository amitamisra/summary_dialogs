(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (DT The) (NN discussion)) (VP (VBZ revolves) (PRT (RP around)) (SBAR (IN whether) (CC or) (RB not) (S (NP (DT a) (NNS clergy) (NN person)) (VP (MD should) (VP (AUX have) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB refuse) (S (VP (TO to) (VP (VB marry) (NP (NP (DT a) (JJ gay) (NN couple)) (VP (VBN based) (PP (IN on) (NP (JJ religious) (NNS beliefs))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (DT this)) (VP (AUX is) (ADJP (JJ alright) (S (VP (TO to) (VP (AUX do) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB agree))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ provides) (NP (NP (DT an) (NN example)) (PP (IN of) (S (NP (NN his/her)) (VP (VBG managing) (NP (NP (DT a) (NN restaurant)) (VP (VBG asking) (SBAR (IN if) (S (NP (PRP it)) (VP (MD would) (VP (AUX be) (ADJP (JJ acceptable)) (SBAR (IN for) (S (NP (PRP him)) (VP (TO to) (VP (VB refuse) (S (VP (TO to) (VP (VB serve) (S (NP (DT a) (NN boy)) (VP (VB scout) (PP (VBN based) (PP (IN on) (NP (DT the) (NN fact)))) (S (NP (PRP they)) (NP (NP (NN conflict)) (PP (IN with) (NP (JJR his/her) (JJ religious) (NNS beliefs))))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP He/she)) (ADVP (RB also)) (VP (VBZ believes) (SBAR (S (NP (NNS clergy) (NNS members)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN permitted) (S (VP (TO to) (VP (VB refuse) (S (NP (DT a) (NN union)) (ADJP (JJ due) (PP (TO to) (NP (DT the) (NN fact) (SBAR (S (NP (DT the) (NN tax) (NNS payers)) (VP (VBP pay) (NP (NP (DT the) (NN salary)) (PP (IN of) (NP (JJ such) (NNS people)))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (VP (VBG forcing) (SBAR (S (NP (DT this) (NN issue)) (VP (AUX is) (NP (NP (DT the) (NN equivalent)) (PP (TO to) (S (VP (VBG forcing) (S (NP (DT a) (JJ Jewish) (NN person)) (VP (TO to) (VP (VB eat) (NP (NN pork) (NNS sandwiches)))))))))))))) (. .)))

