(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ dominates) (NP (DT the) (NN debate))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX is) (PP (IN against) (S (VP (VBG using) (NP (NN religion))))) (, ,) (ADVP (RB specifically)) (NP (NP (JJ Biblical) (NNS references)) (PP (IN in) (NP (NP (DT the) (NN conversation)) (PP (IN around) (NP (JJ gay) (NN marriage))))))) (. .)))

(S1 (S (NP (PRP$ His) (JJ first) (NN comment)) (VP (VBZ seems) (S (VP (TO to) (VP (AUX be) (VP (VBG arguing) (PP (IN against) (NP (NP (DT a) (JJ previous) (NN mention)) (PP (IN of) (NP (VBN increased) (NNS earthquakes) (CC and) (NNS wars)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ argues) (PP (IN for) (NP (NP (DT the) (NN inclusion)) (CC and) (NP (NP (NN support)) (PP (IN of) (NP (NN minority) (NNS issues)))))) (PP (IN despite) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (PRP we)) (VP (VBP live) (PP (IN in) (NP (DT a) (NN majority) (NN rule) (NN state))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (ADVP (RB first)) (, ,) (NP (JJS most) (NNS people)) (VP (VBP fall) (PP (IN into) (NP (NP (DT some) (NN sort)) (PP (IN of) (NP (NN minority) (NN classification))))))) (CC and) (S (ADVP (JJ second)) (NP (DT the) (NNS minorities)) (VP (AUX are) (ADVP (RB still)) (ADJP (JJ large)) (PP (IN in) (NP (NN number)))))))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX is) (NP (NP (DT a) (NN change)) (PP (IN in) (NP (DT the) (NN conversation)))) (PP (IN about) (NP (JJ half) (NN way))) (PP (IN through) (PP (TO to) (NP (NP (NNP genetics)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (VP (VBZ compares) (NP (NN sexuality)) (PP (TO to) (NP (NP (DT a) (NN predisposition)) (PP (IN for) (NP (NN alcoholism)))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (DT the) (NN end))) (NP (PRP he)) (VP (VBZ feels) (SBAR (SBAR (S (NP (NN marriage)) (VP (MD must) (VP (AUX be) (ADJP (JJ consensual)))))) (CC and) (SBAR (IN that) (S (NP (DT any) (NNS parties)) (VP (MD may) (VP (AUX be) (ADJP (JJ free) (S (VP (TO to) (VP (VB leave))))) (, ,) (SBAR (IN whether) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ traditional) (, ,) (JJ homosexual) (CC or) (JJ polygamy))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (ADVP (RB mostly)) (VBZ argues) (NP (NP (DT the) (JJ religious) (NNS points)) (, ,) (PP (IN for) (NP (NN example)))) (S (VP (VBG citing) (NP (NP (DT the) (NN increase)) (PP (IN in) (NP (NNS earthquakes)))) (ADVP (IN outside) (PP (IN of) (NP (NNP Israel)))) (PP (IN in) (NP (JJ recent) (NNS years))) (SBAR (IN though) (S (NP (PRP he)) (VP (VBZ mentions) (NP (NP (DT another) (NN name)) (PP (IN of) (SBAR (WHNP (WP who)) (S (VP (VBD began) (NP (NP (DT the) (NN conversation)) (PP (IN around) (NP (NN religion))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (SBAR (WHADVP (WRB why)) (S (NP (DT the) (NN debate)) (VP (VBZ remains) (PP (IN around) (NP (NNS homosexuals)))))) (CC and) (SBAR (IN whether) (S (NP (NNS polygamists)) (VP (VBP fall) (PP (IN into) (NP (JJ similar) (NNS categories))) (PP (IN in) (NP (DT the) (JJ genetic) (NN argument)))))))) (. .)))

