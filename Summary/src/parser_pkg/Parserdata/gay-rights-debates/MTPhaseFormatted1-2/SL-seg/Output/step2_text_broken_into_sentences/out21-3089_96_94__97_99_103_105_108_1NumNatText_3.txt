<PARAGRAPH> S1 notes that although John Kerry was expected to win the election in Ohio, he lost.
He attributes this loss to the disapproval of homosexuality by Ohioans, as indicated by the passing of a gay marriage amendment and their support for President Bush who he claims won by a larger margin than President Clinton.
S2 rejects the claim that the people of Ohio were silent about their views of same-sex marriage and claims that although President Bush won with a larger margin, President Clinton still won by a larger percentage, due to the increased population and larger voter turnout in President Bush's election.
S1 claims that population is irrelevant, citing several statistics showing a low level of support for President Clinton, and also claims that the same-sex marriage issue definitely had an effect on the outcome of the Ohio election.
S2 again responds with statistics showing that President Clinton won by larger percentages than President Bush.
He does not deny that the same-sex marriage issue was relevant, but denies that there was a silent majority.
