<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG the/DT onslaught/NN of/IN AIDS/NNP in/IN the/DT United/NNP States/NNPS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ the/DT disease/NN was/AUX made/VBN into/IN a/DT huge/JJ health/NN concern/NN as/IN the/DT result/NN of/IN the/DT homosexual/JJ community/NN ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ no/DT one/NN really/RB knows/VBZ how/WRB AIDS/NNP originally/RB arrived/VBD in/IN the/DT United/NNP States/NNPS </C>
<C>and/CC the/DT fault/NN of/IN the/DT spread/NN lies/VBZ with/IN the/DT folks/NNS who/WP used/VBD dirty/JJ needles/NNS </C>
<C>when/WRB taking/VBG drugs/NNS and/CC people/NNS who/WP did/AUX not/RB practice/VB safe/JJ sex/NN ./. </C>
</S>
<S>
<C>S2/NNP also/RB cites/VBZ the/DT practice/NN of/IN untested/JJ blood/NN transfusions/NNS </C>
<C>before/IN it/PRP was/AUX discovered/VBN how/WRB AIDS/NNP could/MD be/AUX spread/VBN ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ that/IN S2/NNP has/AUX provided/VBN no/DT substantial/JJ proof/NN to/TO back/VB his/her/JJ arguments/NNS in/IN regard/NN to/TO how/WRB the/DT disease/NN actually/RB rose/VBD to/TO concerning/VBG levels/NNS in/IN America/NNP and/CC asks/VBZ for/IN a/DT debate/NN between/IN he/she/NNP and/CC S2/NNP ./. </C>
</S>
</P>
</T>
