<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN republicans/NNS focus/VB on/IN ``/`` hot/JJ button/NN ''/'' issues/NNS to/TO take/VB attention/NN away/RB from/IN the/DT important/JJ issues/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN republicans/NNS have/AUX made/VBN issues/NNS out/IN of/IN gay/JJ rights/NNS and/CC abortion/NN </C>
<C>by/IN pushing/VBG legislation/NN to/TO ban/VB them/PRP ./. </C>
</S>
<S>
<C>He/PRP also/RB suggests/VBZ that/IN democrats/NNS are/AUX more/RBR fiscally/RB responsible/JJ than/IN republicans/NNS whose/WP$ only/JJ interest/NN is/AUX to/TO provide/VB tax/NN breaks/NNS for/IN the/DT wealthy/JJ ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN republicans/NNS are/AUX so/RB concerned/JJ about/IN certain/JJ minorities/NNS receiving/VBG their/PRP$ constitutional/JJ rights/NNS that/IN they/PRP are/AUX willing/JJ to/TO change/VB the/DT constitution/NN to/TO prevent/VB it/PRP ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN the/DT republicans/NNS are/AUX focusing/VBG on/IN important/JJ issues/NNS such/JJ as/IN social/JJ security/NN ,/, taxes/NNS ,/, and/CC terrorism/NN ,/, </C>
<C>and/CC that/IN it/PRP is/AUX democrats/NNS who/WP are/AUX pushing/VBG unimportant/JJ issues/NNS like/IN abortion/NN ,/, marriage/NN equality/NN ,/, and/CC global/JJ warming/NN ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN if/IN democrats/NNS had/AUX not/RB been/AUX pushing/VBG such/PDT a/DT radical/JJ agenda/NN ,/, </C>
<C>then/RB the/DT republicans/NNS would/MD not/RB have/AUX had/AUX to/TO come/VB out/RP against/IN their/PRP$ position/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB rejects/VBZ the/DT idea/NN that/IN the/DT democrats/NNS are/AUX the/DT more/RBR fiscally/RB responsible/JJ party/NN ,/, </C>
<C>claiming/VBG that/IN the/DT social/JJ engineering/NN policies/NNS put/VBN in/IN place/NN by/IN the/DT democrats/NNS will/MD prevent/VB the/DT budget/NN from/IN ever/RB being/AUXG balanced/VBN ./. </C>
</S>
</P>
</T>
