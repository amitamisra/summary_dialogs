<T>
<P>
</P>
<P>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- argues/VBZ that/IN overtime-religious/JJ politicians/NNS ,/, conservative/JJ republicans/NNS have/AUX made/VBN the/DT effort/NN to/TO make/VB gay/JJ rights/NNS a/DT poison/NN topic/NN ./. </C>
</S>
<S>
<C>According/VBG to/TO his/PRP$ democrats/NNS want/VBP to/TO do/AUX the/DT right/JJ thing/NN however/RB ,/, they/PRP are/AUX force/NN to/TO moderate/VB their/PRP$ opinions/NNS for/IN public/JJ approval/NN ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- says/VBZ that/IN politicians/NNS lie/VBP about/IN their/PRP$ opinion/NN to/TO not/RB loose/JJ votes/NNS ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN he/PRP would/MD prefer/VB </C>
<C>if/IN politicians/NNS were/AUX honest/JJ ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S1/NNP -RRB-/-RRB- says/VBZ that/IN it/PRP is/AUX common/JJ knowledge/NN that/IN politicians/NNS are/AUX going/VBG to/TO lie/VB </C>
<C>because/IN they/PRP can/MD not/RB simply/RB say/VB things/NNS ,/, </C>
<C>they/PRP have/AUX to/TO be/AUX careful/JJ for/IN the/DT judgment/NN of/IN other/JJ ./. </C>
</S>
<S>
<C>The/DT particular/JJ case/NN of/IN Biden/NNP who/WP stated/VBD that/DT does/AUX not/RB support/VB gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN it/PRP might/MD be/AUX because/IN Biden/NNP did/AUX not/RB wanted/VBN to/TO risk/VB his/PRP$ support/NN and/CC his/PRP$ position/NN in/IN the/DT White/NNP House/NNP ./. </C>
</S>
<S>
<C>-LRB-/-LRB- S2/NNP -RRB-/-RRB- argues/VBZ that/IN -LRB-/-LRB- S1/NNP -RRB-/-RRB- is/AUX misunderstanding/VBG him/PRP </C>
<C>because/RB according/VBG to/TO him/PRP people/NNS need/AUX to/TO put/VB expectation/NN for/IN politicians/NNS to/TO be/AUX more/RBR clear/JJ and/CC honest/JJ about/IN their/PRP$ opinions/NNS ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN people/NNS need/AUX to/TO pay/VB close/JJ attention/NN to/TO politicians/NNS and/CC not/RB excuse/VB politicians/NNS cynicism/NN </C>
<C>so/RB that/IN the/DT American/JJ people/NNS can/MD have/AUX politicians/NNS to/TO be/AUX honest/JJ ./. </C>
</S>
</P>
</T>
