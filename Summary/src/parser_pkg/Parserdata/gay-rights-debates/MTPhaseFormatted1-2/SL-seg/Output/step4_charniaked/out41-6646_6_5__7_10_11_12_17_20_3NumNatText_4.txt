(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (S (VP (VBG protesting))) (VP (VBD made) (NP (DT a) (NN difference))))) (, ,) (CC but) (SBAR (RB not) (IN until) (S (NP (JJ enough) (NNS people)) (VP (AUX had) (VP (VBN grouped) (ADVP (RB together)) (S (VP (TO to) (VP (VB voice) (NP (PRP$ their) (NN opinion))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP point) (PRT (RP out)) (SBAR (IN that) (S (PP (IN despite) (NP (VBG protesting))) (, ,) (NP (NN change)) (VP (MD would) (RB not) (VP (AUX be) (VP (VBN brought) (PRT (RP about)) (SBAR (IN unless) (S (NP (NP (DT the) (NN will)) (PP (IN of) (NP (DT the) (NNS people)))) (VP (AUX was) (RB not) (VP (VBG desiring) (NP (DT that) (NN change)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (S (S (NP (DT this)) (VP (AUX is) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (NN system)) (VP (VBZ works)))))) (, ,) (CC and) (S (NP (DT any) (NN legislation)) (VP (AUX is) (NP (NP (NP (DT a) (NN matter)) (PP (IN of) (NP (NNS voters)))) (CC and) (NP (NP (DT the) (NN will)) (PP (IN of) (NP (VBN elected) (NNS officials) (S (VP (TO to) (VP (VB reflect) (SBAR (WHNP (WP what)) (S (NP (DT the) (NNS voters)) (VP (VBP desire)))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NP (DT the) (JJ true) (NN reason)) (PP (IN behind) (NP (DT the) (NN ballot) (NNS initiatives)))) (VP (AUX is) (S (VP (TO to) (VP (VB determine) (SBAR (IN whether) (CC or) (RB not) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT a) (JJ civil) (NN right)) (PP (IN in) (NP (DT the) (JJ first) (NN place))) (S (VP (TO to) (VP (VB give) (NP (JJ legal) (NN marriage)) (PP (TO to) (SBAR (WHNP (WDT whatever) (NN group) (NN arrangement)) (S (VP (AUX is) (PP (IN in) (NP (NN question))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN difference)) (PP (IN between) (NP (NP (NNS voters)) (VP (VBG voting) (PP (IN on) (NP (NP (NNS laws) (CC and) (NNS legislators)) (VP (VBG voting) (PP (IN on) (NP (NNS laws)))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT a) (JJ good) (NN thing)) (SBAR (S (VP (TO to) (VP (VB allow) (NP (NP (DT the) (NN majority) (NN vote)) (PP (IN on) (NP (JJ civil) (NNS rights) (NNS issues)))) (SBAR (IN because) (S (SBAR (IN if) (S (NP (DT a) (NN minority)) (VP (VBZ needs) (NP (VBG protecting))))) (, ,) (NP (PRP it)) (VP (MD will) (VP (AUX need) (NP (NP (NN protection)) (PP (ADVP (JJS most)) (IN from) (NP (NP (DT the) (NN majority)) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (VP (VBG trying) (NP (PRP$ their) (JJS best)) (S (VP (TO to) (VP (VB keep) (NP (PRP them)) (PRT (RP down)))))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (S (NP (NN society)) (VP (MD should) (VP (AUX be) (VP (VBN allowed) (S (VP (TO to) (VP (AUX be) (VP (VBN bigoted)))))))))))) (. .)))

