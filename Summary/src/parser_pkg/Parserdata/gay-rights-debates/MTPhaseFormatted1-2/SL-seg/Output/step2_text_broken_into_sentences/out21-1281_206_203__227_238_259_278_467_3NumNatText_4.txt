<PARAGRAPH> S1 claims that laws are based on morals and because the United States is a predominantly Christian nation, the laws are going to be biased toward Christian beliefs.
He asserts that homosexuality has negative consequences and harms the integrity of marriage.
He argues that people have the right to believe freely, but that the government controls behavior, citing the freedom of religion until the practices do not conform to society's rules.
He brings up the issue of equality, claiming that different religions are not actually equal even though the law says they should all be treated equally because although each of the religions has the same rights, they are still not all treated equally.
S2 rejects S1's differentiation between the right to believe and the right to behave and says it is inappropriate to compare homosexuality to non-consensual behaviors.
He claims that behaviors like drug trafficking and gambling have links to crime and should therefore be regulated, but that there is no reason to believe that homosexuality is harmful so it should not be controlled.
