<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>The/DT discussion/NN revolves/VBZ around/RP </C>
<C>whether/IN or/CC not/RB a/DT clergy/NNS person/NN should/MD have/AUX the/DT right/NN to/TO refuse/VB to/TO marry/VB a/DT gay/JJ couple/NN based/VBN on/IN religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ this/DT is/AUX alright/JJ to/TO do/AUX </C>
<C>while/IN S2/NNP does/AUX not/RB agree/VB ./. </C>
</S>
<S>
<C>S2/NNP provides/VBZ an/DT example/NN of/IN his/her/NN managing/VBG a/DT restaurant/NN asking/VBG if/IN it/PRP would/MD be/AUX acceptable/JJ for/IN him/PRP to/TO refuse/VB to/TO serve/VB a/DT boy/NN scout/VB based/VBN on/IN the/DT fact/NN they/PRP conflict/NN with/IN his/her/JJR religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>He/she/NNP also/RB believes/VBZ clergy/NNS members/NNS should/MD not/RB be/AUX permitted/VBN to/TO refuse/VB a/DT union/NN due/JJ to/TO the/DT fact/NN the/DT tax/NN payers/NNS pay/VBP the/DT salary/NN of/IN such/JJ people/NNS ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ forcing/VBG this/DT issue/NN is/AUX the/DT equivalent/NN to/TO forcing/VBG a/DT Jewish/JJ person/NN to/TO eat/VB pork/NN sandwiches/NNS ./. </C>
</S>
</P>
</T>
