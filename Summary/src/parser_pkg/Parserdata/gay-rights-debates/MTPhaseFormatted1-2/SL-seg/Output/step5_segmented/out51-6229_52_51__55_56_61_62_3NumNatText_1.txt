<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX talking/VBG about/IN how/WRB Democracy/NN is/AUX based/VBN on/IN majority/NN rule/NN </C>
<C>and/CC whether/IN or/CC not/RB its/PRP$ fair/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX completely/RB unfair/JJ that/IN gay/JJ marriage/NN be/AUX dismissed/VBN or/CC disliked/VBN </C>
<C>and/CC S2/NNP argues/VBZ that/IN no/DT matter/NN what/WP S1/NNP believes/VBZ is/AUX right/JJ or/CC wrong/JJ ,/, </C>
<C>the/DT majority/NN of/IN people/NNS control/VBP what/WP is/AUX normal/JJ in/IN our/PRP$ society/NN ./. </C>
</S>
<S>
<C>That/WDT if/IN S1/NNP is/AUX only/RB towards/IN the/DT rights/NNS of/IN gay/JJ people/NNS it/PRP is/AUX completely/RB understandable/JJ for/IN this/DT lady/NN to/TO be/AUX in/IN belief/NN that/IN marriage/NN as/RB is/AUX is/AUX fine/JJ ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN S1/NNP 's/POS argument/NN is/AUX not/RB a/DT legitimate/JJ viewpoint/NN </C>
<C>because/IN S1/NNP has/AUX no/DT justification/NN that/IN this/DT lady/NN hates/VBZ gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN because/IN this/DT lady/NN is/AUX not/RB in/IN favor/NN of/IN gay/JJ marriage/NN ,/, </C>
<C>the/DT lady/NN is/AUX anti-gay/JJ ./. </C>
</S>
<S>
<C>S1/NNP is/AUX taking/VBG an/DT aggressive/JJ approach/NN </C>
<C>by/IN saying/VBG they/PRP will/MD talk/VB to/TO bible/JJ churches/NNS and/CC disrupt/VB them/PRP </C>
<C>because/IN she/PRP feels/VBZ like/IN she/PRP has/AUX no/DT place/NN in/IN society/NN ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ that/IN the/DT majority/NN of/IN people/NNS is/AUX enslaving/VBG their/PRP$ people/NNS </C>
<C>by/IN not/RB allowing/VBG gay/JJ rights/NNS ,/, </C>
<C>but/CC S2/NNP argues/VBZ that/IN it/PRP happens/VBZ that/DT way/NN </C>
<C>and/CC it/PRP changes/VBZ over/IN time/NN ./. </C>
</S>
</P>
</T>
