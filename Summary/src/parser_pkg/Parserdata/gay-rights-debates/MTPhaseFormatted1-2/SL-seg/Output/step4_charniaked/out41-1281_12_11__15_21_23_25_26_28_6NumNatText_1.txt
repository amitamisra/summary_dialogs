(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (S (NP (DT that) (NN marriage)) (VP (AUX is) (VP (VBN defined) (PP (IN as) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (DT a) (JJ heterosexual) (NN contract))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ refutes) (NP (DT this) (NN assertion)) (, ,) (S (VP (VBG citing) (NP (NP (DT a) (NN number)) (PP (IN of) (NP (NP (NNS countries)) (SBAR (WHNP (WDT which)) (S (VP (VP (VBP recognize) (NP (JJ same-sex) (NN marriage))) (CC and) (VP (VBZ compares) (NP (NP (DT the) (NN lack)) (PP (IN of) (NP (JJ reproductive) (NN potential))) (PP (IN for) (NP (JJ homosexual) (NNS couples)))) (PP (TO to) (NP (NP (DT a) (JJ heterosexual) (NN couple)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBZ chooses) (S (RB not) (VP (TO to) (VP (AUX have) (NP (NNS children)))))) (CC or) (VP (AUX is) (ADJP (JJ unable) (S (VP (TO to) (VP (AUX have) (NP (NNS children)))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ single) (NN parent) (NNS families)) (VP (AUX are) (RB not) (VP (VBN considered) (S (ADJP (JJ traditional) (CC and) (NNS references)) (NP (NP (DT a) (NN poll)) (VP (VBG showing) (NP (NP (NN support)) (PP (IN for) (NP (JJ gay) (NNS rights))) (PP (IN among) (NP (DT the) (CD 18))) (: -) (ADJP (CD 29) (JJ demographic))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (DT a) (JJ traditional) (NN family)) (VP (AUX is) (VP (VBN composed) (PP (IN of) (NP (NP (DT a) (NN husband)) (CC and) (NP (NN wife)))))))) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NNS laws)) (PP (IN of) (NP (JJ other) (NNS countries)))) (VP (AUX are) (ADJP (JJ irrelevant) (PP (TO to) (NP (NP (DT the) (NNS laws)) (PP (IN of) (NP (NNP America))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ single) (NN parent) (NNS families)) (VP (VP (AUX are) (ADJP (JJ natural))) (CC and) (VP (VBZ suggests) (SBAR (S (NP (DT that) (NN time)) (VP (MD will) (VP (VB decide) (SBAR (WHNP (WDT which) (NN side)) (S (VP (AUX is) (ADJP (JJ right)))))))))))))) (. .)))

