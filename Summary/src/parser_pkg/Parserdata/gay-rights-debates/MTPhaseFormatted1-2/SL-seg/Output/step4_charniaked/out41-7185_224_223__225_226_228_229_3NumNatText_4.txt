(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBD suggested) (S (VP (VBG stoning) (NP (NP (NNP Vitter)) (, ,) (NP (NP (PRP$ his) (NNP hooker) (, ,) (NNP Craig) (CC and) (DT the) (NN man)) (SBAR (S (NP (PRP he)) (VP (AUX was) (PP (IN with) (PP (IN on) (NP (DT the) (NNP Capitol) (NNS steps)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD talked) (PP (IN about) (NP (NP (DT a) (`` ``) (JJ Christian) (NN man) ('' '')) (SBAR (WHNP (WP who)) (S (VP (VBD married) (SBAR (S (NP (NP (JJ young)) (, ,) (VP (AUX had) (NP (NNS children))) (, ,)) (VP (VP (VBD divorced) (NP (PRP$ his) (JJ first) (NN wife) (NN wife))) (, ,) (VP (VBD married) (ADJP (ADJP (NP (DT a) (NN woman)) (JJR younger)) (PP (IN than) (NP (PRP$ his) (JJS oldest) (NN child))))) (, ,) (CC and) (VP (ADVP (RB now)) (, ,) (SBAR (IN at) (S (NP (NP (DT the) (NN age)) (PP (IN of) (NP (CD 65)))) (VP (AUX has) (NP (CD two) (ADJP (RB very) (JJ young)) (NNS children))))))))))))))) (. .)))

(S1 (S (S (NP (DT This) (NN man)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX is) (NP (NP (DT a) (NN member)) (PP (IN of) (NP (NP (DT the) (NNS Churches)) (PP (IN of) (NP (NNP Christ))))))))))) (CC but) (S (NP (NNP James) (NNP Dobson)) (VP (VBZ says) (SBAR (S (NP (DT this) (NN man)) (VP (AUX is) (RB not) (NP (DT a) (JJ Christian))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (VBZ regards)) (PP (TO to) (NP (DT the) (VBG stoning) (NN comment))))) (, ,) (NP (NP (NNP S2)) (PP (IN with) (NP (NN humor)))) (VP (VBD said) (SBAR (IN that) (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX did) (NP (NN stone) (NNS people)) (PP (IN on) (NP (DT the) (NNP Capitol) (NNS steps))))))) (VP (MD could) (VP (VB get) (VP (VBN arrested) (PP (IN on) (NP (NN drug) (NNS charges))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD asked) (SBAR (IN if) (S (NP (NNP Dobson)) (VP (VBZ thinks) (SBAR (S (NP (DT the) (NN man)) (VP (VP (AUX is) (VP (VBG pretending) (S (VP (TO to) (VP (AUX be) (NP (DT a) (JJ Christian))))))) (CC and) (VP (VBD said) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ believes) (SBAR (S (NP (NNP Dobson)) (VP (AUX is) (ADVP (RB mostly)) (NP (NP (NN self)) (VP (VBG serving))))))))))))))))) (. .)))

