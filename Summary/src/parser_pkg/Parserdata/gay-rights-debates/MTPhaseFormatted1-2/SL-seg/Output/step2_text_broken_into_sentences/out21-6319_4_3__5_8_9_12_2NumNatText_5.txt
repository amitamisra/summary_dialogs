<PARAGRAPH> S1 notes a provision in law that makes performing marriage ceremonies by sanctioned marriage officers non-mandatory when it comes to same sex couples and marriage as long as performing said ceremony would conflict with that person's conscience, religion and belief.
S1 believes that forcing them to do so against their beliefs would infringe on their religious freedom.
S2 feels that it is unfortunate if that applies to public officials, as they believe that public officials must serve the public in its entirety, equally, under the law.
He or she feels that if one cannot perform the job, then that person should look for another job.
This person believes that it is an impediment to the public servant performing his or her job if it's true.
