<T>
<P>
</P>
<P>
<S>
<C>S1/NNP argues/VBZ that/IN the/DT proponents/NNS of/IN Prop/VB 8/CD need/AUX to/TO verify/VB that/IN the/DT California/NNP Amendment/NNP banning/VBG gay/JJ marriage/NN was/AUX enacted/VBN legally/RB ./. </C>
</S>
<S>
<C>This/DT person/NN believes/VBZ that/IN if/IN DOMA/NNP is/AUX not/RB declared/VBN unconstitutional/JJ than/IN those/DT arguing/VBG against/IN Prop/VB 8/CD do/AUX not/RB have/AUX a/DT valid/JJ platform/NN ./. </C>
</S>
<S>
<C>He/PRP or/CC she/PRP argue/VBP that/IN they/PRP would/MD have/AUX to/TO first/RB strike/VB down/RP the/DT federal/JJ law/NN that/WDT gives/VBZ states/NNS the/DT right/NN to/TO ignore/VB other/JJ states/NNS '/POS laws/NNS </C>
<C>before/IN they/PRP could/MD do/AUX the/DT same/JJ to/TO the/DT California/NNP Amendment/NNP ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN the/DT California/NNP Amendment/NN is/AUX being/AUXG tried/VBN to/TO see/VB if/IN it/PRP violates/VBZ the/DT 14th/JJ Amendment/NN of/IN the/DT Constitution/NNP ./. </C>
</S>
<S>
<C>This/DT person/NN believes/VBZ that/IN DOMA/NNP violates/VBZ the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>This/DT person/NN argues/VBZ that/IN there/EX is/AUX nothing/NN in/IN DOMA/NNP that/WDT prevents/VBZ California/NNP 's/POS court/NN from/IN ruling/VBG the/DT 14th/JJ amendment/NN requires/VBZ gay/JJ marriage/NN ./. </C>
</S>
</P>
</T>
