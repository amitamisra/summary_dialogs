<s> <PARAGRAPH> </s>
<s>  S1 believes that new England and New York will have gay marriage within the next few years and that the west coast might come soon after. </s>
<s> They argue that after this time period, gay marriage legislation will stop for the next twenty years or so and that thirty states already define marriage as between one man and one woman. </s>
<s> They do not believe that the supreme court would overturn gay marriage bans as unconstitutional and believes that states will ultimately be the ones to recognize whether it should be legal or not. </s>
<s> They argue that current marriage laws are disputed between states and have been for decades and believes that the courts will be unable to intervene. </s>
<s> S2 believes that a federal recognition of gay marriage will be soon coming, and argues against S1's claims on the basis of Roberts and Alito being constitutional literalists. </s>
<s> They believe that there is enough support in the Supreme Court to follow the constitution as written and that it will find any laws prohibiting gay marriage unconstitutional as a result. </s>
<s>  </s>
