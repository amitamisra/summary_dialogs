<T>
<P>
</P>
<P>
<S>
<C>S1/NNP seems/VBZ to/TO agree/VB with/IN the/DT comparison/NN between/IN interracial/JJ marriage/NN and/CC same/JJ sex/NN marriage/NN ,/, </C>
<C>citing/VBG that/IN many/JJ of/IN the/DT arguments/NNS against/IN them/PRP are/AUX similar/JJ ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN words/NNS can/MD be/AUX redefined/VBD including/VBG the/DT word/NN marriage/NN and/CC uses/VBZ war/NN as/IN an/DT example/NN ./. </C>
</S>
<S>
<C>He/PRP brings/VBZ up/RP the/DT fact/NN that/IN war/NN has/AUX not/RB been/AUX declared/VBN since/IN WWII/NNP </C>
<C>but/CC the/DT country/NN has/AUX been/AUX involved/VBN in/IN war-like/JJ situations/NNS </C>
<C>and/CC typically/RB these/DT are/AUX referred/VBN to/TO as/IN war/NN ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ this/DT </C>
<C>as/IN being/AUXG off/RB topic/NN </C>
<C>but/CC this/DT debate/NN ends/VBZ up/RP dominating/VBG the/DT remaining/VBG part/NN of/IN the/DT text/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB agree/VB that/IN marriage/NN should/MD be/AUX redefined/VBD </C>
<C>to/TO include/VB same/JJ sex/NN couples/NNS ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB believe/VB himself/PRP </C>
<C>to/TO be/AUX a/DT homophobe/NN and/CC cites/VBZ friendships/NNS with/IN gay/JJ individuals/NNS to/TO back/VB up/RP his/PRP$ claim/NN ./. </C>
</S>
<S>
<C>His/PRP$ issue/NN is/AUX not/RB with/IN rights/NNS or/CC the/DT actual/JJ relationships/NNS themselves/PRP </C>
<C>,/, he/PRP believes/VBZ in/IN full/JJ rights/NNS for/IN gay/JJ couples/NNS ,/, </C>
<C>but/CC he/PRP does/AUX not/RB believe/VB that/IN the/DT relationships/NNS should/MD be/AUX able/JJ to/TO bear/VB the/DT label/NN marriage/NN </C>
<C>because/IN it/PRP is/AUX not/RB between/IN a/DT man/NN and/CC woman/NN ./. </C>
</S>
<S>
<C>When/WRB the/DT topic/NN changes/VBZ to/TO the/DT use/NN of/IN the/DT term/NN war/NN </C>
<C>he/PRP does/AUX not/RB disagree/VB with/IN S1/NNP </C>
<C>but/CC gives/VBZ information/NN challenging/VBG previous/JJ statements/NNS ./. </C>
</S>
</P>
</T>
