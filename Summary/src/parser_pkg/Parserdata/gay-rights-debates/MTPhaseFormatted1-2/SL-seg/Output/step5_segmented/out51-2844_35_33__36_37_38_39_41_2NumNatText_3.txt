<T>
<P>
</P>
<P>
<S>
<C>Two/CD subjects/NNS are/AUX discussing/VBG liberalism/NN and/CC its/PRP$ similarities/NNS or/CC lack/NN thereof/RB to/TO the/DT Jeffersonian/NNP belief/NN system/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ they/PRP are/AUX nothing/NN alike/RB </C>
<C>while/IN S1/NNP believes/VBZ they/PRP have/AUX many/JJ similarities/NNS ./. </C>
</S>
<S>
<C>S2/NNP advises/VBZ Jefferson/NNP believed/VBD liberty/NN would/MD be/AUX threatened/VBN by/IN a/DT centralized/JJ government/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB advises/VBZ liberalism/NN is/AUX not/RB based/VBN on/IN the/DT same/JJ theory/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ the/DT opposite/NN stating/VBG the/DT opinion/NN that/IN the/DT group/NN believes/VBZ in/IN fairness/NN for/IN all/DT ,/, except/IN the/DT gay/JJ community/NN ./. </C>
</S>
<S>
<C>S2/NNP advises/VBZ he/PRP supports/VBZ equal/JJ opportunities/NNS for/IN all/DT people/NNS despite/IN their/PRP$ sexual/JJ preferences/NNS ;/: </C>
<C>however/RB ,/, he/PRP does/AUX not/RB support/VB the/DT redefinition/NN of/IN marriage/NN to/TO allow/VB for/IN gay/JJ unions/NNS ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ this/DT is/AUX a/DT double/JJ standard/NN </C>
<C>because/IN it/PRP is/AUX basically/RB saying/VBG equal/JJ rights/NNS for/IN all/DT </C>
<C>but/CC gays/NNS still/RB cannot/VBP marry/VB ./. </C>
</S>
</P>
</T>
