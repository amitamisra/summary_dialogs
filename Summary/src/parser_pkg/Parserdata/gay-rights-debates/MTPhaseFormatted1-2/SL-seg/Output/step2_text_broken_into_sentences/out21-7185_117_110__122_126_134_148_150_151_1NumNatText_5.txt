<PARAGRAPH> S1 Accuses S2 of using straw-men in arguments.
States courts have supported equal protection.
Claims S2 does not understand definition of equality, and of being maliciously disingenuous.
S2 Denies use of straw-men, asks for their use to be pointed out.
Denies original intent apply to gay marriage under equal protection due to lack of documentation.
Asks why S1 does not answer question.
References other limitations to marriage that exist.
S1 Sarcastically calls self stupid for believing constitutional mention of equal protection applied to gays.
S2 Questions existence of documentation that links equal protection to gays but not other groups.
S1 Accuses S2 of bigotry and links denial of gay marriage to being a denial of "life, liberty and the pursuit of happiness".
S2 Accuses S1 of ignoring S2's points and instead insulting him.
Accuses S1 of borderline breaking the rules of the forum.
S1 States that the lack of equal protection is obvious to a blind-man, S2 is deliberately denying facts.
S2 Questions why it is wrong to deny gay marriage but not to deny incestuous marriage.
