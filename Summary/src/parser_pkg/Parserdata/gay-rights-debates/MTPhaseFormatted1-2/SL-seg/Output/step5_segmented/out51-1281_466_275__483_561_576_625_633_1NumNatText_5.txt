<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN abortions/NNS and/CC unwed/JJ couples/NNS have/AUX nothing/NN to/TO do/AUX with/IN wars/NNS and/CC earthquakes/NNS frequencies/NNS ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN earthquakes/NNS have/AUX increased/VBN exponentially/RB outside/JJ of/IN Israel/NNP </C>
<C>and/CC that/IN the/DT majority/NN makes/VBZ what/WP ever/RB rules/VBZ it/PRP likes/VBZ ./. </C>
</S>
<S>
<C>S1/NNP claims/VBZ that/IN is/AUX a/DT religious/JJ argument/NN about/IN the/DT earthquakes/NNS and/CC either/RB does/AUX not/RB apply/VB </C>
<C>or/CC if/IN you/PRP are/AUX religious/JJ this/DT is/AUX not/RB the/DT right/JJ place/NN to/TO discuss/VB it/PRP ./. </C>
</S>
<S>
<C>S1/NNP claims/VBZ that/IN making/VBG this/DT a/DT religious/JJ argument/NN is/AUX not/RB needed/VBN ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN references/NNS to/TO a/DT loving/VBG parent/NN not/RB killing/VBG some/DT of/IN their/PRP$ children/NNS are/AUX wrong/JJ ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN S1/NNP claimed/VBD earthquakes/NNS are/AUX not/RB a/DT natural/JJ occurrence/NN ./. </C>
</S>
<S>
<C>S2/NNP refutes/VBZ that/IN he/PRP stated/VBD that/DT ./. </C>
</S>
<S>
<C>S1/NNP and/CC S2/NNP go/VBP back/RB and/CC forth/RB arguing/VBG about/IN refuting/VBG each/DT other/JJ 's/POS refutations/NNS and/CC inferences/NNS ./. </C>
</S>
<S>
<C>S1/NNP claims/VBZ that/IN polygamists/NNS and/CC so/RB on/IN will/MD want/VB marriage/NN rights/NNS now/RB as/RB well/RB after/IN homosexuals/NNS ./. </C>
</S>
<S>
<C>S2/NNP refutes/VBZ the/DT slippery/JJ slope/NN argument/NN ./. </C>
</S>
</P>
</T>
