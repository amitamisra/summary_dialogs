<T>
<P>
</P>
<P>
<S>
<C>S1/NNP feels/VBZ that/IN issues/NNS involving/VBG legislative/JJ movements/NNS are/AUX decided/VBN by/IN society/NN ./. </C>
</S>
<S>
<C>Though/IN the/DT legislature/NN votes/VBZ on/IN national/JJ issues/NNS ,/, </C>
<C>the/DT power/NN of/IN the/DT citizens/NNS is/AUX what/WP grants/VBZ or/CC denies/VBZ civil/JJ rights/NNS legislation/NN ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ the/DT hot/JJ issue/NN right/RB now/RB involves/VBZ gay/JJ marriage/NN ,/, </C>
<C>and/CC the/DT outcome/NN of/IN that/DT issue/NN will/MD be/AUX a/DT voting/NN matter/NN ./. </C>
</S>
<S>
<C>According/VBG to/TO him/PRP ,/, </C>
<C>society/NN determines/VBZ its/PRP$ own/JJ standards/NNS </C>
<C>and/CC it/PRP is/AUX not/RB possible/JJ to/TO make/VB gay/JJ marriage/NN civil/JJ rights/NNS matter/NN ./. </C>
</S>
<S>
<C>Legal/JJ marriage/NN cannot/NN just/RB be/AUX handed/VBN out/RP to/TO any/DT group/NN that/WDT calls/VBZ for/IN it/PRP ;/: </C>
<C>society/NN determines/VBZ the/DT facts/NNS and/CC acts/VBZ reasonably/RB and/CC accordingly/RB to/TO the/DT facts/NNS given/VBN about/IN the/DT issue/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ that/IN civil/JJ rights/NNS issues/NNS should/MD not/RB be/AUX left/VBN up/IN to/TO society/NN ,/, </C>
<C>especially/RB if/IN that/DT society/NN is/AUX the/DT main/JJ deterrent/NN in/IN approving/VBG a/DT group/NN 's/POS rights/NNS ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN the/DT government/NN should/MD be/AUX the/DT force/NN behind/IN protecting/VBG citizens/NNS '/POS rights/NNS against/IN the/DT hatred/NN and/CC bigotry/NN of/IN the/DT masses/NNS ./. </C>
</S>
<S>
<C>The/DT government/NN has/AUX involved/VBN itself/PRP in/IN civil/JJ rights/NNS issues/NNS in/IN the/DT past/NN ,/, </C>
<C>and/CC should/MD step/VB up/RP </C>
<C>to/TO defend/VB gay/JJ rights/NNS </C>
<C>when/WRB it/PRP comes/VBZ to/TO legal/JJ marriage/NN ./. </C>
</S>
</P>
</T>
