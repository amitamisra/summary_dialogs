<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN in/IN 1909/CD ,/, the/DT stance/NN against/IN homosexuality/NN was/AUX based/VBN on/IN a/DT moral/JJ obligation/NN and/CC not/RB prejudice/NN </C>
<C>because/IN homosexuality/NN at/IN that/DT time/NN was/AUX viewed/VBN as/IN behavioral/JJ and/CC not/RB genetic/JJ ./. </C>
</S>
<S>
<C>This/DT person/NN parallels/VBZ the/DT treatment/NN of/IN women/NNS and/CC blacks/NNS during/IN that/DT time/NN period/NN and/CC state/NN that/IN it/PRP was/AUX viewed/VBN ,/, back/RB then/RB ,/, as/IN morally/RB right/JJ ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN since/IN most/JJS people/NNS viewed/VBD homosexuality/NN as/IN a/DT behavior/NN and/CC not/RB a/DT state/NN of/IN being/AUXG in/IN 1909/CD ,/, </C>
<C>it/PRP could/MD not/RB have/AUX been/AUX prejudiced/VBN ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB that/IN there/EX was/AUX a/DT social/JJ identity/NN among/IN homosexuals/NNS in/IN 1909/CD </C>
<C>because/IN society/NN viewed/VBD it/PRP as/IN morally/RB wrong/JJ ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ against/IN the/DT idea/NN that/IN ignorance/NN is/AUX a/DT reasonable/JJ excuse/NN for/IN prejudice/NN ./. </C>
</S>
<S>
<C>This/DT person/NN also/RB uses/VBZ the/DT parallel/NN of/IN treatment/NN of/IN blacks/NNS and/CC women/NNS ,/, </C>
<C>but/CC argues/VBZ that/IN morality/NN was/AUX an/DT excuse/NN to/TO hide/VB the/DT prejudice/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN ignorance/NN is/AUX what/WP led/VBD to/TO a/DT distinct/JJ prejudice/NN towards/IN certain/JJ lifestyles/NNP and/CC people/NNS ,/, </C>
<C>and/CC argues/VBZ against/IN the/DT idea/NN that/IN there/EX was/AUX not/RB a/DT social/JJ identity/NN for/IN homosexuals/NNS in/IN 1909/CD when/WRB there/EX was/AUX obvious/JJ disapproval/NN aimed/VBN in/IN their/PRP$ direction/NN ./. </C>
</S>
</P>
</T>
