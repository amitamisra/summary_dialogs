<s> <PARAGRAPH> </s>
<s>  S1 believes gay marriage is about economics. </s>
<s> It would create a new dependent class and be a large new tax on businesses, Social Security, and healthcare. </s>
<s> He thinks same sex partners living together are roommates. </s>
<s> Roommates are single and should not have marriage benefits. </s>
<s> S1 states he is a Jeffersonian liberal and believes in the Constitution as written and the people's will. </s>
<s> Most Americans, he thinks, are against gay marriage. </s>
<s> S2 states that gay people pay into those same systems and believes they should also benefit from them. </s>
<s> He believes there is an economic burden when same sex couples break up without legal recognition of the marriage. </s>
<s> He states that same sex couples are not single and they are not the same thing as roommates. </s>
<s>  </s>
