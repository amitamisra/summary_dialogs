<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG gay/JJ relations/NNS and/CC the/DT possibility/NN of/IN redefining/VBG marriage/NN to/TO include/VB the/DT gay/JJ community/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ gay/JJ relationships/NNS are/AUX a/DT perversion/NN of/IN the/DT normal/JJ thought/NN of/IN society/NN ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN S1/NNP hates/VBZ gays/NNS and/CC is/AUX basically/RB prejudice/NN toward/IN gay/JJ people/NNS based/VBN </C>
<C>upon/IN some/DT remarks/NNS being/AUXG made/VBN ./. </C>
</S>
<S>
<C>Particularly/RB the/DT label/NN of/IN perversion/NN ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN S1/NNP is/AUX not/RB against/IN all/DT diversity/NN ,/, just/RB gay/JJ lifestyle/NN which/WDT in/IN S2/NNP 's/POS opinion/NN makes/VBZ him/her/RBR hateful/JJ toward/IN gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ he/she/NN does/AUX not/RB hate/VB gays/NNS </C>
<C>but/CC does/AUX not/RB feel/VB marriage/NN should/MD be/AUX redefined/VBD </C>
<C>to/TO allow/VB for/IN them/PRP to/TO wed/VBN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ S1/NNP 's/POS objection/NN to/TO gays/NNS and/CC the/DT family/NN 's/POS they/PRP would/MD make/VB is/AUX prejudice/NN ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ S2/NNP is/AUX wrong/JJ in/IN his/her/NN beliefs/NNS ./. </C>
</S>
</P>
</T>
