<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage. </s>
<s> S1 states that US laws does not say man and woman because it presumes that marriage is between a man and a women. </s>
<s> He makes the argument that gay people should be allowed to marry not because of an automatic right, but because it does no harm to society. </s>
<s> He states that in Britain, gay marriage has had no impact to society, and even though Christians believe gay sex is a sin, many other people commit sins and are still allowed to marry. </s>
<s> S2 states that the fact that gay marriage in ancient Rome was allowed means historically it was not always between man and women, as claimed, and gay marriage does not threaten Christianity. </s>
<s>  </s>
