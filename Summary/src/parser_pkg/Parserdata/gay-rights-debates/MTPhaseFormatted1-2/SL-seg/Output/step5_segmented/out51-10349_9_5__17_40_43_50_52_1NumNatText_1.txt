<T>
<P>
</P>
<P>
<S>
<C>S1/NNP States/NNPS that/IN the/DT current/JJ definition/NN of/IN marriage/NN specifically/RB excludes/VBZ homosexuals/NNS </C>
<C>because/IN the/DT current/JJ definition/NN is/AUX ``/`` two/CD people/NNS that/WDT are/AUX not/RB underage/JJ ,/, or/CC opposite/JJ genders/NNS ,/, same/JJ species/NNS ,/, not/RB closely/RB related/JJ and/CC not/RB already/RB married/JJ ''/'' </C>
<C>and/CC that/IN gays/NNS want/VBP to/TO be/AUX married/VBN under/IN current/JJ definition/NN ./. </C>
</S>
<S>
<C>Believes/VBZ that/IN ability/NN to/TO reproduce/VB is/AUX implied/VBN in/IN definition/NN ./. </C>
</S>
<S>
<C>S2/NNP Claims/NNPS S1/NNP 's/POS definition/NN is/AUX not/RB only/RB possible/JJ ,/, </C>
<C>refers/VBZ to/TO lack/NN of/IN reproductive/JJ abilities/NNS of/IN an/DT elderly/JJ person/NN and/CC a/DT younger/JJR person/NN marrying/VBG ./. </C>
</S>
<S>
<C>S1/NNP Refers/VBZ to/TO differences/NNS between/IN cultural/JJ definitions/NNS of/IN marriage/NN and/CC mating/NN pairs/NNS or/CC legal/JJ definitions/NNS of/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/NNS Ask/VBP who/WP agve/VBP S1/NNP right/NN to/TO define/VB cultural/JJ meanings/NNS ./. </C>
</S>
<S>
<C>S1/NNP Claims/VBZ that/IN cultural/JJ meaning/NN comes/VBZ from/IN observation/NN </C>
<C>and/CC that/IN legal/JJ definition/NN can/MD be/AUX decided/VBN any/DT way/NN ./. </C>
</S>
<S>
<C>S2/NNP Asks/VBZ who/WP gets/VBZ to/TO make/VB that/DT legal/JJ definition/NN ./. </C>
</S>
<S>
<C>Claims/VBZ that/IN those/DT that/WDT oppose/VBP gay/JJ marriage/NN are/AUX opposing/VBG something/NN that/WDT harms/VBZ no/DT one/NN and/CC are/AUX trying/VBG to/TO limit/VB other/JJ 's/POS actions/NNS ./. </C>
</S>
<S>
<C>S1/NNP States/NNPS that/IN in/IN America/NNP people/NNS vote/VB on/IN laws/NNS or/CC those/DT that/WDT make/VBP them/PRP </C>
<C>and/CC that/WDT is/AUX who/WP gets/VBZ to/TO determine/VB legality/NN of/IN different/JJ marriage/NN concepts/NNS ./. </C>
</S>
</P>
</T>
