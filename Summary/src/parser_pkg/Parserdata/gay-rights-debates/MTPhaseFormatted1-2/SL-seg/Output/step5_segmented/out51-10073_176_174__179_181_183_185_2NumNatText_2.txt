<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ that/IN comparing/VBG homosexuals/NNS to/TO black/JJ people/NNS is/AUX a/DT bad/JJ argument/NN </C>
<C>because/IN people/NNS are/AUX born/VBN black/JJ but/CC gay/JJ people/NNS make/VB a/DT choice/NN to/TO have/AUX gay/JJ sex/NN which/WDT he/PRP considers/VBZ </C>
<C>to/TO be/AUX a/DT sin/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ if/IN it/PRP is/AUX not/RB a/DT choice/NN </C>
<C>then/RB we/PRP should/MD be/AUX on/IN guard/NN at/IN shopping/NN malls/NNS and/CC street/NN corners/NNS ./. </C>
</S>
<S>
<C>The/DT implication/NN is/AUX people/NNS could/MD be/AUX attacked/VBN </C>
<C>if/IN is/AUX is/AUX not/RB a/DT choice/NN ./. </C>
</S>
<S>
<C>He/PRP views/VBZ gay/JJ sex/NN as/IN a/DT violation/NN of/IN the/DT word/NN of/IN God/NNP ./. </C>
</S>
<S>
<C>S2/NNP laughs/VBZ at/IN the/DT idea/NN of/IN homosexuality/NN being/AUXG a/DT choice/NN and/CC says/VBZ that/IN he/PRP does/AUX not/RB have/AUX a/DT choice/NN in/IN being/AUXG attracted/VBN to/TO either/DT males/NNS or/CC females/NNS ./. </C>
</S>
<S>
<C>He/PRP talks/VBZ about/IN sexual/JJ attraction/NN verses/NNS the/DT actual/JJ sex/NN act/NN ./. </C>
</S>
<S>
<C>He/PRP rejects/VBZ the/DT idea/NN gay/JJ people/NNS should/MD be/AUX celibate/JJ forever/RB ./. </C>
</S>
</P>
</T>
