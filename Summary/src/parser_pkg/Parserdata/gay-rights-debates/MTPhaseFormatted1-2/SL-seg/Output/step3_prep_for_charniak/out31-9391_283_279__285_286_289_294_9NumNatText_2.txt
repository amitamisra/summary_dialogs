<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing gay marriage and relationships. </s>
<s> S1 is opposed to the issue while S2 is for it. </s>
<s> S1 believes those who practice gay relationships are spiritually lacking and feels S2's sarcasm toward the issue at hand is proof of this. </s>
<s> S2 feels S1 is too literal and obsessed with the issue of gay sex rather than gay marriage. </s>
<s> S2 advises what happens in the bedroom is private and not the issue. </s>
<s> S1 makes some rude remarks in regard to gay sex involving gerbils. </s>
<s> S2 feels many heterosexuals also have a history of doing vulgar things as well and it is unfair to categorize only gay individuals in this manner. </s>
<s> S1 does not agree with this. </s>
<s>  </s>
