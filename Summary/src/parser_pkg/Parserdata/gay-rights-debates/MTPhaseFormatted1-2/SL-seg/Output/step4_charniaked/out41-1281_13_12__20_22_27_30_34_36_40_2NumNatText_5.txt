(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX is) (VP (VBG recognizing) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (DT a) (JJ natural) (JJ heterosexual) (NN contract)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (NNS counters) (PP (IN by) (S (VP (VBG asking) (PP (IN about) (NP (JJ artificial) (NN insemination))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (PP (PP (IN about) (NP (NP (DT the) (NN way)) (SBAR (S (NP (NNS children)) (VP (AUX are) (VP (VBN created))))))) (CC but) (PP (IN about) (NP (NP (DT the) (NN nature)) (PP (IN of) (NP (NP (DT the) (NN relationship)) (, ,) (SBAR (WHNP (WDT which)) (S (VP (VBZ remains) (ADJP (JJ natural)))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ questions) (SBAR (WHNP (WDT which)) (S (NP (NNP god)) (VP (MD would) (VP (VB enforce) (NP (NN marriage)) (SBAR (WHADVP (WRB when)) (S (NP (DT the) (NN ceremony)) (VP (AUX is) (VP (AUX done) (PP (IN by) (NP (DT a) (NN judge))))))))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN polygamy)) (VP (AUX is) (NP (NP (DT a) (NN way)) (SBAR (IN for) (S (NP (NNS bisexuals)) (VP (TO to) (VP (VB stay) (PP (IN under) (NP (DT the) (NN radar))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (NNP US)) (VP (AUX has) (VP (AUX been) (VP (VBG dealing) (PP (IN with) (NP (NP (NN polygamy)) (PP (IN for) (NP (NNS years))))) (, ,) (PP (IN with) (NP (ADJP (RB very) (JJ few)) (NNS prosecutions))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NN polygamy)) (VP (MD would) (RB not) (VP (VB count) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (NP (NP (CD one) (NN man)) (CC or) (NP (NP (NNS women)) (VP (JJ married) (PP (TO to) (NP (NP (JJ multiple) (NNS members)) (PP (IN of) (NP (DT the) (JJ opposite) (NN sex)))))))))))))))) (. .)))

