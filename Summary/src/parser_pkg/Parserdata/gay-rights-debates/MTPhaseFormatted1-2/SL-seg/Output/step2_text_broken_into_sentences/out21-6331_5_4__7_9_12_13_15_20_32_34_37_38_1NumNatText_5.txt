<PARAGRAPH> S1 argues that group marriages would not make sense under current marriage laws, as they only account for two individuals in a legal marriage.
They state that opponents of homosexual marriage tend to argue that a change to marriage law would make it too open ended.
This person does not believe that to be true, and argues that homosexuals are not trying to broaden the scope of marriage, but only change the law to be inclusive of themselves.
They believe that bringing up hypothetical arguments to the contrary is a distraction.
S2 argues that an effect of homosexuals being allowed to legally marry would lead to even more changes to marriage laws that would include more than simply homosexual relationships.
This person believes that allowing homosexual marriage is a case that broadens marriage and if allowed, can open basis for future arguments to broaden the law further.
They firmly believe that marriage as an institution is between a heterosexual male and female and that changing that in any way gives legal grounds to continue changing it.
