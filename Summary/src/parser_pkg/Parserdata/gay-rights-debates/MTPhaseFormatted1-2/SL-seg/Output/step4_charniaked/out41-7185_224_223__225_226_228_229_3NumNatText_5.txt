(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNP Vitter) (CC and) (NNP Craig)) (VP (MD should) (VP (AUX be) (NP (NP (NP (NNS stones)) (PP (IN on) (NP (DT the) (NNP Capitol) (NNS Steps)))) (CONJP (RB as) (RB well) (IN as)) (NP (NP (DT the) (NNS people)) (SBAR (S (NP (PRP they)) (VP (VBD slept) (PP (IN with)))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ states) (NP (NP (DT a) (NN hypocrisy)) (PP (IN with) (NP (NP (NNP Fred) (NNP Thompson)) (, ,) (SBAR (WHADVP (WRB where)) (S (NP (PRP he)) (VP (VBD got) (S (NP (NP (PRP$ his) (ADJP (CD eighteen) (NN year)) (JJ old) (NN girlfriend)) (, ,) (NP (NNP Sarah) (NNP Lindsey)) (, ,)) (ADJP (JJ pregnant) (SBAR (WHNP (WDT which)) (S (VP (VBD prompted) (NP (NP (DT an) (JJ immediate) (NN marriage)) (SBAR (WHNP (WDT that)) (S (VP (VBD lasted) (PP (IN for) (NP (JJ twenty-six) (NNS years))) (SBAR (IN before) (S (NP (DT a) (NN divorce)) (VP (AUX was) (VP (VBN ordered))))))))))))))))))))) (. .)))

(S1 (S (PP (IN After) (S (VP (VBG remarrying)))) (, ,) (NP (NNP S1)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (PP (IN in) (NP (NP (NN addition)) (PP (TO to) (NP (NP (CD three) (NNS children)) (PP (IN from) (NP (DT the) (JJ previous) (NN marriage))))))) (, ,) (NP (NNP Thompson)) (VP (AUX had) (NP (QP (CD two) (JJR more))) (PP (IN with) (NP (NP (DT a) (NN woman)) (SBAR (WHNP (WP who)) (S (VP (AUX was) (ADJP (ADJP (JJR younger)) (PP (IN than) (NP (PRP$ his) (JJS oldest) (NN child))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNP James) (NNP Dobson)) (VP (AUX does) (RB not) (VP (VB believe) (S (NP (NNP Thompson)) (VP (TO to) (VP (AUX be) (NP (DT a) (JJ true) (JJ Christian)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ seems) (S (VP (TO to) (ADVP (RB partially)) (VP (VB agree) (PP (IN with) (NP (NNP S1)))))) (, ,) (SBAR (IN though) (S (NP (DT this) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT a) (NN stoning)) (VP (MD would) (VP (VB result) (PP (IN in) (NP (NP (DT the) (NN arrest)) (PP (IN of) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBN perpetrated) (NP (PRP it)) (PP (IN on) (NP (NN drug) (NNS charges)))))))))))))))))) (. .)))

