<T>
<P>
</P>
<P>
<S>
<C>S1/NNP Accuses/VBZ S2/NNP of/IN using/VBG straw-men/NNS in/IN arguments/NNS ./. </C>
</S>
<S>
<C>States/NNS courts/NNS have/AUX supported/VBN equal/JJ protection/NN ./. </C>
</S>
<S>
<C>Claims/NNP S2/NNP does/AUX not/RB understand/VB definition/NN of/IN equality/NN ,/, and/CC of/IN being/AUXG maliciously/RB disingenuous/JJ ./. </C>
</S>
<S>
<C>S2/NNP Denies/VBZ use/NN of/IN straw-men/NN ,/, asks/VBZ for/IN their/PRP$ use/NN to/TO be/AUX pointed/VBN out/RP ./. </C>
</S>
<S>
<C>Denies/VBZ original/JJ intent/NN apply/VB to/TO gay/JJ marriage/NN under/IN equal/JJ protection/NN due/JJ to/TO lack/NN of/IN documentation/NN ./. </C>
</S>
<S>
<C>Asks/VBZ why/WRB S1/NNP does/AUX not/RB answer/VB question/NN ./. </C>
</S>
<S>
<C>References/NNS other/JJ limitations/NNS to/TO marriage/NN that/WDT exist/VBP ./. </C>
</S>
<S>
<C>S1/NNP Sarcastically/NNP calls/VBZ self/NN stupid/JJ for/IN believing/VBG constitutional/JJ mention/NN of/IN equal/JJ protection/NN applied/VBN to/TO gays/NNS ./. </C>
</S>
<S>
<C>S2/NNP Questions/VBZ existence/NN of/IN documentation/NN that/WDT links/VBZ equal/JJ protection/NN to/TO gays/NNS but/CC not/RB other/JJ groups/NNS ./. </C>
</S>
<S>
<C>S1/NNP Accuses/VBZ S2/NNP of/IN bigotry/NN and/CC links/VBZ denial/NN of/IN gay/JJ marriage/NN to/TO being/AUXG a/DT denial/NN of/IN ``/`` life/NN ,/, liberty/NN and/CC the/DT pursuit/NN of/IN happiness/NN ''/'' ./. </C>
</S>
<S>
<C>S2/NNP Accuses/VBZ S1/NNP of/IN ignoring/VBG S2/NNP 's/POS points/NNS and/CC instead/RB insulting/JJ him/PRP ./. </C>
</S>
<S>
<C>Accuses/VBZ S1/NNP of/IN borderline/RB breaking/VBG the/DT rules/NNS of/IN the/DT forum/NN ./. </C>
</S>
<S>
<C>S1/VBG States/NNPS that/IN the/DT lack/NN of/IN equal/JJ protection/NN is/AUX obvious/JJ to/TO a/DT blind-man/NN ,/, </C>
<C>S2/NNP is/AUX deliberately/RB denying/VBG facts/NNS ./. </C>
</S>
<S>
<C>S2/VB Questions/NNS why/WRB it/PRP is/AUX wrong/JJ to/TO deny/VB gay/JJ marriage/NN but/CC not/RB to/TO deny/VB incestuous/JJ marriage/NN ./. </C>
</S>
</P>
</T>
