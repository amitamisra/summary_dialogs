<T>
<P>
</P>
<P>
<S>
<C>S1/NNP asks/VBZ S2/NNP if/IN he/PRP knows/VBZ that/IN the/DT gay/JJ marriage/NN debate/NN is/AUX not/RB synonymous/JJ with/IN someone/NN condoning/VBG violence/NN against/IN gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP agrees/VBZ that/IN gays/NNS should/MD not/RB experience/VB violence/NN and/CC name-calling/NN ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ that/IN on/IN Archie/NNP 's/POS post/NN that/DT Archie/NNP regularly/RB denigrates/VBZ gays/NNS with/IN a/DT variety/NN of/IN slurs/NNS ./. </C>
</S>
<S>
<C>S1/NNP finds/VBZ it/PRP funny/JJ that/IN the/DT whole/JJ violence/NN thing/NN gets/VBZ used/VBN as/IN a/DT means-to-an-end/JJ tool/NN by/IN those/DT who/WP argue/VBP against/IN religious/JJ people/NNS on/IN this/DT ./. </C>
</S>
<S>
<C>People/NNS should/MD not/RB have/AUX to/TO stop/VB standing/VBG up/RP for/IN their/PRP$ principles/NNS just/RB because/IN of/IN other/JJ people/NNS ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ telling/VBG people/NNS that/IN gay/JJ marriage/NN will/MD makes/VBZ their/PRP$ kids/NNS have/AUX to/TO be/AUX taught/VBN about/IN gay/JJ marriage/NN is/AUX a/DT threat/NN ,/, as/RB well/RB as/IN telling/VBG people/NNS that/IN gay/JJ marriage/NN will/MD require/VB churches/NNS to/TO have/AUX to/TO perform/VB gay/JJ marriages/NNS ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ S1/NNP is/AUX ignoring/VBG him/PRP </C>
<C>but/CC S1/NNP says/VBZ he/PRP is/AUX not/RB rather/RB S2/NNP 's/POS tone/NN has/AUX changed/VBN ./. </C>
</S>
<S>
<C>S1/NNP says/VBZ name/NN calling/NN and/CC violence/NN were/AUX the/DT things/NNS Jason/NNP conflated/VBD with/IN opposing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP asks/VBZ S1/NNP to/TO quote/VB exactly/RB what/WP he/PRP said/VBD </C>
<C>because/IN he/PRP thinks/VBZ Jason/NNP did/AUX no/DT such/JJ thing/NN ./. </C>
</S>
</P>
</T>
