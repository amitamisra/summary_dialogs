<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG gay/JJ rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX in/IN support/NN of/IN gay/JJ marriage/NN </C>
<C>while/IN S2/NNP supports/VBZ it/PRP ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ gay/JJ rights/NNS would/MD impose/VB gay/JJ families/NNS onto/IN straight/JJ members/NNS of/IN society/NN </C>
<C>while/IN S1/NNP believes/VBZ S2/NNP 's/POS opinions/NNS are/AUX comparable/JJ to/TO those/DT of/IN Taliban/NNP terrorists/NNS ./. </C>
</S>
<S>
<C>S2/NNP is/AUX in/IN support/NN of/IN comments/NNS made/VBN by/IN a/DT previous/JJ participant/NN ,/, contending/VBG he/PRP did/AUX not/RB make/VB remarks/NNS based/VBN on/IN mandatory/JJ therapy/NN for/IN gays/NNS or/CC anything/NN of/IN the/DT like/JJ ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ the/DT previous/JJ comments/NNS made/VBN center/NN on/IN religion/NN and/CC that/DT civil/JJ law/NN and/CC religion/NN are/AUX not/RB synonymous/JJ ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ his/PRP$ religious/JJ beliefs/NNS are/AUX violated/VBN by/IN just/RB the/DT consideration/NN of/IN allowing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ marriage/NN is/AUX a/DT contract/NN between/IN a/DT woman/NN and/CC a/DT man/NN ,/, not/RB same/JJ sex/NN ./. </C>
</S>
</P>
</T>
