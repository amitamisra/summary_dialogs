(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ identifies) (NP (DT the) (JJ important) (JJ spiritual) (NN situation) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (PP (IN in))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ inappropriate) (S (VP (TO to) (VP (VB make) (NP (NP (NNS jokes)) (PP (IN about) (NP (JJ homosexual) (NNP lifestyles)))))))) (, ,) (SBAR (IN that) (S (NP (DT no) (NN one)) (VP (VBZ approves) (PP (IN of) (NP (NP (DT the) (JJ deviant) (NNS behaviors)) (VP (VBN practiced) (PP (IN by) (NP (NNS homosexuals)))))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (RB not) (VP (AUX be) (ADJP (JJ funny)) (SBAR (IN if) (S (NP (NN homosexuality)) (VP (AUX was) (VP (VBN discovered) (PP (IN in) (NP (PRP$ your) (JJ own) (NN community))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (IN that) (S (NP (JJ sexual) (NNS behaviors)) (SBAR (IN because) (S (NP (PRP we)) (VP (AUX do) (RB not) (VP (VB identify) (NP (NP (NNS non-homosexuals)) (PP (IN by) (NP (DT the) (JJ sexual) (NNS behaviors)))))))) (NP (PRP they)) (VP (VBP engage) (PP (IN in)) (, ,) (SBAR (S (NP (PRP we)) (VP (MD should) (RB not) (VP (AUX do) (ADVP (RB so)) (PP (IN for) (NP (JJ homosexual) (NNS individuals))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (NP (DT a) (NN person) (POS 's)) (JJ private) (NNS behaviors)) (VP (AUX are) (SBAR (S (NP (NP (DT no) (NN one)) (ADVP (RB else))) (VP (AUX 's) (NP (NN business)))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT a) (JJ sexual) (NNS behaviors)) (VP (AUX do) (RB not) (VP (VB define) (SBAR (WHNP (WP who)) (S (NP (NN someone)) (VP (AUX is) (PP (IN as) (NP (DT a) (NN person)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ defends) (NP (JJ homosexual) (NNS behaviors)) (PP (IN by) (S (VP (VBG referencing) (NP (NP (DT the) (JJ deviant) (NNS behaviors)) (VP (VBN performed) (PP (IN by) (NP (NNS heterosexuals)))))))) (, ,) (S (VP (VBG suggesting) (SBAR (IN that) (S (NP (JJ deviant) (JJ sexual) (NNS acts)) (VP (AUX are) (RB not) (NP (NP (DT the) (JJ sole) (NN province)) (PP (IN of) (NP (NNS homosexuals)))))))))) (. .)))

