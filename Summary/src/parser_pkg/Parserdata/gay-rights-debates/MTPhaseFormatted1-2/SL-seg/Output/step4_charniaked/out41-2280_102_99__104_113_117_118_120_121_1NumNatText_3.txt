(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (PP (IN in) (NP (NP (NN spite)) (PP (IN of) (NP (NP (DT the) (NN vitriol)) (VP (VBN employed)))))) (, ,) (S (VP (VBG comparing) (NP (ADJP (JJ interracial) (CC and) (JJ gay)) (NN marriage)))) (VP (AUX is) (NP (DT a) (JJ useful) (NN analogy)))))) (. .)))

(S1 (S (NP (NP (NNS Opponents)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (VP (VBP use) (NP (JJ similar) (NNS arguments)) (PP (IN as) (NP (NP (DT those)) (VP (VBN employed) (PP (IN by) (NP (NP (NNS opponents)) (PP (IN of) (NP (JJ interracial) (NN marriage))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT this) (NN comparison)) (VP (AUX is) (ADJP (JJ irrelevant))))) (, ,) (SBAR (S (NP (PRP he)) (VP (AUX has) (RB not) (VP (VBN mentioned) (NP (NP (NN religion)) (CC or) (NP (JJ interracial) (NN marriage))) (PP (IN in) (NP (NP (PRP$ his) (NN opposition)) (PP (TO to) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NN fact))) (, ,) (NP (NP (NN none)) (PP (IN of) (NP (NP (DT the) (NNS tropes)) (VP (VBN associated) (PP (IN with) (NP (JJ interracial) (NN marriage))))))) (ADVP (RB actually)) (VP (VBP apply)) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX has) (ADVP (RB instead)) (VP (VBN argued) (SBAR (IN that) (S (NP (VBG redefining) (NN marriage)) (VP (MD would) (VP (VB require) (S (VP (VBG redefining) (NP (JJ several) (JJ other) (JJ cultural) (NNS practices)) (ADVP (RB as) (RB well)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX has) (ADVP (RB also)) (VP (VBN proposed) (SBAR (IN that) (S (NP (NP (DT those)) (VP (VBG seeking) (S (VP (TO to) (VP (VB enjoy) (NP (JJ same) (NN sex) (NN marriage))))))) (VP (AUX are) (VP (VBG doing) (ADVP (RB so)) (PP (IN out) (PP (IN of) (NP (NP (DT some) (NN form)) (PP (IN of) (NP (NN self-shame)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NNS words)) (CC and) (NP (VBN associated) (JJ cultural) (NNS practices))) (VP (AUX are) (VP (VBD redefined) (NP (PDT all) (DT the) (NN time)))))) (, ,) (S (VP (VBG providing) (NP (NP (DT the) (NN example)) (PP (IN of) (NP (`` ``) (NN war) ('' '') (S (VP (TO to) (VP (VB illustrate) (NP (PRP$ his) (NN point))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (NN comparison)) (PP (TO to) (NP (DT these) (JJ major) (NNS re-definitions))))) (, ,) (S (VP (VBG redefining) (SBAR (WHNP (WP what)) (S (VP (VBZ constitutes) (NP (DT a) (JJ valid) (NN marriage))))))) (VP (VBZ seems) (ADJP (JJ minor)) (PP (IN in) (NP (NN comparison)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ brings) (PRT (RP up)) (NP (NP (DT a) (NN counterpoint)) (PP (TO to) (NP (NP (NP (NNP S1) (POS 's)) (NN argument)) (PP (IN about) (NP (NN war))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ clarifies) (NP (NP (NP (NNP S2) (POS 's)) (NN argument)) (PP (IN about) (NP (NN war)))) (S (VP (TO to) (VP (VB illustrate) (SBAR (WHADVP (WRB how)) (S (NP (NP (NNP S2) (POS 's)) (NN example)) (VP (AUX is) (PP (IN in) (NP (NP (NN accordance)) (PP (IN with) (NP (NP (NNP S1) (POS 's)) (JJ original) (NN statement)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ stands) (PP (IN by) (NP (PRP$ his) (JJ previous) (NN counter-example)))) (. .)))

