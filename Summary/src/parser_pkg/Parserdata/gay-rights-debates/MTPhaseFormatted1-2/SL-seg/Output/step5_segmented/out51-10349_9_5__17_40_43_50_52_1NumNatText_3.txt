<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX focused/VBN on/IN the/DT definition/NN of/IN marriage/NN and/CC the/DT cultural/JJ definition/NN ,/, both/DT limiting/VBG marriage/NN to/TO heterosexual/JJ couples/NNS ./. </C>
</S>
<S>
<C>The/DT cultural/JJ definition/NN includes/VBZ reproduction/NN ,/, </C>
<C>he/PRP uses/VBZ the/DT term/NN mating/NN mating/NN pairs/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN this/DT definition/NN automatically/RB excludes/VBZ homosexuals/NNS </C>
<C>because/IN they/PRP cannot/VBP ,/, without/IN outside/JJ help/NN ,/, create/VB a/DT family/NN ./. </C>
</S>
<S>
<C>He/PRP discusses/VBZ other/JJ marriage/NN outside/NN of/IN the/DT norm-a/JJ young/JJ woman/NN and/CC older/JJR man-/NNS </C>
<C>as/IN being/AUXG less/RBR meaningful/JJ than/IN those/DT that/WDT fulfill/VBP the/DT cultural/JJ definition/NN ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ it/PRP as/IN the/DT job/NN of/IN elected/VBN officials/NNS and/CC the/DT citizens/NNS to/TO change/VB the/DT legal/JJ definition/NN of/IN marriage/NN </C>
<C>if/IN that/DT is/AUX what/WP the/DT majority/NN want/VBP ./. </C>
</S>
<S>
<C>S2/NNP disagrees/VBZ with/IN and/CC questions/VBZ the/DT cultural/JJ definition/NN given/VBN by/IN S1/NNP ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN the/DT main/JJ factor/NN in/IN marriage/NN is/AUX not/RB reproduction/NN but/CC love/NN and/CC commitment/NN between/IN two/CD individuals/NNS ./. </C>
</S>
<S>
<C>He/PRP challenges/VBZ the/DT cultural/JJ definition/NN with/IN the/DT example/NN of/IN a/DT 65/CD and/CC 68/CD year/NN old/JJ getting/VBG married/VBN ./. </C>
</S>
<S>
<C>He/PRP finds/VBZ it/PRP unfair/JJ that/IN a/DT group/NN of/IN people/NNS are/AUX being/AUXG excluded/VBN from/IN an/DT institution/NN on/IN which/WDT they/PRP pose/VBP no/DT threat/NN to/TO those/DT who/WP are/AUX enjoying/VBG the/DT status/NN quo/NN ./. </C>
</S>
</P>
</T>
