(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT This)) (VP (VBZ seems) (S (VP (TO to) (VP (AUX be) (NP (NP (DT an) (NN argument)) (PP (IN about) (NP (NP (JJ gay) (NNS rights)) (CC and) (S (VP (VBG generalizing) (NP (NP (DT the) (JJ political) (NNS beliefs)) (PP (IN of) (NP (NNS people))))))))))))) (. .)))

(S1 (S (PP (VBG According) (PP (TO to) (NP (NNP S1)))) (, ,) (NP (NNP S2)) (VP (VP (VBZ talks) (PP (IN about) (NP (NNP Paul) (NNP Cameron)))) (CC and) (VP (VBZ makes) (S (NP (PRP it)) (VP (VB seem) (SBAR (IN as) (IN if) (S (NP (PRP he)) (VP (AUX is) (NP (NP (DT the) (NN voice)) (PP (IN of) (NP (DT the) (NNPS Republicans))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NP (DT the) (NN case)) (CC and) (NP (NP (CD one) (NN person)) (SBAR (WHNP (WP who)) (S (NP (NP (NNS sides)) (PP (IN with) (NP (DT a) (JJ specific) (NN party)))) (VP (AUX does) (RB not) (VP (VB represent) (NP (DT the) (NN party)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ uses) (NP (NNP John) (NNP Lennon)) (PP (IN as) (NP (NP (DT an) (NN example)) (PP (IN of) (S (NP (DT this)) (VP (RB not) (AUXG being) (NP (NP (DT a) (JJ proper) (NN way)) (SBAR (S (VP (TO to) (VP (VB generalize) (NP (NP (JJ political) (NNS views)) (PP (IN of) (NP (NP (NNS parties)) (, ,) (PP (IN in) (NP (PRP$ his) (NN case)))))) (NP (DT the) (JJ Democratic) (NN party))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (S (VP (VBG making) (NP (NP (DT an) (NN assertion)) (PP (VBG including) (NP (DT the) (NNPS Republicans))))))) (S (VP (VBG creating) (NP (DT another) (NN Holocaust)))) (, ,) (PP (RB because) (IN of) (NP (DT the) (NN generalization) (SBAR (IN that) (S (NP (NNP S2)) (VP (VBD made))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (NNP Paul) (NNP Cameron) (POS 's)) (NNS views)) (VP (AUX are) (VP (VP (VBN cited)) (CC and) (VP (VBN used) (PP (IN in) (NP (NP (JJ many) (NNS cases)) (PP (IN against) (NP (JJ gay) (NNS rights))))) (SBAR (IN because) (S (NP (JJS most) (NNS people)) (VP (AUX do) (VP (VB share) (NP (DT the) (JJ same) (NNS views)) (, ,) (SBAR (WHNP (WDT which)) (S (NP (NNP S1)) (VP (VBZ thinks) (ADVP (RB otherwise))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NP (JJS most) (NNS attacks)) (PP (IN on) (NP (JJ gay) (NNS rights)))) (VP (AUX have) (VP (VBN cited) (NP (NP (NNP Paul) (NNP Cameron)) (, ,) (ADVP (RB even))) (PP (IN within) (NP (DT the) (NNP US) (NNP Senate))) (, ,) (S (VP (VBG showing) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (NP (NP (DT a) (JJ strong) (NN opinion)) (PP (IN in) (NP (DT the) (NNP Republican) (NN party))))))) (, ,) (CC or) (ADVP (IN at) (JJS least)) (S (NP (PRP it)) (VP (VBZ seems) (ADJP (RB so)))))))))))) (. .)))

