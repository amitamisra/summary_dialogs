<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ the/DT world/NN changed/VBN with/IN the/DT legislation/NN in/IN Massachusetts/NNP to/TO allow/VB gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP assumes/VBZ that/IN 
<M>S2/NNP ,/, </M>
having/AUXG stated/VBN several/JJ times/NNS he/PRP is/AUX against/IN gay/JJ marriage/NN ,/, </C>
<C>S2/NNP will/MD hope/VB the/DT legislation/NN is/AUX overturned/VBN </C>
<C>but/CC may/MD be/AUX out/IN of/IN luck/NN ./. </C>
</S>
<S>
<C>He/PRP questions/VBZ </C>
<C>if/IN S2/NNP 's/POS opinion/NN has/AUX changed/VBN now/RB that/IN gay/JJ marriage/NN is/AUX in/IN place/NN in/IN Massachusetts/NNP </C>
<C>and/CC if/IN he/PRP wants/VBZ it/PRP overturned/VBD ./. </C>
</S>
<S>
<C>S2/NNP resents/VBZ the/DT assumption/NN of/IN S1/NNP and/CC says/VBZ that/IN legislation/NN to/TO overturn/VB the/DT Massachusetts/NNP decision/NN would/MD be/AUX an/DT over/JJ intrusion/NN by/IN the/DT government/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN he/PRP would/MD not/RB want/VB an/DT amendment/NN to/TO ban/VB gay/JJ marriage/NN </C>
<C>and/CC that/IN has/AUX always/RB been/AUX his/PRP$ opinion/NN </C>
<C>because/IN he/PRP does/AUX not/RB agree/VB with/IN over/IN intrusion/NN by/IN the/DT government/NN ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ this/DT as/IN a/DT true/JJ conservative/JJ opinion/NN to/TO take/VB ./. </C>
</S>
</P>
</T>
