<s> <PARAGRAPH> </s>
<s>  S1: Religion is a good tool to guide one's own actions, but not for telling others how to live. </s>
<s> People have been prejudiced against homosexuals for a long time. </s>
<s> Moral or religious prejudice is still prejudice. </s>
<s> A person can claim a moral or religious stance without having one. </s>
<s> You cannot justify an anti-gay agenda on morals or religion. </s>
<s> An anti-gay stance on the grounds of religious belief is prejudice. </s>
<s> Prejudice based on religious belief implies that a religious person's acts of prejudice could have a negative impact on homosexuals, because they believe that being gay is morally reprehensible. </s>
<s> S2: Prejudice is a fact of human existence. </s>
<s> Christianity is a moral system. </s>
<s> When people claim religion in morals, they abandon that moral system. </s>
<s> It is religious belief - not prejudice - that makes one take an anti-gay stance. </s>
<s> The Bible says that homosexuality is wrong, so if you believe in the Bible as your moral canon, then you believe that homosexuality is wrong as well. </s>
<s>  </s>
