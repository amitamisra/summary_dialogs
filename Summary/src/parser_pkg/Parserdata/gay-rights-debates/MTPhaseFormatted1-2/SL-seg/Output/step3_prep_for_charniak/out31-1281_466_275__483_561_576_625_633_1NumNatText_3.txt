<s> <PARAGRAPH> </s>
<s>  S1 dominates the debate. </s>
<s> He is against using religion, specifically Biblical references in the conversation around gay marriage. </s>
<s> His first comment seems to be arguing against a previous mention of increased earthquakes and wars. </s>
<s> He also argues for the inclusion and support of minority issues despite the fact that we live in a majority rule state. </s>
<s> He argues that first, most people fall into some sort of minority classification and second the minorities are still large in number. </s>
<s> There is a change in the conversation about half way through to genetics in which compares sexuality to a predisposition for alcoholism. </s>
<s> In the end he feels marriage must be consensual and that any parties may be free to leave, whether it is traditional, homosexual or polygamy. </s>
<s> S2 mostly argues the religious points, for example citing the increase in earthquakes outside of Israel in recent years though he mentions another name of who began the conversation around religion. </s>
<s> He questions why the debate remains around homosexuals and whether polygamists fall into similar categories in the genetic argument. </s>
<s>  </s>
