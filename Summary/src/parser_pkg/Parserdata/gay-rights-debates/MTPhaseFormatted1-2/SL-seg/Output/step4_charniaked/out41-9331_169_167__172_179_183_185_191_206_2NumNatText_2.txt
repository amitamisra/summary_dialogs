(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (PP (IN in) (NP (NP (JJ unfriendly) (NN argument)) (PP (IN about) (NP (JJ gay) (NN marriage)))))) (. .)))

(S1 (S (NP (DT The) (NN argument)) (ADVP (RB originally)) (VP (VBZ starts) (PP (IN in) (NP (NP (DT a) (NN tangent)) (PP (IN of) (NP (NN Abortion)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG trolling) (NP (NP (DT the) (NN thread) (CC and) (NN briefly) (NN comment)) (PP (IN on) (NP (NP (NN abortion)) (VP (VBG playing) (NP (NP (DT the) (JJ devil) (NN advocate)) (PP (TO to) (NP (NP (NNS shows)) (SBAR (S (NP (NP (DT the) (NN misuse)) (PP (IN of) (NP (NN logic)))) (VP (AUX is) (NP (NP (NP (NNP S1) (POS 's)) (NN thinking)) (PP (IN for) (NP (NP (NNS issues)) (PP (IN in) (NP (JJ gay) (NN marriage))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ continues) (S (VP (TO to) (VP (VB antagonize) (NP (NNP S2)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ continues) (S (VP (TO to) (VP (VB discuss) (NP (NP (DT the) (NN necessity)) (PP (IN of) (NP (NN drawing) (NNS boundaries)))) (PP (IN in) (NP (NP (NN reason)) (CC and) (NP (NP (DT the) (NN importance)) (VP (VBG staying) (PP (IN on) (NP (NN track))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (MD should) (VP (VB use) (NP (NP (JJ familiar) (NN comparison)) (PP (IN of) (NP (NP (NNS attacks)) (PP (IN on) (NP (NP (JJ inter-racial) (NN marriage)) (SBAR (WHADVP (WRB where)) (S (SBAR (IN if) (S (NP (PRP you)) (VP (AUX are) (PP (IN against) (NP (JJ interracial) (NN marriage)))))) (, ,) (NP (PRP you)) (VP (AUX are) (PP (IN for) (NP (NN slavery))) (, ,) (S (VP (TO to) (VP (VB show) (NP (NP (PRP$ his) (NN support)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (, ,) (S (ADVP (RB just)) (VP (TO to) (VP (VB show) (SBAR (S (NP (NP (NNP S1that) (NN type)) (PP (IN of) (NP (NN discourse)))) (VP (AUX is) (RB not) (ADJP (RB logically) (JJ accurate)))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ brings) (NP (NP (NP (DT the) (`` ``) (NN 9th) ('' '')) (PP (IN from) (NP (NNP Maine)))) (CC and) (`` ``) (NP (VB Prop) (CD 8)) ('' '') (PP (IN from) (NP (NNP California)))) (PP (IN as) (NP (NP (NN example)) (PP (IN of) (NP (NP (NNS states)) (SBAR (WHNP (WDT that)) (S (VP (AUX have) (VP (VBN showed) (SBAR (SBAR (IN that) (S (NP (PRP$ their) (NN majority)) (VP (AUX was) (PP (IN against) (NP (JJ gay) (NN marriage)))))) (, ,) (CC but) (SBAR (RB yet) (S (NP (NP (NNS people)) (PP (IN in) (NP (DT those) (NNS states)))) (VP (AUX are) (ADVP (RB still)) (VP (VBG trying) (S (VP (TO to) (VP (VB get) (S (NP (PRP them)) (VP (VBN overturned)))))))))))))))))))) (. .)))

(S1 (S (NP (JJ S2) (NNS compliments)) (VP (VBP S1) (PP (IN for) (NP (JJ clever) (NN banter)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ ends) (NP (DT the) (NN discussion)) (PP (IN by) (S (VP (VBG accusing) (NP (NNP S1)) (PP (IN of) (S (VP (VBG repeating) (NP (NP (PRP$ his) (NNS points)) (, ,) (SBAR (WHNP (WDT which)) (S (VP (VBP pertain) (PP (TO to) (NP (CD two) (JJ separate) (NNS issues)))))))))))))) (. .)))

