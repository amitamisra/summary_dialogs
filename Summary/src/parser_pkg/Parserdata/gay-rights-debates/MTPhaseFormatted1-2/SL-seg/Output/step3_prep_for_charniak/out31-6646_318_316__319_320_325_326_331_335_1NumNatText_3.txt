<s> <PARAGRAPH> </s>
<s>  Two people are discussing about redefining the meaning of marriage. </s>
<s> S1 thinks that it is unnecessary to redefine marriage to include gays. </s>
<s> He states that all of the rights and benefits that married couples have are already granted to gays without the need for redefining. </s>
<s> Redefining only serves to hurt the many people who see marriage as a sacred bond between man and woman. </s>
<s> S2 thinks that it is necessary to redefine marriage. </s>
<s> If not, the rights and benefits granted to gays will not be guaranteed. </s>
<s> Also, he believes that offending someone's sensibility is not real, measurable damage, so there is not reason not to redefine marriage, not doing so only appeases faith justified hate. </s>
<s>  </s>
