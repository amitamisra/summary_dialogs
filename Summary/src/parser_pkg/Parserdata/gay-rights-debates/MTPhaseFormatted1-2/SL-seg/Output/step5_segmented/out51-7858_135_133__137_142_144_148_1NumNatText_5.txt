<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX debating/VBG gay/JJ marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX pro/FW gay/JJ marriage/NN </C>
<C>while/IN S2/NNP appears/VBZ against/IN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ the/DT constitution/NN can/MD evolve/VB with/IN changing/VBG times/NNS </C>
<C>because/IN it/PRP is/AUX a/DT man-made/JJ construct/VB </C>
<C>while/IN S2/NNP cites/VBZ the/DT fact/NN 26/CD states/NNS still/RB consider/VBP the/DT sanctity/NN of/IN marriage/NN to/TO be/AUX one/CD man/NN and/CC one/CD woman/NN ./. </C>
</S>
<S>
<C>Also/RB advising/VBG a/DT key/JJ point/NN to/TO the/DT nation/NN is/AUX the/DT basis/NN of/IN laws/NNS being/AUXG voted/VBN upon/IN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB feel/VB S1/NNP 's/POS statements/NNS are/AUX correct/JJ regarding/VBG </C>
<C>whether/IN or/CC not/RB marriage/NN is/AUX a/DT right/NN ./. </C>
</S>
<S>
<C>S1/NNP sites/NNS Loving/NNP v/NNP ./. </C>
</S>
<S>
<C>Virginia/NNP in/IN argument/NN marriage/NN is/AUX not/RB a/DT human/JJ construct/VB ./. </C>
</S>
<S>
<C>S1/NNP also/RB advises/VBZ marriage/NN is/AUX a/DT fundamental/JJ right/NN and/CC should/MD be/AUX a/DT fundamental/JJ right/NN for/IN all/DT not/RB to/TO be/AUX determined/VBN by/IN each/DT state/NN individually/RB ./. </C>
</S>
</P>
</T>
