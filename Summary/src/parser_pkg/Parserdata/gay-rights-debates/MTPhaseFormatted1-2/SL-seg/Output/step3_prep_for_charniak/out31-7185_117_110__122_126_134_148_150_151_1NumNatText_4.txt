<s> <PARAGRAPH> </s>
<s>  S1 cites court decisions upholding equal protections for people regardless of sexual orientation. </s>
<s> He accuses S2 of claiming the courts are engaging in illegitimate judicial activism whenever they support equal rights for homosexuals. </s>
<s> He argues that it is unconstitutional to prevent homosexuals from marrying because that would be restricting their pursuit of life, liberty, and happiness. </s>
<s> He says that heterosexuals take the right to marry the person you love for granted and should extend the same right to people in homosexual relationships. </s>
<s> S2 reasserts his claim that it is illegitimate to misinterpret the original constitutional meaning of equal protection. </s>
<s> He claims that true equality would require relaxing the marriage restrictions based on age and species, for example, which he suggests would be inappropriate. </s>
<s> He claims that homosexuals are not being denied the right to marry because they are gay, but rather because the genders of the two people involved to not fit the criteria for marriage. </s>
<s> He claims this demonstrates equality because two straight males or females would not be legally allowed to marry either. </s>
<s>  </s>
