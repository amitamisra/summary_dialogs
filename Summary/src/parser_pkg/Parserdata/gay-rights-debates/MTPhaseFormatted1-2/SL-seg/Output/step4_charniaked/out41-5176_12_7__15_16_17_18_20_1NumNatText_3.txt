(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (AUX is) (NP (NP (DT a) (NN heterosexual)) (SBAR (WHNP (WP who)) (S (VP (VBZ believes) (SBAR (SBAR (IN that) (S (PP (IN in) (NP (PRP$ our) (JJ democratic) (NN society))) (, ,) (NP (DT the) (NN majority)) (VP (VBZ rules)))) (, ,) (CC and) (SBAR (IN that) (S (NP (DT this) (NN majority)) (VP (AUX has) (ADVP (RB so) (RB far)) (ADVP (RB consistently)) (VP (VBN ruled) (ADVP (RB again)) (NP (JJ gay) (NNS rights)))))))))))) (. .)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNS courts)) (VP (MD should) (RB not) (VP (VB overturn) (NP (NP (NNS decisions)) (VP (VBN based) (PP (IN off) (NP (NP (DT the) (NN minority)) (VP (VP (VBG complaining)) (CC and) (VP (MD should) (VP (VB abide) (PP (IN by) (NP (NP (DT the) (NN majority)) (, ,) (SBAR (WHADVP (WRB how)) (S (NP (PRP$ our) (NN constitution) (CC and) (NN government)) (VP (AUX was) (VP (VBN designed))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NN majority)) (VP (AUX is) (VP (VP (VBG oppressing) (NP (DT the) (JJ LGBT) (NN community))) (CC and) (VP (AUXG being) (VP (VBN treated) (PP (IN as) (NP (JJ second-class) (NNS citizens)))))))))) (. .)))

(S1 (S (NP (DT The) (NN speaker)) (VP (VBZ believes) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (NN law)) (VP (VBZ effects) (NP (NP (DT an) (NNS individuals) (POS ')) (NN life)) (PP (IN in) (NP (DT an) (JJ unjust) (NN way)))))) (, ,) (ADVP (RB then)) (NP (PRP it)) (VP (AUX is) (NP (NP (NNS courts) (NN responsibility)) (SBAR (S (VP (TO to) (VP (VB overrule) (NP (PRP it)) (PP (IN for) (NP (NP (DT the) (NN individual) (POS 's)) (NN sake)))))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VBZ believes) (SBAR (SBAR (S (NP (NN society)) (VP (AUX is) (VP (VBG oppressing) (NP (NNS people)))))) (, ,) (CC and) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (NN law)) (ADVP (RB adversely)) (VP (VBZ effects) (NP (QP (RB even) (CD one)) (NN person))))) (, ,) (NP (PRP it)) (VP (AUX is) (ADJP (JJ unjust))))))) (. .)))

