<T>
<P>
</P>
<P>
<S>
<C>S1/NNP says/VBZ that/IN someone/NN ,/, ``/`` her/PRP ''/'' ,/, is/AUX trying/VBG to/TO put/VB their/PRP$ religious/JJ beliefs/NNS into/IN law/NN that/WDT goes/VBZ against/IN the/DT religious/JJ beliefs/NNS of/IN S1/NNP ./. </C>
</S>
<S>
<C>The/DT law/NN would/MD bar/VB the/DT family/NN of/IN S1/NNP from/IN legal/JJ protections/NNS </C>
<C>but/CC would/MD not/RB affect/VB her/PRP ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN she/PRP is/AUX not/RB protecting/VBG marriage/NN </C>
<C>but/CC preserving/VBG her/PRP$ heterosexual/JJ privilege/NN ./. </C>
</S>
<S>
<C>Her/PRP$ religious/JJ belief/NN is/AUX that/IN marriage/NN should/MD be/AUX between/IN a/DT man/NN and/CC a/DT woman/NN </C>
<C>but/CC S1/NNP believes/VBZ that/DT marriage/NN should/MD be/AUX between/IN two/CD people/NNS in/IN a/DT loving/JJ relationship/NN regardless/RB of/IN sex/NN ./. </C>
</S>
<S>
<C>If/IN she/PRP put/VBP her/PRP$ religious/JJ belief/NN into/IN law/NN </C>
<C>it/PRP would/MD hurt/VB S1/NNP </C>
<C>but/CC if/IN the/DT religious/JJ views/NNS of/IN S1/NNP are/AUX put/VBN into/IN law/NN </C>
<C>she/PRP would/MD still/RB be/AUX able/JJ to/TO choose/VB who/WP she/PRP wanted/VBD </C>
<C>and/CC S1/NNP would/MD be/AUX able/JJ to/TO choose/VB whomever/NN ./. </C>
</S>
<S>
<C>She/PRP is/AUX trying/VBG to/TO keep/VB gay/JJ people/NNS out/IN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP understands/VBZ the/DT perspective/NN of/IN S1/NNP </C>
<C>but/CC does/AUX not/RB think/VB it/PRP is/AUX a/DT valid/JJ argument/NN ./. </C>
</S>
<S>
<C>S2/NNP holds/VBZ that/IN either/DT if/IN either/DT religious/JJ belief/NN were/AUX put/VBN into/IN law/NN that/IN it/PRP would/MD infringe/VB on/IN the/DT other/JJ 's/POS beliefs/NNS ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN S1/NNP has/AUX not/RB shown/VBN how/WRB the/DT religious/JJ beliefs/NNS would/MD be/AUX taken/VBN away/RB from/IN such/PDT a/DT law/NN ./. </C>
</S>
<S>
<C>It/PRP can/MD be/AUX argued/VBN in/IN other/JJ ways/NNS </C>
<C>but/CC the/DT idea/NN of/IN not/RB giving/VBG gays/NNS marriage/NN benefits/NNS is/AUX imposing/VBG on/IN religious/JJ freedom/NN is/AUX an/DT empty/JJ argument/NN ./. </C>
</S>
<S>
<C>S2/JJ counters/NNS S1/VBN </C>
<C>by/IN saying/VBG that/IN it/PRP is/AUX impossible/JJ to/TO know/VB </C>
<C>whether/IN or/CC not/RB she/PRP is/AUX trying/VBG to/TO turn/VB religious/JJ belief/NN into/IN law/NN ./. </C>
</S>
<S>
<C>It/PRP is/AUX impossible/JJ to/TO know/VB or/CC to/TO police/VB someone/NN 's/POS real/JJ motives/NNS ./. </C>
</S>
</P>
</T>
