<T>
<P>
</P>
<P>
<S>
<C>S1/NNP cites/VBZ court/NN decisions/NNS upholding/VBG equal/JJ protections/NNS for/IN people/NNS regardless/RB of/IN sexual/JJ orientation/NN ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ S2/NNP of/IN claiming/VBG the/DT courts/NNS are/AUX engaging/VBG in/IN illegitimate/JJ judicial/JJ activism/NN </C>
<C>whenever/WRB they/PRP support/VBP equal/JJ rights/NNS for/IN homosexuals/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN it/PRP is/AUX unconstitutional/JJ to/TO prevent/VB homosexuals/NNS from/IN marrying/VBG </C>
<C>because/IN that/DT would/MD be/AUX restricting/VBG their/PRP$ pursuit/NN of/IN life/NN ,/, liberty/NN ,/, and/CC happiness/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN heterosexuals/NNS take/VB the/DT right/NN to/TO marry/VB the/DT person/NN you/PRP love/VBP for/IN granted/VBN and/CC should/MD extend/VB the/DT same/JJ right/NN to/TO people/NNS in/IN homosexual/JJ relationships/NNS ./. </C>
</S>
<S>
<C>S2/NNP reasserts/VBZ his/PRP$ claim/NN that/IN it/PRP is/AUX illegitimate/JJ to/TO misinterpret/VB the/DT original/JJ constitutional/JJ meaning/NN of/IN equal/JJ protection/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN true/JJ equality/NN would/MD require/VB relaxing/VBG the/DT marriage/NN restrictions/NNS based/VBN on/IN age/NN and/CC species/NNS ,/, for/IN example/NN ,/, </C>
<C>which/WDT he/PRP suggests/VBZ would/MD be/AUX inappropriate/JJ ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN homosexuals/NNS are/AUX not/RB being/AUXG denied/VBN the/DT right/NN to/TO marry/VB </C>
<C>because/IN they/PRP are/AUX gay/JJ ,/, </C>
<C>but/CC rather/RB because/IN the/DT genders/NNS of/IN the/DT two/CD people/NNS involved/VBN to/TO not/RB fit/VB the/DT criteria/NNS for/IN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ this/DT demonstrates/VBZ equality/NN </C>
<C>because/IN two/CD straight/JJ males/NNS or/CC females/NNS would/MD not/RB be/AUX legally/RB allowed/VBN to/TO marry/VB either/RB ./. </C>
</S>
</P>
</T>
