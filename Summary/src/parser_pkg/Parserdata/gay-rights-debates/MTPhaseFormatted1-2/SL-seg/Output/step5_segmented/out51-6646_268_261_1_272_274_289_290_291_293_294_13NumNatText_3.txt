<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX arguing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN resorting/VBG to/TO name/VB calling/VBG indicates/VBZ the/DT inability/NN to/TO resort/VB to/TO sound/NN arguments/NNS ./. </C>
</S>
<S>
<C>He/PRP then/RB states/VBZ that/IN the/DT word/NN bigotry/NN is/AUX an/DT ugly/JJ word/NN ,/, and/CC its/PRP$ ignorant/NN to/TO automatically/RB write/VB off/RP disagreements/NNS as/IN ``/`` bigotry/NN ''/'' ./. </C>
</S>
<S>
<C>S2/NNP rebuts/VBZ by/IN saying/VBG that/IN he/PRP 's/AUX calling/VBG it/PRP bigotry/NN </C>
<C>because/IN that/DT 's/AUX what/WP it/PRP is/AUX ./. </C>
</S>
<S>
<C>He/PRP then/RB states/VBZ that/IN if/IN it/PRP were/AUX another/DT minority/NN being/AUXG challenged/VBN then/RB </C>
<C>it/PRP would/MD clearly/RB be/AUX bigotry/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ </C>
<C>whenever/WRB gays/NNS are/AUX involved/VBN a/DT group/NN of/IN people/NNS always/RB opposes/VBZ ./. </C>
</S>
<S>
<C>S1/NNP refutes/VBZ that/IN by/IN saying/VBG its/PRP$ all/RB about/IN motivation/NN ,/, </C>
<C>if/IN any/DT other/JJ minority/NN would/MD try/VB and/CC change/VB traditional/JJ marriage/NN ,/, </C>
<C>they/PRP would/MD be/AUX met/VBN with/IN the/DT same/JJ opposition/NN ./. </C>
</S>
</P>
</T>
