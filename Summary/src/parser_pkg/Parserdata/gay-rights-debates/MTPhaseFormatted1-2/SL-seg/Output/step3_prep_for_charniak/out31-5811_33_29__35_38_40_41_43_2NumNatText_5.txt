<s> <PARAGRAPH> </s>
<s>  S1 believes that in 1909, the stance against homosexuality was based on a moral obligation and not prejudice because homosexuality at that time was viewed as behavioral and not genetic. </s>
<s> This person parallels the treatment of women and blacks during that time period and state that it was viewed, back then, as morally right. </s>
<s> They argue that since most people viewed homosexuality as a behavior and not a state of being in 1909, it could not have been prejudiced. </s>
<s> This person does not believe that there was a social identity among homosexuals in 1909 because society viewed it as morally wrong. </s>
<s> S2 argues against the idea that ignorance is a reasonable excuse for prejudice. </s>
<s> This person also uses the parallel of treatment of blacks and women, but argues that morality was an excuse to hide the prejudice. </s>
<s> They argue that ignorance is what led to a distinct prejudice towards certain lifestyles and people, and argues against the idea that there was not a social identity for homosexuals in 1909 when there was obvious disapproval aimed in their direction. </s>
<s>  </s>
