<PARAGRAPH> S1 argues that most people in the country might identify as Christian and that questioning a study is a very appropriate response to one that vilifies a group of people.
They believe that there is a lot of attention given to trying to make Christianity look bad through generalization, irrelevant studies and misinterpretations.
They believe the divorce rate of Christians has no bearing on the gay marriage amendment.
S2 believes that S1 is making up objections and deciding without any factual evidence.
He or she believes that objections should not be hypothetical, but actual proven ones.
This person thinks that dismissing results, rather than checking validity is reactionary.
This person thinks it is relevant as long as people say they are protecting the sanctity of marriage.
