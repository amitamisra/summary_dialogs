(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (VP (VBN focused) (PP (IN on) (NP (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (CC and) (NP (NP (DT the) (JJ cultural) (NN definition)) (, ,) (NP (NP (DT both)) (VP (VBG limiting) (NP (NN marriage)) (PP (TO to) (NP (JJ heterosexual) (NNS couples)))))))))) (. .)))

(S1 (S (S (NP (DT The) (JJ cultural) (NN definition)) (VP (VBZ includes) (NP (NN reproduction)))) (, ,) (NP (PRP he)) (VP (VBZ uses) (NP (DT the) (NN term) (NN mating) (NN mating) (NNS pairs))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT this) (NN definition)) (ADVP (RB automatically)) (VP (VBZ excludes) (NP (NNS homosexuals)) (SBAR (IN because) (S (NP (PRP they)) (VP (VBP cannot) (, ,) (PP (IN without) (NP (JJ outside) (NN help))) (, ,) (VP (VB create) (NP (DT a) (NN family)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ discusses) (NP (NP (JJ other) (NN marriage) (NN outside)) (PP (IN of) (NP (UCP (NP (DT the) (JJ norm-a) (JJ young) (NN woman)) (CC and) (ADJP (JJR older))) (NNS man-)))) (PP (IN as) (S (VP (AUXG being) (ADJP (RBR less) (JJ meaningful) (PP (IN than) (NP (NP (DT those)) (SBAR (WHNP (WDT that)) (S (VP (VBP fulfill) (NP (DT the) (JJ cultural) (NN definition)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ sees) (NP (PRP it)) (PP (IN as) (NP (NP (DT the) (NN job)) (PP (IN of) (NP (NP (VBN elected) (NNS officials)) (CC and) (NP (NP (DT the) (NNS citizens)) (SBAR (S (VP (TO to) (VP (VB change) (NP (NP (DT the) (JJ legal) (NN definition)) (PP (IN of) (NP (NN marriage)))) (SBAR (IN if) (S (NP (DT that)) (VP (AUX is) (SBAR (WHNP (WP what)) (S (NP (DT the) (NN majority)) (VP (VBP want))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ disagrees) (PP (IN with))) (CC and) (VP (VBZ questions) (NP (NP (DT the) (JJ cultural) (NN definition)) (VP (VBN given) (PP (IN by) (NP (NNP S1))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (JJ main) (NN factor)) (PP (IN in) (NP (NN marriage)))) (VP (AUX is) (RB not) (NP (NP (NN reproduction)) (CC but) (NP (NP (NN love) (CC and) (NN commitment)) (PP (IN between) (NP (CD two) (NNS individuals))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ challenges) (NP (NP (DT the) (JJ cultural) (NN definition)) (PP (IN with) (S (NP (NP (DT the) (NN example)) (PP (IN of) (NP (DT a) (ADJP (NP (CD 65) (CC and) (CD 68) (NN year)) (JJ old))))) (VP (VBG getting) (VP (VBN married))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ finds) (S (NP (PRP it)) (ADJP (JJ unfair)) (SBAR (IN that) (S (NP (NP (DT a) (NN group)) (PP (IN of) (NP (NNS people)))) (VP (AUX are) (VP (AUXG being) (VP (VBN excluded) (PP (IN from) (NP (NP (DT an) (NN institution)) (SBAR (WHPP (IN on) (WHNP (WDT which))) (S (NP (PRP they)) (VP (VBP pose) (NP (NP (DT no) (NN threat)) (PP (TO to) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (VP (VBG enjoying) (NP (DT the) (NN status) (NN quo))))))))))))))))))))) (. .)))

