(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBD labeled) (S (NP (NN someone)) (NP (NNP Mr) (NNP Taliban)))) (CC and) (VP (VBD said) (SBAR (IN that) (S (NP (NP (DT this) (NN person)) (SBAR (WHNP (WP who)) (S (VP (AUX is) (PP (IN against) (NP (NN sharia) (NN law))))))) (VP (AUX is) (S (NP (PRP himself)) (VP (VBG imposing) (NP (DT that)) (PP (IN on) (NP (NNS gays))) (PP (IN through) (NP (PRP$ his) (JJ religious) (NN belief) (SBAR (IN that) (S (NP (DT a) (JJ civil) (NN marriage) (NN contract)) (VP (AUX is) (ADVP (RB only)) (PP (IN for) (NP (NP (DT a) (NN man)) (CC and) (NP (DT a) (NN woman)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (S (NP (NP (NNP Mr.) (NNP Taliban) (POS 's)) (JJ religious) (NNS beliefs)) (VP (MD should) (RB not) (VP (AUX have) (NP (NP (NN anything)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (JJ civil) (NN law))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD challenged) (NP (NP (NP (NNP S1) (POS 's)) (NN comparison)) (PP (IN of) (NP (DT the) (NN man)))) (PP (TO to) (NP (DT the) (NNP Taliban))) (SBAR (IN as) (S (NP (PRP he)) (VP (AUX has) (RB not) (VP (VBN advocated) (NP (NP (DT the) (NN execution) (, ,) (NN jailing) (, ,) (FW etc.)) (PP (IN of) (NP (NNS homosexuals))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBD stated) (SBAR (IN that) (S (VP (AUX is) (ADVP (RB indeed)) (NP (NP (DT the) (JJ gay) (NN lobby)) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (VP (VBG attempting) (S (VP (TO to) (VP (VB impose) (NP (PRP$ its) (NN view)) (PP (IN on) (NP (DT the) (NN man))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD concluded) (PP (IN by) (S (VP (VBG asking) (NP (NNP S1)) (S (VP (TO to) (VP (VB stop) (S (VP (VBG complaining) (SBAR (IN as) (S (NP (EX there)) (VP (AUX have) (VP (AUX been) (NP (NP (DT no) (JJ real) (NNS violations)) (PP (IN of) (NP (NP (NNP S1) (POS 's)) (NNS rights))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBD said) (SBAR (S (NP (PRP he)) (VP (MD can) (VP (AUX have) (NP (NP (DT a) (NN contract)) (CC and) (NP (ADJP (RB as) (JJ much)) (NN sex))) (SBAR (IN as) (S (NP (PRP he)) (VP (VBZ likes))))))))) (. .)))

