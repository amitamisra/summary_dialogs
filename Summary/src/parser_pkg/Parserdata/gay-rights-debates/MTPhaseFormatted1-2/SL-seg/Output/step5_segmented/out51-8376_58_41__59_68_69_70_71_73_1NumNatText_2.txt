<T>
<P>
</P>
<P>
<S>
<C>This/DT discussion/NN is/AUX about/IN the/DT government/NN roll/NN in/IN marriage/NN ,/, specifically/RB pertaining/VBG to/TO homosexual/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN the/DT government/NN should/MD have/AUX no/DT say/NN in/IN marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN government/NN should/MD have/AUX a/DT say/NN in/IN marriage/NN </C>
<C>because/IN it/PRP is/AUX more/RBR practical/JJ in/IN today/NN 's/POS world/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ that/IN individual/JJ contracts/NNS by/IN lawyers/NNS rather/RB than/IN the/DT government/NN being/AUXG involved/VBN would/MD be/AUX the/DT fairest/JJS compromise/NN ./. </C>
</S>
<S>
<C>S2/NNP encountered/VBD problems/NNS with/IN this/DT view/NN in/IN their/PRP$ personal/JJ life/NN </C>
<C>when/WRB their/PRP$ partner/NN died/VBD ,/, </C>
<C>and/CC they/PRP were/AUX unable/JJ to/TO sort/VB out/RP all/DT of/IN the/DT legal/JJ issues/NNS involved/VBN in/IN death/NN ,/, like/IN inheritance/NN ,/, or/CC social/JJ security/NN rights/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ there/EX will/MD be/AUX privileges/NNS that/WDT come/VBP only/RB in/IN government/NN recognized/VBN unions/NNS ./. </C>
</S>
<S>
<C>S1/JJ comments/NNS best/JJS way/NN to/TO go/VB about/IN solving/VBG these/DT problems/NNS is/AUX to/TO take/VB them/PRP one/CD issue/NN at/IN a/DT time/NN rather/RB then/RB facing/VBG the/DT problem/NN as/IN a/DT whole/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ that/IN you/PRP need/AUX to/TO look/VB at/IN situations/NNS in/IN from/IN different/JJ perspectives/NNS like/IN trees/NNS in/IN the/DT woods/NNS ./. </C>
</S>
<S>
<C>Both/DT S1/NNP and/CC S2/NNP have/AUX previous/JJ experience/NN with/IN married/JJ life/NN and/CC single/JJ life/NN ./. </C>
</S>
</P>
</T>
