<T>
<P>
</P>
<P>
<S>
<C>Two/CD subjects/NNS are/AUX discussing/VBG the/DT issue/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP is/AUX in/IN favor/NN of/IN gay/JJ marriage/NN </C>
<C>while/IN S2/NNP is/AUX against/IN it/PRP ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ </C>
<C>if/IN allowances/NNS are/AUX altered/VBN to/TO allow/VB gay/JJ marriage/NN ,/, </C>
<C>then/RB the/DT other/JJ restrictions/NNS would/MD need/AUX removed/VBN as/RB well/RB ./. </C>
</S>
<S>
<C>He/PRP also/RB feels/VBZ doing/VBG so/RB would/MD reduce/VB moral/JJ judgment/NN ./. </C>
</S>
<S>
<C>S1/NNP disagrees/VBZ advising/VBG the/DT general/JJ consensus/NN is/AUX that/IN all/DT people/NNS are/AUX entitled/VBN to/TO equal/JJ rights/NNS </C>
<C>except/IN when/WRB they/PRP include/VBP gays/NNS ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ there/EX is/AUX no/DT document/NN defining/VBG equal/JJ rights/NNS making/VBG S1/NNP 's/POS argument/NN invalid/JJ ./. </C>
</S>
<S>
<C>S1/NNP defends/VBZ his/PRP$ original/JJ argument/NN </C>
<C>by/IN saying/VBG the/DT Constitution/NNP </C>
<C>as/IN it/PRP is/AUX defined/VBN right/RB now/RB prevents/VBZ homosexuals/NNS from/IN their/PRP$ rights/NNS to/TO life/NN ,/, liberty/NN ,/, and/CC the/DT pursuit/NN of/IN happiness/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB feels/VBZ that/IN supporting/VBG the/DT rules/NNS remaining/VBG the/DT same/JJ amounts/NNS to/TO bigotry/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ this/DT </C>
<C>by/IN stating/VBG his/PRP$ feelings/NNS that/IN if/IN laws/NNS were/AUX altered/VBN to/TO allow/VB gay/JJ marriage/NN ,/, </C>
<C>they/PRP would/MD also/RB need/AUX to/TO be/AUX altered/VBN to/TO allow/VB things/NNS such/JJ as/IN incest/NN marriages/NNS ,/, </C>
<C>challenging/VBG S1/NNP to/TO offer/VB information/NN to/TO support/VB why/WRB gay/JJ marriage/NN laws/NNS should/MD be/AUX passed/VBN ;/: </C>
<C>however/RB ,/, a/DT ban/NN remain/VBP on/IN other/JJ such/JJ unions/NNS ./. </C>
</S>
</P>
</T>
