(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (S (NP (NNP S2)) (VP (AUX has) (VP (VBN answered) (NP (NP (PRP$ her) (NN question)) (PP (IN of) (SBAR (WHADVP (WRB how)) (S (NP (JJ equal) (NN marriage)) (VP (VBZ hurts) (NP (DT any) (NN heterosexual))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN point)) (VP (AUX has) (VP (AUX been) (VP (VBN made) (SBAR (IN because) (S (NP (JJ different) (NNS laws)) (VP (VBP affect) (NP (JJ different) (NNS people))))))))))) (. .)))

(S1 (S (NP (JJ S2) (NNS attacks)) (VP (VBN S1) (PP (IN by) (S (VP (VBG calling) (S (NP (PRP$ their) (NN argument)) (`` ``) (ADJP (JJ silly)) ('' '')) (SBAR (IN because) (S (NP (PRP it)) (VP (VBZ violates) (NP (DT the) (NNP First) (NN amendment) (NNS rights))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ remarks) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (ADVP (RB only)) (VP (VBN concerned) (PP (IN with) (NP (NP (DT the) (NNS rights)) (PP (IN of) (NP (JJ gay) (CC and) (DT the) (JJ anti-gay) (NNP Christianity)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB specifically)) (VP (NNS names) (S (NP (NP (DT a) (NN woman)) (PP (IN in) (ADJP (JJ particular)))) (VP (TO to) (VP (VB admit) (NP (PRP$ her) (NN discrimination)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ expresses) (SBAR (IN that) (S (NP (NP (DT the) (NNS rights)) (PP (IN of) (NP (NNS gays)))) (VP (AUX is) (NP (NP (PRP$ their) (JJ first) (NN concern)) (CC and) (NP (NP (NN protection)) (PP (IN from) (NP (NN church))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ threatens) (S (VP (TO to) (VP (VB respond) (PP (IN with) (NP (NP (JJ similar) (NNS actions)) (PP (IN against) (NP (DT the) (NN church)))))))) (, ,) (PP (IN like) (S (VP (VBG protesting) (NP (NP (JJ ceremonial) (NNS events)) (CC and) (NP (NN church) (NNS services))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ urges) (NP (NNP S1)) (S (VP (TO to) (VP (VB allow) (S (NP (DT the) (JJ democratic) (NN process)) (VP (TO to) (VP (VB decide) (NP (DT the) (NN issue)) (, ,) (S (VP (VBG believing) (SBAR (S (NP (DT the) (JJ anti-gay) (NN movement)) (VP (MD may) (ADVP (RB also)) (VP (AUX have) (NP (NN legitimacy))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ questions) (NP (DT the) (NN majority) (NN rule) (NN ideology)) (PP (IN because) (IN of) (NP (NP (NNS incidents)) (PP (IN in) (NP (DT the) (NN past))))) (PP (IN like) (NP (NN slavery) (CC and) (NN concentration) (NNS camps)))) (. .)))

(S1 (S (PRN (, ,) (CC and) (ADVP (RB once) (RB again))) (VP (NNS calls) (PRT (RP out)) (NP (DT the) (NNS women))) (. .)))

(S1 (S (NP (NNP S2)) (VP (NNS ends) (PP (IN by) (S (VP (VBG stating) (SBAR (S (NP (DT those)) (VP (MD would) (RB not) (VP (AUX be) (ADJP (JJ possible) (NP (NN today))) (PP (IN because) (IN of) (NP (DT the) (JJ 13th) (NN amendment))))))))))) (. .)))

