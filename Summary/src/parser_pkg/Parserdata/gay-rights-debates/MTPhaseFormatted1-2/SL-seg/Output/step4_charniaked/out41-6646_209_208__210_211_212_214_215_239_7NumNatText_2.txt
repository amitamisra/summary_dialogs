(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG debating) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (MD should) (VP (AUX be) (VP (VBN decided) (PP (IN by) (NP (DT a) (JJ public) (NN vote))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ cruel)) (S (VP (TO to) (VP (VB limit) (NP (NP (NN someone) (RB else)) (PP (IN from) (NP (VBG enjoying) (NNS rights))) (SBAR (S (NP (PRP you)) (VP (MD can) (ADVP (RB currently)) (VP (VB enjoy))))))))))))) (. .)))

(S1 (S (NP (NP (NNS Opponents)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (VP (MD should) (VP (VB try) (S (VP (TO to) (VP (AUX have) (NP (NP (JJR more) (NN empathy)) (PP (IN for) (NP (NP (JJ gay) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP wish) (S (VP (TO to) (VP (VB marry))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ retorts) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ cruel) (SBAR (IN for) (S (NP (NN society)) (VP (TO to) (VP (VB bar) (NP (NN someone)) (PP (IN from) (S (VP (VBG enjoying) (NP (DT a) (JJ particular) (NN right)))))))))))))) (. .)))

(S1 (S (NP (NN Society)) (VP (VP (MD can)) (CC and) (VP (AUX does) (VP (VB decide) (SBAR (WHNP (WP what)) (S (NP (PRP it)) (VP (VBZ deems) (S (VP (TO to) (VP (AUX be) (ADJP (RB socially) (JJ acceptable))))) (PP (IN through) (NP (NN consensus))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (S (VP (VP (NN tradition)) (CC or) (VP (AUXG being) (ADJP (JJ mundane))))) (VP (AUX does) (RB not) (VP (VB make) (NP (NP (NN something)) (ADJP (DT any) (ADJP (RBR more) (JJ wrong)) (CC or) (ADJP (JJ right))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ compares) (NP (NP (NNP S1) (POS 's)) (NN argument)) (PP (TO to) (NP (NP (DT a) (NN justification)) (PP (IN for) (NP (NN slavery)))))) (. .)))

(S1 (S (NP (DT The) (NN majority)) (ADVP (RB once)) (VP (VBD held) (NP (NN slavery)) (S (VP (TO to) (VP (AUX be) (ADJP (JJ correct)) (, ,) (SBAR (RB hence) (S (NP (PRP it)) (VP (AUX was) (ADJP (RB morally) (CC and) (RB socially) (JJ acceptable)) (PP (VBG according) (PP (TO to) (NP (NP (NNP S1) (POS 's)) (NN logic))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (VP (VBG arguing) (PP (IN for) (NP (NP (NN something)) (VP (AUXG being) (ADJP (JJ correct))))) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ traditional))))) (, ,) (SBAR (RB only) (IN that) (S (NP (PRP it)) (VP (MD can) (VP (AUX be) (VP (VBN changed) (PP (IN through) (NP (JJ convincing) (NN society))) (S (VP (TO to) (VP (VB change)))))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (PP (IN up) (PP (TO to) (NP (NN society)))) (S (VP (TO to) (VP (VB determine) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (JJ permissible)) (PP (IN through) (NP (NP (DT a) (NN dialog)) (PP (IN between) (NP (NP (DT the) (NNS proponents)) (PP (IN of) (NP (JJ gay) (NN marriage) (CC and) (NN society)))))))))))))) (. .)))

