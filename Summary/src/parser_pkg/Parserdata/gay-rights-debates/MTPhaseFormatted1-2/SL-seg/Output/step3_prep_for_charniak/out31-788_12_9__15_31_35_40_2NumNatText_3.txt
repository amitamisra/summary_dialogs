<s> <PARAGRAPH> </s>
<s>  S1 claims that legalizing homosexual marriage would eventually lead to the legalization of polygamous marriage, citing a proposed constitutional amendment that bans both gay marriage and polygamy. </s>
<s> He cites a passage from the Bible where Jesus said a man should be cleaved to his wife. </s>
<s> He claims that if people on the right wing support a ban on gay marriage, then the left wing must oppose a ban on gay marriage. </s>
<s> S2 cites instances of God supporting polygamy in the Bible. </s>
<s> He claims that we already practice serial polygamy because of America's liberal divorce laws. </s>
<s> He also points out the logical fallacy in S1's argument about the left and right wing, stating that one wing supporting something does not mean the other wing opposes it. </s>
<s>  </s>
