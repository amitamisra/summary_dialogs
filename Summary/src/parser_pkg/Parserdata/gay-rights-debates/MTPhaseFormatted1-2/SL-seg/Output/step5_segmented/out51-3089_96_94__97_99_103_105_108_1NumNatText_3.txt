<T>
<P>
</P>
<P>
<S>
<C>S1/NNP notes/VBZ that/IN although/IN John/NNP Kerry/NNP was/AUX expected/VBN to/TO win/VB the/DT election/NN in/IN Ohio/NNP ,/, </C>
<C>he/PRP lost/VBD ./. </C>
</S>
<S>
<C>He/PRP attributes/VBZ this/DT loss/NN to/TO the/DT disapproval/NN of/IN homosexuality/NN by/IN Ohioans/NNPS ,/, </C>
<C>as/IN indicated/VBN by/IN the/DT passing/NN of/IN a/DT gay/JJ marriage/NN amendment/NN and/CC their/PRP$ support/NN for/IN President/NNP Bush/NNP who/WP he/PRP claims/VBZ won/VBN by/IN a/DT larger/JJR margin/NN than/IN President/NNP Clinton/NNP ./. </C>
</S>
<S>
<C>S2/NNP rejects/VBZ the/DT claim/NN that/IN the/DT people/NNS of/IN Ohio/NNP were/AUX silent/JJ about/IN their/PRP$ views/NNS of/IN same-sex/JJ marriage/NN and/CC claims/VBZ that/IN although/IN President/NNP Bush/NNP won/VBD with/IN a/DT larger/JJR margin/NN ,/, </C>
<C>President/NNP Clinton/NNP still/RB won/VBD by/IN a/DT larger/JJR percentage/NN ,/, due/JJ to/TO the/DT increased/VBN population/NN and/CC larger/JJR voter/NN turnout/NN in/IN President/NNP Bush/NNP 's/POS election/NN ./. </C>
</S>
<S>
<C>S1/NNP claims/VBZ that/IN population/NN is/AUX irrelevant/JJ ,/, </C>
<C>citing/VBG several/JJ statistics/NNS showing/VBG a/DT low/JJ level/NN of/IN support/NN for/IN President/NNP Clinton/NNP ,/, </C>
<C>and/CC also/RB claims/VBZ that/IN the/DT same-sex/JJ marriage/NN issue/NN definitely/RB had/AUX an/DT effect/NN on/IN the/DT outcome/NN of/IN the/DT Ohio/NNP election/NN ./. </C>
</S>
<S>
<C>S2/NNP again/RB responds/VBZ with/IN statistics/NNS showing/VBG that/IN President/NNP Clinton/NNP won/VBD by/IN larger/JJR percentages/NNS than/IN President/NNP Bush/NNP ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB deny/VB that/IN the/DT same-sex/JJ marriage/NN issue/NN was/AUX relevant/JJ ,/, </C>
<C>but/CC denies/VBZ that/IN there/EX was/AUX a/DT silent/JJ majority/NN ./. </C>
</S>
</P>
</T>
