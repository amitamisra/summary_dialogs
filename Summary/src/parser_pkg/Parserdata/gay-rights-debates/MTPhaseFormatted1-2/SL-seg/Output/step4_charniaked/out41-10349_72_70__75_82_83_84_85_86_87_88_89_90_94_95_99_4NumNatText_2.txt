(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN everyone)) (VP (MD should) (VP (VB live) (PP (IN within) (NP (NP (DT the) (NNS bounds)) (PP (IN of) (NP (NP (DT the) (NNS laws)) (PP (IN of) (NP (NN society)))))))))))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (AUX are) (ADJP (JJ free) (S (VP (TO to) (VP (AUX be) (VP (VBN changed)))))))) (CC but) (S (ADJP (RB not) (JJ broken))) (. .)))

(S1 (S (NP (NP (NP (NNP Society) (POS 's)) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (RB not) (VP (VBN based) (ADVP (RB merely)) (PP (PP (IN on) (NP (NN religion))) (, ,) (CC but) (PP (IN on) (NP (NN biology)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VP (AUX are) (RB not) (ADJP (JJ able) (S (VP (TO to) (VP (VB reproduce)))))) (CC but) (VP (MD can) (VP (VB marry))))))) (VP (VBZ invalidates) (NP (NP (NNP S1) (POS 's)) (NN argument)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ reiterates) (SBAR (IN that) (S (NP (NP (DT the) (JJ ultimate) (VBN intended) (NN goal)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (S (VP (TO to) (VP (VB make) (S (NP (NN reproduction)) (ADJP (JJR easier)))))))))) (. .)))

(S1 (S (VP (VBG Bringing) (PRT (RP up)) (NP (NNS exceptions) (S (VP (TO to) (VP (AUX does) (RB not) (VP (VB change) (NP (NN anything)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NP (NNP S2) (POS 's)) (NN opposition)) (PP (TO to) (NP (JJ gay) (NN marriage) (NNS bans)))) (VP (VBZ stems) (PP (IN from) (S (RB not) (VP (VBG receiving) (NP (NNS benefits) (SBAR (IN that) (S (NP (DT a) (JJ married) (NN couple)) (VP (AUX does)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ gay) (NN marriage) (NNS bans)) (VP (AUX are) (ADJP (JJ unequal) (CC and) (JJ unjust)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ emphasizes) (NP (JJ universal) (NN equality))) (CC and) (VP (VBZ desires) (S (VP (TO to) (VP (AUX be) (VP (VBN treated) (SBAR (RB just) (IN as) (S (NP (NP (JJ heterosexual) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX do) (RB not) (VP (VB reproduce)))))) (VP (AUX are) (PP (IN in) (NP (NP (NNS terms)) (PP (IN of) (NP (DT the) (NN ability) (S (VP (TO to) (VP (VB marry))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (PP (IN by) (S (VP (VBG placing) (NP (NN marriage) (NN law)) (PP (IN in) (NP (NP (DT the) (NN realm)) (PP (IN of) (NP (DT the) (NNS states)))))))) (, ,) (RB then) (NP (DT the) (NNS states)) (VP (MD can) (VP (VB determine) (NP (NP (WDT which) (NN type)) (PP (IN of) (NP (NN marriage)))) (S (VP (TO to) (VP (VB recognize))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (SBAR (RB even) (IN if) (S (NP (DT this)) (VP (AUX were) (NP (DT the) (NN case))))) (, ,) (NP (PRP it)) (VP (MD would) (ADVP (RB still)) (VP (AUX be) (ADJP (JJ unjust))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNP S2)) (VP (VBZ ignores) (NP (NP (DT the) (JJ biological) (NNS differences)) (VP (VBN involved))))))) (. .)))

