<T>
<P>
</P>
<P>
<S>
<C>S1/NNP feels/VBZ that/IN Jim/NNP and/CC MrWrite/NNP 's/POS argument/NN has/AUX become/VBN a/DT battle/NN of/IN insults/NNS ,/, </C>
<C>and/CC that/IN this/DT is/AUX unfortunate/JJ ,/, </C>
<C>but/CC that/IN the/DT comparison/NN between/IN the/DT argument/NN against/IN gay/JJ marriage/NN and/CC that/DT against/IN interracial/JJ marriage/NN is/AUX not/RB baseless/JJ ./. </C>
</S>
<S>
<C>He/PRP cites/VBZ that/IN opponents/NNS of/IN gay/JJ marriage/NN use/VBP many/JJ of/IN the/DT same/JJ arguments/NNS that/WDT were/AUX used/VBN to/TO oppose/VB interracial/JJ marriage/NN ,/, </C>
<C>before/IN that/DT was/AUX outlawed/VBN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN most/JJS opponents/NNS of/IN gay/JJ marriage/NN do/AUX so/RB for/IN religious/JJ or/CC homophobic/JJ reasons/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN words/NNS get/VBP redefined/VBD on/IN an/DT ongoing/JJ basis/NN ,/, </C>
<C>citing/VBG the/DT word/NN war/NN being/AUXG modified/VBN to/TO suit/VB political/JJ purposes/NNS ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB believe/VB that/IN the/DT justifications/NNS for/IN the/DT current/JJ war/NN in/IN Iraq/NNP have/AUX much/JJ merit/NN ,/, </C>
<C>and/CC that/IN the/DT same/JJ argument/NN could/MD be/AUX made/VBN for/IN reentering/VBG Vietnam/NNP or/CC Korea/NNP ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB feel/VB that/IN interracial/JJ marriage/NN and/CC homosexual/JJ marriage/NN have/AUX anything/NN to/TO do/AUX with/IN each/DT other/JJ ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN separate-but-equal/JJ would/MD apply/VB to/TO the/DT records/NNS of/IN gay/JJ couples/NNS </C>
<C>but/CC would/MD not/RB justify/VB any/DT restrictions/NNS of/IN there/RB rights/NNS or/CC privileges/NNS as/IN a/DT couple/NN ./. </C>
</S>
<S>
<C>He/PRP preemptively/RB denies/VBZ being/AUXG homophobic/JJ ,/, </C>
<C>citing/VBG his/PRP$ having/AUXG gay/JJ friends/NNS who/WP know/VBP what/WP his/PRP$ views/NNS on/IN the/DT word/NN marriage/NN are/AUX ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB feel/VB that/IN the/DT Iraq/NNP War/NNP is/AUX technically/RB a/DT new/JJ war/NN requiring/VBG all/DT of/IN the/DT congressional/JJ steps/NNS ,/, </C>
<C>rather/RB that/IN it/PRP was/AUX merely/RB a/DT continuation/NN of/IN the/DT Gulf/NNP War/NNP ,/, </C>
<C>or/CC that/IN the/DT same/JJ arguments/NNS could/MD be/AUX applied/VBN to/TO those/DT other/JJ conflicts/NNS ./. </C>
</S>
</P>
</T>
