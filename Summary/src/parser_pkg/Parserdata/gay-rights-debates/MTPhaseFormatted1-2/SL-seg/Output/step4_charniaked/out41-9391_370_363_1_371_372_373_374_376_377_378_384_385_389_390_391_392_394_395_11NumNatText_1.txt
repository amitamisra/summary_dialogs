(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (PRP$ their) (NN argument)) (VP (AUX has) (NP (NP (NN anything)) (SBAR (S (VP (TO to) (VP (AUX do) (S (VP (VBG relating) (PP (TO to) (NP (JJ racial) (NN segregation))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNP S2)) (VP (VP (AUX has) (ADVP (RB either)) (VP (VBN misinterpreted) (NP (PRP$ their) (NN argument)))) (CC or) (VP (AUX is) (ADJP (JJ ignorant) (PP (IN of) (SBAR (WHNP (WP what)) (S (VP (AUX was) (ADVP (RB actually)) (VP (VBN argued)))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ argues) (SBAR (IN that) (SBAR (IN because) (S (NP (NNP S1) (NNS cannot)) (VP (VBP answer) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBD thought) (SBAR (S (NP (PRP they)) (VP (AUX were) (VP (VBG arguing) (, ,) (SBAR (IN that) (S (NP (PRP they)) (VP (VBP cannot) (ADJP (RB truly) (JJ validate)) (NP (PRP$ their) (NN argument))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB also)) (VP (VBP argue) (SBAR (IN that) (S (PP (IN because) (IN of) (NP (DT this))) (, ,) (NP (NNP S2)) (VP (VBD cannot) (SBAR (S (VP (VB prove) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBD said) (SBAR (S (VP (AUX was) (ADJP (JJ correct) (CC or) (JJ incorrect))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP state) (SBAR (IN that) (S (NP (PRP$ their) (NN argument)) (VP (AUX was) (RB not) (VP (VBN meant) (S (VP (TO to) (VP (AUX be) (VP (VBN taken) (SBAR (IN that) (S (NP (NP (DT the) (NN plight)) (PP (IN of) (NP (NNS homosexuals)))) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (NP (DT the) (NN plight)) (PP (IN of) (NP (JJ racial) (NNS minorities)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (NNP S1) (POS 's)) (NN argument)) (VP (VBZ purports) (SBAR (IN that) (S (NP (JJ racial) (NN segregation)) (VP (AUX has) (NP (NP (DT no) (NN parallel)) (PP (TO to) (NP (NP (NN discrimination)) (PP (IN against) (NP (NNS homosexuals))))))))))))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NP (DT every) (NN time) (NN prejudice)) (, ,) (NP (NN racism)) (, ,) (NP (NN sexism)) (, ,) (NP (NNP anti-semitism)) (CC or) (NP (NN homophobia))) (VP (AUX is) (VP (VBN challenged))))))) (, ,) (S (NP (DT the) (NNS perpetrators)) (ADVP (RB always)) (VP (VBP claim) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (DT the) (JJ previous) (NN time))))))))) (CC but) (S (PP (IN in) (NP (NN truth))) (NP (PRP it)) (ADVP (RB actually)) (VP (AUX is) (PP (IN without) (NP (NN exception))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG arguing) (SBAR (IN that) (S (NP (NP (NN discrimination)) (PP (IN against) (NP (NNS homosexuals)))) (VP (AUX is) (ADJP (JJ different) (PP (IN from) (NP (NP (NN discrimination)) (PP (IN against) (NP (JJ racial) (NNS minorities)))))))))))))) (. .)))

(S1 (S (ADVP (RB Eventually)) (NP (PRP they)) (VP (VBP realize) (SBAR (IN that) (S (NP (PRP they)) (VP (VP (MD may) (VP (AUX have) (VP (AUX been) (ADJP (JJ wrong))))) (CC and) (VP (VP (VB retract) (NP (PRP$ their) (NN statement))) (CC and) (VP (VB apologize))))))) (. .)))

