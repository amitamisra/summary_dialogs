<T>
<P>
</P>
<P>
<S>
<C>S1/NNP points/VBZ out/RP that/IN his/PRP$ heteronormative/JJ family/NN represents/VBZ the/DT norm/NN and/CC suggests/VBZ that/IN deviation/NN from/IN the/DT norm/NN in/IN the/DT form/NN of/IN homosexual/JJ families/NNS is/AUX perverse/JJ ./. </C>
</S>
<S>
<C>He/PRP clarifies/VBZ that/IN he/PRP does/AUX not/RB hate/VB gays/NNS ,/, </C>
<C>but/CC he/PRP simply/RB does/AUX not/RB believe/VB that/IN the/DT definition/NN of/IN marriage/NN should/MD be/AUX changed/VBN to/TO include/VB homosexual/JJ unions/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN S2/NNP 's/POS beliefs/NNS do/AUX not/RB make/VB him/PRP correct/VB ./. </C>
</S>
<S>
<C>S2/NNP suggests/VBZ that/IN deviation/NN from/IN the/DT norm/NN is/AUX simply/RB a/DT form/NN of/IN diversity/NN and/CC is/AUX not/RB necessarily/RB perverse/JJ ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ S1/NNP of/IN hating/VBG homosexuals/NNS ./. </C>
</S>
<S>
<C>In/IN response/NN to/TO S1/NNP 's/POS claims/NNS that/IN he/PRP does/AUX not/RB hate/VB homosexuals/NNS ,/, S2/NNP points/VBZ out/RP that/IN calling/VBG homosexual/JJ relationships/NNS perverse/JJ is/AUX clearly/RB a/DT form/NN of/IN hate/NN ,/, </C>
<C>and/CC that/IN it/PRP is/AUX clear/JJ from/IN S1/NNP 's/POS language/NN that/IN he/PRP does/AUX in/IN fact/NN hate/VB people/NNS who/WP are/AUX homosexual/JJ ./. </C>
</S>
</P>
</T>
