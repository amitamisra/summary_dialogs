<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX debating/VBG the/DT topic/NN of/IN what/WP marriage/NN should/MD mean/VB to/TO our/PRP$ government/NN ./. </C>
</S>
<S>
<C>S1/NNP Believes/VBZ it/PRP is/AUX not/RB a/DT bad/JJ idea/NN </C>
<C>because/IN they/PRP think/VBP it/PRP is/AUX not/RB necessary/JJ for/IN the/DT government/NN to/TO be/AUX involved/VBN in/IN most/JJS aspects/NNS of/IN our/PRP$ lives/NNS ,/, </C>
<C>while/IN S2/NNP is/AUX questioning/VBG why/WRB it/PRP 's/AUX necessary/JJ to/TO try/VB and/CC remove/VB government/NN involvement/NN </C>
<C>when/WRB it/PRP is/AUX unavoidable/JJ ./. </C>
</S>
<S>
<C>S1/NNP brings/VBZ up/RP that/IN because/IN several/JJ people/NNS are/AUX against/IN the/DT government/NN due/JJ to/TO gay/JJ marriage/NN </C>
<C>and/CC not/RB because/IN it/PRP 's/AUX just/RB marriage/NN altogether/RB ,/, </C>
<C>it/PRP does/AUX not/RB mean/VB it/PRP 's/AUX not/RB a/DT bad/JJ idea/NN to/TO remove/VB government/NN intervention/NN ./. </C>
</S>
<S>
<C>S1/NNP mentions/VBZ the/DT ``/`` Healthy/JJ Marriage/NN Initiative/NN ''/'' </C>
<C>and/CC how/WRB it/PRP is/AUX not/RB necessary/JJ ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ that/IN there/EX is/AUX too/RB much/RB legal/JJ work/NN and/CC documents/NNS behind/IN marriage/NN in/IN the/DT government/NN ,/, such/JJ as/IN inheritance/NN ,/, pension/NN and/CC social/JJ rights/NNS ,/, etc./FW ,/, </C>
<C>and/CC that/IN it/PRP would/MD be/AUX a/DT lot/NN more/JJR work/NN if/IN the/DT government/NN did/AUX not/RB intervene/VB ,/, </C>
<C>also/RB stating/VBG that/IN removing/VBG marriage/NN from/IN government/NN will/MD only/RB benefit/VB lawyers/NNS ./. </C>
</S>
<S>
<C>Both/DT S1/NNP and/CC S2/NNP have/AUX had/AUX experience/NN with/IN marriage/NN ,/, </C>
<C>S2/NNP states/VBZ that/IN marriage/NN benefits/NNS are/AUX good/JJ because/IN of/IN government/NN intervention/NN ./. </C>
</S>
</P>
</T>
