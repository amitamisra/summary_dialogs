<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ relationships/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX in/IN favor/NN of/IN these/DT unions/NNS </C>
<C>while/IN S2/NNP does/AUX not/RB support/VB them/PRP ./. </C>
</S>
<S>
<C>S1/NNP compares/VBZ homosexual/JJ relationships/NNS to/TO those/DT of/IN men/NNS who/WP practice/VB multiple/JJ marriage/NN contending/VBG homosexuals/NNS are/AUX more/RBR committed/VBN to/TO fidelity/NNP than/IN those/DT who/WP believe/VBP in/IN multiple/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP begs/VBZ the/DT question/NN of/IN how/WRB anyone/NN can/MD say/VB that/IN a/DT man/NN who/WP believes/VBZ in/IN and/CC practices/NNS multiple/JJ marriage/NN does/AUX not/RB love/VB all/DT of/IN his/PRP$ wives/NNS ./. </C>
</S>
<S>
<C>S2/NNP cites/VBZ a/DT 2003/CD study/NN advising/VBG at/IN that/DT time/NN ,/, </C>
<C>62/CD %/NN of/IN Americans/NNPS were/AUX not/RB in/IN favor/NN of/IN gay/JJ unions/NNS ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ the/DT roots/NNS are/AUX deeper/JJR than/IN that/DT particular/JJ poll/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ he/PRP has/AUX a/DT Constitutional/JJ right/NN to/TO be/AUX opposed/VBN to/TO gay/JJ marriage/NN as/RB well/RB as/IN gay/JJ people/NNS themselves/PRP ./. </C>
</S>
</P>
</T>
