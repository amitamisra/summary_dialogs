<PARAGRAPH> S1 claims that it is inappropriate to compare opposing homosexuality with racial discrimination.
He explains the difference that racial discrimination is the fear of a particular person and that opposing homosexuality is based on disapproval of a person's behavior.
He proposes that if a homosexual were fired from their job, it would be due to their incompetence and not due to their homosexuality.
S2 objects to S1's claim that homophobia is disapproval of behavior, questioning which behaviors warrant disapproval.
He claims that being homosexual is in itself not a behavior.
He also argues that homosexuals are frequently legally fired for their sexual preference with no regard for competence.
Finally, he asks what a gay activist is and S1 responds by recommending S2 google the term.
