<s> <PARAGRAPH> </s>
<s>  S1 suggests that statistics on divorce rates are skewed by the high proportion of young people who end up getting divorces. </s>
<s> He claims that the divorce rate drops significantly if you focus only on couples who have been marriage more than five years. </s>
<s> He also claims that statistics show that homosexuals are more promiscuous than heterosexuals and that this would lead to a higher divorce rate for same-sex marriages. </s>
<s> S2 argues that the age of the spouses is irrelevant and that more than half of all heterosexual marriages end in divorce and suggests that it would likely be no difference for same-sex marriages. </s>
<s> He also rejects the comparison of promiscuity between heterosexuals and homosexuals by pointing out that only heterosexuals are legally allowed to marry. </s>
<s>  </s>
