(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (JJ gay) (NNS rights)) (CC and) (NP (NP (DT the) (NN redefinition)) (PP (IN of) (NP (NN marriage))))) (SBAR (IN as) (S (NP (PRP it)) (VP (VBZ pertains) (PP (TO to) (NP (JJ gay) (NNS couples)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (S (NP (DT the) (NNP Massachusetts) (NN decision) (S (VP (TO to) (VP (VB allow) (PP (IN for) (NP (JJ gay) (NNS couples))))))) (VP (TO to) (VP (VB marry) (SBAR (S (VP (AUX is) (NP (NN proof) (SBAR (S (NP (JJ positive) (DT the) (NN world)) (PP (IN as) (NP (DT a) (NN whole))) (VP (AUX is) (VP (VBG changing) (S (VP (TO to) (VP (AUX be) (ADJP (ADJP (RBR more) (VBG accepting)) (PP (IN of) (NP (JJ gay) (NNS couples))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX is) (VP (VBG defending) (NP (DT a) (NN statement)) (NP (NP (NN he/she)) (VP (VBN made) (PP (IN in) (NP (NP (NN regard)) (PP (TO to) (NP (DT the) (NN possibility) (SBAR (IN that) (S (NP (NNP Massachusetts)) (VP (MD may) (PP (IN at) (NP (DT some) (NN point))) (VP (VB decide) (S (VP (TO to) (VP (VB overturn) (NP (DT the) (NN decision) (S (VP (TO to) (VP (VB allow) (S (NP (JJ gay) (NNS couples)) (VP (TO to) (VP (VB marry))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (NP (NNS statements)) (VP (VBN made) (PP (IN by) (NP (NNP S2))))) (VP (VBP indicate) (SBAR (S (NP (NN he/she)) (VP (AUX is) (ADJP (RB completely) (JJ opposed) (PP (TO to) (NP (NP (JJ equal) (NNS rights)) (PP (IN for) (NP (JJ gay) (NNS couples))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (S (NP (NN he/she)) (VP (AUX is) (PP (IN against) (NP (NP (DT an) (NN amendment)) (VP (VBN put) (PP (IN in) (NP (NN place))) (S (VP (TO to) (VP (VB ban) (NP (JJ gay) (NN marriage)) (SBAR (IN as) (S (NP (PRP it)) (VP (MD would) (VP (AUX be) (S (NP (DT the) (NN government)) (VP (AUXG being) (ADJP (RB overly) (JJ intrusive) (PP (IN into) (NP (NP (DT the) (NNS lives)) (PP (IN of) (NP (DT the) (NN public)))))))))))))))))))))) (. .)))

