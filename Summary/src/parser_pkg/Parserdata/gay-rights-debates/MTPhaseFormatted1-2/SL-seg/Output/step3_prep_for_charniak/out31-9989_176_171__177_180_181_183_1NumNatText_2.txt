<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing a quiz which was run on the New York Times website in regard to comparing views of the general public as compared to those of the Supreme Court. </s>
<s> S2 feels the the quiz is horrible as it, in S2's opinion, reinforces the point that people view the court as a legislative body. </s>
<s> S1 feels the quiz is useful but does have some issues. </s>
<s> S2 believes the rulings should not be considered conservative or liberal. </s>
<s> S1 agrees with this point. </s>
<s> S2 feels court rulings should not be based upon a judge's personal beliefs as they are supposed to be based on the constitution. </s>
<s> S2 uses the example of corporations being allowed to run campaign ads. </s>
<s>  </s>
