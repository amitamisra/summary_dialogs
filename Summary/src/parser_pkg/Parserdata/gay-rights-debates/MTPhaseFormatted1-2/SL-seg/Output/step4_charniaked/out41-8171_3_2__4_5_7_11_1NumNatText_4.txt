(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (IN of) (NP (DT the) (NN opinion) (SBAR (IN that) (S (NP (NP (DT a) (JJ certain) (NN level)) (PP (IN of) (NP (NN dishonesty)))) (VP (AUX is) (ADJP (JJ understandable) (CC and) (VBN expected)) (PP (IN in) (NP (NNS politics))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (NN regard)) (PP (TO to) (NP (JJ gay) (NNS rights))))) (, ,) (NP (DT the) (JJ religious) (ADJP (JJ right) (CC and) (JJ conservative)) (NNS republicans)) (VP (AUX have) (VP (VBN made) (S (NP (PRP it)) (ADJP (NP (DT a) (VBN poisoned) (NN topic)) (JJ such)) (SBAR (IN that) (S (NP (NNS politicians)) (VP (MD will) (RB not) (VP (VB say) (NP (DT the) (JJ right) (NN thing))))))) (SBAR (IN because) (S (NP (PRP it)) (VP (MD will) (VP (VB cost) (NP (PRP them)) (NP (NNS votes)))))))) (. .)))

(S1 (S (NP (NNPS Democrats)) (VP (AUX have) (VP (AUX had) (S (VP (TO to) (VP (AUX be) (ADJP (JJ moderate)) (PP (IN on) (NP (DT the) (NN issue))) (SBAR (IN in) (NN order) (S (VP (TO to) (ADVP (RB still)) (VP (AUX be) (PP (IN in) (NP (NP (DT a) (NN position)) (SBAR (S (VP (TO to) (VP (AUX do) (NP (NN something))))))))))))))))) (. .)))

(S1 (S (S (NP (PRP It)) (VP (AUX 's) (ADJP (RB too) (JJ naive) (S (VP (TO to) (VP (VB expect) (PP (TO to) (NP (DT a) (NN politician))))))) (S (VP (TO to) (VP (VB give) (NP (PRP$ their) (NN view)) (PP (IN on) (NP (NNS matters)))))))) (, ,) (CC and) (S (ADVP (RB so)) (NP (PRP it)) (VP (AUX is) (ADJP (JJ necessary) (S (VP (TO to) (VP (VB figure) (PRT (RP out)) (SBAR (SBAR (WHADVP (WRB how) (RB often)) (S (NP (PRP they)) (VP (VBP lie)))) (, ,) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP lie) (PP (IN about))))) (, ,) (CC and) (SBAR (WHADJP (WRB how) (JJ big)) (S (NP (PRP$ their) (NNS lies)) (VP (AUX are))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (DT this) (JJ particular) (NN case))) (, ,) (SBAR (IN if) (S (NP (NNP Biden)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB support) (NP (JJ gay) (NN marriage))))))))) (NP (NNP S1)) (VP (AUX is) (ADJP (ADVP (RB just) (RB as)) (JJ likely) (S (VP (TO to) (VP (VB think) (SBAR (S (NP (PRP it)) (VP (AUX 's) (SBAR (IN because) (S (NP (PRP he)) (VP (VBZ knows) (SBAR (IN that) (S (NP (JJ open) (NN support)) (VP (MD could) (VP (VB cost) (NP (PRP him)) (NP (DT the) (NNP White) (NNP House)) (PP (IN as) (NP (DT any) (JJ other) (NN reason)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (PP (IN as) (NP (JJ political) (NNS consumers))) (NP (PRP we)) (VP (MD should) (VP (VB expect) (NP (NP (NN honesty)) (PP (IN of) (NP (DT the) (NNS politicians))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (RB not) (ADVP (RB too) (RB much)) (S (VP (TO to) (VP (VB demand) (NP (NN honesty)))))) (. .)))

(S1 (S (PP (IN Without) (NP (DT that) (NN expectation))) (NP (PRP it)) (VP (AUX is) (ADJP (JJ pointless) (PP (IN in) (S (VP (VBG asking) (NP (DT a) (NN politician)) (NP (DT a) (JJ direct) (NN question))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ understands) (SBAR (IN that) (S (NP (NNS politicians)) (VP (VBP lie))))) (CC but) (VP (VBZ holds) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NP (NNS words)) (PP (IN of) (NP (NNS politicians)))) (VP (VBP mean) (NP (NN nothing))))) (ADVP (RB then)) (NP (EX there)) (VP (AUX is) (NP (NP (JJ little) (NN point)) (PP (IN in) (S (VP (AUXG having) (NP (JJ political) (NN discourse))))))))))) (. .)))

(S1 (S (NP (JJ Political) (NN cynicism)) (VP (AUX is) (NP (NP (DT no) (NN excuse)) (SBAR (S (VP (TO to) (RB not) (VP (VB put) (NP (NNS expectations)) (PP (IN on) (NP (PRP$ our) (NNS candidates) (S (VP (TO to) (VP (AUX be) (ADJP (JJ honest) (PP (IN with) (NP (DT the) (JJ American) (NNS people))))))))))))))) (. .)))

