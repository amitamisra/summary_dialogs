(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NNS issues)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (JJ gay) (NNS people)) (VP (VBP make) (NP (DT a) (NN choice) (S (VP (TO to) (VP (VB engage) (PP (IN in) (NP (NP (JJ sexual) (NNS relations)) (PP (IN with) (NP (NP (NNS members)) (PP (IN of) (NP (DT the) (JJ same) (NN sex))))))))))))))) (. .)))

(S1 (S (S (S (NP (JJ S2) (NNS supports)) (VP (AUXG being) (ADJP (JJ gay)))) (VP (AUX is) (NP (NP (RB not) (DT a) (NN choice)) (SBAR (S (NP (NN someone)) (VP (MD can) (VP (VB make)))))))) (, ,) (CC but) (S (ADVP (RB instead)) (VP (AUX is) (SBAR (WHADVP (WRB how)) (S (NP (DT an) (NN individual)) (VP (AUX is) (VP (VBN born))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ poses) (NP (DT the) (NN question) (SBAR (IN that) (S (SBAR (IN if) (S (SBAR (WHNP (WP what)) (S (NP (NNP S1)) (VP (VBZ says)))) (VP (AUX is) (ADJP (JJ true))))) (, ,) (NP (RB then) (NN he/she)) (VP (MD must) (ADVP (RB also)) (VP (VB believe) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD can) (VP (VB choose) (NP (NP (DT the) (NN gender)) (PP (IN of) (NP (NP (DT those)) (SBAR (S (NP (PRP they)) (VP (AUX are) (ADVP (RB sexually)) (VP (VBN attracted) (PP (TO to))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (S (VP (VBG engaging) (PP (IN in) (NP (JJ gay) (NN sex))))) (VP (VP (AUX is) (NP (NP (DT a) (NN violation)) (PP (IN of) (NP (NP (NNP God) (POS 's)) (NN word))))) (CC and) (VP (AUX is) (PRN (: ;) (ADVP (RB therefore)) (, ,)) (NP (DT a) (NN sin))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ asks) (NP (NNP S1)) (SBAR (IN if) (S (NP (NN he/she)) (VP (VBZ believes) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (NP (JJR hisher) (NN theory)) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD should) (ADVP (RB then)) (VP (AUX be) (ADJP (JJ celibate)) (ADVP (RB forever)) (SBAR (RB so) (IN as) (S (VP (TO to) (VP (VP (RB not) (VB offend) (NP (NNP God))) (CC and) (`` ``) (NP (PRP$ his) (NN word)))))))))))))))))) (. .) ('' '')))

