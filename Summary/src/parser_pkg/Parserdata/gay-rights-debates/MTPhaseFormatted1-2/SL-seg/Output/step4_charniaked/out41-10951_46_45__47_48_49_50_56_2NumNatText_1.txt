(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NNS rights)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (JJ gay) (NN marriage))))) (SBAR (IN while) (S (NP (NNP S2)) (VP (VBZ supports) (NP (PRP it)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (JJ gay) (NNS rights)) (VP (MD would) (VP (VB impose) (NP (JJ gay) (NNS families)) (PP (IN onto) (NP (NP (JJ straight) (NNS members)) (PP (IN of) (NP (NN society))))) (SBAR (IN while) (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NP (NNP S2) (POS 's)) (NNS opinions)) (VP (AUX are) (ADJP (JJ comparable) (PP (TO to) (NP (NP (DT those)) (PP (IN of) (NP (NNP Taliban) (NNS terrorists))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX is) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (NP (NNS comments)) (VP (VBN made) (PP (IN by) (NP (NP (DT a) (JJ previous) (NN participant)) (, ,) (VP (VBG contending) (SBAR (S (NP (PRP he)) (VP (AUX did) (RB not) (VP (VB make) (NP (NP (NNS remarks)) (PP (VBN based) (PP (IN on) (NP (NP (JJ mandatory) (NN therapy)) (PP (IN for) (NP (NP (NNS gays)) (CC or) (NP (NP (NN anything)) (PP (IN of) (NP (DT the) (JJ like))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NP (DT the) (JJ previous) (NNS comments)) (VP (VBN made) (NP (NN center)) (PP (IN on) (NP (NP (NN religion)) (CC and) (NP (DT that) (JJ civil) (NN law) (CC and) (NN religion)))))) (VP (AUX are) (RB not) (ADJP (JJ synonymous)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (PRP$ his) (JJ religious) (NNS beliefs)) (VP (AUX are) (VP (VBN violated) (PP (IN by) (NP (NP (RB just) (DT the) (NN consideration)) (PP (IN of) (S (VP (VBG allowing) (NP (JJ gay) (NN marriage)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NN marriage)) (VP (AUX is) (NP (NP (DT a) (NN contract)) (PP (IN between) (NP (NP (DT a) (NN woman)) (CC and) (NP (NP (DT a) (NN man)) (PRN (, ,) (RB not) (NP (JJ same) (NN sex))))))))))) (. .)))

