<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN Vitter/NNP and/CC Craig/NNP should/MD be/AUX stones/NNS on/IN the/DT Capitol/NNP Steps/NNS as/RB well/RB as/IN the/DT people/NNS they/PRP slept/VBD with/IN ./. </C>
</S>
<S>
<C>This/DT person/NN states/VBZ a/DT hypocrisy/NN with/IN Fred/NNP Thompson/NNP ,/, </C>
<C>where/WRB he/PRP got/VBD his/PRP$ eighteen/CD year/NN old/JJ girlfriend/NN ,/, Sarah/NNP Lindsey/NNP ,/, pregnant/JJ which/WDT prompted/VBD an/DT immediate/JJ marriage/NN that/WDT lasted/VBD for/IN twenty-six/JJ years/NNS </C>
<C>before/IN a/DT divorce/NN was/AUX ordered/VBN ./. </C>
</S>
<S>
<C>After/IN remarrying/VBG ,/, </C>
<C>S1/NNP points/VBZ out/RP that/IN in/IN addition/NN to/TO three/CD children/NNS from/IN the/DT previous/JJ marriage/NN ,/, Thompson/NNP had/AUX two/CD more/JJR with/IN a/DT woman/NN who/WP was/AUX younger/JJR than/IN his/PRP$ oldest/JJS child/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN James/NNP Dobson/NNP does/AUX not/RB believe/VB Thompson/NNP to/TO be/AUX a/DT true/JJ Christian/JJ ./. </C>
</S>
<S>
<C>S2/NNP seems/VBZ to/TO partially/RB agree/VB with/IN S1/NNP ,/, </C>
<C>though/IN this/DT person/NN believes/VBZ that/IN a/DT stoning/NN would/MD result/VB in/IN the/DT arrest/NN of/IN those/DT who/WP perpetrated/VBN it/PRP on/IN drug/NN charges/NNS ./. </C>
</S>
</P>
</T>
