(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (ADJP (JJ uncomfortable) (PP (IN with) (NP (DT the) (NN study)))) (SBAR (IN because) (S (NP (EX there)) (VP (AUX is) (RB not) (NP (NP (NN information)) (PP (IN on) (SBAR (WHADVP (WRB how)) (S (NP (PRP they)) (VP (VBD got) (NP (DT the) (NNS results))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ goes) (ADVP (RB on)) (S (VP (TO to) (VP (VB say) (SBAR (S (NP (DT the) (NN study)) (VP (VBZ vilifies) (NP (NP (DT a) (NN group)) (PP (IN of) (NP (NP (NNS people)) (, ,) (RB namely) (NP (NNS Christians))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (JJS most) (NNS people)) (PP (IN in) (NP (DT the) (NN country)))) (VP (MD may) (VP (VB identify) (PP (IN as) (NP (NNP Christian)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB also)) (VP (VBZ thinks) (SBAR (S (NP (NP (DT the) (NN divorce) (NN rate)) (PP (IN of) (NP (NNS Christians)))) (VP (AUX has) (NP (NP (DT no) (NN bearing)) (PP (IN on) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NNP S1)) (VP (MD should) (VP (VP (VB ask) (NP (NP (NNS questions)) (PP (IN about) (NP (DT the) (NN study))))) (CONJP (RB rather) (IN than)) (ADVP (RB just)) (VP (VB assume) (SBAR (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ incorrect))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (VP (AUXG being) (VP (VBN used) (S (VP (TO to) (VP (VB vilify) (NP (NN anyone))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (S (NP (DT the) (NNS results)) (VP (AUX are) (ADJP (JJ professional)))) (CC and) (S (NP (NNP S1)) (VP (AUX is) (ADJP (RB just) (JJ uncomfortable) (PP (IN with) (NP (DT the) (NNS results))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NP (DT the) (NN divorce) (NN rate)) (PP (IN of) (NP (NNS Christians)))) (VP (AUX is) (ADJP (RB very) (JJ important) (PP (TO to) (NP (DT the) (NN issue)))) (SBAR (IN because) (S (NP (NNS Christians)) (VP (VBP say) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG protecting) (NP (NP (DT the) (`` ``) (NN sanctity) ('' '')) (PP (IN of) (NP (NN marriage))))))))))))))) (. .)))

