(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NP (DT any) (NN evidence)) (VP (VBG coming) (PP (IN from) (NP (NNP Cameron))))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN viewed) (PP (IN without) (NP (NN suspicion))) (PP (IN on) (NP (NNS grounds) (SBAR (IN that) (S (NP (PRP he)) (VP (VP (AUX has) (VP (AUX been) (VP (VBN kicked) (PRT (RP out)) (PP (IN of) (NP (DT the) (JJ legitimate) (JJ scientific) (NN community)))))) (CC and) (VP (AUX has) (VP (VBN gotten) (PP (IN in) (NP (NP (NN trouble)) (PP (IN with) (NP (NP (NNS judges) (CC and) (NNS scientists)) (SBAR (WHNP (WDT that)) (S (VP (ADVP (RB publicly)) (VB condemn) (NP (PRP him)) (PP (IN for) (S (VP (VBG misusing) (NP (PRP$ their) (NN research)))))))))))))))))))))))))) (. .)))

(S1 (SINV (S (NP (PRP They)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (SBAR (RB even) (IN if) (S (NP (PRP it)) (VP (AUX were) (ADJP (JJ true))))) (, ,) (NP (PRP it)) (VP (AUX does) (RB not) (VP (VB legitimize) (NP (NP (JJ anti-gay) (NN legislation)) (CC or) (NP (NN discrimination))))))) (CC and) (SBAR (IN that) (S (S (VP (AUXG being) (ADJP (JJ homosexual)))) (VP (AUX does) (RB not) (VP (VB hurt) (NP (NN anyone))))))))) (, ,) (CC nor) (MD should) (NP (PRP it)) (VP (AUX be) (VP (VBN counted) (PP (IN as) (NP (DT a) (NN disease))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNP HIV)) (VP (MD can) (VP (VB affect) (NP (NP (NN anyone)) (, ,) (CONJP (RB not) (RB just)) (NP (DT the) (JJ homosexual) (NN community)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (S (VP (VBG giving) (NP (NNS homosexuals)) (NP (NNS rights) (CC and) (NNS privileges)))) (VP (VBZ affects) (NP (NP (NN everyone)) (ADJP (RB else))) (ADVP (RB financially)))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NN something)) (VP (MD should) (VP (AUX be) (VP (AUX done) (SBAR (IN if) (S (NP (DT a) (JJ scientific) (NN study)) (VP (VBZ links) (NP (DT a) (JJ dangerous) (NN disease)) (PP (TO to) (NP (NP (DT the) (NNS activities)) (PP (IN of) (NP (NP (DT a) (JJ certain) (NN group)) (PP (IN of) (NP (NNS people)))))))))))))))) (. .)))

