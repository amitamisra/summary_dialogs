(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS subjects)) (VP (AUX are) (VP (VBG discussing) (NP (NP (NP (NN liberalism)) (CC and) (NP (PRP$ its) (NNS similarities))) (CC or) (NP (NP (NN lack)) (ADVP (RB thereof) (PP (TO to) (NP (DT the) (NNP Jeffersonian) (NN belief) (NN system)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (PRP they)) (VP (AUX are) (NP (NP (NN nothing)) (ADVP (RB alike))) (SBAR (IN while) (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (PRP they)) (VP (AUX have) (NP (JJ many) (NNS similarities)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (S (NP (NNP Jefferson)) (VP (VBD believed) (SBAR (S (NP (NN liberty)) (VP (MD would) (VP (AUX be) (VP (VBN threatened) (PP (IN by) (NP (DT a) (JJ centralized) (NN government)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ advises) (SBAR (S (NP (NN liberalism)) (VP (AUX is) (RB not) (VP (VBN based) (PP (IN on) (NP (DT the) (JJ same) (NN theory)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (NP (NP (DT the) (NN opposite)) (VP (VBG stating) (NP (DT the) (NN opinion) (SBAR (IN that) (S (NP (DT the) (NN group)) (VP (VBZ believes) (PP (IN in) (NP (NP (NN fairness)) (PP (IN for) (NP (NP (DT all)) (, ,) (PP (IN except) (NP (DT the) (JJ gay) (NN community)))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (S (NP (PRP he)) (VP (VBZ supports) (NP (NP (JJ equal) (NNS opportunities)) (PP (IN for) (NP (DT all) (NNS people)))) (PP (IN despite) (NP (PRP$ their) (JJ sexual) (NNS preferences)))))))) (: ;) (S (ADVP (RB however)) (, ,) (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB support) (NP (NP (DT the) (NN redefinition)) (PP (IN of) (NP (NN marriage) (S (VP (TO to) (VP (VB allow) (PP (IN for) (NP (JJ gay) (NNS unions)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (DT a) (JJ double) (NN standard)) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB basically)) (VP (VBG saying) (NP (NP (JJ equal) (NNS rights)) (PP (IN for) (NP (NP (DT all)) (SBAR (S (CC but) (NP (NNS gays)) (ADVP (RB still)) (VP (VBP cannot) (S (VP (VB marry))))))))))))))))) (. .)))

