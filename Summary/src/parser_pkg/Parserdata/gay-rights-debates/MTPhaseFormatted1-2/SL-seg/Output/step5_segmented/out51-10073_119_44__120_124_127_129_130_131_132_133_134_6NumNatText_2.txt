<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN gay/JJ marriages/NNS should/MD not/RB be/AUX viewed/VBN the/DT same/JJ way/NN as/IN heterosexual/JJ marriages/NNS ;/: </C>
<C>the/DT speaker/NN does/AUX state/VB that/IN he/PRP does/AUX not/RB disagree/VB with/IN the/DT idea/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN gay/JJ marriage/NN is/AUX a/DT deviation/NN from/IN the/DT traditional/JJ union/NN between/IN a/DT man/NN and/CC woman/NN ./. </C>
</S>
<S>
<C>Any/DT sexual/JJ relationship/NN that/WDT is/AUX a/DT deviation/NN from/IN the/DT ``/`` norm/NN ,/, ''/'' like/IN polygamist/NN and/CC incestuous/JJ relationships/NNS ,/, should/MD also/RB be/AUX granted/VBN the/DT same/JJ rights/NNS ./. </C>
</S>
<S>
<C>The/DT speaker/NN feels/VBZ that/IN these/DT relationships/NNS are/AUX all/DT on/IN the/DT same/JJ levels/NNS ,/, </C>
<C>and/CC one/PRP should/MD not/RB be/AUX afforded/VBN rights/NNS that/IN the/DT others/NNS are/AUX not/RB ./. </C>
</S>
<S>
<C>S2/NNP identifies/VBZ as/IN a/DT gay/JJ male/NN ,/, </C>
<C>who/WP has/AUX been/AUX in/IN a/DT 15-year-long/JJ homosexual/JJ relationship/NN ,/, </C>
<C>and/CC has/AUX a/DT set/NN of/IN twins/NNP due/RB soon/RB ;/: </C>
<C>he/PRP feels/VBZ his/PRP$ family/NN should/MD be/AUX granted/VBN the/DT same/JJ rights/NNS as/IN a/DT heterosexual/JJ ,/, married/JJ couple/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN legalizing/VBG gay/JJ marriage/NN allows/VBZ for/IN automatic/JJ guardianship/NN of/IN children/NNS </C>
<C>if/IN something/NN happens/VBZ to/TO the/DT biological/JJ parent/NN ,/, and/CC also/RB awards/NNS survivors/NNS benefits/NNS from/IN social/JJ security/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN other/JJ types/NNS of/IN marriage/NN should/MD be/AUX looked/VBN at/IN based/VBN on/IN its/PRP$ own/JJ situations/NNS and/CC conditions/NNS ./. </C>
</S>
</P>
</T>
