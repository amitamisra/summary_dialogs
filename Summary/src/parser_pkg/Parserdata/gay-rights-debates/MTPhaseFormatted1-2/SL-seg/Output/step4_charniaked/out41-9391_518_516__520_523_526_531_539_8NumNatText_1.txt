(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (NP (NP (JJR more) (NNS limits)) (PP (TO to) (NP (NN marriage))) (PP (IN than) (NP (RB just) (DT the) (NN limit))) (PP (TO to) (NP (JJ opposite) (NNS sexes))))))) (CC and) (SBAR (IN that) (S (SBAR (IN if) (S (NP (PRP you)) (VP (AUX do) (RB not) (VP (VB follow) (NP (DT the) (NNS rules)))))) (NP (PRP you)) (VP (AUX do) (RB not) (VP (VB qualify) (PP (IN for) (NP (NP (DT the) (NNS benefits)) (PP (IN of) (NP (NN marriage))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (SBAR (RB just) (IN because) (S (NP (EX there)) (VP (AUX are) (NP (NNS rules))))) (, ,) (NP (NN anyone)) (VP (AUX is) (VP (VBN barred) (PP (IN from) (NP (DT the) (NN custom))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX have) (NP (NP (DT the) (NN right)) (PP (TO to) (NP (DT the) (NNS changes))) (SBAR (S (NP (PRP they)) (VP (VBP want) (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (VBZ comes) (PP (TO to) (NP (NN marriage))) (SBAR (IN as) (S (NP (PRP they)) (VP (VBP argue) (SBAR (IN that) (S (NP (DT the) (NNS rules)) (VP (AUX are) (ADJP (JJ global) (CC and) (RB not) (JJ unequal)))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (NP (NP (JJ racial) (NN inequality)) (PP (IN between) (NP (NP (NNP African) (NNPS Americans)) (CC and) (NP (JJ white) (NNPS Americans)))) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (PRP they)) (ADVP (JJ cold)) (VP (RB not) (VB marry))))) (VP (AUX was) (ADJP (JJ unequal))))) (, ,) (CC but) (SBAR (IN that) (S (NP (DT the) (NN situation) (NN cannot)) (VP (AUX be) (VP (VBN compared) (PP (TO to) (NP (NNS homosexuals))) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ dissimilar))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (ADJP (JJ wrong))))) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NN argument)) (PP (IN for) (NP (NN equality)))) (VP (MD can) (VP (VB apply) (PP (TO to) (NP (NNS homosexuals))) (ADVP (ADVP (RB as) (RB well)) (, ,) (SBAR (IN since) (S (NP (JJ heterosexual) (NNS couples)) (VP (MD can) (VP (VB marry) (SBAR (IN while) (S (NP (JJ homosexual) (NNS couples)) (VP (VBP cannot))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (SBAR (RB just) (IN as) (S (NP (NP (NN race) (NNS criteria)) (PP (IN from) (NP (NN marriage)))) (VP (AUX has) (VP (AUX been) (VP (VBN removed) (PP (IN upon) (NP (NP (DT the) (NNS grounds)) (PP (IN of) (S (VP (AUXG being) (ADJP (JJ inappropriate) (CC and) (JJ discriminatory)))))))))))) (, ,) (NP (RB so)) (VP (MD should) (VP (VB gender)))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (PP (IN because) (IN of) (NP (DT the) (NNS rules))) (, ,) (NP (JJ heterosexual) (NNS couples)) (VP (AUX have) (VP (NNS benefits) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX do) (RB not))))))))) (. .)))

