<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN the/DT problem/NN with/IN an/DT unmentioned/JJ study/NN is/AUX that/IN the/DT majority/NN of/IN people/NNS identify/VBP as/IN Christian/NNP ,/, </C>
<C>but/CC he/PRP also/RB recognizes/VBZ S2/NNP 's/POS argument/NN that/IN more/JJR details/NNS of/IN the/DT study/NN would/MD be/AUX helpful/JJ to/TO determine/VB </C>
<C>whether/IN this/DT is/AUX actually/RB a/DT problem/NN ./. </C>
</S>
<S>
<C>S1/NNP also/RB claims/VBZ that/IN the/DT forum/NN tends/VBZ to/TO make/VB Christianity/NNP look/VB bad/JJ ,/, </C>
<C>and/CC points/VBZ out/RP that/IN the/DT divorce/NN rate/NN of/IN Christians/NNS is/AUX irrelevant/JJ to/TO the/DT gay/JJ marriage/NN amendment/NN ./. </C>
</S>
<S>
<C>S2/NNP defends/VBZ the/DT unmentioned/JJ study/NN </C>
<C>by/IN suggesting/VBG that/IN S1/NNP is/AUX unaware/JJ of/IN the/DT particular/JJ details/NNS of/IN the/DT study/NN which/WDT could/MD invalidate/VB his/PRP$ argument/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB suggests/VBZ that/IN S1/NNP dismissed/VBD the/DT results/NNS of/IN the/DT study/NN </C>
<C>without/IN trying/VBG to/TO understand/VB them/PRP ,/, </C>
<C>and/CC that/IN the/DT study/NN was/AUX in/IN fact/NN well/RB done/AUX </C>
<C>and/CC any/DT objections/NNS should/MD be/AUX based/VBN on/IN evidence/NN ./. </C>
</S>
</P>
</T>
