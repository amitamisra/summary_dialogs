<PARAGRAPH> S1 References S2 that marriage is a public institution limited by law.
This is subject to the will of the people expressed in a vote.
It's ok to lobby on either side.
S2 Calls for empathy and that people are denying gays what everyone else has.
S1 Claims it is ok for people to believe as they wish on the issue and can lobby on it, but not to belittle those who oppose it due any reason.
S2 Refers back to the empathy question and wishes S1 to imagine a reversed world where being heterosexual denied you marriage rights.
S1 Claims it's not injustice because it's not denying something that you're allowed to do and refers to society's right to determine its own rules.
S2 Refers to similarity between gay marriage and slavery, refers to empathy and claims cruelty to deny gay marriage.
S1 Denies similarity and degree between gay marriage and slavery, Again refers to society determining own rules.
S2 States not comparing the two issues just applying S1's beliefs and standards to slavery.
