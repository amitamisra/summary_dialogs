<T>
<P>
</P>
<P>
<S>
<C>S1/NNP expressed/VBD his/PRP$ opinion/NN that/IN his/PRP$ family/NN and/CC other/JJ heterosexual/NN families/NNS are/AUX the/DT norm/NN </C>
<C>and/CC that/IN homosexual/JJ families/NNS -LRB-/-LRB- like/IN S2/NNP 's/POS -RRB-/-RRB- deviate/VBP from/IN the/DT norm/NN ./. </C>
</S>
<S>
<C>He/PRP called/VBD such/JJ families/NNS ``/`` perverse/JJ ''/'' ./. </C>
</S>
<S>
<C>When/WRB this/DT comment/NN resulted/VBD in/IN a/DT challenge/NN from/IN S1/NNP </C>
<C>he/PRP denied/VBD hating/VBG gays/NNS ./. </C>
</S>
<S>
<C>He/PRP then/RB stated/VBD his/PRP$ opinion/NN that/IN that/DT marriage/NN should/MD not/RB be/AUX redefined/VBD for/IN gay/JJ people/NNS ../NNS ./. </C>
</S>
<S>
<C>He/PRP continued/VBD to/TO deny/VB his/PRP$ hate/NN for/IN gays/NNS ./. </C>
</S>
<S>
<C>S2/NNP stated/VBD his/PRP$ belief/NN that/IN deviations/NNS from/IN the/DT norm/NN are/AUX diverse/JJ ./. </C>
</S>
<S>
<C>He/PRP said/VBD that/IN he/PRP knew/VBD that/IN S2/NNP is/AUX not/RB against/IN all/DT diversity/NN </C>
<C>but/CC seems/VBZ to/TO be/AUX very/RB much/RB against/IN gays/NNS ./. </C>
</S>
<S>
<C>He/PRP bristled/VBD against/IN S1/NNP 's/POS definition/NN of/IN his/PRP$ family/NN as/IN ``/`` perverse/JJ ''/'' and/CC pointed/VBD at/IN the/DT use/NN of/IN that/DT word/NN </C>
<C>as/IN being/AUXG illustrative/JJ of/IN S1/NNP 's/POS hatred/NN of/IN gays/NNS ./. </C>
</S>
</P>
</T>
