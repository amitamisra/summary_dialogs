(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ makes) (NP (NP (DT a) (NN comment)) (PP (IN about) (NP (NNS gerbils))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (S (NP (NNS homosexuals)) (VP (AUX have) (VP (VBN created) (NP (NP (ADJP (JJ sick) (CC and) (JJ perverted)) (JJ sexual) (NNS practices)) (, ,) (PP (JJ such) (IN as) (NP (NP (DT those)) (VP (VBG involving) (NP (NNS gerbils))))))))) (, ,) (CC and) (S (NP (PRP they)) (VP (AUX are) (PP (IN in) (NP (DT a) (JJ dangerous) (JJ spiritual) (NN situation))) (PP (IN because) (IN of) (NP (PRP it)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ denies) (VP (VBG joking))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ laughs) (ADVP (RB sarcastically))) (CC and) (VP (VBZ questions) (SBAR (IN if) (S (NP (NN sex)) (VP (AUX is) (NP (NP (DT all)) (SBAR (S (NP (DT some) (NNS people)) (VP (VBP think) (PP (IN about))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (DT no) (NN one)) (VP (VBZ cares) (SBAR (WHNP (WP what)) (S (NP (NNS people)) (VP (AUX do) (PP (IN in) (NP (PRP$ their) (NNS bedrooms))) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (UCP (JJ private) (CC and) (RB not) (NP (NP (DT a) (NN reflection)) (PP (IN of) (SBAR (WHNP (WP who)) (S (NP (PRP they)) (VP (AUX are) (PP (IN as) (NP (DT a) (NN person))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNS people)) (VP (AUX do) (RB not) (VP (VB define) (NP (PRP$ their) (NN neighbor) (CC or) (NN mailman)) (PP (VBN based) (PP (IN on) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (AUX do) (PP (IN in) (NP (NP (PRP$ their) (NN bedroom)) (CC and) (NP (DT that)))) (SBAR (IN if) (S (NP (DT that)) (VP (AUX were) (NP (NP (DT the) (NN case)) (SBAR (S (NP (EX there)) (VP (MD could) (VP (AUX be) (NP (NP (JJ many) (JJ negative) (NNS comments)) (VP (VBN made) (PP (IN about) (NP (NNP S1))))))))))))))))))))))) (. .)))

(S1 (S (ADVP (RB Also)) (, ,) (NP (NNP S2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS heterosexuals)) (VP (AUX have) (VP (AUX been) (VP (VBN known) (S (VP (TO to) (VP (AUX have) (VP (VBN messed) (PRT (RP up)) (NP (JJ sexual) (NNS practices)))))) (ADVP (RB as) (RB well)))))))) (. .)))

