<T>
<P>
</P>
<P>
<S>
<C>S1/NNP makes/VBZ the/DT statement/NN ,/, '/'' It/PRP 's/AUX nothing/NN like/IN racial/JJ segregation/NN '/POS ../NNS ./. </C>
</S>
<S>
<C>When/WRB questioned/VBN about/IN this/DT statement/NN </C>
<C>the/DT speaker/NN begins/VBZ repeatedly/RB asking/VBG what/WP it/PRP was/AUX that/IN he/PRP had/AUX contended/VBN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN the/DT other/JJ speaker/NN has/AUX no/DT idea/NN what/WP he/PRP meant/VBN by/IN his/PRP$ statement/NN </C>
<C>and/CC that/IN he/PRP is/AUX just/RB making/VBG unfounded/JJ assumptions/NNS ./. </C>
</S>
<S>
<C>He/PRP insists/VBZ that/IN he/PRP made/VBD no/DT position/NN about/IN what/WP segregation/NN means/VBZ definitely/RB ./. </C>
</S>
<S>
<C>He/PRP insists/VBZ further/RBR that/IN he/PRP did/AUX not/RB imply/VB the/DT plight/NN of/IN blacks/NNS and/CC gays/NNS were/AUX dissimilar/JJ ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ with/IN S1/NNP about/IN what/WP he/PRP believes/VBZ the/DT speaker/NN actually/RB meant/VBD from/IN his/PRP$ statement/NN ./. </C>
</S>
<S>
<C>His/PRP$ first/JJ point/NN is/AUX adamantly/RB made/VBN ,/, </C>
<C>that/IN the/DT segregation/NN of/IN blacks/NNS is/AUX in/IN actuality/NN just/RB like/IN the/DT segregation/NN that/WDT is/AUX faced/VBN by/IN homosexuals/NNS ./. </C>
</S>
<S>
<C>He/PRP insists/VBZ that/IN the/DT prejudice/NN that/IN they/PRP face/VBP is/AUX just/RB like/IN the/DT previous/JJ prejudices/NNS ,/, </C>
<C>that/WDT in/IN their/PRP$ time/NN were/AUX also/RB seen/VBN as/IN as/RB '/'' different/JJ '/'' ./. </C>
</S>
<S>
<C>He/PRP also/RB claims/VBZ that/IN the/DT statement/NN implies/VBZ that/IN if/IN the/DT segregation/NN of/IN blacks/NNS were/AUX imposed/VBN on/IN homosexuals/NNS ,/, </C>
<C>that/IN it/PRP would/MD not/RB be/AUX less/RBR wrong/JJ ./. </C>
</S>
<S>
<C>He/PRP concedes/VBZ his/PRP$ assumption/NN was/AUX made/VBN in/IN error/NN </C>
<C>yet/CC he/PRP has/AUX discredited/VBN the/DT speaker/NN ./. </C>
</S>
</P>
</T>
