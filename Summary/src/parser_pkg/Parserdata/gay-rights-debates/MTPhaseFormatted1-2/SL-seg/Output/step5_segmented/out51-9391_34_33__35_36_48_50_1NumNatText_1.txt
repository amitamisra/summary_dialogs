<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN the/DT research/NN demonstrating/VBG shorter/JJR lifespans/NNS for/IN homosexuals/NNS has/AUX been/AUX discredited/VBN ,/, </C>
<C>and/CC that/IN even/RB if/IN it/PRP were/AUX true/JJ ,/, </C>
<C>it/PRP would/MD not/RB make/VB anti-gay/JJ discrimination/NN acceptable/JJ ./. </C>
</S>
<S>
<C>He/PRP rejects/VBZ the/DT analogy/NN proposed/VBN by/IN S2/NNP where/WRB a/DT hypothetical/JJ disease/NN is/AUX contracted/VBN by/IN a/DT small/JJ population/NN due/JJ to/TO their/PRP$ chosen/VBN lifestyle/NN ,/, </C>
<C>arguing/VBG that/IN homosexuality/NN is/AUX not/RB a/DT disease/NN ./. </C>
</S>
<S>
<C>He/PRP explains/VBZ that/IN anyone/NN is/AUX capable/JJ of/IN contracting/VBG HIV/NNP </C>
<C>and/CC that/IN the/DT virus/NN is/AUX spread/VBN by/IN promiscuity/NN and/CC drug/NN use/NN ,/, </C>
<C>meaning/VBG that/IN it/PRP is/AUX not/RB exclusive/JJ to/TO homosexuals/NNS ./. </C>
</S>
<S>
<C>S2/NNP reiterates/VBZ his/PRP$ assertion/NN that/IN legalizing/VBG same-sex/JJ marriage/NN affects/VBZ the/DT rest/NN of/IN the/DT population/NN financially/RB ./. </C>
</S>
<S>
<C>He/PRP then/RB attacks/VBZ S1/NNP 's/POS character/NN ,/, </C>
<C>claiming/VBG that/IN the/DT strength/NN of/IN his/PRP$ own/JJ position/NN has/AUX caused/VBN S1/NNP to/TO deflect/VB the/DT obviously/RB accurate/JJ assertion/NN that/IN S2/NNP has/AUX proposed/VBN ./. </C>
</S>
</P>
</T>
