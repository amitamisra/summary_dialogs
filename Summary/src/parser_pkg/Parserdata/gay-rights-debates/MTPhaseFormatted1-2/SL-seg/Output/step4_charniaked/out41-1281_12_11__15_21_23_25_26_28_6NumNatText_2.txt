(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (JJ polygamous) (NN marriage)) (VP (VP (AUX is) (RB not) (VP (VBN recognized) (PP (IN by) (NP (NP (NNS governments)) (PP (IN in) (NP (JJ Western) (NNS societies))))))) (CC and) (VP (AUX is) (ADJP (JJ unlikely) (S (VP (TO to) (VP (AUX be) (VP (VBN recognized) (PP (IN in) (NP (NN future))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ points) (PP (TO to) (NP (DT the) (NN trend) (S (VP (TO to) (VP (VB legalize) (NP (NP (NP (JJ gay) (NN marriage)) (PP (IN in) (NP (NP (JJ western) (NNS countries)) (PP (JJ such) (IN as) (NP (NP (NNP Netherlands)) (, ,) (NP (NNP Belgium)) (, ,)))))) (CC and) (NP (NP (JJS most)) (PP (IN of) (NP (NNP Canada))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ points) (PP (TO to) (NP (NP (JJ high) (NN support)) (PP (IN for) (NP (JJ gay) (NNS marriages))) (PP (IN among) (NP (NP (DT the) (CD 18-29) (NNS year-olds)) (PP (IN in) (NP (DT the) (NNP US)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT these)) (VP (VBP show) (SBAR (IN that) (S (S (VP (VBG legalizing) (NP (JJ gay) (NN marriage)))) (VP (MD may) (VP (VB happen) (PP (IN in) (NP (DT the) (NN future))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ asserts) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX is) (VP (VBG recognizing) (NP (NP (DT a) (NN family) (NN unit)) (VP (VBN based) (PP (IN on) (NP (DT a) (JJ natural) (JJ heterosexual) (NN contract))))) (PRT (RP out))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (SBAR (RB even) (IN though) (S (NP (DT some) (JJ European) (NNS countries)) (VP (VBP allow) (NP (JJ gay) (NN marriage))))) (, ,) (NP (NP (CD 90) (NN %)) (PP (IN of) (NP (DT the) (NN world)))) (VP (VBZ bans) (NP (JJ gay) (NNS marriages)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (NNP US)) (VP (VP (AUX is) (RB not) (NP (NNP Europe))) (CC and) (VP (AUX is) (ADJP (JJ unlikely) (S (VP (TO to) (VP (VB move) (PP (IN in) (NP (DT the) (JJ same) (NN direction))) (PP (IN on) (S (VP (VBG legalizing) (ADJP (JJ gay)))))))))))))) (. .)))

