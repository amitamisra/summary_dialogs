(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX has) (VP (VBN posted) (NP (NP (NN news)) (PP (IN of) (NP (NP (DT the) (NNP DC) (NNP Council)) (VP (VBG voting) (S (VP (TO to) (VP (VB authorize) (NP (JJ gay) (NN marriage))))))))))) (CC and) (VP (AUX is) (ADJP (JJ celebratory) (PP (IN about) (NP (PRP it)))))) (. .)))

(S1 (S (NP (NP (NNP S2)) (PRN (-LRB- -LRB-) (NP (NNP JP)) (-RRB- -RRB-))) (VP (VBZ asserts) (SBAR (SBAR (IN that) (S (NP (DT the) (NNS politicians)) (VP (MD will) (VP (VB pay) (NP (DT a) (NN price)))))) (CC and) (SBAR (IN that) (S (NP (DT the) (NN matter)) (VP (MD will) (VP (AUX be) (VP (VBN overturned) (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (AUX is) (VP (VBN put) (PP (TO to) (NP (DT a) (NN vote)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT a) (NN majority)) (PP (IN of) (NP (NP (NNS people)) (, ,) (PP (VBG including) (NP (PRP himself))) (, ,)))) (VP (MD would) (VP (VB support) (NP (NP (NNS laws)) (VP (VBG providing) (NP (NNS rights) (CC and) (NNS benefits)) (PP (IN for) (NP (NP (NNS gays)) (SBAR (WHNP (WP who)) (S (VP (VBP elect) (S (VP (TO to) (VP (VB enter) (PP (IN into) (NP (NP (JJ civil) (NNS unions)) (, ,) (CONJP (CC but) (RB not)) (NP (JJ gay) (NN marriage))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ asserts) (SBAR (S (NP (DT no) (NN electorate)) (VP (MD would) (VP (VB vote) (PP (IN for) (NP (JJ gay) (NN marriage)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ refutes) (NP (DT the) (NN suggestion)) (PP (IN by) (S (VP (VBG noting) (SBAR (IN that) (S (NP (CD 5) (NNS States)) (VP (AUX had) (ADVP (RB already)) (VP (VBN approved) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ notes) (SBAR (IN that) (S (NP (JJ gay) (NN marriage) (NNS laws)) (VP (MD would) (RB not) (VP (VB force) (S (NP (NNS heterosexuals)) (VP (TO to) (VP (VP (VB enter) (PP (IN into) (NP (PRP them)))) (CC or) (VP (VB attend) (NP (JJ gay) (NNS weddings))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB seemingly)) (VP (VBZ corrects) (NP (NNP S1)) (PP (IN by) (S (VP (VBG noting) (SBAR (IN that) (S (NP (NP (DT the) (NNS States)) (PP (IN in) (NP (NN question)))) (VP (AUX had) (RB not) (VP (VBN put) (NP (DT the) (NN issue)) (PP (TO to) (NP (DT the) (JJ general) (NN public))) (PP (IN for) (NP (DT a) (NN vote))))))))))) (. .)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP he)) (VP (MD would) (VP (AUX be) (ADJP (JJ happy) (PP (IN with) (NP (JJ civil) (NN union) (NNS laws)))))))))) (, ,) (CC and) (S (NP (NP (DT each)) (PP (IN of) (NP (PRP them)))) (VP (VBP agree) (SBAR (SBAR (IN that) (S (NP (DT the) (JJ gay) (NN community)) (VP (AUX has) (NP (DT a) (NNP PR) (NN problem))))) (CC and) (SBAR (IN that) (S (NP (JJ many) (NNS homosexuals)) (VP (AUX are) (ADVP (RB actually)) (ADJP (JJ boring) (CC and) (JJ inconspicuous)))))))) (. .)))

