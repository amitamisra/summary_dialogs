<s> <PARAGRAPH> </s>
<s>  S1 challenges S2 to debate the cause of the spread of AIDS in the United States and encourages S2 to attempt to challenge his claims with proof that they are untrue. </s>
<s> He accuses S2 of attempting to change the debate and making personal attacks rather than addressing the question at hand. </s>
<s> He suggests that if the debate occurs, he will prove his assertions true. </s>
<s> S2 claims that the origin of AIDS in the United States is unknown and that the spread of AIDS was caused by unclean needles, unprotected sex, and contaminated blood transfusions. </s>
<s> He rejects the requirement that he should provide proof for his claims by citing S1's failure to do so. </s>
<s> He suggests that once S1 begins to debate appropriately, then he will too. </s>
<s>  </s>
