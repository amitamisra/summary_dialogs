<PARAGRAPH> S1 believes that the best way to legalize same sex marriage is through lobbying.
He thinks that in any discrepancy both sides should lobby and the one who has the majority support should win.
Society is run by the majority opinion and he agrees that this is how it should be.
He agrees that same sex marriage should be legal but that the only way it can and should happen is when the majority believes it, even if roles were reversed and his relationship was not able to be acknowledged as a marriage.
When S2 compares marriage inequality to slavery he disagree that the comparison is valid but continues his argument for lobbying citing Wilberforce in his fight to abolish slavery.
S2 attempts to appeal to S1's emotions by repeatedly asking that he put himself in the shoes of those who are being denied the right to marriage.
He questions S1's ability to feel empathy and compares the issue of marriage equality to slavery in regards to the majority allowing it to exist.
