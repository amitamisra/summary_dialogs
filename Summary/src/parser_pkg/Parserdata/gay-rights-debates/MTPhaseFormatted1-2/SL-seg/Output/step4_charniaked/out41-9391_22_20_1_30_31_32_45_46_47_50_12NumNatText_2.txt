(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ civil) (NNS unions) (CC and) (NNS marriages)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ favors) (NP (NP (NP (JJ civil) (NNS unions)) (PP (IN for) (NP (NNS homosexuals)))) (CONJP (CC but) (RB not)) (NP (NN marriage))) (, ,) (SBAR (IN since) (S (NP (PRP he)) (VP (VP (AUX is) (ADJP (JJ religious))) (CC and) (VP (VBZ views) (NP (NN marriage)) (PP (IN as) (NP (DT a) (NN sacrament))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (MD can) (VP (VB vote) (S (VP (VBG using) (SBAR (WHNP (WDT whatever) (NNS criteria)) (S (NP (PRP he)) (VP (VBZ feels)))))))))) (, ,) (CC and) (SBAR (IN though) (S (NP (DT the) (NN government)) (VP (MD can) (RB not) (VP (VP (VB establish) (NP (NN religion))) (CC or) (VP (VB give) (NP (JJ special) (NN consideration)) (PP (TO to) (NP (JJ certain) (NNS groups))) (PP (IN as) (NP (NP (DT a) (NN voter)) (SBAR (S (NP (PRP he)) (VP (MD can))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (JJ civil) (NNS unions)) (VP (AUX are) (RB not) (ADJP (JJ equivalent) (PP (TO to) (NP (NNS marriages))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (DT the) (NNP US) (NN government)) (VP (AUX does) (RB not) (VP (VB consider) (S (VP (NN marriage) (NP (DT a) (NN sacrament))))))) (CC and) (S (NP (PRP he)) (VP (VBZ states) (SBAR (S (NP (NNP S1)) (VP (AUX is) (S (VP (VBG mixing) (NP (NN religion) (CC and) (NN government)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (NN majority)) (VP (MD should) (RB not) (VP (VB vote) (PP (IN on) (NP (NP (DT the) (JJ civil) (NNS rights)) (PP (IN of) (NP (NP (DT another) (NN group)) (SBAR (WHNP (WDT that)) (S (VP (MD will) (RB not) (VP (VB affect) (NP (PRP them)))))))) (, ,) (SBAR (WHNP (WDT which)) (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (ADVP (RB financially)) (NP (PRP it)) (VP (MD will)))))))))))))) (. .)))

