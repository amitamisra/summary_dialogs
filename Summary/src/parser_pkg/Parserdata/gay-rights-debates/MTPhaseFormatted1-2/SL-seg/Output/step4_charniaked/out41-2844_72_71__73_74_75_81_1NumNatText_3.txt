(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (IN in) (NP (NP (NN favor)) (PP (IN of) (NP (NP (NN marriage)) (PP (IN between) (NP (JJ homosexual) (NNS couples)))))))) (. .)))

(S1 (S (PP (IN Within) (NP (PRP$ his) (NN debate))) (NP (PRP he)) (VP (VBZ brings) (PRT (RP up)) (NP (NP (DT the) (NNP Republican) (NN use)) (PP (IN of) (NP (`` ``) (JJ hot-button) ('' '') (NNS issues)))) (, ,) (S (VP (VBG insinuating) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (NP (DT an) (NN issue)) (VP (VBN used) (S (VP (TO to) (VP (VB divide) (CC and) (VB distract) (PP (IN from) (NP (JJ other) (`` ``) (NP (NN bread) (CC and) (NN butter)) ('' '') (NNS issues))) (, ,) (S (VP (VBG mentioning) (NP (NN tax) (NNS breaks)) (PP (IN for) (NP (DT the) (JJ rich) (CC and) (JJ global) (NN warming)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ disagrees) (PP (IN with) (NP (NP (NNS decisions)) (VP (AUXG being) (VP (VBN made))) (SBAR (WHNP (WDT that)) (S (VP (VBP affect) (NP (NP (JJ private) (NNS affairs)) (PP (VBG including) (NP (JJ gay) (NN marriage)))))))))) (CC and) (VP (ADVP (RB also)) (VBZ mentions) (NP (NN abortion)))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ brings) (PRT (RP up)) (NP (NP (DT the) (NN issue)) (PP (IN of) (S (NP (NN morality)) (VP (AUXG being) (NP (NP (DT a) (NN part)) (PP (IN of) (NP (NP (DT the) (NN conversation)) (SBAR (WHADVP (WRB when)) (S (NP (NP (CD one) (POS 's)) (NN morality) (CC and) (NNS beliefs)) (VP (AUX is) (RB not) (NP (NN everyone) (RB else) (POS 's))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (ADJP (JJ wrong)) (PP (IN because) (IN of) (NP (NP (DT the) (JJ traditional) (NN role)) (PP (IN of) (NP (NN marriage))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB care) (ADVP (RB much)) (PP (IN about) (SBAR (WHNP (WP what)) (S (NP (NNS people)) (VP (VP (AUX do) (PP (IN in) (NP (PRP$ their) (JJ own) (NN time)))) (CC but) (VP (VBZ draws) (NP (DT the) (NN line)) (PP (IN at) (S (VP (VBG allowing) (NP (JJ same) (NN sex) (NN marriage))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NP (NNS democrats)) (CC and) (NP (DT the) (JJ left))) (PP (IN of) (S (VP (VBG pushing) (NP (DT a) (JJ radical) (NN agenda)))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB specifically)) (VP (VBZ accuses) (NP (DT the) (JJ left)) (PP (IN of) (S (VP (VBG manipulating) (NP (DT the) (NN system)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ ends) (PP (IN with) (S (VP (VBG defending) (NP (NP (NNS states) (POS ')) (NNS rights) (S (VP (TO to) (RB not) (VP (VB acknowledge) (NP (JJ gay) (NN marriage)) (S (VP (VBG comparing) (NP (PRP it)) (PP (TO to) (NP (NP (DT the) (NN right)) (PP (IN of) (NP (NP (NNS states)) (SBAR (S (VP (TO to) (RB not) (VP (VB acknowledge) (NP (NP (DT a) (NN driver) (POS 's)) (NN licence)) (SBAR (IN if) (FRAG (RB not) (ADJP (JJ fitting) (PP (IN into) (NP (PRP$ their) (NNS requirements)))))))))))))))))))))))) (. .)))

