(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ inappropriate) (S (VP (TO to) (VP (VB compare) (NP (JJ opposing) (NN homosexuality)) (PP (IN with) (NP (JJ racial) (NN discrimination))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ explains) (NP (DT the) (NN difference) (SBAR (SBAR (IN that) (S (NP (JJ racial) (NN discrimination)) (VP (AUX is) (NP (NP (DT the) (NN fear)) (PP (IN of) (NP (DT a) (JJ particular) (NN person))))))) (CC and) (SBAR (IN that) (S (S (VP (VBG opposing) (NP (NN homosexuality)))) (VP (AUX is) (VP (VBN based) (PP (IN on) (NP (NP (NN disapproval)) (PP (IN of) (NP (NP (DT a) (NN person) (POS 's)) (NN behavior)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ proposes) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (JJ homosexual)) (VP (AUX were) (VP (VBN fired) (PP (IN from) (NP (PRP$ their) (NN job))))))) (, ,) (NP (PRP it)) (VP (MD would) (VP (AUX be) (UCP (ADJP (JJ due) (PP (TO to) (NP (PRP$ their) (NN incompetence)))) (CC and) (RB not) (ADJP (JJ due) (PP (TO to) (NP (PRP$ their) (NN homosexuality)))))))))) (. .)))

(S1 (S (NP (NP (CD S2) (NNS objects)) (TO to) (NP (NNP S1) (POS 's))) (VP (VBP claim) (SBAR (IN that) (S (NP (NN homophobia)) (VP (AUX is) (NP (NP (NN disapproval)) (PP (IN of) (NP (NN behavior))) (, ,) (VP (VBG questioning) (SBAR (WHNP (WDT which)) (S (NP (NNS behaviors)) (VP (VBP warrant) (NP (NN disapproval))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (S (VP (AUXG being) (ADJP (JJ homosexual)))) (VP (AUX is) (PP (IN in) (NP (PRP itself) (RB not) (NP (DT a) (NN behavior)))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (ADVP (RB frequently)) (VP (ADVP (RB legally)) (VBN fired) (PP (IN for) (NP (PRP$ their) (JJ sexual) (NN preference))) (PP (IN with) (NP (NP (DT no) (NN regard)) (PP (IN for) (NP (NN competence)))))))))) (. .)))

(S1 (S (ADVP (RB Finally)) (, ,) (NP (PRP he)) (VP (VBZ asks) (SBAR (WHNP (WP what)) (S (S (NP (DT a) (JJ gay) (NN activist)) (VP (AUX is))) (CC and) (S (NP (NNP S1)) (VP (VBZ responds) (PP (IN by) (S (VP (VBG recommending) (S (NP (NNP S2)) (VP (VB google) (NP (DT the) (NN term)))))))))))) (. .)))

