<s> <PARAGRAPH> </s>
<s>  S1 argues that heterosexual marriage is one of the most universal of all human institutions, found in almost all cultures that have ever existed. </s>
<s> They believe that the true question is proposing a justification in revolutionizing that ancient institution. </s>
<s> They believe that there are groups of homosexuals who want to change marriage for various reasons, whether it be for romance, economic and legal reasons but also to affirm that homosexuality and heterosexuality are just as good as each other. </s>
<s> They warn that there is a risk of backlash with this way of thinking. </s>
<s> They believe that homosexuality is an eccentricity and is not equal in value to heterosexuality. </s>
<s> S2 does not believe that homosexuals are attempting to destroy the idea of marriage but only wish to be included into it. </s>
<s> They believe that homosexuals should be as accepted socially as heterosexuals and find it difficult to see a problem with homosexuals believing and acting as equals to heterosexuals. </s>
<s> They believe that homosexuals want to have a positive influence on the institution of marriage, not destroy it. </s>
<s>  </s>
