<PARAGRAPH> S1: Opposers to gay marriage often jump to conclusions about who can get married -why not cousins, group marriages, children, dogs?
Polygamous marriage raises legal concerns that are not currently covered in traditional marriage laws, for example.
The goal of gay marriage supporters is to be able to get married - not to broaden marriage.
Broadened marriage is a side effect of legal gay marriage.
S2: Gay marriage supporters want marriage laws changed to accommodate gay marriage.
Broadened marriage could change the current traditional marriage laws.
Polygamy, for example, would require more marriage laws changed than gay marriage would, as more people are involved.
But if enough people wanted legal polygamous marriage, the laws would be passed.
Marriage would be broadened if gay marriage is made legal, whether that is the goal of gay marriage or not.
