<T>
<P>
</P>
<P>
<S>
<C>S1/NNP agrees/VBZ that/IN when/WRB a/DT husband/NN is/AUX part/NN of/IN a/DT heterosexual/JJ relationship/NN a/DT gay/JJ husband/NN is/AUX not/RB legally/RB recognized/VBN in/IN the/DT same/JJ category/NN ./. </C>
</S>
<S>
<C>His/PRP$ statistics/NNS support/VBP the/DT fact/NN that/IN 60,000/CD gay/JJ couples/NNS would/MD get/VB married/VBN </C>
<C>if/IN they/PRP were/AUX allowed/VBN ./. </C>
</S>
<S>
<C>The/DT equal/JJ rights/NNS are/AUX not/RB there/RB when/WRB it/PRP comes/VBZ to/TO same/JJ sex/NN marriage/NN ./. </C>
</S>
<S>
<C>Welfare/NNP is/AUX referenced/VBN </C>
<C>when/WRB making/VBG a/DT point/NN about/IN political/JJ muscle/NN ./. </C>
</S>
<S>
<C>American/JJ freedom/NN never/RB existed/VBD </C>
<C>and/CC no/DT one/PRP can/MD not/RB argue/VB that/IN FDR/NNP 's/AUX public/JJ opinion/NN on/IN the/DT role/NN on/IN the/DT federal/JJ government/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ that/IN it/PRP is/AUX a/DT problem/NN </C>
<C>because/IN same/JJ sex/NN marriage/NN has/AUX never/RB been/AUX legally/RB recognizes/VBZ is/AUX a/DT big/JJ problem/NN ./. </C>
</S>
<S>
<C>The/DT debate/NN is/AUX more/JJR for/IN straight/JJ couples/NNS than/IN it/PRP is/AUX about/IN gay/JJ relationships/NNS ./. </C>
</S>
<S>
<C>He/PRP notes/VBZ that/IN there/EX are/AUX logical/JJ reasons/NNS for/IN marriage/NN discrimination/NN ./. </C>
</S>
</P>
</T>
