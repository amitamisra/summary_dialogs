(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NNS rights)))) (. .)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX is) (NP (NP (DT a) (NN contract)) (PP (IN between) (NP (NP (DT the) (NN state)) (CC and) (NP (NP (DT the) (CD two) (NNS people)) (VP (VBN involved))))))))))) (CC and) (S (NP (PRP he)) (VP (VBZ finds) (NP (NN irony)) (SBAR (IN that) (S (NP (DT the) (NNS Christians)) (VP (AUX are) (ADJP (VBN opposed) (PP (TO to) (NP (NP (JJ gay) (JJ religious) (NN marriage)) (SBAR (WHADVP (WRB when)) (S (S (NP (DT the) (JJ civil) (NN part)) (VP (AUX does) (RB not) (VP (VB affect) (NP (PRP them))))) (, ,) (CC but) (S (NP (NNS gays)) (VP (MD can) (VP (AUX have) (NP (NP (DT a) (JJ religious) (NN marriage)) (CONJP (CC but) (RB not)) (NP (NP (DT a) (NN contract)) (PP (IN with) (NP (DT the) (NN state)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NP (NN someone) (POS 's)) (NN marriage)) (VP (AUX is) (NP (PRP$ their) (JJ own) (NN business))))) (, ,) (CC and) (SBAR (RB just) (IN as) (S (NP (PRP he)) (VP (AUX is) (VP (VBN forced) (S (VP (TO to) (VP (VB accept) (NP (JJ heterosexual) (NNS marriages))))) (SBAR (WHADVP (WRB why)) (S (MD should) (NP (RB not) (NNS others)) (VP (VBP accept) (NP (JJ homosexual) (NNS marriages))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (IN that) (S (SBAR (IN since) (S (NP (NN everyone)) (VP (VBZ lives) (PP (IN in) (NP (DT the) (NN society)))))) (, ,) (NP (PRP$ its) (NN policy)) (VP (AUX is) (NP (JJ everyones) (NN business)))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ heterosexual) (NNS marriages)) (VP (AUX is) (SBAR (S (NP (NP (DT the) (FW status) (FW quo) (, ,) (CC and) (NP (DT the) (NNP challenger))) (PP (TO to) (NP (DT that)))) (VP (AUX is) (NP (NP (DT the) (NN one)) (SBAR (WHNP (WP who)) (S (VP (AUX has) (S (VP (TO to) (VP (VB justify) (NP (DT the) (NNS changes)))))))))))))))) (. .)))

