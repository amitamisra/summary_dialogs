(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (S (VP (VBG legalizing) (NP (JJ homosexual) (NN marriage)))) (VP (MD would) (ADVP (RB eventually)) (VP (VB lead) (PP (TO to) (NP (NP (DT the) (NN legalization)) (PP (IN of) (NP (JJ polygamous) (NN marriage))))) (, ,) (S (VP (VBG citing) (NP (NP (DT a) (VBN proposed) (JJ constitutional) (NN amendment)) (SBAR (WHNP (WDT that)) (S (VP (VBZ bans) (NP (DT both) (JJ gay) (NN marriage) (CC and) (NN polygamy))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (DT a) (NN passage)) (PP (IN from) (NP (NP (DT the) (NNP Bible)) (SBAR (WHADVP (WRB where)) (S (NP (NNP Jesus)) (VP (VBD said) (SBAR (S (NP (DT a) (NN man)) (VP (MD should) (VP (AUX be) (VP (VBN cleaved) (PP (TO to) (NP (PRP$ his) (NN wife))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NP (NNS people)) (PP (IN on) (NP (DT the) (JJ right) (NN wing)))) (VP (VBP support) (NP (NP (DT a) (NN ban)) (PP (IN on) (NP (JJ gay) (NN marriage))))))) (, ,) (NP (RB then) (DT the) (JJ left) (NN wing)) (VP (MD must) (VP (VB oppose) (NP (NP (DT a) (NN ban)) (PP (IN on) (NP (JJ gay) (NN marriage))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ cites) (NP (NP (NNS instances)) (PP (IN of) (NP (NNP God))) (VP (VBG supporting) (NP (NP (NN polygamy)) (PP (IN in) (NP (DT the) (NNP Bible))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (PRP we)) (ADVP (RB already)) (VP (VB practice) (NP (JJ serial) (NN polygamy)) (PP (IN because) (IN of) (NP (NP (NNP America) (POS 's)) (JJ liberal) (NN divorce) (NNS laws))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ points) (PRT (RP out)) (NP (NP (DT the) (JJ logical) (NN fallacy)) (PP (IN in) (NP (NP (NNP S1) (POS 's)) (NN argument))) (PP (IN about) (NP (DT the) (JJ left) (CC and) (JJ right) (NN wing))) (, ,) (VP (VBG stating) (SBAR (IN that) (S (NP (NP (CD one) (NN wing)) (VP (VBG supporting) (NP (NN something)))) (VP (AUX does) (RB not) (VP (VB mean) (SBAR (S (NP (DT the) (JJ other) (NN wing)) (VP (VBZ opposes) (NP (PRP it)))))))))))) (. .)))

