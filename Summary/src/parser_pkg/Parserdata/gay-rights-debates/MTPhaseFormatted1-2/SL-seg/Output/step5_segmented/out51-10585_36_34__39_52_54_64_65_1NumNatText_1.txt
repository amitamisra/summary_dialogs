<T>
<P>
</P>
<P>
<S>
<C>S1/NNP asserts/VBZ that/IN the/DT characters/NNS Bert/NNP and/CC Ernie/NNP from/IN Sesame/NNP Street/NNP at/IN not/RB homosexual/JJ partners/NNS ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN homosexuals/NNS cannot/VBP understand/VB how/WRB two/CD men/NNS can/MD be/AUX close/JJ without/IN engaging/VBG in/IN homosexual/JJ acts/NNS ./. </C>
</S>
<S>
<C>He/PRP compares/VBZ using/VBG Bert/NNP and/CC Ernie/NNP as/IN homosexual/JJ icons/NNS to/TO using/VBG Kermit/NNP and/CC Miss/NNP Piggy/NNP as/IN a/DT symbol/NN for/IN interspecies/NNS relationships/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN it/PRP is/AUX unfair/JJ for/IN homosexuals/NNS to/TO claim/VB that/IN they/PRP are/AUX not/RB sexual/JJ deviants/NNS when/WRB they/PRP are/AUX projecting/VBG deviant/JJ sex/NN onto/IN children/NNS 's/POS programs/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN homosexuals/NNS engage/VBP in/IN deviant/JJ sexual/JJ practices/NNS </C>
<C>and/CC that/IN they/PRP are/AUX attempting/VBG to/TO project/VB these/DT behaviors/NNS onto/IN Bert/NNP and/CC Ernie/NNP under/IN the/DT pretense/NN of/IN them/PRP being/AUXG married/VBN ./. </C>
</S>
<S>
<C>He/PRP also/RB claims/VBZ that/IN once/RB same-sex/JJ marriage/NN is/AUX legalized/VBN ,/, </C>
<C>it/PRP is/AUX possible/JJ that/IN straight/JJ men/NNS will/MD marry/VB each/DT other/JJ </C>
<C>in/IN order/NN to/TO gain/VB some/DT unspecified/JJ advantages/NNS ./. </C>
</S>
<S>
<C>S2/NNP asserts/VBZ that/IN the/DT argument/NN currently/RB being/AUXG discussed/VBN is/AUX regarding/VBG marriage/NN ,/, not/RB sexual/JJ behaviors/NNS of/IN married/JJ couples/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN it/PRP is/AUX possible/JJ to/TO represent/VB two/CD characters/NNS as/IN homosexual/JJ </C>
<C>without/IN invoking/VBG sexual/JJ behaviors/NNS ,/, </C>
<C>comparing/VBG this/DT to/TO portraying/VBG heterosexual/JJ characters/NNS </C>
<C>without/IN being/AUXG sexually/RB explicit/JJ ./. </C>
</S>
</P>
</T>
