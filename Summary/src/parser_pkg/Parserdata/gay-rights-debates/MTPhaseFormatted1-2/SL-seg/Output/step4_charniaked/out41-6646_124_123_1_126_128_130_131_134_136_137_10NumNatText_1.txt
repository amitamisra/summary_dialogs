(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ challenges) (S (NP (NNP S2)) (VP (TO to) (VP (VB debate) (NP (NP (DT the) (NN cause)) (PP (IN of) (NP (NP (DT the) (NN spread)) (PP (IN of) (NP (NNP AIDS)))))) (PP (IN in) (NP (DT the) (NNP United) (NNPS States))))))) (CC and) (VP (VBZ encourages) (S (NP (NNP S2)) (VP (TO to) (VP (VB attempt) (S (VP (TO to) (VP (VB challenge) (NP (PRP$ his) (NNS claims)) (PP (IN with) (NP (NN proof) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ untrue))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (S (VP (VP (VBG attempting) (S (VP (TO to) (VP (VB change) (NP (DT the) (NN debate)))))) (CC and) (VP (VBG making) (NP (NP (JJ personal) (NNS attacks)) (PP (RB rather) (IN than) (S (VP (VBG addressing) (NP (NP (DT the) (NN question)) (PP (IN at) (NP (NN hand))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT the) (NN debate)) (VP (VBZ occurs)))) (, ,) (NP (PRP he)) (VP (MD will) (VP (VB prove) (S (NP (PRP$ his) (NNS assertions)) (ADJP (JJ true)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (NP (DT the) (NN origin)) (PP (IN of) (NP (NNP AIDS))) (PP (IN in) (NP (DT the) (NNP United) (NNPS States)))) (VP (AUX is) (ADJP (JJ unknown))))) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NN spread)) (PP (IN of) (NP (NNP AIDS)))) (VP (AUX was) (VP (VP (VBN caused) (PP (IN by) (NP (JJ unclean) (NNS needles)))) (, ,) (VP (VBN unprotected) (NP (NN sex))) (, ,) (CC and) (VP (VBN contaminated) (NP (NN blood) (NNS transfusions))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ rejects) (NP (DT the) (NN requirement) (SBAR (IN that) (S (NP (PRP he)) (VP (MD should) (VP (VB provide) (NP (NP (NN proof)) (PP (IN for) (NP (PRP$ his) (NNS claims)))) (PP (IN by) (S (VP (VBG citing) (NP (NP (NNP S1) (POS 's)) (NN failure) (S (VP (TO to) (VP (AUX do) (ADVP (RB so))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (SBAR (IN once) (S (NP (NNP S1)) (VP (VBZ begins) (S (VP (TO to) (VP (VB debate) (ADVP (RB appropriately)))))))) (, ,) (ADVP (RB then)) (NP (PRP he)) (VP (MD will) (RB too))))) (. .)))

