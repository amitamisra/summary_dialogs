<s> <PARAGRAPH> </s>
<s>  S1 makes the statement, 'It's nothing like racial segregation'... </s>
<s> When questioned about this statement the speaker begins repeatedly asking what it was that he had contended. </s>
<s> He claims that the other speaker has no idea what he meant by his statement and that he is just making unfounded assumptions. </s>
<s> He insists that he made no position about what segregation means definitely. </s>
<s> He insists further that he did not imply the plight of blacks and gays were dissimilar. </s>
<s> S2 argues with S1 about what he believes the speaker actually meant from his statement. </s>
<s> His first point is adamantly made, that the segregation of blacks is in actuality just like the segregation that is faced by homosexuals. </s>
<s> He insists that the prejudice that they face is just like the previous prejudices, that in their time were also seen as as 'different'. </s>
<s> He also claims that the statement implies that if the segregation of blacks were imposed on homosexuals, that it would not be less wrong. </s>
<s> He concedes his assumption was made in error yet he has discredited the speaker. </s>
<s>  </s>
