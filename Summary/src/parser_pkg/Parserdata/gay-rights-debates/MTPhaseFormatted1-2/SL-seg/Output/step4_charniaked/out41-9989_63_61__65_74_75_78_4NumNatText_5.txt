(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG arguing) (NP (IN about) (NN religion)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT the) (NNS homosexuals)) (CC and) (NP (PRP$ their) (NNS supporters))) (VP (AUX are) (ADJP (JJ close) (PP (VBN minded) (PP (TO to) (SBAR (WHNP (WP what)) (S (NP (DT the) (NN bible)) (VP (VBZ teaches) (PP (IN about) (NP (JJ gay) (NN sex))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (DT the) (JJ gay) (NN community)) (VP (AUX is) (VP (VBG saying) (SBAR (S (NP (PRP$ their) (NN sin)) (VP (AUX is) (ADJP (JJ ok))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (VP (VBG looking) (PP (IN for) (NP (PRP$ their) (JJS best) (NNS interests))) (PP (IN by) (S (VP (VBG asking) (NP (PRP them)) (S (VP (TO to) (VP (VB repent) (PP (TO to) (NP (NNP Jesus) (NNP Christ))) (PP (IN for) (NP (NP (DT the) (NN remission)) (PP (IN of) (NP (PRP$ their) (NNS sins))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP he)) (VP (VBD figured) (PRT (RP out)) (PP (RB right) (IN from) (ADJP (JJ wrong))) (PP (PP (IN by) (S (VP (VBG treating) (NP (NNS people)) (PP (IN with) (NP (NN kindness) (CC and) (NN respect)))))) (, ,) (RB not) (PP (IN by) (NP (DT a) (NN book)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (NN bible)) (ADVP (RB also)) (VP (VBZ says) (S (VP (TO to) (VP (NN stone) (NP (NNS children)) (PP (TO to) (NP (NN death))) (SBAR (IN so) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NP (DT a) (NN resource)) (SBAR (S (VP (TO to) (VP (AUX be) (VP (VBN trusted))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (S (VP (ADVP (RB only)) (VBG believing) (SBAR (IN in) (S (NP (NNP Jesus) (NNP Christ)) (VP (VBZ gets) (NP (PRP you)) (PP (TO to) (NP (NN heaven)))))))) (, ,) (NP (NN anything) (RB else)) (VP (VBZ leads) (PP (TO to) (NP (NN Hell))))))) (. .)))

