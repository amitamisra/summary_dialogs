<PARAGRAPH> S1 is talking about a female who is ignoring S1's religious freedom and barring his family from legal protections.
S2 questions to how she is impeding S1 from his religious freedom and argues that she is not trying to take away his right from a religious ceremony.
S1 says the woman's religious belief is that marriage should be between a man and a woman, and S1's religious belief is that marriage can be between any two people that love each other regardless of their gender.
If the woman's religious belief gets incorporated in to the law, S1 will not be able to marry the person of his choice but she will still be able to marry regardless of the law.
The woman is just trying to keep gay people out of marriage to preserve her heterosexual privilege.
S2 argues that either one of them can be impeding on the others religious freedom.
S1 does not give in and keeps arguing that she is trying to turn her religious belief into law and bar him from getting married.
