(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ wrong)) (SBAR (IN for) (S (NP (JJ gay) (NNS activists)) (VP (TO to) (VP (VB compare) (NP (NP (JJ public) (NN disapproval)) (PP (IN of) (NP (NN homosexuality)))) (PP (IN with) (NP (NN race) (NN discrimination))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ takes) (NP (DT a) (NN jab) (SBAR (IN at) (S (NP (JJ gay) (NNS activists)) (VP (VBP love) (S (VP (VBG comparing) (NP (NP (JJ public) (NN disapproval)) (PP (IN of) (NP (NN homosexuality)))) (PP (IN with) (NP (JJ racial) (NN discrimination)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX has) (NP (NP (NNS complaints)) (PP (IN about) (NP (NP (NP (DT the) (NN public) (POS 's)) (JJ overwhelming) (NN disapproval)) (PP (IN of) (NP (NP (DT another) (JJ human) (NN being) (POS 's)) (NN behavior))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ notes) (SBAR (IN that) (S (NP (DT the) (CD two)) (VP (VP (AUX are) (NP (CD two) (JJ different) (NNS entities))) (CC and) (VP (MD can) (RB not) (ADVP (RB truly)) (VP (AUX be) (VP (VBN compared)))))))) (. .)))

(S1 (S (ADVP (RB Furthermore)) (, ,) (NP (PRP he)) (VP (VBZ states) (SBAR (IN if) (S (NP (PRP you)) (VP (AUX were) (VP (VBN fired) (SBAR (S (NP (PRP it)) (VP (MD would) (VP (AUX be) (PP (IN for) (S (VP (AUXG being) (ADJP (JJ incompetent) (PP (RB rather) (IN than) (S (VP (AUXG being) (ADJP (JJ gay)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (DT the) (NNS questions) (SBAR (IN that) (S (NP (DT the) (NN GUEST)) (VP (AUX is) (VP (VBG asking)))))) (VP (AUX are) (RB not) (ADJP (JJ relevant) (RB enough) (S (VP (TO to) (VP (VB answer) (NP (PRP them)))))))))) (. .)))

(S1 (S (PP (IN In) (NP (DT the) (NN end))) (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJ gay) (NN activist)) (VP (MD will) (ADVP (RB soon)) (VP (VB find) (PRT (RP out)) (SBAR (WHADVP (WRB why)) (S (NP (DT the) (NN behavior) (CC and) (NNS thoughts)) (VP (AUX are) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (AUX are)))))))))))) (. .)))

(S1 (S (NP (DT The) (DT both)) (VP (VBP argue) (SBAR (IN that) (S (NP (DT each) (NN one)) (VP (AUX 's) (ADJP (JJ wrong)))))) (. .)))

