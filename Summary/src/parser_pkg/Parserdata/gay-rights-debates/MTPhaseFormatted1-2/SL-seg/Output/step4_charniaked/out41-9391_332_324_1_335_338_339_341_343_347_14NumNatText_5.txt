(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (RB not) (PP (IN against) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (S (NP (NNP Britain)) (VP (AUX has) (NP (PRP it)))) (CC and) (S (NP (PRP he)) (VP (AUX has) (RB not) (VP (VBN seen) (NP (PRP it)) (VP (AUX have) (NP (NP (DT a) (JJ negative) (NN impact)) (PP (IN on) (NP (NN anyone)))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNS gays)) (VP (AUX do) (RB not) (VP (AUX have) (NP (NP (DT a) (NN right)) (PP (TO to) (NP (NN marriage)))))))))) (, ,) (CC but) (S (NP (PRP it)) (VP (MD should) (VP (AUX be) (VP (VBN allowed) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX does) (RB not) (VP (VB harm) (NP (NN society)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ wonders) (SBAR (WHADVP (WRB why)) (S (NP (EX there)) (VP (AUX is) (RB not) (NP (NP (DT a) (JJ positive) (NN approach)) (PP (TO to) (NP (NN marriage))) (PP (IN by) (NP (JJ gay) (NNS people)))))))) (. .)))

(S1 (S (SBAR (IN While) (S (NP (PRP he)) (VP (VBZ views) (NP (NN homosexuality)) (PP (IN as) (NP (DT a) (NN sin)))))) (, ,) (NP (PRP he)) (VP (VBZ views) (NP (NP (JJ other) (NNS things)) (PP (IN as) (NP (DT a) (NN sin)))) (ADVP (RB as) (RB well))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ talks) (PP (IN of) (NP (NP (JJ gay) (NN marriage)) (PP (IN in) (NP (NP (NN history)) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ false) (S (VP (TO to) (VP (VB claim) (SBAR (S (NP (NN marriage)) (VP (AUX has) (ADVP (RB always)) (VP (AUX been) (NP (DT a) (NN man) (CC and) (NN woman)))))))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ concedes) (SBAR (S (NP (PRP it)) (VP (MD may) (RB not) (VP (AUX be) (NP (DT a) (NN right)))))))) (, ,) (CC but) (S (VP (VBZ thinks) (SBAR (IN that) (S (ADVP (RB also)) (VP (VBZ applies) (PP (TO to) (NP (NNS heterosexuals)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (AUX does) (RB not) (VP (VB view) (NP (JJ gay) (NN marriage)) (PP (IN as) (NP (NP (DT a) (NN threat)) (PP (TO to) (NP (NNP Christianity))))))) (CC and) (VP (VBZ asks) (SBAR (WHADVP (WRB how)) (S (NP (NNP Britain)) (VP (VBD got) (NP (PRP it))))))) (. .)))

