<PARAGRAPH> S1 seems to agree with the comparison between interracial marriage and same sex marriage, citing that many of the arguments against them are similar.
He believes that words can be redefined including the word marriage and uses war as an example.
He brings up the fact that war has not been declared since WWII but the country has been involved in war-like situations and typically these are referred to as war.
He sees this as being off topic but this debate ends up dominating the remaining part of the text.
S2 does not agree that marriage should be redefined to include same sex couples.
He does not believe himself to be a homophobe and cites friendships with gay individuals to back up his claim.
His issue is not with rights or the actual relationships themselves, he believes in full rights for gay couples, but he does not believe that the relationships should be able to bear the label marriage because it is not between a man and woman.
When the topic changes to the use of the term war he does not disagree with S1 but gives information challenging previous statements.
