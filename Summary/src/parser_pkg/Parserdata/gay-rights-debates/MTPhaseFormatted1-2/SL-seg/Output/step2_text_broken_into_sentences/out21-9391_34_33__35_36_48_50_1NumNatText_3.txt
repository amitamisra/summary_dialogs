<PARAGRAPH> S1 does not believe that any evidence coming from Cameron should not be viewed without suspicion on grounds that he has been kicked out of the legitimate scientific community and has gotten in trouble with judges and scientists that publicly condemn him for misusing their research.
They argue that even if it were true, it does not legitimize anti-gay legislation or discrimination and that being homosexual does not hurt anyone, nor should it be counted as a disease.
They believe that HIV can affect anyone, not just the homosexual community.
S2 believes that giving homosexuals rights and privileges affects everyone else financially.
They believe that something should be done if a scientific study links a dangerous disease to the activities of a certain group of people.
