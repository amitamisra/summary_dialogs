(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (PP (IN about) (S (VP (VBG redefining) (NP (NP (DT the) (NN meaning)) (PP (IN of) (NP (NN marriage))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ unnecessary) (S (VP (TO to) (VP (VB redefine) (NP (NN marriage)) (S (VP (TO to) (VP (VB include) (NP (NNS gays))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT all)) (PP (IN of) (NP (DT the) (NNS rights) (CC and) (NNS benefits))) (SBAR (WHNP (IN that)) (S (NP (JJ married) (NNS couples)) (VP (AUX have))))) (VP (AUX are) (ADVP (RB already)) (VP (VBN granted) (PP (TO to) (NP (NNS gays))) (PP (IN without) (NP (NP (DT the) (NN need)) (PP (IN for) (NP (VBG redefining)))))))))) (. .)))

(S1 (S (NP (VBG Redefining)) (ADVP (RB only)) (VP (VBZ serves) (S (VP (TO to) (VP (VB hurt) (NP (NP (DT the) (JJ many) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP see) (NP (NN marriage)) (PP (IN as) (NP (NP (DT a) (JJ sacred) (NN bond)) (PP (IN between) (NP (NN man) (CC and) (NN woman))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ necessary) (S (VP (TO to) (VP (VB redefine) (NP (NN marriage)))))))))) (. .)))

(S1 (S (SBAR (IN If) (FRAG (ADVP (RB not)))) (, ,) (NP (NP (DT the) (NNS rights) (CC and) (NNS benefits)) (VP (VBN granted) (PP (TO to) (NP (NNS gays))))) (VP (MD will) (RB not) (VP (AUX be) (VP (VBN guaranteed)))) (. .)))

(S1 (S (ADVP (RB Also)) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ believes) (SBAR (IN that) (S (S (VP (VBG offending) (NP (NP (NN someone) (POS 's)) (NN sensibility)))) (VP (AUX is) (RB not) (ADJP (JJ real))))))) (, ,)) (NP (NP (JJ measurable) (NN damage)) (, ,) (ADVP (RB so) (RB there))) (VP (AUX is) (RB not) (NP (NP (NN reason)) (SBAR (S (RB not) (VP (TO to) (VP (VB redefine) (NP (NN marriage)) (, ,) (S (VP (RB not) (VBG doing) (SBAR (IN so) (S (NP (NP (JJ only) (NNS appeases)) (ADJP (NN faith) (VBN justified))) (VP (VBP hate)))))))))))) (. .)))

