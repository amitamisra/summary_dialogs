<PARAGRAPH> Two people are discussing the financial effect of allowing for gay marriage.
S1 originally believed allowing gay marriage would have a negative financial impact on society as a whole.
He has since amended that belief due to information found in the CBO.
His reasoning for still supporting the ban on gay marriage has changed over to moral and health related concerns.
Although S2 is pleased with the concession of S1's former opinion, he believes jumping to the moral high ground still ignores other issues.
He would like to discuss the positive impact that allowing gay marriage would have on the country practically.
S1 advises while he is a defender of the Constitution, he does feel society should decide.
