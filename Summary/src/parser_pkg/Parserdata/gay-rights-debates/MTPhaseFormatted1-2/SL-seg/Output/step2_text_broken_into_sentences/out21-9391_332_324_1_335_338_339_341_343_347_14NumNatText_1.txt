<PARAGRAPH> S1 is in Britain where the experience with some years of gay marriages has allowed her to be comfortable that it is not a problem, although she believes gay sex is a sin.
She argues that marriage is by definition between a man and a woman even if it is not stated, just as it would not be expressly specified to be between a man and a goldfish.
S2 sees S1 as being anti-gay marriage as a result and challenges her about it, but S1 is actually unopposed to gay marriage.
S1 and S2 also discuss the issue of whether possible production of handicapped children, such as in incestuous relationships or others involving genetically specific information should be barred on public policy grounds.
