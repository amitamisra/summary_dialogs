(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NNS rights)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ challenges) (S (NP (NNP S2)) (VP (TO to) (VP (VB find) (NP (JJ real) (JJ scientific) (NN evidence))))) (, ,) (SBAR (IN as) (S (NP (NP (NN anything)) (VP (VBG coming) (PP (IN from) (NP (NNP Cameron))))) (VP (AUX does) (RB not) (VP (VB apply) (SBAR (IN as) (S (NP (PRP he)) (VP (AUX has) (VP (AUX been) (VP (VP (VBN kicked) (PRT (RP out))) (CC and) (VP (VBN condemned) (ADVP (RB publicly)) (PP (IN from) (NP (DT the) (JJ legitimate) (JJ scientific) (NN community)))))))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NN homosexuality)) (VP (AUX is) (RB not) (NP (DT a) (NN disease))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNP HIV)) (VP (VBZ infects) (NP (NN anyone)))))))) (: ;) (S (S (VP (AUXG being) (ADJP (JJ gay)))) (VP (AUX does) (RB not) (VP (VB put) (SBAR (S (NP (NP (NNS people)) (PP (IN at) (NP (NP (DT a) (JJR greater) (NN risk)) (, ,) (NP (NN promiscuity)) (CC and) (NP (NNP IV) (NN drug) (NN usage))))) (VP (AUX do))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ gives) (NP (NP (DT a) (JJ hypothetical) (NN situation)) (, ,) (SBAR (WHADVP (WRB where)) (S (NP (NP (DT a) (NN minister)) (PP (IN of) (NP (JJ public) (NN health)))) (VP (AUX has) (NP (NN proof) (SBAR (IN that) (S (S (NP (NP (NNS people)) (PP (IN with) (NP (DT a) (JJ certain) (NN lifestyle)))) (VP (VBP live) (ADJP (JJR shorter)))) (CC and) (S (NP (PRP he)) (VP (VBZ questions) (SBAR (WHNP (WP what)) (S (NP (NNP S1)) (VP (MD will) (VP (AUX do) (PP (IN in) (NP (DT that) (NN situation))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (S (VP (VBG granting) (ADVP (RB gays)) (NP (NP (DT the) (NNS rights) (CC and) (NNS privileges)) (PP (IN of) (NP (NN marriage)))))) (VP (VBZ affects) (NP (NN everyone)) (ADVP (RB financially) (CC and) (RB negatively)))))) (. .)))

