(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (NP (JJ gay) (NNS relations)) (CC and) (NP (NP (DT the) (NN possibility)) (PP (IN of) (NP (VBG redefining) (NN marriage) (S (VP (TO to) (VP (VB include) (NP (DT the) (JJ gay) (NN community))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (JJ gay) (NNS relationships)) (VP (AUX are) (NP (NP (DT a) (NN perversion)) (PP (IN of) (NP (NP (DT the) (JJ normal) (NN thought)) (PP (IN of) (NP (NN society)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NNP S1)) (VP (VP (VBZ hates) (NP (NNS gays))) (CC and) (VP (AUX is) (ADVP (RB basically)) (NP (NP (NN prejudice)) (PP (IN toward) (NP (NP (JJ gay) (NNS people)) (VP (VBN based) (PP (IN upon) (S (NP (DT some) (NNS remarks)) (VP (AUXG being) (VP (VBN made)))))))))))))) (. .)))

(S1 (FRAG (ADVP (RB Particularly)) (NP (NP (DT the) (NN label)) (PP (IN of) (NP (NN perversion)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (RB not) (PP (IN against) (NP (NP (DT all) (NN diversity)) (, ,) (RB just) (NP (NP (JJ gay) (NN lifestyle)) (SBAR (WHNP (WDT which)) (S (PP (IN in) (NP (NP (NNP S2) (POS 's)) (NN opinion))) (VP (VBZ makes) (S (ADJP (RBR him/her) (JJ hateful) (PP (IN toward) (NP (NNS gays))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (S (NP (NN he/she)) (VP (VP (AUX does) (RB not) (VP (VB hate) (NP (NNS gays)))) (CC but) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (S (NP (NN marriage)) (VP (MD should) (VP (AUX be) (VP (VBD redefined) (S (VP (TO to) (VP (VB allow) (PP (IN for) (NP (PRP them))) (PP (TO to) (ADJP (VBN wed))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (NP (NP (NNP S1) (POS 's)) (NN objection)) (PP (TO to) (NP (NP (NNS gays) (CC and) (NP (DT the) (NN family) (POS 's))) (SBAR (S (NP (PRP they)) (VP (MD would) (VP (VB make)))))))) (VP (AUX is) (NP (NN prejudice)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (S (NP (NNP S2)) (VP (AUX is) (ADJP (JJ wrong) (PP (IN in) (NP (NN his/her) (NNS beliefs)))))))) (. .)))

