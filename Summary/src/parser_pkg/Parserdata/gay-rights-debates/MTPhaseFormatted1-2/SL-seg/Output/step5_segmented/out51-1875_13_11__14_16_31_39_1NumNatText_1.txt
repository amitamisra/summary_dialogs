<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN because/IN healthcare/NN is/AUX generally/RB extended/VBN to/TO spouses/NNS ,/, </C>
<C>homosexuals/NNS will/MD not/RB be/AUX eligible/JJ to/TO receive/VB healthcare/NN through/IN their/PRP$ partner/NN </C>
<C>because/IN same-sex/JJ marriages/NNS are/AUX not/RB recognized/VBN in/IN Virginia/NNP ./. </C>
</S>
<S>
<C>He/PRP likens/VBZ this/DT situations/NNS to/TO a/DT heterosexual/JJ male/NN with/IN a/DT girlfriend/NN who/WP would/MD not/RB receive/VB his/PRP$ healthcare/NN benefits/NNS ./. </C>
</S>
<S>
<C>He/PRP presents/VBZ his/PRP$ opinion/NN that/IN homosexuals/NNS should/MD move/VB out/IN of/IN Virginia/NNP and/CC his/PRP$ hope/NN that/IN same-sex/JJ marriages/NNS will/MD be/AUX made/VBN illegal/JJ everywhere/RB ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN the/DT cases/NNS presented/VBN by/IN S1/NNP are/AUX not/RB equivalent/JJ </C>
<C>because/IN the/DT heterosexual/JJ male/NN has/AUX the/DT right/NN to/TO marry/VB his/PRP$ girlfriend/NN </C>
<C>if/IN he/PRP chooses/VBZ ,/, </C>
<C>at/IN which/WDT point/NN she/PRP would/MD be/AUX eligible/JJ for/IN his/PRP$ benefits/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB points/VBZ out/RP that/IN after/IN cohabitating/VBG for/IN a/DT certain/JJ period/NN of/IN time/NN ,/, </C>
<C>the/DT woman/NN would/MD be/AUX eligible/JJ under/IN common/JJ law/NN marriage/NN ./. </C>
</S>
</P>
</T>
