<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN since/IN support/NN for/IN marriage/NN equality/NN is/AUX growing/VBG ,/, </C>
<C>it/PRP is/AUX only/RB a/DT matter/NN of/IN time/NN </C>
<C>before/IN the/DT majority/NN votes/VBZ in/IN its/PRP$ favor/NN ./. </C>
</S>
<S>
<C>This/DT person/NN uses/VBZ the/DT DC/JJ council/NN votes/NNS as/IN evidence/NN </C>
<C>to/TO support/VB this/DT as/RB well/RB as/IN the/DT fact/NN that/IN five/CD states/NNS have/AUX passed/VBN gay/JJ marriage/NN legislation/NN ./. </C>
</S>
<S>
<C>They/PRP state/VBP that/IN any/DT step/NN towards/IN viewing/VBG homosexuals/NNS as/IN normal/JJ people/NNS is/AUX a/DT good/JJ thing/NN ,/, </C>
<C>and/CC that/IN even/RB viewing/VBG civil/JJ unions/NNS in/IN a/DT positive/JJ light/NN would/MD and/CC could/MD be/AUX enough/RB ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB that/IN marriage/NN equality/NN is/AUX something/NN that/WDT the/DT majority/NN of/IN the/DT voters/NNS wish/VBP to/TO see/VB enacted/VBN ./. </C>
</S>
<S>
<C>This/DT person/NN argue/VBP that/IN any/DT legislation/NN put/VBN into/IN motion/NN has/AUX been/AUX pushed/VBN around/IN the/DT voters/NNS ,/, </C>
<C>and/CC never/RB accounted/VBN for/IN on/IN an/DT actual/JJ ballot/NN ./. </C>
</S>
<S>
<C>They/PRP refute/VB S1/NNP 's/POS argument/NN that/IN five/CD states/NNS have/AUX passed/VBN gay/JJ marriage/NN legislation/NN </C>
<C>on/IN the/DT grounds/NNS that/IN it/PRP was/AUX not/RB done/AUX via/IN the/DT voters/NNS ,/, </C>
<C>and/CC never/RB actually/RB put/VBD on/IN a/DT ballot/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN if/IN homosexuals/NNS were/AUX willing/JJ to/TO simply/RB accept/VB civil/JJ unions/NNS that/WDT are/AUX equivalent/JJ to/TO marriage/NN ,/, </C>
<C>they/PRP might/MD find/VB stronger/JJR grounds/NNS for/IN legal/JJ acceptance/NN ./. </C>
</S>
</P>
</T>
