<s> <PARAGRAPH> </s>
<s>  S1: Washington DC' s city council recently legalized gay marriage. </s>
<s> People's support of gay marriage has been growing steadily in the past ten years, and will soon be the majority opinion. </s>
<s> Five states have already legalized gay marriage. </s>
<s> Those opposed to gay marriage may view gays as promiscuous and sexual deviants, but most gay men are assuredly quite normal. </s>
<s> Civil unions will not be accepted in place of marriage, as they do not allow the same legal rights as marriage. </s>
<s> S2: Councils and legislatures may vote in favor of gay marriage, but it has yet to be the majority vote of the people. </s>
<s> Gay marriage should be played in a positive light by its supporters, because the current public perception of gay men is that they are promiscuous and lead a "gay lifestyle" that is offensive. </s>
<s> Anti-gay marriage supporters will support civil unions in lieu of gay marriage, and homosexuals should be satisfied with that. </s>
<s>  </s>
