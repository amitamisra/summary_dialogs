<T>
<P>
</P>
<P>
<S>
<C>S1/NNP Denies/VBZ bringing/VBG up/RP religion/NN ,/, </C>
<C>someone/NN else/RB did/AUX ./. </C>
</S>
<S>
<C>States/NNS freedom/NN of/IN belief/NN is/AUX law/NN </C>
<C>but/CC rules/NNS govern/VBP actions/NNS ./. </C>
</S>
<S>
<C>Also/RB that/DT changing/VBG something/NN frequently/RB weakens/VBZ its/PRP$ integrity/NN ./. </C>
</S>
<S>
<C>S2/NNP Denies/VBZ validity/NN of/IN S1/NNP 's/POS earlier/JJR arguments/NNS ,/, as/IN referencing/VBG that/IN were/AUX not/RB between/IN consensual/JJ adults/NNS ./. </C>
</S>
<S>
<C>Questions/NNS if/IN freedom/NN of/IN religion/NN were/AUX allowed/VBN but/CC expression/NN limited/JJ ./. </C>
</S>
<S>
<C>S1/NNP States/NNP many/JJ religious/JJ practices/NNS such/JJ as/IN polygamy/NN are/AUX illegal/JJ and/CC that/IN many/JJ other/JJ consensual/JJ acts/NNS such/JJ as/IN drug/NN use/NN as/RB well/RB ./. </C>
</S>
<S>
<C>S2/NNP States/NNPS that/IN other/JJ banned/VBN activities/NNS are/AUX linked/VBN to/TO crime/NN directly/RB </C>
<C>but/CC homosexuality/NN is/AUX not/RB ./. </C>
</S>
<S>
<C>S1/NNP States/NNPS that/VBP </C>
<C>if/IN sodomy/NN is/AUX illegal/JJ then/RB homosexuality/NN is/AUX a/DT crime/NN ./. </C>
</S>
<S>
<C>States/VBZ that/IN the/DT people/NNS are/AUX equal/JJ not/RB the/DT religions/NNS they/PRP follow/VBP ./. </C>
</S>
<S>
<C>S2/NNP Refutes/NNP S1/NNP ,/, denies/VBZ any/DT link/NN between/IN homosexuality/NN and/CC crime/NN ./. </C>
</S>
<S>
<C>Cites/VBZ difference/NN between/IN the/DT letter/NN and/CC intent/NN of/IN the/DT law/NN ./. </C>
</S>
<S>
<C>S1/NNP States/NNPS that/DT equality/NN is/AUX treating/VBG everybody/NN the/DT same/JJ ,/, not/RB every/DT behavior/NN ./. </C>
</S>
<S>
<C>The/DT people/NNS determine/VBP what/WP is/AUX in/IN the/DT public/NN 's/POS interest/NN ./. </C>
</S>
<S>
<C>States/VBZ differences/NNS between/IN state/NN laws/NNS with/IN prostitution/NN ,/, </C>
<C>age/NN of/IN consent/NN and/CC references/NNS physical/JJ effects/NNS reported/VBN by/IN doctors/NNS of/IN homosexuality/NN also/RB links/VBZ to/TO serial/JJ killers/NNS ./. </C>
</S>
</P>
</T>
