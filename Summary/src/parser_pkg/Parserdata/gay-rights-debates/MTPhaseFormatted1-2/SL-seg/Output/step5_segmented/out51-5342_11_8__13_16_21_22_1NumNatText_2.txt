<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP enter/VBP a/DT discussion/NN about/IN gay/JJ rights/NNS ,/, but/CC more/RBR specifically/RB on/IN the/DT claims/NNS made/VBN by/IN a/DT certain/JJ Republican/NNP Party/NNP member/NN :/: Paul/NNP Cameron/NNP ./. </C>
</S>
<S>
<C>S1/NNP is/AUX angered/JJ that/IN S2/NNP is/AUX making/VBG wide/JJ generalization/NN about/IN the/DT Republican/NNP Party/NNP ,/, </C>
<C>and/CC agrees/VBZ that/IN Cameron/NNP is/AUX eccentric/JJ party/NN member/NN ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN since/IN Republican/NNP Party/NNP members/NNS often/RB use/VBP Cameron/NNP as/IN citation/NN for/IN research/NN </C>
<C>and/CC therefore/RB most/JJS share/NN his/PRP$ views/NNS that/IN gays/NNS are/AUX bad/JJ for/IN the/DT world/NN and/CC must/MD be/AUX exterminated/VBN ,/, like/IN the/DT Jews/NNPS in/IN the/DT Holocaust/NNP ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ S2/NNP is/AUX making/VBG a/DT generalization/NN on/IN the/DT entire/JJ party/NN 's/POS based/VBN off/IN one/CD person/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ this/DT unfair/JJ </C>
<C>because/IN Cameron/NNP is/AUX not/RB apart/RB of/IN the/DT Republican/NNP Leadership/NN ,/, </C>
<C>and/CC that/IN the/DT views/NNS of/IN Cameron/NNP are/AUX outdated/VBN with/IN the/DT views/NNS of/IN Republicans/NNPS today/NN ./. </C>
</S>
<S>
<C>S1/NNP compares/VBZ this/DT to/TO quoting/VBG singer/NN John/NNP Lennon/NNP against/IN religion/NN and/CC attributing/VBG it/PRP to/TO the/DT entire/JJ Democratic/NNP Party/NNP ./. </C>
</S>
<S>
<C>S2/NNP responds/VBZ by/IN connecting/VBG Cameron/NNP with/IN George/NNP W./NNP Bush/NNP ,/, </C>
<C>and/CC believes/VBZ that/IN if/IN a/DT party/NN constantly/RB uses/VBZ the/DT research/NN of/IN single/JJ person/NN ,/, </C>
<C>then/RB they/PRP must/MD share/VB the/DT beliefs/NNS of/IN that/DT person/NN ./. </C>
</S>
</P>
</T>
