<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN it/PRP has/AUX nothing/NN to/TO do/AUX with/IN marriage/NN equality/NN ,/, but/CC an/DT attempt/NN to/TO redefine/VB the/DT meaning/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP wishes/VBZ that/IN the/DT gay/JJ posters/NNS would/MD be/AUX honest/JJ and/CC not/RB use/VB words/NNS such/JJ as/IN equality/NN and/CC discrimination/NN where/WRB they/PRP do/AUX not/RB apply/VB ,/, </C>
<C>since/IN marriage/NN is/AUX an/DT institution/NN created/VBN to/TO be/AUX a/DT union/NN between/IN man/NN and/CC woman/NN that/WDT leads/VBZ to/TO the/DT creation/NN of/IN children/NNS ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN marriage/NN has/AUX nothing/NN to/TO do/AUX with/IN two/CD people/NNS giving/VBG each/DT other/JJ a/DT wedding/NN ring/NN ./. </C>
</S>
<S>
<C>S2/NNP retorts/VBZ by/IN questioning/VBG how/WRB it/PRP is/AUX not/RB discrimination/NN if/IN he/PRP can/MD not/RB marry/VB the/DT person/NN of/IN his/PRP$ choice/NN </C>
<C>but/CC a/DT heterosexual/NN can/MD ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN Las/NNP Vegas/NNP itself/PRP refutes/VBZ S1/NNP 's/POS arguments/NNS ./. </C>
</S>
</P>
</T>
