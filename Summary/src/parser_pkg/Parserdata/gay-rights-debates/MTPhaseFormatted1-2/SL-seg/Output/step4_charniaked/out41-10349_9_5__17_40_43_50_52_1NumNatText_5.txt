(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (DT the) (JJ legal) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (NP (NP (DT a) (NN contract)) (PP (IN between) (NP (NP (DT a) (NN man)) (CC and) (NP (NP (DT a) (NN woman)) (, ,) (SBAR (WHNP (WP who)) (S (VP (AUX are) (ADJP (RB not) (JJ related)) (, ,) (PP (IN with) (NP (NP (DT no) (NN regard)) (PP (TO to) (NP (NP (VB love)) (, ,) (CC or) (NP (NP (DT the) (NN intention)) (PP (IN of) (NP (NN reproduction))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ maintains) (, ,) (PP (IN from) (NP (NP (DT the) (NN basis)) (PP (IN of) (NP (NN observation))))) (, ,) (SBAR (IN that) (S (NP (NP (DT the) (JJ cultural) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (NP (NP (DT a) (NN formalization)) (PP (IN of) (NP (NP (DT a) (NN mating) (NN pair)) (, ,) (NP (NN love)) (CC and) (NP (DT the) (NN intension) (S (VP (TO to) (VP (VB reproduce) (S (VP (AUXG being) (VP (VBN implied))))))))))))))) (. .)))

(S1 (S (S (VP (VBG Saying) (SBAR (SBAR (IN that) (S (NP (JJ homosexual) (NNS couples)) (VP (AUX are) (RB not) (NP (NP (DT a) (NN part)) (PP (IN of) (NP (DT either) (NN definition))))))) (, ,) (CC and) (SBAR (IN that) (S (S (VP (AUXG being) (VP (VBN included) (PP (IN in) (NP (DT the) (JJ current) (NN definition)))))) (VP (AUX is) (NP (DT a) (NN catch-22)))))))) (, ,) (NP (IN as) (NN homosexuality)) (VP (AUX is) (RB not) (PP (IN in) (NP (NP (NN sync)) (PP (IN with) (NP (NN evolution)))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB further)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (JJ only) (NN connection) (IN between)) (, ,) (NP (NP (NN mating) (NNS couples)) (CC and) (NP (NN marriage))) (, ,) (VP (AUX are) (ADJP (JJ cultural)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ defines) (NP (NN marriage)) (PP (IN as) (NP (NP (DT a) (NN commitment)) (PP (IN between) (NP (NP (CD two) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP love) (NP (CD one) (DT another)))))))))) (. .)))

(S1 (S (S (VP (VBG Using) (NP (NP (DT a) (JJ hypothetical) (NN marriage)) (PP (IN between) (NP (NP (DT a) (ADJP (CD 65) (NN year)) (JJ old) (NN woman)) (CC and) (NP (DT a) (ADJP (CD 68) (NN year)) (JJ old) (NN man))))) (PP (IN as) (NP (DT an) (NN example))))) (, ,) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (NN reproduction)) (VP (AUX has) (NP (NP (DT no) (NN bearing)) (PP (IN on) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage))))))))) (, ,) (CC and) (SBAR (IN that) (S (PP (IN without) (NP (NP (DT a) (NN harm)) (VP (VBN involved)))) (, ,) (NP (NP (NN equality)) (PP (IN of) (NP (NNS rights)))) (VP (AUX is) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (JJ important)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ personal) (NNS opinions)) (VP (VP (AUX are) (ADJP (JJ subjective))) (CC and) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN used) (PP (IN as) (NP (DT the) (NN basis))) (S (VP (TO to) (VP (VB limit) (NP (NP (DT the) (NNS rights)) (PP (IN of) (NP (DT the) (NN minority)))))))))))))) (. .)))

