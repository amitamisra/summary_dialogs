<T>
<P>
</P>
<P>
<S>
<C>S1/NNP states/VBZ that/IN allowing/VBG gays/NNS to/TO marry/VB redefines/NNS the/DT meaning/NN of/IN the/DT word/NN marriage/NN </C>
<C>because/IN marriage/NN was/AUX created/VBN to/TO be/AUX union/NN between/IN a/DT man/NN and/CC a/DT woman/NN that/WDT leads/VBZ to/TO child/NN rearing/NN within/IN ''/'' a/DT stable/JJ nuclear/JJ family/NN ''/'' ./. </C>
</S>
<S>
<C>He/PRP further/RB said/VBD this/DT has/AUX nothing/NN to/TO do/AUX with/IN gays/NNS ./. </C>
</S>
<S>
<C>It/PRP has/AUX nothing/NN to/TO do/AUX with/IN guys/NNS giving/VBG each/DT other/JJ rings/NNS and/CC kidding/VBG themselves/PRP that/IN they/PRP are/AUX ``/`` normal/JJ like/IN straight/JJ people/NNS ./. </C>
</S>
<S>
<C>''/'' S2/NNP questioned/VBD S1/NNP asking/VBG him/PRP why/WRB he/PRP believes/VBZ that/IN that/DT redefining/VBG marriage/NN is/AUX a/DT bad/JJ thing/NN ./. </C>
</S>
<S>
<C>He/PRP then/RB said/VBD that/IN if/IN he/PRP cannot/VBP marry/VB who/WP he/PRP wants/VBZ </C>
<C>but/CC a/DT straight/JJ person/NN can/MD ,/, </C>
<C>it/PRP is/AUX discrimination/NN ./. </C>
</S>
<S>
<C>He/PRP said/VBD that/IN he/PRP could/MD not/RB define/VB it/PRP as/IN anything/NN else/RB ./. </C>
</S>
<S>
<C>He/PRP said/VBD S1/NNP was/AUX never/RB in/IN Los/NNP Vegas/NNP ./. </C>
</S>
</P>
</T>
