<T>
<P>
</P>
<P>
<S>
<C>The/DT issues/NNS here/RB appears/VBZ to/TO stem/VB from/IN an/DT employee/NN citing/VBG some/DT religious/JJ view/NN to/TO his/her/JJR boss/NN indicating/VBG why/WRB they/PRP cannot/VBP perform/VB a/DT job/NN duty/NN ./. </C>
</S>
<S>
<C>Speaker/NNP one/NN does/AUX not/RB think/VB people/NNS should/MD pick/VB and/CC choose/VB what/WP they/PRP want/VBP equal/JJ rights/NNS on/IN ,/, in/IN this/DT case/NN between/IN religion/NN and/CC other/JJ rights/NNS issues/NNS ,/, </C>
<C>that/IN it/PRP should/MD be/AUX all/DT or/CC nothing/NN ./. </C>
</S>
<S>
<C>Speaker/NN one/CD therefore/RB believes/VBZ that/IN speaker/NN two/CD is/AUX unfairly/RB discriminating/VBG against/IN an/DT employee/NN based/VBN off/IN their/PRP$ religious/JJ views/NNS ./. </C>
</S>
<S>
<C>Speaker/NNP two/CD believes/VBZ that/IN society/NN dictates/VBZ that/IN you/PRP have/AUX to/TO be/AUX biased/VBN at/IN times/NNS ./. </C>
</S>
<S>
<C>Speaker/NN two/CD continues/VBZ that/IN if/IN speaker/NN one/NN 's/POS logic/NN holds/VBZ true/JJ ,/, </C>
<C>then/RB he/she/NN should/MD be/AUX able/JJ to/TO turn/VB away/RP customers/NNS based/VBN off/IN his/her/NN own/JJ personal/JJ religious/JJ views/NNS ./. </C>
</S>
</P>
</T>
