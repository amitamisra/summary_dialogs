<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB that/IN new/JJ laws/NNS will/MD call/VB for/IN the/DT imprisonment/NN or/CC execution/NN of/IN homosexual/JJ citizens/NNS in/IN the/DT United/NNP States/NNPS ./. </C>
</S>
<S>
<C>They/PRP believes/VBZ that/IN there/EX is/AUX more/JJR danger/NN of/IN economic/JJ collapse/NN under/IN President/NNP Obama/NNP than/IN those/DT laws/NNS concerning/VBG the/DT gay/JJ issues/NNS ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB think/VB that/IN S2/NNP 's/POS fears/NNS are/AUX warranted/VBN </C>
<C>and/CC such/JJ extremes/NNS cannot/VBP happen/VB again/RB ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN every/DT Republican/JJ candidate/NN has/AUX stood/VBN on/IN a/DT platform/NN that/WDT opposes/VBZ gay/JJ rights/NNS ,/, such/JJ as/IN marriage/NN and/CC civil/JJ unions/NNS and/CC wish/VBP to/TO reinstate/VB sodomy/JJ laws/NNS across/IN the/DT country/NN ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN President/NNP Obama/NNP personally/RB believes/VBZ that/DT marriage/NN is/AUX between/IN one/CD man/NN and/CC one/CD woman/NN ,/, </C>
<C>but/CC does/AUX not/RB wish/VB to/TO push/VB his/PRP$ personal/JJ beliefs/NNS into/IN law/NN and/CC instead/RB has/AUX supported/VBN marriage/NN equality/NN as/RB much/RB as/IN he/PRP can/MD ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN no/DT president/NN has/AUX done/AUX more/JJR for/IN the/DT homosexual/JJ community/NN as/IN President/NNP Obama/NNP ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN there/EX is/AUX a/DT legitimate/JJ danger/NN of/IN laws/NNS going/VBG too/RB far/RB </C>
<C>and/CC that/IN imprisonment/NN of/IN homosexuals/NNS can/MD happen/VB </C>
<C>as/IN it/PRP was/AUX federally/RB accepted/VBN in/IN the/DT past/NN of/IN different/JJ races/NNS such/JJ as/IN the/DT Native/JJ Americans/NNPS and/CC Japanese/NNPS ./. </C>
</S>
</P>
</T>
