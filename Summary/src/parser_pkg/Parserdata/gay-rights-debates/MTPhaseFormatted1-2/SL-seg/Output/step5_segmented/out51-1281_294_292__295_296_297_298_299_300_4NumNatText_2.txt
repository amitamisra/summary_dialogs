<T>
<P>
</P>
<P>
<S>
<C>S1/NNP asserts/VBZ that/IN the/DT push/NN for/IN gay/JJ marriage/NN is/AUX driven/VBN by/IN economics/NNS ./. </C>
</S>
<S>
<C>Gay/JJ marriage/NN can/MD bring/VB in/RP new/JJ taxes/NNS ./. </C>
</S>
<S>
<C>S1/NNP describes/VBZ himself/PRP as/IN a/DT Jeffersonian/NNP Liberal/NNP who/WP believes/VBZ in/IN the/DT application/NN of/IN the/DT Constitution/NNP </C>
<C>as/IN it/PRP is/AUX written/VBN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN the/DT majority/NN of/IN Americans/NNPS are/AUX against/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN the/DT majority/NN will/MD support/VB less/RBR ambiguous/JJ laws/NNS that/WDT ban/VBP gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN legalizing/VBG gay/JJ marriage/NN will/MD alleviate/VB the/DT state/NN 's/POS burden/NN on/IN support/NN for/IN the/DT dependent/JJ spouse/NN in/IN the/DT case/NN of/IN a/DT break-up/NN ./. </C>
</S>
<S>
<C>Without/IN gay/JJ marriage/NN ,/, the/DT state/NN has/AUX to/TO support/VB the/DT dependent/JJ spouse/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ that/IN a/DT federal/JJ amendment/NN to/TO the/DT US/NNP Constitution/NNP is/AUX unlike/IN over/IN the/DT next/JJ few/JJ years/NNS ./. </C>
</S>
</P>
</T>
