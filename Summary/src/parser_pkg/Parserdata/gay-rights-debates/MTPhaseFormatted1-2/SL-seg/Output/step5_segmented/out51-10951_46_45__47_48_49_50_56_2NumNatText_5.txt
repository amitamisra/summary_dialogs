<T>
<P>
</P>
<P>
<S>
<C>S1/NNP compares/VBZ being/AUXG against/IN gay/JJ marriage/NN to/TO the/DT Taliban/NNP and/CC Sharia/NNP law/NN ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ no/DT difference/NN in/IN those/DT that/WDT oppose/VBP gay/JJ marriage/NN for/IN religious/JJ reasons/NNS and/CC have/AUX laws/NNS against/IN it/PRP and/CC Muslims/NNPS that/WDT pass/VBP laws/NNS based/VBN on/IN religion/NN ./. </C>
</S>
<S>
<C>He/PRP finds/VBZ it/PRP hypocritical/JJ for/IN people/NNS to/TO be/AUX against/IN Sharia/NNP law/NN when/WRB they/PRP are/AUX ,/, in/IN his/PRP$ opinion/NN ,/, doing/VBG the/DT same/JJ thing/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ marriage/NN is/AUX a/DT legal/JJ contract/NN between/IN two/CD people/NNS and/CC the/DT state/NN </C>
<C>and/CC he/PRP supports/VBZ gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB agree/VB with/IN the/DT comparison/NN of/IN Sharia/NNP law/NN and/CC being/NN against/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP has/AUX not/RB advocated/VBN for/IN jail/NN time/NN or/CC the/DT death/NN of/IN homosexuals/NNS ./. </C>
</S>
<S>
<C>They/PRP are/AUX free/JJ to/TO do/AUX as/IN they/PRP wish/VBP ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ gay/JJ marriage/NN pushes/VBZ that/DT relationship/NN onto/IN others/NNS ,/, </C>
<C>and/CC he/PRP opposes/VBZ it/PRP ./. </C>
</S>
</P>
</T>
