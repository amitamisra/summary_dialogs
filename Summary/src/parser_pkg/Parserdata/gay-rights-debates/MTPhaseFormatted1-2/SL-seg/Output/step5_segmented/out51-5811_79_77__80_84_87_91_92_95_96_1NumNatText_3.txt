<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX prejudice/NN as/IN opposed/VBN to/TO religious/JJ or/CC moral/JJ beliefs/NNS which/WDT fuel/VB the/DT anti-gay/JJ agenda/NN ;/: </C>
<C>advising/VBG a/DT moral-bases/JJ system/NN does/AUX not/RB allow/VB for/IN the/DT intentional/JJ targeting/VBG of/IN another/DT and/CC also/RB religion/NN could/MD be/AUX cited/VBN </C>
<C>but/CC is/AUX not/RB based/VBN upon/IN dictating/NN how/WRB another/DT should/MD live/VB ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ all/DT religious/JJ based/VBN arguments/NNS regarding/VBG gay/JJ issues/NNS eventually/RB circle/NN around/IN to/TO prejudice/NN and/CC invites/VBZ S2/NNP </C>
<C>to/TO support/VB the/DT religious/JJ argument/NN without/IN this/DT happening/NN ./. </C>
</S>
<S>
<C>S1/NNP advises/VBZ there/EX is/AUX no/DT issue/NN with/IN people/NNS who/WP do/AUX not/RB try/VB to/TO push/VB their/PRP$ views/NNS on/IN others/NNS ./. </C>
</S>
<S>
<C>S2/NNP concedes/VBZ religion/NN is/AUX not/RB to/TO blame/VB in/IN and/CC of/IN itself/PRP and/CC eludes/VBZ to/TO the/DT point/NN that/IN often/RB times/NNS people/NNS who/WP are/AUX claiming/VBG a/DT religious/JJ or/CC moral/JJ reason/NN for/IN their/PRP$ actions/NNS are/AUX doing/VBG so/RB </C>
<C>after/IN having/AUXG gone/VBN against/IN the/DT premise/NN on/IN which/WDT their/PRP$ belief/NN is/AUX based/VBN ./. </C>
</S>
<S>
<C>S2/NNP also/RB advises/VBZ he/she/NN is/AUX against/IN same/JJ sex/NN relationships/NNS </C>
<C>because/IN the/DT Bible/NNP says/VBZ it/PRP 's/AUX wrong/JJ </C>
<C>but/CC does/AUX not/RB impose/VB his/her/JJR beliefs/NNS on/IN others/NNS and/CC is/AUX not/RB prejudice/NN ./. </C>
</S>
<S>
<C>S2/NNP refuses/VBZ to/TO give/VB the/DT summary/NN of/IN their/PRP$ religious/JJ argument/NN ./. </C>
</S>
</P>
</T>
