(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ notes) (NP (NP (DT a) (NN provision)) (PP (IN in) (NP (NN law))) (SBAR (WHNP (WDT that)) (S (VP (VBZ makes) (S (NP (NP (VBG performing) (NN marriage) (NNS ceremonies)) (PP (IN by) (NP (VBN sanctioned) (NN marriage) (NNS officers)))) (ADJP (JJ non-mandatory))) (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (VBZ comes) (PP (TO to) (NP (NP (JJ same) (NN sex) (NNS couples)) (CC and) (NP (NN marriage)))) (ADVP (RB as) (JJ long) (PP (IN as) (S (VP (VBG performing) (VP (VBD said) (SBAR (S (NP (NN ceremony)) (VP (MD would) (VP (NN conflict) (PP (IN with) (NP (NP (DT that) (NN person) (POS 's)) (NN conscience) (, ,) (NN religion) (CC and) (NN belief)))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (S (VP (VBG forcing) (S (NP (PRP them)) (VP (TO to) (VP (AUX do) (ADVP (RB so)) (PP (IN against) (NP (PRP$ their) (NNS beliefs)))))))) (VP (MD would) (VP (VB infringe) (PP (IN on) (NP (PRP$ their) (JJ religious) (NN freedom)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ unfortunate)) (SBAR (IN if) (S (NP (DT that)) (VP (VBZ applies) (PP (TO to) (NP (JJ public) (NNS officials))) (, ,) (SBAR (IN as) (S (NP (PRP they)) (VP (VBP believe) (SBAR (IN that) (S (NP (JJ public) (NNS officials)) (VP (MD must) (VP (VB serve) (NP (DT the) (NN public)) (PP (IN in) (NP (PRP$ its) (NN entirety))) (, ,) (ADVP (RB equally)) (, ,) (PP (IN under) (NP (DT the) (NN law))))))))))))))))) (. .)))

(S1 (S (NP (PRP He) (CC or) (PRP she)) (VP (VBZ feels) (SBAR (IN that) (S (SBAR (IN if) (S (NP (CD one) (NN cannot)) (VP (VB perform) (NP (DT the) (NN job))))) (, ,) (NP (RB then) (DT that) (NN person)) (VP (MD should) (VP (VB look) (PP (IN for) (NP (DT another) (NN job)))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT an) (NN impediment)) (PP (TO to) (NP (NP (DT the) (JJ public) (NN servant)) (VP (VBG performing) (NP (PRP$ his) (CC or) (PRP$ her) (NN job)) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ true))))))))))))) (. .)))

