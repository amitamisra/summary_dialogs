<s> <PARAGRAPH> </s>
<s>  Two people are discussing whether or not Christians want to ban gay people. </s>
<s> S1 contends that most Christians are not trying to ban the decriminalization of same-sex, it was just an accusation made. </s>
<s> He contends that evangelicals are rarely united for anything, let alone banning gay people. </s>
<s> He also states that gay lobbyists have long stepped over the line of simply counter-attacking with their defense. </s>
<s> S2 retorts by stating that a majority of self-identified Christians in the U.S. oppose civil marriage for gay people, among other things such as civil unions, gay adoptions and anti-discrimination laws. </s>
<s> He states that in Spain, they do not do that, and he points out a state constitutional amendment trying to prevent same-sex marriage. </s>
<s>  </s>
