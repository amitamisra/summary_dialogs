<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG the/DT spread/NN of/IN AIDS/NNP in/IN the/DT US/NNP as/IN part/NN of/IN a/DT discussion/NN on/IN gay/JJ marriage/NN in/IN the/DT USA/NNP ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN location/NN is/AUX relevant/JJ ,/, </C>
<C>as/IN the/DT way/NN AIDS/NNP spread/VBD in/IN Africa/NNP is/AUX different/JJ from/IN how/WRB it/PRP spreads/VBZ in/IN the/DT US/NNP ./. </C>
</S>
<S>
<C>He/PRP continues/VBZ to/TO state/VB that/IN AIDS/NNP spread/VBD in/IN the/DT US/NNP because/IN of/IN the/DT gay/JJ community/NN ,/, </C>
<C>and/CC the/DT gay/JJ community/NN continually/RB tries/VBZ to/TO deflect/VB the/DT argument/NN and/CC include/VB Africa/NNP in/IN the/DT discussion/NN </C>
<C>instead/RB of/IN providing/VBG real/JJ facts/NNS ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN bringing/VBG up/RP the/DT topic/NN of/IN AIDS/NNP is/AUX solely/RB as/IN a/DT weapon/NN against/IN gays/NNS ,/, </C>
<C>and/CC that/IN the/DT location/NN of/IN the/DT AIDS/NNP epidemic/NN is/AUX irrelevant/JJ to/TO the/DT discussion/NN ./. </C>
</S>
<S>
<C>He/PRP then/RB states/VBZ that/IN Africa/NNP is/AUX included/VBN </C>
<C>in/IN order/NN to/TO refute/VB the/DT opposing/JJ logic/NN ./. </C>
</S>
</P>
</T>
