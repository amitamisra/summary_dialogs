(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (S (NP (NN he/she)) (VP (AUX is) (UCP (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (NP (JJ civil) (NN union) (NNS rights)) (PP (IN for) (NP (JJ same) (NN sex) (NNS couples))))))) (CC but) (VP (VBN opposed) (PP (TO to) (NP (NN marriage) (NNS rights))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (SBAR (IN if) (S (NP (DT a) (JJ civil) (NN union)) (VP (AUX is) (S (VP (TO to) (VP (AUX be) (NP (NP (DT the) (JJ only) (NN right)) (VP (VBN given) (PP (TO to) (NP (JJ same) (NN sex) (NNS couples))))))))))) (, ,) (RB then) (NP (DT the) (JJ same)) (VP (MD should) (VP (AUX be) (ADJP (JJ true) (PP (IN for) (NP (NNS heterosexuals)))) (ADVP (IN as) (RB well))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NNP Bible)) (VP (MD should) (VP (AUX have) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (DT the) (NN law)) (, ,) (VP (VBG advising) (SBAR (S (NP (DT the) (NNP American) (NN government)) (VP (AUX does) (RB not) (VP (VB recognize) (NP (NN marriage)) (PP (IN as) (NP (NP (DT a) (NN sacrament)) (ADJP (JJ due) (PP (TO to) (NP (NP (DT the) (NN separation)) (PP (IN of) (NP (NN church) (CC and) (NN state))))))))))))))))))))))))) (. .)))

(S1 (S (SBAR (IN While) (S (NP (NNP S1)) (VP (VBZ agrees) (NP (NN church) (CC and) (NN state))))) (VP (AUX are) (NP (NP (JJ separate) (NNS entities)) (SBAR (S (NP (NN he/she)) (VP (VBZ believes) (SBAR (S (NP (NNS voters)) (VP (MD can) (VP (VB cast) (NP (DT a) (NN ballot)) (PP (IN in) (NP (NP (DT any) (NN direction)) (SBAR (S (NP (PRP they)) (ADVP (RB so)) (VP (VBP choose)))))) (, ,) (SBAR (WHNP (WDT which)) (S (ADVP (RB essentially)) (VP (VBZ boils) (PRT (RP down)) (PP (TO to) (NP (NP (DT each) (NN voter) (POS 's)) (JJ religious) (NNS beliefs))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (NP (NP (NN marriage) (NNS rights)) (PP (IN for) (NP (NNS gays) (NNS effects)))) (NP (NP (DT the) (JJ financial) (NN welfare)) (PP (IN of) (NP (DT every) (NN voter))))) (. .)))

