<PARAGRAPH> S1 argues that homosexuality is a disgusting habit.
He uses certain sexual practices common in the gay community as evidence.
Further, S1 argues that supporting gay rights shows the lack of moral character of supporters.
S2 retorts that in fact opposing gay rights shows a lack of moral character and accuses S1 of in fact being in denial of his own homosexuality.
He bases this accusation on S1's knowledge of aforementioned sexual practices and familiarity with gay cultural practices.
S1 argues that barring same sex marriage is an exercise of constitutional rights and that he has been happily committed to a heterosexual marriage for many years.
S2 argues that gay marriage was denied to married, loving gay couples and that being married to a woman is no indicator of one's true sexual preference.
S2 argues that S1's familiarity with gay practices points to him possibly being in denial or hiding his true sexual preference.
S1's repeated mocking of S2, according to S2 does little to hide the evidence of S1's true nature or sexual preference.
