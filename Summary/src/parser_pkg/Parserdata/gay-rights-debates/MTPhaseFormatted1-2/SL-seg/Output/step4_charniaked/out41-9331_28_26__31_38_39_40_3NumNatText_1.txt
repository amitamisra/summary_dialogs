(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ same) (NN sex) (NN marriage)) (VP (AUX is) (RB not) (NP (NP (DT the) (JJ same) (JJ essential) (NN component)) (PP (TO to) (NP (NP (NNS people) (POS 's)) (JJ general) (NN welfare))) (SBAR (IN that) (S (NP (NP (CD one) (NN man)) (CC and) (NP (CD one) (NN woman))) (VP (AUX is))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (NN society)) (VP (MD can) (VP (VB survive) (PP (IN without) (NP (JJ homosexual) (NNS unions) (, ,) (NN polygamy) (CC and) (NN bestiality))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (RB not) (VP (VB survive) (ADVP (RB long)) (PP (IN without) (NP (JJ heterosexual) (NN marriage))))))))) (. .)))

(S1 (S (NP (PRP He) (CC or) (PRP she)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX is) (ADVP (RB simply)) (VP (VBG disagreeing) (PP (IN on) (SBAR (WHADVP (WRB where)) (S (NP (DT the) (JJ legal) (NN line)) (VP (MD should) (VP (AUX be) (VP (VBN drawn) (PP (IN for) (NP (NN marriage))))))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NN government)) (VP (AUX has) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB impose) (NP (DT a) (JJ legal) (NN distinction)) (ADVP (RB somewhere))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (JJ homosexual) (NNS couples)) (CC and) (NP (NNS families))) (ADVP (DT all)) (VP (AUX have) (NP (NP (DT the) (JJ same) (JJ basic) (NNS needs)) (, ,) (NP (NP (NNS responsibilities)) (PP (TO to) (NP (CD one) (DT another)))) (CC and) (NP (NN society))) (SBAR (IN as) (S (NP (JJ heterosexual) (NNS people)) (VP (AUX do)))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (ADJP (JJ wrong) (PP (IN by) (S (VP (VBG rebutting) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (NN society)) (VP (MD could) (ADVP (RB also)) (VP (VB survive) (PP (IN without) (S (VP (VBG allowing) (NP (JJ many) (NNS minorities)) (NP (NP (DT the) (JJ same) (NNS rights)) (PP (IN as) (NP (DT the) (NN majority)))))))))))))))))))) (. .)))

