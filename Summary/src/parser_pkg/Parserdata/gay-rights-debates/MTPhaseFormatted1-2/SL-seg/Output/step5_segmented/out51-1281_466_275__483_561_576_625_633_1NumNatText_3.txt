<T>
<P>
</P>
<P>
<S>
<C>S1/NNP dominates/VBZ the/DT debate/NN ./. </C>
</S>
<S>
<C>He/PRP is/AUX against/IN using/VBG religion/NN ,/, specifically/RB Biblical/JJ references/NNS in/IN the/DT conversation/NN around/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>His/PRP$ first/JJ comment/NN seems/VBZ to/TO be/AUX arguing/VBG against/IN a/DT previous/JJ mention/NN of/IN increased/VBN earthquakes/NNS and/CC wars/NNS ./. </C>
</S>
<S>
<C>He/PRP also/RB argues/VBZ for/IN the/DT inclusion/NN and/CC support/NN of/IN minority/NN issues/NNS </C>
<C>despite/IN the/DT fact/NN that/IN we/PRP live/VBP in/IN a/DT majority/NN rule/NN state/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN first/RB ,/, most/JJS people/NNS fall/VBP into/IN some/DT sort/NN of/IN minority/NN classification/NN </C>
<C>and/CC second/JJ the/DT minorities/NNS are/AUX still/RB large/JJ in/IN number/NN ./. </C>
</S>
<S>
<C>There/EX is/AUX a/DT change/NN in/IN the/DT conversation/NN about/IN half/JJ way/NN through/IN to/TO genetics/NNP in/IN which/WDT compares/VBZ sexuality/NN to/TO a/DT predisposition/NN for/IN alcoholism/NN ./. </C>
</S>
<S>
<C>In/IN the/DT end/NN he/PRP feels/VBZ marriage/NN must/MD be/AUX consensual/JJ </C>
<C>and/CC that/IN any/DT parties/NNS may/MD be/AUX free/JJ to/TO leave/VB ,/, </C>
<C>whether/IN it/PRP is/AUX traditional/JJ ,/, homosexual/JJ or/CC polygamy/JJ ./. </C>
</S>
<S>
<C>S2/NNP mostly/RB argues/VBZ the/DT religious/JJ points/NNS ,/, for/IN example/NN citing/VBG the/DT increase/NN in/IN earthquakes/NNS outside/IN of/IN Israel/NNP in/IN recent/JJ years/NNS </C>
<C>though/IN he/PRP mentions/VBZ another/DT name/NN of/IN who/WP began/VBD the/DT conversation/NN around/IN religion/NN ./. </C>
</S>
<S>
<C>He/PRP questions/VBZ why/WRB the/DT debate/NN remains/VBZ around/IN homosexuals/NNS </C>
<C>and/CC whether/IN polygamists/NNS fall/VBP into/IN similar/JJ categories/NNS in/IN the/DT genetic/JJ argument/NN ./. </C>
</S>
</P>
</T>
