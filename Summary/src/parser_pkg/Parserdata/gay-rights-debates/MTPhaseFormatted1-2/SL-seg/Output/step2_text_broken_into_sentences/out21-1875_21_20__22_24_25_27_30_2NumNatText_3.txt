<PARAGRAPH> S1 and S2 are debating gay marriage and healthcare benefits.
S1 is against gay marriage as well as shared healthcare between gay couples.
S2 feels this objection points to S1's being in support of things such as segregation and prohibition.
S2 argues that gays do have the right to marry in order to qualify for healthcare, they just have to marry someone of the opposite sex, advising straight people and gay people have the same rights due to this.
S2 contends that due to the fact gay people cannot choose to marry someone they love, of the same sex, they do not have the same rights as heterosexuals and this is prejudice.
S2 believes gay marriage laws will be passed.
