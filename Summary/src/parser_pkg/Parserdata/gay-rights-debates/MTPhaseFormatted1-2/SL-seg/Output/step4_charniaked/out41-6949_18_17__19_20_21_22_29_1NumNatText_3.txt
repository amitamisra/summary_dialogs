(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN world)) (VP (VBD changed) (SBAR (WHADVP (WRB when)) (S (NP (NNP Massachusetts) (NN legislature)) (VP (VBD ratified) (NP (NP (DT a) (NN court) (NN decision)) (PP (IN for) (NP (JJ equal) (NNS rights))) (VP (VBG regarding) (NP (NNS homosexuals))))))))))) (. .)))

(S1 (S (NP (DT The) (NN speaker)) (ADVP (RB also)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN world)) (VP (AUX is) (RB not) (ADJP (ADJP (RB as) (JJ anti-gay)) (SBAR (IN as) (S (NP (PRP it)) (VP (VBD used) (S (VP (TO to) (VP (AUX be)))))))))))) (. .)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN speaker) (CD two)) (VP (AUX is) (ADJP (VBN opposed) (PP (TO to) (NP (JJ gay) (NN marriage)))) (SBAR (IN because) (S (NP (NN speaker) (CD two)) (VP (AUX does) (RB not) (VP (VB support) (NP (NP (JJ equal) (NNS rights)) (PP (IN for) (NP (NNS homosexuals)))))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VP (AUX has) (VP (VBN opposed) (NP (NP (JJ gay) (UCP (NN marriage) (CC and) (JJ likely)) (JJ equal) (NNS rights)) (PP (IN for) (NP (NP (NNS gays)) (PP (IN in) (NP (DT the) (NN past)))))))) (, ,) (CC but) (VP (NNS opinions) (SBAR (IN that) (S (NP (NP (DT the) (NN cause)) (PP (IN of) (NP (DT this) (NN opposition)))) (VP (AUX is) (SBAR (IN that) (S (NP (DT the) (NN speaker)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN government)) (VP (AUX is) (ADJP (JJ over-intruding) (PP (IN by) (S (VP (VBG creating) (NP (NP (NN legislature)) (VP (VBG regarding) (NP (NP (DT the) (NN topic)) (PP (IN of) (NP (JJ gay) (NN marriage))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VP (AUX has) (NP (DT a) (ADJP (RB very) (JJ conservative)) (NN stance)) (PP (IN in) (NP (NNS politics)))) (CC and) (VP (VBZ believes) (SBAR (S (NP (NN government)) (VP (MD should) (RB not) (VP (AUX be) (S (VP (VBG interfering) (PP (IN with) (NP (NP (NNS people) (POS 's)) (JJ personal) (NNS lives))))))))))) (. .)))

