<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage laws. </s>
<s> S1 contends that the current system the most votes win, to which S2 contends that the laws should be decided based on looking at the individuals that are affected. </s>
<s> S1 states that if one wants to change the law, he must get a majority of people to do it, and the majority should not be ignored because someone objects to them. </s>
<s> S2 states that the courts should look at the individual lives that will be affected and decide, and that laws that a majority favors that are unjust in an individuals life must be overruled. </s>
<s> S1 rebuts that that goes against how the courts were designed to work. </s>
<s>  </s>
