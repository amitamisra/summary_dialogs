(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ supports) (NP (JJ gay) (NN marriage))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (QP (IN at) (JJS least) (CD two)) (NNS men)) (PP (IN in) (NP (DT a) (JJ committed) (NN relationship)))) (VP (NN practice) (NP (NNP fidelity)) (PP (IN unlike) (NP (NP (NNS heterosexuals)) (PP (IN with) (NP (QP (JJR more) (IN than) (CD one)) (NN wife))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (S (NP (NNP fidelity)) (VP (TO to) (VP (AUX be) (NP (NP (NN part)) (PP (PP (IN of) (NP (DT the) (JJ normal) (JJ American) (NN attitude))) (, ,) (CC or) (ADVP (IN at) (JJS least)) (PP (IN of) (NP (NNS Christians))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (NP (NN opposition)) (PP (TO to) (NP (JJ gay) (NN marriage)))) (VP (VP (VBZ goes) (PP (IN beyond) (NP (NN party) (NNS lines)))) (CC and) (VP (VBZ wonders) (SBAR (WHADVP (WRB why)) (S (NP (NNS people)) (VP (AUX are) (ADJP (RB so) (JJ opposed) (PP (TO to) (NP (PRP it)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (NP (DT those)) (PP (IN with) (NP (QP (JJR more) (IN than) (CD one)) (NN wife)))) (VP (MD can) (ADVP (RB still)) (VP (AUX be) (VP (VBN committed))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ gives) (NP (NP (DT the) (NNS results)) (PP (IN of) (NP (NP (DT a) (NN poll)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (NP (CD 59) (NN percent)) (PP (IN of) (NP (NNPS Democrats)))) (VP (AUX are) (PP (IN against) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (IN if) (S (NP (NNPS Democrats)) (VP (AUX are) (NP (JJ extreme) (NNS Christians)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ views) (NP (NP (DT those)) (SBAR (WHNP (WDT that)) (S (VP (VBP favor) (NP (JJ gay) (NN marriage)) (PP (IN as) (S (VP (VBG forcing) (NP (PRP it)) (PP (IN upon) (NP (NNS others))))))))))) (CC and) (VP (VBZ believes) (SBAR (S (NP (NP (NNS homosexuals)) (PP (IN for) (NP (NP (JJ gay) (NN marriage)) (SBAR (S (VP (TO to) (VP (AUX be) (ADJP (JJ heterophobic) (PP (IN as) (NP (NP (DT those)) (PP (IN against) (NP (JJ gay) (NN marriage))))))))))))) (VP (AUX are) (VP (VBN seen) (PP (IN as) (ADJP (JJ homophobic))))))))) (. .)))

