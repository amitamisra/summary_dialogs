<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ in/IN a/DT conservative/JJ silent/JJ majority/NN that/WDT negates/VBZ liberal/JJ or/CC left/JJ polling/NN and/CC votes/VBZ in/IN a/DT politically/RB correct/JJ and/CC like/JJ minded/JJ conservative/JJ fashion/NN </C>
<C>in/IN order/NN to/TO defeat/VB liberal/JJ agendas/NNS such/JJ as/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB think/VB the/DT conservatives/NNS are/AUX silent/JJ </C>
<C>and/CC they/PRP both/DT cite/VBP various/JJ election/NN results/NNS from/IN the/DT past/NN </C>
<C>to/TO debate/VB the/DT premise/NN that/IN there/EX is/AUX a/DT silent/JJ conservative/JJ majority/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB there/EX is/AUX a/DT silent/JJ conservative/JJ majority/NN and/CC offers/VBZ statistics/NNS to/TO suggest/VB that/IN President/NNP Clinton/NNP was/AUX better/RB supported/VBN by/IN the/DT electorate/NN than/IN President/NNP George/NNP W./NNP Bush/NNP ./. </C>
</S>
<S>
<C>S1/NNP is/AUX clearly/RB a/DT republican/JJ and/CC repeatedly/RB refers/VBZ to/TO President/NNP Clinton/NNP as/IN ``/`` Slick/JJ Willie/NNP ''/'' ,/, </C>
<C>whereas/IN S2/NNP is/AUX somewhat/RB more/RBR guarded/VBN about/IN his/PRP$ party/NN affiliation/NN in/IN an/DT apparent/JJ attempt/NN to/TO argue/VB the/DT matter/NN from/IN a/DT point/NN of/IN neutrality/NN ,/, </C>
<C>both/DT asserting/VBG Clinton/NNP 's/POS superior/JJ level/NN of/IN support/NN over/IN Bush/NNP ,/, </C>
<C>and/CC simultaneously/RB implying/VBG that/IN he/PRP ,/, S2/NNP ,/, is/AUX not/RB a/DT Clinton/NNP supporter/NN ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ that/IN Kerry/NNP expected/VBD to/TO win/VB Ohio/NNP </C>
<C>and/CC that/IN the/DT silent/JJ majority/NN surprised/VBD him/PRP ./. </C>
</S>
<S>
<C>S2/NNP replies/VBZ that/IN Kerry/NNP was/AUX not/RB so/RB sure/JJ he/PRP was/AUX going/VBG to/TO win/VB Ohio/NNP ./. </C>
</S>
</P>
</T>
