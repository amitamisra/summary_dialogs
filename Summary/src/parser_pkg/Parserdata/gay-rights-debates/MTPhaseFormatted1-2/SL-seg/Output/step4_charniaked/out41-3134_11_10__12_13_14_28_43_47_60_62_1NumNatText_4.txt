(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (NP (DT a) (JJR better) (NN comparison)) (VP (AUX is) (NP (NNP Sodom)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (NP (CD 100) (NNS men)) (PP (IN in) (NP (DT a) (NNP daisy) (NN chain)))) (VP (VBG performing) (NP (JJ unprotected) (NN sex)) (PP (IN on) (NP (CD one) (DT another))))) (VP (MD would) (VP (AUX be) (PP (IN in) (NP (NP (NN comparison)) (PP (TO to) (NP (NNP Sodom)))))))))) (. .)))

(S1 (S (NP (NP (NN Everyone)) (ADJP (RB else))) (VP (MD should) (VP (VB condemn) (NP (PRP it)))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ informs) (NP (NP (NNP S2)) (PP (IN of) (NP (NP (DT the) (NNS bathhouses)) (SBAR (WHNP (WDT that)) (S (VP (MD can) (VP (AUX be) (VP (VBN looked) (PRT (RP up)))))))))) (, ,) (SBAR (S (NP (PRP they)) (VP (AUX were) (VP (VBN shut) (PRT (RP down)) (SBAR (WHADVP (WRB when)) (S (NP (NNP AIDS)) (ADVP (RB first)) (VP (VP (VBD came) (PRT (RP out))) (CC and) (VP (AUX are) (VP (VBG flourishing) (ADVP (RB once) (RB again)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ asks) (NP (NNP S2)) (S (VP (TO to) (VP (VB go) (PP (TO to) (NP (NP (DT a) (VB cite)) (VP (VB link) (S (VP (TO to) (VP (VB see) (SBAR (WHADVP (WRB how)) (S (NP (NNS gays)) (VP (AUX are) (VP (VBG trying) (S (VP (TO to) (VP (VB reopen) (NP (NP (NN bath) (NNS houses)) (PP (IN in) (NP (NNP San) (NNP Francisco))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NNP S1) (POS 's)) (NNS comments)) (VP (VBP make) (S (NP (PRP him)) (NP (DT a) (NN bigot))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ proclaims) (SBAR (IN that) (S (NP (PRP he)) (ADVP (RB just)) (VP (AUX does) (RB not) (VP (VB believe) (NP (NN anything)) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG saying)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ determines) (SBAR (IN that) (S (S (NP (NNP S2)) (VP (AUX is) (VP (VBG making) (NP (NNS claims)) (PP (IN without) (NP (DT any) (NN proof)))))) (CC and) (S (NP (PRP he)) (VP (VBZ wants) (NP (NP (DT the) (NNS claims)) (VP (VBN justified) (CC or) (VBN taken))) (ADVP (RB back))))))) (, ,) (RB then) (VP (VBZ calls) (S (NP (NNP S1)) (ADJP (JJ irresponsible))))) (. .)))

