(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT The) (NN issue)) (VP (AUX is) (S (ADVP (RB here)) (VP (AUX is) (UCP (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (, ,) (CC and) (SBAR (WHNP (WP what)) (S (NP (NN government) (NN body/level)) (VP (AUX has) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB change) (NP (DT this) (NN definition))))))))))))) (. .)))

(S1 (S (NP (NN Speaker) (CD one)) (VP (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (NNS courts)) (CC and) (NP (JJ federal) (NN government))) (VP (MD should) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB rule) (PP (IN on) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))))))))))))) (CC and) (VP (VBZ cites) (NP (JJ several) (JJ previous) (NNS cases)) (PP (IN in) (NP (NP (DT the) (NN past)) (SBAR (WHADVP (WRB where)) (S (NP (DT the) (JJ federal) (NN government)) (VP (VBD stepped) (PRT (RP up)) (S (VP (TO to) (VP (VB introduce) (NP (NN legislature)))))))))))) (. .)))

(S1 (S (NP (DT This) (NN speaker)) (VP (VBZ believes) (SBAR (IN that) (S (`` ``) (NP (JJ fundamental) (NNS rights)) ('' '') (VP (VP (AUX are) (RB not) (ADJP (JJ subject) (PP (TO to) (NP (NN state) (NNS votes))))) (CC and) (VP (MD can) (VP (AUX be) (VP (VBN decided) (PP (IN by) (NP (NN government)))))))))) (. .)))

(S1 (S (NP (NNP Speaker) (CD two)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT each) (NN state)) (VP (MD should) (VP (VB decide) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage) (CC and) (NNS questions)))) (SBAR (WHNP (WP what)) (S (`` ``) (NP (JJ fundamental) (NNS rights)) ('' '') (VP (AUX have) (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NP (DT the) (`` ``) (NN definition) ('' '')) (PP (IN of) (NP (NN marriage)))))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN speaker)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (VP (VBN left) (PRT (RP up)) (PP (TO to) (NP (DT a) (NN vote) (S (VP (TO to) (VP (VB decide) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage))))))))))))) (CC and) (SBAR (IN that) (S (NP (NN legislature)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN confused) (PP (IN with) (NP (NN judiciary)))))))))) (. .)))

