<PARAGRAPH> Two people are discussing whether or not being gay is a choice.
S1 contends that people are born black, but they can control their zippers to avoid gay sex.
He jokes that if gay people do not have a choice then S2 had to be on his guard in shopping malls and he states that gay sex is in violation of the Word of God.
S2 rebuts, stating that gay people do not have a choice, and refutes S1's joke by stating that they are discussing sexuality, not action.
He states that being gay or straight is something that happens, and based on S1's logic gay people should stay celibate simply because someone will not like what they do.
