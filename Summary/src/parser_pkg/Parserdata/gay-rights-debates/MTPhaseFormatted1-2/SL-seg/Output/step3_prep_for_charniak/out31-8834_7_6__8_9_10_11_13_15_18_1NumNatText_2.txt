<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage. </s>
<s> S1 states that in the next 20 years the wave of gay marriage legalization will be over, to which S2 states that if Congress removes the Defense of Marriage Act, the full faith and credit clause is a clear constitutional mandate. </s>
<s> S1 rebuts saying that even without the federal DOMA, the choice of law doctrine means that states can choose not to recognize gay marriage, or even be forced to recognize another state's gay marriage. </s>
<s> He also states that the full faith and credit clause has never been used in that way, and due to the lengthy legal argument involved it is unlikely to be used. </s>
<s> To contradict this, S2 states that each Justice will vote to side with gay marriage, and there is a current tradition of one state respecting another's marriage license when it comes to heteros, a tradition that will be kept if gay marriage laws are passed. </s>
<s>  </s>
