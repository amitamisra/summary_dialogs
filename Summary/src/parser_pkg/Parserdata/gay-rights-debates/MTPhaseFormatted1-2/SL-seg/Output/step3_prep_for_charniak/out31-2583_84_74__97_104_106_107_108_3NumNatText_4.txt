<s> <PARAGRAPH> </s>
<s>  Two people are discussing the benefit to society that same-sex marriage has. </s>
<s> S1 concedes that same-sex marriage does not have the negative financial impact that he thought it would. </s>
<s> He states his wish to move past fiscal concerns and move ont the the moral and health concerns of same-sex marriage. </s>
<s> S2 contends that he wishes to discuss the practical benefits. </s>
<s> He states that the discussion of "morality" is a safe ground because morality is relative and subjective. </s>
<s> S1 retorts by stating that he does not feel the need to impose his morality on anyone; he is willing to let society determine its own limits. </s>
<s> He also states his wish that S2 inform him of the benefits of same-sex marriage. </s>
<s>  </s>
