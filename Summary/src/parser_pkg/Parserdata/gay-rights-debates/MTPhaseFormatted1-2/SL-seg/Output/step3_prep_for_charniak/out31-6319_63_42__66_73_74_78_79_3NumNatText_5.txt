<s> <PARAGRAPH> </s>
<s>  Two people are discussing gay marriage. </s>
<s> The discussion revolves around whether or not a clergy person should have the right to refuse to marry a gay couple based on religious beliefs. </s>
<s> S1 believes this is alright to do while S2 does not agree. </s>
<s> S2 provides an example of his/her managing a restaurant asking if it would be acceptable for him to refuse to serve a boy scout based on the fact they conflict with his/her religious beliefs. </s>
<s> He/she also believes clergy members should not be permitted to refuse a union due to the fact the tax payers pay the salary of such people. </s>
<s> S1 feels forcing this issue is the equivalent to forcing a Jewish person to eat pork sandwiches. </s>
<s>  </s>
