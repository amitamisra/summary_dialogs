<s> <PARAGRAPH> </s>
<s>  S1 argues that the cultural meaning of marriage is a formalization of a mating pair, who create and support a family unit. </s>
<s> They believe that in order for homosexuals to be included, the definition and cultural meaning of marriage would have to change to accommodate it. </s>
<s> They argue that the most dominant cultural view on marriage is based upon evolutionary sound logic, that a monogamous pair of individuals will reproduce. </s>
<s> This person does not believe that the argument stands on equality because they do not believe that equality factors into the evolutionary view of marriage. </s>
<s> S2 argues against the idea that there is a set, evolutionary definition of marriage and uses the example that people well over the age of reproduction are not only legally capable of getting married, but also suffer no special stigma for doing so. </s>
<s> They believe what defines marriage is love between two people who desire a commitment between themselves. </s>
<s> They argue that many do not share S1's definition of marriage and that it's an issue of personal belief rather than culture. </s>
<s>  </s>
