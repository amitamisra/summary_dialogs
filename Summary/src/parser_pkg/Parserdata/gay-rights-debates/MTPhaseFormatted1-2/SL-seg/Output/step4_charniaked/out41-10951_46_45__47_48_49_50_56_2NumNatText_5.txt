(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ compares) (S (VP (AUXG being) (PP (IN against) (NP (NP (JJ gay) (NN marriage)) (PP (TO to) (NP (DT the) (NNP Taliban) (CC and) (NNP Sharia) (NN law)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ sees) (NP (NP (DT no) (NN difference)) (PP (IN in) (NP (NP (DT those)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBP oppose) (NP (NP (JJ gay) (NN marriage)) (PP (IN for) (NP (JJ religious) (NNS reasons))))) (CC and) (VP (AUX have) (NP (NP (NNS laws)) (PP (IN against) (NP (NP (PRP it)) (CC and) (NP (NNPS Muslims)))) (SBAR (WHNP (WDT that)) (S (VP (VBP pass) (NP (NP (NNS laws)) (PP (VBN based) (PP (IN on) (NP (NN religion))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ finds) (S (NP (PRP it)) (ADJP (JJ hypocritical)) (SBAR (IN for) (S (NP (NNS people)) (VP (TO to) (VP (AUX be) (PP (IN against) (NP (NNP Sharia) (NN law))) (SBAR (WHADVP (WRB when)) (S (NP (PRP they)) (VP (AUX are) (, ,) (PP (IN in) (NP (PRP$ his) (NN opinion))) (, ,) (VP (VBG doing) (NP (DT the) (JJ same) (NN thing)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (S (NP (NN marriage)) (VP (AUX is) (NP (NP (DT a) (JJ legal) (NN contract)) (PP (IN between) (NP (NP (CD two) (NNS people)) (CC and) (NP (DT the) (NN state))))))) (CC and) (S (NP (PRP he)) (VP (VBZ supports) (NP (JJ gay) (NN marriage))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB agree) (PP (IN with) (NP (NP (DT the) (NN comparison)) (PP (IN of) (NP (NNP Sharia))))) (NP (NP (NN law) (CC and) (NN being)) (PP (IN against) (NP (JJ gay) (NN marriage)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX has) (RB not) (VP (VBN advocated) (PP (IN for) (NP (NP (NN jail) (NN time)) (CC or) (NP (NP (DT the) (NN death)) (PP (IN of) (NP (NNS homosexuals)))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX are) (ADJP (JJ free) (S (VP (TO to) (VP (AUX do) (SBAR (IN as) (S (NP (PRP they)) (VP (VBP wish))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (VBZ pushes) (NP (DT that) (NN relationship)) (PP (IN onto) (NP (NNS others)))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ opposes) (NP (PRP it)))) (. .)))

