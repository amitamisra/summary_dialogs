(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (JJ religious) (NNS beliefs)) (VP (AUX are) (VP (VBG preventing) (NP (NP (DT the) (NN recognition)) (PP (IN of) (NP (JJ homosexual) (NNS marriages))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ compares) (S (VP (VBG basing) (NP (NN law)) (PP (IN on) (NP (NNP Christian) (NNS beliefs) (S (VP (TO to) (VP (AUX be) (NP (NP (DT the) (JJ same) (NN thing)) (PP (IN as) (S (NP (NNP Islamists)) (VP (VBG advocating) (NP (NNP Sharia) (NN law)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (DT the) (JJ religious) (NN belief)) (NP (DT that) (NN marriage)) (VP (AUX is) (PP (IN between) (NP (DT a) (NN man))))) (CC and) (S (NP (DT a) (NN woman)) (VP (MD should) (VP (AUX have) (NP (NP (DT no) (NN influence)) (PP (IN on) (NP (DT a) (JJ civil) (NN marriage) (NN contract)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (RB not) (VP (AUXG being) (VP (VBN persecuted) (PP (IN through) (NP (NP (NNS things)) (PP (IN like) (NP (NP (NN execution)) (, ,) (NP (NN imprisonment)) (, ,) (CC or) (NP (JJ mandatory) (NN therapy))))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB actually)) (NP (NP (DT the) (JJ homosexual) (NN lobby)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (VP (VBG pressing) (NP (NP (PRP$ their) (NNS beliefs)) (PP (IN about) (NP (NN marriage)))) (PP (IN onto) (NP (NP (NN everyone)) (ADJP (RB else)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (JJ homosexual) (NN marriage)) (VP (AUX has) (RB not) (VP (AUX been) (VP (VBN banned) (PP (IN in) (NP (DT any) (NN state))))))))) (, ,) (SBAR (SBAR (S (NP (PRP it)) (VP (AUX is) (ADVP (RB simply)) (RB not) (VP (VBN recognized))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS people)) (VP (AUX are) (ADJP (JJ free) (S (VP (VP (TO to) (VP (VB sign) (SBAR (WHNP (WDT whatever) (NNS contracts)) (S (NP (PRP they)) (VP (VBP want)))))) (CC and) (VP (TO to) (VP (VB engage) (PP (IN in) (NP (NP (NN intercourse)) (PP (IN with) (NP (NP (NN whomever)) (SBAR (S (NP (PRP they)) (VP (VBP want)))))))))))))))))) (. .)))

