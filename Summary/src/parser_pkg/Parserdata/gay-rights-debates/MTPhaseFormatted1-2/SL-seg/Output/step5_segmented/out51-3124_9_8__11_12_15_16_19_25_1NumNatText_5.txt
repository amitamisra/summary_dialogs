<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB agree/VB with/IN the/DT comparison/NN of/IN gay/JJ rights/NNS and/CC gay/JJ marriage/NN to/TO African/NNP American/NNP civil/JJ rights/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ it/PRP is/AUX an/DT insult/NN to/TO all/DT African/NNP Americans/NNPS ./. </C>
</S>
<S>
<C>He/PRP asks/VBZ for/IN references/NNS of/IN Coretta/NNP Scott/NNP King/NNP and/CC Julian/NNP Bond/NNP being/AUXG for/IN gay/JJ marriage/NN and/CC is/AUX surprised/VBN to/TO get/VB them/PRP ./. </C>
</S>
<S>
<C>He/PRP then/RB amends/VBZ his/PRP$ statement/NN from/IN all/DT African/NNP Americans/NNPS to/TO most/RBS African/JJ Americans/NNPS ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/RB to/TO wonder/VB what/WP MLK/NNP would/MD have/AUX had/AUX to/TO say/VB on/IN the/DT subject/NN ./. </C>
</S>
<S>
<C>He/PRP considers/VBZ MLK/NNP to/TO have/AUX far/RB more/JJR credibility/NN than/IN either/DT of/IN the/DT two/CD people/NNS referenced/VBN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB agree/VB all/DT African/NNP Americans/NNPS are/AUX against/IN gay/JJ marriage/NN </C>
<C>or/CC that/IN it/PRP 's/AUX an/DT insult/NN and/CC posts/NNS two/CD different/JJ reference/NN links/NNS to/TO both/DT Coretta/NNP Scott/NNP King/NNP and/CC Julian/NNP Bond/NNP being/AUXG for/IN gay/JJ marriage/NN ./. </C>
</S>
</P>
</T>
