<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX uncomfortable/JJ with/IN the/DT study/NN </C>
<C>because/IN there/EX is/AUX not/RB information/NN on/IN how/WRB they/PRP got/VBD the/DT results/NNS ./. </C>
</S>
<S>
<C>S1/NNP goes/VBZ on/RB to/TO say/VB the/DT study/NN vilifies/VBZ a/DT group/NN of/IN people/NNS ,/, namely/RB Christians/NNS ,/, </C>
<C>and/CC that/IN most/JJS people/NNS in/IN the/DT country/NN may/MD identify/VB as/IN Christian/NNP ./. </C>
</S>
<S>
<C>S1/NNP also/RB thinks/VBZ the/DT divorce/NN rate/NN of/IN Christians/NNS has/AUX no/DT bearing/NN on/IN the/DT issue/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ S1/NNP should/MD ask/VB questions/NNS about/IN the/DT study/NN rather/RB than/IN just/RB assume/VB it/PRP is/AUX incorrect/JJ </C>
<C>and/CC that/IN it/PRP is/AUX not/RB being/AUXG used/VBN to/TO vilify/VB anyone/NN ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ the/DT results/NNS are/AUX professional/JJ </C>
<C>and/CC S1/NNP is/AUX just/RB uncomfortable/JJ with/IN the/DT results/NNS ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ the/DT divorce/NN rate/NN of/IN Christians/NNS is/AUX very/RB important/JJ to/TO the/DT issue/NN </C>
<C>because/IN Christians/NNS say/VBP they/PRP are/AUX protecting/VBG the/DT ``/`` sanctity/NN ''/'' of/IN marriage/NN ./. </C>
</S>
</P>
</T>
