<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG the/DT subject/NN of/IN gay/JJ marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX against/IN gay/JJ marriage/NN </C>
<C>but/CC adamantly/RB states/VBZ his/PRP$ objection/NN is/AUX not/RB based/VBN on/IN religion/NN </C>
<C>but/CC because/IN there/EX are/AUX laws/NNS prohibiting/VBG the/DT practice/NN ./. </C>
</S>
<S>
<C>S2/NNP is/AUX in/IN support/NN of/IN gay/JJ marriage/NN and/CC argues/VBZ laws/NNS could/MD also/RB be/AUX passed/VBN to/TO ban/VB things/NNS such/JJ as/IN worshiping/VBG God/NNP and/CC begs/VBZ the/DT question/NN of/IN </C>
<C>whether/IN or/CC not/RB S1/NNP would/MD so/RB adamantly/RB support/VB the/DT laws/NNS he/PRP cites/VBZ </C>
<C>if/IN that/DT were/AUX the/DT case/NN ./. </C>
</S>
<S>
<C>S1/NNP advises/VBZ he/PRP would/MD still/RB support/VB the/DT law/NN </C>
<C>as/IN there/EX are/AUX already/RB laws/NNS banning/VBG certain/JJ religious/JJ practices/NNS such/JJ as/IN polygamy/NN ./. </C>
</S>
<S>
<C>He/PRP advises/VBZ that/IN even/RB though/IN such/PDT a/DT thing/NN would/MD most/RBS likely/RB not/RB happen/VB ,/, </C>
<C>he/PRP would/MD still/RB support/VB it/PRP </C>
<C>because/IN it/PRP would/MD be/AUX fair/JJ and/CC equal/JJ toward/IN all/DT ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ that/IN if/IN this/DT were/AUX the/DT case/NN ,/, </C>
<C>crime/NN rate/NN would/MD increase/VB due/JJ to/TO the/DT banning/VBG of/IN religious/JJ freedom/NN ,/, in/IN a/DT sense/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ homosexuality/NN should/MD be/AUX considered/VBN a/DT crime/NN </C>
<C>because/IN sodomy/NN is/AUX a/DT crime/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ S1/NNP is/AUX distorting/VBG the/DT law/NN to/TO justify/VB homosexuals/NNS being/AUXG discriminated/VBN against/IN ./. </C>
</S>
<S>
<C>S1/NNP denies/VBZ this/DT accusation/NN ./. </C>
</S>
</P>
</T>
