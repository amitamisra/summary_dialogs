(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (JJ silent) (NN majority)) (PP (IN in) (NP (NNP Ohio)))) (VP (VBD spoke) (PRT (RP out)) (PP (IN about) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (NP (NP (DT the) (NN state) (POS 's)) (NN passing)) (PP (IN of) (NP (DT a) (JJ gay) (NNS rights) (NN amendment))))))))))) (, ,) (CC and) (VP (VBZ compares) (NP (PRP it)) (PP (TO to) (NP (NP (DT the) (JJ large) (NN percentage)) (PP (IN of) (NP (NP (NNS voters)) (PP (IN in) (NP (JJ past) (JJ presidential) (NNS elections))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (DT the) (NNS morals) (CC and) (NNS beliefs)) (PP (IN in) (NP (DT the) (NN state)))) (VP (AUX were) (NP (NP (NP (DT the) (NNS reasons)) (PP (IN for) (NP (DT the) (JJ Republican) (JJ presidential) (NN candidate)))) (, ,) (NP (NP (NNP George) (NNP Bush)) (, ,) (NP (NP (VBG winning)) (PP (IN by) (NP (NP (DT a) (JJR larger) (NN percentage)) (PP (IN than) (NP (DT either) (JJ democratic) (NN candidate)))))) (, ,) (NP (NNP John) (NNP Kerry)) (CC and) (NP (NNP Bill) (NNP Clinton)))) (, ,) (PP (IN in) (NP (DT the) (JJ past) (NNS elections))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ rebuts) (SBAR (IN that) (S (S (NP (NP (DT the) (NN majority)) (PP (IN of) (NP (NP (NNS people)) (PP (IN in) (NP (NNP Ohio)))))) (VP (AUX were) (RB not) (ADJP (JJ silent) (PP (IN about) (NP (NP (PRP$ their) (NNS views)) (PP (PP (IN on) (NP (JJ gay) (NN marriage))) (CC or) (PP (IN on) (NP (NP (PRP$ their) (NN choice)) (PP (IN for) (NP (NN president))))))))))) (: ;) (S (NP (NP (DT the) (NNS people)) (PP (IN in) (NP (DT that) (NN state)))) (VP (AUX were) (ADJP (RB very) (JJ vocal) (PP (IN about) (NP (PRP$ their) (NNS views))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (NNP Bush)) (VP (AUX did) (VP (VB win) (PP (IN by) (NP (NP (DT a) (JJR larger) (NN percentage)) (PP (IN than) (NP (NNP Clinton)))))))) (, ,) (S (NP (NP (DT the) (NN increase)) (PP (IN in) (NP (NN population)))) (VP (AUX was) (NP (NP (DT the) (VBG determining) (NN factor)) (PP (IN for) (NP (DT those) (NNS numbers))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (NN majority) (POS 's)) (NN desire) (S (VP (TO to) (VP (VB oppress) (NP (DT the) (NN minority)))))) (, ,) (PP (IN in) (NP (NNP Ohio))) (, ,) (VP (AUX did) (VP (VB play) (NP (DT some) (NN factor)) (PP (IN in) (NP (NP (NNP Bush) (POS 's)) (NN win)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ uses) (NP (NP (NNS percentages)) (PP (IN from) (NP (JJ various) (NNS candidates)))) (, ,) (S (NP (NN likeClinton) (, ,) (NNP Bush) (, ,) (NNP Dole) (, ,) (NNP Kerry) (CC and) (NNP Gore)) (VP (TO to) (VP (VB back) (PRT (RP up)) (NP (PRP$ his) (NN argument)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ disagrees) (SBAR (IN that) (S (NP (NNP Bush)) (VP (AUX was) (ADJP (RBR more) (VBN supported) (PP (IN than) (NP (NNP Clinton)))))))) (. .)))

