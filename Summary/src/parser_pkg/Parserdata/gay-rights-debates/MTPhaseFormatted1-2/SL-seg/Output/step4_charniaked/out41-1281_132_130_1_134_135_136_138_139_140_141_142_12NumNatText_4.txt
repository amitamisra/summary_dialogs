(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ claims) (SBAR (IN that) (S (NP (JJ traditional) (, ,) (JJ heterosexual) (NN marriage)) (VP (AUX is) (NP (NP (DT a) (JJ universal) (JJ human) (NN institution) (VBG existing)) (PP (IN in) (NP (ADJP (RB almost) (DT every)) (NN culture))) (PP (IN across) (NP (DT the) (NNP Earth)))))))) (CC and) (VP (VBZ compares) (NP (NP (JJ traditional) (NN marriage)) (PP (TO to) (NP (DT the) (NNP Parthenon)))) (PP (IN as) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NP (DT an) (JJ ancient) (NN structure)) (SBAR (WHNP (WDT that)) (S (VP (VP (AUX is) (VP (VBN damaged))) (CC but) (VP (VBZ deserves) (S (VP (TO to) (VP (AUX be) (VP (VBN preserved))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (DT some) (JJ homosexual) (NNS unions)) (VP (MD would) (VP (VB occur) (ADVP (RB simply)) (S (VP (TO to) (VP (VB demonstrate) (SBAR (SBAR (IN that) (S (NP (NN homosexuality)) (VP (AUX is) (ADJP (JJ equal) (PP (TO to) (NP (NN heterosexuality))))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ acceptable)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (RB totally) (JJ unacceptable) (S (VP (TO to) (VP (VB revise) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage))))))))))) (, ,) (CC but) (SBAR (IN that) (S (NP (EX there)) (VP (MD would) (VP (AUX need) (S (VP (TO to) (VP (AUX be) (NP (NP (DT a) (ADJP (RB very) (JJ good)) (NN reason)) (SBAR (S (VP (TO to) (VP (AUX do) (ADVP (RB so)))))))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NN homosexuality)) (VP (AUX is) (ADVP (RB simply)) (NP (NP (DT an) (NN eccentricity)) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (RB not) (ADJP (ADJP (JJ equal) (PP (IN in) (NP (NN value)))) (PP (TO to) (NP (NN heterosexuality))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (S (VP (VBG claiming) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX has) (ADVP (RB always)) (VP (AUX been) (ADJP (JJ heterosexual)))))))) (VP (AUX is) (NP (JJ circular) (NN logic)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ asserts) (SBAR (IN that) (S (S (S (VP (VBG legalizing) (NP (JJ homosexual) (NN marriage)))) (VP (MD would) (RB not) (VP (VB damage) (NP (JJ heterosexual) (NNS marriages))))) (, ,) (CC but) (S (NP (PRP it)) (VP (MD would) (VP (VB allow) (S (NP (JJ homosexual) (NNS couples)) (VP (TO to) (VP (VB receive) (NP (NP (DT the) (NNS rights) (CC and) (NNS protections)) (VP (ADVP (RB currently)) (VBN afforded) (PP (ADVP (RB only)) (TO to) (NP (JJ heterosexual) (NNS couples)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (SBAR (RB just) (IN because) (S (NP (NN something)) (VP (AUX is) (ADJP (JJ traditional))))) (VP (AUX does) (RB not) (VP (VB guarantee) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ right)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBD admitted) (PP (TO to) (S (VP (VBG believing) (SBAR (S (NP (NNS homosexuals)) (VP (AUX were) (PP (IN of) (NP (JJR lesser) (NN value))))))))))))) (. .)))

