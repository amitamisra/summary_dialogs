(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (S (VP (VBG banning) (NP (JJ same-sex) (NN marriage)))) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (JJ racial) (NN segregation)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX is) (VP (VBG misrepresenting) (NP (PRP$ his) (NN argument)))))) (CC and) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX was) (RB not) (S (ADVP (RB actually)) (VP (VBG implying) (SBAR (IN that) (S (NP (NP (DT the) (NN situation)) (PP (IN for) (NP (NNS homosexuals)))) (VP (AUX is) (PP (RB completely) (IN unlike) (NP (NP (DT that)) (PP (IN for) (NP (NNS blacks))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB understand) (NP (DT the) (NN point)) (SBAR (S (NP (PRP he)) (VP (AUX is) (VP (VBG trying) (S (VP (TO to) (VP (VB make) (, ,) (SBAR (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX is) (ADJP (RB simply) (JJ wrong))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNP S2)) (VP (AUX has) (ADVP (RB only)) (VP (VBN succeeded) (PP (IN in) (NP (NP (NN discrediting)) (NP (PRP himself))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ rejects) (NP (NP (NNP S1) (POS 's)) (NN claim) (SBAR (IN that) (S (S (VP (VBG banning) (NP (JJ same-sex) (NN marriage)))) (VP (AUX is) (NP (NP (NN nothing)) (PP (IN like) (NP (JJ racial) (NN segregation))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB exactly)) (PP (IN like) (NP (NP (JJ racial) (CC or) (DT any) (JJ other) (NN type)) (PP (IN of) (NP (NN segregation))) (SBAR (WHNP (WDT that)) (S (VP (AUX has) (ADVP (RB ever)) (VP (VBN occurred))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (SBAR (IN that) (S (SBAR (WHADVP (WRB whenever)) (S (NP (NP (DT a) (JJ discriminatory) (NN behavior)) (PP (IN like) (NP (NP (NN racism)) (, ,) (NP (NN sexism)) (, ,) (NP (NN anti-Semetism)) (, ,) (CC or) (NP (NN homophobia))))) (VP (AUX is) (VP (VBN challenged))))) (, ,) (NP (DT the) (NNS perpetrators)) (VP (VBP claim) (SBAR (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (NP (DT the) (JJ same)) (PP (IN as) (NP (JJ previous) (NNS instances)))))))))) (, ,) (CC but) (SBAR (IN that) (S (NP (NP (DT all) (NNS instances)) (PP (IN of) (NP (NN discrimination)))) (VP (AUX are) (ADVP (RB inherently)) (NP (DT the) (JJ same))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ interprets) (NP (NP (NNP S1) (POS 's)) (NN argument)) (PP (IN as) (S (VP (VBG implying) (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (RB not) (VP (AUX be) (ADJP (RB as) (JJ bad)) (SBAR (IN if) (S (NP (NN society)) (VP (VBN treated) (NP (NNS homosexuals)) (NP (NP (DT the) (JJ same) (NN way)) (SBAR (S (NP (PRP they)) (VP (VBD treated) (NP (NNS blacks)) (PP (IN in) (NP (DT the) (NN past)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ attacks) (NP (NNP S1)) (, ,) (S (VP (VBG claiming) (SBAR (IN that) (S (NP (JJ personal) (NNS attacks)) (VP (AUX are) (RB not) (NP (NP (DT an) (JJ effective) (NN method)) (PP (IN of) (S (VP (VBG validating) (NP (DT an) (NN argument)))))))))))) (. .)))

