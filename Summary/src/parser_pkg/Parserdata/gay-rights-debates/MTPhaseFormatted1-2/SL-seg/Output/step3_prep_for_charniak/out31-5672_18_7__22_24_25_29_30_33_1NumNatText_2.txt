<s> <PARAGRAPH> </s>
<s>  S1 feels that marriage and family are being redefined, and that including gay marriage into the definition of traditional marriage affects traditional marriage in a negative manner. </s>
<s> He expresses that he supports some of what Colson says and does not support some of the other stuff he says. </s>
<s> He admits to accidentally miss-wording something, and refutes any "game" being involved. </s>
<s> He feels that S2 is being hostile, and that S2's main reason for being there is to disagree with people and not to debate the topic at hand. </s>
<s> S2 feels that Colson has done very well for being a felon, but that he should stop drawing conclusions about gay parenthood based on observations of single parenthood. </s>
<s> He thinks S1 does not actually support gay rights, but is purposely misrepresenting himself, and points to times that he said one thing and then voiced opposition to that same thing. </s>
<s> He questions wether S1 might be getting paid to be there, and feels this is the least bad reason that S1 could have for the discrepancies in his views. </s>
<s>  </s>
