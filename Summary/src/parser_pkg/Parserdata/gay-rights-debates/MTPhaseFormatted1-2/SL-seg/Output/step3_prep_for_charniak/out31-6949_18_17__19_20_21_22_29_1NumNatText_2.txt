<s> <PARAGRAPH> </s>
<s>  S1 believes the world changed with the legislation in Massachusetts to allow gay marriage. </s>
<s> He assumes that S2, having stated several times he is against gay marriage, will hope the legislation is overturned but may be out of luck. </s>
<s> He questions if S2's opinion has changed now that gay marriage is in place in Massachusetts and if he wants it overturned. </s>
<s> S2 resents the assumption of S1 and says that legislation to overturn the Massachusetts decision would be an over intrusion by the government. </s>
<s> He states that he would not want an amendment to ban gay marriage and that has always been his opinion because he does not agree with over intrusion by the government. </s>
<s> He sees this as a true conservative opinion to take. </s>
<s>  </s>
