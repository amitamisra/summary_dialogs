(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ Questions) (NP (NP (DT the) (NN role)) (PP (IN of) (NP (NN government))) (PP (IN in) (NP (NN marriage)))) (, ,) (S (VP (VBG maintaining) (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (VP (AUX have) (NP (DT a) (ADJP (RB very) (VBN limited)) (NN one))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS governments)) (VP (VBP want) (S (VP (TO to) (VP (AUX be) (VP (VBN involved) (PP (IN in) (NP (PRP$ our) (NNS lives))) (ADVP (ADVP (RB as) (RB much)) (SBAR (SBAR (IN as) (S (NP (PRP they)) (VP (MD can) (VP (AUX be))))) (, ,) (CC and) (SBAR (IN that) (S (S (VP (VBG getting) (NP (DT the) (NN government)) (PP (IN out) (PP (IN of) (NP (NN marriage)))) (S (VP (VBG getting) (NP (DT the) (NN government)) (PP (IN out) (PP (IN of) (NP (NN marriage)))))))) (VP (MD would) (VP (AUX be) (NP (DT a) (JJ good) (NN idea)) (SBAR (RB even) (IN if) (S (NP (NNS people)) (VP (AUX are) (VP (VBG supporting) (NP (DT this) (NN change)) (PP (IN for) (NP (DT the) (JJ wrong) (NNS reasons))))))))))))))))))))) (. .)))

(S1 (S (S (NP (PRP It)) (VP (AUX is) (RB not) (ADJP (JJ necessary) (S (VP (TO to) (VP (AUX have) (NP (NP (NN government)) (PP (IN in) (NP (NN marriage)))))))))) (CC and) (S (NP (NNS citizens)) (VP (MD should) (VP (AUX be) (ADJP (JJ vigilant) (PP (IN in) (S (VP (VBG making) (ADJP (JJ sure) (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (AUX does) (RB not) (VP (VBP intrude) (SBAR (WHADVP (WRB where)) (S (NP (PRP it)) (VP (AUX is) (RB not) (VP (VBN needed))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ single) (NNS people)) (VP (AUX have) (NP (NP (DT the) (JJS biggest) (NN stake)) (PP (IN in) (NP (DT this) (NN issue)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ criticizes) (NP (NP (DT the) (JJ new) (NN push)) (PP (IN for) (S (NP (NN government)) (VP (VBG getting) (PP (IN out) (PP (IN of) (NP (NN marriage))))))))) (. .)))

(S1 (S (NP (NP (JJ S2) (NNS talks)) (PP (IN about) (NP (NN marriage)))) (VP (AUXG being) (NP (DT an) (JJ ancient) (NN tradition))) (, ,) (VP (VBN based) (PP (IN on) (NP (NP (DT the) (NN pair) (NN bonding)) (SBAR (SBAR (WHNP (WDT that)) (S (VP (AUX is) (ADJP (JJ natural) (PP (IN for) (NP (NNS humans))))))) (, ,) (CC and) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (NP (NP (CD one)) (PP (IN of) (NP (NP (DT the) (NNS foundations)) (PP (IN for) (NP (JJ social) (NN stability))))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (VP (VBN thought) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (VP (AUX be) (ADJP (JJ natural)) (SBAR (IN for) (S (NP (DT the) (NN government)) (VP (TO to) (VP (VB want) (S (VP (TO to) (VP (VB support) (NP (PRP it))))))))))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (SBAR (IN because) (S (NP (DT the) (NN government)) (VP (AUX is) (NP (NP (DT the) (NN source)) (PP (IN of) (NP (NN law))))))))))))) (. .)))

(S1 (S (S (VP (VBG Replacing) (NP (NN marriage)))) (VP (MD would) (VP (VB mean) (S (NP (NP (DT a) (NN plethora)) (PP (IN of) (NP (ADJP (JJ minor) (CC and) (JJ specific)) (JJ legal) (NNS contracts)))) (VP (TO to) (VP (VB cover) (NP (NP (NP (PDT all) (DT the) (NNS hundreds)) (PP (IN of) (NP (NNS rights)))) (CC and) (NP (NP (JJ mutual) (NNS obligations)) (VP (ADVP (RB currently)) (VBD conferred) (PP (IN by) (NP (NN marriage))))))))))) (. .)))

(S1 (NP (NP (NP (JJ Legal) (NNS issues)) (PP (JJ such) (IN as) (NP (NN inheritance) (, ,) (NN pension) (CC and) (JJ social) (NN security) (NNS rights)))) (, ,) (NP (NP (NN adoption)) (, ,) (NP (NN custody)) (, ,) (CC and) (NP (NP (NN home) (NN ownership)) (SBAR (WHNP (WDT that)) (S (VP (MD would) (VP (AUX need) (NP (JJ separate) (NNS contracts)) (PP (IN without) (NP (NP (DT the) (NN umbrella)) (PP (IN of) (NP (NN marriage))))))))))) (. .)))

(S1 (S (NP (NN Marriage)) (VP (AUX is) (ADJP (RBR more) (JJ convenient))) (. .)))

