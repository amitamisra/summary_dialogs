<PARAGRAPH> The issue is here is the definition of marriage, and what government body/level has the right to change this definition.
Speaker one believes that courts and federal government should be able to rule on the definition of marriage and cites several previous cases in the past where the federal government stepped up to introduce legislature.
This speaker believes that "fundamental rights" are not subject to state votes and can be decided by government.
Speaker two believes that each state should decide the definition of marriage and questions what "fundamental rights" have to do with the "definition" of marriage.
This speaker believes that it should left up to a vote to decide the definition of marriage and that legislature should not be confused with judiciary.
