(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NN marriage)) (VP (AUX is) (NP (NP (DT a) (NN contract)) (PP (IN between) (NP (NP (DT the) (NN state)) (CC and) (NP (CD two) (NNS people))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ compares) (NP (PRP it)) (PP (TO to) (S (VP (VBG forming) (NP (DT a) (NN corporation)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ finds) (NP (PRP it)) (SBAR (S (NP (JJ ironic) (NNS Christians)) (VP (AUX are) (PP (IN against) (NP (NP (DT the) (JJ religious) (NNS aspects)) (PP (IN of) (NP (NN marriage))))) (ADVP (RB yet)) (, ,) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ unable) (S (VP (TO to) (VP (VB stop) (NP (NP (JJ religious) (NN marriage)) (SBAR (S (NP (PRP they)) (VP (VBP want) (S (VP (TO to) (VP (VB stop) (NP (DT the) (NN contract)))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (S (NP (PRP he)) (VP (AUX is) (VP (VBN forced) (S (VP (TO to) (VP (VB recognize) (NP (JJ heterosexual) (NN marriage)))))))) (CC and) (S (NP (NNS others)) (VP (MD should) (VP (VB recognize) (NP (JJ gay) (NNS marriages)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (S (NP (NNS rights)) (VP (MD should) (VP (AUX be) (VP (VP (VBN voted) (PRT (RP on))) (CC and) (VP (VBZ gives) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NN slavery)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB think) (SBAR (S (NP (NN anyone)) (VP (VP (AUX is) (VP (VBN harmed) (PP (IN by) (NP (JJ gay) (NN marriage))))) (CC but) (VP (VBZ believes) (SBAR (S (NP (JJ official) (NNS policies)) (VP (AUX are) (NP (NP (NN everyone) (POS 's)) (NN business))))))))))) (. .)))

(S1 (S (S (NP (JJ Heterosexual) (NN marriage)) (VP (AUX is) (NP (DT the) (FW status) (FW quo)))) (, ,) (CC so) (S (NP (PRP it)) (VP (AUX is) (VP (ADVP (RB thus)) (VBN recognized)))) (. .)))

(S1 (S (NP (NP (DT Any) (NNS changes)) (PP (TO to) (NP (DT the) (NN status) (NN quo)))) (VP (MD should) (VP (AUX be) (ADJP (JJ justified)))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB see) (NP (NN marriage)) (PP (IN as) (NP (NP (DT a) (ADJP (RB right) (DT an)) (NN example)) (VP (AUXG being) (VP (VBD unmarried) (NP (NNS people)))))))) (. .)))

