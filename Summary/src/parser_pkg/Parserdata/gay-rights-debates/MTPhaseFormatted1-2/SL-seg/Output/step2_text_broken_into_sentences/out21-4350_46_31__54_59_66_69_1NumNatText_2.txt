<PARAGRAPH> S1 and S2 discuss same-sex marriage.
S1 is for gay marriage, and opens the discussion by showing logical fallacies used by the opposition to deter people from believing gay marriage is an extension of equal rights.
S1 believes that modern families are not traditional families and are often better off for it.
S1 specifically points out the comparison of gay marriage to incest or polygamy.
S1 says this is a scare tactic to turn people away.
S2 believes that homosexuality, incest, and polygamy are all related because they are all taboos of society.
S2 wants to draw the line as is so the definition of marriage does not change.
If it does change then there would be no limit to who could marry.
S1 explains that love is present in all marriage so the claims made by S2 about gay marriage leading to incest, could be used for straight marriage as well.
S2 attacks S1 and thinks that if the other parties mentioned went and paraded their lifestyle then they would also have an equal opportunity to make their marriage a social issue.
