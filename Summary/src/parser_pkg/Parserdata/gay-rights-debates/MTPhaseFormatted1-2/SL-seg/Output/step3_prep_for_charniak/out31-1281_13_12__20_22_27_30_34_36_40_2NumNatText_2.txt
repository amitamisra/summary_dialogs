<s> <PARAGRAPH> </s>
<s>  S1 views the marriage arrangement as society recognizing a family unit based on a natural heterosexual contract. </s>
<s> He explains that natural means between a man and a woman. </s>
<s> He accepts that everyone has their own values but it is unreasonable to expect that all values are socially acceptable. </s>
<s> He believes legalizing gay marriage is a form of forcing society to accept gay behavior. </s>
<s> S2 questions the validity of using the term natural to define marriage. </s>
<s> He uses an example of same-sex marriage in a Congo tribe to show that marriage is defined by the needs of the social group. </s>
<s> Banning gay marriage is forcing one set of believes onto others. </s>
<s> He argues that legalizing gay marriage does not force gay values onto anyone. </s>
<s>  </s>
