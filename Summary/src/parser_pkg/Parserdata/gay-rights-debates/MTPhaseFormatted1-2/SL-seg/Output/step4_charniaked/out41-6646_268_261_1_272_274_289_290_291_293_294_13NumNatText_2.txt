(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (S (VP (VBG referring) (PP (TO to) (NP (NP (NNS arguments)) (PP (IN of) (NP (NN disagreement))) (PP (IN as) (NP (NN bigotry))))))) (VP (AUX is) (VP (AUX done) (SBAR (IN because) (S (NP (DT the) (NN accuser)) (VP (VP (VBZ lacks) (NP (JJ factual) (NN information))) (CC and) (VP (VBZ believes) (SBAR (S (NP (NNP S2)) (VP (VBZ uses) (NP (NP (DT the) (NN statement)) (PP (IN for) (NP (PRP$ its) (NN effect))) (SBAR (IN since) (S (NP (PRP it)) (VP (AUX is) (VP (VBN thought) (PP (IN of)) (PP (IN as) (NP (DT an) (JJ ugly) (NN word)))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ advises) (NP (NP (RB not) (DT all) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (PP (IN against) (NP (JJ gay) (NN marriage))) (VP (AUX are) (NP (NNS bigots))) (CC but) (VP (VBP feel) (NP (NP (DT the) (NN way)) (SBAR (S (NP (PRP they)) (VP (AUX do) (ADJP (JJ due) (PP (TO to) (NP (NP (DT the) (NN way)) (SBAR (S (NP (PRP they)) (VP (VBP view) (NP (NN marriage)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (SBAR (S (NP (DT the) (NN issue)) (VP (AUX is) (RB not) (ADVP (RB always)) (PP (IN about) (S (VP (VBG excluding) (NP (NP (NNS gays)) (PP (IN from) (NP (JJ different) (NNS things)))))))))) (CC but) (SBAR (IN that) (S (NP (PRP it)) (VP (VBZ boils) (PRT (RP down)) (PP (TO to) (NP (NN marriage)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ invites) (S (NP (DT the) (NN word)) (ADJP (JJ gay) (S (VP (TO to) (VP (AUX be) (VP (VBN replaced) (PP (IN with) (NP (NP (DT another) (NN minority) (CC and) (DT the) (NNS statements)) (VP (VBN made) (S (VP (TO to) (RB not) (VP (VB appear) (S (VP (TO to) (VP (AUX be) (VP (VBN bigoted)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (S (S (VP (VBG excluding) (NP (NP (NNS gays)) (PP (IN from) (NP (NP (JJ different) (NNS pieces)) (PP (IN of) (NP (NN society)))))))) (VP (VP (AUX is) (NP (NP (DT the) (JJ same)) (PP (IN as) (S (VP (VBG excluding) (NP (DT any) (JJ other) (NN minority))))))) (CC and) (VP (AUX is) (ADVP (RB just) (RB simply)) (NP (NN bigotry))))))) (. .)))

