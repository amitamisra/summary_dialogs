(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (SBAR (IN that) (S (NP (NNP Colson) (POS 's)) (VP (AUX is) (NP (NP (DT the) (ADJP (RBS most) (JJ compelling)) (NN argument)) (PP (IN for) (NP (DT the) (NN Marriage) (NNP Protection) (NNP Amendment)))) (ADVP (RB yet))))) (, ,) (CC but) (SBAR (IN that) (S (NP (PRP he)) (ADVP (RB still)) (VP (AUX does) (RB not) (VP (VB support) (NP (PRP it)) (ADVP (RB personally)) (, ,) (PP (IN as) (NP (NP (DT the) (NN changing)) (SBAR (S (NP (NP (DT the) (NN constitution)) (PP (IN of) (NP (PRP it)))) (VP (AUX is) (RB not) (ADJP (JJ right))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (NN legalization)) (PP (IN of) (NP (JJ gay) (NN marriage)))) (VP (MD would) (VP (VB redefine) (SBAR (WHNP (WP what) (NN family)) (S (VP (AUX is) (PP (IN in) (NP (DT a) (JJ negative) (NN manner))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (VBG broadening)) (PP (IN of) (SBAR (WHNP (WP what)) (S (VP (VBZ constitutes) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage))))))))) (VP (MD would) (VP (ADVP (RB somehow)) (VB weaken) (NP (JJ traditional) (NN marriage))))))) (. .)))

(S1 (S (NP (DT This)) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ says))) (, ,)) (VP (MD would) (VP (VB lead) (PP (TO to) (NP (JJR more) (NN crime))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (NNS problems)) (PP (IN with) (NP (NP (JJ traditional) (NN marriage)) (PP (IN in) (NP (DT this) (NN country)))))) (VP (VBD began) (SBAR (ADVP (RB long)) (IN before) (S (NP (JJ gay) (NN marriage)) (VP (VBD began) (S (VP (TO to) (VP (AUX be) (VP (VBN legalized)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ makes) (NP (NP (DT an) (NN analogy)) (PP (IN of) (NP (NP (DT a) (NN man)) (VP (VBG jumping) (PP (IN out) (PP (IN of) (NP (NP (DT a) (NN plane)) (PP (IN before) (NP (NP (JJ gay) (NN marriage)) (, ,) (CC and) (NP (NP (JJ gay) (NN marriage)) (VP (AUXG being) (VP (VBN blamed)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX has) (NP (NP (DT any) (NN effect)) (PP (IN on) (NP (JJ traditional) (NN marriage)))) (ADVP (IN at) (DT all))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NN society)) (VP (AUX is) (RB not) (VP (VBN harmed) (PP (IN by) (NP (NP (DT the) (NN inclusion)) (PP (IN of) (NP (JJ gay) (NN marriage)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT any) (NN statement) (IN that)) (VP (VBZ says) (SBAR (IN that) (S (NP (JJ gay) (NN marriage) (NNS harms) (NN society)) (VP (MD should) (VP (AUX be) (VP (VBN proven) (PP (IN before) (S (VP (AUXG being) (VP (ADVP (RB widely)) (VBN talked) (PP (IN about))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VP (VBG misrepresenting) (NP (PRP himself))) (CC and) (VP (VBG twisting) (NP (PRP$ his) (NN argument) (S (VP (TO to) (VP (VB make) (NP (NP (NNS statements)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (ADJP (JJ different) (PP (IN from) (SBAR (WHNP (WP what)) (S (NP (NNP Colson)) (VP (AUX had) (VP (VBN written))))))))))))))))))))) (. .)))

