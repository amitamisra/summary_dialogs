(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (DT This)) (VP (AUX is) (PP (IN about) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ seems) (S (VP (TO to) (VP (VB believe) (SBAR (IN that) (S (NP (NN someone)) (VP (AUX is) (VP (VBG making) (NP (NN religion)) (PP (IN into) (NP (NN law))))))) (PP (IN by) (S (VP (VBG saying) (SBAR (S (NP (NN marriage)) (VP (MD should) (VP (AUX be) (PP (IN between) (NP (DT a) (JJ heterosexual) (NN couple)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX is) (VP (VBG arguing) (SBAR (IN that) (S (NP (NP (NNP S1) (POS 's)) (NN point)) (VP (AUX is) (RB not) (ADJP (RB very) (JJ valid)) (SBAR (IN because) (S (NP (NNP S1)) (VP (AUX is) (RB not) (S (VP (VBG understanding) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (RB not) (VP (VBG using) (NP (DT the) (NNS persons) (NN perspective)))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (DT that) (NN person)) (VP (AUX is) (VP (VBG trying) (S (VP (TO to) (VP (VB hurt) (NP (PRP them))))))))) (, ,) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (VP (VBG saying) (SBAR (IN that) (S (NP (DT the) (NN person)) (VP (MD may) (RB not) (ADVP (RB necessarily)) (VP (AUX be) (NP (DT that) (NN way)))))))))) (CC and) (SBAR (IN that) (S (NP (DT the) (NN person)) (VP (MD may) (VP (AUX be) (VP (VBG arguing) (PP (IN for) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP believe) (S (VP (TO to) (VP (AUX be) (ADJP (ADJP (JJR greater)) (PP (IN for) (NP (NN society)))) (, ,) (PP (VBN based) (PP (IN on) (NP (PRP$ their) (JJ own) (NN perspective)))))))))))))))))) (. .)))

(S1 (S (NP (NP (NP (NNP S1) (POS 's)) (JJ religious) (NN view)) (PP (IN of) (NP (NN marriage)))) (VP (AUX is) (VP (VBN based) (PP (IN on) (NP (NP (NP (CD two) (NNS people) (POS 's)) (NN love)) (PP (IN for) (NP (DT each) (JJ other))))) (, ,) (SBAR (IN while) (S (NP (DT the) (JJ other) (NN person)) (VP (VBZ believes) (NP (PRP$ its)) (PP (IN between) (NP (DT a) (NN man) (CC and) (NN woman)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (SBAR (S (NP (NP (NNP S1) (POS 's)) (NN argument)) (VP (AUX is) (ADJP (JJ empty))))) (CC and) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (RB not) (VP (VBG showing) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (JJ other) (NN person)) (VP (AUX is) (VP (AUXG being) (VP (VBN hurt) (PP (IN by) (NP (JJ gay) (NN marriage)))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ thinks) (SBAR (S (NP (NNP S1)) (VP (AUX is) (RB not) (S (VP (VP (VBG understanding) (NP (DT the) (JJ overall) (NNS perspectives))) (CC and) (VP (ADVP (RB just)) (VBG focusing) (PP (IN on) (NP (PRP$ their) (JJ own) (VBZ needs)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NP (NP (DT the) (NN person) (POS 's)) (NN religion)) (VP (VBN put) (PP (IN into) (NP (NN law))))) (VP (VBZ affects) (S (NP (NNS peoples) (NN ability)) (VP (TO to) (VP (VB marry) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ gay))))))))))) (, ,) (CC but) (SBAR (IN that) (S (NP (NP (NP (NNP S1) (POS 's)) (NN religion)) (PP (IN on) (NP (NN marriage))) (VP (VBN put) (PP (IN into) (NP (NN law))))) (VP (MD would) (RB not) (VP (VB affect) (NP (NN marriage)) (PP (IN for) (NP (NN anyone))))))))) (. .)))

