<s> <PARAGRAPH> </s>
<s>  S1 believes that everyone should live within the bounds of the laws of society. </s>
<s> They are free to be changed but not broken. </s>
<s> Society's definition of marriage is not based merely on religion, but on biology. </s>
<s> S2 argues that people who are not able to reproduce but can marry invalidates S1's argument. </s>
<s> S1 reiterates that the ultimate intended goal of marriage is to make reproduction easier. </s>
<s> Bringing up exceptions to does not change anything. </s>
<s> S1 argues that S2's opposition to gay marriage bans stems from not receiving benefits that a married couple does. </s>
<s> S2 argues that gay marriage bans are unequal and unjust. </s>
<s> S2 emphasizes universal equality and desires to be treated just as heterosexual people who do not reproduce are in terms of the ability to marry. </s>
<s> S1 argues that by placing marriage law in the realm of the states, then the states can determine which type of marriage to recognize. </s>
<s> S2 argues that even if this were the case, it would still be unjust. </s>
<s> S1 argues that S2 ignores the biological differences involved. </s>
<s>  </s>
