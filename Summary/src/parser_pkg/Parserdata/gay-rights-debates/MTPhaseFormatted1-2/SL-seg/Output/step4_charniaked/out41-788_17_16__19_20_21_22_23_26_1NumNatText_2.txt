(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBD said) (SBAR (IN that) (S (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (VBZ comes) (PP (TO to) (NP (NP (DT the) (NN word)) (ADJP (`` ``) (JJ normal) ('' ''))))))) (, ,) (NP (NNP fidelity)) (VP (MD should) (VP (AUX be) (VP (VBN looked) (PP (IN at) (SBAR (CC and) (S (NP (JJ many) (NNS gays)) (VP (AUX have) (VP (NNP fidelity) (PP (IN in) (NP (PRP$ their) (NN relationshipso)))))))))))))) (. .)))

(S1 (S (FRAG (VP (AUX Is) (RB not) (NP (NP (NNP fidelity) (NN part)) (PP (IN of) (NP (NP (DT the) (JJ american) (NN attitude)) (PP (IN of) ('' '') (ADJP (JJ normal)) ('' '')))))) (. ?)) (, ,) (NP (PRP he)) (VP (VBD said)) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD questioned) (NP (NP (NP (NNP S2) (POS 's)) (VBG labeling)) (PP (IN of) (NP (JJ gay) (NNS people)))) (PP (IN as) (NP (NNS heterophobes)))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD asked) (NP (NNP S2)) (SBAR (WHNP (WP what)) (S (NP (PRP$ his) (NN problem)) (VP (AUX was) (, ,) (SBAR (WHADVP (WRB why)) (S (NP (PRP he)) (ADVP (RB personally)) (VP (AUX did) (RB not) (VP (VB want) (NP (JJ gay) (NN marriage)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD quoted) (NP (NP (DT a) (NNP CNN) (NN poll)) (SBAR (WHNP (WDT which)) (S (VP (VBD found) (SBAR (IN that) (S (NP (JJ many) (NNS people)) (VP (AUX do) (RB not) (VP (VB want) (NP (JJ gay) (NN marriage)) (, ,) (ADJP (JJ includingm))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (SBAR (WHADVP (WRB when)) (S (NP (NNS people)) (VP (AUX are) (ADJP (VBN opposed) (PP (TO to) (NP (JJ gay) (NN marriage))))))) (NP (PRP they)) (VP (AUX are) (ADVP (RB often)) (VP (VBN called) (S (NP (DT a) (NN homophobe)))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (NP (DT a) (NN person)) (VP (AUX is) (ADJP (VBN opposed) (PP (TO to) (NP (NP (DT the) (VBG banning)) (PP (IN of) (NP (JJ gay) (NN marriage))))))))) (, ,) (NP (PRP they)) (VP (MD must) (ADVP (RB therefore)) (VP (AUX be) (NP (DT a) (NN heterophobe)))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (NP (NP (DT a) (NN right) (AUX be)) (PP (IN against) (NP (JJ gay) (NN marriage)))))))) (. .)))

