(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBD said) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ continues) (S (VP (TO to) (VP (`` ``) (VP (VB stand) (PRT (RP by))) ('' '') (CC and) (VP (VB adhere) (PP (TO to) (NP (PRP$ his) (JJ previous) (NN post))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD challenged) (S (NP (NNP S2)) (VP (TO to) (VP (VB show) (NP (NN proof) (SBAR (IN that) (S (NP (DT this)) (VP (AUX was) (RB not) (NP (NP (DT the) (NN case)) (PRN (-LRB- -LRB-) (ADJP (JJ seeming) (SBAR (IN that) (S (NP (NNS gays)) (VP (VBD caused) (S (NP (NNP AIDs)) (VP (TO to) (VP (AUX be) (NP (DT a) (JJ major) (NN health) (NN concern))))))))) (-RRB- -RRB-)) (CC and) (S (VP (TO to) (VP (VB stop) (S (VP (VBG making) (NP (JJ empty) (NNS arguments)) (S (VP (NN and/or) (S (VP (VBG attacking) (NP (PRP him)))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB impatiently)) (VP (VBD waited) (PP (IN for) (NP (DT the) (NNS proofs))) (SBAR (S (NP (PRP he)) (VP (VBD wanted))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD stated) (NP (PRP$ his) (NN belief) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (VBZ remains) (NP (NP (DT a) (NN mystery)) (PP (IN about) (SBAR (WHADVP (WRB how)) (S (NP (NNP AIDs)) (VP (VBD reached) (NP (DT the) (NNP United) (NNPS States)))))))))) (CC and) (SBAR (IN that) (S (NP (NP (NP (NNP IV) (NN drug) (NNS users)) (SBAR (WHNP (WP who)) (S (VP (VBD exchanged) (NP (NNS needles)) (PP (IN without) (S (VP (VBG cleaning) (NP (PRP them))))))))) (CC and) (NP (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX did) (RB not) (VP (VB practice) (NP (JJ safe) (NN sex))))))) (CC and) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBD gave) (NP (NP (NN blood)) (PP (IN for) (NP (NP (NNS transfusions)) (PP (IN before) (NP (NNP AIDS)))))))))))) (VP (AUX was) (VP (VBN known) (PP (IN about) (NP (NN spread))) (NP (DT the) (NN disease))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (NP (PRP he)) (VP (MD would) (RB not) (VP (VB debate) (NP (NNP S1)) (SBAR (IN until) (S (NP (NNP S1)) (VP (VBZ starts) (S (NP (PRP himself)) (VP (VBG giving) (NP (NP (DT the) (NNS proofs)) (SBAR (WHNP (IN that)) (S (NP (PRP he)) (VP (NNS requests) (PP (IN of) (NP (NNS others))))))))))))))))) (. .)))

