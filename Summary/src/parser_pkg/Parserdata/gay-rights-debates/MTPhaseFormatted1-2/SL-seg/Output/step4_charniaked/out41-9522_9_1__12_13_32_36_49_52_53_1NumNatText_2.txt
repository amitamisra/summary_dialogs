(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (NNS people)) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (NN marriage) (NN equality)))))) (VP (AUX is) (VP (VBG growing))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NN equality)) (VP (MD will) (ADVP (RB soon)) (VP (VB stand) (PP (IN on) (NP (NP (DT the) (NN side)) (PP (IN of) (NP (DT the) (NN majority))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (SBAR (SBAR (IN that) (S (NP (NNS gays)) (VP (VBP lose) (NP (NP (DT every) (NN time)) (SBAR (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (VP (VBN put) (PP (TO to) (NP (NP (DT a) (NN vote)) (PP (IN in) (NP (DT a) (NN ballot))))))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS gays)) (VP (AUX need) (NP (DT a) (JJR better) (NNP PR) (NN strategy))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (NP (NN majority)) (PP (IN of) (NP (NNS people)))) (VP (MD will) (VP (VB support) (NP (JJ civil) (NNS unions)) (PP (IN with) (NP (NP (DT the) (JJ same) (NNS benefits)) (CONJP (CC but) (RB not)) (NP (NN marriage))))))) (CC and) (S (NP (EX there)) (VP (MD will) (VP (AUX be) (NP (NP (NNS repercussions)) (ADJP (JJ due) (PP (TO to) (NP (DT the) (NNS efforts) (S (VP (TO to) (VP (VB give) (NP (NN tax) (NN money)) (PP (TO to) (NP (NP (JJ federal) (NN employee) (POS 's)) (JJ same) (NN sex) (NNS partners))) (PP (IN despite) (NP (DT the) (NAC (NNP Defense) (PP (IN of) (NP (NN Marriage)))) (NNP Act)))))))))))))))) (. .)))

(S1 (S (S (VP (TO To) (VP (VB contradict)))) (, ,) (NP (NNP S2)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (ADVP (RB so) (RB far)) (NP (CD 5) (NNS states)) (VP (AUX have) (VP (VBN passed) (NP (JJ gay) (NN marriage) (NNS laws)))))) (, ,) (CC and) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (VB pay) (NP (PRP$ their) (NNS taxes)) (PP (RB just) (IN like) (NP (NP (NN everyone)) (ADJP (RB else))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (JJ gay) (NN community)) (VP (AUX is) (RB not) (NP (NP (DT a) (NN bunch)) (PP (IN of) (NP (JJ promiscuous) (NN sex) (VBN crazed) (NNS people)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ rebutts) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (PP (IN in) (NP (DT those) (CD 5) (NNS states))) (, ,) (NP (JJ gay) (NN marriage)) (VP (VP (AUX was) (RB not) (VP (VBN put) (PP (TO to) (NP (DT a) (NN ballot))))) (, ,) (CC but) (VP (VBN forced) (PP (IN through) (NP (DT the) (NN court) (CC or) (NN legislature))))))))))) (. .)))

