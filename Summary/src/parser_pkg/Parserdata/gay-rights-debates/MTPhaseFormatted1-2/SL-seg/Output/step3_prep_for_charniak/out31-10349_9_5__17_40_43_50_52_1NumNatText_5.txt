<s> <PARAGRAPH> </s>
<s>  S1 states that the legal definition of marriage is a contract between a man and a woman, who are not related, with no regard to love, or the intention of reproduction. </s>
<s> S1 maintains, from the basis of observation, that the cultural definition of marriage is a formalization of a mating pair, love and the intension to reproduce being implied. </s>
<s> Saying that homosexual couples are not a part of either definition, and that being included in the current definition is a catch-22, as homosexuality is not in sync with evolution. </s>
<s> S1 further states that the only connection between, mating couples and marriage, are cultural. </s>
<s> S2 defines marriage as a commitment between two people who love one another. </s>
<s> Using a hypothetical marriage between a 65 year old woman and a 68 year old man as an example, argues that reproduction has no bearing on the definition of marriage, and that without a harm involved, equality of rights is what is important. </s>
<s> S2 believes that personal opinions are subjective and should not be used as the basis to limit the rights of the minority. </s>
<s>  </s>
