<PARAGRAPH> S1 states that the majority of the supreme court cases which have addressed a ban on same-sex marriage have deemed the laws unconstitutional.
He recognized the difficulty imposed by the fact that marriage is both a legal contract and a religious ceremony.
He claims that although the cited cases involved state constitutions, the ruling were based on provisions very similar to the federal constitution.
S2 argues that supreme court decisions regarding violations of state constitutions are irrelevant to the federal constitution.
He claims that the Massachusetts ruling was based on a provision prohibiting discrimination based on sexual preference which is not found in the federal constitution, but later admits that he was in error and that it was based on prohibiting discrimination based on sex.
