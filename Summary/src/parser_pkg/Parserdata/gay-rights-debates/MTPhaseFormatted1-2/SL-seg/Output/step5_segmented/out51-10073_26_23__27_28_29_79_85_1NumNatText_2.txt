<T>
<P>
</P>
<P>
<S>
<C>S1/NNP maintains/VBZ a/DT fundamental/JJ view/NN of/IN marriage/NN as/IN being/AUXG between/IN a/DT man/NN and/CC a/DT woman/NN and/CC as/IN having/AUXG been/AUX create/VB </C>
<C>to/TO have/AUX children/NNS in/IN a/DT stable/JJ environment/NN ./. </C>
</S>
<S>
<C>Gay/JJ marriage/NN ,/, S1/NNP believes/VBZ ,/, would/MD change/VB the/DT very/JJ nature/NN of/IN the/DT marriage/NN relationship/NN ,/, </C>
<C>while/IN merely/RB providing/VBG gay/JJ men/NNS the/DT ability/NN to/TO wear/VB wedding/NN rings/NNS and/CC delude/VB themselves/PRP that/IN they/PRP are/AUX normal/JJ married/JJ people/NNS ,/, </C>
<C>although/IN he/PRP concedes/VBZ the/DT latter/JJ part/NN is/AUX only/RB his/PRP$ opinion/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN excluding/VBG gays/NNS from/IN being/AUXG able/JJ to/TO marry/VB the/DT person/NN of/IN their/PRP$ choice/NN is/AUX discrimination/NN ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ that/IN discrimination/NN and/CC equality/NN are/AUX inappropriate/JJ terms/NNS </C>
<C>because/IN the/DT very/JJ concept/NN of/IN marriage/NN excludes/VBZ homosexuals/NNS ./. </C>
</S>
<S>
<C>He/PRP regrets/VBZ that/IN more/RBR gay/JJ posters/NNS in/IN the/DT forum/NN do/AUX not/RB agree/VB with/IN him/PRP on/IN the/DT point/NN ./. </C>
</S>
</P>
</T>
