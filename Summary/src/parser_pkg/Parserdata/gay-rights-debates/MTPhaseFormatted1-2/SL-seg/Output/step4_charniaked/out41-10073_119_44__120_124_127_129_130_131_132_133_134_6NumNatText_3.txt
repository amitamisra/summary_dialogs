(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ states) (SBAR (IN that) (S (NP (JJ same-sex) (NN marriage)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN prohibited))))))) (, ,) (CC but) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT a) (JJ homosexual) (NN marriage)) (VP (AUX is) (ADJP (JJ different) (PP (IN than) (NP (DT a) (JJ heterosexual) (NN marriage))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (S (VP (VBG recognizing) (NP (DT this) (NN difference)))) (VP (AUX is) (RB not) (NP (NN discrimination)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (UCP (NP (NP (NNS children)) (PP (IN of) (NP (JJ same-sex) (NNS couples)))) (CC or) (ADJP (JJ comparable) (PP (TO to) (NP (NP (NNS children)) (PP (IN from) (NP (ADJP (JJ incestuous) (, ,) (JJ polygamous) (, ,) (CC or) (JJ other)) (JJ sexual) (NN deviant) (NNS relationships)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ makes) (NP (DT the) (NN argument) (SBAR (IN that) (S (SBAR (IN if) (S (NP (JJ same-sex) (NNS marriages)) (VP (AUX are) (VP (VBN legalized))))) (, ,) (NP (NP (RB then) (DT all) (NNS types)) (PP (IN of) (NP (NP (NNS marriages)) (PP (IN between) (NP (VBG consenting) (NNS adults)))))) (VP (MD should) (VP (AUX be) (VP (VBN legalized)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (DT the) (NNS arguments)) (PP (IN in) (NP (NP (NN favor)) (PP (IN of) (NP (JJ same-sex) (NN marriage)))))) (VP (MD would) (VP (VB work) (ADVP (RB equally) (RB well)) (PP (IN for) (NP (NP (NN incestuous)) (CC or) (NP (JJ polygamous) (NNS relationships))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ identifies) (NP (PRP himself)) (PP (IN as) (NP (DT a) (JJ homosexual)))) (CC and) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ same-sex) (NNS families)) (VP (VBP deserve) (NP (NP (DT the) (JJ same) (NNS benefits)) (PP (IN as) (NP (JJ heterosexual) (NNS families))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (WHNP (WDT what) (NN difference)) (S (VP (VBZ exists) (SBAR (IN that) (S (VP (VBZ distinguishes) (NP (NP (PRP$ his) (NN relationship)) (PP (IN from) (NP (NP (DT that)) (PP (IN of) (NP (NP (DT a) (JJ heterosexual) (NN couple)) (ADJP (JJ other) (PP (IN than) (NP (NP (DT the) (NNS genitals)) (VP (VBN involved))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ identifies) (NP (NP (DT a) (NN variety)) (PP (IN of) (NP (NP (NNS benefits)) (VP (VBN received) (PP (ADVP (RB only)) (IN through) (NP (NN marriage)))) (, ,) (PP (JJ such) (IN as) (NP (NP (JJ social) (NN security) (CC and) (NN guardianship)) (PP (IN among) (NP (NNS others))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ takes) (NP (NN offense)) (PP (IN at) (NP (NP (NNP S1) (POS 's)) (NN comment) (SBAR (IN that) (S (NP (PRP$ his) (NNS children)) (VP (AUX are) (`` ``) (ADJP (JJ outsourced)) ('' ''))))))) (CC and) (VP (VBZ rejects) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX are) (VP (VBG fighting) (PP (IN for) (NP (NN marriage) (NN equality))) (SBAR (ADVP (RB simply)) (IN because) (S (NP (PRP they)) (VP (VBP free) (NP (NP (NNS handouts)) (PP (IN from) (NP (DT the) (NN government)))))))))))))) (. .)))

