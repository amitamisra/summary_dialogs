(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (PRP It)) (VP (VBZ seems) (SBAR (IN like) (S (NP (PRP they)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN inconsistency)) (PP (IN of) (NP (NP (DT each) (JJ other) (POS 's)) (NNS arguments))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (AUX is) (VP (VBG getting) (ADJP (JJ annoyed)))) (CC and) (VP (VBZ seems) (S (VP (TO to) (ADVP (RB just)) (VP (AUX be) (VP (VBG arguing) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (VP (`` ``) (VBG venting) ('' '') (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (VP (VP (VBG saying) (SBAR (IN that) (S (NP (DT the) (NN inconsistency)) (VP (AUX is) (ADJP (JJ present)))))) (CC and) (VP (VBG making) (NP (NP (DT all) (NNS arguments)) (PP (JJ invalid) (IN without) (NP (NN proof))))))))))))))))))) (. .)))

(S1 (S (S (NP (DT Both) (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (PP (IN for) (NP (JJ gay) (NNS rights))))) (, ,) (NP (NNP S2)) (VP (AUX is) (VP (VBG stating) (SBAR (IN that) (S (NP (NNP S1)) (VP (VP (AUX is) (RB not) (VP (AUXG being) (ADJP (JJ consistent)))) (CC and) (VP (`` ``) (VBZ admits) ('' '') (PP (TO to) (S (VP (AUXG being) (ADJP (JJ inconsistent)) (, ,) (SBAR (IN while) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG saying) (SBAR (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB care) (PP (IN about) (NP (DT the) (NN inconsistency))) (PP (IN because) (IN of) (NP (NP (DT the) (NN point)) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG trying) (S (VP (TO to) (VP (VB get) (PRT (RP across)))))))))))))))))))))))))))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (VP (VBP bring) (PRT (RP up)) (NP (NNP Biden))) (CC and) (VP (NP (NN abortion)) (PP (IN in) (NP (NP (NNS ways)) (SBAR (WHNP (WDT that)) (S (VP (AUX do) (RB not) (ADVP (RB exactly)) (VP (VB fit) (NP (DT the) (NN discussion))))))))))) (, ,) (CC but) (S (ADVP (RB just)) (VP (TO to) (VP (VB show) (NP (NP (DT the) (NN inconsistency)) (PP (IN in) (NP (DT each) (NNS others) (NNS opinions))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX is) (VP (VBG saying) (SBAR (S (NP (PRP they)) (VP (MD would) (VP (VB handle) (NP (DT this) (JJ same) (NN situation)) (PP (IN with) (NP (NNP Biden))) (NP (DT the) (JJ exact) (JJ same) (NN way)) (SBAR (IN because) (S (NP (NNP S2)) (VP (AUX is) (ADVP (RB simply)) (VP (VBG talking) (PP (IN about) (NP (NN perspective))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NNP S2)) (VP (AUX is) (VP (VBG stating) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBZ believes) (PP (IN in) (NP (NP (JJ gay) (NN marriage)) (CONJP (CC but) (RB not)) (NP (JJ gay) (NNS rights)) (, ,) (SBAR (WHNP (WDT which)) (S (VP (VBZ seems) (ADJP (JJ contradictory) (PP (TO to) (NP (NP (NNP S1) (POS 's)) (NN point))))))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN argument)) (VP (AUX does) (RB not) (VP (AUX have) (NP (DT a) (VBN set) (NN topic)))) (. .)))

