(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG debating) (NP (NP (DT the) (NN topic)) (PP (IN of) (NP (JJ gay) (NN marriage) (NNS rights)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (S (VP (VBG allowing) (S (NP (NNS gays)) (VP (TO to) (VP (VB marry)))))) (VP (MD will) (VP (VB open) (PRT (RP up)) (NP (DT the) (NN door) (S (VP (TO to) (VP (VB legalize) (NP (NP (JJ other) (NNS types)) (PP (IN of) (NP (NN marriage))) (PP (JJ such) (IN as) (NP (NN polygamy))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ holds) (PP (TO to) (NP (DT the) (NN theory) (SBAR (IN that) (SBAR (IN if) (S (NP (NNP King) (NNP David)) (VP (AUX was) (ADJP (JJ able) (S (VP (TO to) (VP (AUX have) (NP (JJ multiple) (NNS wives)) (, ,) (SBAR (WHADVP (WRB why)) (S (ADVP (RB then)) (VP (MD should) (NP (DT the) (JJ common) (NN man) (RB not)) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to)))) (, ,) (SBAR (ADVP (RB especially)) (IN if) (S (NP (NN religion)) (VP (AUX is) (S (VP (TO to) (ADVP (RB now)) (VP (AUX be) (NP (NP (DT the) (NN basis)) (PP (IN for) (NP (DT all) (NNS laws))))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NN practice)) (VP (VP (AUX was) (PP (IN of) (NP (DT the) (NNP Old) (NNP Testament)))) (CC and) (VP (AUX is) (ADVP (RB no) (RB longer)) (VP (VBN considered) (S (ADJP (JJ commonplace))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (SBAR (S (NP (NNP S1)) (VP (VP (AUX is) (ADVP (RB too)) (NP (JJ left) (NN wing)) (S (VP (TO to) (VP (VB entertain) (NP (NP (DT the) (NN thought)) (PP (IN of) (S (VP (VBG allowing) (NP (JJ gay) (NN marriage)))))))))) (, ,) (CC but) (VP (VBZ uses) (NP (NN religion)) (PP (IN as) (NP (DT the) (NN basis))) (SBAR (RB even) (IN though) (S (NP (EX there)) (VP (AUX are) (NP (NP (ADJP (RB so) (JJ many)) (NNS contradictions)) (PP (IN between) (NP (DT the) (NNP Old) (NNP Testament))))))))))) (CC and) (SBAR (WHNP (WP what)) (S (VP (AUX is) (VP (VBN considered) (S (NP (DT the) (NNP New) (NNP Testament))))))))) (. .)))

