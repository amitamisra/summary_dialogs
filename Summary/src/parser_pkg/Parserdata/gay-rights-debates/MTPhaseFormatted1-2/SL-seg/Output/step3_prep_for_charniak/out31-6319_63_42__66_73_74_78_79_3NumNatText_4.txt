<s> <PARAGRAPH> </s>
<s>  Two people are discussing equality and natural rights. </s>
<s> S1 states that discrimination on people for being religious and keeping people from religion infringes on natural rights, and if you are for equality for gays then you need equality for all. </s>
<s> S2 states that this allows people to do whatever they want and use their religious beliefs as an excuse. </s>
<s> To contradict, S1 states that only serious religious views are taken into account. </s>
<s> S2 then proposes a scenario where the only person available to perform the gay marriage ceremony is exempted based on religious beliefs, their rights will not be upholded. </s>
<s> S1 rebuts by stating that the proposed provision would allow someone else, and this is due to religious freedom. </s>
<s>  </s>
