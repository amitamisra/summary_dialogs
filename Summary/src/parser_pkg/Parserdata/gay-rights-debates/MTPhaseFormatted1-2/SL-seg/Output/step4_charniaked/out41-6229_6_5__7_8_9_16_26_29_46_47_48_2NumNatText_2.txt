(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)) (PP (IN in) (NP (NP (DT the) (NN context)) (PP (IN of) (NP (NP (DT an) (JJ unknown) (`` ``) (NN she) ('' '')) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (VP (VBG pushing) (NP (NP (DT a) (NN law)) (SBAR (S (VP (TO to) (VP (VB ban) (NP (JJ gay) (NN marriage))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (PRP$ her) (JJ religious) (NN freedom)) (VP (AUX is) (VP (AUXG being) (VP (VBN persecuted) (PP (IN from) (NP (NP (NNS people)) (PP (IN against) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (DT this)) (VP (MD will) (VP (VB result) (PP (IN in) (S (NP (PRP$ her) (NN family)) (VP (RB not) (AUXG having) (NP (JJ legal) (NN protection)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NP (NNP S1) (POS 's)) (JJ religious) (NN freedom)) (VP (AUX is) (VP (AUXG being) (VP (VBN persecuted) (SBAR (IN because) (S (NP (PRP she)) (VP (AUX is) (ADVP (RB still)) (ADJP (JJ bale) (S (VP (TO to) (VP (VB hold) (NP (JJ religious) (NNS ceremonies)))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT the) (JJ same) (NN argument)) (VP (MD can) (VP (AUX be) (VP (VBN made) (S (ADVP (RB fro)) (NP (NNP m)) (NP (PRP$ her) (NN perspective))))))))) (. .)))

(S1 (S (NP (NNP S1) (POS 's)) (VP (VBP believe) (SBAR (S (PP (IN of) (NP (NN marriage))) (VP (AUX is) (SBAR (IN that) (S (NP (PRP it)) (VP (MD should) (VP (AUX be) (VP (VBN based) (PP (PP (IN on) (NP (NN love))) (CC and) (RB not) (PP (IN on) (NP (NN sex))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (JJ religious) (NN freedom) (NN argument)) (VP (MD could) (VP (AUX be) (VP (VBN used) (PP (IN in) (NP (DT any) (NN format))) (S (VP (TO to) (VP (VB justify) (NP (DT any) (NN action))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (ADJP (JJ angered) (SBAR (IN that) (S (NP (NP (DT the) (JJ religious) (NN freedom)) (PP (IN of) (NP (DT another) (NN person)))) (VP (MD will) (VP (VP (VB go) (PP (IN into) (NP (NN law)))) (CC and) (VP (VB bar) (NP (PRP her)) (PP (IN from) (NP (NN marriage))) (SBAR (IN so) (S (NP (PRP she)) (VP (MD can) (VP (VB sustain) (NP (PRP$ her) (JJ heterosexual) (NN privilege))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ disagrees) (CC and) (VBZ thinks) (SBAR (S (`` ``) (NP (PRP she)) ('' '') (VP (AUX is) (VP (VBG doing) (SBAR (WHNP (WP what)) (S (NP (PRP she)) (VP (VBZ thinks) (SBAR (S (VP (AUX is) (PP (IN before) (NP (NN society)))))))))))))) (. .)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (VBP seem) (S (VP (TO to) (VP (VB get) (ADJP (VBN annoyed) (PP (IN with) (NP (DT each) (JJ other)))) (PP (IN by) (NP (NP (DT the) (NN end)) (PP (IN of) (NP (NN dialog))))))))) (. .)))

