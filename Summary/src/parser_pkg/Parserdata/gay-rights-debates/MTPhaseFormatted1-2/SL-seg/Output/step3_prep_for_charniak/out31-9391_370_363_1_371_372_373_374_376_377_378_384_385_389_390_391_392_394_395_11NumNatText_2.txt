<s> <PARAGRAPH> </s>
<s>  S1 states that "it is nothing like racial discrimination", in regards to discrimination against gays, and denies saying it. </s>
<s> He does not feel that his post implied that the plight of gays is nothing like the plight of blacks. </s>
<s> He asks why S2 would think that he implied that, and calls the assumption that he did wrong. </s>
<s> Several times he accuses S2 of not knowing what he is talking about. </s>
<s> He says that S2 has only succeeded in discrediting himself. </s>
<s> S2 responds to S1 saying, "it is nothing like racial discrimination", with the argument that all segregation, throughout history, has been the same. </s>
<s> That the perpetrators have used the same argument to support it, namely the claim that "this is nothing like the last" and that, without exception, it is. </s>
<s> He contends that if S1 had not wanted to compare racial discrimination to discrimination against gays he miss-phrased his post, and asks S1 to explain himself. </s>
<s> He accuses S1 of throwing a temper tantrum to validate his argument, and that the job of discrediting him is painfully easy. </s>
<s>  </s>
