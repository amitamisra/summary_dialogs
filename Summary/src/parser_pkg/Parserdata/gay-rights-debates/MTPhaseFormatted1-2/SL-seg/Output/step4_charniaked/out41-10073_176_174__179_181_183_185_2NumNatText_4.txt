(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN while) (S (NP (DT a) (NN person)) (VP (MD may) (VP (AUX be) (VP (VBN born) (NP (DT a) (JJ certain) (NN color))))))) (NP (PRP he)) (VP (MD should) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VP (VB control) (NP (PRP$ his) (CC or) (PRP$ her) (JJ sexual) (NN behavior))) (CC and) (RB not) (VP (VB engage) (PP (IN in) (NP (`` ``) (JJ gay) (NN sex) (NN sin) ('' ''))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBD stated) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NNS homosexuals)) (VP (VBP cannot) (NP (NN control)) (NP (DT this) (NN behavior))))) (, ,) (NP (RB then) (JJ public) (NNS places)) (VP (AUX are) (ADJP (JJ unsafe)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD closed) (NP (PRP$ his) (NNS comments)) (PP (IN by) (S (VP (VBG saying) (SBAR (IN that) (S (NP (JJ homosexual) (JJ sexual) (NN behavior)) (VP (VP (AUX is) (NP (DT a) (NN choice))) (CC and) (VP (AUX is) (PP (IN against) (NP (NP (NNP God) (POS 's)) (NN word))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBD pointed) (PRT (RP out)) (SBAR (IN that) (S (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (ADJP (JJ gay)))))) (VP (AUX were) (VP (VBN born) (NP (DT that) (NN way))))))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (AUX did))) (CC and) (S (VP (AUX do) (RB not) (VP (AUX have) (NP (NP (DT a) (NN choice)) (PP (IN about) (NP (PRP$ their) (JJ sexual) (NN orientation))))))) (. .)))

(S1 (S (S (NP (PRP They)) (ADVP (RB either)) (VP (VBP like) (NP (NNS girls)))) (CC or) (S (CC or) (NP (PRP they)) (VP (VBP like) (UCP (NP (NNS guys)) (CC and) (SBAR (IN that) (S (NP (PRP it)) (NP (NP (DT the) (NN way)) (SBAR (S (NP (PRP it)) (VP (AUX is)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (MD should) (VP (VB remain) (S (VP (VB celibate) (NP (NP (DT all)) (PP (IN of) (NP (PRP$ their) (NNS lives)))) (SBAR (RB just) (IN because) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB approve) (PP (IN of) (NP (PRP$ their) (NN behavior))))))))))))))))) (. .)))

