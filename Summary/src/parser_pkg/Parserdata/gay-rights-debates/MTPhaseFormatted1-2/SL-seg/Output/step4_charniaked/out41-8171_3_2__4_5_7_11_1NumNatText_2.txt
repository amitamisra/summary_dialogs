(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ begins) (NP (NP (DT the) (NN dialog)) (PP (IN by) (NP (VBG attacking) (JJ conservative) (NNS republicans)))) (PP (IN for) (NP (NP (DT the) (NN lack)) (PP (IN of) (NP (JJ gay) (NNS rights))))) (PP (IN in) (NP (NNP America))))) (, ,) (CC and) (S (NP (NNS claims) (NNS democrats)) (VP (VP (AUX have) (VP (AUX been) (VP (VBG trying) (S (VP (TO to) (VP (VB progress) (NP (JJ gay) (NNS rights)))))))) (CC but) (VP (AUX are) (ADJP (JJ afraid) (S (VP (TO to) (VP (VB loose) (NP (PRP$ their) (JJ political) (NN position))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ responds) (PP (IN by) (S (VP (VBG commenting) (SBAR (IN that) (S (NP (NNP Democratic) (NNP Vice) (NNP President) (NNP Joe) (NNP Biden)) (VP (VBD stated) (SBAR (S (NP (PRP he)) (VP (AUX was) (PP (IN against) (NP (JJ gay) (NN marriage))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (PRP he)) (VP (AUX was) (VP (VBG lying) (S (VP (TO to) (VP (VB attract) (NP (NNS votes)))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ expresses) (NP (NP (NN desire)) (PP (IN for) (NP (NP (DT a) (NN candidate)) (SBAR (WHNP (WP who)) (S (VP (VBZ stands) (PP (IN behind) (NP (PRP$ their) (NNS beliefs))))))))) (ADVP (RB regardless) (PP (IN of) (NP (JJ popular) (NN opinion)))))) (, ,) (CC and) (S (RB not) (VP (VBP lie) (S (VP (TO to) (VP (VB get) (NP (NNS votes))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ calls) (S (NP (NNP S2)) (ADJP (JJ naive) (PP (IN for) (S (VP (VBG believing) (SBAR (IN that) (S (NP (VBG lying)) (VP (AUX does) (RB not) (VP (VB happen) (PP (IN in) (NP (NNS politics))) (NP (NN today)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT the) (NN responsibility)) (PP (IN of) (NP (DT the) (NN public) (S (VP (TO to) (VP (VB decipher) (NP (JJ political) (NN language)) (S (VP (TO to) (VP (VB know) (SBAR (WHADVP (WRB when)) (S (NP (NNS politicians)) (VP (AUX are) (UCP (VP (VBG lying)) (CC or) (RB not)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NN public)) (VP (MD should) (VP (VP (VB demand) (NP (NN honesty))) (CC but) (RB not) (VP (VB assume) (NP (PRP it)))))))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX is) (SBAR (S (NP (DT no) (NN point)) (VP (AUX is) (VP (VBG questioning) (NP (NNS candidates)) (, ,) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (VP (VBN known) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG going) (S (VP (TO to) (VP (VB lie))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ addresses) (SBAR (IN that) (S (NP (NNP S1)) (ADVP (RB also)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NN honesty)) (VP (MD should) (VP (AUX be) (VP (VBN demanded) (SBAR (IN because) (S (NP (PRP$ their) (JJR earlier) (NNS statements)) (VP (VBD insinuated) (SBAR (IN that) (S (NP (PRP they)) (VP (VBD thought) (SBAR (S (NP (DT this) (NN practice)) (VP (AUX was) (ADJP (JJ wrong))))))))))))))))))))))

