<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN it/PRP is/AUX a/DT dangerous/JJ spiritual/JJ situation/NN that/IN S2/NNP is/AUX in/IN ,/, </C>
<C>and/CC to/TO take/VB homosexuality/NN lightly/RB or/CC amusing/JJ in/IN any/DT way/NN is/AUX proof/NN that/IN one/NN does/AUX not/RB care/VB about/IN it/PRP ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB that/IN anyone/NN should/MD be/AUX laughing/VBG or/CC joking/VBG at/IN what/WP they/PRP think/VBP is/AUX a/DT perverted/JJ and/CC unnatural/JJ practice/NN that/WDT homosexuals/NNS engage/VBP in/IN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB that/IN sexual/JJ activity/NN is/AUX what/WP makes/VBZ up/RP the/DT homosexual/JJ community/NN ,/, </C>
<C>and/CC that/IN people/NNS do/AUX not/RB ,/, and/CC should/MD not/RB ,/, care/VB about/IN what/WP one/NN does/AUX in/IN the/DT bedroom/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP such/JJ activities/NNS should/MD remain/VB private/JJ </C>
<C>and/CC that/IN it/PRP is/AUX no/DT one/NN else/RB 's/AUX business/NN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB that/IN what/WP is/AUX done/AUX behind/IN closed/JJ doors/NNS defines/VBZ a/DT person/NN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB agree/VB that/IN homosexuals/NNS should/MD be/AUX classified/VBN as/IN deviant/NN ,/, as/IN heterosexual/JJ activities/NNS can/MD be/AUX likewise/RB ./. </C>
</S>
</P>
</T>
