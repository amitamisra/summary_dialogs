<T>
<P>
</P>
<P>
<S>
<C>Speaker/NN one/CD is/AUX a/DT heterosexual/NN who/WP believes/VBZ that/IN in/IN our/PRP$ democratic/JJ society/NN ,/, the/DT majority/NN rules/VBZ ,/, </C>
<C>and/CC that/IN this/DT majority/NN has/AUX so/RB far/RB consistently/RB ruled/VBN again/RB gay/JJ rights/NNS ./. </C>
</S>
<S>
<C>Speaker/NN one/CD believes/VBZ that/IN courts/NNS should/MD not/RB overturn/VB decisions/NNS based/VBN off/IN the/DT minority/NN complaining/VBG and/CC should/MD abide/VB by/IN the/DT majority/NN ,/, </C>
<C>how/WRB our/PRP$ constitution/NN and/CC government/NN was/AUX designed/VBN ./. </C>
</S>
<S>
<C>Speaker/NNP two/CD believes/VBZ that/IN the/DT majority/NN is/AUX oppressing/VBG the/DT LGBT/JJ community/NN and/CC being/AUXG treated/VBN as/IN second-class/JJ citizens/NNS ./. </C>
</S>
<S>
<C>The/DT speaker/NN believes/VBZ that/IN if/IN a/DT law/NN effects/VBZ an/DT individuals/NNS '/POS life/NN in/IN an/DT unjust/JJ way/NN ,/, </C>
<C>then/RB it/PRP is/AUX courts/NNS responsibility/NN to/TO overrule/VB it/PRP for/IN the/DT individual/NN 's/POS sake/NN ./. </C>
</S>
<S>
<C>Speaker/NNP two/CD believes/VBZ society/NN is/AUX oppressing/VBG people/NNS ,/, </C>
<C>and/CC that/IN if/IN a/DT law/NN adversely/RB effects/VBZ even/RB one/CD person/NN ,/, </C>
<C>it/PRP is/AUX unjust/JJ ./. </C>
</S>
</P>
</T>
