<T>
<P>
</P>
<P>
<S>
<C>S2/NNP thinks/VBZ homosexuality/NN is/AUX a/DT sin/NN and/CC should/MD be/AUX a/DT crime/NN along/IN with/IN heavy/JJ fines/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ liberals/NNS are/AUX hypocritical/JJ in/IN talking/VBG about/IN tolerance/NN and/CC justice/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ liberals/NNS think/VBP only/RB they/PRP are/AUX right/JJ ,/, push/VBP the/DT liberal/JJ way/NN as/IN social/JJ norm/NN ,/, and/CC ridicule/NN anyone/NN that/WDT does/AUX not/RB agree/VB ./. </C>
</S>
<S>
<C>S2/NNP points/VBZ out/RP this/DT is/AUX not/RB a/DT theocracy/NN </C>
<C>and/CC that/IN if/IN someone/NN wants/VBZ to/TO live/VB in/IN a/DT society/NN based/VBN on/IN religion/NN </C>
<C>there/EX are/AUX other/JJ places/NNS that/WDT have/AUX that/DT ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ Christians/NNS are/AUX hypocrites/NNS </C>
<C>because/IN they/PRP claim/VBP to/TO be/AUX persecuted/VBN </C>
<C>because/IN they/PRP can/MD not/RB force/VB their/PRP$ religion/NN on/IN others/NNS ./. </C>
</S>
<S>
<C>He/PRP says/VBZ they/PRP may/MD practice/VB </C>
<C>as/IN they/PRP wish/VBP ,/, </C>
<C>but/CC they/PRP can/MD not/RB force/VB others/NNS to/TO do/AUX so/RB ./. </C>
</S>
</P>
</T>
