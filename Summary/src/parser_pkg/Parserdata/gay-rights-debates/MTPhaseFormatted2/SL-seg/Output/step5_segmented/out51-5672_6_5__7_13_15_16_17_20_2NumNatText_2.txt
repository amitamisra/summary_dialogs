<T>
<P>
</P>
<P>
<S>
<C>S1/NNP feels/VBZ that/IN Colson/NNP 's/POS is/AUX the/DT most/RBS compelling/JJ argument/NN for/IN the/DT Marriage/NN Protection/NNP Amendment/NNP yet/RB ,/, </C>
<C>but/CC that/IN he/PRP still/RB does/AUX not/RB support/VB it/PRP personally/RB ,/, as/IN the/DT changing/NN the/DT constitution/NN of/IN it/PRP is/AUX not/RB right/JJ ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN the/DT legalization/NN of/IN gay/JJ marriage/NN would/MD redefine/VB what/WP family/NN is/AUX in/IN a/DT negative/JJ manner/NN ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN the/DT broadening/VBG of/IN what/WP constitutes/VBZ the/DT definition/NN of/IN marriage/NN would/MD somehow/RB weaken/VB traditional/JJ marriage/NN ./. </C>
</S>
<S>
<C>This/DT ,/, he/PRP says/VBZ ,/, would/MD lead/VB to/TO more/JJR crime/NN ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ that/IN the/DT problems/NNS with/IN traditional/JJ marriage/NN in/IN this/DT country/NN began/VBD </C>
<C>long/RB before/IN gay/JJ marriage/NN began/VBD to/TO be/AUX legalized/VBN ./. </C>
</S>
<S>
<C>He/PRP makes/VBZ an/DT analogy/NN of/IN a/DT man/NN jumping/VBG out/IN of/IN a/DT plane/NN before/IN gay/JJ marriage/NN ,/, and/CC gay/JJ marriage/NN being/AUXG blamed/VBN ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB feel/VB that/IN gay/JJ marriage/NN has/AUX any/DT effect/NN on/IN traditional/JJ marriage/NN at/IN all/DT ,/, </C>
<C>and/CC that/IN society/NN is/AUX not/RB harmed/VBN by/IN the/DT inclusion/NN of/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN any/DT statement/NN that/IN says/VBZ that/IN gay/JJ marriage/NN harms/NNS society/NN should/MD be/AUX proven/VBN </C>
<C>before/IN being/AUXG widely/RB talked/VBN about/IN ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN S1/NNP is/AUX misrepresenting/VBG himself/PRP and/CC twisting/VBG his/PRP$ argument/NN to/TO make/VB statements/NNS that/WDT are/AUX different/JJ from/IN what/WP Colson/NNP had/AUX written/VBN ./. </C>
</S>
</P>
</T>
