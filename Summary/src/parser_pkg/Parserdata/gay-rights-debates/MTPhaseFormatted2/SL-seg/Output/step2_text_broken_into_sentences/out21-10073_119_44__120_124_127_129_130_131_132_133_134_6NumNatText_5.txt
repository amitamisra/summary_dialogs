<PARAGRAPH> S1 argues that allowing gay marriage would set a precedent that would be used to allow marriage for other forms of traditionally socially unacceptable intimate or sexual relationships, such as incest between half brothers or a mother and son, and/or polygamy.
S2 is in a 15 year male homosexual relationship and is anticipating twin children that are biologically related to him.
He challenges S1 to explain why he and his family should not be entitled to the many benefits associated with marriage, such as social security surviver status.
S1 provides a putative explanation by noting that his children are outsourced, meaning that one of the parents of the children is not part of the family intended to receive the benefits of marriage.
S1 also notes that others do not qualify, and likens S2 to a single mother who has many children and then complains about not enough help from the government.
S1 repeatedly compares gay relationships to other types of relationships which he considers to be devious in order to justify excluding gays from marriage rights.
