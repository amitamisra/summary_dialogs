(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ necessary)) (SBAR (IN for) (S (NP (NNP S2)) (VP (TO to) (VP (VP (VB provide) (NP (JJ credible) (NNS arguments))) (CC and) (VP (NN scholarship) (S (VP (VBG refuting) (NP (NP (DT the) (NN resurrection)) (PP (IN of) (NP (NNP Christ)))) (S (VP (TO to) (VP (AUX be) (VP (VBN taken) (ADVP (RB seriously))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (S (VP (AUXG being) (NP (NP (DT a) (JJ left) (NN wing)) (ADJP (NNP Christ) (JJ denier))))))) (CC and) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NNP S2)) (VP (MD will) (VP (VB go) (PP (TO to) (NP (NN Hell))) (SBAR (IN unless) (S (NP (PRP he)) (VP (VBZ repents)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT the) (NN burden)) (PP (IN of) (NP (NN proof)))) (VP (MD should) (ADVP (RB actually)) (VP (AUX be) (PP (IN on) (NP (NP (DT the) (NNS people)) (VP (VBG claiming) (SBAR (S (NP (DT the) (NN resurrection)) (VP (AUX is) (ADJP (JJ true))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (DT the) (NN fact) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (NP (NP (DT no) (NN eye) (NN witness) (NNS accounts) (CC or) (JJ other) (NN proof)) (PP (IN of) (NP (DT the) (NN resurrection))))))) (CC and) (SBAR (IN that) (S (NP (JJ scientific) (NN evidence)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (ADJP (RB highly) (JJ improbable)) (SBAR (IN for) (S (NP (DT a) (NN man)) (VP (TO to) (VP (AUX be) (VP (VBN revived) (PP (IN after) (S (VP (AUXG being) (ADJP (JJ dead) (PP (IN for) (NP (CD three) (NNS days)))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (SBAR (RB even) (IN if) (S (NP (DT the) (JJ Bible)) (VP (AUX were) (ADJP (RB completely) (JJ true))))) (, ,) (NP (JJ many) (JJ self-professed) (NNS Christians)) (VP (MD would) (ADVP (RB also)) (VP (AUX be) (VP (VBN condemned) (PP (TO to) (NP (NN Hell))) (PP (IN for) (NP (PRP$ their) (JJ non-Christian) (NNS behaviors))))))))) (. .)))

