<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ DOMA/NN on/IN a/DT state/NN level/NN is/AUX unconstitutional/JJ ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ gay/JJ marriage/NN has/AUX an/DT impact/NN on/IN all/DT tax/NN payers/NNS in/IN a/DT negative/JJ way/NN ./. </C>
</S>
<S>
<C>He/she/NNP believes/VBZ the/DT state/NN ban/NN on/IN gay/JJ marriage/NN in/IN California/NNP was/AUX enacted/VBN legally/RB additionally/RB citing/VBG the/DT federal/JJ ruling/NN that/IN each/DT state/NN must/MD acknowledge/VB the/DT marriage/NN laws/NNS of/IN another/DT state/NN ./. </C>
</S>
<S>
<C>S2/NNP provides/VBZ the/DT argument/NN that/IN DOMA/NN on/IN a/DT federal/JJ level/NN could/MD actually/RB be/AUX found/VBN unconstitutional/JJ ./. </C>
</S>
<S>
<C>S2/NNP also/RB argues/VBZ the/DT impact/NN on/IN taxpayers/NNS advising/VBG gay/JJ couples/NNS pay/VB taxes/NNS on/IN heterosexual/JJ marriages/NNS and/CC activities/NNS </C>
<C>so/IN they/PRP are/AUX impacted/VBN based/VBN on/IN fundamental/JJ law/NN thus/RB making/VBG S1/NNP 's/POS observance/NN incorrect/JJ ,/, in/IN S2/NNP 's/POS theory/NN ./. </C>
</S>
<S>
<C>S2/NNP also/RB advises/VBZ a/DT state/NN constitution/NN cannot/VB supersede/VB the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN S2/NNP is/AUX incorrect/JJ concerning/VBG each/DT state/NN 's/POS recognition/NN ./. </C>
</S>
</P>
</T>
