<s> <PARAGRAPH> </s>
<s>  S1 and S2 are debating gay marriage. </s>
<s> S1 argues that gay marriage should be decided by a public vote. </s>
<s> S2 argues that it's cruel to limit someone else from enjoying rights you can currently enjoy. </s>
<s> Opponents of gay marriage should try to have more empathy for gay people who wish to marry. </s>
<s> S1 retorts that it is not cruel for society to bar someone from enjoying a particular right. </s>
<s> Society can and does decide what it deems to be socially acceptable through consensus. </s>
<s> S2 states that tradition or being mundane does not make something any more wrong or right. </s>
<s> S2 compares S1's argument to a justification for slavery. </s>
<s> The majority once held slavery to be correct, hence it was morally and socially acceptable according to S1's logic. </s>
<s> S1 says that he is not arguing for something being correct because it's traditional, only that it can be changed through convincing society to change. </s>
<s> It is up to society to determine what is permissible through a dialog between the proponents of gay marriage and society. </s>
<s>  </s>
