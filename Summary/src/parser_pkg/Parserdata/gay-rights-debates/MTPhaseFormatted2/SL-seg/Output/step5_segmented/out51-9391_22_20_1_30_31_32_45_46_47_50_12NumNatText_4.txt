<T>
<P>
</P>
<P>
<S>
<C>S1/NNP reminds/VBZ S2/NNP that/IN he/PRP is/AUX in/IN favor/NN of/IN civil/JJ unions/NNS ,/, not/RB just/RB marriage/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB declares/VBZ that/IN he/PRP is/AUX a/DT religious/JJ person/NN </C>
<C>and/CC that/IN the/DT Bible/NNP is/AUX clear/JJ on/IN homosexuality/NN ./. </C>
</S>
<S>
<C>He/PRP lets/VBZ him/PRP know/VB he/PRP will/MD not/RB vote/VB in/IN favor/NN of/IN civil/JJ unions/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN voters/NNS can/MD do/AUX as/IN they/PRP please/VBP ./. </C>
</S>
<S>
<C>As/IN a/DT voter/NN he/PRP chooses/VBZ not/RB to/TO want/VB same/JJ sex/NN marriage/NN included/VBN under/IN the/DT definition/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ that/IN he/PRP has/AUX proven/VBN the/DT fact/NN that/IN gay/JJ marriage/NN does/AUX affect/VB us/PRP financially/RB ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN the/DT Bible/NNP has/AUX nothing/NN to/TO do/AUX with/IN the/DT law/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN the/DT Bible/NNP is/AUX nothing/NN but/CC a/DT bunch/NN of/IN fairy/NN tails/NNS ./. </C>
</S>
<S>
<C>The/DT government/NN does/AUX not/RB consider/VB marriage/NN to/TO be/AUX a/DT sacrament/NN ./. </C>
</S>
<S>
<C>Marriage/NN licenses/NNS are/AUX contracts/NNS and/CC not/RB a/DT religion/NN ./. </C>
</S>
</P>
</T>
