<s> <PARAGRAPH> </s>
<s>  S1 and S2 are discussing the political processes that may lead to gay marriage. </s>
<s> S2 is of the opinion that society and government failure to recognize gay marriage and provide it equal protection is a civil rights violation being carried out against a minority group, implicitly suggesting that it should be protected under the Constitution by the Courts. </s>
<s> S1 on the other hand believes the issue is one that needs to spend time in the public awareness and consideration and be realized over time in the results of various political processes such as election results, initiatives in State legislatures and ballot initiatives. </s>
<s> S2 complains that this is obfuscation and an attempt by S1 to avoid the rights violation issue. </s>
<s> S2 disagrees and notes that just calling something a violation or stupid or wrong does not advance the process, and defies S1 to draw appropriate lines on where to draw the line, such as with regard to protection of other less acceptable group intimate relationships. </s>
<s> S2 asserts that the government has protected minorities against bigotry in the past. </s>
<s>  </s>
