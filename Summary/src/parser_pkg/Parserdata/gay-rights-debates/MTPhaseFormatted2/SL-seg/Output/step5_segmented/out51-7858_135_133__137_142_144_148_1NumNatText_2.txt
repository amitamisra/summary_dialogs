<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ the/DT definition/NN of/IN marriage/NN is/AUX subject/JJ to/TO change/NN ./. </C>
</S>
<S>
<C>As/IN an/DT example/NN S1/NNP points/VBZ to/TO an/DT old/JJ definition/NN of/IN marriage/NN in/IN which/WDT a/DT woman/NN lost/VBD her/PRP$ identity/NN into/IN that/DT of/IN a/DT man/NN ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ fundamental/JJ rights/NNS are/AUX not/RB subject/JJ to/TO vote/VB and/CC cites/VBZ Loving/NNP v/NNP ./. </C>
</S>
<S>
<C>Virginia/NNP as/IN an/DT example/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ the/DT courts/NNS are/AUX an/DT intermediate/JJ body/NN between/IN the/DT people/NNS and/CC legislature/NN ;/: </C>
<C>thus/RB to/TO ban/VB gay/JJ marriage/NN </C>
<C>one/PRP must/MD prove/VB it/PRP is/AUX not/RB a/DT right/NN or/CC give/VBP compelling/JJ evidence/NN against/IN it/PRP ./. </C>
</S>
<S>
<C>S2/NNP wonders/VBZ what/WP marriage/NN being/AUXG a/DT fundamental/JJ right/NN has/AUX to/TO do/AUX with/IN defining/VBG marriage/NN ./. </C>
</S>
<S>
<C>If/IN marriage/NN is/AUX subject/JJ to/TO change/NN </C>
<C>then/RB he/PRP asks/VBZ how/WRB it/PRP is/AUX supposed/VBN to/TO be/AUX changed/VBN if/IN not/RB by/IN vote/NN ./. </C>
</S>
<S>
<C>He/PRP takes/VBZ issue/NN with/IN the/DT judiciary/NN becoming/VBG the/DT legislative/JJ branch/NN ./. </C>
</S>
</P>
</T>
