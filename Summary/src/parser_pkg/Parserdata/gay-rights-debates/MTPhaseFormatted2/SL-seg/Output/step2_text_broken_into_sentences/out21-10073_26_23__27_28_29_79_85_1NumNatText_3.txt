<PARAGRAPH> Two people are discussing gay marriage.
S1 states that it has nothing to do with marriage equality, but an attempt to redefine the meaning of marriage.
He wishes that the gay posters would be honest and not use words such as equality and discrimination where they do not apply, since marriage is an institution created to be a union between man and woman that leads to the creation of children.
He states that marriage has nothing to do with two people giving each other a wedding ring.
S2 retorts by questioning how it is not discrimination if he can not marry the person of his choice but a heterosexual can.
He also states that Las Vegas itself refutes S1's arguments.
