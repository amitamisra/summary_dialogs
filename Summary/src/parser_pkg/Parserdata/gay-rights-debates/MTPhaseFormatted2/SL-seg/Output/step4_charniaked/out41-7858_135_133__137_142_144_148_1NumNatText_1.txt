(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (VB discuss) (S (NP (DT the) (NNP United) (NNPS States)) (ADJP (JJ Constitutional) (SBAR (UCP (NP (JJ legal) (NN system)) (CC and) (IN whether) (CC and) (WHADVP (WRB how))) (S (NP (JJ same) (NN sex) (NN marriage)) (VP (MD can) (VP (AUX be) (VP (VBN banned))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NP (DT the) (NN concept)) (PP (IN of) (NP (NN marriage)))) (VP (VP (AUX has) (VP (VBN evolved) (ADVP (RB dramatically)) (PP (IN over) (NP (NN time))))) (CC and) (VP (MD can) (ADVP (RB now)) (VP (AUX be) (VP (VBN declared) (PP (IN by) (NP (DT the) (NNP Supreme) (NNP Court))) (S (VP (TO to) (VP (AUX be) (NP (DT a) (JJ fundamental) (NN right) (S (RB not) (VP (TO to) (VP (AUX be) (VP (VBN denied) (PP (TO to) (NP (JJ same) (NN sex) (NNS couples)))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (PRP it)) (VP (AUX is) (NP (NP (DT a) (JJ legislative) (NN matter)) (SBAR (WHNP (WDT which)) (S (VP (MD should) (VP (AUX be) (VP (VBN left) (PP (TO to) (NP (NN voting))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ fundamental) (NNS rights)) (VP (VP (AUX are) (VP (VBN protected) (PP (IN by) (NP (DT the) (NNP Constitution))))) (CC and) (VP (AUX are) (RB not) (ADJP (JJ subject) (PP (TO to) (NP (DT a) (NN plebescite))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (S (VP (VP (VBG believing) (PP (IN in) (NP (DT an) (JJ activist) (NN judiciary)))) (CC and) (VP (VBG evolving) (NP (NP (JJ Constitutional) (NNS standards)) (SBAR (WHNP (WDT which)) (S (VP (VB usurp) (NP (NP (DT the) (NN role)) (PP (IN of) (NP (DT the) (NN legislature))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ criticizes) (NP (NNP Loving) (NNP v))) (. .)))

(S1 (NP (NP (NNP Virginia)) (ADJP (ADJP (RB as) (JJ incorrect)) (PP (IN in) (NP (NP (NN reasoning)) (CC and) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (JJ judicial) (NN activism))))))) (. .)))

