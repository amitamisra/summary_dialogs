<s> <PARAGRAPH> </s>
<s>  S1 claims that despite arguments linking same-sex marriage to negative effects on society and traditional families, he is against the constitutional amendment to ban same-sex marriages on the grounds that it is an inappropriate topic for a constitutional amendment. </s>
<s> He references a person named Colson who believes that the terms "marriage" and "family" need to meet specific criteria, and that legalizing same-sex marriage would change those criteria, leading to changes in the meaning of traditional marriage. </s>
<s> He also claims that Colson believes that traditional families have the best outcomes in regards to criminal behavior of offspring. </s>
<s> S2 rejects the claim that same-sex marriage has a negative effect on society, suggesting that traditional marriage was on the decline long before same-sex marriage was ever legalized. </s>
<s> He also questions what negative effect same-sex marriage could have on society, claiming that there are no studies suggesting that two heterosexual parents fare any better than two homosexual parents because there is no difference in reality, and that there is no reason to predict that same-sex marriages would affect crime. </s>
<s>  </s>
