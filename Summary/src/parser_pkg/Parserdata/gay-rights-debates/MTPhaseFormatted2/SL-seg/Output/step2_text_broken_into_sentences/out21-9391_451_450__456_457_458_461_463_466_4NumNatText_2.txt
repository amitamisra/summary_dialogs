<PARAGRAPH> S1 and S2 are discussing gay marriage.
Both seem to be somewhat in favor, although S2 seems a bit indifferent to the issue.
S1 feels it is unfair for the gay community to be forced to recognize heterosexual marriages while gay citizens are denied the right to wed.
S2 believes things, including gay marriage, should be voted on and that right and wrong are personal judgments.
S1 then wonders if S2 feels all civil rights should be voted on.
S2 advises that civil rights are actually constitutional rights and cannot and should not be voted on and does not feel that unmarried people, gay or straight are not denied civil rights.
Two people with similar opinions but different views.
