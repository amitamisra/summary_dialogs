<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN all/DT homosexuals/NNS are/AUX doomed/VBN to/TO the/DT fiery/JJ pits/NNS of/IN hell/NN ,/, </C>
<C>grouping/VBG them/PRP all/DT together/RB and/CC calling/VBG them/PRP godless/VB heathens/NNS ./. </C>
</S>
<S>
<C>He/PRP is/AUX of/IN the/DT mind/NN that/IN it/PRP is/AUX his/PRP$ christian/JJ duty/NN to/TO inform/VB them/PRP of/IN this/DT ,/, </C>
<C>as/IN non/NN of/IN S1/NNP 's/POS brethren/NNS will/MD ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN until/IN some/DT scholarly/JJ evidence/NN is/AUX produced/VBN that/IN refutes/VBZ the/DT resurrection/NN of/IN Christ/NNP ,/, </C>
<C>any/DT non-believers/NNS are/AUX just/RB noisy/JJ Christ/NNP deniers/NNS ./. </C>
</S>
<S>
<C>He/PRP uses/VBZ strong/JJ words/NNS ,/, threats/NNS ,/, and/CC insults/NNS ,/, </C>
<C>to/TO validate/VB his/PRP$ argument/NN ,/, as/RB well/RB as/IN a/DT story/NN on/IN Fox/NNP news/NN ./. </C>
</S>
<S>
<C>S2/NNP calls/VBZ S1/NNP 's/POS entire/JJ argument/NN into/IN question/NN ,/, does/AUX not/RB believe/VB that/IN he/PRP will/MD be/AUX going/VBG to/TO Hell/NN ,/, </C>
<C>that/IN it/PRP does/AUX not/RB exist/VB ,/, </C>
<C>and/CC that/IN there/EX is/AUX no/DT proof/NN to/TO the/DT contrary/NN ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN if/IN proof/NN of/IN the/DT resurrection/NN of/IN Christ/NNP existed/VBD </C>
<C>it/PRP would/MD have/AUX already/RB been/AUX produced/VBN ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ S1/NNP of/IN dodging/VBG the/DT argument/NN and/CC attempting/VBG to/TO reverse/VB the/DT burden/NN of/IN proof/NN ,/, </C>
<C>he/PRP calls/VBZ this/DT tactic/NN pathetic/JJ and/CC uses/VBZ Fox/NNP as/IN proof/NN of/IN this/DT point/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN he/PRP does/AUX not/RB think/VB that/IN insults/NNS and/CC threats/NNS are/AUX an/DT intelligent/JJ or/CC useful/JJ form/NN of/IN debate/NN ./. </C>
</S>
</P>
</T>
