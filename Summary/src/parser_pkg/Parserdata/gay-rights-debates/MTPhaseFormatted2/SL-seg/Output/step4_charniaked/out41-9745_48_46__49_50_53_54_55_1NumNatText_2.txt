(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (NNP Thomas) (NNP Jefferson)) (VP (AUX did) (RB not) (VP (VB recommend) (NP (NP (DT the) (JJ negative) (NNS behaviors)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (VP (VBN targeted) (PP (IN toward) (NP (NNS homosexuals)))))))))))) (, ,) (CC and) (SBAR (IN that) (S (SBAR (IN although) (S (NP (NN homosexuality)) (VP (VP (AUX is) (NP (DT a) (NN sin)) (PP (VBG according) (PP (TO to) (NP (NNP Christianity))))) (CC and) (VP (MD may) (VP (AUX have) (VP (AUX been) (ADJP (JJ illegal)) (PP (IN in) (NP (DT the) (NN past))))))))) (, ,) (NP (PRP it)) (VP (AUX is) (RB not) (ADVP (RB anymore))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ points) (PRT (RP out)) (SBAR (SBAR (IN that) (S (NP (NNP America)) (VP (AUX is) (RB not) (NP (DT a) (NN theocracy))))) (CC and) (SBAR (IN that) (S (NP (NNS Christians)) (VP (AUX are) (RB not) (ADJP (VBN persecuted)) (, ,) (S (VP (VBG citing) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (ADJP (JJ free) (S (VP (TO to) (VP (VB practice) (NP (NN religion)))))) (SBAR (IN as) (S (NP (PRP they)) (VP (VBP choose))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNP Thomas) (NNP Jefferson)) (VP (AUX was) (ADJP (JJ correct) (PP (IN in) (S (VP (VBG labeling) (NP (NN homosexuality)) (PP (IN as) (NP (NP (DT a) (NN sin)) (CC and) (NP (DT a) (NN crime)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (EX there)) (VP (MD should) (VP (AUX be) (NP (JJ heavy) (JJ monetary) (NNS fines)) (PP (IN for) (S (VP (VP (VBG engaging) (PRT (RP in))) (CC or) (VP (VBG advocating) (NP (JJ homosexual) (NN behavior))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (JJ liberal) (NN tolerance)) (VP (AUX is) (ADJP (ADVP (RB just) (RB as)) (JJ dogmatic) (PP (IN as) (NP (NNP Christianity))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB no) (RBR more)) (ADJP (JJ right) (S (VP (TO to) (VP (VB base) (NP (NP (NNS laws)) (PP (IN on) (NP (NP (DT this) (NN perspective)) (PP (IN over) (NP (DT any) (DT another))))))))))))))) (. .)))

