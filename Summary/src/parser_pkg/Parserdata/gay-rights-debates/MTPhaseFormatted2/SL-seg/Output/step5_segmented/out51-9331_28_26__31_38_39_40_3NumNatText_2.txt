<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN same-sex/JJ marriage/NN is/AUX not/RB the/DT same/JJ essential/JJ component/NN of/IN society/NN that/IN opposite-sex/JJ marriage/NN is/AUX ./. </C>
</S>
<S>
<C>He/PRP clarifies/VBZ by/IN suggesting/VBG that/IN society/NN can/MD function/VB without/IN same-sex/JJ marriage/NN ,/, but/CC not/RB without/IN opposite-sex/JJ marriage/NN ,/, presumably/RB due/JJ to/TO the/DT reproductive/JJ element/NN involved/VBN ./. </C>
</S>
<S>
<C>He/PRP redefines/VBZ the/DT argument/NN to/TO be/AUX whether/IN the/DT government/NN has/AUX the/DT right/NN to/TO decide/VB which/WDT types/NNS of/IN marriages/NNS it/PRP will/MD recognize/VB ,/, </C>
<C>suggesting/VBG that/IN the/DT debate/NN is/AUX just/RB about/IN where/WRB the/DT line/NN is/AUX drawn/VBN ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN same-sex/JJ and/CC opposite-sex/JJ families/NNS have/AUX the/DT same/JJ needs/NNS and/CC responsibilities/NNS within/IN society/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN S1/NNP is/AUX correct/JJ that/IN society/NN could/MD function/VB </C>
<C>without/IN giving/VBG minorities/NNS the/DT same/JJ rights/NNS as/IN majority/NN </C>
<C>but/CC that/IN this/DT is/AUX amoral/JJ ,/, </C>
<C>and/CC that/IN there/EX is/AUX no/DT good/JJ reason/NN to/TO deny/VB people/NNS the/DT right/NN of/IN same-sex/JJ marriage/NN ./. </C>
</S>
</P>
</T>
