<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX not/RB against/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>Britain/NNP has/AUX it/PRP </C>
<C>and/CC he/PRP has/AUX not/RB seen/VBN it/PRP have/AUX a/DT negative/JJ impact/NN on/IN anyone/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ that/IN gays/NNS do/AUX not/RB have/AUX a/DT right/NN to/TO marriage/NN ,/, </C>
<C>but/CC it/PRP should/MD be/AUX allowed/VBN </C>
<C>because/IN it/PRP does/AUX not/RB harm/VB society/NN ./. </C>
</S>
<S>
<C>He/PRP wonders/VBZ why/WRB there/EX is/AUX not/RB a/DT positive/JJ approach/NN to/TO marriage/NN by/IN gay/JJ people/NNS ./. </C>
</S>
<S>
<C>While/IN he/PRP views/VBZ homosexuality/NN as/IN a/DT sin/NN ,/, </C>
<C>he/PRP views/VBZ other/JJ things/NNS as/IN a/DT sin/NN as/RB well/RB ./. </C>
</S>
<S>
<C>S2/NNP talks/VBZ of/IN gay/JJ marriage/NN in/IN history/NN </C>
<C>and/CC that/IN it/PRP is/AUX false/JJ to/TO claim/VB marriage/NN has/AUX always/RB been/AUX a/DT man/NN and/CC woman/NN ./. </C>
</S>
<S>
<C>S2/NNP concedes/VBZ it/PRP may/MD not/RB be/AUX a/DT right/NN ,/, </C>
<C>but/CC thinks/VBZ that/IN also/RB applies/VBZ to/TO heterosexuals/NNS ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB view/VB gay/JJ marriage/NN as/IN a/DT threat/NN to/TO Christianity/NNP and/CC asks/VBZ how/WRB Britain/NNP got/VBD it/PRP ./. </C>
</S>
</P>
</T>
