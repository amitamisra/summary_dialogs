(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ maintains) (NP (NP (DT a) (JJ fundamental) (NN view)) (PP (IN of) (NP (NN marriage)))) (PP (PP (IN as) (S (VP (AUXG being) (PP (IN between) (NP (NP (DT a) (NN man)) (CC and) (NP (DT a) (NN woman))))))) (CC and) (PP (IN as) (S (VP (AUXG having) (VP (AUX been) (S (VP (VB create) (S (VP (TO to) (VP (AUX have) (NP (NNS children)) (PP (IN in) (NP (DT a) (JJ stable) (NN environment)))))))))))))) (. .)))

(S1 (S (NP (JJ Gay) (NN marriage)) (PRN (, ,) (S (NP (NNP S1)) (VP (VBZ believes))) (, ,)) (VP (MD would) (VP (VB change) (NP (NP (DT the) (JJ very) (NN nature)) (PP (IN of) (NP (DT the) (NN marriage) (NN relationship)))) (, ,) (SBAR (IN while) (S (ADVP (RB merely)) (VP (VBG providing) (NP (JJ gay) (NNS men)) (NP (DT the) (NN ability) (S (VP (TO to) (VP (VB wear) (NP (NN wedding) (NNS rings)) (CC and) (VP (VB delude) (NP (PRP themselves)) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (NP (JJ normal) (JJ married) (NNS people)) (, ,) (SBAR (IN although) (S (NP (PRP he)) (VP (VBZ concedes) (SBAR (S (NP (DT the) (JJ latter) (NN part)) (VP (AUX is) (ADVP (RB only)) (NP (PRP$ his) (NN opinion))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (S (VP (VBG excluding) (NP (NNS gays)) (PP (IN from) (S (VP (AUXG being) (ADJP (JJ able) (S (VP (TO to) (VP (VB marry) (NP (NP (DT the) (NN person)) (PP (IN of) (NP (PRP$ their) (NN choice))))))))))))) (VP (AUX is) (NP (NN discrimination)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (NN discrimination)) (CC and) (NP (NN equality))) (VP (AUX are) (NP (JJ inappropriate) (NNS terms)) (SBAR (IN because) (S (NP (NP (DT the) (JJ very) (NN concept)) (PP (IN of) (NP (NN marriage)))) (VP (VBZ excludes) (NP (NNS homosexuals))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ regrets) (SBAR (IN that) (S (NP (NP (RBR more) (JJ gay) (NNS posters)) (PP (IN in) (NP (DT the) (NN forum)))) (VP (AUX do) (RB not) (VP (VB agree) (PP (IN with) (NP (PRP him))) (PP (IN on) (NP (DT the) (NN point)))))))) (. .)))

