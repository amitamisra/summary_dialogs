(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (S (NP (CD two) (NNS men)) (VP (AUXG having) (NP (JJ sexual) (NN intercourse)) (PP (IN with) (NP (CD one) (DT another))))) (VP (AUX is) (NP (NP (DT a) (NN deviant)) (VP (VB act) (CC and) (RB not) (VBN approved) (PP (IN by) (NP (JJS most) (NNS people))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB think) (SBAR (SBAR (IN that) (S (NP (NNS people)) (VP (VBP trust) (NP (DT the) (JJ homosexual) (NN community))))) (CC and) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (JJ religious) (NNS beliefs)))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB think) (SBAR (IN that) (IN because) (S (NP (DT some) (NNS people)) (VP (AUX are) (ADJP (ADJP (JJ okay) (PP (IN with) (NP (JJ homosexual) (NN activity)))) (SBAR (IN that) (S (NP (NN everyone)) (VP (VBZ needs) (S (VP (TO to) (VP (AUX be))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (JJS most) (NNS people)) (VP (AUX do) (RB not) (VP (VB care) (PP (IN about) (NP (NP (DT the) (NN sex) (NNS lives)) (PP (IN of) (NP (JJ other) (VBG consenting) (NNS adults))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS people)) (VP (VBP support) (NP (NP (JJ equal) (NNS rights)) (PP (IN for) (NP (NNS homosexuals)))) (ADVP (ADVP (RB as) (RB well)) (SBAR (IN as) (S (VP (VBP believe) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (RB perfectly) (JJ acceptable)))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (S (VP (VBG denying) (SBAR (IN that) (S (NP (DT a) (NN child)) (VP (VBZ needs) (NP (DT a) (VBG loving) (NN parent)) (S (VP (TO to) (VP (VB satiate) (NP (NP (CD one) (POS 's)) (JJ own) (NN bigotry)))))))))) (VP (AUX is) (ADJP (JJ wrong)))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP agree) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (RB not) (NP (NP (JJ wide) (NN support)) (PP (IN for) (NP (NP (DT the) (NN allowance)) (PP (IN of) (NP (NNS homosexuals) (S (VP (TO to) (VP (VB marry) (NP (NP (RB as) (JJ much)) (PP (IN as) (NP (JJ general) (NN acceptance)))))))))))))))) (. .)))

