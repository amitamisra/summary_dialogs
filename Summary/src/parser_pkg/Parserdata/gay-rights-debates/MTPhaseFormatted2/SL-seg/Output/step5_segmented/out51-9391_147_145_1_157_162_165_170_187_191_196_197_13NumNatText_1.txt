<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN the/DT amount/NN of/IN homosexual/JJ encounters/NNS at/IN rest/NN stops/NNS is/AUX very/RB high/JJ </C>
<C>and/CC that/IN rest/NN stops/NNS are/AUX closing/VBG down/RP because/RB of/IN them/PRP ./. </C>
</S>
<S>
<C>This/DT person/NN has/AUX not/RB found/VBN a/DT reasonable/JJ refutation/NN of/IN this/DT fact/NN in/IN their/PRP$ eyes/NNS from/IN any/DT gay/JJ supporter/NN ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB see/VB any/DT lack/NN of/IN moral/JJ character/NN exhibited/VBN by/IN people/NNS who/WP are/AUX exercising/VBG their/PRP$ Constitutional/JJ Rights/NNS to/TO deny/VB the/DT sanctity/NN of/IN marriage/NN ,/, to/TO what/WP they/PRP believe/VBP as/RB immoral/JJ associations/NNS ,/, </C>
<C>by/IN not/RB allowing/VBG the/DT definition/NN of/IN what/WP they/PRP believe/VBP marriage/NN to/TO be/AUX to/TO change/VB ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ there/EX is/AUX a/DT lack/NN of/IN moral/JJ character/NN in/IN those/DT who/WP vote/VB away/RP other/JJ people/NNS 's/POS rights/NNS ./. </C>
</S>
<S>
<C>They/PRP use/VBP Proposition/NNP 8/CD as/IN an/DT example/NN of/IN this/DT ./. </C>
</S>
<S>
<C>They/PRP compare/VBP the/DT example/NN of/IN illicit/JJ public/JJ sexual/JJ acts/NNS in/IN rest/NN stops/VBZ between/IN homosexuals/NNS to/TO heterosexual/JJ men/NNS who/WP also/RB make/VBP it/PRP public/JJ that/IN they/PRP do/AUX the/DT same/JJ with/IN heterosexual/JJ women/NNS in/IN public/JJ places/NNS ./. </C>
</S>
<S>
<C>This/DT person/NN somewhat/RB agrees/VBZ with/IN S1/NNP that/IN exercising/VBG Constitutional/JJ Rights/NNPS is/AUX not/RB morally/RB objectionable/JJ ,/, </C>
<C>but/CC using/VBG that/DT as/IN a/DT platform/NN to/TO deny/VB marriage/NN rights/NNS to/TO loving/NN ,/, </C>
<C>committed/JJ homosexual/JJ couples/NNS is/AUX ./. </C>
</S>
</P>
</T>
