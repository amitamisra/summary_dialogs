<PARAGRAPH> S1 believes that the gay-rights movement, S2 in particular, are paranoid.
There is not much being said to support his argument, most of the conversation is dominated by S2.
He argues that economic issues are more likely to hurt those in the gay rights movement than any possible laws.
S2 has concerns about the possibility of creating a true second class citizens of the GLBTQ community.
He refers to President Obama of being the only politician to put his beliefs aside and support gay marriage.
He references past injustices to support his claims that it is possible that gays will be lawfully discriminated against.
He mentions Native Americans and the genocide they experienced; Japanese Americans and the internment camps; African Americans and past slavery.
He worries that history will be allowed to repeat itself in the form of discrimination of homosexuals and points out recent attempts at renewing sodomy laws by some politicians.
At the end he seems to be in favor of an amendment to the Constitution to protect gay rights.
