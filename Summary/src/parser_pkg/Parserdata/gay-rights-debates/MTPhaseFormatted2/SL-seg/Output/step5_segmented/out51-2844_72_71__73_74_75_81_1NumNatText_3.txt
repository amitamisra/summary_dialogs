<T>
<P>
</P>
<P>
<S>
<C>S1/NNP is/AUX in/IN favor/NN of/IN marriage/NN between/IN homosexual/JJ couples/NNS ./. </C>
</S>
<S>
<C>Within/IN his/PRP$ debate/NN he/PRP brings/VBZ up/RP the/DT Republican/NNP use/NN of/IN ``/`` hot-button/JJ ''/'' issues/NNS ,/, </C>
<C>insinuating/VBG that/IN gay/JJ marriage/NN is/AUX an/DT issue/NN used/VBN to/TO divide/VB and/CC distract/VB from/IN other/JJ ``/`` bread/NN and/CC butter/NN ''/'' issues/NNS ,/, </C>
<C>mentioning/VBG tax/NN breaks/NNS for/IN the/DT rich/JJ and/CC global/JJ warming/NN ./. </C>
</S>
<S>
<C>He/PRP disagrees/VBZ with/IN decisions/NNS being/AUXG made/VBN that/WDT affect/VBP private/JJ affairs/NNS including/VBG gay/JJ marriage/NN and/CC also/RB mentions/VBZ abortion/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB brings/VBZ up/RP the/DT issue/NN of/IN morality/NN being/AUXG a/DT part/NN of/IN the/DT conversation/NN when/WRB one/CD 's/POS morality/NN and/CC beliefs/NNS is/AUX not/RB everyone/NN else/RB 's/POS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN gay/JJ marriage/NN is/AUX wrong/JJ because/IN of/IN the/DT traditional/JJ role/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN he/PRP does/AUX not/RB care/VB much/RB about/IN what/WP people/NNS do/AUX in/IN their/PRP$ own/JJ time/NN </C>
<C>but/CC draws/VBZ the/DT line/NN at/IN allowing/VBG same/JJ sex/NN marriage/NN ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ democrats/NNS and/CC the/DT left/JJ of/IN pushing/VBG a/DT radical/JJ agenda/NN ./. </C>
</S>
<S>
<C>He/PRP specifically/RB accuses/VBZ the/DT left/JJ of/IN manipulating/VBG the/DT system/NN ./. </C>
</S>
<S>
<C>He/PRP ends/VBZ with/IN defending/VBG states/NNS '/POS rights/NNS to/TO not/RB acknowledge/VB gay/JJ marriage/NN comparing/VBG it/PRP to/TO the/DT right/NN of/IN states/NNS to/TO not/RB acknowledge/VB a/DT driver/NN 's/POS licence/NN if/IN not/RB fitting/JJ into/IN their/PRP$ requirements/NNS ./. </C>
</S>
</P>
</T>
