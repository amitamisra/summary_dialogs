(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ Biblical) (NN prophecy)) (VP (AUX is) (RB not) (NP (NP (NN proof)) (PP (IN of) (NP (NNP America))) (VP (VBG heading) (PP (IN in) (NP (NP (DT the) (JJ wrong) (NN direction)) (PP (IN from) (NP (DT a) (JJ religious) (NN perspective))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ notes) (SBAR (IN that) (S (NP (NP (DT the) (NN number)) (PP (IN of) (NP (NNS earthquakes))) (VP (VBN seen) (PP (IN in) (NP (DT the) (JJ last) (NN century))))) (VP (AUX have) (VP (VBN increased) (ADVP (RB dramatically)) (, ,) (S (VP (VBG providing) (NP (NN evidence)) (PP (TO to) (NP (DT the) (NN contrary)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT a) (JJ pre-existing) (JJ religious) (NN belief)) (VP (AUX is) (ADJP (JJ necessary) (PP (IN for) (NP (NP (DT any) (NN evidence)) (VP (VBG regarding) (NP (NNS earthquakes) (S (VP (TO to) (VP (AUX be) (ADJP (JJ relevant)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NN religion)) (VP (MD should) (VP (AUX be) (VP (VBN excluded) (PP (IN from) (S (VP (VBG determining) (NP (NP (DT the) (NN cause)) (PP (IN of) (NP (JJ common) (NNS events))))))))))))) (. .)))

(S1 (S (ADVP (RB Further)) (, ,) (SBAR (IN if) (S (NP (NNP God)) (VP (AUX is) (ADJP (JJ willing) (S (VP (TO to) (VP (VB cause) (NP (NNS earthquakes)))))) (SBAR (IN because) (S (NP (NNS people)) (VP (VP (VBP accept) (NP (NN homosexuality))) (CC but) (VP (VBZ ignores) (NP (JJR greater) (NNS tragedies))))))))) (, ,) (RB then) (NP (NP (NNP God) (POS 's)) (JJ ultimate) (NN benevolence)) (VP (AUX is) (PP (IN up) (PP (IN for) (NP (NN question))))) (. .)))

(S1 (S (NP (NP (DT All)) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (VP (VBN required)))))) (VP (AUX is) (SBAR (IN that) (S (NP (JJ enough) (NNS people)) (VP (VBP want) (S (VP (TO to) (VP (VB accept) (NP (NP (NN homosexuality)) (PP (IN for) (NP (NP (JJ such) (NN acceptance)) (SBAR (S (VP (TO to) (VP (AUX be) (ADJP (RB socially) (JJ correct)))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (ADVP (RB purposefully)) (VP (VBG misunderstanding) (NP (NP (NNP S2) (POS 's)) (NNS statements)))))) (CC and) (SBAR (IN that) (S (NP (NP (NNP God) (POS 's)) (NNS motivations)) (VP (AUX are) (ADJP (RB ultimately) (JJ unknowable))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG claiming) (S (VP (TO to) (VP (VB know) (NP (NP (DT the) (NN nature)) (PP (IN of) (NP (NP (NN homosexuality)) (CC and) (NP (NNP God)) (ADVP (RB alike))))) (PP (IN without) (S (VP (AUXG being) (NP (NP (DT an) (NN expert)) (PP (IN on) (NP (DT the) (NN situation))))))))))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (ADVP (RB then)) (VP (VBZ asks) (SBAR (WHADVP (WRB why)) (S (NP (NNS gays)) (VP (MD should) (VP (AUX be) (VP (VBN considered) (S (ADJP (JJ acceptable)))))))))) (CC but) (S (NP (NNS polygamists)) (VP (AUX are) (RB not))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ admits) (SBAR (S (NP (PRP he)) (VP (VBZ sees) (NP (NP (DT no) (NNS problems)) (PP (IN with) (NP (NP (NN polygamy)) (VP (AUXG being) (VP (VBN allowed) (ADVP (RB either))))))))))) (. .)))

