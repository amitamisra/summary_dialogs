<T>
<P>
</P>
<P>
<S>
<C>S1/NNS :/: The/DT court/NN upholds/VBZ equal/JJ protections/NNS ,/, but/CC not/RB for/IN gays/NNS in/IN federal/JJ and/CC state/NN laws/NNS ./. </C>
</S>
<S>
<C>Equal/JJ protection/NN under/IN the/DT law/NN is/AUX for/IN all/DT ./. </C>
</S>
<S>
<C>There/EX is/AUX no/DT evidence/NN in/IN the/DT constitution/NN that/IN equal/JJ protection/NN does/AUX not/RB extend/VB to/TO homosexuals/NNS ./. </C>
</S>
<S>
<C>For/IN gays/NNS ,/, not/RB being/AUXG able/JJ to/TO marry/VB is/AUX an/DT obstacle/NN to/TO their/PRP$ pursuit/NN of/IN life/NN ,/, liberty/NN and/CC happiness/NN ./. </C>
</S>
<S>
<C>Gay/JJ partnerships/NNS are/AUX not/RB protected/VBN in/IN the/DT same/JJ way/NN that/IN heterosexual/JJ marriages/NNS are/AUX ./. </C>
</S>
<S>
<C>Denying/VBG gay/JJ marriage/NN means/VBZ denying/VBG an/DT equal/JJ right/NN ./. </C>
</S>
<S>
<C>S2/NNP :/: Marriage/NN is/AUX restricted/VBN by/IN age/NN ,/, sex/NN ,/, species/NNS ,/, number/NN ,/, capacity/NN etc/NN ./. </C>
</S>
<S>
<C>If/IN equal/JJ protection/NN for/IN gay/JJ marriage/NN is/AUX desired/VBN ,/, </C>
<C>than/IN these/DT restrictions/NNS would/MD have/AUX to/TO be/AUX removed/VBN ./. </C>
</S>
<S>
<C>The/DT constitution/NN would/MD have/AUX to/TO be/AUX changed/VBN ./. </C>
</S>
<S>
<C>Gay/JJ marriage/NN is/AUX not/RB a/DT part/NN of/IN the/DT term/NN ``/`` equal/JJ protection/NN ''/'' in/IN the/DT constitution/NN ./. </C>
</S>
<S>
<C>The/DT original/JJ understanding/NN of/IN the/DT 14th/JJ amendment/NN does/AUX not/RB apply/VB to/TO gays/NNS </C>
<C>unless/IN they/PRP are/AUX denied/VBN marriage/NN 
<M>-LRB-/-LRB- as/IN it/PRP is/AUX defined/VBN today/NN -RRB-/-RRB- </M>
for/IN being/AUXG gay/JJ ./. </C>
</S>
</P>
</T>
