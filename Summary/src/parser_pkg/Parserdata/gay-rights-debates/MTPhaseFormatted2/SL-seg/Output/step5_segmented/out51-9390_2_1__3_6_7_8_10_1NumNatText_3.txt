<T>
<P>
</P>
<P>
<S>
<C>Two/CD subjects/NNS are/AUX discussing/VBG discrimination/NN against/IN homosexuals/NNS in/IN the/DT workplace/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ there/EX is/AUX no/DT comparison/NN between/IN racial/JJ discrimination/NN and/CC disapproval/NN of/IN homosexual/JJ relationships/NNS ./. </C>
</S>
<S>
<C>S2/NNP advises/VBZ it/PRP is/AUX currently/RB legal/JJ to/TO fire/VB someone/NN simply/RB for/IN being/AUXG gay/JJ rather/RB than/IN their/PRP$ ability/NN to/TO complete/VB the/DT job/NN correctly/RB and/CC feels/VBZ this/DT is/AUX discrimination/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ this/DT is/AUX incorrect/JJ as/IN discrimination/NN is/AUX defined/VBN as/IN a/DT hatred/NN or/CC fear/NN of/IN another/DT person/NN or/CC group/NN </C>
<C>while/IN disapproval/NN of/IN gay/JJ relationships/NNS is/AUX due/JJ to/TO not/RB agreeing/VBG with/IN another/DT 's/AUX behavior/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB feel/VB being/AUXG gay/JJ can/MD be/AUX described/VBN as/IN a/DT behavior/NN ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ there/EX is/AUX no/DT issue/NN with/IN this/DT practice/NN ./. </C>
</S>
<S>
<C>S2/NNP wonders/VBZ what/WDT behavior/NN can/MD be/AUX disapproved/VBN of/IN if/IN the/DT person/NN does/AUX his/her/JJR job/NN correctly/RB ./. </C>
</S>
</P>
</T>
