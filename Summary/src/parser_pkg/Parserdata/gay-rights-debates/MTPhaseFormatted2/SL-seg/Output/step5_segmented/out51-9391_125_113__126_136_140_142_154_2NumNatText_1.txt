<T>
<P>
</P>
<P>
<S>
<C>S1/NNP asserts/VBZ that/IN any/DT sexual/JJ activity/NN between/IN two/CD men/NNS is/AUX deviant/JJ behavior/NN </C>
<C>and/CC that/IN it/PRP is/AUX not/RB generally/RB accepted/VBN as/IN appropriate/JJ ./. </C>
</S>
<S>
<C>He/PRP complains/VBZ that/IN homosexuals/NNS respond/VB to/TO criticism/NN </C>
<C>by/IN implying/VBG that/IN their/PRP$ opponent/NN is/AUX a/DT closet/NN homosexual/JJ ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN if/IN homosexual/JJ males/NNS were/AUX allowed/VBN to/TO adopt/VB children/NNS ,/, </C>
<C>they/PRP would/MD sodomize/VB them/PRP ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN most/JJS people/NNS support/VBP equal/JJ rights/NNS for/IN homosexuals/NNS and/CC believe/VBP homosexuality/NN is/AUX acceptable/JJ ./. </C>
</S>
<S>
<C>She/PRP cites/VBZ several/JJ people/NNS she/PRP knows/VBZ to/TO demonstrate/VB that/IN homosexuals/NNS are/AUX not/RB bad/JJ people/NNS ,/, </C>
<C>and/CC claims/VBZ that/IN her/PRP$ son/NN 's/POS gay/JJ father/NN is/AUX a/DT better/JJR father/NN than/IN her/PRP$ own/JJ heterosexual/JJ father/NN ./. </C>
</S>
<S>
<C>S1/NNP claims/VBZ that/IN his/PRP$ approval/NN of/IN deviant/JJ sexual/JJ behavior/NN should/MD not/RB be/AUX based/VBN on/IN whether/IN homosexuals/NNS are/AUX nice/JJ ,/, </C>
<C>and/CC rejects/VBZ the/DT assertion/NN that/IN most/JJS people/NNS accept/VBP homosexuality/NN ./. </C>
</S>
</P>
</T>
