<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN Thomas/NNP Jefferson/NNP did/AUX not/RB recommend/VB the/DT negative/JJ behaviors/NNS that/WDT are/AUX targeted/VBN toward/IN homosexuals/NNS ,/, </C>
<C>and/CC that/IN although/IN homosexuality/NN is/AUX a/DT sin/NN according/VBG to/TO Christianity/NNP and/CC may/MD have/AUX been/AUX illegal/JJ in/IN the/DT past/NN ,/, </C>
<C>it/PRP is/AUX not/RB anymore/RB ./. </C>
</S>
<S>
<C>He/PRP points/VBZ out/RP that/IN America/NNP is/AUX not/RB a/DT theocracy/NN </C>
<C>and/CC that/IN Christians/NNS are/AUX not/RB persecuted/VBN ,/, </C>
<C>citing/VBG the/DT fact/NN that/IN they/PRP are/AUX free/JJ to/TO practice/VB religion/NN as/IN they/PRP choose/VBP ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN Thomas/NNP Jefferson/NNP was/AUX correct/JJ in/IN labeling/VBG homosexuality/NN as/IN a/DT sin/NN and/CC a/DT crime/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN there/EX should/MD be/AUX heavy/JJ monetary/JJ fines/NNS for/IN engaging/VBG in/RP or/CC advocating/VBG homosexual/JJ behavior/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN liberal/JJ tolerance/NN is/AUX just/RB as/RB dogmatic/JJ as/IN Christianity/NNP ,/, </C>
<C>and/CC that/IN it/PRP is/AUX no/RB more/RBR right/JJ to/TO base/VB laws/NNS on/IN this/DT perspective/NN over/IN any/DT another/DT ./. </C>
</S>
</P>
</T>
