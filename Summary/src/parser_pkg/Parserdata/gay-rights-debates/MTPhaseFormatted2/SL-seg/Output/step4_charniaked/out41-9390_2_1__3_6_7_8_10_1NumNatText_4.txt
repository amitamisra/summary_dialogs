(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NP (DT the) (NN difference)) (PP (IN between) (NP (NP (NN homosexuality)) (CC and) (NP (JJ racial) (NN discrimination))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (CD one)) (VP (AUX is) (NP (NP (DT a) (NN hatred)) (CC or) (NP (NP (NN fear)) (PP (IN of) (NP (DT another) (JJ human) (NN being))))) (, ,) (SBAR (IN while) (S (NP (DT the) (JJ other)) (VP (AUX is) (NP (NP (DT a) (NN disapproval)) (PP (IN of) (NP (NP (NN someone) (POS 's)) (NN behavior))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (JJ homosexual)) (VP (AUX was) (VP (VBN fired))))) (, ,) (NP (PRP it)) (VP (MD would) (VP (AUX be) (PP (PP (IN for) (NP (NN incompetence))) (CONJP (RB rather) (IN than)) (PP (IN for) (S (VP (AUXG being) (ADJP (JJ gay))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (RB perfectly) (JJ legal) (PP (IN in) (NP (NP (JJS most) (NNS places)) (PP (IN in) (NP (DT the) (NNP U.S.)))))) (S (VP (TO to) (VP (VB fire) (NP (NN someone)) (PP (PP (ADVP (RB simply)) (IN for) (S (VP (AUXG being) (ADJP (JJ gay))))) (, ,) (RB not) (PP (IN for) (NP (NN incompetence)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (ADVP (RB currently)) (VP (VBG sitting) (PP (IN in) (S (NP (PRP$ his) (NNS pajamas)) (VP (AUXG being) (ADJP (JJ gay)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ questions) (NP (WP what) (NN behavior)) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (VP (VBG exhibiting) (NP (DT that) (NNS warrants) (NN disapproval))))))) (. .)))

