(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (UCP (NN state) (CC and) (JJ federal)) (NNS laws)) (PP (IN on) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (JJ homosexual) (NN marriage)))))) (VP (AUX are) (ADVP (RB specifically)) (VP (VBN directed) (PP (IN against) (NP (NNS homosexuals)))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (SBAR (IN that) (S (NP (DT the) (NNS courts)) (VP (AUX are) (VP (VBG upholding) (NP (JJ equal) (NNS protections)))))) (CC and) (SBAR (IN that) (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP oppose) (NP (DT the) (NNS rulings)))))) (VP (AUX do) (ADVP (RB so)) (PP (IN because) (IN of) (NP (NN prejudice)))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VP (VBP argue) (SBAR (IN that) (S (NP (NP (JJ equal) (NN protection)) (PP (IN under) (NP (DT the) (NN law)))) (VP (AUX is) (NP (DT a) (JJ global) (NN blanket) (NN effect)))))) (CC and) (VP (VBP cannot) (VP (AUX be) (VP (VBN argued) (S (VP (TO to) (VP (VB exclude) (NP (JJ certain) (NNS groups))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (VP (VBG barring) (NP (NNS homosexuals)) (PP (IN from) (S (VP (VBG marrying) (SBAR (S (NP (DT each) (JJ other)) (VP (VBZ serves) (PP (IN as) (NP (NP (DT an) (NN obstacle)) (PP (IN in) (NP (NP (NP (DT a) (JJ homosexual) (POS 's)) (NN pursuit)) (PP (IN of) (NP (NN life) (, ,) (NN liberty) (CC and) (NN happiness))))))) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (VP (VBN forbidden) (S (VP (TO to) (VP (VB marry) (NP (NP (DT the) (NN person)) (SBAR (WHNP (IN that)) (S (NP (PRP they)) (VP (VBP love)))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ illegitimate) (S (VP (TO to) (VP (VB argue) (SBAR (IN that) (S (NP (DT the) (NN constitution)) (VP (VBZ means) (NP (NP (CD one) (NN thing)) (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (AUX was) (ADVP (RB never)) (VP (VBN meant) (PP (TO to)) (PP (IN in) (NP (DT the) (JJ first) (NN place))))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (SBAR (IN in) (NN order) (S (VP (TO to) (VP (VB get) (NP (JJ equal) (NN protection)))))) (, ,) (NP (DT that) (NN one)) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB change) (NP (DT the) (NN constitution) (S (VP (TO to) (VP (VB mean) (NP (NP (NN something)) (ADJP (RB completely) (JJ different))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (JJ homosexual) (NN marriage)) (VP (AUX was) (PP (IN in) (NP (NP (DT any) (NN part)) (PP (IN of) (NP (NP (DT the) (JJ original) (NN meaning)) (PP (IN of) (NP (JJ equal) (NN protection)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (NP (DT the) (JJ 14th) (NN amendment)) (VP (MD would) (RB not) (VP (VB apply) (PP (TO to) (NP (NNS homosexuals))) (SBAR (IN unless) (S (NP (PRP they)) (VP (AUX were) (ADVP (RB originally)) (VP (VBN denied) (NP (NN marriage)) (SBAR (IN as) (S (VP (VBN defined) (NP (NN today)))))))))))))) (. .)))

