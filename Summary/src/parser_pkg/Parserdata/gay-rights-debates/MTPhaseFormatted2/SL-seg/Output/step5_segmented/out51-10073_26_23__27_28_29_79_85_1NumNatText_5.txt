<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB the/DT argument/NN has/AUX to/TO do/AUX with/IN marriage/NN equality/NN ,/, </C>
<C>and/CC has/AUX everything/NN to/TO do/AUX with/IN redefining/VBG the/DT very/JJ meaning/NN of/IN marriage/NN </C>
<C>to/TO allow/VB homosexuals/NNS to/TO marry/VB ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB that/IN equality/NN and/CC discrimination/NN are/AUX valid/JJ opinions/NNS in/IN support/NN of/IN homosexual/JJ marriage/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN marriage/NN was/AUX instituted/VBN for/IN the/DT purpose/NN of/IN a/DT stable/JJ nuclear/JJ family/NN to/TO procreate/VB </C>
<C>and/CC that/IN homosexuals/NNS have/AUX no/DT place/NN in/IN such/PDT an/DT institution/NN ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB homosexuals/NNS to/TO be/AUX normal/JJ ,/, </C>
<C>and/CC believe/VB that/IN heterosexuality/NN is/AUX normal/RB ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN it/PRP is/AUX ,/, in/IN fact/NN ,/, discrimination/NN ,/, </C>
<C>because/IN they/PRP cannot/VBP marry/VB the/DT person/NN of/IN their/PRP$ choice/NN ,/, but/CC a/DT heterosexual/JJ person/NN can/MD ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB believe/VB that/IN this/DT is/AUX equality/NN ,/, </C>
<C>and/CC that/IN it/PRP is/AUX not/RB fair/JJ to/TO them/PRP ./. </C>
</S>
</P>
</T>
