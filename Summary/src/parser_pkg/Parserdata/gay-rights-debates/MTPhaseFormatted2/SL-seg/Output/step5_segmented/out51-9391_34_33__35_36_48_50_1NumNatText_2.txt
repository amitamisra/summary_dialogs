<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP is/AUX in/IN favor/NN of/IN same/JJ sex/NN marriage/NN </C>
<C>while/IN S2/NNP opposes/VBZ ./. </C>
</S>
<S>
<C>S2/NNP seems/VBZ to/TO believe/VB that/IN the/DT gay/JJ community/NN is/AUX singularly/RB responsible/JJ for/IN the/DT spread/NN of/IN HIV/NNP </C>
<C>and/CC for/IN that/DT reason/NN should/MD not/RB be/AUX permitted/VBN to/TO marry/VB ./. </C>
</S>
<S>
<C>S1/NNP feels/VBZ this/DT is/AUX a/DT way/NN of/IN saying/VBG homosexuality/NN is/AUX a/DT disease/NN </C>
<C>and/CC S1/NNP does/AUX not/RB feel/VB that/IN is/AUX the/DT case/NN ./. </C>
</S>
<S>
<C>S1/NNP wants/VBZ scientific/JJ proof/NN of/IN the/DT information/NN S2/NNP offers/NNS and/CC does/AUX not/RB feel/VB Cameron/NNP is/AUX a/DT viable/JJ source/NN ./. </C>
</S>
<S>
<C>S2/NNP adheres/VBZ to/TO the/DT opinion/NN that/IN allowing/VBG homosexuals/NNS to/TO wed/VBN would/MD effect/VB the/DT rest/NN of/IN society/NN financially/RB ./. </C>
</S>
<S>
<C>S2/NNP accuses/VBZ S1/NNP of/IN chickening/VBG out/RP </C>
<C>because/IN he/she/NN does/AUX not/RB want/VB to/TO answer/VB the/DT hypothetical/JJ question/NN posed/VBN about/IN HIV/NNP ./. </C>
</S>
</P>
</T>
