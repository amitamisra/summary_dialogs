<s> <PARAGRAPH> </s>
<s>  Two people are discussing laws and politics. </s>
<s> S2 states that homosexual sex is a sin and a crime, and there should be a huge fine on the behavior. </s>
<s> S1 retorts that while Christianity states its a sin, it is not a crime anymore. </s>
<s> He states that the US is not a theocracy, and the freedom available in the US means that the responsibility of appeasing a Christian God cannot be forced on the population. </s>
<s> S2 retorts that liberal tolerance is contradictory, dogmatic, intolerant and coercive as it only allows for one correct view. </s>
<s> To contradict, S1 states that the "liberal" tolerance still allows for the free practice of religion. </s>
<s>  </s>
