(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (NNS issues)) (VP (VBG involving) (NP (JJ legislative) (NNS movements)))) (VP (AUX are) (VP (VBN decided) (PP (IN by) (NP (NN society)))))))) (. .)))

(S1 (S (SBAR (IN Though) (S (NP (DT the) (NN legislature)) (VP (VBZ votes) (PP (IN on) (NP (JJ national) (NNS issues)))))) (, ,) (NP (NP (DT the) (NN power)) (PP (IN of) (NP (DT the) (NNS citizens)))) (VP (AUX is) (SBAR (WHNP (WP what)) (S (VP (VP (VBZ grants)) (CC or) (VP (VBZ denies) (NP (JJ civil) (NNS rights)) (NP (NN legislation))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ feels) (SBAR (S (NP (DT the) (JJ hot) (NN issue)) (ADVP (RB right) (RB now)) (VP (VBZ involves) (NP (JJ gay) (NN marriage))))))) (, ,) (CC and) (S (NP (NP (DT the) (NN outcome)) (PP (IN of) (NP (DT that) (NN issue)))) (VP (MD will) (VP (AUX be) (NP (DT a) (NN voting) (NN matter))))) (. .)))

(S1 (S (PP (VBG According) (PP (TO to) (NP (PRP him)))) (, ,) (S (NP (NN society)) (VP (VBZ determines) (NP (PRP$ its) (JJ own) (NNS standards)))) (CC and) (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ possible) (S (VP (TO to) (VP (VB make) (NP (JJ gay) (NN marriage) (JJ civil) (NNS rights)) (ADVP (NN matter)))))))) (. .)))

(S1 (S (S (NP (JJ Legal) (NN marriage) (NN cannot)) (ADVP (RB just)) (VP (AUX be) (VP (VBN handed) (PRT (RP out)) (PP (TO to) (NP (NP (DT any) (NN group)) (SBAR (WHNP (WDT that)) (S (VP (VBZ calls) (PP (IN for) (NP (PRP it))))))))))) (: ;) (S (NP (NN society)) (VP (VP (VBZ determines) (NP (DT the) (NNS facts))) (CC and) (VP (VBZ acts) (ADVP (RB reasonably) (CC and) (RB accordingly)) (PP (TO to) (NP (NP (DT the) (NNS facts)) (PP (VBN given) (PP (IN about) (NP (DT the) (NN issue))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (IN that) (S (NP (JJ civil) (NNS rights) (NNS issues)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN left) (PP (IN up) (PP (TO to) (NP (NN society)))) (, ,) (SBAR (ADVP (RB especially)) (IN if) (S (NP (DT that) (NN society)) (VP (AUX is) (NP (NP (DT the) (JJ main) (NN deterrent)) (PP (IN in) (S (VP (VBG approving) (NP (NP (DT a) (NN group) (POS 's)) (NNS rights))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (MD should) (VP (AUX be) (NP (NP (DT the) (NN force)) (PP (IN behind) (S (VP (VBG protecting) (NP (NP (NNS citizens) (POS ')) (NNS rights)) (PP (IN against) (NP (NP (DT the) (NN hatred) (CC and) (NN bigotry)) (PP (IN of) (NP (DT the) (NNS masses)))))))))))))) (. .)))

(S1 (S (NP (DT The) (NN government)) (VP (VP (AUX has) (VP (VBN involved) (NP (PRP itself)) (PP (IN in) (NP (JJ civil) (NNS rights) (NNS issues))) (PP (IN in) (NP (DT the) (NN past))))) (, ,) (CC and) (VP (MD should) (VP (VB step) (PRT (RP up)) (S (VP (TO to) (VP (VB defend) (NP (JJ gay) (NNS rights))))) (SBAR (WHADVP (WRB when)) (S (NP (PRP it)) (VP (VBZ comes) (PP (TO to) (NP (JJ legal) (NN marriage))))))))) (. .)))

