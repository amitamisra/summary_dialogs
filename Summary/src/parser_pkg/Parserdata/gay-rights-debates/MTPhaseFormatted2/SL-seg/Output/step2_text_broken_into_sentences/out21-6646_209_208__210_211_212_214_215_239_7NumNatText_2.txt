<PARAGRAPH> S1 and S2 are debating gay marriage.
S1 argues that gay marriage should be decided by a public vote.
S2 argues that it's cruel to limit someone else from enjoying rights you can currently enjoy.
Opponents of gay marriage should try to have more empathy for gay people who wish to marry.
S1 retorts that it is not cruel for society to bar someone from enjoying a particular right.
Society can and does decide what it deems to be socially acceptable through consensus.
S2 states that tradition or being mundane does not make something any more wrong or right.
S2 compares S1's argument to a justification for slavery.
The majority once held slavery to be correct, hence it was morally and socially acceptable according to S1's logic.
S1 says that he is not arguing for something being correct because it's traditional, only that it can be changed through convincing society to change.
It is up to society to determine what is permissible through a dialog between the proponents of gay marriage and society.
