(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NNS posts) (NN he/she)) (VP (AUX has) (VP (VBN read) (SBAR (S (VP (AUX are) (NP (JJ bigoted) (NNS statements)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB also)) (VP (VBZ asks) (SBAR (IN that) (S (NP (DT the) (JJ Bible)) (VP (AUX be) (VP (VBN stepped) (PRT (RP out)) (PP (IN of)) (S (VP (TO to) (VP (VB see) (SBAR (WHADVP (WRB how)) (S (NP (DT these) (NNS statements)) (VP (AUX are) (VP (VBG treating) (NP (NNS people)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NNS statements)) (VP (VP (AUX are) (RB not) (VP (VBG looking) (PRT (RP out)) (PP (IN for) (NP (NP (DT the) (JJS best) (NNS interests)) (PP (IN of) (NP (JJ gay) (NNS people))))))) (CC but) (VP (ADVP (RB actually)) (JJ degrading) (NP (PRP them))))))) (. .)))

(S1 (S (NP (NNP He/she)) (VP (VBZ advises) (SBAR (S (NP (NNS people)) (VP (MD should) (RB not) (VP (VB trust) (NP (NP (DT the) (JJ Bible)) (PP (IN as) (NP (DT a) (JJ viable) (NN source)))) (SBAR (IN because) (S (NP (PRP it)) (ADVP (RB also)) (VP (VBD advised) (PP (TO to) (NP (NP (NN stone) (NNS children)) (SBAR (WHNP (WP who)) (S (VP (AUX do) (RB not) (VP (VB behave))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (S (NP (NP (NN he/she)) (PRN (-LRB- -LRB-) (PP (IN along) (PP (IN with) (NP (NNS others)))) (-RRB- -RRB-))) (VP (AUX are) (VP (VBG speaking) (PP (VBN based) (PP (IN on) (NP (PRP$ their) (JJ religious) (NNS beliefs))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (NP (DT the) (JJ gay) (NN community)) (CONJP (RB as) (RB well) (IN as)) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP support) (NP (PRP them))))))) (VP (MD should) (VP (VP (VB read) (NP (DT the) (JJ Bible))) (, ,) (VP (VB pray)) (, ,) (CC and) (VP (VB get) (VP (VBN saved) (S (VP (TO to) (VP (VB learn) (ADJP (JJ right)) (PP (IN from) (S (NP (JJ wrong)) (ADVP (RB also)) (VP (VBG stating) (SBAR (S (NP (JJ religious) (NNS beliefs)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN given) (PRT (RP up)) (PP (IN for) (NP (JJ political) (NN correctness))))))))))))))))))))) (. .)))

