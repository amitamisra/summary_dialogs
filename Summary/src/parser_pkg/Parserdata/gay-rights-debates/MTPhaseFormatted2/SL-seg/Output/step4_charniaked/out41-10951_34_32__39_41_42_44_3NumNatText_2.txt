(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBD attacked) (NP (NP (NP (NNP S1) (POS 's)) (NN lack)) (PP (IN of) (NP (NP (NN belief)) (PP (IN in) (NP (DT the) (NN resurrection))))) (PP (IN of) (NP (NNP Jesus))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (IN that) (S (PP (IN until) (NP (NNP S2))) (VP (VBZ presents) (NP (NP (JJ credible) (NN scholarship)) (CC and) (NP (NP (JJ good) (NNS arguments)) (SBAR (WHNP (WDT that)) (S (VP (VBP burst) (NP (NP (DT the) (NN belief)) (PP (IN in) (NP (NP (DT the) (NN resurrection)) (PP (IN of) (NP (NNP Jesus)))))) (SBAR (S (NP (PRP he)) (VP (VP (AUX is) (ADJP (ADJP (NN nothing) (JJR more)) (PP (IN than) (NP (DT a) (NNP Christ) (NN denier))))) (CC and) (VP (MD might) (ADVP (RB as) (RB well)) (VP (VB go) (PP (IN into) (NP (DT the) (NN Pit))))))))))))))))) (. .)))

(S1 (S (CC And) (PP (IN of) (NP (NN course) (SBAR (IN since) (S (NP (PRP he)) (VP (VP (AUX is) (ADJP (JJ gay))) (CC and) (VP (AUX is) (NP (DT a) (NN Sodomite)))))))) (NP (PRP he)) (VP (MD would) (RB not) (VP (VB go) (PP (TO to) (NP (NN heaven))) (SBAR (IN unless) (S (NP (PRP he)) (VP (VBD repented)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBD called) (S (NP (NP (NNP S1) (POS 's)) (NN reply)) (ADJP (JJ lame)))) (CC and) (VP (ADVP (RB then)) (VBD proceeded) (S (VP (TO to) (VP (VB argue) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (AUX were) (NP (NP (NP (DT no) (NN eyewitness) (NNS accounts)) (ADJP (RB only) (JJ hearsay))) (, ,) (NP (DT no) (NN evidence)) (, ,) (FW etc.))))) (CC and) (SBAR (IN that) (S (NP (NNP S1)) (VP (MD should) (VP (AUX be) (NP (NP (DT the) (NN one)) (SBAR (S (VP (TO to) (VP (VB come) (PRT (RP up)) (PP (IN with) (NP (NN proof)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (PP (TO to) (NP (NP (NNP S1)) (, ,) (NP (NNP Ezridden)) (, ,))) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT the) (NN pit)) (ADVP (RB really)) (VP (VBD existed)))) (ADVP (RB then)) (NP (NNP Ezridden)) (VP (MD would) (VP (AUX be) (ADVP (RB there)) (PP (ADVP (RB too)) (IN because) (IN of) (SBAR (WHADVP (WRB how)) (S (NP (PRP he)) (VP (VBZ treats) (NP (JJ other) (NNS people))))))))))) (. .)))

