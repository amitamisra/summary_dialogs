(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (ADVP (RB S1)) (: :) (NP (NNP Heterosexual) (NN marriage)) (VP (AUX has) (VP (VBN existed) (PP (IN in) (NP (DT all) (NNS cultures))) (PP (IN for) (NP (DT a) (RB very) (JJ long) (NN time))))) (. .)))

(S1 (SBARQ (WHADVP (WRB How)) (SQ (MD can) (NP (PRP one)) (VP (VB justify) (S (VP (VBG revolutionizing) (NP (DT an) (JJ ancient) (NN institution)))))) (. ?)))

(S1 (S (S (NP (DT Some) (NNS homosexuals)) (VP (VBP want) (S (VP (TO to) (VP (VB marry) (PP (IN for) (NP (NN love) (CC or) (JJ legal) (NN status)))))))) (, ,) (CC but) (S (NP (NP (JJ many)) (PP (IN of) (NP (PRP them)))) (VP (VBP want) (S (VP (TO to) (VP (AUX do) (NP (PRP it)) (S (VP (TO to) (VP (VB prove) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (ADVP (RB just)) (ADJP (ADJP (RB as) (JJ good)) (PP (IN as) (NP (NNS heterosexuals))))))))))))))) (. .)))

(S1 (S (NP (NNS Homosexuals)) (VP (VP (VBP want) (SBAR (WHNP (WP what)) (S (NP (NNS heterosexuals)) (VP (AUX have))))) (, ,) (CC and) (VP (VBP want) (S (VP (TO to) (VP (VB make) (S (NP (DT the) (NN mainstream)) (VP (VB accept) (NP (JJ gay) (NN culture))))))))) (. .)))

(S1 (S (NP (JJ Gay) (NN marriage)) (VP (AUX is) (RB not) (NP (NP (DT a) (JJ good) (JJ enough) (NN justification)) (PP (IN for) (S (VP (VBG revolutionizing) (NP (NP (DT the) (NN institution)) (PP (IN of) (NP (NN marriage))))))))) (. .)))

(S1 (S (NP (NN Homosexuality)) (VP (AUX is) (NP (NP (DT an) (NN anomaly)) (PP (IN among) (NP (NN man)))) (, ,) (VP (AUX is) (RB not) (ADJP (JJ equal) (PP (IN with) (NP (NN heterosexuality))))) (, ,) (CC and) (VP (AUX does) (RB not) (VP (VB deserve) (NP (NN marriage))))) (. .)))

(S1 (S (S (VP (VB S2))) (: :) (S (NP (NNPS Gays)) (VP (VBP want) (S (VP (TO to) (VP (VB marry) (SBAR (RB so) (IN that) (S (NP (PRP$ their) (NNS families)) (VP (MD can) (VP (AUX have) (NP (NP (DT the) (NN strength)) (CC and) (NP (NP (NN protection)) (SBAR (WHNP (IN that)) (S (NP (NP (DT the) (NN institution)) (PP (IN of) (NP (NN marriage)))) (VP (VBZ provides))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX have) (NP (NP (DT no) (NN interest)) (PP (IN in) (S (VP (VBG harming) (NP (NP (DT the) (NN tradition)) (PP (IN of) (NP (JJ heterosexual) (NN marriage))))))))) (. .)))

(S1 (S (NP (NNS Gays)) (VP (VBP want) (S (VP (TO to) (VP (VB marry) (ADVP (ADVP (RB as) (RB badly)) (SBAR (IN as) (S (NP (NNS heterosexuals)) (VP (VBP want) (S (RB not) (VP (TO to) (VP (AUX be) (VP (VBN married))))))))))))) (. .)))

(S1 (S (NP (NN Living) (NNS laws)) (VP (AUX are) (VP (VBN meant) (S (VP (TO to) (VP (VB evolve) (PP (IN with) (NP (NN society)))))))) (. .)))

(S1 (S (NP (NNS Homosexuals)) (VP (AUX are) (ADVP (RB just)) (ADJP (ADJP (RB as) (JJ good)) (PP (IN as) (NP (NNS heterosexuals))))) (. .)))

(S1 (S (S (NP (JJ Other) (NNS societies)) (VP (AUX have) (NP (JJ legal) (JJ gay) (NN marriage)))) (CC and) (S (NP (DT no) (NNS revolutions)) (VP (AUX have) (VP (VBN occurred)))) (. .)))

(S1 (S (SBAR (RB Just) (IN because) (S (NP (NN something)) (VP (AUX is) (ADJP (JJ traditional))))) (VP (AUX does) (RB not) (VP (VB make) (NP (PRP it)) (ADVP (RB just)))) (. .)))

