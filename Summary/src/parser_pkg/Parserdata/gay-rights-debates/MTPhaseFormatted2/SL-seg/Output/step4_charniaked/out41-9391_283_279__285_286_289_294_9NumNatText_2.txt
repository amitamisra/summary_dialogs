(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage) (CC and) (NNS relationships)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (ADJP (VBN opposed) (PP (TO to) (NP (DT the) (NN issue)))) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (PP (IN for) (NP (PRP it))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (NP (NN practice) (JJ gay) (NNS relationships)) (VP (VP (AUX are) (RB spiritually) (VP (VBG lacking))) (CC and) (VP (VBZ feels) (NP (NP (NP (NNP S2) (POS 's)) (NN sarcasm)) (PP (IN toward) (NP (DT the) (NN issue)))) (PP (IN at) (NP (NN hand)))))))) (VP (AUX is) (NP (NP (NN proof)) (PP (IN of) (NP (DT this)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (NNP S1)) (VP (AUX is) (ADJP (ADJP (RB too) (JJ literal) (CC and) (JJ obsessed)) (PP (IN with) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (NP (JJ gay) (NN sex)) (CONJP (RB rather) (IN than)) (NP (JJ gay) (NN marriage))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (WHNP (WP what)) (S (VP (VBZ happens) (PP (IN in) (NP (NP (DT the) (NN bedroom)) (SBAR (S (VP (AUX is) (UCP (JJ private) (CC and) (RB not) (NP (DT the) (NN issue)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ makes) (NP (NP (DT some) (JJ rude) (NNS remarks)) (PP (IN in) (NP (NP (NN regard)) (PP (TO to) (NP (NP (JJ gay) (NN sex)) (VP (VBG involving) (NP (NNS gerbils))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (S (NP (JJ many) (NNS heterosexuals)) (ADVP (RB also)) (VP (AUX have) (NP (NP (DT a) (NN history)) (PP (IN of) (S (VP (VBG doing) (NP (JJ vulgar) (NNS things)) (ADVP (RB as) (RB well)))))))) (CC and) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ unfair) (S (VP (TO to) (VP (VB categorize) (NP (JJ only) (JJ gay) (NNS individuals)) (PP (IN in) (NP (DT this) (NN manner)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB agree) (PP (IN with) (NP (DT this))))) (. .)))

