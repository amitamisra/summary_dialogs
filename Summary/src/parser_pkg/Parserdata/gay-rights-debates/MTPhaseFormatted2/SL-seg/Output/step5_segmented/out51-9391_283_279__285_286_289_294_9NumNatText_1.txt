<T>
<P>
</P>
<P>
<S>
<C>S1/NNP identifies/VBZ the/DT important/JJ spiritual/JJ situation/NN that/IN homosexuals/NNS are/AUX in/IN ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ that/IN it/PRP is/AUX inappropriate/JJ to/TO make/VB jokes/NNS about/IN homosexual/JJ lifestyles/NNP ,/, </C>
<C>that/IN no/DT one/NN approves/VBZ of/IN the/DT deviant/JJ behaviors/NNS practiced/VBN by/IN homosexuals/NNS ,/, </C>
<C>and/CC that/IN it/PRP would/MD not/RB be/AUX funny/JJ if/IN homosexuality/NN was/AUX discovered/VBN in/IN your/PRP$ own/JJ community/NN ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN sexual/JJ behaviors/NNS </C>
<C>because/IN we/PRP do/AUX not/RB identify/VB non-homosexuals/NNS by/IN the/DT sexual/JJ behaviors/NNS </C>
<C>they/PRP engage/VBP in/IN ,/, </C>
<C>we/PRP should/MD not/RB do/AUX so/RB for/IN homosexual/JJ individuals/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN a/DT person/NN 's/POS private/JJ behaviors/NNS are/AUX no/DT one/NN else/RB 's/AUX business/NN ,/, </C>
<C>and/CC that/IN a/DT sexual/JJ behaviors/NNS do/AUX not/RB define/VB who/WP someone/NN is/AUX as/IN a/DT person/NN ./. </C>
</S>
<S>
<C>He/PRP defends/VBZ homosexual/JJ behaviors/NNS </C>
<C>by/IN referencing/VBG the/DT deviant/JJ behaviors/NNS performed/VBN by/IN heterosexuals/NNS ,/, </C>
<C>suggesting/VBG that/IN deviant/JJ sexual/JJ acts/NNS are/AUX not/RB the/DT sole/JJ province/NN of/IN homosexuals/NNS ./. </C>
</S>
</P>
</T>
