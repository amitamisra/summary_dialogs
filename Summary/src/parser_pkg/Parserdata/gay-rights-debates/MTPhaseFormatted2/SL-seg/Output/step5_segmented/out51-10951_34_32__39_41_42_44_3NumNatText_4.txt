<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG Christianity/NNP ./. </C>
</S>
<S>
<C>S2/NNP contends/VBZ that/IN there/EX is/AUX no/DT eye/NN witness/NN accounts/NNS of/IN any/DT resurrection/NN ever/RB recorded/VBD ,/, </C>
<C>and/CC that/IN everything/NN in/IN the/DT Gospels/NNP is/AUX hearsay/JJ ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN not/RB only/RB is/AUX there/RB evidence/NN ,/, </C>
<C>but/CC scientifically/RB and/CC logically/RB no/DT one/NN can/MD be/AUX dead/JJ for/IN three/CD days/NNS and/CC get/VB up/RP and/CC walk/VB away/RB ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN the/DT burden/NN of/IN proof/NN instead/RB lies/VBZ with/IN those/DT who/WP believe/VBP in/IN the/DT resurrection/NN ,/, </C>
<C>and/CC that/IN based/VBN on/IN the/DT way/NN Christians/NNS life/NN their/PRP$ lives/NNS they/PRP would/MD ironically/RB end/VB up/RP in/IN the/DT pit/NN also/RB ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN it/PRP is/AUX all/DT unfounded/JJ conjecture/NN ,/, full/JJ of/IN left/JJ wing/NN ,/, antichrist/JJS dysentery/JJ ./. </C>
</S>
<S>
<C>He/PRP challenges/VBZ S2/NNP to/TO find/VB credible/JJ arguments/NNS that/IN bust/NN the/DT resurrection/NN of/IN Christ/NNP ./. </C>
</S>
</P>
</T>
