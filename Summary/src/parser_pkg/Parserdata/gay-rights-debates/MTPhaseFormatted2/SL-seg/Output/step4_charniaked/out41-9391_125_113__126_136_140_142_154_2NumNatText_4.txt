(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage) (NNS rights)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ contends) (SBAR (IN that) (S (NP (NP (DT any) (JJ sexual) (NN activity)) (PP (IN between) (NP (CD two) (NNS men)))) (VP (VP (AUX is) (ADJP (JJ deviant)) (NP (NN sex))) (, ,) (CC and) (VP (VBZ laughs) (PP (IN at) (SBAR (WHADVP (WRB how) (WRB when)) (S (NP (DT a) (JJ gay) (NN person)) (VP (VBZ loses) (NP (NP (DT an) (NN argument)) (SBAR (S (NP (PRP they)) (VP (VBP imply) (SBAR (S (NP (DT the) (NN objector)) (VP (AUX is) (ADJP (NP (DT a) (NN closet)) (JJ homosexual)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJS most) (NNS people)) (VP (AUX do) (RB not) (VP (VB trust) (NP (NNS gays))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (JJS most) (NNS people)) (VP (AUX do) (RB not) (VP (VB care) (PP (PP (IN about) (NP (NP (DT the) (NN sex) (NNS lives)) (PP (IN of) (NP (JJ other) (NNS adults))))) (, ,) (CC and) (PP (VBN based) (PP (IN on) (NP (NP (JJ current) (NNS states)) (SBAR (S (NP (JJS most) (NNS people)) (VP (AUX are) (PP (IN for) (NP (NP (JJ equal) (NNS rights)) (PP (IN for) (NP (NP (NNS gays)) (PP (IN save) (PP (IN for) (NP (NN marriage)))))))))))))))))))) (. .)))

(S1 (S (NP (PRP She)) (ADVP (RB also)) (VP (VP (VBZ lists) (NP (NP (JJ numerous) (NNS accounts)) (PP (IN of) (NP (NP (JJ good) (JJ gay) (NNS people)) (SBAR (S (NP (PRP she)) (VP (VBZ knows)))))) (, ,) (PP (PP (IN from) (NP (NP (NP (PRP$ her) (NN son) (POS 's)) (JJ adoptive) (NN father)) (, ,) (SBAR (WHNP (WP who)) (S (VP (AUX is) (NP (DT a) (JJ marvelous) (NN father))))) (, ,))) (PP (TO to) (NP (NP (PRP$ her) (NN aunt)) (CC and) (NP (ADJP (JJ lesbian) (JJS best)) (NN friend))))))) (, ,) (CC and) (VP (VBZ states) (SBAR (IN that) (S (NP (NNS people)) (VP (AUX do) (VP (VB trust) (NP (NNS gays)))))))) (. .)))

