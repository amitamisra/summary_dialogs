<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG the/DT sanctity/NN of/IN Christian/JJ marriage/NN ./. </C>
</S>
<S>
<C>S1/NNP expresses/VBZ his/PRP$ wish/NN that/IN Vitter/NNP and/CC Craig/NNP be/AUX stoned/VBN ./. </C>
</S>
<S>
<C>He/PRP then/RB states/VBZ a/DT tale/NN about/IN a/DT man/NN named/VBN Fred/NNP Thompson/NNP who/WP claims/VBZ to/TO be/AUX a/DT member/NN of/IN the/DT Churches/NNS of/IN Christ/NNP ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN Fred/NNP married/VBD his/PRP$ pregnant/JJ girlfriend/NN at/IN 18/CD and/CC were/AUX married/JJ for/IN 26/CD years/NNS ,/, </C>
<C>having/AUXG multiple/JJ children/NNS ,/, </C>
<C>until/IN they/PRP divorced/VBD </C>
<C>and/CC he/PRP married/VBD a/DT woman/NN who/WP is/AUX younger/JJR than/IN his/PRP$ oldest/JJS child/NN ./. </C>
</S>
<S>
<C>He/PRP then/RB states/VBZ that/IN James/NNP Dobson/NNP says/VBZ Fred/NNP is/AUX not/RB really/RB a/DT Christian/JJ ./. </C>
</S>
<S>
<C>S2/NNP questions/VBZ </C>
<C>if/IN Dobson/NNP thinks/VBZ the/DT man/NN is/AUX just/RB acting/VBG ,/, </C>
<C>to/TO which/WDT S1/NNP retorts/VBZ ,/, </C>
<C>asking/VBG if/IN Dobson/NNP thinks/VBZ period/NN ./. </C>
</S>
<S>
<C>S2/NNP states/VBZ maybe/RB about/IN himself/PRP ,/, but/CC on/IN issues/NNS its/PRP$ hard/JJ to/TO say/VB ./. </C>
</S>
</P>
</T>
