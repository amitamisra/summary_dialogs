<s> <PARAGRAPH> </s>
<s>  S1 believes that Republicans are pushing issues by appealing to people's ignorance and prejudice in order to distract from more important issues. </s>
<s> He or she believes that it is Republicans who are trying to make issues out of topics such as gay marriage, global warming and abortion. </s>
<s> They do not view judges as being manipulated, as they are doing their job, which is to interpret the law. </s>
<s> This person believes that conservatives are attempting to change the constitution so that they can impose their religious views and deny rights to others. </s>
<s> S2 argues that if the Democrats were not pushing radical agendas, then Republicans would not have to take action against them. </s>
<s> He or she believes that homosexuals were attempting to manipulate judges by imposing the Full Faith and Credit clause on other states, which they argue does not apply to licenses, which would include marriage licenses. </s>
<s> He or she argues that states are given the right to govern laws that are not specifically covered in the constitution, and that marriage licenses fall into the category of states' governance. </s>
<s>  </s>
