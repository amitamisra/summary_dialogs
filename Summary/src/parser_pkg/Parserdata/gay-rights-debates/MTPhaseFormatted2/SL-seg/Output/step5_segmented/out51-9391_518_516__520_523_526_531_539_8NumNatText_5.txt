<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN homosexual/JJ couples/NNS are/AUX not/RB eligible/JJ for/IN marriage/NN </C>
<C>because/IN there/EX are/AUX rules/NNS for/IN which/WDT relationships/NNS quality/JJ as/IN a/DT marriage/NN and/CC homosexual/JJ couples/NNS do/AUX not/RB fall/VB within/IN those/DT rules/NNS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN this/DT is/AUX the/DT same/JJ as/IN people/NNS being/AUXG prevented/VBN from/IN marrying/VBG someone/NN who/WP is/AUX already/RB married/JJ ,/, an/DT animal/NN ,/, a/DT minor/JJ ,/, or/CC a/DT sibling/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN this/DT is/AUX different/JJ from/IN the/DT previous/JJ rule/NN limiting/VBG marriage/NN to/TO intraracial/JJ couples/NNS ./. </C>
</S>
<S>
<C>He/PRP further/RB argues/VBZ that/IN heterosexual/JJ relationships/NNS are/AUX more/RBR valid/JJ at/IN the/DT evolutionary/JJ level/NN </C>
<C>because/IN they/PRP have/AUX a/DT reproductive/JJ purpose/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN gender/NN should/MD be/AUX removed/VBN as/IN a/DT criteria/NNS for/IN marriage/NN in/IN the/DT same/JJ way/NN that/IN race/NN was/AUX because/IN it/PRP is/AUX inappropriate/JJ and/CC discriminatory/JJ ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN it/PRP is/AUX unfair/JJ for/IN heterosexuals/NNS to/TO be/AUX eligible/JJ for/IN the/DT benefits/NNS of/IN marriage/NN such/JJ as/IN the/DT ability/NN to/TO make/VB important/JJ medical/JJ decisions/NNS ,/, to/TO cover/VB each/DT other/JJ under/IN their/PRP$ insurance/NN ,/, to/TO file/VB taxes/NNS jointly/RB ,/, to/TO purchase/VB property/NN together/RB ,/, to/TO visit/VB each/DT other/JJ in/IN the/DT hospital/NN ,/, or/CC to/TO be/AUX a/DT legal/JJ heir/NN ,/, </C>
<C>when/WRB those/DT same/JJ privileges/NNS are/AUX denied/VBN to/TO homosexuals/NNS ./. </C>
</S>
</P>
</T>
