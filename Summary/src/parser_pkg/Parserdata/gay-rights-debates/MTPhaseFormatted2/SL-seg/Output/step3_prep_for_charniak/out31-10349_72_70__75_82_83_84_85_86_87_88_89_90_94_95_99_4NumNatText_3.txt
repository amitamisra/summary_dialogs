<s> <PARAGRAPH> </s>
<s>  S1 sees marriage as it is today as part of a social contract that is based upon biology and reproduction. </s>
<s> The drive to reproduce is part of the heterosexual bond and marriage is its formalization. </s>
<s> S1 is annoyed with S2 demanding that because his marriage is not universally recognized that there is something unconstitutional in place. </s>
<s> He explains that marriage is left to the States to determine what is legal and that the Federal government, according to the Constitution, has no place to change that. </s>
<s> S2 would like to see a change in the definition of marriage. </s>
<s> He holds a marriage licence but it is not universally recognized. </s>
<s> He argues that biology and reproduction do not apply when people past the age where childbearing is possible can marry. </s>
<s> He challenges S1 that if marriage truly is about reproduction and family that a married couple must prove that this is a part of their relationship or their marriage would be terminated. </s>
<s> He is offended that others' beliefs can play a role in his life. </s>
<s>  </s>
