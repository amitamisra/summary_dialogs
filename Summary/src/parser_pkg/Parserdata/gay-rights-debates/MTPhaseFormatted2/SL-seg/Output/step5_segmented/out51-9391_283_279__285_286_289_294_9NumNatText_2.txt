<T>
<P>
</P>
<P>
<S>
<C>S1/NNP and/CC S2/NNP are/AUX discussing/VBG gay/JJ marriage/NN and/CC relationships/NNS ./. </C>
</S>
<S>
<C>S1/NNP is/AUX opposed/VBN to/TO the/DT issue/NN </C>
<C>while/IN S2/NNP is/AUX for/IN it/PRP ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ those/DT who/WP practice/NN gay/JJ relationships/NNS are/AUX spiritually/RB lacking/VBG and/CC feels/VBZ S2/NNP 's/POS sarcasm/NN toward/IN the/DT issue/NN at/IN hand/NN is/AUX proof/NN of/IN this/DT ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ S1/NNP is/AUX too/RB literal/JJ and/CC obsessed/JJ with/IN the/DT issue/NN of/IN gay/JJ sex/NN rather/RB than/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>S2/NNP advises/VBZ what/WP happens/VBZ in/IN the/DT bedroom/NN is/AUX private/JJ and/CC not/RB the/DT issue/NN ./. </C>
</S>
<S>
<C>S1/NNP makes/VBZ some/DT rude/JJ remarks/NNS in/IN regard/NN to/TO gay/JJ sex/NN involving/VBG gerbils/NNS ./. </C>
</S>
<S>
<C>S2/NNP feels/VBZ many/JJ heterosexuals/NNS also/RB have/AUX a/DT history/NN of/IN doing/VBG vulgar/JJ things/NNS as/RB well/RB </C>
<C>and/CC it/PRP is/AUX unfair/JJ to/TO categorize/VB only/JJ gay/JJ individuals/NNS in/IN this/DT manner/NN ./. </C>
</S>
<S>
<C>S1/NNP does/AUX not/RB agree/VB with/IN this/DT ./. </C>
</S>
</P>
</T>
