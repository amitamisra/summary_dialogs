<T>
<P>
</P>
<P>
<S>
<C>S2/NNP and/CC S1/NNP have/AUX had/AUX numerous/JJ previous/JJ discussions/NNS about/IN homosexuality/NN and/CC gay/JJ rights/NNS </C>
<C>and/CC S2/NNP is/AUX sarcastically/RB suggesting/VBG that/IN S1/NNP ,/, despite/IN his/PRP$ professed/VBN and/CC vocal/JJ conservative/JJ stance/NN ,/, is/AUX actually/RB a/DT closet/NN homosexual/JJ who/WP is/AUX hiding/VBG that/DT fact/NN from/IN his/PRP$ wife/NN of/IN 31/CD years/NNS based/VBN on/IN the/DT fact/NN that/IN S1/NNP purports/VBZ </C>
<C>to/TO know/VB so/RB much/RB about/IN what/WP gays/NNS would/MD think/VB ,/, </C>
<C>i.e.-/VBG he/PRP obtained/VBD that/DT information/NN from/IN his/PRP$ relations/NNS with/IN other/JJ gays/NNS </C>
<C>while/IN engaged/VBN in/IN secret/JJ homosexual/JJ relationships/NNS ./. </C>
</S>
<S>
<C>In/IN a/DT similar/JJ dig/NN ,/, S1/NNP suggests/VBZ that/IN S2/NNP is/AUX a/DT closet/NN homosexual/JJ </C>
<C>because/IN he/PRP purports/VBZ </C>
<C>not/RB to/TO know/VB about/IN the/DT gay/JJ community/NN 's/POS frequent/JJ use/NN of/IN highway/NN rest/NN stops/VBZ as/IN places/NNS to/TO hook/NN up/RP ./. </C>
</S>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX an/DT exercise/NN of/IN moral/JJ character/NN to/TO use/VB the/DT political/JJ process/NN </C>
<C>to/TO prevent/VB marriage/NN from/IN becoming/VBG bastardized/JJ ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN recognition/NN of/IN loving/VBG committed/JJ gay/JJ marriage/NN would/MD not/RB be/AUX bastardizing/VBG the/DT institution/NN of/IN marriage/NN ./. </C>
</S>
<S>
<C>The/DT two/CD feel/VBP they/PRP have/AUX similar/JJ understandings/NNS of/IN references/NNS to/TO Ted/NNP Haggard/NNP and/CC Sartre/NNP and/CC use/VBP the/DT term/NN gloryhole/NN with/IN a/DT meaning/NN that/WDT is/AUX not/RB entirely/RB clear/JJ from/IN this/DT dialog/NN ./. </C>
</S>
</P>
</T>
