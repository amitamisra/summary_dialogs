<s> <PARAGRAPH> </s>
<s>  S1 claims that republicans focus on "hot button" issues to take attention away from the important issues. </s>
<s> He claims that republicans have made issues out of gay rights and abortion by pushing legislation to ban them. </s>
<s> He also suggests that democrats are more fiscally responsible than republicans whose only interest is to provide tax breaks for the wealthy. </s>
<s> He claims that republicans are so concerned about certain minorities receiving their constitutional rights that they are willing to change the constitution to prevent it. </s>
<s> S2 claims that the republicans are focusing on important issues such as social security, taxes, and terrorism, and that it is democrats who are pushing unimportant issues like abortion, marriage equality, and global warming. </s>
<s> He suggests that if democrats had not been pushing such a radical agenda, then the republicans would not have had to come out against their position. </s>
<s> He also rejects the idea that the democrats are the more fiscally responsible party, claiming that the social engineering policies put in place by the democrats will prevent the budget from ever being balanced. </s>
<s>  </s>
