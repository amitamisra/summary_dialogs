(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NN marriage)) (VP (AUX has) (NP (NP (DT a) (ADJP (ADJP (JJ specific)) (CC and) (ADJP (RB deeply) (VBN embedded))) (NN definition)) (PP (IN within) (NP (NN culture)))))))) (. .)))

(S1 (S (NP (NN Marriage)) (VP (AUX is) (NP (DT a) (JJ formalized) (NN mating) (NN pair))) (. .)))

(S1 (S (NP (NP (NN Marriage) (POS 's)) (JJ legal) (NN definition)) (VP (ADVP (RB closely)) (VBZ mirrors) (NP (DT that) (NN definition))) (. .)))

(S1 (S (NP (JJ Homosexual) (NN marriage)) (VP (AUX has) (ADVP (RB never)) (VP (AUX been) (NP (NP (DT an) (ADJP (JJ explicit) (CC or) (JJ implicit)) (NN part)) (PP (IN of) (NP (DT those) (NNS definitions)))))) (. .)))

(S1 (S (VP (VBG Including) (SBAR (S (NP (PRP it)) (VP (MD would) (VP (VB make) (S (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (ADJP (RB inherently) (JJ self-contradictory)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (VP (VBG exercising) (NP (DT a) (JJ subjective) (NN definition))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ notes) (SBAR (IN that) (S (NP (NP (JJ different) (NNS members)) (PP (IN of) (NP (NN society)))) (VP (AUX have) (NP (NP (JJ different) (NNS definitions)) (PP (IN of) (NP (NN marriage)))))))) (. .)))

(S1 (S (ADVP (RB Further)) (, ,) (NP (EX there)) (VP (AUX are) (NP (NP (NNS instances)) (PP (IN of) (NP (NP (JJ accepted) (NN marriage)) (SBAR (WHNP (WDT which)) (S (VP (VBP violate) (NP (NP (NNP S1) (POS 's)) (VBN provided) (NNS definitions))))))))) (. .)))

(S1 (S (ADVP (RB Instead)) (PRN (, ,) (S (VP (VBP love))) (, ,)) (NP (DT the) (JJ emotional) (NN connection)) (VP (AUX is) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (RBS most) (JJ important)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ legal) (CC and) (JJ cultural) (NNS definitions)) (VP (AUX are) (RB not) (NP (DT the) (JJ same)))))) (. .)))

(S1 (S (NP (DT The) (JJ cultural) (NN definition)) (VP (AUX is) (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (JJ important)))))) (. .)))

(S1 (S (NP (NP (RBS Most) (JJ associate) (NN marriage)) (PP (IN with) (S (VP (VBG producing) (NP (NN offspring)))))) (, ,) (ADVP (RB hence)) (NP (PRP it)) (VP (AUX is) (VP (VBN deemed) (S (ADJP (JJ important))) (PP (IN in) (NP (DT the) (JJ cultural) (NN definition))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ questions) (NP (NP (NNP S1) (POS 's)) (NN authority) (S (VP (TO to) (VP (VB determine) (SBAR (SBAR (WHNP (WP what)) (S (VP (AUX is) (ADJP (RB culturally) (JJ important))))) (CC and) (SBAR (IN whether) (S (NP (PRP$ his) (JJ cultural) (NN definition)) (VP (AUX is) (ADJP (JJ appropriate) (PP (IN for) (NP (DT a) (JJ legal) (NN definition))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (S (NP (NN appropriateness)) (VP (AUX is) (ADJP (JJ irrelevant)))))) (. .)))

(S1 (S (NP (JJ Cultural) (NNS definitions)) (VP (AUX have) (VP (VBN formed) (NP (NP (DT the) (NN root)) (PP (IN of) (NP (DT all) (JJ other) (NNS definitions)))))) (. .)))

(S1 (S (NP (NP (DT The) (JJ cultural) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (AUX has) (ADVP (RB always)) (VP (AUX been) (NP (NP (CD one)) (ADJP (JJ male) (CC and) (JJ female))) (PP (IN in) (NP (NN monogamy))))) (. .)))

