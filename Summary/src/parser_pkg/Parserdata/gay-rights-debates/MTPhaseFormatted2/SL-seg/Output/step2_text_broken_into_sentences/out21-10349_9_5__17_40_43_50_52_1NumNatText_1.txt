<PARAGRAPH> S1 States that the current definition of marriage specifically excludes homosexuals because the current definition is "two people that are not underage, or opposite genders, same species, not closely related and not already married" and that gays want to be married under current definition.
Believes that ability to reproduce is implied in definition.
S2 Claims S1's definition is not only possible, refers to lack of reproductive abilities of an elderly person and a younger person marrying.
S1 Refers to differences between cultural definitions of marriage and mating pairs or legal definitions of marriage.
S2 Ask who agve S1 right to define cultural meanings.
S1 Claims that cultural meaning comes from observation and that legal definition can be decided any way.
S2 Asks who gets to make that legal definition.
Claims that those that oppose gay marriage are opposing something that harms no one and are trying to limit other's actions.
S1 States that in America people vote on laws or those that make them and that is who gets to determine legality of different marriage concepts.
