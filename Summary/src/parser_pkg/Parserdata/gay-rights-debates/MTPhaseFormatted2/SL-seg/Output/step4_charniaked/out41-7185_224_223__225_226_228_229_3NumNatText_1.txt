(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NNS politicians)) (VP (MD should) (VP (AUX be) (VP (VBN stoned) (PP (IN in) (NP (NN public))) (PP (IN as) (NP (NP (NN punishment)) (PP (IN for) (NP (PRP$ their) (JJ deviant) (JJ sexual) (NNS behaviors))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (DT the) (NN story)) (PP (IN of) (NP (NP (NNP Fred) (NNP Thompson)) (, ,) (NP (NP (DT a) (NN politician)) (SBAR (WHNP (WP who)) (S (VP (AUX was) (VP (VP (VBN married) (PP (IN at) (NP (CD 17)))) (CC and) (VP (VBN fathered) (NP (NP (CD three) (NNS children)) (PP (IN with) (NP (PRP$ his) (JJ first) (NN wife)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX was) (ADVP (RB then)) (VP (VBN divorced) (CC and) (VBN remarried) (PP (TO to) (ADJP (ADJP (NP (DT a) (NN woman)) (JJR younger)) (PP (IN than) (NP (NP (PRP$ his) (JJS oldest) (NN child)) (SBAR (WHPP (IN with) (WHNP (WP whom))) (S (NP (PRP he)) (VP (VBD fathered) (NP (CD two) (JJR more) (NNS children))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (ADJP (JJ unclear)) (SBAR (WHADVP (WRB why)) (S (NP (DT this)) (VP (AUX is) (ADJP (JJ relevant)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ goes) (ADVP (RB on)) (S (VP (TO to) (VP (VB claim) (SBAR (SBAR (IN that) (S (NP (NNP Fred) (NNP Thompson)) (VP (AUX is) (NP (NP (DT a) (NN member)) (PP (IN of) (NP (NP (DT the) (NNS Churches)) (PP (IN of) (NP (NNP Christ))))) (SBAR (WHNP (WDT which)) (S (VP (AUX is) (NP (DT a) (JJ religious) (NN cult))))))))) (CC and) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (ADVP (RB really)) (NP (DT a) (JJ Christian)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ jokes) (SBAR (IN that) (S (PP (IN if) (NP (NP (PRP you) (NN stone) (NN someone)) (PP (IN in) (NP (NN public))))) (NP (PRP you)) (VP (MD might) (VP (AUX be) (VP (VBN brought) (PRT (RP up)) (PP (IN on) (NP (NN drug) (NNS charges))))))))) (CC and) (VP (VBZ wonders) (SBAR (IN if) (S (NP (NNS people)) (VP (VBP think) (SBAR (S (NP (NNP Fred) (NNP Thompson)) (VP (AUX is) (ADVP (RB simply)) (VP (VBG pretending) (S (VP (TO to) (VP (AUX be) (ADJP (JJ religious)))))))))))))) (. .)))

