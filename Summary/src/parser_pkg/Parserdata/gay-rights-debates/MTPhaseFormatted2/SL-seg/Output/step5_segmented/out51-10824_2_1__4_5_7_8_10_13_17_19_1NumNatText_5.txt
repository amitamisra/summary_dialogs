<T>
<P>
</P>
<P>
<S>
<C>S1/NNP liked/VBD an/DT editorial/NN about/IN President/NNP Barrack/NNP Obama/NNP ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ every/DT Republican/JJ candidate/NN wants/VBZ gays/NNS to/TO be/AUX in/IN jail/NN </C>
<C>by/IN reinstating/VBG anti-sodomy/JJ laws/NNS and/CC gay/JJ marriage/NN banned/VBN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ President/NNP Barrack/NNP Obama/NNP has/AUX done/AUX more/JJR for/IN gays/NNS then/RB any/DT other/JJ president/NN </C>
<C>and/CC that/IN he/PRP will/MD come/VB out/RP for/IN full/JJ marriage/NN equality/NN </C>
<C>but/CC is/AUX not/RB trying/VBG to/TO impose/VB his/PRP$ views/NNS on/IN others/NNS </C>
<C>as/IN the/DT Republicans/NNPS are/AUX ./. </C>
</S>
<S>
<C>S1/JJ references/NNS S2/VBP </C>
<C>as/IN fear-mongering/VBG and/CC engaging/VBG in/IN gay-pride/JJ politics/NNS ./. </C>
</S>
<S>
<C>S2/NNP references/NNS the/DT Republicans/NNPS as/IN being/AUXG like/IN the/DT brown/JJ shirts/NNS ./. </C>
</S>
<S>
<C>S2/NNP claims/VBZ that/IN Rick/NNP Perry/NNP supports/VBZ a/DT Ugandan/NNP law/NN that/WDT would/MD execute/VB gays/NNS ./. </C>
</S>
<S>
<C>S1/NNP accuses/VBZ S2/NNP of/IN paranoia/NN ./. </C>
</S>
<S>
<C>S2/JJ references/NNS Japanese/JJ internment/NN and/CC killings/NNS of/IN Native/JJ Americans/NNPS by/IN the/DT military/NN ./. </C>
</S>
<S>
<C>S1/NNP states/VBZ that/IN these/DT events/NNS have/AUX not/RB happened/VBN since/IN then/RB ./. </C>
</S>
<S>
<C>S2/VB that/IN they/PRP could/MD happen/VB again/RB ./. </C>
</S>
<S>
<C>S1/NNS make/VBP rude/JJ comments/NNS about/IN the/DT police/NN not/RB closing/VBG meat-markets/NNS or/CC hook-up/NN spots/NNS ./. </C>
</S>
<S>
<C>S2/NNP references/NNS that/IN that/DT has/AUX happened/VBN under/IN this/DT constitution/NN and/CC can/MD again/RB ./. </C>
</S>
<S>
<C>S1/NNP asks/VBZ what/WP he/PRP wants/VBZ done/RB ,/, a/DT change/NN to/TO the/DT constitution/NN ./. </C>
</S>
<S>
<C>S2/NNP says/VBZ a/DT limit/NN to/TO religions/NNS role/NN in/IN society/NN ./. </C>
</S>
</P>
</T>
