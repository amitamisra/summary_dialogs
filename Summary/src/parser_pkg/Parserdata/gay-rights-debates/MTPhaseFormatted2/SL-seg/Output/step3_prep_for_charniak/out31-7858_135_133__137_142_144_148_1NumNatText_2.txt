<s> <PARAGRAPH> </s>
<s>  S1 thinks the definition of marriage is subject to change. </s>
<s> As an example S1 points to an old definition of marriage in which a woman lost her identity into that of a man. </s>
<s> S1 believes fundamental rights are not subject to vote and cites Loving v. </s>
<s> Virginia as an example. </s>
<s> He believes the courts are an intermediate body between the people and legislature; thus to ban gay marriage one must prove it is not a right or give compelling evidence against it. </s>
<s> S2 wonders what marriage being a fundamental right has to do with defining marriage. </s>
<s> If marriage is subject to change then he asks how it is supposed to be changed if not by vote. </s>
<s> He takes issue with the judiciary becoming the legislative branch. </s>
<s>  </s>
