<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG gay/JJ rights/NNS ./. </C>
</S>
<S>
<C>S1/NNP challenges/VBZ S2/NNP to/TO find/VB real/JJ scientific/JJ evidence/NN ,/, </C>
<C>as/IN anything/NN coming/VBG from/IN Cameron/NNP does/AUX not/RB apply/VB </C>
<C>as/IN he/PRP has/AUX been/AUX kicked/VBN out/RP and/CC condemned/VBN publicly/RB from/IN the/DT legitimate/JJ scientific/JJ community/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN homosexuality/NN is/AUX not/RB a/DT disease/NN ,/, </C>
<C>and/CC that/IN HIV/NNP infects/VBZ anyone/NN ;/: </C>
<C>being/AUXG gay/JJ does/AUX not/RB put/VB people/NNS at/IN a/DT greater/JJR risk/NN ,/, promiscuity/NN and/CC IV/NNP drug/NN usage/NN do/AUX ./. </C>
</S>
<S>
<C>S2/NNP gives/VBZ a/DT hypothetical/JJ situation/NN ,/, </C>
<C>where/WRB a/DT minister/NN of/IN public/JJ health/NN has/AUX proof/NN that/IN people/NNS with/IN a/DT certain/JJ lifestyle/NN live/VBP shorter/JJR </C>
<C>and/CC he/PRP questions/VBZ what/WP S1/NNP will/MD do/AUX in/IN that/DT situation/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN granting/VBG gays/RB the/DT rights/NNS and/CC privileges/NNS of/IN marriage/NN affects/VBZ everyone/NN financially/RB and/CC negatively/RB ./. </C>
</S>
</P>
</T>
