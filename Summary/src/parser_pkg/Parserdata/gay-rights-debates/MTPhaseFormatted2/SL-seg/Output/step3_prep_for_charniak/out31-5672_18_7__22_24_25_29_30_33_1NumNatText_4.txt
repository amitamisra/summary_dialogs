<s> <PARAGRAPH> </s>
<s>  S1 is pointing out that by changing the definition of marriage in a proposed amendment it will affect traditional marriages. </s>
<s> S1 quotes Colson and is attempting to make sure that Colson is not misrepresented. </s>
<s> That Colson's belief is that the "traditional" household is the best scenario and does the best job in regards to keeping their children out of prison. </s>
<s> He asserts that Colson feels the same regarding single parent households as well. </s>
<s> That they are not "bad" but traditional households have the most success. </s>
<s> S1 agrees with some of Colson's points but also agrees with many gay rights issues regarding discrimination, harassment, and adoption. </s>
<s> S1 also points out that S2 seems to attack posters personally rather than the topics at hand. </s>
<s> S2 feels that S1 continually draws conclusions about gay marriage from single parent household statistics. </s>
<s> S2 also feels that S1 may be compensated financially for participation in the forum due to the discrepancy of S1 opposing the amendment and his stated beliefs but constantly bringing up arguments which are in opposition to those beliefs. </s>
<s>  </s>
