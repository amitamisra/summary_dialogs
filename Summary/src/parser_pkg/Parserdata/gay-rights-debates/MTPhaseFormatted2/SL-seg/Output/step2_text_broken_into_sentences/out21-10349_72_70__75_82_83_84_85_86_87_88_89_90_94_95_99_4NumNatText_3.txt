<PARAGRAPH> S1 sees marriage as it is today as part of a social contract that is based upon biology and reproduction.
The drive to reproduce is part of the heterosexual bond and marriage is its formalization.
S1 is annoyed with S2 demanding that because his marriage is not universally recognized that there is something unconstitutional in place.
He explains that marriage is left to the States to determine what is legal and that the Federal government, according to the Constitution, has no place to change that.
S2 would like to see a change in the definition of marriage.
He holds a marriage licence but it is not universally recognized.
He argues that biology and reproduction do not apply when people past the age where childbearing is possible can marry.
He challenges S1 that if marriage truly is about reproduction and family that a married couple must prove that this is a part of their relationship or their marriage would be terminated.
He is offended that others' beliefs can play a role in his life.
