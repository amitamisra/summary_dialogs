<T>
<P>
</P>
<P>
<S>
<C>S1/NNP Claims/NNP never/RB argued/VBD stance/NN based/VBN upon/IN religion/NN ,/, </C>
<C>that/WDT would/MD be/AUX arbitrary/JJ ./. </C>
</S>
<S>
<C>Claims/NNS argument/NN is/AUX based/VBN upon/IN biology/NN ./. </C>
</S>
<S>
<C>S2/VBG Claims/NNS argument/NN on/IN biology/NN void/NN ,/, </C>
<C>references/NNS age/VBP difference/NN ./. </C>
</S>
<S>
<C>S1/NNP Claims/NNPS debate/NN is/AUX really/RB about/IN benefits/NNS not/RB marriage/NN itself/PRP ./. </C>
</S>
<S>
<C>Claims/NNS heterosexual/JJ marriage/NN is/AUX based/VBN on/IN biology/NN </C>
<C>even/RB if/IN reproduction/NN is/AUX impossible/JJ ./. </C>
</S>
<S>
<C>S2/NNP Wants/VBZ equality/NN ,/, claims/NNS constitutionality/NN ./. </C>
</S>
<S>
<C>S1/NNP Refers/VBZ to/TO S2s/JJ arguments/NNS about/IN benefits/NNS ./. </C>
</S>
<S>
<C>S2/NNP Claims/VBZ to/TO have/AUX been/AUX commenting/VBG for/IN years/NNS ./. </C>
</S>
<S>
<C>S1/NNP Does/AUX not/RB care/VB ,/, </C>
<C>accuses/VBZ S2/NNP of/IN hypocrisy/NN ./. </C>
</S>
<S>
<C>S2/NNP Refers/VBZ to/TO heterosexuals/NNS that/WDT can/MD not/RB breed/VB ,/, or/CC will/MD not/RB ,/, can/MD still/RB get/VB married/VBN ./. </C>
</S>
<S>
<C>S1/NNP Refers/VBZ to/TO S2/NNP having/AUXG legal/JJ marriage/NN in/IN one/CD state/NN and/CC not/RB others/NNS as/IN will/NN of/IN the/DT people/NNS ./. </C>
</S>
<S>
<C>Again/RB refers/VBZ to/TO the/DT demand/NN being/AUXG about/IN benefits/NNS ./. </C>
</S>
<S>
<C>S2/NNP Asks/VBZ why/WRB gays/NNS should/MD be/AUX different/JJ ./. </C>
</S>
<S>
<C>S1/NNP Says/VBZ that/IN the/DT idea/NN of/IN homosexuals/NNS and/CC heterosexuals/NNS are/AUX the/DT same/JJ is/AUX political/JJ fiction/NN ./. </C>
</S>
<S>
<C>Claims/VBZ that/IN being/AUXG gay/JJ is/AUX like/IN being/AUXG left-handed/JJ ,/, </C>
<C>it/PRP 's/AUX too/RB bad/JJ that/IN the/DT world/NN is/AUX made/VBN for/IN right/RB handed/VBN heterosexuals/NNS ./. </C>
</S>
<S>
<C>Because/IN you/PRP want/VBP something/NN does/AUX not/RB mean/VB it/PRP 's/AUX your/PRP$ right/NN ./. </C>
</S>
<S>
<C>Claims/VBZ it/PRP 's/AUX ignorant/JJ ,/, insulting/JJ &/CC arrogant/JJ to/TO believe/VB that/DT ./. </C>
</S>
</P>
</T>
