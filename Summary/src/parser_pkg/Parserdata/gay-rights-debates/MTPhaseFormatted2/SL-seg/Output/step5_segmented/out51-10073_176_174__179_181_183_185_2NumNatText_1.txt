<T>
<P>
</P>
<P>
<S>
<C>S1/NNP suggests/VBZ that/IN homosexuals/NNS can/MD choose/VB to/TO ignore/VB their/PRP$ sexual/JJ impulses/NNS which/WDT is/AUX different/JJ than/IN a/DT person/NN controlling/VBG their/PRP$ ethnicity/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN if/IN homosexuals/NNS do/AUX not/RB have/AUX a/DT choice/NN about/IN </C>
<C>whether/IN or/CC not/RB to/TO engage/VB in/IN homosexual/JJ fornication/NN ,/, </C>
<C>then/RB they/PRP would/MD be/AUX sexually/RB assaulting/VBG people/NNS ./. </C>
</S>
<S>
<C>S2/NNP suggests/VBZ that/IN homosexuals/NNS do/AUX not/RB have/AUX a/DT choice/NN about/IN who/WP they/PRP are/AUX attracted/VBN to/TO ./. </C>
</S>
<S>
<C>He/PRP clarifies/VBZ that/IN he/PRP is/AUX referring/VBG to/TO the/DT lack/NN of/IN choice/NN regarding/VBG sexual/JJ attraction/NN ,/, </C>
<C>that/IN being/AUXG straight/JJ or/CC gay/JJ is/AUX not/RB a/DT choice/NN ,/, rather/RB than/IN the/DT choice/NN regarding/VBG whether/IN to/TO engage/VB in/IN homosexual/JJ acts/NNS ./. </C>
</S>
<S>
<C>In/IN response/NN to/TO S1/NNP claiming/VBG that/IN homosexuals/NNS should/MD not/RB engage/VB in/IN ``/`` illicit/JJ gay/JJ sex/NN ,/, ''/'' S2/NNP asks/VBZ if/IN it/PRP is/AUX therefore/RB appropriate/JJ for/IN a/DT homosexual/JJ to/TO remain/VB celibate/VB their/PRP$ entire/JJ lives/NNS ./. </C>
</S>
</P>
</T>
