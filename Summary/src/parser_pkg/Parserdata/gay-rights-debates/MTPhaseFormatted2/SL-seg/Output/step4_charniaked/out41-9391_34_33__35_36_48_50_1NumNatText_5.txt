(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ rejects) (NP (DT the) (NN idea) (SBAR (IN that) (S (NP (NN homosexuality)) (VP (AUX is) (NP (DT a) (NN disease))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ doubts) (NP (NP (DT the) (NN evidence)) (PP (IN by) (NP (NNP Cameron)))) (PP (JJ due) (PP (TO to) (NP (DT the) (NN fact) (SBAR (IN that) (S (NP (PRP he)) (VP (VP (AUX was) (VP (VBN kicked) (PRT (RP out)) (PP (IN of) (NP (DT the) (JJ legitimate) (JJ scientific) (NN community))))) (CC and) (VP (AUX has) (S (NP (JJ numerous) (NNS scientists)) (VP (VB condemn) (NP (PRP him)) (PP (IN for) (NP (NP (NN misuse)) (PP (IN of) (NP (NN research))))))))))))))) (. .)))

(S1 (S (S (NP (NP (JJ S1) (NNS states)) (SBAR (WHNP (WDT that)) (S (NP (NNP HIV)) (VP (MD will) (VP (VP (VB affect) (NP (NN anyone))) (CC and) (VP (AUXG being) (ADJP (JJ gay)))))))) (VP (AUX does) (RB not) (VP (VBN put) (NP (CD one)) (PP (IN at) (NP (JJR greater) (NN risk)))))) (, ,) (CC but) (S (NP (NN promiscuity) (CC and) (NN drug) (NN use)) (VP (VP (AUX do)) (CC and) (VP (AUX are) (RB not) (ADJP (JJ exclusive) (PP (TO to) (NP (NNS homosexuals))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (NP (DT the) (NN analogy)) (PP (IN of) (NP (NP (NN homosexuality)) (PP (IN as) (NP (DT a) (NN disease)))))) (VP (AUX is) (ADJP (JJ harmful)))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ gives) (NP (NP (DT a) (JJ hypothetical) (NN situation)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (NP (DT a) (NN minister)) (PP (IN of) (NP (JJ public) (NN health)))) (VP (AUX has) (NP (NN proof) (SBAR (IN that) (S (NP (NP (NN part)) (PP (IN of) (NP (DT a) (NN population)))) (VP (AUX has) (VP (VBN contracted) (NP (NP (DT a) (NN disease)) (PP (JJ due) (TO to) (NP (PRP$ their) (NN lifestyle) (CC and) (NNS questions)))) (SBAR (WHNP (WP what)) (S (NP (DT the) (JJ proper) (NN response)) (VP (MD should) (VP (AUX be))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (VP (VBG granting) (NP (JJ gay) (NN marriage) (NNS rights) (NNS effects)) (NP (NP (NN everyone)) (ADJP (RB else) (RB financially))))))) (. .)))

