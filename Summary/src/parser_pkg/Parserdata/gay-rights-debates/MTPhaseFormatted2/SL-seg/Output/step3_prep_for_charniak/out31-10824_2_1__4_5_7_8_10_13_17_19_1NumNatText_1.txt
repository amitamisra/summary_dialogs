<s> <PARAGRAPH> </s>
<s>  S1 argues that main stream media is biased for Obama on the gay marriage issue. </s>
<s> S2 states that every Republican presidential candidate is anti-gay marriage and pro-sodomy bans. </s>
<s> According to S2, unlike the Republicans, Obama has refrained from attempting to make his personal opinion law. </s>
<s> Further, he has done more for gay rights than any other president. </s>
<s> S1 accuses S2 of fear mongering by misrepresenting the Republican stance on gay marriage. </s>
<s> S2 accuses the Republican party of fascism and a zero-tolerance position regarding gay marriage. </s>
<s> S1 accuses S2 of misrepresenting the nature of Republican opposition. </s>
<s> S2 notes that the United States has a history of oppressing minorities, such as Native Americans and African Americans, which could be extended to include gay citizens. </s>
<s> S1 notes that these are past not present US policies and unlikely to happen again. </s>
<s> S2 is not so sure. </s>
<s> S1 asks if S2's solution is to put a protection for gays in the US constitution. </s>
<s> S2 proposes limiting religious influence upon society as a suitable means of providing protection for homosexuals in America. </s>
<s>  </s>
