(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (NP (VB Prop) (CD 8)) (VP (VBZ needs) (S (VP (TO to) (VP (AUX be) (VP (VBN verified) (PP (IN as) (S (VP (AUXG being) (VP (VBN enacted) (ADVP (RB legally)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NNP DOMA)) (VP (AUX is) (RB not) (ADJP (JJ unconstitutional))))) (NP (EX there)) (VP (AUX is) (RB not) (NP (NP (NN point)) (PP (IN in) (S (VP (VBG fighting) (PP (IN over) (NP (PRP it))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (AUX has) (NP (NP (DT a) (JJ financial) (NN impact)) (PP (IN on) (NP (PDT all) (NP (NN tax) (NNS payers)) (CC and) (SBAR (IN that) (S (NP (NP (JJ federal) (NN law)) (PRN (-LRB- -LRB-) (NP (RB about) (NN DOMA)) (-RRB- -RRB-))) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (AUX be) (VP (VBN struck) (ADVP (RB down)) (SBAR (IN before) (S (NP (DT the) (NNP California) (NN law)) (VP (MD could) (VP (AUX be) (VP (VBN considered) (S (ADJP (JJ unconstitutional))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ says) (SBAR (SBAR (IN that) (S (NP (DT the) (JJ federal) (NN DOMA)) (VP (AUX is) (ADJP (JJ unconstitutional))))) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NN discussion)) (PP (IN of) (NP (DT the) (NNP California) (NN law)))) (VP (MD could) (VP (VB lead) (PP (TO to) (NP (DT this) (NN recognition))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (JJ gay) (NNS people)) (VP (VP (VB pay) (NP (NNS taxes))) (CC and) (VP (VB pay) (PP (IN for) (NP (JJ straight) (NNS marriages))) (SBAR (IN while) (S (VP (AUXG being) (VP (VBN denied) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB marry) (NP (PRP themselves))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (DT the) (JJ 14th) (NN amendment)) (VP (VBZ requires) (NP (JJ gay) (NN marriage)))))) (. .)))

