<T>
<P>
</P>
<P>
<S>
<C>S1/NNP contends/VBZ he/she/NN is/AUX in/IN support/NN of/IN civil/JJ union/NN rights/NNS for/IN same/JJ sex/NN couples/NNS but/CC opposed/VBN to/TO marriage/NN rights/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ </C>
<C>if/IN a/DT civil/JJ union/NN is/AUX to/TO be/AUX the/DT only/JJ right/NN given/VBN to/TO same/JJ sex/NN couples/NNS ,/, </C>
<C>then/RB the/DT same/JJ should/MD be/AUX true/JJ for/IN heterosexuals/NNS as/IN well/RB ./. </C>
</S>
<S>
<C>S2/NNP also/RB feels/VBZ the/DT Bible/NNP should/MD have/AUX nothing/NN to/TO do/AUX with/IN the/DT law/NN ,/, advising/VBG the/DT American/NNP government/NN does/AUX not/RB recognize/VB marriage/NN as/IN a/DT sacrament/NN due/JJ to/TO the/DT separation/NN of/IN church/NN and/CC state/NN ./. </C>
</S>
<S>
<C>While/IN S1/NNP agrees/VBZ church/NN and/CC state/NN are/AUX separate/JJ entities/NNS he/she/NN believes/VBZ voters/NNS can/MD cast/VB a/DT ballot/NN in/IN any/DT direction/NN they/PRP so/RB choose/VBP ,/, </C>
<C>which/WDT essentially/RB boils/VBZ down/RP to/TO each/DT voter/NN 's/POS religious/JJ beliefs/NNS ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ marriage/NN rights/NNS for/IN gays/NNS effects/NNS the/DT financial/JJ welfare/NN of/IN every/DT voter/NN ./. </C>
</S>
</P>
</T>
