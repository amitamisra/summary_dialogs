<s> <PARAGRAPH> </s>
<s>  S1 feels that Jim and MrWrite's argument has become a battle of insults, and that this is unfortunate, but that the comparison between the argument against gay marriage and that against interracial marriage is not baseless. </s>
<s> He cites that opponents of gay marriage use many of the same arguments that were used to oppose interracial marriage, before that was outlawed. </s>
<s> He believes that most opponents of gay marriage do so for religious or homophobic reasons. </s>
<s> He argues that words get redefined on an ongoing basis, citing the word war being modified to suit political purposes. </s>
<s> He does not believe that the justifications for the current war in Iraq have much merit, and that the same argument could be made for reentering Vietnam or Korea. </s>
<s> S2 does not feel that interracial marriage and homosexual marriage have anything to do with each other. </s>
<s> He feels that separate-but-equal would apply to the records of gay couples but would not justify any restrictions of there rights or privileges as a couple. </s>
<s> He preemptively denies being homophobic, citing his having gay friends who know what his views on the word marriage are. </s>
<s> He does not feel that the Iraq War is technically a new war requiring all of the congressional steps, rather that it was merely a continuation of the Gulf War, or that the same arguments could be applied to those other conflicts. </s>
<s>  </s>
