(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (DT some) (NN merit)) (PP (IN in) (NP (NP (DT the) (NN comparison)) (PP (IN of) (NP (NP (NNS arguments)) (PP (IN against) (NP (NP (JJ interracial) (NN marriage)) (CC and) (NP (DT those)))) (PP (IN against) (NP (JJ homosexual) (NN marriage))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (NP (JJ many) (NNS opponents)) (PP (IN of) (NP (JJ homosexual) (NN marriage)))) (VP (VBP use) (NP (NP (JJ many)) (PP (IN of) (NP (NP (DT the) (JJ same) (NNS arguments)) (SBAR (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBD opposed) (NP (JJ interracial) (NN marriage)))))) (VP (AUX did))))))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB also)) (VP (VBP argue) (SBAR (IN that) (S (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VBP oppose) (NP (JJ homosexual) (NN marriage)))))) (VP (AUX do) (ADVP (RB so)) (PP (IN for) (NP (DT both) (JJ religious) (CC and) (JJ homophobic) (NNS reasons))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NNS words)))) (VP (VB get) (VP (VP (ADVP (RB frequently)) (VBN modified) (PP (IN in) (NP (DT some) (NN way)))) (CC and) (VP (VBZ uses) (NP (NP (DT the) (NN example)) (PP (IN of) (NP (NP (DT the) (NN use)) (PP (IN of) (NP (DT the) (NN word) (POS ')))))) (NP (NNP War) (POS ')) (SBAR (WHADVP (WRB when)) (S (VP (VBG referring) (PP (TO to) (NP (DT the) (NNP Iraq) (NNP War))) (SBAR (WHADVP (WRB when)) (S (NP (NNP Congress)) (VP (VBD gave) (NP (NP (DT no) (JJ official) (NN declaration)) (PP (IN of) (NP (NNP War)))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (IN that) (S (NP (NN today)) (NP (NP (DT a) (NN lot)) (PP (IN of) (NP (JJ military) (NNS operations)))) (VP (AUX are) (ADVP (RB automatically)) (VP (VBN grouped) (PP (IN under) (NP (NP (DT the) (NN word) (POS ')) (NX (NNP War) (POS '))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (S (VP (VBG using) (NP (NP (NNS examples)) (PP (IN of) (NP (JJ interracial) (NN marriage) (NNS protests)))))) (VP (AUX has) (NP (NP (NN relevance)) (PP (TO to) (NP (NP (DT the) (NN argument)) (PP (IN over) (S (VP (VBG allowing) (NP (JJ homosexual) (NN marriage))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP argue) (SBAR (SBAR (IN that) (S (NP (NP (DT any) (JJ separate) (NN marriage) (NN provision)) (PP (IN for) (NP (NNS Homosexuals)))) (VP (MD should) (VP (AUX be) (ADJP (ADJP (RB as) (JJ inclusive)) (PP (IN as) (NP (JJ Heterosexual) (NNS marriages)))))))) (, ,) (CC but) (SBAR (IN that) (S (NP (DT the) (CD two)) (VP (MD should) (VP (VB remain) (ADJP (JJ separate) (PP (IN from) (NP (DT each) (JJ other)))) (PP (IN by) (NP (NN definition))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN marriage)))) (VP (MD should) (VP (VB change) (S (VP (TO to) (VP (VB include) (NP (JJ homosexual) (NNS couples))))))))))) (. .)))

