(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ accuses) (NP (NNP S2)) (PP (IN of) (NP (VBG lying)))) (CC and) (VP (VBZ calls) (S (NP (PRP him)) (NP (DT a) (JJ racist) (NN sodomite))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (NP (NP (DT a) (NN Sodomite)) (PP (CC but) (NP (DT a) (NNP Houstonian)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (AUX was) (ADJP (VBN married) (PP (TO to) (NP (NP (DT a) (JJ black) (NN man)) (PP (IN in) (NP (CD 2003))))))) (CC and) (VP (VBZ recommends) (SBAR (IN that) (S (NP (NNP S1)) (VP (VB obtain) (NP (DT a) (JJ remedial) (NN education)) (PP (IN in) (NP (NP (DT the) (NN use)) (PP (IN of) (NP (DT the) (JJ English) (NN language)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB repeatedly)) (VP (VBZ chastises) (NP (NP (NNP S2)) (CC and) (NP (PRP$ his) (NN husband))) (PP (IN for) (S (VP (VBG living) (PP (IN in) (NP (NP (NN sin)) (CC and) (NP (PRP$ their) (NN need) (S (VP (TO to) (VP (VB repent))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB listen) (PP (TO to) (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VP (VBP preach) (PP (IN like) (NP (DT that)))) (, ,) (CC and) (VP (VBZ challenges) (S (NP (NNP S1)) (VP (TO to) (VP (VB call) (S (VP (TO to) (VP (VB mind) (NP (NP (PRP$ her) (JJ own) (NNS sins)) (SBAR (WHPP (IN for) (WHNP (WDT which))) (S (NP (NN repentance)) (VP (MD would) (VP (AUX be) (ADJP (JJ appropriate)))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ says) (SBAR (IN that) (S (NP (NP (NNP S2)) (CC and) (NP (PRP$ his) (NN husband))) (VP (AUX are) (VP (VBG going) (PP (TO to) (NP (NN hell)))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ replies) (SBAR (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB believe) (NP (PRP it)))))))) (, ,) (NP (PRP he)) (VP (VBZ suggests) (SBAR (S (NP (NNPS S1)) (VP (VBP preach) (PP (TO to) (NP (NP (NN someone)) (ADJP (JJ stupid) (RB enough) (S (VP (TO to) (VP (VB believe) (NP (PRP her)))))))))))) (. .)))

