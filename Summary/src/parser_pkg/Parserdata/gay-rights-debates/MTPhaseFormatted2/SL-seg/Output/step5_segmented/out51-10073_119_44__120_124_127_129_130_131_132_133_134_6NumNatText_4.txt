<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN homosexual/JJ marriages/NNS should/MD not/RB be/AUX prohibited/VBN ,/, but/CC also/RB believes/VBZ that/IN heterosexual/JJ marriages/NNS and/CC homosexual/JJ marriages/NNS are/AUX not/RB the/DT same/JJ ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN if/IN homosexuals/NNS are/AUX allowed/VBN to/TO marry/VB ,/, </C>
<C>than/IN any/DT kind/NN of/IN marriage/NN between/IN consenting/VBG adults/NNS should/MD be/AUX allowed/VBN ,/, </C>
<C>otherwise/RB you/PRP are/AUX discriminating/VBG against/IN the/DT person/NN wanting/VBG to/TO enter/VB into/IN the/DT marriage/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN S2/NNP deserves/VBZ as/RB much/JJ protection/NN as/IN an/DT incestuous/JJ or/CC polygamous/JJ relationship/NN that/WDT has/AUX children/NNS ./. </C>
</S>
<S>
<C>Overall/JJ ,/, they/PRP believe/VBP that/IN if/IN homosexual/JJ marriages/NNS are/AUX allowed/VBN then/RB </C>
<C>it/PRP should/MD branch/VB out/RP to/TO other/JJ types/NNS of/IN potential/JJ marriages/NNS ./. </C>
</S>
<S>
<C>S2/NNP points/VBZ out/RP that/IN with/IN full/JJ civil/JJ marriage/NN equality/NN ,/, benefits/NNS otherwise/RB not/RB available/JJ to/TO homosexual/JJ couples/NNS are/AUX now/RB options/NNS such/JJ as/IN survivor/NN 's/POS benefits/NNS of/IN social/JJ security/NN or/CC retained/VBN guardianship/NN of/IN their/PRP$ children/NNS ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB see/VB a/DT reason/NN for/IN homosexual/JJ couples/NNS to/TO have/AUX to/TO go/VB through/IN a/DT different/JJ process/NN than/IN heterosexuals/NNS </C>
<C>in/IN order/NN to/TO gain/VB the/DT same/JJ benefits/NNS of/IN heterosexual/JJ marriage/NN ./. </C>
</S>
<S>
<C>Since/IN S2/NNP has/AUX children/NNS ,/, </C>
<C>they/PRP do/AUX not/RB believe/VB that/IN they/PRP are/AUX any/DT less/RBR deserving/JJ of/IN government/NN benefits/NNS of/IN marriage/NN than/IN heterosexual/JJ couples/NNS with/IN children/NNS ./. </C>
</S>
</P>
</T>
