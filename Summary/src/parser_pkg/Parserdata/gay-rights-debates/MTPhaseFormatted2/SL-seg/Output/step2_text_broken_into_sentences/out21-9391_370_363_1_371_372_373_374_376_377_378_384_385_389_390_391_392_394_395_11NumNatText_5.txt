<PARAGRAPH> S1 argues that banning same-sex marriage is nothing like racial segregation.
He claims that S2 is misrepresenting his argument and that he was not actually implying that the situation for homosexuals is completely unlike that for blacks.
He argues that S2 does not understand the point he is trying to make, that S2 is simply wrong, and that S2 has only succeeded in discrediting himself.
S2 rejects S1's claim that banning same-sex marriage is nothing like racial segregation.
He argues that it is exactly like racial or any other type of segregation that has ever occurred.
He suggests that whenever a discriminatory behavior like racism, sexism, anti-Semetism, or homophobia is challenged, the perpetrators claim it is not the same as previous instances, but that all instances of discrimination are inherently the same.
He interprets S1's argument as implying that it would not be as bad if society treated homosexuals the same way they treated blacks in the past.
He also attacks S1, claiming that personal attacks are not an effective method of validating an argument.
