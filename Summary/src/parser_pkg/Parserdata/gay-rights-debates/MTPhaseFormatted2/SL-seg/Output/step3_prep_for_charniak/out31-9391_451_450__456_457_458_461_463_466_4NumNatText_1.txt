<s> <PARAGRAPH> </s>
<s>  S1 asserts that marriage is a contract between two people and the state and that it is not appropriate for religious institutions to be involved with that decision. </s>
<s> He claims that because he is forced to accept heterosexual marriages, society should be forced to accept homosexual marriages. </s>
<s> He accuses Christian organizations for supporting anti-gay legislation and claims that denying same-sex marriage is unjust, and that preventing same-sex marriage is a method of promoting heterosexual privilege. </s>
<s> S2 argues that heterosexual marriage is the status quo, so it rests on proponents of same-sex marriage to demonstrate that this new definition of marriage is superior to the current definition. </s>
<s> He argues that we do not actually have a basic right to love or to marry whomever we love. </s>
<s>  </s>
