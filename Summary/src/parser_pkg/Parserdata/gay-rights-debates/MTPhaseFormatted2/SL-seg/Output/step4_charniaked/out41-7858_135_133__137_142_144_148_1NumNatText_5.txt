(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG debating) (NP (JJ gay) (NN marriage) (NNS rights)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (AUX is) (PP (FW pro) (NP (JJ gay) (NN marriage))) (SBAR (IN while) (S (NP (NNP S2)) (VP (VBZ appears) (PP (IN against)))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NN constitution)) (VP (MD can) (VP (VB evolve) (PP (IN with) (NP (VBG changing) (NNS times))) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (NP (DT a) (JJ man-made) (S (VP (VB construct) (SBAR (IN while) (S (NP (NNP S2)) (VP (VBZ cites) (NP (DT the) (NN fact) (SBAR (S (NP (CD 26) (NNS states)) (ADVP (RB still)) (VP (VBP consider) (S (NP (NP (DT the) (NN sanctity)) (PP (IN of) (NP (NN marriage)))) (VP (TO to) (VP (AUX be) (NP (NP (CD one) (NN man)) (CC and) (NP (CD one) (NN woman)))))))))))))))))))))))) (. .)))

(S1 (S (S (ADVP (RB Also)) (VP (VBG advising) (NP (DT a) (JJ key) (NN point)) (PP (TO to) (NP (DT the) (NN nation))))) (VP (AUX is) (NP (NP (DT the) (NN basis)) (PP (IN of) (NP (NP (NNS laws)) (VP (AUXG being) (VP (VBN voted) (PP (IN upon)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (S (NP (NP (NNP S1) (POS 's)) (NNS statements)) (VP (AUX are) (ADJP (JJ correct) (S (VP (VBG regarding) (SBAR (IN whether) (CC or) (RB not) (S (NP (NN marriage)) (VP (AUX is) (NP (DT a) (NN right))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (NNS sites) (NP (NNP Loving) (NNP v))) (. .)))

(S1 (S (NP (NP (NNP Virginia)) (PP (IN in) (NP (NN argument) (NN marriage)))) (VP (AUX is) (RB not) (NP (DT a) (JJ human) (VB construct))) (. .)))

(S1 (S (NP (NNP S1)) (ADVP (RB also)) (VP (VBZ advises) (SBAR (S (NP (NN marriage)) (VP (VP (AUX is) (NP (DT a) (JJ fundamental) (NN right))) (CC and) (VP (MD should) (VP (AUX be) (NP (NP (DT a) (JJ fundamental) (NN right)) (PP (IN for) (NP (DT all))) (S (RB not) (VP (TO to) (VP (AUX be) (VP (VBN determined) (PP (IN by) (NP (DT each) (NN state))) (ADVP (RB individually))))))))))))) (. .)))

