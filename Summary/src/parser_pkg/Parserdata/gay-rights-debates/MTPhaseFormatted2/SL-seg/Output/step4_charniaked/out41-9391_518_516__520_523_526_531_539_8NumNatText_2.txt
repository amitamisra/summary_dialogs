(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (IN that) (S (NP (NP (DT the) (NN right)) (PP (TO to) (NP (NP (NN recognition)) (PP (IN of) (NP (NP (CD two) (NNS people)) (SBAR (S (VP (TO to) (VP (VB form) (NP (DT a) (NN couple))))))))))) (VP (AUX is) (VP (AUXG being) (VP (ADVP (RB unfairly)) (VBN denied) (NP (NNS homosexuals)))))))) (. .)))

(S1 (S (SBAR (IN That) (S (NP (DT a) (JJ heterosexual) (NN couple)) (VP (AUX is) (VP (VBN allowed) (S (VP (TO to) (VP (VB make) (NP (NN life) (NNS decisions))))))))) (, ,) (VP (VP (VB file) (NP (NNS taxes))) (CC and) (VP (VP (AUX be) (VP (VBN covered) (PP (IN under) (NP (NN insurance))) (ADVP (RB jointly)))) (, ,) (VP (VBP inherit) (NP (NN property))) (, ,) (CC and) (VP (ADJP (JJR more))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ compares) (NP (NP (DT the) (NN ban)) (PP (IN on) (NP (NP (JJ homosexual) (NN marriage)) (PP (TO to) (NP (NP (DT the) (JJ former) (NN ban)) (PP (IN on) (NP (JJ interracial) (NN marriage))))))))) (, ,) (CC and) (VP (VBZ feels) (SBAR (IN that) (S (S (VP (AUXG having) (NP (NN gender)) (PP (IN as) (NP (NP (DT a) (VBG qualifying) (NN factor)) (PP (IN for) (NP (NN marriage))))))) (VP (AUX is) (ADJP (JJ equal) (PP (TO to) (S (VP (VBG using) (NP (NN race)) (PP (IN as) (NP (DT a) (NN factor)))))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ calls) (S (NP (NP (DT the) (NNS arguments)) (PP (IN against) (NP (JJ homosexual) (NN couple)))) (ADJP (JJ semantic))))) (, ,) (CC and) (S (VP (VB feel) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (VP (AUXG being) (VP (VBN denied) (NP (NNS rights)) (SBAR (IN because) (S (NP (DT some) (NNS people)) (VP (AUX do) (RB not) (VP (VB like) (S (NP (NP (DT the) (NN idea)) (PP (IN of) (NP (PRP them)))) (VP (AUXG having) (NP (NN sex))))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ denies) (NP (NP (DT the) (NN inequality)) (PP (IN of) (NP (NP (NNS restrictions)) (PP (IN between) (NP (JJ homosexual) (CC and) (JJ heterosexual) (NNS couples))))))) (CC and) (VP (AUX does) (RB not) (VP (VB feel) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB change) (NP (DT this) (NN law)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ denies) (SBAR (IN that) (S (NP (NN race) (CC and) (VB gender)) (VP (AUX are) (NP (NN equivalent) (NNS differences)))))) (CC and) (VP (VBZ says) (SBAR (IN that) (S (NP (EX there)) (VP (AUX are) (NP (NP (DT no) (JJ unshared) (NNS barriers)) (PP (IN between) (NP (UCP (NN hetero) (CC and) (NN homo)) (JJ sexual) (NNS couples))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (NP (NP (NNS individuals)) (SBAR (WHNP (WDT that)) (S (VP (AUX have) (VP (NNS rights) (NP (RB not) (NNS couples)))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NN no-one)) (VP (MD can) (VP (VB marry)))))))) (: ;) (S (NP (NN someone)) (VP (VP (VBN married)) (, ,) (NP (NP (DT an) (JJ animal)) (, ,) (NP (DT a) (JJ minor)) (, ,) (CC or) (NP (DT a) (JJ sibling))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ uses) (NP (NN evolution)) (S (VP (TO to) (VP (VB validate) (NP (DT the) (NN view) (SBAR (IN that) (S (NP (JJ gay) (NNS relationships)) (VP (AUX have) (ADJP (ADJP (JJR less) (VBG meaning)) (PP (IN than) (NP (JJ straight) (NNS relationships)))))))))))) (. .)))

