<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB believe/VB in/IN Christianity/NNP and/CC asks/VBZ for/IN proof/NN of/IN the/DT resurrection/NN of/IN Christ/NNP ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ scientific/JJ evidence/NN and/CC human/JJ intelligence/NN show/VBP that/IN a/DT man/NN can/MD not/RB get/VB up/RP </C>
<C>after/IN being/AUXG dead/JJ for/IN three/CD days/NNS ./. </C>
</S>
<S>
<C>He/PRP sees/VBZ no/DT evidence/NN of/IN it/PRP being/AUXG true/JJ and/CC thinks/VBZ it/PRP 's/AUX all/DT hearsay/NN and/CC fairy/NN tales/NNS ./. </C>
</S>
<S>
<C>Just/RB because/IN billions/NNS believe/VBP something/NN </C>
<C>it/PRP does/AUX not/RB mean/VB it/PRP is/AUX true/JJ ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ faced/VBN with/IN a/DT lack/NN of/IN evidence/NN Christians/NNS resort/VBP to/TO name/VB calling/VBG and/CC he/PRP feels/VBZ </C>
<C>if/IN the/DT pit/NN were/AUX real/JJ they/PRP would/MD both/DT be/AUX in/IN it/PRP ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN S1/NNP is/AUX a/DT anti-Christ/NNP that/WDT likes/VBZ to/TO wine/NN ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB agree/VB with/IN S1/NNP 's/POS opinions/NNS and/CC thinks/VBZ there/EX will/MD be/AUX no/DT Sodomites/NNS ,/, meaning/VBG homosexuals/NNS ,/, </C>
<C>in/IN heaven/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ S1/NNP will/MD go/VB to/TO hell/NN ./. </C>
</S>
</P>
</T>
