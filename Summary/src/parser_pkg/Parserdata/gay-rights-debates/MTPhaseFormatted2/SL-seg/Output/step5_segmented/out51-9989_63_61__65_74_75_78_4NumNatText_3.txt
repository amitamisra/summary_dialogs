<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ people/NNS need/AUX to/TO stop/VB being/JJ bigots/NNS and/CC open/VB their/PRP$ minds/NNS to/TO the/DT fact/NN that/IN not/RB everyone/NN is/AUX going/VBG to/TO be/AUX like/IN they/PRP are/AUX ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ political/JJ correctness/NN is/AUX good/JJ </C>
<C>and/CC that/IN you/PRP learn/VBP right/RB from/IN wrong/JJ on/IN your/PRP$ own/JJ by/IN being/AUXG kind/NN to/TO people/NNS and/CC treating/VBG them/PRP with/IN respect/NN ./. </C>
</S>
<S>
<C>S1/NNP thinks/VBZ the/DT Bible/NNP is/AUX a/DT cruel/JJ book/NN that/WDT should/MD not/RB be/AUX used/VBN as/IN an/DT example/NN ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ the/DT Bible/NNP is/AUX right/JJ </C>
<C>and/CC how/WRB people/NNS learn/VBP right/RB from/IN wrong/JJ ./. </C>
</S>
<S>
<C>He/PRP also/RB thinks/VBZ nothing/NN outside/JJ of/IN the/DT Bible/NNP is/AUX important/JJ ./. </C>
</S>
<S>
<C>S2/NNP thinks/VBZ he/PRP is/AUX helping/VBG gay/JJ people/NNS </C>
<C>by/IN saving/VBG them/PRP from/IN hell/NN </C>
<C>even/RB if/IN it/PRP does/AUX hurt/VB them/PRP </C>
<C>while/IN they/PRP are/AUX here/RB ./. </C>
</S>
</P>
</T>
