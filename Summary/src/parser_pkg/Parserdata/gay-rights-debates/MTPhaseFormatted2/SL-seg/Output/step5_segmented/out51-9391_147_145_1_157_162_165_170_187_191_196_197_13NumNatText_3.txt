<T>
<P>
</P>
<P>
<S>
<C>S1/NNP argues/VBZ that/IN homosexuality/NN is/AUX a/DT disgusting/JJ habit/NN ./. </C>
</S>
<S>
<C>He/PRP uses/VBZ certain/JJ sexual/JJ practices/NNS common/JJ in/IN the/DT gay/JJ community/NN as/IN evidence/NN ./. </C>
</S>
<S>
<C>Further/RB ,/, S1/NNP argues/VBZ that/IN supporting/VBG gay/JJ rights/NNS shows/VBZ the/DT lack/NN of/IN moral/JJ character/NN of/IN supporters/NNS ./. </C>
</S>
<S>
<C>S2/NNP retorts/VBZ that/IN in/IN fact/NN opposing/VBG gay/JJ rights/NNS shows/VBZ a/DT lack/NN of/IN moral/JJ character/NN and/CC accuses/VBZ S1/NNP of/IN in/IN fact/NN being/AUXG in/IN denial/NN of/IN his/PRP$ own/JJ homosexuality/NN ./. </C>
</S>
<S>
<C>He/PRP bases/VBZ this/DT accusation/NN on/IN S1/NNP 's/POS knowledge/NN of/IN aforementioned/JJ sexual/JJ practices/NNS and/CC familiarity/NN with/IN gay/JJ cultural/JJ practices/NNS ./. </C>
</S>
<S>
<C>S1/NNP argues/VBZ that/IN barring/VBG same/JJ sex/NN marriage/NN is/AUX an/DT exercise/NN of/IN constitutional/JJ rights/NNS </C>
<C>and/CC that/IN he/PRP has/AUX been/AUX happily/RB committed/VBN to/TO a/DT heterosexual/JJ marriage/NN for/IN many/JJ years/NNS ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN gay/JJ marriage/NN was/AUX denied/VBN to/TO married/JJ ,/, loving/JJ gay/JJ couples/NNS </C>
<C>and/CC that/IN being/AUXG married/VBN to/TO a/DT woman/NN is/AUX no/DT indicator/NN of/IN one/CD 's/POS true/JJ sexual/JJ preference/NN ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN S1/NNP 's/POS familiarity/NN with/IN gay/JJ practices/NNS points/NNS to/TO him/PRP possibly/RB being/AUXG in/IN denial/NN </C>
<C>or/CC hiding/VBG his/PRP$ true/JJ sexual/JJ preference/NN ./. </C>
</S>
<S>
<C>S1/NNP 's/POS repeated/VBD mocking/NN of/IN S2/NNP ,/, according/VBG to/TO S2/NNP does/AUX little/JJ to/TO hide/VB the/DT evidence/NN of/IN S1/NNP 's/POS true/JJ nature/NN or/CC sexual/JJ preference/NN ./. </C>
</S>
</P>
</T>
