<T>
<P>
</P>
<P>
<S>
<C>S1/NNP suggested/VBD stoning/VBG Vitter/NNP ,/, his/PRP$ hooker/NNP ,/, Craig/NNP and/CC the/DT man/NN he/PRP was/AUX with/IN on/IN the/DT Capitol/NNP steps/NNS ./. </C>
</S>
<S>
<C>He/PRP talked/VBD about/IN a/DT ``/`` Christian/JJ man/NN ''/'' who/WP married/VBD young/JJ ,/, had/AUX children/NNS ,/, divorced/VBD his/PRP$ first/JJ wife/NN wife/NN ,/, married/VBD a/DT woman/NN younger/JJR than/IN his/PRP$ oldest/JJS child/NN ,/, and/CC now/RB ,/, </C>
<C>at/IN the/DT age/NN of/IN 65/CD has/AUX two/CD very/RB young/JJ children/NNS ./. </C>
</S>
<S>
<C>This/DT man/NN says/VBZ he/PRP is/AUX a/DT member/NN of/IN the/DT Churches/NNS of/IN Christ/NNP </C>
<C>but/CC James/NNP Dobson/NNP says/VBZ this/DT man/NN is/AUX not/RB a/DT Christian/JJ ./. </C>
</S>
<S>
<C>In/IN regards/VBZ to/TO the/DT stoning/VBG comment/NN ,/, S2/NNP with/IN humor/NN said/VBD that/IN those/DT who/WP did/AUX stone/NN people/NNS on/IN the/DT Capitol/NNP steps/NNS could/MD get/VB arrested/VBN on/IN drug/NN charges/NNS ./. </C>
</S>
<S>
<C>He/PRP asked/VBD if/IN Dobson/NNP thinks/VBZ the/DT man/NN is/AUX pretending/VBG to/TO be/AUX a/DT Christian/JJ and/CC said/VBD that/IN he/PRP believes/VBZ Dobson/NNP is/AUX mostly/RB self/NN serving/VBG ./. </C>
</S>
</P>
</T>
