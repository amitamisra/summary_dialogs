<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ it/PRP is/AUX important/JJ to/TO look/VB at/IN how/WRB one/PRP treats/VBZ others/NNS and/CC not/RB be/AUX close/RB minded/VBN ./. </C>
</S>
<S>
<C>This/DT person/NN argues/VBZ that/IN you/PRP can/MD discern/VB right/RB </C>
<C>and/CC wrong/RB by/IN learning/VBG through/IN how/WRB you/PRP treat/VBP people/NNS with/IN kindness/NN and/CC respect/NN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB the/DT Bible/NNP is/AUX trustworthy/JJ </C>
<C>as/IN it/PRP advises/VBZ against/IN homosexuality/NN </C>
<C>while/IN advocating/VBG stoning/NN and/CC other/JJ such/JJ punishments/NNS ./. </C>
</S>
<S>
<C>S2/NNP believes/VBZ that/IN it/PRP is/AUX those/DT who/WP do/AUX not/RB believe/VB in/IN the/DT Bible/JJ that/WDT are/AUX close/RB minded/VBN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB it/PRP is/AUX right/JJ to/TO disregard/VB the/DT Bible/NNP out/IN of/IN political/JJ correctness/NN ./. </C>
</S>
<S>
<C>This/DT person/NN believes/VBZ that/IN you/PRP can/MD only/RB know/VB right/RB from/IN wrong/JJ if/IN you/PRP read/VBP the/DT Bible/JJ ,/, are/AUX saved/VBN ,/, </C>
<C>and/CC that/IN telling/VBG people/NNS they/PRP are/AUX living/VBG in/IN sin/NN is/AUX only/RB done/AUX with/IN their/PRP$ best/JJS interests/NNS in/IN mind/NN ./. </C>
</S>
</P>
</T>
