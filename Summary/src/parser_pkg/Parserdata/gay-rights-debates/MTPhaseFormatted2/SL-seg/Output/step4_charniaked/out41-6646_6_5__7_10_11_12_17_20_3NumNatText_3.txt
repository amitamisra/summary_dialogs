(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (DT any) (JJ legislative) (NN movement)) (VP (VBZ requires) (NP (NP (DT the) (NN support)) (PP (IN of) (NP (NP (DT the) (NNS people)) (SBAR (S (VP (TO to) (VP (AUX be) (ADJP (JJ successful))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NP (DT the) (JJ legal) (NNS decisions)) (PP (IN about) (NP (JJ important) (NNS issues)))) (VP (VBP involve) (NP (CONJP (RB not) (RB just)) (NP (DT the) (NN legislature)) (, ,) (CC but) (NP (JJ national) (NNS debates) (CC and) (NNS protests))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (DT this) (JJ same) (NN process)) (VP (AUX is) (ADVP (RB now)) (VP (VBG occurring) (PP (IN over) (NP (JJ gay) (NNS rights))))))) (CC and) (SBAR (IN that) (S (NP (NP (NNS people)) (VP (VBG voting) (PP (IN on) (NP (NNS laws))))) (VP (AUX is) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (JJ legal) (NN system)) (VP (VBZ works) (SBAR (IN in) (NN order) (S (VP (TO to) (VP (VB allow) (S (NP (NN society)) (VP (TO to) (VP (VB determine) (NP (PRP$ its) (JJ own) (NNS standards))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN problem)) (PP (IN with) (NP (NN marriage))) (VP (AUXG being) (VP (VBD redefined) (S (VP (TO to) (VP (VB include) (NP (NN same-sex) (NNS couples))))))))))) (, ,) (CC but) (SBAR (IN that) (S (NP (NP (DT this) (NN change)) (PP (IN of) (NP (NN definition)))) (VP (MD must) (VP (VB occur) (PP (IN through) (NP (DT the) (JJ official) (NN process))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ accuses) (NP (NNP S1)) (PP (IN of) (S (VP (VBG shifting) (PRT (RP off)) (NP (NN topic)) (UCP (PP (TO to) (NP (DT the) (NN voting) (NN process))) (CC and) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (NN legislature)) (VP (VBZ works) (, ,) (S (VP (VBG claiming) (SBAR (IN that) (S (NP (DT the) (JJ legal) (NNS intricacies)) (VP (AUX are) (ADJP (JJ irrelevant))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (SBAR (IN that) (S (S (VP (VBG allowing) (S (NP (DT the) (JJ general) (NN public)) (VP (TO to) (VP (VB determine) (NP (JJ civil) (NNS rights))))))) (VP (AUX is) (ADJP (JJ wrong))))) (CC and) (SBAR (IN that) (S (SBAR (IN if) (S (NP (DT a) (NN group)) (VP (VBZ needs) (S (VP (VBG protecting) (NP (PRP it))))))) (VP (AUX is) (NP (NP (DT the) (NN government) (POS 's)) (NN job) (S (VP (TO to) (VP (AUX do) (ADVP (RB so)))))) (, ,) (S (VP (VBG citing) (NP (NP (NNS instances)) (PP (IN in) (NP (DT the) (NN past))) (SBAR (WHADVP (WRB where)) (S (NP (DT the) (NN government)) (VP (AUX has) (VP (VBN taken) (NP (NP (DT a) (NN stand)) (PP (IN against) (NP (NN bigotry)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (SBAR (RB just) (IN because) (S (NP (NN something)) (VP (AUX is) (ADVP (RB currently)) (ADJP (JJ legal))))) (VP (AUX does) (RB not) (VP (VB mean) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ wrong)))))))))) (. .)))

