<T>
<P>
</P>
<P>
<S>
<C>S1/NNP says/VBZ it/PRP is/AUX wrong/JJ for/IN gay/JJ activists/NNS to/TO compare/VB public/JJ disapproval/NN of/IN homosexuality/NN with/IN race/NN discrimination/NN ./. </C>
</S>
<S>
<C>He/PRP takes/VBZ a/DT jab/NN at/IN gay/JJ activists/NNS love/VBP comparing/VBG public/JJ disapproval/NN of/IN homosexuality/NN with/IN racial/JJ discrimination/NN ./. </C>
</S>
<S>
<C>He/PRP has/AUX complaints/NNS about/IN the/DT public/NN 's/POS overwhelming/JJ disapproval/NN of/IN another/DT human/JJ being/NN 's/POS behavior/NN ./. </C>
</S>
<S>
<C>He/PRP notes/VBZ that/IN the/DT two/CD are/AUX two/CD different/JJ entities/NNS and/CC can/MD not/RB truly/RB be/AUX compared/VBN ./. </C>
</S>
<S>
<C>Furthermore/RB ,/, he/PRP states/VBZ </C>
<C>if/IN you/PRP were/AUX fired/VBN it/PRP would/MD be/AUX for/IN being/AUXG incompetent/JJ rather/RB than/IN being/AUXG gay/JJ ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ that/IN the/DT questions/NNS that/IN the/DT GUEST/NN is/AUX asking/VBG are/AUX not/RB relevant/JJ enough/RB to/TO answer/VB them/PRP ./. </C>
</S>
<S>
<C>In/IN the/DT end/NN S1/NNP states/VBZ that/IN gay/JJ activist/NN will/MD soon/RB find/VB out/RP why/WRB the/DT behavior/NN and/CC thoughts/NNS are/AUX what/WP they/PRP are/AUX ./. </C>
</S>
<S>
<C>The/DT both/DT argue/VBP that/IN each/DT one/NN 's/AUX wrong/JJ ./. </C>
</S>
</P>
</T>
