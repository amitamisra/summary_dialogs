<T>
<P>
</P>
<P>
<S>
<C>S1/NNP feels/VBZ that/IN the/DT silent/JJ majority/NN in/IN Ohio/NNP spoke/VBD out/RP about/IN the/DT issue/NN of/IN the/DT state/NN 's/POS passing/NN of/IN a/DT gay/JJ rights/NNS amendment/NN ,/, </C>
<C>and/CC compares/VBZ it/PRP to/TO the/DT large/JJ percentage/NN of/IN voters/NNS in/IN past/JJ presidential/JJ elections/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN the/DT morals/NNS and/CC beliefs/NNS in/IN the/DT state/NN were/AUX the/DT reasons/NNS for/IN the/DT Republican/JJ presidential/JJ candidate/NN ,/, George/NNP Bush/NNP ,/, winning/VBG by/IN a/DT larger/JJR percentage/NN than/IN either/DT democratic/JJ candidate/NN ,/, John/NNP Kerry/NNP and/CC Bill/NNP Clinton/NNP ,/, in/IN the/DT past/JJ elections/NNS ./. </C>
</S>
<S>
<C>S2/NNP rebuts/VBZ that/IN the/DT majority/NN of/IN people/NNS in/IN Ohio/NNP were/AUX not/RB silent/JJ about/IN their/PRP$ views/NNS on/IN gay/JJ marriage/NN or/CC on/IN their/PRP$ choice/NN for/IN president/NN ;/: </C>
<C>the/DT people/NNS in/IN that/DT state/NN were/AUX very/RB vocal/JJ about/IN their/PRP$ views/NNS ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN Bush/NNP did/AUX win/VB by/IN a/DT larger/JJR percentage/NN than/IN Clinton/NNP ,/, </C>
<C>the/DT increase/NN in/IN population/NN was/AUX the/DT determining/VBG factor/NN for/IN those/DT numbers/NNS ./. </C>
</S>
<S>
<C>He/PRP feels/VBZ that/IN majority/NN 's/POS desire/NN to/TO oppress/VB the/DT minority/NN ,/, in/IN Ohio/NNP ,/, did/AUX play/VB some/DT factor/NN in/IN Bush/NNP 's/POS win/NN ./. </C>
</S>
<S>
<C>He/PRP uses/VBZ percentages/NNS from/IN various/JJ candidates/NNS ,/, </C>
<C>likeClinton/NN ,/, Bush/NNP ,/, Dole/NNP ,/, Kerry/NNP and/CC Gore/NNP to/TO back/VB up/RP his/PRP$ argument/NN ./. </C>
</S>
<S>
<C>He/PRP disagrees/VBZ that/IN Bush/NNP was/AUX more/RBR supported/VBN than/IN Clinton/NNP ./. </C>
</S>
</P>
</T>
