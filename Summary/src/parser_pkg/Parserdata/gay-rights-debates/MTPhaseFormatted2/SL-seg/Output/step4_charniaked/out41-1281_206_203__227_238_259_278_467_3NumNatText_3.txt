(S1 (NP (NNP <PARAGRAPH>)))

(S1 (FRAG (NP (NNP S1)) (: :) (S (NP (DT All) (NNS laws)) (VP (AUX are) (VP (VBN based) (PP (IN on) (NP (NNS morals)))))) (: -) (NP (NP (NNP Christian) (NNS morals)) (, ,) (SBAR (IN as) (S (NP (DT the) (NNP U.S.)) (VP (AUX was) (VP (VBN founded) (PP (IN as) (NP (DT a) (NNP Christian) (NN country)))))))) (. .)))

(S1 (S (PP (IN By) (S (VP (VBG changing) (NP (NN family) (NNS standards)) (PP (IN by) (S (VP (VBG allowing) (NP (JJ gay) (NN marriage)))))))) (, ,) (NP (NP (DT the) (NN integrity)) (PP (IN of) (NP (NN heterosexual) (NN marriage) (CC and) (NN family) (NN structure)))) (VP (AUX is) (ADVP (RB no) (RB longer)) (ADJP (JJ stable))) (. .)))

(S1 (S (S (S (NP (NP (NN Prostitution)) (, ,) (NP (JJ polygamy) (, ,) (JJ human) (NN trafficking)) (, ,) (NP (NN gambling)) (CC and) (NP (NN drug) (NN distribution))) (VP (AUX are) (NP (NP (DT all) (`` ``) (JJ consensual) (NNS acts) ('' '')) (PP (IN as) (NP (JJ gay) (NNS relations)))))) (VP (VBP claim) (S (VP (TO to) (VP (AUX be)))))) (: ;) (SQ (MD should) (NP (DT these)) (VP (AUX be) (VP (VBN legalized) (ADVP (RB too)))) (. ?))))

(S1 (S (S (NP (NN Homosexuality)) (VP (AUX is) (VP (VBN linked) (PP (TO to) (NP (NN crime)))))) (, ,) (CC and) (S (NP (JJ many)) (VP (VBP consider) (S (NP (NN homosexuality)) (VP (TO to) (VP (AUX be) (ADJP (JJ harmful))))))) (. .)))

(S1 (S (NP (NN Equality)) (VP (VBZ means) (S (VP (VP (VBG treating) (NP (NN everyone)) (NP (DT the) (JJ same))) (, ,) (CC but) (RB not) (VP (VBG treating) (NP (DT every) (NN behavior)) (NP (DT the) (JJ same)))))) (. .)))

(S1 (S (NP (DT The) (NNS people)) (VP (MD will) (VP (VB determine) (SBAR (WHNP (WP what)) (S (VP (AUX is) (PP (IN in) (NP (NP (DT the) (NN public) (POS 's)) (JJS best) (NN interest))) (S (VP (TO to) (VP (VB protect) (CC and) (VB ban))))))))) (. .)))

(S1 (S (S (FRAG (NP (NNP S2)) (: :) (WP What) (SBAR (IN if) (S (NP (NP (NN belief)) (PP (IN in) (NP (NNP God)))) (VP (VP (AUX was) (VP (ADVP (RB legally)) (VBN allowed))) (, ,) (CC but) (VP (VP (VBG worshiping) (PRT (RP in)) (NP (NNS churches))) (CC or) (ADVP (RB even)) (VP (VBG praying) (PP (IN in) (ADJP (JJ private) (SBAR (S (VP (AUX was) (ADJP (JJ illegal)))))))))))) (. ?)) (NP (NP (DT A) (NN parallel)) (PP (TO to) (NP (NP (JJ gay) (NN marriage)) (: -) (S (NP (NP (JJ gay) (NNS relations)) (PP (IN between) (NP (CD two)))) (VP (VBG consenting) (NP (NNS adults))))))) (VP (AUX is) (ADJP (JJ fine)))) (, ,) (CC but) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (RB not))) (. .)))

(S1 (S (S (NP (NP (NNS Acts)) (PP (IN like) (NP (NN prostitution) (CC and) (NN drug) (NN trafficking)))) (VP (VP (AUX are) (RB not) (NP (JJ equal) (NNS exchanges))) (, ,) (CC and) (VP (AUX are) (VP (VBN linked) (PP (TO to) (NP (NN crime))))))) (: ;) (S (NP (NN homosexuality)) (VP (AUX is) (RB not))) (. .)))

(S1 (S (NP (NN Equality)) (VP (VBZ means) (NP (JJ worshiping) (NN whomever)) (SBAR (S (NP (PRP you)) (VP (VBP want)))) (, ,) (SBAR (WHNP (RB not) (WP what)) (S (NP (DT the) (NN majority)) (VP (VBZ believes) (SBAR (S (VP (AUX is) (ADJP (JJ correct))))))))) (. .)))

