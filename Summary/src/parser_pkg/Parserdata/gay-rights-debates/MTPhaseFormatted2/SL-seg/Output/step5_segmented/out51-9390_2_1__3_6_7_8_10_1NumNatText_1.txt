<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ that/IN gay/JJ activists/NNS compare/VBP public/JJ disapproval/NN of/IN homosexuality/NN with/IN racial/JJ discrimination/NN ./. </C>
</S>
<S>
<C>This/DT person/NN does/AUX not/RB agree/VB that/IN this/DT is/AUX a/DT viable/JJ comparison/NN </C>
<C>because/IN they/PRP believe/VBP that/IN racial/JJ discrimination/NN is/AUX the/DT hatred/NN or/CC fear/NN of/IN another/DT human/JJ being/NN ,/, </C>
<C>while/IN the/DT other/JJ is/AUX just/RB disapproval/NN of/IN another/DT person/NN 's/POS behavior/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN employers/NNS base/VBP their/PRP$ employment/NN on/IN competency/NN and/CC not/RB sexuality/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB agree/VB that/IN being/AUXG homosexual/JJ is/AUX a/DT behavior/NN ,/, </C>
<C>and/CC that/IN it/PRP is/AUX within/IN the/DT rights/NNS under/IN current/JJ states/NNS '/POS laws/NNS for/IN employers/NNS to/TO let/VB their/PRP$ workers/NNS go/VB because/IN of/IN one/CD 's/POS sexuality/NN ./. </C>
</S>
<S>
<C>They/PRP believe/VBP this/DT is/AUX discrimination/NN and/CC not/RB just/RB disapproval/NN </C>
<C>because/IN the/DT law/NN supports/VBZ the/DT right/NN for/IN employers/NNS to/TO release/VB employees/NNS solely/RB on/IN the/DT fact/NN that/IN they/PRP are/AUX homosexual/JJ and/CC not/RB due/JJ to/TO competency/NN ./. </C>
</S>
</P>
</T>
