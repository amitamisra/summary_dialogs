(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (CD Two) (NNS people)) (VP (AUX are) (VP (VBG discussing) (NP (NNS laws) (CC and) (NNS politics)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (JJ homosexual) (NN sex)) (VP (AUX is) (NP (NP (DT a) (NN sin)) (CC and) (NP (DT a) (NN crime))))) (, ,) (CC and) (S (NP (EX there)) (VP (MD should) (VP (AUX be) (NP (NP (DT a) (JJ huge) (NN fine)) (PP (IN on) (NP (DT the) (NN behavior)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ retorts) (SBAR (IN that) (S (SBAR (IN while) (S (NP (NNP Christianity)) (VP (VBZ states) (NP (PRP$ its) (NNP a) (NN sin))))) (, ,) (NP (PRP it)) (VP (AUX is) (RB not) (NP (DT a) (NN crime)) (ADVP (RB anymore)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (DT the) (NNP US)) (VP (AUX is) (RB not) (NP (DT a) (NN theocracy)))) (, ,) (CC and) (S (NP (DT the) (NN freedom)) (ADJP (JJ available) (PP (IN in) (NP (NP (DT the)) (SBAR (S (NP (PRP US)) (VP (VBZ means) (SBAR (IN that) (S (NP (NP (DT the) (NN responsibility)) (PP (IN of) (S (VP (VBG appeasing) (NP (DT a) (NNP Christian) (NNP God) (NN cannot)))))) (VP (AUX be) (VP (VBN forced) (PP (IN on) (NP (DT the) (NN population))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ retorts) (SBAR (IN that) (S (NP (JJ liberal) (NN tolerance)) (VP (AUX is) (ADJP (JJ contradictory) (, ,) (JJ dogmatic) (, ,) (JJ intolerant) (CC and) (JJ coercive)) (SBAR (IN as) (S (NP (PRP it)) (VP (ADVP (RB only)) (VBZ allows) (PP (IN for) (NP (CD one) (JJ correct) (NN view)))))))))) (. .)))

(S1 (S (S (VP (TO To) (VP (VB contradict)))) (, ,) (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT the) (`` ``) (JJ liberal) ('' '') (NN tolerance)) (ADVP (RB still)) (VP (VBZ allows) (PP (IN for) (NP (NP (DT the) (JJ free) (NN practice)) (PP (IN of) (NP (NN religion))))))))) (. .)))

