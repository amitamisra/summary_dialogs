<T>
<P>
</P>
<P>
<S>
<C>S1/NNP brings/VBZ up/RP a/DT quiz/NN from/IN The/DT New/NNP York/NNP Times/NNP that/WDT supposedly/RB determines/VBZ how/WRB you/PRP compare/VBP to/TO the/DT current/JJ Supreme/NNP Court/NNP Justices/NNS on/IN Supreme/NNP Court/NNP issues/NNS ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN the/DT problem/NN with/IN the/DT quiz/NN is/AUX that/IN it/PRP is/AUX based/VBN on/IN people/NNS 's/POS opinions/NNS </C>
<C>and/CC he/PRP does/AUX not/RB believe/VB that/IN the/DT Supreme/NNP Court/NNP should/MD make/VB rulings/NNS based/VBN on/IN their/PRP$ opinions/NNS </C>
<C>because/IN they/PRP are/AUX supposed/VBN to/TO be/AUX ruling/VBG entirely/RB based/VBN on/IN their/PRP$ interpretion/NN of/IN the/DT constitution/NN ./. </C>
</S>
<S>
<C>S2/NNP agrees/VBZ with/IN S1/NNP that/IN the/DT Supreme/NNP Court/NNP decisions/NNS should/MD not/RB be/AUX described/VBN as/IN conservative/JJ or/CC liberal/JJ </C>
<C>because/IN political/JJ viewpoints/NNS should/MD be/AUX irrelevant/JJ to/TO interpreting/VBG the/DT constitution/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB provides/VBZ an/DT example/NN of/IN one/CD Supreme/NNP Court/NNP Justice/NN who/WP is/AUX known/VBN for/IN making/VBG rulings/NNS that/WDT do/AUX not/RB appeal/VB to/TO either/DT political/JJ party/NN ./. </C>
</S>
</P>
</T>
