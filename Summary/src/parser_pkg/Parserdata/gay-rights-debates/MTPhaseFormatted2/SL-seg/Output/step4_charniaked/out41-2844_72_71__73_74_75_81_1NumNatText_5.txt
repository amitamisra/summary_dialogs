(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNPS Republicans)) (VP (AUX are) (VP (VBN focused) (PP (IN on) (NP (JJ unimportant) (NNS issues))) (PP (IN by) (S (VP (VBG appealing) (PP (TO to) (NP (NP (NP (NNS people) (POS 's)) (NNS prejudices)) (CONJP (RB instead) (IN of)) (NP (DT the) (JJ important) (NNS ones)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (DT the) (NNS issues)) (PP (IN of) (NP (NNPS Democrats)))) (VP (AUX are) (NP (NP (JJ important) (NNS issues)) (, ,) (PP (JJ such) (IN as) (NP (NP (JJ social) (NN security)) (, ,) (NP (JJ global) (NN warming)) (, ,) (NP (JJ poor) (NNS schools)) (CC and) (NP (NN terrorism))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNPS Republicans)) (ADVP (RB only)) (VP (VB care) (PP (IN about) (NP (NN tax) (NNS cuts))) (PP (IN for) (NP (DT the) (JJ rich))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT the) (NNPS Democrats)) (VP (AUX are) (VP (VBG manipulating) (NP (DT the) (NN system)) (ADVP (RB unfairly))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (IN that) (S (NP (DT the) (NNPS Democrats)) (VP (AUX are) (VP (VBG pushing) (NP (DT an) (NN extremist) (NN agenda))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNPS Democrats)) (VP (AUX were) (ADJP (ADVP (RBR more) (RB fiscally)) (JJ responsible) (PP (IN by) (S (VP (VBG balancing) (NP (DT the) (NN budget)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (S (NP (PRP it)) (VP (AUX was) (NP (NP (DT a) (JJ Republican) (NN house)) (SBAR (WHNP (WDT that)) (S (S (VP (VP (VBN balanced) (NP (DT the) (NN budget))) (CC but) (VP (AUX was) (VP (VBN hindered) (PP (IN by) (NP (NNP Democrat))))))) (VP (VBD created) (NP (NN legacy) (NNS costs)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ claims) (SBAR (IN that) (S (NP (DT the) (NN left)) (ADVP (RB always)) (VP (VBZ claims) (SBAR (S (NP (NNPS Republicans)) (VP (VBP want) (S (VP (TO to) (VP (VB starve) (NP (DT the) (JJ elderly) (CC and) (NN school) (NNS children)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VP (VBZ claims) (SBAR (IN that) (S (NP (DT the) (NNS judges)) (VP (AUX were) (VP (VP (VBG doing) (ADVP (RB there)) (NP (NNS jobs))) (CC and) (VP (VBG interpreting) (NP (DT the) (NN constitution)))))))) (CC and) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNPS Republicans)) (VP (VBP try) (S (VP (TO to) (VP (VB force) (NP (JJ religious) (NNS rules)) (PP (IN on) (NP (NN everyone))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ wants) (S (NP (NN marriage)) (VP (TO to) (VP (RB not) (VP (AUX be) (VP (VBN messed) (PP (IN with)))) (CC and) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (PRP$ its) (JJ legal)) (PP (IN for) (NP (JJ different) (NNS states)))) (VP (TO to) (VP (AUX have) (NP (JJ different) (NNS standards))))))))))) (. .)))

(S1 (S (NP (JJ S2) (NNS references)) (VP (NN driver) (NP (NP (NN licensing) (NNS laws)) (VP (VBG differing) (PP (IN between) (NP (NP (NNS states)) (PP (IN as) (NP (NN evidence))))))) (PP (IN for) (NP (PRP$ his) (NN opinion)))) (. .)))

