(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (NP (JJ gay) (NN marriage)))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ opposes) (NP (NP (RB not) (RB only) (JJ gay) (ADJP (NN marriage) (CC but) (JJ homosexual)) (NNS relationships)) (PP (IN in) (ADJP (JJ general)))) (SBAR (IN while) (S (NP (NNP S2)) (VP (AUX is) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (DT both))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (S (NP (NP (JJ sexual) (NNS relations)) (PP (IN between) (NP (CD two) (NNS men)))) (VP (VP (AUX is) (ADJP (JJ deviant)) (NP (NN sex))) (CC and) (VP (VBZ compares) (NP (NP (DT the) (NN legalization)) (PP (IN of) (NP (NP (JJ same) (NN sex) (NN marriage)) (PP (TO to) (NP (NN pedophilia))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ advises) (SBAR (S (NP (PRP she)) (VP (AUX is) (NP (NP (DT a) (JJ straight) (NN female)) (SBAR (WHNP (WP who)) (S (VP (AUX is) (, ,) (PP (IN in) (NP (NN fact))) (, ,) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (NP (JJ gay) (NN marriage)) (PP (IN in) (NP (NP (NN answer)) (PP (TO to) (NP (NP (NNP S1) (POS 's)) (NN theory) (SBAR (IN that) (S (NP (NP (JJS most) (NNS people)) (PP (IN in) (NP (NN society))) (SBAR (WHNP (WP who)) (S (VP (AUX are) (RB not) (ADJP (JJ gay)))))) (VP (AUX do) (RB not) (VP (VB support) (NP (PRP it)))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (ADVP (RB also)) (VP (VBZ advises) (SBAR (S (NP (PRP$ her) (NN son)) (VP (AUX has) (NP (NP (NP (DT a) (NN day)) (JJ adoptive) (NN father)) (SBAR (WHNP (WP who)) (S (VP (AUX is) (NP (DT a) (ADJP (RB very) (JJ good)) (NN parent)))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ adheres) (PP (TO to) (NP (DT the) (NN opinion) (SBAR (IN that) (S (S (NP (NP (DT those)) (PP (IN in) (NP (NP (NN support)) (PP (IN of) (NP (JJ gay) (NNS unions)))))) (VP (AUX are) (NP (DT the) (NN minority)))) (CC and) (S (NP (JJS most) (NNS people)) (VP (AUX do) (RB not) (VP (VB trust) (NP (NNS gays)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (AUX does) (RB not) (VP (VB agree))) (. .)))

