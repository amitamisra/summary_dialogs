(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ thinks) (SBAR (S (NP (PRP it)) (VP (AUX does) (RB not) (VP (VB matter) (SBAR (IN if) (S (NP (NN anyone)) (VP (AUX is) (VP (VBN harmed) (PP (IN by) (NP (NP (JJ gay) (NN marriage)) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ different) (PP (IN than) (NP (DT the) (JJ basic) (NN structure)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (S (NP (NN society)) (VP (MD could) (RB not) (VP (VB survive) (PP (IN without) (NP (JJ heterosexual) (NN marriage)))))) (CC but) (S (NP (PRP it)) (VP (MD can))) (PP (IN without) (NP (JJ gay) (NN marriage)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (JJ real) (NN issue)) (VP (AUX is) (SBAR (WHADVP (WRB where)) (S (VP (TO to) (VP (VB draw) (NP (NP (DT the) (NNS lines)) (PP (IN on) (NP (NN marriage) (NNS laws)))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (NN society)) (VP (MD could) (VP (VB survive) (PP (IN without) (NP (NP (JJ many)) (PP (IN of) (NP (NP (DT the) (NNS rights)) (SBAR (S (NP (PRP it)) (VP (AUX has)))))))))))) (CC and) (SBAR (IN that) (S (NP (NN argument)) (VP (AUX is) (ADVP (RB really)) (NP (NP (DT a) (NN way)) (PP (IN of) (S (VP (VBG discriminating) (PP (IN against) (NP (NNS minorities))))))) (, ,) (SBAR (RB therefore) (S (NP (PRP it)) (VP (AUX is) (RB not) (NP (DT an) (NN argument)))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VP (VBZ agrees) (SBAR (S (NP (DT the) (NN issue)) (VP (AUX is) (SBAR (WHADVP (WRB where)) (S (VP (TO to) (VP (VB draw) (NP (DT the) (NN line)))))))))) (CC but) (VP (VBZ states) (SBAR (S (S (NP (PRP you)) (VP (AUX need) (NP (NP (NNS reasons)) (SBAR (S (NP (DT the) (NN line)) (VP (MD should) (VP (AUX be) (ADVP (RB there))))))))) (CC and) (S (NP (EX there)) (VP (AUX is) (RB not) (NP (NP (CD one)) (PP (IN for) (NP (JJ gay) (NN marriage)))))))))) (. .)))

