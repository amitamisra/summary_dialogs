(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (NNP S2)) (VP (VBZ wants) (S (NP (NNP Bert) (CC and) (NNP Ernie)) (VP (TO to) (VP (AUX be) (ADJP (JJ gay)))))))) (CC and) (SBAR (IN that) (S (NP (DT this)) (VP (MD will) (VP (VB sexualize) (NP (PRP them)) (PP (TO to) (NP (NNS children))) (SBAR (IN because) (S (NP (JJ gay) (NNS men)) (VP (VBP engage) (PP (IN in) (NP (`` ``) (JJ deviant) ('' '') (JJ sexual) (NN behavior)))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (PRP you)) (VP (MD could) (VP (AUX have) (S (NP (NNP Bert) (CC and) (NNP Ernie)) (VP (VP (AUX be) (ADJP (JJ straight))) (CC and) (VP (VB get) (VP (VBN married)))))))) (CC and) (S (NP (DT this)) (VP (MD will) (RB not) (VP (VB sexualize) (NP (PRP them)))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNP S2)) (VP (VBZ wants) (S (NP (NNP Bert) (CC and) (NNP Ernie)) (VP (TO to) (VP (AUX be) (VP (VBN described) (PP (TO to) (NP (NNS children))) (PP (IN as) (S (VP (VBG engaging) (PP (PP (IN in) (NP (JJ deviant) (JJ sexual) (NN behavior))) (CC but) (PP (IN under) (NP (NP (DT the) (NN guise)) (PP (IN of) (ADJP (`` ``) (JJ married) ('' ''))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ points) (PRT (RP out)) (SBAR (SBAR (IN that) (S (NP (DT the) (NN show)) (VP (AUX is) (NP (NP (DT a) (NN work)) (PP (IN of) (NP (NN fiction))))))) (CC and) (SBAR (IN that) (S (NP (PRP he)) (ADVP (RB merely)) (VP (VBD said) (NP (NN marriage))))) (CC and) (SBAR (IN that) (S (NP (NNP S1)) (VP (AUX is) (SBAR (WHNP (WP who)) (S (VP (VBD brought) (NP (NN sex) (NNS acts)) (PP (IN into) (NP (DT the) (NN equation))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (JJ straight) (NNS people)) (VP (AUX are) (VP (VBN portrayed) (PP (TO to) (NP (NNS children)))))) (CC and) (S (NP (PRP it)) (VP (AUX has) (NP (NP (NN nothing)) (SBAR (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (PRP$ their) (NN sex) (NN life))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB merely)) (VP (VBZ thinks) (SBAR (IN that) (S (S (VP (AUXG having) (NP (NP (CD two) (JJ male) (NNS characters)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (VP (VBN married)))))))) (VP (MD will) (VP (VB help) (S (VP (VB support) (NP (JJ gay) (NN inclusion))))))))) (. .)))

