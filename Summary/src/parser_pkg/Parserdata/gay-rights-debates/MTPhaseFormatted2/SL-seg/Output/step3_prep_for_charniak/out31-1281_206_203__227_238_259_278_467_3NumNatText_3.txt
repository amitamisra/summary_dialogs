<s> <PARAGRAPH> </s>
<s>  S1: All laws are based on morals - Christian morals, as the U.S. was founded as a Christian country. </s>
<s> By changing family standards by allowing gay marriage, the integrity of heterosexual marriage and family structure is no longer stable. </s>
<s> Prostitution, polygamy, human trafficking, gambling and drug distribution are all "consensual acts" as gay relations claim to be; should these be legalized too? </s>
<s> Homosexuality is linked to crime, and many consider homosexuality to be harmful. </s>
<s> Equality means treating everyone the same, but not treating every behavior the same. </s>
<s> The people will determine what is in the public's best interest to protect and ban. </s>
<s> S2: What if belief in God was legally allowed, but worshiping in churches or even praying in private was illegal? A parallel to gay marriage - gay relations between two consenting adults is fine, but gay marriage is not. </s>
<s> Acts like prostitution and drug trafficking are not equal exchanges, and are linked to crime; homosexuality is not. </s>
<s> Equality means worshiping whomever you want, not what the majority believes is correct. </s>
<s>  </s>
