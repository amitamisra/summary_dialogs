<T>
<P>
</P>
<P>
<S>
<C>Two/CD people/NNS are/AUX discussing/VBG civil/JJ unions/NNS and/CC marriages/NNS ./. </C>
</S>
<S>
<C>S1/NNP contends/VBZ that/IN he/PRP favors/VBZ civil/JJ unions/NNS for/IN homosexuals/NNS but/CC not/RB marriage/NN ,/, </C>
<C>since/IN he/PRP is/AUX religious/JJ and/CC views/VBZ marriage/NN as/IN a/DT sacrament/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN he/PRP can/MD vote/VB using/VBG whatever/WDT criteria/NNS he/PRP feels/VBZ ,/, </C>
<C>and/CC though/IN the/DT government/NN can/MD not/RB establish/VB religion/NN or/CC give/VB special/JJ consideration/NN to/TO certain/JJ groups/NNS as/IN a/DT voter/NN he/PRP can/MD ./. </C>
</S>
<S>
<C>S2/NNP retorts/VBZ by/IN stating/VBG that/IN civil/JJ unions/NNS are/AUX not/RB equivalent/JJ to/TO marriages/NNS ./. </C>
</S>
<S>
<C>He/PRP states/VBZ that/IN the/DT US/NNP government/NN does/AUX not/RB consider/VB marriage/NN a/DT sacrament/NN </C>
<C>and/CC he/PRP states/VBZ S1/NNP is/AUX mixing/VBG religion/NN and/CC government/NN ./. </C>
</S>
<S>
<C>He/PRP also/RB states/VBZ that/IN the/DT majority/NN should/MD not/RB vote/VB on/IN the/DT civil/JJ rights/NNS of/IN another/DT group/NN that/WDT will/MD not/RB affect/VB them/PRP ,/, </C>
<C>which/WDT S1/NNP states/VBZ that/IN financially/RB it/PRP will/MD ./. </C>
</S>
</P>
</T>
