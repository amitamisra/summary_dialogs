(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ gay) (NNS marriages)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN viewed) (NP (DT the) (JJ same) (NN way)) (PP (IN as) (NP (JJ heterosexual) (NNS marriages)))))))))) (: ;) (S (NP (DT the) (NN speaker)) (VP (AUX does) (VP (VB state) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB disagree) (PP (IN with) (NP (NP (DT the) (NN idea)) (PP (IN of) (NP (JJ gay) (NN marriage)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (JJ gay) (NN marriage)) (VP (AUX is) (NP (NP (DT a) (NN deviation)) (PP (IN from) (NP (NP (DT the) (JJ traditional) (NN union)) (PP (IN between) (NP (DT a) (NN man) (CC and) (NN woman)))))))))) (. .)))

(S1 (S (NP (NP (DT Any) (JJ sexual) (NN relationship)) (SBAR (WHNP (WDT that)) (S (VP (AUX is) (NP (NP (DT a) (NN deviation)) (PP (IN from) (NP (DT the) (`` ``) (NN norm)))))))) (PRN (, ,) ('' '') (PP (IN like) (NP (NP (NN polygamist)) (CC and) (NP (JJ incestuous) (NNS relationships)))) (, ,)) (VP (MD should) (ADVP (RB also)) (VP (AUX be) (VP (VBN granted) (NP (DT the) (JJ same) (NNS rights))))) (. .)))

(S1 (S (S (NP (DT The) (NN speaker)) (VP (VBZ feels) (SBAR (IN that) (S (NP (DT these) (NNS relationships)) (VP (AUX are) (DT all) (PP (IN on) (NP (DT the) (JJ same) (NNS levels)))))))) (, ,) (CC and) (S (NP (PRP one)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN afforded) (NP (NNS rights)) (SBAR (IN that) (S (NP (DT the) (NNS others)) (VP (AUX are) (RB not)))))))) (. .)))

(S1 (S (S (NP (NNP S2)) (VP (VBZ identifies) (PP (IN as) (NP (NP (DT a) (JJ gay) (NN male)) (, ,) (SBAR (WHNP (WP who)) (S (VP (VP (AUX has) (VP (AUX been) (PP (IN in) (NP (DT a) (JJ 15-year-long) (JJ homosexual) (NN relationship))))) (, ,) (CC and) (VP (AUX has) (NP (NP (DT a) (NN set)) (PP (IN of) (NP (NNP twins)))) (ADVP (RB due) (RB soon)))))))))) (: ;) (S (NP (PRP he)) (VP (VBZ feels) (SBAR (S (NP (PRP$ his) (NN family)) (VP (MD should) (VP (AUX be) (VP (VBN granted) (NP (DT the) (JJ same) (NNS rights)) (PP (IN as) (NP (DT a) (JJ heterosexual) (, ,) (JJ married) (NN couple)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (S (VP (VBG legalizing) (NP (JJ gay) (NN marriage)))) (VP (VP (VBZ allows) (PP (IN for) (NP (NP (JJ automatic) (NN guardianship)) (PP (IN of) (NP (NNS children))))) (SBAR (IN if) (S (NP (NN something)) (VP (VBZ happens) (PP (TO to) (NP (DT the) (JJ biological) (NN parent))))))) (, ,) (CC and) (ADVP (RB also)) (VP (NNS awards) (NP (NNS survivors) (NNS benefits)) (PP (IN from) (NP (JJ social) (NN security)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (JJ other) (NNS types)) (PP (IN of) (NP (NN marriage)))) (VP (MD should) (VP (AUX be) (VP (VBN looked) (PP (IN at) (PP (VBN based) (PP (IN on) (NP (PRP$ its) (JJ own) (NNS situations) (CC and) (NNS conditions))))))))))) (. .)))

