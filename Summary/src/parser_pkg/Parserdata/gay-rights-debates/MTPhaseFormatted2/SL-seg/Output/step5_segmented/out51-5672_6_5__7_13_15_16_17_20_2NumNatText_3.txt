<T>
<P>
</P>
<P>
<S>
<C>S1/NNP does/AUX not/RB agree/VB that/IN amending/VBG the/DT Constitution/NNP is/AUX the/DT right/NN things/NNS to/TO do/AUX ,/, </C>
<C>though/IN they/PRP find/VBP the/DT argument/NN connecting/VBG society/NN and/CC traditional/JJ family/NN compelling/JJ regardless/RB ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN the/DT definition/NN of/IN the/DT words/NNS family/NN and/CC marriage/NN are/AUX being/AUXG broadened/VBD and/CC redefined/VBD ./. </C>
</S>
<S>
<C>They/PRP argue/VBP that/IN those/DT who/WP oppose/VBP this/DT change/NN believe/VBP that/IN marriage/NN and/CC family/NN fit/VBP a/DT certain/JJ criteria/NNS to/TO truly/RB be/AUX a/DT marriage/NN ,/, </C>
<C>and/CC that/IN criteria/NNS is/AUX traditional/JJ marriage/NN ./. </C>
</S>
<S>
<C>They/PRP agree/VBP on/IN some/DT points/NNS with/IN these/DT people/NNS ,/, </C>
<C>and/CC believe/VBP that/IN adopting/VBG homosexual/JJ marriage/NN will/MD have/AUX an/DT effect/NN on/IN that/DT definition/NN ,/, </C>
<C>but/CC they/PRP do/AUX not/RB on/IN others/NNS ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB believe/VB that/IN there/EX will/MD be/AUX an/DT effect/NN on/IN traditional/JJ marriage/NN because/IN of/IN the/DT adoption/NN of/IN homosexual/JJ marriages/NNS ./. </C>
</S>
<S>
<C>They/PRP believe/VBP that/IN the/DT traditional/JJ family/NN has/AUX its/PRP$ own/JJ problems/NNS and/CC has/AUX been/AUX in/IN decline/NN </C>
<C>long/RB before/IN the/DT issue/NN was/AUX brought/VBN up/RP ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB agree/VB that/IN connecting/VBG society/NN and/CC traditional/JJ family/NN to/TO homosexual/JJ marriage/NN is/AUX logical/JJ or/CC compelling/JJ </C>
<C>and/CC that/IN there/EX is/AUX reasonable/JJ evidence/NN for/IN the/DT contrary/NN ./. </C>
</S>
<S>
<C>They/PRP do/AUX not/RB believe/VB there/EX is/AUX substantial/JJ proof/NN to/TO argue/VB that/IN homosexual/JJ marriage/NN will/MD affect/VB society/NN ./. </C>
</S>
</P>
</T>
