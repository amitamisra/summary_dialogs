<T>
<P>
</P>
<P>
<S>
<C>S1/NNP claims/VBZ that/IN legalizing/VBG same-sex/JJ marriage/NN requires/VBZ redefining/VBG family/NN and/CC marriage/NN </C>
<C>and/CC that/IN it/PRP would/MD affect/VB traditional/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP references/NNS a/DT person/NN named/VBN Colson/NNP who/WP believes/VBZ that/IN same-sex/JJ households/NNS are/AUX deficient/JJ compared/VBN to/TO heterosexual/JJ households/NNS in/IN the/DT same/JJ way/NN that/IN a/DT single-parent/JJ household/NN is/AUX deficient/JJ compared/VBN to/TO a/DT two-parent/JJ household/NN ./. </C>
</S>
<S>
<C>There/EX is/AUX nothing/NN inherently/RB wrong/JJ with/IN the/DT deficient/JJ family/NN systems/NNS ,/, </C>
<C>but/CC they/PRP are/AUX not/RB ideal/JJ ./. </C>
</S>
<S>
<C>S1/NNP notes/VBZ that/IN he/PRP does/AUX sometimes/RB agree/VB with/IN gay/JJ rights/NNS supporters/NNS ,/, for/IN example/NN ,/, believing/VBG that/IN the/DT constitutional/JJ amendment/NN banning/VBG same-sex/JJ marriage/NN is/AUX a/DT bad/JJ idea/NN ,/, </C>
<C>that/IN homosexuals/NNS do/AUX experience/VB violence/NN and/CC harassment/NN ,/, </C>
<C>and/CC that/IN homosexuals/NNS should/MD not/RB be/AUX discriminated/VBN against/IN when/WRB it/PRP comes/VBZ to/TO adoption/NN ,/, housing/NN ,/, or/CC employment/NN ./. </C>
</S>
<S>
<C>S2/NNP rejects/VBZ the/DT comparison/NN of/IN same-sex/JJ households/NNS to/TO single-parent/JJ households/NNS </C>
<C>by/IN noting/VBG that/IN there/EX is/AUX not/RB a/DT constitutional/JJ amendment/NN to/TO ban/VB single-parenthood/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN S1/NNP 's/POS inconsistent/JJ arguments/NNS in/IN favor/NN of/IN and/CC against/IN same-sex/JJ marriage/NN make/VBP it/PRP doubtful/JJ </C>
<C>whether/IN he/PRP actually/RB does/AUX support/VB gay/JJ rights/NNS ,/, </C>
<C>and/CC he/PRP suggests/VBZ that/IN it/PRP is/AUX possible/JJ that/IN S1/NNP is/AUX being/AUXG paid/VBN to/TO post/VB on/IN the/DT forum/NN ./. </C>
</S>
</P>
</T>
