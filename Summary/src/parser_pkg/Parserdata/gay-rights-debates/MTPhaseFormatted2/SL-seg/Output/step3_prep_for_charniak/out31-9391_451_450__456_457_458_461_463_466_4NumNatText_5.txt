<s> <PARAGRAPH> </s>
<s>  S1 believes marriage is a contract between the state and two people. </s>
<s> He compares it to forming a corporation. </s>
<s> He finds it ironic Christians are against the religious aspects of marriage yet, because they are unable to stop religious marriage they want to stop the contract. </s>
<s> He believes he is forced to recognize heterosexual marriage and others should recognize gay marriages. </s>
<s> He does not believe rights should be voted on and gives an example of slavery. </s>
<s> S2 does not think anyone is harmed by gay marriage but believes official policies are everyone's business. </s>
<s> Heterosexual marriage is the status quo, so it is thus recognized. </s>
<s> Any changes to the status quo should be justified. </s>
<s> He does not see marriage as a right an example being unmarried people. </s>
<s>  </s>
