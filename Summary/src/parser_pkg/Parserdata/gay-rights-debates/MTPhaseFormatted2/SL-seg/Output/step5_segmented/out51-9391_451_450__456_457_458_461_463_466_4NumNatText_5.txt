<T>
<P>
</P>
<P>
<S>
<C>S1/NNP believes/VBZ marriage/NN is/AUX a/DT contract/NN between/IN the/DT state/NN and/CC two/CD people/NNS ./. </C>
</S>
<S>
<C>He/PRP compares/VBZ it/PRP to/TO forming/VBG a/DT corporation/NN ./. </C>
</S>
<S>
<C>He/PRP finds/VBZ it/PRP ironic/JJ Christians/NNS are/AUX against/IN the/DT religious/JJ aspects/NNS of/IN marriage/NN yet/RB ,/, </C>
<C>because/IN they/PRP are/AUX unable/JJ to/TO stop/VB religious/JJ marriage/NN they/PRP want/VBP to/TO stop/VB the/DT contract/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ he/PRP is/AUX forced/VBN to/TO recognize/VB heterosexual/JJ marriage/NN </C>
<C>and/CC others/NNS should/MD recognize/VB gay/JJ marriages/NNS ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB believe/VB rights/NNS should/MD be/AUX voted/VBN on/RP and/CC gives/VBZ an/DT example/NN of/IN slavery/NN ./. </C>
</S>
<S>
<C>S2/NNP does/AUX not/RB think/VB anyone/NN is/AUX harmed/VBN by/IN gay/JJ marriage/NN </C>
<C>but/CC believes/VBZ official/JJ policies/NNS are/AUX everyone/NN 's/POS business/NN ./. </C>
</S>
<S>
<C>Heterosexual/JJ marriage/NN is/AUX the/DT status/FW quo/FW ,/, </C>
<C>so/CC it/PRP is/AUX thus/RB recognized/VBN ./. </C>
</S>
<S>
<C>Any/DT changes/NNS to/TO the/DT status/NN quo/NN should/MD be/AUX justified/JJ ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB see/VB marriage/NN as/IN a/DT right/RB an/DT example/NN being/AUXG unmarried/VBD people/NNS ./. </C>
</S>
</P>
</T>
