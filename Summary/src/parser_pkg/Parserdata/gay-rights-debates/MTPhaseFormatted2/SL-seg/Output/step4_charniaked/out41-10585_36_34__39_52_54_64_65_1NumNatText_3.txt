(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (SBAR (IN that) (S (NP (NNP Bert) (CC and) (NNP Ernie)) (VP (AUX are) (ADJP (JJ homosexual))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS homosexuals) (NNS cannot)) (VP (VBP understand) (SBAR (IN that) (S (NP (CD two) (NNS men)) (VP (MD can) (VP (AUX be) (S (VP (VB close) (NP (NNS friends)) (PP (IN without) (S (VP (VBG desiring) (NP (NN sex)) (PP (IN with) (NP (DT each) (JJ other)))))))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN person)) (VP (VBZ believes) (SBAR (IN that) (S (NP (PRP it)) (VP (AUX was) (RB not) (NP (NP (NP (DT the) (NNS creators) (POS ')) (NNS intentions)) (SBAR (IN for) (S (NP (PRP it)) (VP (TO to) (VP (AUX be) (VP (VBN taken) (NP (DT that) (NN way)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (S (NP (NNS homosexuals)) (VP (VBP want) (S (VP (TO to) (VP (VB interject) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP believe) (S (VP (TO to) (VP (AUX be) (NP (NP (JJ deviant) (JJ sexual) (NNS practices)) (PP (IN into) (NP (NP (NNS children) (POS 's)) (NN programming))))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN distinction)) (PP (IN between) (NP (UCP (NN homosexuality) (CC and) (JJ sexual)) (NNS relations)))))) (, ,) (CC and) (SBAR (IN that) (S (NP (CD one) (NN cannot)) (VP (AUX be) (ADJP (JJ homosexual)) (PP (IN without) (S (VP (VBG practicing) (NP (NP (NN intercourse)) (PP (IN with) (NP (NP (CD one) (POS 's)) (JJ own) (NN gender)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (WHADVP (WRB when)) (S (NP (NNS people)) (VP (VB discuss) (NP (JJ homosexual) (NN marriage) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (VP (VBG trying) (S (VP (TO to) (VP (VB disguise) (SBAR (WHNP (WP what)) (S (NP (PRP they)) (VP (VBP believe) (S (VP (TO to) (VP (AUX be) (NP (JJ deviant) (JJ sexual) (NN activity)) (PP (IN by) (S (VP (VBG using) (NP (DT the) (NN word) (NN marriage))))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ argues) (PP (IN against) (NP (NNP S1))) (PP (IN by) (S (VP (VBG stating) (SBAR (IN that) (S (NP (JJ heterosexual) (NNS marriages)) (VP (MD can) (VP (AUX be) (VP (VP (VBN portrayed) (PP (IN without) (S (NP (JJ sexual) (NNS relations)) (VP (AUXG being) (VP (VBD alluded) (PP (TO to))))))) (CC or) (VP (VBN mentioned))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP believe) (SBAR (IN that) (S (NP (NNS heterosexuals) (CC and) (NNS homosexuals)) (VP (MD can) (VP (VB discuss) (NP (NN marriage)) (PP (IN without) (S (ADVP (RB ever)) (VP (VP (VBG needing) (S (VP (TO to) (VP (VB get) (PP (IN into) (NP (NP (DT the) (NNS details)) (PP (IN of) (NP (JJ sexual) (NN activity))))))))) (CC or) (VP (ADVP (RB even)) (VBG touching) (NP (NP (DT the) (NN topic)) (PP (IN of) (NP (JJ sexual) (NN activity))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (AUX do) (RB not) (VP (VB agree) (SBAR (IN that) (WHADVP (WRB when)) (S (NP (PRP one)) (VP (VBZ discusses) (NP (NN marriage)) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (ADVP (RB also)) (VP (VBG trying) (S (VP (TO to) (VP (VB discuss) (NP (JJ sexual) (NN activity)))))))))))))) (. .)))

