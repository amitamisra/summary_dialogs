(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NP (DT the) (JJS best) (NN way)) (SBAR (S (VP (TO to) (VP (VB legalize) (NP (JJ same) (NN sex) (NN marriage))))))) (VP (AUX is) (ADVP (RB through)) (VP (VBG lobbying)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (S (PP (IN in) (NP (DT any) (NN discrepancy))) (NP (DT both) (NNS sides)) (VP (MD should) (VP (VB lobby)))) (CC and) (S (NP (NP (DT the) (NN one)) (SBAR (WHNP (WP who)) (S (VP (AUX has) (NP (DT the) (NN majority) (NN support)))))) (VP (MD should) (VP (VB win))))))) (. .)))

(S1 (S (S (NP (NN Society)) (VP (AUX is) (VP (VBN run) (PP (IN by) (NP (DT the) (NN majority) (NN opinion)))))) (CC and) (S (NP (PRP he)) (VP (VBZ agrees) (SBAR (IN that) (S (NP (DT this)) (VP (AUX is) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (MD should) (VP (AUX be)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ agrees) (SBAR (SBAR (IN that) (S (NP (JJ same) (NN sex) (NN marriage)) (VP (MD should) (VP (AUX be) (ADJP (JJ legal)))))) (CC but) (SBAR (IN that) (S (NP (NP (DT the) (JJ only) (NN way)) (SBAR (S (NP (PRP it)) (VP (MD can) (CC and) (MD should) (VP (VB happen)))))) (VP (AUX is) (SBAR (WHADVP (WRB when)) (S (NP (DT the) (NN majority)) (VP (VBZ believes) (NP (PRP it)) (, ,) (SBAR (RB even) (IN if) (S (S (NP (NNS roles)) (VP (AUX were) (VP (VBN reversed)))) (CC and) (S (NP (PRP$ his) (NN relationship)) (VP (AUX was) (RB not) (ADJP (JJ able) (S (VP (TO to) (VP (AUX be) (VP (VBN acknowledged) (PP (IN as) (NP (DT a) (NN marriage)))))))))))))))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (NP (NNP S2)) (VP (VBZ compares) (NP (NN marriage) (NN inequality)) (PP (TO to) (NP (NN slavery)))))) (NP (PRP he)) (VP (VBP disagree) (SBAR (IN that) (S (NP (DT the) (NN comparison)) (VP (VP (AUX is) (ADJP (JJ valid))) (CC but) (VP (VBZ continues) (NP (PRP$ his) (NN argument)) (PP (IN for) (S (VP (VBG lobbying) (S (VP (VBG citing) (NP (NNP Wilberforce)) (PP (IN in) (NP (PRP$ his) (NN fight) (S (VP (TO to) (VP (VB abolish) (NP (NN slavery))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ attempts) (S (VP (TO to) (VP (VB appeal) (PP (TO to) (NP (NP (NNP S1) (POS 's)) (NNS emotions))) (PP (IN by) (S (ADVP (RB repeatedly)) (VP (VBG asking) (SBAR (IN that) (S (NP (PRP he)) (VP (VBD put) (NP (PRP himself)) (PP (IN in) (NP (NP (DT the) (NNS shoes)) (PP (IN of) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (AUX are) (VP (AUXG being) (VP (VBN denied) (NP (NP (DT the) (NN right)) (PP (TO to) (NP (NN marriage))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (NP (NP (NNP S1) (POS 's)) (NN ability) (S (VP (TO to) (VP (VP (VB feel) (NP (NN empathy))) (CC and) (VP (VBZ compares) (NP (NP (DT the) (NN issue)) (PP (IN of) (NP (NN marriage) (NN equality)))) (PP (TO to) (NP (NP (NN slavery)) (PP (IN in) (NP (VBZ regards))) (PP (TO to) (NP (NP (DT the) (NN majority)) (VP (VBG allowing) (S (NP (PRP it)) (VP (TO to) (VP (VB exist))))))))))))))) (. .)))

