<T>
<P>
</P>
<P>
<S>
<C>S1/NNP thinks/VBZ it/PRP does/AUX not/RB matter/VB </C>
<C>if/IN anyone/NN is/AUX harmed/VBN by/IN gay/JJ marriage/NN it/PRP is/AUX different/JJ than/IN the/DT basic/JJ structure/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ society/NN could/MD not/RB survive/VB without/IN heterosexual/JJ marriage/NN </C>
<C>but/CC it/PRP can/MD without/IN gay/JJ marriage/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN the/DT real/JJ issue/NN is/AUX where/WRB to/TO draw/VB the/DT lines/NNS on/IN marriage/NN laws/NNS ./. </C>
</S>
<S>
<C>S2/NNP argues/VBZ that/IN society/NN could/MD survive/VB without/IN many/JJ of/IN the/DT rights/NNS it/PRP has/AUX </C>
<C>and/CC that/IN argument/NN is/AUX really/RB a/DT way/NN of/IN discriminating/VBG against/IN minorities/NNS ,/, </C>
<C>therefore/RB it/PRP is/AUX not/RB an/DT argument/NN ./. </C>
</S>
<S>
<C>S2/NNP agrees/VBZ the/DT issue/NN is/AUX where/WRB to/TO draw/VB the/DT line/NN </C>
<C>but/CC states/VBZ you/PRP need/AUX reasons/NNS the/DT line/NN should/MD be/AUX there/RB </C>
<C>and/CC there/EX is/AUX not/RB one/CD for/IN gay/JJ marriage/NN ./. </C>
</S>
</P>
</T>
