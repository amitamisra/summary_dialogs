<s> <PARAGRAPH> </s>
<s>  The text begins with S1 saying "It's nothing like racial segregation". </s>
<s> But the statement he was responding to is not mentioned in the text. </s>
<s> The statement he was responding to is also not from the other writer ( S2), but from someone mentioned later as Addison. </s>
<s> S1 believes the other writer ( S2) has misunderstood his comment to Addison. </s>
<s> He does not believe his words had anything to do with denying the current plight of gay people is similar to the past plight of black people. </s>
<s> He believes the other writer is jumping to conclusions, possibly based on information in other posts. </s>
<s> S1 is offended by the other writer's use of the phrase, "It's nothing like racial segregation". </s>
<s> He believes the words are about the possibility of segregating gay people. </s>
<s> He the words imply that the segregation of gay people would either be nothing like the past treatment of black people, or would be less wrong. </s>
<s> He believes segregation throughout history is always the same, but people always deny the similarities. </s>
<s> This includes segregation according to race, religion, or sexual orientation. </s>
<s> At one point, he admits he may have interpreted the other writer's words incorrectly. </s>
<s>  </s>
