<s> <PARAGRAPH> </s>
<s>  S1 states that same-sex marriage should not be prohibited, but believes that a homosexual marriage is different than a heterosexual marriage. </s>
<s> He argues that recognizing this difference is not discrimination. </s>
<s> He claims that children of same-sex couples or comparable to children from incestuous, polygamous, or other sexual deviant relationships. </s>
<s> He makes the argument that if same-sex marriages are legalized, then all types of marriages between consenting adults should be legalized. </s>
<s> He claims that the arguments in favor of same-sex marriage would work equally well for incestuous or polygamous relationships. </s>
<s> S2 identifies himself as a homosexual and argues that same-sex families deserve the same benefits as heterosexual families. </s>
<s> He questions what difference exists that distinguishes his relationship from that of a heterosexual couple other than the genitals involved. </s>
<s> He identifies a variety of benefits received only through marriage, such as social security and guardianship among others. </s>
<s> He takes offense at S1's comment that his children are "outsourced" and rejects the idea that homosexuals are fighting for marriage equality simply because they free handouts from the government. </s>
<s>  </s>
