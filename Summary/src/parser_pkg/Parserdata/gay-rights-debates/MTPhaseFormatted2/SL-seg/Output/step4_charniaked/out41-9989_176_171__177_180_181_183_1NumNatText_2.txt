(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1) (CC and) (NNP S2)) (VP (AUX are) (VP (VBG discussing) (S (NP (NP (DT a) (NN quiz)) (SBAR (WHNP (WDT which)) (S (VP (AUX was) (VP (VBN run) (PP (IN on) (NP (DT the) (NNP New) (NNP York) (NNP Times)))))))) (VP (VB website) (PP (IN in) (NP (NP (NN regard)) (PP (TO to) (S (VP (VBG comparing) (NP (NP (NNS views)) (PP (IN of) (NP (DT the) (JJ general) (NN public)))) (PP (IN as) (PP (VBN compared) (PP (TO to) (NP (NP (DT those)) (PP (IN of) (NP (DT the) (NNP Supreme) (NNP Court)))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (DT the) (S (NP (DT the) (NN quiz)) (VP (AUX is) (ADJP (JJ horrible)) (SBAR (IN as) (S (NP (PRP it)) (, ,) (PP (IN in) (NP (NP (NNP S2) (POS 's)) (NN opinion))) (, ,) (VP (VBZ reinforces) (NP (DT the) (NN point) (SBAR (IN that) (S (NP (NNS people)) (VP (VBP view) (NP (DT the) (NN court)) (PP (IN as) (NP (DT a) (JJ legislative) (NN body)))))))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ feels) (SBAR (S (NP (DT the) (NN quiz)) (VP (VP (AUX is) (ADJP (JJ useful))) (CC but) (VP (AUX does) (VP (AUX have) (NP (DT some) (NNS issues)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NNS rulings)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN considered) (S (ADJP (JJ conservative) (CC or) (JJ liberal))))))))) (. .)))

(S1 (S (NP (NNP S1)) (VP (VBZ agrees) (PP (IN with) (NP (DT this) (NN point)))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ feels) (SBAR (S (NP (NN court) (NNS rulings)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN based) (PP (IN upon) (NP (NP (DT a) (NN judge) (POS 's)) (JJ personal) (NNS beliefs))) (SBAR (IN as) (S (NP (PRP they)) (VP (AUX are) (VP (VBN supposed) (S (VP (TO to) (VP (AUX be) (VP (VBN based) (PP (IN on) (NP (DT the) (NN constitution))))))))))))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ uses) (NP (NP (DT the) (NN example)) (PP (IN of) (NP (NP (NNS corporations)) (VP (AUXG being) (VP (VBN allowed) (S (VP (TO to) (VP (VB run) (NP (NN campaign) (NNS ads))))))))))) (. .)))

