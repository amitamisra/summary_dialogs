(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP S1)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (MD can) (VP (VB choose) (S (VP (TO to) (VP (VB ignore) (NP (NP (PRP$ their) (JJ sexual) (NNS impulses)) (SBAR (WHNP (WDT which)) (S (VP (AUX is) (ADJP (JJ different) (PP (IN than) (NP (NP (DT a) (NN person)) (VP (VBG controlling) (NP (PRP$ their) (NN ethnicity)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ claims) (SBAR (IN that) (S (SBAR (IN if) (S (NP (NNS homosexuals)) (VP (AUX do) (RB not) (VP (AUX have) (NP (NP (DT a) (NN choice)) (PP (IN about) (SBAR (IN whether) (CC or) (RB not) (S (VP (TO to) (VP (VB engage) (PP (IN in) (NP (JJ homosexual) (NN fornication))))))))))))) (, ,) (ADVP (RB then)) (NP (PRP they)) (VP (MD would) (VP (AUX be) (VP (ADVP (RB sexually)) (VBG assaulting) (NP (NNS people)))))))) (. .)))

(S1 (S (NP (NNP S2)) (VP (VBZ suggests) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (AUX do) (RB not) (VP (AUX have) (NP (NP (DT a) (NN choice)) (PP (IN about) (SBAR (WHNP (WP who)) (S (NP (PRP they)) (VP (AUX are) (VP (VBN attracted) (PP (TO to))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ clarifies) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (VP (VBG referring) (PP (TO to) (NP (NP (DT the) (NN lack)) (PP (IN of) (NP (NN choice))) (VP (VBG regarding) (NP (JJ sexual) (NN attraction))))) (, ,) (SBAR (IN that) (S (S (VP (AUXG being) (ADJP (JJ straight) (CC or) (JJ gay)))) (VP (AUX is) (RB not) (NP (NP (DT a) (NN choice)) (, ,) (CONJP (RB rather) (IN than)) (NP (NP (DT the) (NN choice)) (VP (VBG regarding) (SBAR (IN whether) (S (VP (TO to) (VP (VB engage) (PP (IN in) (NP (JJ homosexual) (NNS acts)))))))))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NP (NN response)) (PP (TO to) (NP (NNP S1))) (VP (VBG claiming) (SBAR (IN that) (S (NP (NNS homosexuals)) (VP (MD should) (RB not) (VP (VB engage) (PP (IN in) (NP (`` ``) (JJ illicit) (JJ gay) (NN sex)))))))))) (, ,) ('' '') (NP (NNP S2)) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB therefore)) (ADJP (JJ appropriate) (PP (IN for) (NP (DT a) (JJ homosexual)))) (S (VP (TO to) (VP (VB remain) (S (VP (VB celibate) (NP (PRP$ their) (JJ entire) (NNS lives))))))))))) (. .)))

