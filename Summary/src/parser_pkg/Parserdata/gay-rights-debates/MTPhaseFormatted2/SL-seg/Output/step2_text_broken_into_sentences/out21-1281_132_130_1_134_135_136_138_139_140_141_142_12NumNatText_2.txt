<PARAGRAPH> S1 and S2 are discussing the topic of gay marriage rights.
S1 states he is not exactly opposed to gay marriage but would like someone to give valid reasoning behind restructuring the basis of marriage since it's been the same since the beginning of time, comparing it to the Parthenon.
S2 feels it is circular to classify marriage as simply heterosexual.
He believes no one is attempting to take away the idea of marriage, just asking for homosexual couples to be included in the fray.
S1 disagrees with S2's statements that at the root, most homosexuals fighting for marriage rights are doing so on a conservative note.
He feels instead that the reasons are a "we are entitled to what you have" mentality.
S2 disagrees stating that while many do want acceptance from society, most want marriage for the basic reasons of marriage.
He also feels S1's arguments support the notion that laws should stay absolute and cannot be changed or altered.
S1 disagrees with this statement advising he simply wants the reasons supported.
