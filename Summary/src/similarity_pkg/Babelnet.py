'''
Created on Apr 12, 2015

@author: amita
'''
import urllib2
import urllib
import json
import gzip
from StringIO import StringIO
from nltk.corpus import wordnet as wn
from SPARQLWrapper import SPARQLWrapper, JSON

def getedges(synsetid,key):
    service_url = 'https://babelnet.io/v1/getEdges'
    params = {
        'id' : synsetid,
        'key'  : key
    }
    
    url = service_url + '?' + urllib.urlencode(params)
    request = urllib2.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    response = urllib2.urlopen(request)
    if response.info().get('Content-Encoding') == 'gzip':
        buf = StringIO( response.read())
        f = gzip.GzipFile(fileobj=buf)
        data = json.loads(f.read())
    
        # retrieving Edges data
        for result in data:
            target = result.get('target')
            language = result.get('language')
    
            # retrieving BabelPointer data
            pointer = result['pointer']
            relation = pointer.get('fName')
    
            print language.encode('utf-8') \
              + "\t" + str(target.encode('utf-8')) \
              + "\t" + str(relation.encode('utf-8'))
# get all synsets of a word 
def getsynset(word,pos,key):
    service_url = 'https://babelnet.io/v1/getSynsetIds'
    lang = 'EN'
    ListSynset=[]
    params = {
            'word' : word,
            'lang' : lang,
            'key'  : key,
            'pos'  : pos
    }
    
    url = service_url + '?' + urllib.urlencode(params)
    request = urllib2.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    response = urllib2.urlopen(request)
    
    if response.info().get('Content-Encoding') == 'gzip':
            buf = StringIO( response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read())
            for result in data:
                    ListSynset.append(result["id"])
                    
def getsynsetdata(synsetid,key):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
        SELECT ?narrower WHERE {
    <http://babelnet.org/rdf/s00000356n> a skos:Concept .
    OPTIONAL { <http://babelnet.org/rdf/s00000356n> skos:narrower ?narrower } } """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
     
    for result in results["results"]["bindings"]:
        print(result["narrower"]["value"])
                        
if __name__ == '__main__':
    listnoun=["country", "British", "law", "nation", "USD", "rules", "nations"]
    key="bddb072a-56a3-4b79-be13-00b7d7f12266"
    synsets =  wn.synsets('home') 
    wn._synset_from_pos_and_offset('n',03259505)
    synsetAll=[]
    for word in listnoun:
        synsetlist= getsynset(word,"NOUN",key)
        for synsetid in  synsetlist:
           getsynsetdata(synsetid,key) 
           synsetAll.append(synsetlist)
        