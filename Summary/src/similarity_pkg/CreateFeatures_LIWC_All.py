#!/usr/bin/env python
#coding: utf8 

'''
Created on Oct 16, 2014
This creates features for similarity among labels, takes input as a json formed from executing stanford parser followed by distributional 
similarity

RunCoreNLP_json.java. RunCoreNLP_json.java takes  input as more2scu.json
for each pair of strings
@author: amita
'''

from __future__ import division
from random import shuffle
from requests import get
from data_pkg import FileHandling
from file_formatting import csv_wrapper
import itertools 
import operator
import os
from collections import defaultdict
from operator import itemgetter
from  nltk.metrics import edit_distance
from nltk.corpus import stopwords
import nltk
from nltk.corpus import wordnet as wn
from  CompareStanfordDependencyRelations import GetCorrPair
from nltk.corpus import webtext
import numpy as np
import math
import sys
from collections import Counter
from pyrouge import Rouge155
from data_pkg import NewFormat_text
from collections import Counter
from operator import sub
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.snowball import SnowballStemmer
import word_category_counter
from copy import deepcopy 
from data_pkg.NewFormat_text import convertUnicode_to_dictlist as convertUnicode_to_dictlist
import ast


global  goldstandard
global NotToUseCategory

NotToUseCategory=["Unique Words","Words Per Sentence","Word Count","Dictionary Words","Common Verbs", "Six Letter Words", "Present Tense",\
                   "Total Function Words", "Word Count", "Total Pronouns","Present Tense","Adverbs", "Impersonal Pronouns", "Sentences","Space","Past Tense", "Auxiliary Verbs",\
                   "Quantifiers","Dash","Number","All Punctuation","Impersonal Pronouns","Personal Pronouns", "First Person Singular", "First Person Plural",\
                   "Second Person", "Third Person Singular", "Third Person Plural", "Articles", "Future Tense", "Total third person", "Total first person", \
                    "Impersonal Pronouns"]

LIWC_StopList=["'s","think","say","said","get","'ve","believe","saying"]


 

global fieldnames
fieldnames=["HITId","HITTypeId","UMBC_" ,"doccounta_", "doccountb_","keya_", "keyb_", "label_cluster_ ", "stringa_", "stringb_"]
class Features:
    def __init__(self,Input_Json,FeatureFile,RandomizeFeatureFile,TrainingTextFile,similarity_label,writedirRouge):
        self.Input_Json=Input_Json
        self.FeatureFile=FeatureFile
        self.TrainingTextFile=TrainingTextFile
        self.RandomizeFeatureFile=RandomizeFeatureFile
        self.UMBCError=[]
        self.similarity_label=similarity_label
        self.writedirRouge=writedirRouge
     
    def KeepSubVerbAdjAdv(self,WordDictList): 
        WordList=list()
        for worddict in WordDictList:
            if (worddict["pos"]).startswith( ("JJ","RB", "NN","VB") ):
                WordList.append(worddict)
        return   WordList      
           
    def ReadJson(self):
        InputRows=FileHandling.jsontorowdicts(self.Input_Json)
        return InputRows
   
    def buildVector(self,iterable1, iterable2):
        counter1 = Counter(iterable1)
        counter2= Counter(iterable2)
        all_items = set(counter1.keys()).union( set(counter2.keys()) )
        vector1 = [counter1[k] for k in all_items]
        vector2 = [counter2[k] for k in all_items]
        return (vector1 , vector2)

    def cosim(self,v1, v2):
        dot_product = sum(n1 * n2 for n1,n2 in zip(v1, v2) )
        magnitude1 = math.sqrt (sum(n ** 2 for n in v1))
        magnitude2 = math.sqrt (sum(n ** 2 for n in v2))
        if (magnitude1==0 or magnitude2==0):
            return 0
        return dot_product / (magnitude1 * magnitude2)

    
    def UnigramCosine(self,row,simlabel):
        XWordDict=row["Word_Dict"+simlabel+"a"]
        YWordDict=row["Word_Dict"+ simlabel+"b"]
        Xlist=list()
        Ylist=list()
        for wordx in XWordDict:
            if wordx["word"].lower() not in Stop_List:
                Xlist.append(wordx["lemma"] )
        for wordy in YWordDict:
            if wordy["word"] not in Stop_List:
                Ylist.append(wordy["lemma"] )
                
        X_Y=self.buildVector(Xlist,Ylist) 
        cosimunigram=self.cosim(X_Y[0],X_Y[1])
        row["cosunigram"]=cosimunigram
                       
        
    def bigramCosine(self,row,simlabel): 
        XWordDict=row["Word_Dict"+ simlabel + "a"]
        YWordDict=row["Word_Dict"+ simlabel + "b"]
        Xlistsorted = sorted(XWordDict, key=lambda x: int(operator.itemgetter("index")(x)))
        Ylistsorted=sorted(YWordDict, key=lambda x: int(operator.itemgetter("index")(x)))
        Xlist=[word["lemma"] for word in Xlistsorted]
        Ylist=[word["lemma"] for word in Ylistsorted ]
        
        Xbigram=list(nltk.bigrams(Xlist))
        Ybigram=list(nltk.bigrams(Ylist))
        X_Y=self.buildVector(Xbigram,Ybigram) 
        cosimbigram=self.cosim(X_Y[0],X_Y[1])
        row["cosbigram"]=cosimbigram
    
    def ExtNVACosine(self,Xlist,Ylist):
        X_Y=self.buildVector(Xlist,Ylist) 
        cosimunigram=self.cosim(X_Y[0],X_Y[1])
        return cosimunigram
    #Find common unigrams in pairs
    def ngramOverlap(self,row,NewRow,ngram, simlabel):
       # Uni_lemma_overlap_count=0
        #Uni_word_overlap_count=0
        Bi_lemma_overlap_count=0
        Bi_word_overlap_count=0
        Tri_lemma_overlap_count=0
        Tri_word_overlap_count=0
        common_word_unigram={}
        common_lemma_unigram={}
        XWordDict=row["Word_Dict"+ simlabel +"a"]
        XWordDict=ast.literal_eval(XWordDict)
        YWordDict=row["Word_Dict"+ simlabel +"b"]
        YWordDict=ast.literal_eval(YWordDict)
        Xlistsorted = sorted(XWordDict, key=lambda x: int(operator.itemgetter("index")(x)))
        Ylistsorted=sorted(YWordDict, key=lambda x: int(operator.itemgetter("index")(x)))
        Xlist_lemma=[word["lemma"] for word in Xlistsorted]
        Ylist_lemma=[word["lemma"] for word in Ylistsorted ]
        Xlist_word=[word["word"] for word in Xlistsorted]
        Ylist_word=[word["word"] for word in Ylistsorted ]
        if "1" in ngram:
            for i in Xlist_lemma:
                if i not in Stop_List:
                    common_lemma_unigram[i] = Ylist_lemma.count(i) 
            for i in Xlist_word:
                if i not in Stop_List:
                    common_word_unigram[i] = Ylist_word.count(i) 
            Uni_lemma_overlap_count=sum(common_lemma_unigram.itervalues()) 
            Uni_word_overlap_count= sum(common_word_unigram.itervalues())        
        
        if "2" in ngram:  
            common_lemma_bigram ={}
            common_word_bigram={}
            Xbigram_lemma=list(nltk.bigrams(Xlist_lemma))
            Ybigram_lemma=list(nltk.bigrams(Ylist_lemma))
            Xbigram_word=list(nltk.bigrams(Xlist_word))
            Ybigram_word=list(nltk.bigrams(Ylist_word))
            for i in Xbigram_lemma:
                common_lemma_bigram[i] = Ybigram_lemma.count(i) 
            for i in Xbigram_word:
                common_word_bigram[i] = Ybigram_word.count(i)
            Bi_lemma_overlap_count=sum(common_lemma_bigram.itervalues()) 
            Bi_word_overlap_count= sum(common_word_bigram.itervalues())        
        if "3" in ngram: 
            common_lemma_trigram ={}
            common_word_trigram={}
            Xtrigram_lemma=list(nltk.trigrams(Xlist_lemma))
            Ytrigram_lemma=list(nltk.trigrams(Ylist_lemma))
            Xtrigram_word=list(nltk.trigrams(Xlist_word))
            Ytrigram_word=list(nltk.trigrams(Ylist_word))
            for i in Xtrigram_lemma:
                common_lemma_trigram[i] = Ytrigram_lemma.count(i) 
            for i in Xtrigram_word:
                common_word_trigram[i] = Ytrigram_word.count(i)
            Tri_lemma_overlap_count=sum(common_lemma_trigram.itervalues()) 
            Tri_word_overlap_count= sum(common_word_trigram.itervalues()) 
                
        NewRow["Uni_lemma_overlap_count"]=Uni_lemma_overlap_count  
        NewRow["Bi_lemma_overlap_count"]= Bi_lemma_overlap_count    
        NewRow["Tri_lemma_overlap_count"]=Tri_lemma_overlap_count
       # return(Uni_lemma_overlap_count,Uni_word_overlap_count,Bi_lemma_overlap_count,Bi_word_overlap_count, Tri_lemma_overlap_count,Tri_word_overlap_count)           
    def LIWCOverlap(self, row,NewRow,simlabel):
        StringA=row[simlabel+"a"]
        StringB=row[simlabel+"b"]
        LIWCA=word_category_counter.score_text(StringA)
        LIWCB=word_category_counter.score_text(StringB)
        #=======================================================================
        # print LIWCA
        # print LIWCB  // used to test
        #=======================================================================
        for category in self.FinalLIWC :
            A_categ_count=LIWCA[category]
            B_categ_count=LIWCB[category]
            NewRow["LIWC_Catg_"+category]=min(A_categ_count,B_categ_count)
    #def WordNetSyn(self, word,pos):
    def Rougue(self,row,NewRow,simlabel, writedirRouge):
        r = Rouge155()
        count=0
        LinesA=list()
        StringA=NewFormat_text.ascii_only(row[simlabel+"a"])
        StringA=" I like Banana"
        Filename=writedirRouge+"System/"+simlabel+"_."+str(count)
        LinesA.append(StringA)
        FileHandling.WriteTextFile(Filename, LinesA)
        StringB=NewFormat_text.ascii_only(row[simlabel+"b"])
        StringB=" I like banana"
        LinesB=list()
        LinesB.append(StringB)
        Filename=writedirRouge+"Model/" + simlabel +"_.A."+str(count)
        FileHandling.WriteTextFile(Filename, LinesB)
        r.system_dir = writedirRouge+"System/"
        r.model_dir = writedirRouge+"Model/"
        r.system_filename_pattern = simlabel+'_.(\d+).txt'
        r.model_filename_pattern = simlabel+'_.[A-Z].#ID#.txt'
        output = r.convert_and_evaluate()
        output_dict = r.output_to_dict(output)
        NewRow["rouge_1_f_score"]=output_dict["rouge_1_f_score"]
        NewRow["rouge_2_f_score"]=output_dict["rouge_2_f_score"]
        NewRow["rouge_3_f_score"]=output_dict["rouge_3_f_score"]
        NewRow["rouge_4_f_score"]=output_dict["rouge_4_f_score"]
        NewRow["rouge_l_f_score"]=output_dict["rouge_l_f_score"]
        NewRow["rouge_s*_f_score"]=output_dict["rouge_s*_f_score"]
        NewRow["rouge_su*_f_score"]=output_dict["rouge_su*_f_score"]
        NewRow["rouge_w_1.2_f_score"]=output_dict["rouge_w_1.2_f_score"]
        
    def WordDiff(self,row,NewRow):
        Word_DictA=row["Word_Dictstringa_"]
        Word_DictB=row["Word_Dictstringb_"]
        NewRow["diff_len"]=abs(sub(len(Word_DictA), len(Word_DictB)))
            
    
    def Distributionalsimilarity(self,row,NewRow, simlabel): 
        Ext5Noun_a=(row["5ExtendedNoun_"+ simlabel +"a"]).split()
        lemmExt5Noun_a=list(set([lmtzr.stem(word) for word in Ext5Noun_a]))
        
        Ext5Noun_b=(row["5ExtendedNoun_"+ simlabel + "b"]).split()
        lemmExt5Noun_b=list(set([lmtzr.stem(word) for word in Ext5Noun_b]))
        
        Ext5Verb_a=(row["5ExtendedVerb_" + simlabel + "a"]).split()
        lemmExt5Verb_a=list(set([lmtzr.stem(word) for word in Ext5Verb_a]))
        
        Ext5Verb_b=(row["5ExtendedVerb_"+ simlabel + "b"]).split()
        lemmExt5Verb_b=list(set([lmtzr.stem(word) for word in Ext5Verb_b]))
        
        Ext5Adj_a=(row["5ExtendedAdj_"+ simlabel + "a"]).split()
        lemmExt5Adj_a=list(set([lmtzr.stem(word) for word in Ext5Adj_a]))
       
        Ext5Adj_b=(row["5ExtendedAdj_"+ simlabel + "b"]).split()
        lemmExt5Adj_b=list(set([lmtzr.stem(word) for word in Ext5Adj_b]))
        
        #=======================================================================
        # row["ExtNounsim"]=self.ExtNVACosine(lemmExt5Noun_a, lemmExt5Noun_b)
        # row["ExtVerbsim"]=self.ExtNVACosine(lemmExt5Verb_a, lemmExt5Verb_b)
        # row["ExtAdjsim"]=self.ExtNVACosine(lemmExt5Adj_a, lemmExt5Adj_b)
        #=======================================================================
        stem_a=lemmExt5Noun_a+ lemmExt5Verb_a+ lemmExt5Adj_a
        stem_b=lemmExt5Noun_b+ lemmExt5Verb_b+ lemmExt5Adj_b
        NewRow["Dist_Sim_Noun"]=self.ExtNVACosine(lemmExt5Noun_a, lemmExt5Noun_b)
        NewRow["Dist_Sim_Verb"]=self.ExtNVACosine(lemmExt5Verb_a, lemmExt5Verb_b)
        NewRow["Dist_Sim_Adj"]=self.ExtNVACosine(lemmExt5Adj_a, lemmExt5Adj_b)
        
        
    def AddLabel(self,row,NewRow)  :
        NewRow["MeanSimLabel"]=row["SimLabel"]
     
   # def Synsetsim(self,row,NewRow):
    
    #------------------------------------------ def wordNetSim(self,row,NewRow):
        #------------------------------------------------- Synsetsim(row,NewRow)
           
    def AddFeatures(self,row,NewRow,writedirRouge):
        FeatureList=["UMBC","LIWC","Ext_Dist","UnigramOverlap","bigramoverlap","Rougue"]
        
        
        if "LIWC" in FeatureList:
            self.LIWCOverlap(row,NewRow,self.similarity_label)
        
        if "Ext_Dist" in FeatureList:
            self.Distributionalsimilarity(row,NewRow,self.similarity_label)
        
        if "worddiff" in FeatureList:
            self.WordDiff(row,NewRow)
        
        
        if   "UnigramOverlap"  in FeatureList:
            ngram=["1","2","3"]
            self.ngramOverlap(row,NewRow,ngram,self.similarity_label)
        if "Rougue" in FeatureList:
            self.Rougue(row,NewRow,self.similarity_label,writedirRouge) 
        if "SynsetDistance" in FeatureList:
            print
            self.SynsetDistanceFeatures(row,NewRow)
        
        if  "bigramCosine"   in FeatureList:
            print
            self.bigramCosine(row,NewRow,self.similarity_label);
        if "LCS" in FeatureList:
            KeepSubVerbObj=True
            self.LCS(row, KeepSubVerbObj)
        if "LevDistance" in FeatureList:
            KeepSubVerbObj=True
            self.LevDistance(row,NewRow, KeepSubVerbObj)  
        if  "MeanSimilarityMT" in FeatureList:
            self.AddLabel(row,NewRow,self.similarity_label)
        if goldstandard==True:
            NewRow["gold"]=row["Id_A2TNNKHFNY5WQ4"]  
        
                
      
             
def createLIWCCategory(Featuresobj):  
    LIWCList=word_category_counter.Dictionary._liwc_categories
    keys=[LiwcCat[0] for LiwcCat in  LIWCList]
    FinalLiwc= [ word for word in keys if word not in NotToUseCategory]
    Featuresobj.FinalLIWC = FinalLiwc
             
    
def Execute(Featuresobj) :
    createLIWCCategory(Featuresobj)  
    Allrowdicts=list()
    Allrow_string=list()
    rows=Featuresobj.ReadJson() 
    
    for row in rows:
        Row_withString=dict()
        NewRow=dict()
        Featuresobj.AddFeatures(row,NewRow,writedirRouge)
        Row_withString= deepcopy(NewRow)
        Row_withString["stringa"]=row["stringa_"]
        Row_withString["stringb"]=row["stringb_"]
        Allrowdicts.append(NewRow)
        Allrow_string.append(Row_withString)
    fieldnames=sorted(Allrowdicts[0].keys())    
    csv_wrapper.write_csv(Featuresobj.FeatureFile, Allrowdicts) 
    csv_wrapper.write_csv(Featuresobj.RandomizeFeatureFile,Allrow_string)
       
        
if __name__ == '__main__':
    #Stop_List = set(stopwords.words('english'))
    Remove_List=["(",")"]
    stem=True
    lmtzr= SnowballStemmer("english")
    goldstandard=False
    #webtext_ic = wn.ic(webtext,False,0) # @UndefinedVariable
#------------------------------------------------------------------------------ done for NAACL
    #------------------------------------------------ topic="gay-rights-debates"
    # TrainingLIWC="/Users/amita/git/summary_repo/Summary/src/Similarity_Labels/Similarity_Data/CD_Convince_Forums_taining_gayrightsText.txt"
    # LIWC_words=os.getcwd() + "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/TopLIWC_category_words"
    # LIWC_Category=os.getcwd() + "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/TopLIWC_category"
    # Input_withdistSim=os.getcwd()+ "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/AllMT_task/Results/ExtDist5_NVA_CoreNLP_AllMT_Reg.json"
    # FeatureFile=os.getcwd() + "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/AllMT_task/Results/FeatureFileAllMT_Reg.csv"
    # InputCSvwithdistSim=os.getcwd()+ "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/AllMT_task/Results/ExtDist5_NVA_CoreNLP_AllMT_Reg"
    # StringFeatureFile=os.getcwd() + "/Similarity_Data/gay-rights-debates/MTdata_cluster/Labels_Updated/AllMT_task/Results/String_FeatureFileAllMT_Reg.csv"
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 
    
    topic="gun-control"
    pkgdir= "/Users/amita/git/summary_repo/Summary/src"
    FeatureFile=pkgdir + "/similarity_pkg/Similarity_Pkg_Data/"+ topic+"/Phase1/Features_Cos_Cluster_70_AVG_Noun_Verb_Ad."
    InputCSV=pkgdir + "/mechanicalTurk_pkg/gun-control/MTdata_cluster/Phase1/MT_PairsLabels_Oneitem"
    StringFeatureFile=pkgdir + "/similarity_pkg/Similarity_Pkg_Data/"+ topic+"/Phase1/gun-control/MTdata_cluster/Phase1/MT_PairsLabels_Oneitem.csv "
    LIWC_words="None"
    LIWC_Category="None"
    TrainingLIWC="None"
    FileHandling.csvtojson(InputCSV)
    InputJson=InputCSV+".json"
    similarity_label="label_"
    writedirRouge=os.getcwd() + "/Similarity_Data/gun-control/MTdata_cluster/Phase1/Rougue/"
    Featuresobj=Features(InputJson,FeatureFile,StringFeatureFile,TrainingLIWC,similarity_label,writedirRouge)
    Execute(Featuresobj)
    pass