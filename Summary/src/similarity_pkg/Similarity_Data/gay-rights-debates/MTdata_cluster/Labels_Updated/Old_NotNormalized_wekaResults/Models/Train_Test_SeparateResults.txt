BUILD A TRAIN MODEL USING 750 INSTANCES AND TEST ON HELD OUT TEST SET

=== Run information ===

Scheme:       weka.classifiers.functions.LinearRegression -S 0 -R 1.0E-8
Relation:     Rand_FeatureFileAllMT_Reg-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0-weka.filters.unsupervised.instance.Randomize-S42-weka.filters.unsupervised.instance.RemovePercentage-P25.0
Instances:    750
Attributes:   43
              Bi_lemma_overlap_count
              Dist_Sim_Adj
              Dist_Sim_Noun
              Dist_Sim_Verb
              LIWC_Catg_Achievement
              LIWC_Catg_Affective Processes
              LIWC_Catg_Biological Processes
              LIWC_Catg_Causation
              LIWC_Catg_Certainty
              LIWC_Catg_Cognitive Processes
              LIWC_Catg_Discrepancy
              LIWC_Catg_Family
              LIWC_Catg_Hear
              LIWC_Catg_Humans
              LIWC_Catg_Insight
              LIWC_Catg_Motion
              LIWC_Catg_Negative Emotion
              LIWC_Catg_Perceptual Processes
              LIWC_Catg_Positive Emotion
              LIWC_Catg_Relativity
              LIWC_Catg_Religion
              LIWC_Catg_See
              LIWC_Catg_Sexual
              LIWC_Catg_Social Processes
              LIWC_Catg_Tentative
              LIWC_Catg_Time
              LIWC_Catg_Work
              Lev_Dist
              MeanSimLabel
              Tri_lemma_overlap_count
              UMBC
              Uni_lemma_overlap_count
              diff_len
              noun_Path
              rouge_1_f_score
              rouge_2_f_score
              rouge_3_f_score
              rouge_4_f_score
              rouge_l_f_score
              rouge_s*_f_score
              rouge_su*_f_score
              rouge_w_1.2_f_score
              verb_Path
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===


Linear Regression Model

MeanSimLabel =

      2.4091 * Bi_lemma_overlap_count +
     -0.2523 * Dist_Sim_Adj +
      0.4816 * LIWC_Catg_Achievement +
     -0.3848 * LIWC_Catg_Affective Processes +
      0.5378 * LIWC_Catg_Causation +
     -0.6993 * LIWC_Catg_Humans +
      0.6362 * LIWC_Catg_Motion +
      0.3999 * LIWC_Catg_Negative Emotion +
      0.6646 * LIWC_Catg_Religion +
      0.289  * LIWC_Catg_Sexual +
     -0.6469 * LIWC_Catg_Social Processes +
      0.4888 * LIWC_Catg_Work +
     -0.3834 * Lev_Dist +
      3.0824 * UMBC +
      0.6114 * Uni_lemma_overlap_count +
     -4.6855 * rouge_2_f_score +
      2.8253 * rouge_4_f_score +
      3.8562 * rouge_l_f_score +
      0.5259 * rouge_s*_f_score +
     -3.3977 * rouge_w_1.2_f_score +
     -0.7603 * verb_Path +
      0.967 

Time taken to build model: 0.02 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0 seconds

=== Summary ===

Correlation coefficient                  0.4269
Mean absolute error                      0.8606
Root mean squared error                  1.0841
Relative absolute error                 87.4627 %
Root relative squared error             91.6828 %
Total Number of Instances              250  






---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





=== Run information ===

Scheme:       weka.classifiers.misc.InputMappedClassifier -I -trim -W weka.classifiers.functions.LinearRegression -- -S 0 -R 1.0E-8
Relation:     Rand_FeatureFileAllMT_Reg-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0-weka.filters.unsupervised.instance.Randomize-S42-weka.filters.unsupervised.instance.RemovePercentage-P25.0-weka.filters.unsupervised.attribute.Remove-R1-28,30,32-43
Instances:    750
Attributes:   2
              MeanSimLabel
              UMBC
Test mode:    user supplied test set:  size unknown (reading incrementally)

=== Classifier model (full training set) ===

InputMappedClassifier:


Linear Regression Model

MeanSimLabel =

      3.6064 * UMBC +
      0.248 
Attribute mappings:

Model attributes        	    Incoming attributes
------------------------	    ----------------
(numeric) MeanSimLabel  	--> 29 (numeric) MeanSimLabel
(numeric) UMBC          	--> 31 (numeric) UMBC


Time taken to build model: 0.01 seconds

=== Evaluation on test set ===

Time taken to test model on supplied test set: 0.01 seconds

=== Summary ===

Correlation coefficient                  0.358 
Mean absolute error                      0.9194
Root mean squared error                  1.1131
Relative absolute error                 93.4328 %
Root relative squared error             94.1319 %
Total Number of Instances              250     

   

