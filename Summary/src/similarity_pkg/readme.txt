This directory contains LIWC and a Python script which replicates it but can be called through code. The Python script requires a .dic file to work (recommend the 2007 version). Check the timestamp for when it was last modified.

********************************
* LIWC and the .dic files are copyrighted commercial software and thus made private for a reason. 
* DON'T SHARE WITH JUST ANYONE AND DON'T LINK ON THE OPEN WEB!!!! Log into nldslab to access. 
********************************


If they are made public temporarily for some reason, to make them private log in and run:
chmod 660 file.foo

The Python script can be shared with whomever, updated, and modified by anyone. If it gets a bad edit or deleted, ask around, somone will have an old version. Proper version control and a nice cleanup would be nice but I'll let someone else do it if they want it..

If you have scp you can get everything by:
scp user@nldslab.soe.ucsc.edu:/var/www/liwc/* ./my_local_dir/


