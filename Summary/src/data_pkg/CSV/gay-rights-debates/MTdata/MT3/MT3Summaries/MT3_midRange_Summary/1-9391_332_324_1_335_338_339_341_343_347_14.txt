
----------
D0
----------
S1 is in Britain where the experience with some years of gay marriages has allowed her to be comfortable that it is not a problem, although she believes gay sex is a sin. She argues that marriage is by definition between a man and a woman even if it is not stated, just as it would not be expressly specified to be between a man and a goldfish.  S2 sees S1 as being anti-gay marriage as a result and challenges her about it, but S1 is actually unopposed to gay marriage.  S1 and S2 also discuss the issue of whether possible production of handicapped children, such as in incestuous relationships or others involving genetically specific information should be barred on public policy grounds.


----------
D1
----------
Two people are discussing gay marriage.  S1 states that US laws doesn't say man and woman because it presumes that marriage is between a man and a women.  He makes the argument that gay people should be allowed to marry not because of an automatic right, but because it does no harm to society.  He states that in Britain, gay marriage has had no impact to society, and even though Christians believe gay sex is a sin, many other people commit sins and are still allowed to marry.  S2 states that the fact that gay marriage in ancient Rome was allowed means historically it wasn't always between man and women, as claimed, and gay marriage does not threaten Christianity.


----------
D2
----------
Both S1 and S2 are in favor of same sex marriage, but for different reasons.  S1 also is in agreement with the Christians who believe gay marriage is a sin.  While S2 feels gay couples should be granted the right to wed, S1 is of the opinion gay couples should be able to marry just because others are able to marry, even citing the fact that incestuous couples are permitted to marry and have children.  S1 wonders why gay people themselves are not taking a more positive approach to obtaining the right to marry advising the practice is accepted in Britain and has effected only the gay community.  S1 still does not feel gay marriage is a threat to Christian society.


----------
D3
----------
S1 is not against gay marriage. Britian has accepted gay marriage for a while and the sky hasn't fallen yet. He argues that gay people should be allowed to marry, not because they have a right but because it does not harm our society. He questions why there isn't a more positive approach to gay marriage by gays themselves. He adds that he believes being gay is a sin and everyone should be able to find their own path to hell. 
S2 claims that gays have existed in our history and questions what S1 is even arguing about if he is for gay marriage. He argues that gay marriage isn't a threat to Christianity and that's not the reason people want to ban gay marriage.


----------
D4
----------
S1 is not against gay marriage. Britain has it and he hasn't seen it have a negative impact on anyone. He thinks that gays do not have a right to marriage, but it should be allowed because it doesn't harm society. He wonders why there is not a positive approach to marriage by gay people. While he views homosexuality as a sin, he views other things as a sin as well. S2 talks of gay marriage in history and that it is false to claim marriage has always been a man and woman. S2 concedes it may not be a right, but thinks that also applies to heterosexuals. He does not view gay marriage as a threat to Christianity and asks how Britain got it.

