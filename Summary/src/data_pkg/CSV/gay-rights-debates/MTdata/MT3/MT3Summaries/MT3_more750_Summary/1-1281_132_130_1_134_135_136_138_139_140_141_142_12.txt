
----------
D0
----------
S1 argues that heterosexual marriage is one of the most universal of all human institutions, found in almost all cultures that have ever existed. They believe that the true question is proposing a justification in revolutionizing that ancient institution. They believe that there are groups of homosexuals who want to change marriage for various reasons, whether it be for romance, economic and legal reasons but also to affirm that homosexuality and heterosexuality are just as good as each other. They warn that there is a risk of backlash with this way of thinking. They believe that homosexuality is an eccentricity and is not equal in value to heterosexuality.
S2 does not believe that homosexuals are attempting to destroy the idea of marriage but only wish to be included into it. They believe that homosexuals should be as accepted socially as heterosexuals and find it difficult to see a problem with homosexuals believing and acting as equals to heterosexuals. They believe that homosexuals want to have a positive influence on the institution of marriage, not destroy it.


----------
D1
----------
S1 and S2 are discussing the topic of gay marriage rights.  S1 states he is not exactly opposed to gay marriage but would like someone to give valid reasoning behind restructuring the basis of marriage since it's been the same since the beginning of time, comparing it to the Parthenon.  S2 feels it is circular to classify marriage as simply heterosexual.  He believes no one is attempting to take away the idea of marriage, just asking for homosexual couples to be included in the fray.  S1 disagrees with S2's statements that at the root, most homosexuals fighting for marriage rights are doing so on a conservative note.  He feels instead that the reasons are a "we are entitled to what you have" mentality.  S2 disagrees stating that while many do want acceptance from society, most want marriage for the basic reasons of marriage.  He also feels S1's arguments support the notion that laws should stay absolute and cannot be changed or altered.  S1 disagrees with this statement advising he simply wants the reasons supported.


----------
D2
----------
S1: Heterosexual marriage has existed in all cultures for a very long time. How can one justify revolutionizing an ancient institution? Some homosexuals want to marry for love or legal status, but many of them want to do it to prove that they are just as good as heterosexuals. Homosexuals want what heterosexuals have, and want to make the mainstream accept gay culture. Gay marriage is not a good enough justification for revolutionizing the institution of marriage. Homosexuality is an anomaly among man, is not equal with heterosexuality, and does not deserve marriage.
 
S2: Gays want to marry so that their families can have the strength and protection that the institution of marriage provides. They have no interest in harming the tradition of heterosexual marriage. Gays want to marry as badly as heterosexuals want not to be married. Living laws are meant to evolve with society. Homosexuals are just as good as heterosexuals. Other societies have legal gay marriage and no revolutions have occurred. Just because something is traditional does not make it just.


----------
D3
----------
S1 claims that traditional, heterosexual marriage is a universal human institution existing in almost every culture across the Earth and compares traditional marriage to the Parthenon as an example of an ancient structure that is damaged but deserves to be preserved. He claims that some homosexual unions would occur simply to demonstrate that homosexuality is equal to heterosexuality and that it is acceptable. He argues that it is not totally unacceptable to revise the definition of marriage, but that there would need to be a very good reason to do so, and that homosexuality is simply an eccentricity that is not equal in value to heterosexuality. 

S2 argues that claiming that marriage has always been heterosexual is circular logic. He asserts that legalizing homosexual marriage wouldn't damage heterosexual marriages, but it would allow homosexual couples to receive the rights and protections currently afforded only to heterosexual couples.  He argues that just because something is traditional does not guarantee that it is right. He points out that S1 admitted to believing homosexuals were of lesser value.


----------
D4
----------
S1 Compares heterosexual marriage Parthenon, asks why it should be torn down.  States heterosexual marriage is a universal institution. S2 States that marriage is a living institution and subject to change, that S1 is using circular logic. States it is a small change to law and gays want to strengthen their families. S1 Asserts gay marriage is a major change. Believes that gays want marriage to advance acceptability of gay life style or legal benefits. S2 Claims that acceptance has been ongoing and marriage is a small step, gays are as good as heterosexuals. References ongoing changes to gender roles. S1 Claims that gender, sexuality and family organization are fundamental aspects of society. Claims homosexuality is an eccentricity not equal to heterosexuality. S2 States that S1 thinks homosexuals are of lower value, that the existing definition was made by heterosexuals. S1 Denies homosexuals are of less value, objects to lifestyle not individuals. S2 Links individuals to the behavior. S1 States it is possible to make person/behavior distinction. S2 States that orientation leads to the behavior.

