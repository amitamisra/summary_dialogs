
----------
D0
----------
S1 argues that at the turn of the 20th century, the topic of homosexuals would have been viewed mostly from a moral standpoint. They do not believe that anyone would have recognized it as an innate thing, and as entirely behavioral. They argue that it was not about prejudice and bigotry but about the fact that people at the time felt it morally wrong. They argue that prejudices against race at the turn of the century had nothing to do with whether they felt it was right or wrong at the time. They also argues that when a homosexual individual was put on trial, they were, not because of their homosexuality, but because they were performing acts that society at the time viewed as wrong.
S2 believes that people at the turn of the century felt it was a moral obligation to believe that certain races and women were inferior. They argue that the use of labeling, such as 'sodomite', on homosexual individuals is a reflection that how they were treated was based purely on prejudice.


----------
D1
----------
S1 divides the sexuality from the social identity of homosexuals throughout his arguments.  He defends those who in 1909 were against homosexuality.  He saw this as an issue not of bigotry but of ignorance.  He argues that other moral decisions that have been overturned such as racist and sexist decisions were not based on an argument that they were "good" decisions.  In dealing with sexuality, the term homosexual was relatively new and people did not even have the idea of two men in a relationship in their conceptual realm. There was persecution of gay men but he argues they were not persecuting against the people but rather the act of sodomy.
S2 does not believe there was an intentional separation of act and person in the early persecution of gay men.  He compares viewing homosexuality as immoral to previous racist and sexist beliefs that were considered the norm.  He questions how people can create a moral code based off ignorance.  He also argues against the separation of the act and person by citing the trial of Oscar Wilde and use of derogatory terms against homosexuals.


----------
D2
----------
S1 argues that homosexuality was understood to be a choice rather than an innate characteristic in the past. As a result, gay marriage could have never been considered right. S2 argues that this view is incorrect, anti-gay bigotry has always been anti-gay bigotry. S1 disagrees, stating that S2's opinion about past views on gay rights were misrepresentations brought about by modern gay rights proponents. Further, S1 states that S2's argument is ultimately irrelevant, gay behavior was, in the past, ultimately seen as immoral in nature. S2 argues that the morality underlying anti-gay rights agendas is based on ignorance and bigotry, which should make it invalid as a foundation for determining moral behavior. S1 reaffirms his belief that past classifications of homosexuality were based on their best understanding of the practice. S2 argues that  according to S1, gays are fine so long as they remain in hiding. Further, historically, people used slurs against gays and persecuted homosexuals, demonstrating their understanding of homosexuality as a behavioral choice. S1 argues that this is purely evidence of their ignorance.


----------
D3
----------
S1 and S2 are discussing attitudes toward homosexuality in 1909 as a result of an apparently recent 6-0 opinion of justices of a court which suggest to S1 that the justices agreed that in 1909 attitudes against homosexuality did not reflect prejudice or bigotry, but rather a more inherent moral aversion as a result of homosexuality having been less understood at the time.  S1 is of the opinion that these days it is generally accepted that being homosexual is a state of being, likely attributable to genetics, as opposed to aberrant behavior.  In 1909, he argues, the acts of homosexual sex or sodomy was what was persecuted and prosecuted, because the state of being an intrinsic homosexual was not studied or understood at the time. S2 is less forgiving and suggests homosexuality was well known in the Victorian era, citing the well publicized trials of Oscar Wilde, and that anti-gay attitudes has always been bigotry. S1 believes that in the past 'they' really did think that homosexuality was wrong, but that today 'we' know better.


----------
D4
----------
S1 believes that society in 1909 were being prejudiced in their intolerance for gay's, rather that they didn't have any concept of homosexuality, and that they were merely treating gays as if they were committing morally wrong acts, by their own free will not by any sort of genetical predisposition. He does not believe that people of the time thought that oppression of blacks or women was doing good, but that there were occasionally  religious arguments made to support those behaviors.
   S2 feels that this argument is hogwash. He makes no distinction between prejudice against behavior and that against states of being. He feels that basing a moral code on ignorance and prejudice are not intelligent. He cites that men from a certain time did not even think that women or other races should be viewed as being equal. He finds the idea that, as long as gays lead straight lives they are accepted, to be personally offensive. He points to the terms "sodomite" and "molly" as proof that people had an identity that they felt hatred towards.

