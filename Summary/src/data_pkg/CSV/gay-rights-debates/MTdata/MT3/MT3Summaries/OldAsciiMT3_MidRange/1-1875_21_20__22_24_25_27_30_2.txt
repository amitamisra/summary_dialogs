
----------
D0
----------
S1 claims that homosexual partners shouldn't expect shared healthcare in the same way that an unmarried couple shouldn't. He suggests that homosexuals are not being denied any rights, that they have all the same rights as heterosexuals. He points out that nobody can marry someone of the same sex, not just homosexuals. He rejects the idea that homosexuals will succeed in legalizing same-sex marriage, citing several cases where homosexual marriages were banned.
S2 accuses S1 of accepting that some people should not receive the basic right to marry the person they love. He likens this to slavery, prohibition, and women's suffrage. He claims that the proponents of same-sex marriage will eventually be successful, just as human rights advocates were with slavery, voting, and interracial marriage.


----------
D1
----------
S1 believes the two gay people living together should not expect healthcare since straight people that aren't married can't get it. He thinks gays can marry members of the opposite sex just as heterosexuals do and heterosexuals can't marry members of the same sex. He thinks gay people have the same rights as heterosexuals and does not view marriage as a right. Therefore, slavery and women's rights are bad comparisons. S2 thinks S1's argument is ridiculous and gays lack rights. He says gay people can't marry who they love and compares it to people protesting interracial marriage, slavery, and women's rights. He believes gay people are in the process to overcome and they will win the right to marry and to have healthcare as well.


----------
D2
----------
S1 and S2 are debating gay marriage and healthcare benefits.  S1 is against gay marriage as well as shared healthcare between gay couples.  S2 feels this objection points to S1's being in support of things such as segregation and prohibition.  S2 argues that gays do have the right to marry in order to qualify for healthcare, they just have to marry someone of the opposite sex, advising straight people and gay people have the same rights due to this.  S2 contends that due to the fact gay people cannot choose to marry someone they love, of the same sex, they do not have the same rights as heterosexuals and this is prejudice.  S2 believes gay marriage laws will be passed.


----------
D3
----------
S1 said that if gays want healthcare they can marry someone of the opposite sex just like straight people do. He said that there never will be a federal law allowing gay marriage in the United States. He said that he believes that S2's is wrong in his assertion that the gay marriage issue is similar to the issues of slavery, women's right to vote, etc. He said that gay marriage is not an unalienable right. 
S2 said that S1 probably liked segregation and was most likely against women voting too. He said that gays do not now have the right to marry  who they love but eventually will. He compared the struggle for gay marriage to conquering polio and getting the vote, etc.


----------
D4
----------
Two people are discussing gay marriage rights.  S1 states that gays don't have the right to shared health care because they're not married.  He states that gays do have the right to marry members of the opposite sex, but not the same sex, which is the same has heterosexuals.  He also states that there will never be a federal law allowing gay marriages in the US, as there is allot of backlash going on lately.  S2 contends that gays don't have the right to marry who they love, which is what they are fighting for.  He states that they will win, just as what happened with slavery and women voting, to which S1 states where entirely different issues.

