
----------
D0
----------
S1 believes that there are more limits to marriage than just the limit to opposite sexes and that if you do not follow the rules you do not qualify for the benefits of marriage. They do not believe that just because there are rules, anyone is barred from the custom. They do not believe that homosexuals have the right to the changes they want when it comes to marriage as they argue that the rules are global and not unequal. They argue that racial inequality between African Americans and white Americans in which they cold not marry was unequal, but that the situation cannot be compared to homosexuals because they are dissimilar.
S2 believes that S1 is wrong and that the argument for equality can apply to homosexuals as well, since heterosexual couples can marry while homosexual couples cannot. They believe that just as race criteria from marriage has been removed upon the grounds of being inappropriate and discriminatory, so should gender. They argue that because of the rules, heterosexual couples have benefits that homosexuals don't.


----------
D1
----------
S2 feels that the right to recognition of two people to form a couple is being unfairly denied homosexuals. That a heterosexual couple is allowed to make life decisions, file taxes and be covered under insurance jointly, inherit property, and more. He compares the ban on homosexual marriage to the former ban on interracial marriage, and feels that having gender as a qualifying factor for marriage is equal to using race as a factor. He calls the arguments against homosexual couple semantic, and feel that they are being denied rights because some people don't like the idea of them having sex.
   S1 denies the inequality of restrictions between homosexual and heterosexual couples and does not feel that they have the right to change this law. He denies that race and gender  are equivalent differences and says that there are no unshared barriers between hetero and homo sexual couples. He states that it is individuals that have rights not couples, and that no-one can marry; someone married, an animal, a minor, or a sibling. He uses evolution to validate the view that gay relationships have less meaning than straight relationships.


----------
D2
----------
S1 believes people are using some valid arguments for keeping homosexual couples from getting married, but he does not necessarily side with all of those arguments.  He suggests that couples of any kind, gay or straight, do not have rights.  Individuals are the ones who have rights, but those rights are limited by rules that cannot be changed just because somebody personally wants them changed.  For example, a person does not have the right to marry someone if they are already married, or someone who is a minor.  Further, he believes heterosexual couples can be viewed as more valid on an evolutionary basis.

S2 does not believe there any valid arguments to keep homosexuals from marrying.  He believes the issue of gay marriage is no different than than of marriage between different races.  He also disagrees with the notion that only individuals have rights.  The gay marriage issue is largely about the rights of couples, including the right to visit one another in a hospital, the right to inherit from one another, and the right to file taxes jointly.  He also believes the true reason for people being against gay marriage is disapproval of gays having sex.


----------
D3
----------
S1 claims that because there are rules doesn't mean one group is barred from the custom. Argues that the denial of marriage between races as discrimination is not similar to argument based on gender. Based on the rules, the barriers of marriage are the same for both heterosexual and homosexual couples and therefore one group is not being discriminated against. Just as no one can marry someone that is already married, an animal, or a minor, there are rules. He concedes that there is nothing wrong with changing something that is commonly agreed upon as need changing, but as it's not the case, homosexuals cannot have a change just because they want it. 
S2 is of the opinion that just like race was found to be a discriminatory qualifier for the criteria of marriage, so should a couple's gender be found discriminatory criteria also. He says that it is not just the custom that's denied, it's their rights as individuals in a relationship to obtain benefits, to visit in the hospital, or to purchase property together.


----------
D4
----------
S1 claims that homosexual couples are not eligible for marriage because there are rules for which relationships quality as a marriage and homosexual couples do not fall within those rules. He argues that this is the same as people being prevented from marrying someone who is already married, an animal, a minor, or a sibling. He claims that this is different from the previous rule limiting marriage to intraracial couples. He further argues that heterosexual relationships are more valid at the evolutionary level because they have a reproductive purpose. 
S2 argues that gender should be removed as a criteria for marriage in the same way that race was because it is inappropriate and discriminatory. He claims that it is unfair for heterosexuals to be eligible for the benefits of marriage such as the ability to make important medical decisions, to cover each other under their insurance, to file taxes jointly, to purchase property together, to visit each other in the hospital, or to be a legal heir, when those same privileges are denied to homosexuals.

