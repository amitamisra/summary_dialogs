
----------
D0
----------
S1 believes that gay couples, and their rights in regard to marriage, are no different than other deviant relationships, such as incestuous and polygamous relationships. He does not feel that gay marriage should be prohibited, rather that it is simply different than a normal heterosexual marriage, and to recognize this difference is not discrimination. He feels that if gay relationships were to be allowed then not extending the same benefits to all forms of relationships between consenting adults would then be discriminatory. He feels that gay couples who have children through surrogates have outsourced their children, and he should therefore not be responsible for them.
   S2 makes the case that he has been with his partner for 15 years, and will be having his own biological children soon. He feels that his family should be afforded the same rights as a heterosexual couple, that his husband should gain automatic custody if he dies, survivor benefits from social security, and others. He does not see his relationship with his husband as being the same as other forms of non-sanctioned relationships, and that each new form of marriage arrangements would need to be argued on their own merits individually.


----------
D1
----------
S1 believes that gay marriages should not be viewed the same way as heterosexual marriages; the speaker does state that he does not disagree with the idea of gay marriage. He says that gay marriage is a deviation from the traditional union between a man and woman. Any sexual relationship that is a deviation from the "norm," like polygamist and incestuous relationships, should also be granted the same rights. The speaker feels that these relationships are all on the same levels, and one should not be afforded rights that the others are not.

S2 identifies as a gay male, who has been in a 15-year-long homosexual relationship, and has a set of twins due soon; he feels his family should be granted the same rights as a heterosexual, married couple. He states that legalizing gay marriage allows for automatic guardianship of children if something happens to the biological parent, and also awards survivors benefits from social security. He believes that other types of marriage should be looked at based on its own situations and conditions.


----------
D2
----------
S1 states that same-sex marriage should not be prohibited, but believes that a homosexual marriage is different than a heterosexual marriage. He argues that recognizing this difference is not discrimination. He claims that children of same-sex couples or comparable to children from incestuous, polygamous, or other sexual deviant relationships. He makes the argument that if same-sex marriages are legalized, then all types of marriages between consenting adults should be legalized. He claims that the arguments in favor of same-sex marriage would work equally well for incestuous or polygamous relationships.
S2 identifies himself as a homosexual and argues that same-sex families deserve the same benefits as heterosexual families. He questions what difference exists that distinguishes his relationship from that of a heterosexual couple other than the genitals involved. He identifies a variety of benefits received only through marriage, such as social security and guardianship among others. He takes offense at S1's comment that his children are "outsourced" and rejects the idea that homosexuals are fighting for marriage equality simply because they free handouts from the government.


----------
D3
----------
S1 believes that homosexual marriages should not be prohibited, but also believes that heterosexual marriages and homosexual marriages are not the same. They believe that if homosexuals are allowed to marry, than any kind of marriage between consenting adults should be allowed, otherwise you are discriminating against the person wanting to enter into the marriage. They believe that S2 deserves as much protection as an incestuous or polygamous relationship that has children. Overall, they believe that if homosexual marriages are allowed then it should branch out to other types of potential marriages.
S2 points out that with full civil marriage equality, benefits otherwise not available to homosexual couples are now options such as survivor's benefits of social security or retained guardianship of their children. They don't see a reason for homosexual couples to have to go through a different process than heterosexuals in order to gain the same benefits of heterosexual marriage. Since S2 has children, they do not believe that they are any less deserving of government benefits of marriage than heterosexual couples with children.


----------
D4
----------
S1 argues that allowing gay marriage would set a precedent that would be used to allow marriage for other forms of traditionally socially unacceptable intimate or sexual relationships, such as incest between half brothers or a mother and son, and/or polygamy.  S2 is in a 15 year male homosexual relationship and is anticipating twin children that are biologically related to him.  He challenges S1 to explain why he and his family should not be entitled to the many benefits associated with marriage, such as social security surviver status.  S1 provides a putative explanation by noting that his children are outsourced, meaning that one of the parents of the children is not part of the family intended to receive the benefits of marriage.  S1 also notes that others do not qualify, and likens S2 to a single mother who has many children and then complains about not enough help from the government.  S1 repeatedly compares gay relationships to other types of relationships which he considers to be devious in order to justify excluding gays from marriage rights.

