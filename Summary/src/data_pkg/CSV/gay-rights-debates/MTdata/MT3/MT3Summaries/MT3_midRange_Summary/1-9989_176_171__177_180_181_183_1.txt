
----------
D0
----------
S1 brings up a quiz from The New York Times that supposedly determines how you compare to the current Supreme Court Justices on Supreme Court issues. He claims that the problem with the quiz is that it is based on people's opinions and he does not believe that the Supreme Court should make rulings based on their opinions because they are supposed to be ruling entirely based on their interpretion of the constitution. S2 agrees with S1 that the Supreme Court decisions should not be described as conservative or liberal because political viewpoints should be irrelevant to interpreting the constitution. He also provides an example of one Supreme Court Justice who is known for making rulings that do not appeal to either political party.


----------
D1
----------
S1 and S2 are discussing a quiz which was run on the New York Times website in regard to comparing views of the general public as compared to those of the Supreme Court.  S2 feels the the quiz is horrible as it, in S2's opinion, reinforces the point that people view the court as a legislative body.  S1 feels the quiz is useful but does have some issues.  S2 believes the rulings should not be considered conservative or liberal.  S1 agrees with this point.  S2 feels court rulings should not be based upon a judge's personal beliefs as they are supposed to be based on the constitution.  S2 uses the example of corporations being allowed to run campaign ads.


----------
D2
----------
S1 does not believe that the Supreme Court should rule based on the majority's opinions of the issue, but on the Court's interpretation of the United States Constitution. They don't believe that court rulings should be reflective of the justices' personal political beliefs. They are against applying labels such as "conservative" or "liberal" to the court's decisions.
S2 believes that most people are using the Supreme Court as a second legislative body, and agrees with S1 that a ruling should be made in an unbiased manner and purely on their legality under the Constitution. They agree that the Supreme Court rulings should not lean to one political outlook or the other. They believe that those passing rulings do so regardless if their decision offends the public.


----------
D3
----------
Two people are discussing how Supreme Court Justices should rule on an issue.  S1 introduces the topic with a quiz by the NY Times that shows how a person's views line up with those of the current Supreme Court.  He states that its main problem is that it asks people's opinions of the issue, but doesn't address the constitutional questions involved and he states that the Supreme Court should rule on the constitution, not their personal beliefs.  S2 agrees, adding that rulings shouldn't be considered "conservative" or "liberal."  He also states that people are making the court a second legislative body, and if the Supreme Court makes decisions based on opinions then Congress should do it because they are voted in.


----------
D4
----------
S1 provides a link to a quiz to see where ones views line up with those of the Supreme Court and the American public. He thinks the problem with the quiz is that it is opinion based. S1 thinks the Supreme Court should not rule based on their opinions, but rather on their interpretation of the Constitution. Also, he thinks labels such as conservative are incorrect as they express a political view and not judicial interpretation. S2 dislikes the quiz. He thinks people are making courts a legislative body and if rulings are opinion based Congress should do it as they are voted in. He agrees that the rulings should not be considered liberal or conservative and gives an example of corporations running ad campaigns.

