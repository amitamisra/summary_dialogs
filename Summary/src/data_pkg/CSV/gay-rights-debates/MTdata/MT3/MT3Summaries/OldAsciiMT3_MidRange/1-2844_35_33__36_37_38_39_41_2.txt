
----------
D0
----------
S2 suggests that he would be liberal if modern liberalism was actually Jeffersonian, meaning that democracy is viewed as the power of the people. He cites Jefferson's beliefs that war can be necessary in order to maintain liberty and that centralized government is a threat, and points out how these beliefs are the opposite of modern liberalism. He further claims that the current Democratic party is anti-religion, over-regulatory, and controlled by the gay lobby. S1 interprets S2's argument as a claim that fairness should exist for all people except homosexuals. S2 clarifies that he does support equal opportunity for all people regardless of their sexual behavior, but that he is still opposed to redefining the traditional view of marriage by legalizing same-sex marriage for homosexuals.


----------
D1
----------
S1 asks S2 to clarify his statement about Jeffersonian liberalism. He believes that S2 is hypocritical about supporting Jeffersonian values on "making sure there is fairness" and "honest advocacy" for the people while opposing gay marriage. 
S2 believes that the term "liberalism" has a different meaning different compared to the time of Thomas Jefferson. He said that Jefferson is for decentralization of government which is the opposite stance taken by the Democratic Party. He believes that liberalism has been hijacked by extremist special interest groups such as the gay lobby. The Democratic Party today is favoring special interest groups instead of engaging in honest advocacy for the people. S2 supports equal opportunity for gay people, but not the redefinition of marriage in the US.


----------
D2
----------
Two subjects are discussing liberalism and its similarities or lack thereof to the Jeffersonian belief system.  S2 believes they are nothing alike while S1 believes they have many similarities.  S2 advises Jefferson believed liberty would be threatened by a centralized government.  He also advises liberalism is not based on the same theory.  S1 believes the opposite stating the opinion that the group believes in fairness for all, except the gay community.  S2 advises he supports equal opportunities for all people despite their sexual preferences; however, he does not support the redefinition of marriage to allow for gay unions.  S1 feels this is a double standard because it is basically saying equal rights for all but gays still cannot marry.


----------
D3
----------
S1 supports gay marriage and thinks S2 is being hypocritical by saying he agrees with fairness and honest advocacy but excluding gay people from it. He implies with sarcasm that S2 thinks marriage is only for heterosexuals that wish to reproduce and  only uses religiously approved positions. S2 wishes modern liberalism were more like Jeffersonian liberalism. He states decentralization was a big theme for Jefferson and he would have opposed over-regulation. He says he agrees with taking care of the elderly, fairness, and freedom. However, he does not support redefining marriage, and thinks modern liberalism has been taken over by special interest groups, such as the gay lobby, in the same way the Republicans were taken over by the religious right in the 1980's.


----------
D4
----------
Two people are discussing politics and gay marriage. S2 contends that modern liberalism has strayed from what Jefferson believed.  He states that Jefferson believed that liberty was threatened by a centralized government, and he would have opposed over-regulation.  Instead, the liberalism embodied by the Democratic party is opposite in stance, becoming anti-religion, over-regulatory and taken over by extremist interest groups, much like the Republicans were taken over by the religious right.  S2 states that he supports equal opportunity for people regardless of sexual behavior, but not the redefinition of marriage.  S1 contends that S2 is doing the same thing by effectively stating marriage is only for heterosexual people who use sex only for procreation.

