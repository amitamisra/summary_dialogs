
----------
D0
----------
S1 and S2 are talking about how Democracy is based on majority rule and whether or not its fair. S1 believes it is completely unfair that gay marriage be dismissed or disliked and S2 argues that no matter what S1 believes is right or wrong, the majority of people control what is normal in our society. That if S1 is only towards the rights of gay people it is completely understandable for this lady to be in belief that marriage as is is fine. S2 believes that S1's argument is not a legitimate viewpoint because S1 has no justification that this lady hates gays. S1 believes that because this lady is not in favor of gay marriage, the lady is anti-gay. S1 is taking an aggressive approach by saying they will talk to bible churches and disrupt them because she feels like she has no place in society. S1 thinks that the majority of people is enslaving their people by not allowing gay rights, but S2 argues that it happens that way and it changes over time.


----------
D1
----------
S1 does not believe S2 has answered her question of how equal marriage hurts any heterosexual.  S2 believes the point has been made because different laws affect different people.  S2 attacks S1 by calling their argument silly because it violates the First amendment rights. S2 remarks that S1 is only concerned with the rights of gay and the anti-gay Christianity. S1 specifically names a woman in particular to admit her discrimination. S1 expresses that the rights of gays is their first concern and protection from church. S1 threatens to respond with similar actions against the church, like protesting ceremonial events and church services.  S2 urges S1 to allow the democratic process to decide the issue, believing the anti-gay movement may also have legitimacy. S1 questions the majority rule ideology because of incidents in the past like slavery and concentration camps., and once again calls out the women. S2 ends by stating those would not be possible today because of the 13th amendment. 

----------
D2
----------
(S1) states how can gay marriage hurt politicians or anyone else? (S2) says that there are laws that hurt all kinds of peoples and argues about violation of the First Amendment by imposing someones belief over another. (S1) argues that he is only concern with gay peoples rights and says that the politician wants gays to remain as second-class citizens. Argues that politician/she is discriminating and protecting her heterosexual privilege. He also argues that not accepting gay marriage is punishment to him, his family and others. (S2) He argues about the kind of democracy in where majority of the people decide what best for society and how being anti-gay is a right of the First Amendment.  Is a gay person is allowed to have the freedom, so does he who and others who may not agree. (S1) then gives the example that not just because majority rules it means that its a good thing. For example, enslaving people was accepted in previous societies. (S2) argues that although previous societies had legal rights to enslave people but that doesnt mean that it makes those decisions all the time and that is like gay marriage. 

----------
D3
----------

 
 
 S1 asks S2 how equal marriage would hurt someone, her, or anyone else. S1s way of equal marriage would not S1 or her. S1 is only concerned with gay rights and will talk people out of attending bible churches. S1 will try to legally disrupt any of the fundraising activities that they have going on, protest their weddings and their services. Once they crossed the line and enacted laws that hurt S1 and S1s family, then they have no place in society. The majority should not have the right to enslave their fellow citizens and send them to concentration camps. She should admit to her bigotry, discrimination and anti-gay views.
 
 
 S2 is glad that S1 abandoned a silly argument about the violation of the first amendment right, but still does not see why only laws that do not cause harm should be enacted. S2 is surprised at S1s affinity for the harm principle given that S1 has expressed being at war with evangelical anti-gay christianity. S2 holds that people should be about to legislate their views, and wants S1 to explain why that shouldnt be so with anti-gay people. In a democracy like ours people use the democratic process to have views enacted into law. If she is free to do this, then so is S1.

----------
D4
----------
S1 questions to how equal marriages hurts the woman or any other heterosexual. The only person that gets hurt is S1. If equal marriage is incorporated into the law then nobody gets hurt. S2 talks about what if a proposed law hurt someone and that there are all kinds of laws that hurt all kinds of people. S1 says he is only concerned with the rights of gay people. He calls the woman evil and that she should admit that she is anti-gay and wants to protect her heterosexual privilege. S2 believes that S1 just cares about gay people but has not shown why anti-gay people should not be able to legislate their view. S2 says that S1 and the woman both have their own opinions. S1 asks whether the majority should have the right to enslave their fellow citizens or lock people up in concentration camps. S2 responds saying the majority did have the right to enslave fellow citizens until the 13th amendment took place and says that the majority wins in the united states.
