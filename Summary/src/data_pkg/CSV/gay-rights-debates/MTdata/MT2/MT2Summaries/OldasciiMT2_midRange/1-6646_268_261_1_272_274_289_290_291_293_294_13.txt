
----------
D0
----------
S1 claims that it is inappropriate to label disagreement as bigotry, and that while someone might think an argument is bigoted, it may be motivated by something other than bigotry. He argues that many people's opinions are based on their view of marriage, not their dislike of homosexuals, and that they would be against any group wanting to alter the traditional view of marriage. 
S2 claims that opponents of gay rights are discriminatory and bigoted. He suggests that if you replaced gays with another minority, then the arguments against them would appear negative (e.g., trying to exclude any other minority from getting married would not be acceptable). He believes that opponents of gay marriage are not protecting traditional marriage, but are discriminating against homosexuals. 


----------
D1
----------
S1 believes referring to arguments of disagreement as bigotry is done because the accuser lacks factual information and believes S2 uses the statement for its effect since it is thought of as an ugly word.  S1 advises not all people who are against gay marriage are bigots but feel the way they do due to the way they view marriage.  S1 argues the issue is not always about excluding gays from different things but that it boils down to marriage.

S2 invites the word gay to be replaced with another minority and the statements made to not appear to be bigoted.  S2 advises excluding gays from different pieces of society is the same as excluding any other minority and is just simply bigotry.


----------
D2
----------
Two people are arguing gay marriage.  S1 states that resorting to name calling indicates the inability to resort to sound arguments.  He then states that the word bigotry is an ugly word, and its ignorant to automatically write off disagreements as "bigotry".  S2 rebuts by saying that he's calling it bigotry because that's what it is.  He then states that if it were another minority being challenged then it would clearly be bigotry. He states whenever gays are involved a group of people always opposes.  S1 refutes that by saying its all about motivation, if any other minority would try and change traditional marriage, they would be met with the same opposition.  


----------
D3
----------
S1 does not believe that disagreement is the same as being bigoted, and that if someone resorts to name calling in an argument it means they no longer have any sound arguments. They argue that any argument goes back to one's motivations and that people don't often oppose homosexual marriage on the basis of not liking homosexual individuals, but because of their views on marriage. They argue that people who base their arguments on how they view marriage would still think that way towards any group that desired to change their definition of marriage.
S2 disagrees and believes that arguments made against homosexual marriage are based upon a bias against homosexuality as a whole. They argue that these peoples' opinions are definitively against homosexuals in general. 


----------
D4
----------
S1 asserts that those who are against gay marriage are not anti-gay per se, but are against redefining marriage. He argues that any minority group who attempts to redefine marriage to include anything beyond a man and a woman will get a similar response from the majority group. Hence calling those who oppose gay marriage bigot is inaccurate
S2 asserts that those who oppose gay marriage are not simply attempting to preserve the current definition of marriage as between a man and a woman. He argues that the opposition to gay marriage is a lack of acceptance of homosexuality or its visible and open displays and representations. This is an anti-gay bias. Hence the term bigot is appropriate to those who oppose gay marriage.

