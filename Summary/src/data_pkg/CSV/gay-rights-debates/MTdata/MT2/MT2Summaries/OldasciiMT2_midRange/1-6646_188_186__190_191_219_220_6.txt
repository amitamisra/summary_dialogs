
 ----------
D0
----------
S1 presents the idea that gay marriage has been pervasive in many cultures, including the Romans, Greeks, Celts, and American Indians. He also mentions the existence of other alternative forms of marriage such as polygamy and polyandry. He claims that gay marriage is not a new idea and that the idea of marriage being between a man and a woman is a recent development.
S 2 claims that although homosexual acts did occur in many past cultures, these relationships were not marriages. He insists that marriages were almost exclusively between people of opposite genders. He argues that same-sex marriage has never been commonplace or accepted, despite homosexual acts occurring at times, and that the original idea of marriage is a union between people of opposite genders. 


----------
D1
----------
S1 provides two websites which outline the history of gay marriage in a few different cultures advising homosexuals were held in high regard in some cultures and were thought of as healers and prophets.  S1 cites several cultures who practices gay unions such as Romans and Celts.  S1 advises some cultures still practice polygamy and one in Nigeria  even practices polyandry. 

S1 advises while homosexual unions were present in history they were not actually thought of as marriages but more of unions for a specific purpose.  He/she contends that matrimonial unions were between opposite genders, even throughout history.  S1 concedes that homosexual liaisons were common in other times and cultures but never recognized as actual marriages and were frowned upon. 


----------
D2
----------
Two people are discussing gay marriage. S1 states that gay marriage is not a new idea, as many societies have historically practiced it and in some cases the homosexuals were valued.  He also asserts that even in the old testament marriage was not just between one man and one woman; polygamy was present.  S2 states that historically marriage has been about different genders.  He then states that even though the societies had homosexual liaisons, but they were not regarded as marriage but as unions for a particular purpose.  In some cases they were even frowned upon.


----------
D3
----------
S1 bring sup historical evidence in support that homosexual marriage was conducted in the past, naming the Romans, Celts, Greeks and Native American tribes. They argue that such unions were important in the Native American tribe's culture as these individuals were seen as healers and prophets. They also point out that polygamy has been around historically as well, and argues that marriage has never been a binary union between one man and one woman.
S2 responds by stating that the evidence provided does not classify these unions as marriage, but special case scenarios that do not overlap with existing marriage laws in existence. They argue that marriage has always been considered to be between one man and one woman and that homosexual liaisons were special cases. 


----------
D4
----------
S1 asserts that gay marriage and polygamy are commonplace in ancient Greece, Rome and China. He listed hyperlinks to the source of his assertion. He argues that marriage as a union between one man and one woman is a recent construct as compared to gay marriage.
S2 asserts that same-sex marriage has never been commonplace. He argues that a couple of Buddhist sects allow it. It was frowned upon in Roman times. The Greeks had homosexual liaisons, some from desire, some for a form of mentoring reasons. But those were neither marriages nor assumed to be so by those societies. Marriage is between people of opposite genders in those societies, and thus it is not a new concept.

