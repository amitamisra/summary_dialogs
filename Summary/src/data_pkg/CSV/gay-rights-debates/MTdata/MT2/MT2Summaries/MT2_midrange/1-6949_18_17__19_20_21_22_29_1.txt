
----------
D0
----------
S1 and S2 are discussing a legislative action associated with a Massachusetts court ruling which is favorable to gay rights.  S1 is enthusiastic about the matter and sees it as a step in the right direction against bigotry, and a recognition that the world is not as anti-gay as it used to be, which S2 is advised to acknowledge and accept.  S1 also accuses S2 of being inconsistent in having been on the record on the website opposing gay marriage (and therefor according to S1 being opposed to equal rights for gays), but now also taking a position that the state should not intervene to ban gay marriage.  S2 defends that position and argues it is based on conservative principles against intrusions into individual lives.


----------
D1
----------
S1 believes the world changed with the legislation in Massachusetts to allow gay marriage. He assumes that S2, having stated several times he is against gay marriage, will hope the legislation is overturned but may be out of luck. He questions if S2's opinion has changed now that gay marriage is in place in Massachusetts and if he wants it overturned. S2 resents the assumption of S1 and says that legislation to overturn the Massachusetts decision would be an over intrusion by the government. He states that he would not want an amendment to ban gay marriage and that has always been his opinion because he does not agree with over intrusion by the government. He sees this as a true conservative opinion to take.


----------
D2
----------
Speaker one believes the world changed when Massachusetts legislature ratified a court decision for equal rights regarding homosexuals. The speaker also believes the world is not as anti-gay as it used to be. Speaker one believes that speaker two is opposed to gay marriage because speaker two does not support equal rights for homosexuals.

Speaker two has opposed gay marriage and likely equal rights for gays in the past, but opinions that the cause of this opposition is that the speaker believes that government is over-intruding by creating legislature regarding the topic of gay marriage. Speaker two has a very conservative stance in politics and believes government should not be interfering with people's personal lives. 



----------
D3
----------
Two people are discussing gay rights.  S1 states that the world changed when the Mass. legislature ratified a court decision for equal rights.  To him, it means that people are changing against homophobic bigotry and he contends that S2 hopes that the bigots will overturn equality.  S2 disagrees with this, stating that the court decision for equal rights is not a big change, and he only disagrees because the legislation is an over-intrusion by the government.  S1 rebuts stating that such a position means S2 favors gay marriage, to which S2 rebuts by stating that he believes that legislation favoring or banning gay marriage are both over-intrusive into people's lives by the government.


----------
D4
----------
Two people are discussing gay rights and the redefinition of marriage as it pertains to gay couples.  S1 believes the Massachusetts decision to allow for gay couples to marry is proof positive the world as a whole is changing to be more accepting of gay couples.  S2 is defending a statement he/she made in regard to the possibility that Massachusetts may at some point decide to overturn the decision to allow gay couples to marry.  S1 feels statements made by S2 indicate he/she is completely opposed to equal rights for gay couples.  S2 contends he/she is against an amendment put in place to ban gay marriage as it would be the government being overly intrusive into the lives of the public. 

