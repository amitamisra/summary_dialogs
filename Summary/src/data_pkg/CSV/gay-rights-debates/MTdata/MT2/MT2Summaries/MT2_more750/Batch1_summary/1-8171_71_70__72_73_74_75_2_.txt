
----------
D0
----------
It seems like they are discussing the inconsistency of each other's arguments. S1 is getting annoyed and seems to just be arguing because they are "venting" while S2 is saying that the inconsistency is present and making all arguments invalid without proof. Both S1 and S2 are for gay rights, S2 is stating that S1 isn't being consistent and "admits" to being inconsistent, while S1 is saying they don't care about the inconsistency because of the point they are trying to get across. They bring up Biden and abortion in ways that dont exactly fit the discussion, but just to show the inconsistency in each others opinions. S2 is saying they would handle this same situation with Biden the exact same way because S2 is simply talking about perspective. S1 believes S2 is stating that S1 believes in gay marriage but not gay rights, which seems contradictory to S1's point. This argument does not have a set topic.


----------
D1
----------
In this discussion S1 and S2 are addressing the inconsistencies of the position of being for gay rights, but against gay marriage. S1 believes that Biden saying this is inconsistent, and it should bother S2 because S2 is for gay marriage. S2 does not believe this is an inconsistent statement and attributes Bidens view to S1, making it seem like S1 is calling his own view inconsistent. S1 and S2 begin arguing about the meaning of inconsistent and eventually starting fighting. The example of abortion comes up to try and prove inconsistency, but both use it in different ways so it does not stop the argument. S1 believes that S2 should be more concerned with Biden being inconsistent, while S1 is just trying to address the issue as a whole. S2 claims to only have brought up Biden to address the lack of political action due to social pressures. S2 claims that if given the opportunity, they would express the same concerns with Biden on a one on one conversation. 
 
 


----------
D2
----------

 (S1) states that one who supports gay right but not gay marriage brings an inconsistent problem. (S2) says that he does not sees any inconsistency, and compares it to the issue of abortion and how one can be against such issue for society but for ones position it may be possible. (S1) tells (S2) that it seems like he does not understand the concept of inconsistency. (S1) also says that he hopes that (S2) did not think of gay marriage to be a right. They keep arguing about positions in the matter and who is inconsistent. (S2)  says that many people often times say things such as I dont believe gay marriage is a right but they also believe that gay people should have equal rights. He argues that Bidens opinion is consistent and Biden is not lying about what he believes. (S1) says that he know that there is inconsistency but he does not care. (S2) says that people weight the ay marriage opinion issue against politicians weeks before elections and balance the responding approach. 


----------
D3
----------

 S1 one believes that it is inconsistent to support gay rights but to be against gay marriage. S1 does not have a problem with such an inconsistency but knowing that S2 supports gay marriage it would make sense that S2 would be bothered by it. S1 doesnt believe that S2 understands the concept of inconsistency. S1 thinks that S2 would be troubled by Bidens position on gay marriage.  it would be like someone saying they are for reproductive / abortion rights , but yet being against it being legal for women to get an abortion at an abortion clinic . 
 
 
 S2 does not see an inconsistency in not supporting gay marriage but being for gay rights. S2 explains that  there is not an inconsistency in being against gay marriage , but for gay rights in every other way . Showing an inconsistency would be like Frank saying he is against abortion rights but has no problem with his wife getting one. Just saying that Frank is inconsistent on abortion issues is not enough to show what is inconsistent. S2 is arguing against the position regardless of who is holding it.

----------
D4
----------
S1 is not bothered by the inconsistency of one supporting gay rights but not gay marriage, but if one supports gay marriage they would be bothered by it. S2 doesnt see the inconsistency and doesnt understand why S1 keeps calling it that. He says that S1 hasnt shown an inconsistency yet and gives an example of how it is like saying Frank is inconsistent on the abortion issue, but would be showing inconsistency if is Frank said I am against all abortion. S1 doesnt know what else to say and that it would be different if S2 didnt think gay marriage would be a right. S2 argues that S1s position is inconsistent and that S2 can see a person for gay rights and against gay marriage. He thinks Biden is wrong but his view is consistent. S1 says he doesnt care about the inconsistency of one supporting gay rights and not gay marriage. S2 says to push hard on Biden about the gay marriage opinion a few weeks away from election day and balance the response.
